/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Register a <script> for <write> event from Knowledge Management.
 *  
 *      <canvas>{Text} to <script>.innerHTML</script>
 *      
 *      @Value of event.object.toString()
 *      
 *  Will wrap contents of <script> with an anonymous function.
 *  
 *  <script>
 *      (function(){
 *          <!-- CONTENTS -->
 *      })()
 *  </script>
 *  
 */
(function(){
})()
