/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Register <text> for "knowledge management"
 */
(function(){
    console.log("KM.js")
    var catalog=function(event){
        console.debug("Event Catalog",event)
        if(!event.catalog){
            var origin=GNA.factory(event)
            document.addEventListener("catalog", catalog, true)
            event.catalog=[function(){
                document.dispatchEvent(origin)
            }];
            return;
        }
        event.catalog.push(function(){
            document.dispatchEvent(origin)
            })
    }
    KM=document.createEvent("Event");
    KM.initEvent(KM.timeStamp,true,true);
    document.addEventListener(KM.type, catalog, true);
    console.warn("Dispatch KM");
    document.dispatchEvent(KM);
    var update=function(){
        
    }
    var plus=function(){
        var location=[0,window.innerHeight];
        var canvas=DNA.html("canvas",".plus");
        var context=canvas.getContext("2d")
        canvas.className="plus";
        canvas.width=parseInt(window.innerWidth/20),
        canvas.height=parseInt(window.innerHeight/20);
        DNA.style.append(".plus","left",location[0]+"px");
        DNA.style.append(".plus","top",location[1]+"px");
        DNA.style.apply(".plus");
        var moveUp=function(){
            if(location[0]>window.innerWidth){
                location[0]=0;
                location[1]=window.innerHeight
            }else{
                location[0]+=1;
            }
            if(location[1]<0){
                location[0]=0;
                location[1]=window.innerHeight
            }else{
                location[1]-=1;
            }
            DNA.style.append(".plus","left",location[0]+"px");
            DNA.style.append(".plus","top",location[1]+"px");
            DNA.style.apply(".plus");
        }
        var upGradient=function(){
            var center=[parseInt(canvas.width/2),parseInt(canvas.height/2)];
            var radial=context.createRadialGradient(center[0],center[1],0,center[0],center[1],canvas.height);
            radial.addColorStop(0,"#ffffff");
            radial.addColorStop(.5,"#000000");
            // Fill with gradient
            context.fillStyle=radial;
            context.fillRect(0, 0, canvas.width, canvas.height);
        }
        var interval=window.setInterval(moveUp,10000);
        upGradient();
    }
})()


