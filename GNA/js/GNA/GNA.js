/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    console.warn('GNA == (GoF ==> G([F(x,y)])==GNA(DNA(TNA,QED))) //transformation requires the set of functions at position (x,y)')
    /**
     * i 	Perform case-insensitive matching
     * g 	Perform a global match (find all matches rather than stopping after the first match)
     * m 	Perform multiline matching
     * 
     */
    id=/id/g;
    console.groupCollapsed("Regular Expression details");
    var canvas=document.createElement("canvas");
    
    var context=canvas.getContext("2d");
    var color=context.createImageData(1,1);
    /**
    local.update();
    var pallet=function(){
        var select=function(color){
            canvas.backgroundColor=color;
        }
        var local={
            red:0,
            green:0,
            blue:0,
            trans:255,
            update:function(){
                console.log("UPDATE!")
                color.data[0]=local.red,
                color.data[1]=local.green,
                color.data[2]=local.blue,
                color.data[3]=local.trans,
                context.putImageData(color,0,0)
            }
        }
    }
    
    
    GNA.init(event);
    var clone={
        plant:function(){
            console.group("EVENT EVENT EVENT EVENT EVENT!!(@i):%o",counter++,Date.now());
            GNA={
                now:Date.now(),
                wait:function(offset){
                    var temp=offset,
                    wait=function(){
                        return temp;
                    }
                    return 
                },
                time:function(){
                    return Date.now()
                },
                _context:"Event",
                event:"event",
                couple:"couple",
                decouple:"decouple",
                trigger:"trigger",
                destroy:"destroy"
            }
        },
        tree:function(){
            console.group("TREE TREE TREE TREE TREE TREE TREE TREE TREE !!(@i):%o",counter++,Date.now());
            GNA={
                _worker:function(feature){
                    if(!GNA[GNA.context]){
                        declare(GNA.context)
                    }
                    return GNA[GNA.context](feature);
                }
            }
        }
    }
    return clone;
    */
})()
