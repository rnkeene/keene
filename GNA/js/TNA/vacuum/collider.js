/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 *  
 *  ------- PATTERN REGISTRY
 *  
 *  LAWS 
 *      QED RULEBOOK
 *  
 *  BEZIER CURVES
 *  
 */
(function(){
    QED.state();
    var collapse= QED.gravity.shift();
    console.log("COLLAPSED",collapse)
    window.addEventListener("keydown", collapse.faster, true)
    window.addEventListener("keyup", collapse.slower, true)
    window.addEventListener("mousedown", collapse.stop, true)
    window.addEventListener("mouseup", collapse.faster, true)
})()
