/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var body=function(){
        document.body.style.position="static",
        document.body.style.backgroundColor="#FF0000",
        document.body.style.overflow="hidden",
        document.body.style.zIndex=0;
        document.body.style.margin=0;
        document.body.style.padding=0;
        document.body.width=window.innerWidth;
        document.body.height=window.innerHeight;
        body=null,delete body;
    }
    window.addEventListener("load",body,true)
    var tree=function(grow){
        grow.event=document.createEvent("Event");
        grow.game=function(catalyst){
            var history=function(event){
                event.agent.voltage=Date.now()
            }
            grow.event.initEvent(grow.event.timeStamp,true,true);
            document.addEventListener(grow.event.type,grow)
            document.addEventListener(grow.event.type,history)
            document.addEventListener(grow.event.type,catalyst)
            return function(){
                document.dispatchEvent(grow.event)
                tree=null,delete tree;
            }
        }
        grow.child=function(nurture){
            document.addEventListener(grow.event.type,nurture)
        }
    },agent=function(event){
        event.agent={
            current:Date.now()
        }
    },dungeon=function(event){
        event.agent.resistance=Date.now()
    }
    tree(agent);
    var play=agent.game(dungeon);
    TNA=function(nurture){
        if(!!nurture){
            agent.child(nurture)
        }
        if(document.body!=null){
            play()
            return
        }
        document.addEventListener("load",play,true)
    }
    var local=function(event){
        document.removeEventListener(event,local);
        event.agent.constants={
            time:[event.timeStamp,Date.now()],
            gravity:[299792458,299792458*299792458],
            space:[window.innerWidth,window.innerHeight],
            min:["0","0","0"],
            max:["FF","FF","FF"],
            ohm:{
                current:event.agent.current,
                resistance:event.agent.resistance,
                voltage:event.agent.voltage
            },
            color:{
                black:["0","0","0","FF"],
                white:["FF","FF","FF","FF"],
                red:["FF","0","0","FF"],
                yellow:["FF","FF","0","FF"],
                green:["0","FF","0","FF"],
                blue:["0","0","FF","FF"],
                glow:[["0","FF","FF","FF"],["FF","0","FF","FF"]]
            }
        }
        event.agent.current=null,delete event.agent.current,
        event.agent.resistance=null,delete event.agent.resistance,
        event.agent.voltage=null,delete event.agent.voltage;
        local=null,delete local;
    }
    TNA(local);
    var resize=function(event){
        event.agent.constants.space=[window.innerWidth,window.innerHeight];
        TNA();
    }
    window.addEventListener("resize",resize,true);
})()
