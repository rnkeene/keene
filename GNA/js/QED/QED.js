/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var pixel=function(context){
        var color=context.createImageData(1,1),
        local={
            red:0,
            green:0,
            blue:0,
            trans:255,
            update:function(){
                color.data[0]=local.red,
                color.data[1]=local.green,
                color.data[2]=local.blue,
                color.data[3]=local.trans,
                context.putImageData(color,0,0)
            }
        }
        local.update();
        return local;
    }
    var active=function(x,y){
        var apply=function(){
            document.body.appendChild(canvas);
        }
        var canvas=document.createElement("canvas");
        canvas.style.position="absolute",
        canvas.width=1,
        canvas.height=1;
        var context=canvas.getContext("2d"),
        coord=pixel(context),
        local={
            origin:[x,y],
            left:x,
            top:y,
            context:context,
            point:coord,
            place:function(){
                canvas.style.top=local.top+"px",
                canvas.style.left=local.left+"px"
            },
            reset:function(){
                local.left=local.origin[0],
                local.top=local.origin[1],
                local.place();
            }
        }
        local.place()
        window.addEventListener("load",apply,true)
        return local;
    }
    QED={
        gravity:[],
        pallet:{
            color:{
                black:["0","0","0","FF"],
                white:["FF","FF","FF","FF"],
                red:["FF","0","0","FF"],
                yellow:["FF","FF","0","FF"],
                green:["0","FF","0","FF"],
                blue:["0","0","FF","FF"],
                glow:[["0","FF","FF","FF"],["FF","0","FF","FF"]]
            }
        },
        legend:{
            collision:{
                min:[0,0],
                half:[parseInt(window.innerWidth/2),parseInt(window.innerHeight/2)],
                max:[window.innerWidth,window.innerHeight],
                nw:function(key){
                    key.pixel.left++,
                    key.pixel.top++,
                    key.pixel.place()
                    if(key.pixel.left>QED.legend.collision.max[0] && key.pixel.top>QED.legend.collision.max[1]){
                        key.rule=QED.legend.collision.se
                    }else if(key.pixel.left>QED.legend.collision.max[0]){
                        key.rule=QED.legend.collision.ne
                    }else if(key.pixel.top>QED.legend.collision.max[1]){
                        key.rule=QED.legend.collision.sw
                    }
                },
                ne:function(key){
                    key.pixel.left--,
                    key.pixel.top++;
                    
                    key.pixel.place()
                    if(key.pixel.left<QED.legend.collision.min[0] && key.pixel.top>QED.legend.collision.max[1]){
                        key.rule=QED.legend.collision.sw
                    }else if(key.pixel.left<QED.legend.collision.min[0]){
                        key.rule=QED.legend.collision.nw
                    }else if(key.pixel.top>QED.legend.collision.max[1]){
                        key.rule=QED.legend.collision.se
                    }
                },
                se:function(key){
                    key.pixel.left--,
                    key.pixel.top--;
                    key.pixel.place()
                    if(key.pixel.left<QED.legend.collision.min[0] && key.pixel.top<QED.legend.collision.min[1]){
                        key.rule=QED.legend.collision.nw
                    }else if(key.pixel.left<QED.legend.collision.min[0]){
                        key.rule=QED.legend.collision.sw
                    }else if(key.pixel.top<QED.legend.collision.min[1]){
                        key.rule=QED.legend.collision.ne
                    }
                },
                sw:function(key){
                    key.pixel.left++,
                    key.pixel.top--;
                    key.pixel.place()
                    if(key.pixel.left>QED.legend.collision.max[0] && key.pixel.top<QED.legend.collision.min[1]){
                        key.rule=QED.legend.collision.ne
                    }else if(key.pixel.left>QED.legend.collision.max[0]){
                        key.rule=QED.legend.collision.se
                    }else if(key.pixel.top<QED.legend.collision.min[1]){
                        key.rule=QED.legend.collision.nw
                    }
                },
                n:function(key){
                    key.pixel.top--,
                    key.pixel.place()
                    if(key.pixel.top<QED.legend.collision.min[1]){
                        key.rule=QED.legend.collision.s
                        key.pixel.place()
                    }
                },
                e:function(key){
                    key.pixel.left++,
                    key.pixel.place()
                    if(key.pixel.left>QED.legend.collision.max[0]){
                        key.rule=QED.legend.collision.w
                        key.pixel.place()
                    }
                },
                s:function(key){
                    key.pixel.top++;
                    key.pixel.place()
                    if(key.pixel.top>QED.legend.collision.max[1]){
                        key.rule=QED.legend.collision.n
                        key.pixel.place()
                    }
                },
                w:function(key){
                    key.pixel.left--,
                    key.pixel.place()
                    if(key.pixel.left<QED.legend.collision.min[0]){
                        key.rule=QED.legend.collision.e
                        key.pixel.place()
                    }
                },
                compass:function(){
                    var local={
                        w:{
                            pixel:active(QED.legend.collision.half[0],QED.legend.collision.half[1]),
                            rule:QED.legend.collision.w
                        },
                        n:{
                            pixel:active(QED.legend.collision.half[0],QED.legend.collision.half[1]),
                            rule:QED.legend.collision.n
                        },
                        e:{
                            pixel:active(QED.legend.collision.half[0],QED.legend.collision.half[1]),
                            rule:QED.legend.collision.e
                        },
                        s:{
                            pixel:active(QED.legend.collision.half[0],QED.legend.collision.half[1]),
                            rule:QED.legend.collision.s
                        }
                    }
                    return local;
                },
                rose:function(){
                    var local={
                        nw:{
                            pixel:active(QED.legend.collision.min[0],QED.legend.collision.min[1]),
                            rule:QED.legend.collision.nw
                        },
                        ne:{
                            pixel:active(QED.legend.collision.max[0],QED.legend.collision.min[1]),
                            rule:QED.legend.collision.ne
                        },
                        se:{
                            pixel:active(QED.legend.collision.max[0],QED.legend.collision.max[1]),
                            rule:QED.legend.collision.se
                        },
                        sw:{
                            pixel:active(QED.legend.collision.min[0],QED.legend.collision.max[1]),
                            rule:QED.legend.collision.sw
                        }
                    }
                    return local;
                }
            }
        },
        relative:function(){
            var apply=function(){
                document.body.appendChild(canvas);
            }
            var local=QED.legend.collision.compass();
            console.log(local)
            var canvas=document.createElement("canvas");
            var context=canvas.getContext("2d")
            window.addEventListener("load",apply,true)
            var gravity=function(){
                var hash=[local.n,local.e,local.s,local.w];
                var speed=[];
                var path=function(){
                    var wall=[].concat(hash);
                    while(wall.length){
                        var key=wall.shift()
                        key.rule(key);
                    }
                    plot();
                }
                var plot=function(){
                        canvas.width=window.innerWidth;
                        canvas.height=window.innerHeight;
                        context.moveTo(local.n.pixel.left,local.n.pixel.top);
                        
                        context.beginPath();
                        context.bezierCurveTo(local.e.pixel.left,local.e.pixel.top,local.s.pixel.left,local.s.pixel.top,local.w.pixel.left,local.w.pixel.top)
                        context.bezierCurveTo(local.s.pixel.left,local.s.pixel.top,local.e.pixel.left,local.e.pixel.top,local.w.pixel.left,local.w.pixel.top)
                        context.bezierCurveTo(local.s.pixel.left,local.s.pixel.top,local.w.pixel.left,local.w.pixel.top,local.e.pixel.left,local.e.pixel.top)
                        context.bezierCurveTo(local.w.pixel.left,local.w.pixel.top,local.s.pixel.left,local.s.pixel.top,local.e.pixel.left,local.e.pixel.top)
                        context.stroke();
                }
                
                var faster=function(){
                    speed.unshift(window.setInterval(path,50))
                }
                var slower=function(){
                    window.clearInterval(speed.shift())
                }
                var stop=function(){
                    while(speed.length){
                        slower();
                    }
                }
                return{
                    faster:faster,
                    slower:slower,
                    stop:stop
                }
            }
            QED.gravity.push(gravity());
        },
        state:function(){
            var local=QED.legend.collision.rose(),
            gravity=function(){
                var hash=[local.nw,local.ne,local.se,local.sw];
                var speed=[];
                var path=function(){
                    var wall=[].concat(hash);
                    while(wall.length){
                        var key=wall.shift()
                        key.rule(key);
                    }
                }
                var faster=function(){
                    speed.unshift(window.setInterval(path,50))
                }
                var slower=function(){
                    window.clearInterval(speed.shift())
                }
                var stop=function(){
                    while(speed.length){
                        slower();
                    }
                }
                return{
                    faster:faster,
                    slower:slower,
                    stop:stop
                }
            }
            QED.gravity.push(gravity());
        }
    }
    console.warn('QED == ([x] ==> [y]) //set x requires set y')
})()
