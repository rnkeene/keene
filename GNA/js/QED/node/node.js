/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *            PHOTON
 *            
 *       (a.k.a 4-color Map)
 */
(function(){
    console.info("QED/node/node.js (@x,@y)")
    var pixel=function(context){
        var color=context.createImageData(1,1),
        local={
            red:0,
            green:0,
            blue:0,
            trans:255,
            update:function(){
                console.log("UPDATE!")
                color.data[0]=local.red,
                color.data[1]=local.green,
                color.data[2]=local.blue,
                color.data[3]=local.trans,
                context.putImageData(color,0,0)
            }
        }
        local.update();
        return local;
    }
    
    var active=function(x,y){
        var canvas=document.createElement("canvas");
        canvas.style.position="absolute",
        canvas.width=1,
        canvas.height=1;
        var context=canvas.getContext("2d"),
        coord=pixel(context,x,y),
        local={
            origin:[x,y],
            left:x,
            top:y,
            context:context,
            point:coord,
            place:function(){
                canvas.style.top=local.top+"px",
                canvas.style.left=local.left+"px"
            },
            reset:function(){
                local.left=local.origin[0],
                local.top=local.origin[1],
                local.place();
            }
        }
        local.place(),
        document.body.appendChild(canvas);
        return local;
    }
    QED={
        graph:function(minX,minY,maxX,maxY){
            var min=[minX,minY],max=[maxX,maxY];
            var rulebook={
                nw:function(key){
                    key.pixel.left++,
                    key.pixel.top++,
                    key.pixel.place()
                    if(key.pixel.left>max[0] && key.pixel.top>max[1]){
                        key.rule=rulebook.se
                    }else if(key.pixel.left>max[0]){
                        key.rule=rulebook.ne
                    }else if(key.pixel.top>max[1]){
                        key.rule=rulebook.sw
                    }
                },
                ne:function(key){
                    key.pixel.left--,
                    key.pixel.top++;
                    key.pixel.place()
                    if(key.pixel.left<min[0] && key.pixel.top>max[1]){
                        key.rule=rulebook.sw
                    }else if(key.pixel.left<min[0]){
                        key.rule=rulebook.nw
                    }else if(key.pixel.top>max[1]){
                        key.rule=rulebook.se
                    }
                },
                se:function(key){
                    key.pixel.left--,
                    key.pixel.top--;
                    key.pixel.place()
                    if(key.pixel.left<min[0] && key.pixel.top<min[1]){
                        key.rule=rulebook.nw
                    }else if(key.pixel.left<min[0]){
                        key.rule=rulebook.sw
                    }else if(key.pixel.top<min[1]){
                        key.rule=rulebook.ne
                    }
                },
                sw:function(key){
                    key.pixel.left++,
                    key.pixel.top--;
                    key.pixel.place()
                    if(key.pixel.left>max[0] && key.pixel.top<min[1]){
                        key.rule=rulebook.ne
                    }else if(key.pixel.left>max[0]){
                        key.rule=rulebook.se
                    }else if(key.pixel.top<min[1]){
                        key.rule=rulebook.nw
                    }
                }
            }
            var local={
                nw:{
                    pixel:active(min[0],min[1]),
                    rule:rulebook.nw
                },
                ne:{
                    pixel:active(max[0],min[1]),
                    rule:rulebook.ne
                },
                se:{
                    pixel:active(max[0],max[1]),
                    rule:rulebook.se
                },
                sw:{
                    pixel:active(min[0],max[1]),
                    rule:rulebook.sw
                }
            }
            return local;
        },
        collapse:function(graph){
            var local=graph,
            gravity=function(){
                var hash=[local.nw,local.ne,local.se,local.sw];
                var speed=[];
                var path=function(){
                    var wall=[].concat(hash);
                    while(wall.length){
                        var key=wall.shift()
                        key.rule(key);
                    }
                }
                var faster=function(){
                    speed.unshift(window.setInterval(path,50))
                    console.log(QED.graph(local.nw.pixel.left,local.nw.pixel.top,local.se.pixel.left,local.se.pixel.top));
                    console.log(QED.graph(local.sw.pixel.left,local.sw.pixel.top,local.ne.pixel.left,local.ne.pixel.top))
                    local.nw.pixel.point.green=200;
                    local.nw.pixel.point.red=0;
                    local.nw.pixel.point.blue=200;
                    local.nw.pixel.point.update();
                }
                var slower=function(){
                    window.clearInterval(speed.shift())
                    console.log(QED.graph(local.nw.pixel.left,local.nw.pixel.top,local.se.pixel.left,local.se.pixel.top));
                    console.log(QED.graph(local.sw.pixel.left,local.sw.pixel.top,local.ne.pixel.left,local.ne.pixel.top))
                }
                var stop=function(){
                    while(speed.length){
                        slower();
                    }
                }
                return{
                    faster:faster,
                    slower:slower,
                    stop:stop
                }
            }
            return gravity();
        },
        hash:function(){
            try{
                var graph=QED.graph(1,1,window.innerWidth-1,window.innerHeight-1);
                var collapse=QED.collapse(graph)
                window.addEventListener("keydown", collapse.faster, true)
                window.addEventListener("keyup", collapse.slower, true)
                window.addEventListener("mousedown", collapse.stop, true)
                window.addEventListener("mouseup", collapse.faster, true)
            }catch(e){
                console.log(e)
            }
        }
    }
    
    var display=document.createEvent("Event");
    display.initEvent(Date.now(),true,true);
    document.addEventListener(display.type, QED.hash, true)
    TNA.push(function(){
        document.dispatchEvent(display)
        })
        
    var grow=function(){
        while(DNA.length){
            DNA.shift()();
        }
    }
})()
