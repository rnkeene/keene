/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\  
 *
 */

(function(){
    var body=function(event){
        console.warn("DNA")
        event.invoke();
    },root=function(event){
        console.info("DNA")
        document.body.innerHTML="";
        GNA.thread(limit,process)
    }
    GNA.thread(root,body);
    var child="canvas",parent="div",dimension="2d",
    process=function(event){
        console.warn("DNA - process")
    },limit=function(){
        console.info("DNA - process")
        var clone={
            color:["#000000","#FFFF00"],
            relative:["static","absolute"],
            scale:2,
            flip:function(){
                clone.canvas.style.zIndex=clone.scale
            },
            move:function(){
                clone.canvas.style.top=clone.y+"px",
                clone.canvas.style.left=clone.x+"px"
            },
            up:function(){
                clone.y+=clone.scale
            },
            down:function(){
                clone.y-=clone.scale
            },
            left:function(){
                clone.x+=clone.scale
            },
            right:function(){
                clone.x-=clone.scale
            },
            grow:function(){
                clone.y*=clone.scale,
                clone.x*=clone.scale
            },
            shrink:function(){
                clone.y=clone.y/parseInt(clone.scale),
                clone.x=clone.x/parseInt(clone.scale)
            },
            _origin:function(){
                var top=parseInt(clone.wrapper.offsetHeight/clone.scale),
                bottom=parseInt(clone.wrapper.offsetHeight/clone.scale),
                left=parseInt(clone.wrapper.offsetWidth/clone.scale),
                right=parseInt(clone.wrapper.offsetWidth/clone.scale);
                clone.x=left,
                clone.y=top,
                clone.x2=right,
                clone.y2=bottom;
            },
            _offset:function(){
                var x=parseInt(canvas.offsetWidth/clone.scale),
                y=parseInt(canvas.offsetHeight/clone.scale);
                clone.y+=y,clone.x+=x,clone.y2-=y,clone.x2-=x;
            }
        }
    }
    var start=function(clone){
        clone.canvas=document.createElement(child),
        clone.context=canvas.getContext(dimension),
        clone.wrapper=document.createElement(parent),
        clone.wrapper.style.margin=0,
        clone.wrapper.style.padding=0,
        clone.wrapper.clone.wrapper.style.position=clone.relative[0],
        clone.wrapper.style.backgroundColor=clone.color[0],
        clone.wrapper.style.overflow="hidden",
        clone.wrapper.style.zIndex=1,
        clone.canvas.style.position=clone.relative[1],
        clone.canvas.style.backgroundColor=clone.color[1],
        clone.wrapper.style.width=window.innerWidth+"px",
        clone.wrapper.style.height=window.innerHeight+"px",
        clone.canvas.width=clone.scale,
        clone.canvas.height=clone.scale,
        clone._origin(),
        clone._offset(),
        clone.apply=function(){
            clone.wrapper.appendChild(clone.canvas);
            document.body.appendChild(clone.wrapper);
            clone.apply=null,delete clone.apply;
        }
    }
}())
