(function(){
    var canvas=function(){
        /**
         * New <canvas>
         *      accepts:
         *          Click
         *          Keydown/Keyup
         *          Resize (Redraw)
         *          
         *          Color
         *          Position
         *          Scale
         *          
         *          "History"
         *          "Move/Drag-Drop"
         *          "Z-Index"
         * 
         */
        var base=[20,150],input=[],history=[];
        var alpha=function(event){
            base=[event.clientX,event.clientY];
            history.push(input);
            input=[];
        },beta=function(event){
            if(!event.altKey && !event.ctrlKey){
                RNA.root.context.font="20px Arial";
                RNA.root.context.fillStyle="#FFFF00";
                if(event.keyCode>64 && event.keyCode<91 || event.keyCode==32){
                    var lc=String.fromCharCode(event.keyCode);
                    if(!event.shiftKey){
                        lc=lc.toLowerCase();
                    }
                    input.push(lc);
                    RNA.root.context.fillText(input.join(""),base[0],base[1]);
                }
                if(event.keyCode==49 && event.shiftKey){
                    input.push("!")
                    RNA.root.context.fillText(input.join(""),base[0],base[1]);
                }
                if(event.keyCode==13){
                    base[1]+=30;
                    history.push(input)
                    input=[];
                }
                if(event.keyCode==8){
                    RNA.root.context.fillStyle="#000000";
                    RNA.root.context.fillRect(0,0,window.innerWidth,window.innerHeight)
                    RNA.root.context.fillStyle="#FFFF00";
                    input.pop();
                    var rewrite=[].concat(history);
                    base=[20,150];
                    while(rewrite.length>0){
                        RNA.root.context.fillText(rewrite.shift().join(""),base[0],base[1]);
                        base[1]+=30;
                    }
                    RNA.root.context.fillText(input.join(""),base[0],base[1]);
                }
            }
        },gamma=function(event){
            window.addEventListener("click",alpha,true)
            window.addEventListener("keydown",beta,true)
            window.addEventListener("keyup",gamma,true)
            console.log("Goodbye Press",event)
        }
        var safe=[alpha,beta,gamma],stub={
            alpha:function(event){
                safe.shift()();
                RNA.finished(event,stub.alpha)
            },
            beta:function(event){
                safe.shift()();
                RNA.finished(event,stub.beta)
            },
            gamma:function(event){
                safe.shift()();
                RNA.finished(event,stub.gamma)
            },
            delta:function(event){
                RNA.finished(event,stub.delta)
            }
        }
        RNA.register(stub)
    }
    window.addEventListener("load",canvas,true);
}())