/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\  
 *
 */
(function(){
    var sensor={
        color:"#000000",
        update:function(){
            sensor.local.context.fillStyle=sensor.color;
        },
        slow:function(id){
            window.clearInterval(sensor.running.splice(id));
            console.info(sensor.running)
        },
        start:{
            up:function(){
                return window.setInterval(sensor._up,1000);
            },
            down:function(){
                return window.setInterval(sensor._down,1000);
            },
            left:function(){
                return window.setInterval(sensor._left,1000);
            },
            right:function(){
                return window.setInterval(sensor._right,1000);
            }
        },
        _up:function(){
            sensor.local.up(),
            sensor.local.move();
        },
        _down:function(){
            sensor.local.down(),
            sensor.local.move();
        },
        _left:function(){
            sensor.local.left(),
            sensor.local.move();
        },
        _right:function(){
            sensor.local.right(),
            sensor.local.move();
        }
    }
    var arena=function(event){
        console.warn("DNA.key{}()");
        var going={
            left:true,
            right:true,
            up:true,
            down:true
        },processor={
            left:function(){
                going.left=!going.left;
            },
            right:function(){
                going.right=!going.right;
            },
            up:function(){
                going.up=!going.up;
            },
            down:function(){
                going.down=!going.down;
            }
        }
        var left=function(event){
            
        }
        var right=function(event){
            
        }
        var up=function(event){
            
        }
        var down=function(event){
            
        }
        var iup=function(event){
            console.log("THE Up Observer!")
        }
        var idown=function(event){
            console.log("THE Down Observer!")
        }
        var sensor=function(event){
            console.log("THE SENSOR!",event)
        }
        GNA.thread(sensor,idown);
        GNA.thread(sensor,iup);
        window.setTimeout(event.invoke,25);
        window.setTimeout(event.invoke,100);
        window.setTimeout(event.invoke,400);
        window.setTimeout(event.invoke,1000);
    }
    var player=function(){
        console.info("DNA.arena()");
        var keydown=function(event){
            switch(event.keyCode){
                case 37:
                    DNA.keyDown.trigger()//                    going.left=true
                    break;
                case 38:
                    //                    going.up=true
                    break;
                case 39:
                    //                    going.right=true
                    break;
                case 40:
                    //                    going.down=true
                    break;
            }
        }
        var keyup=function(event){
            switch(event.keyCode){
                case 37:
                    //                    going.left=false
                    break;
                case 38:
                    //                    going.up=false
                    break;
                case 39:
                    //                    going.right=false
                    break;
                case 40:
                    //                    going.down=false
                    break;
            }
        }
        window.addEventListener("keydown",keydown,true)
        window.addEventListener("keyup",keyup,true)
        console.info("DNA.arena()");
    }
    GNA.thread(player,arena);
}())
