(function(){
    var clean=function(){
        var done=function(){
            window["quadrant"]=null,delete window["quadrant"];
            Function("html/arena/legend/risk");
        }
        done(),clean=null,delete clean;
    }
    var append=function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create=function(){
        var CENTER=function(){
            GNA.transistor.coordinates=[0,0],
            GNA.transistor.corner=[0,0],
            GNA.transistor.offset=[0,0],
            GNA.transistor.style=window["style"]["css"]();
        },EFFECT=function(){
            GNA.coordinate();
            Function["function"]["final"](GNA.read([]));
            Function["function"]["final"]["return"]();
        },START=function(){
            GNA.center();
            GNA.transistor.canvas=window["canvas"].create();
            GNA.transistor.context=window["canvas"].context(GNA.transistor.canvas);
            GNA.transistor.canvas.height=0,
            GNA.transistor.canvas.width=0,
            window["element"].add(GNA.transistor.canvas);
            GNA.transistor.start=null,delete GNA.transistor.start;
            GNA.effect();
        },COLOR=function(transistor){
            if(transistor!=null){
                GNA.transistor=transistor;
                GNA.field.context.strokeStyle=["#",GNA.transistor.HRGB.join("")].join("");
            }
            return GNA.field.context.strokeStyle;
        },READ=function(array){
            array.push(GNA.transistor.style.position()),
            array.push(GNA.transistor.style.overflow()),
            array.push(GNA.transistor.style.alignment()),
            array.push(GNA.transistor.style.padding(0)),
            array.push(GNA.transistor.style.margin(0)),
            array.push(GNA.transistor.style.left(GNA.transistor.coordinates[0])),
            array.push(GNA.transistor.style.top(GNA.transistor.coordinates[1])),
            array.push(GNA.transistor.index());
            window["style"].update(GNA.transistor.canvas.className,array);
        },OPTION=["alpha","beta","gamma","delta"],INTERCEPT=function(){
            var index = (Math.floor((Math.random()*100)+1)%4);
            switch(Date.now()%5){
                case 0:
                    GNA[OPTION[index]].HRGB=["FF","FF","FF"];
                    break;
                case 1:
                    GNA[OPTION[index]].HRGB=["00","FF","00"];
                    break;
                case 2:
                    GNA[OPTION[index]].HRGB=["00","00","FF"];
                    break;
                case 3:
                    GNA[OPTION[index]].HRGB=["FF","FF","00"];
                    break;
                case 4:
                    GNA[OPTION[index]].HRGB=["FF","00","00"];
                    break;
            }
            GNA.field.path(GNA.resistor,GNA.alpha,GNA.color(GNA.alpha));
            GNA.field.path(GNA.resistor,GNA.beta,GNA.color(GNA.beta));
            GNA.field.path(GNA.resistor,GNA.gamma,GNA.color(GNA.gamma));
            GNA.field.path(GNA.resistor,GNA.delta,GNA.color(GNA.delta));
        },TRAVERSE=function(){
            GNA.field.path(GNA.alpha,GNA.beta,GNA.color(GNA.alpha)),
            GNA.field.path(GNA.beta,GNA.gamma,GNA.color(GNA.beta)),
            GNA.field.path(GNA.gamma,GNA.delta,GNA.color(GNA.gamma)),
            GNA.field.path(GNA.delta,GNA.alpha,GNA.color(GNA.delta))
        },init=function(){
            console.log("Initializing Quadrant");
            GNA.quadrant=function(){
                GNA.style=window["style"]["css"](),
                GNA.center=CENTER,
                GNA.effect=EFFECT,
                GNA.start=START,
                GNA.color=COLOR,
                GNA.intercept=INTERCEPT,
                GNA.traverse=TRAVERSE,
                GNA.read=READ
            }
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["quadrant"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature=window["quadrant"]();
    feature.calculi("quadrant","basic");
    Function["function"]["final"]["return"]();
}())