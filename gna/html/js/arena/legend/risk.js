(function(){
    var clean=function(){
        var done=function(){
            window["risk"]=null,delete window["risk"];
            Function("html/arena/legend/flyweight");
        }
        done(),clean=null,delete clean;
    }
    var append=function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create=function(){
        var init=function(){
            GNA.reality=function(){
                /* True order of space at Time 'stamped' */
                return {
                    relative:GNA.resistor,
                    stamped:Date.now(),
                    LTR:{
                        alpha:[GNA.alpha,GNA.beta,GNA.gamma,GNA.delta],
                        beta:[GNA.beta,GNA.gamma,GNA.delta,GNA.alpha],
                        gamma:[GNA.gamma,GNA.delta,GNA.alpha,GNA.beta],
                        delta:[GNA.delta,GNA.alpha,GNA.beta,GNA.gamma]
                    },
                    RTL:{
                        alpha:[GNA.alpha,GNA.delta,GNA.gamma,GNA.beta],
                        beta:[GNA.beta,GNA.alpha,GNA.delta,GNA.gamma],
                        gamma:[GNA.gamma,GNA.beta,GNA.alpha,GNA.delta],
                        delta:[GNA.delta,GNA.gamma,GNA.beta,GNA.alpha]
                    },
                    suppliment:{
                        horizontal:[[GNA.alpha,GNA.beta],[GNA.delta,GNA.gamma]],
                        vertical:[[GNA.alpha,GNA.delta],[GNA.beta,GNA.gamma]]
                    },
                    compliment:{
                        deltaBeta:[[GNA.delta,GNA.alpha,GNA.beta],[GNA.delta,GNA.gamma,GNA.beta]],
                        alphaGamma:[[GNA.alpha,GNA.delta,GNA.gamma],[GNA.alpha,GNA.beta,GNA.gamma]]
                    },
                    slope:{
                        negative:[GNA.alpha,GNA.gamma],
                        positive:[GNA.delta,GNA.beta]
                    }
                }
            }
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["risk"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature=window["risk"]();
    feature.calculi("risk","basic");
    Function["function"]["final"]["return"]();
}())