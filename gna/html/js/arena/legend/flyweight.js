(function(){
    var clean=function(){
        var done=function(){
            window["flyweight"]=null,delete window["flyweight"];
            Function("html/arena/collider/radix");
        }
        done(),clean=null,delete clean;
    }
    var append=function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create=function(){
        var init=function(){
            GNA.cluster=function(){
                return {
                    relative:GNA.resistor,
                    stamped:Date.now(),
                    regular:{
                        /*
                         *  alpha triangle
                         *      - beta to alpha line
                         *      - delta to alpha line
                         *      - gamma to alpha line
                         */
                        alpha:function(){},
                        /*
                         *  beta triangle
                         *      - alpha to beta line
                         *      - gamma to beta line
                         *      - delta to beta line
                         */
                        beta:function(){},
                        /*
                         *  gamma triangle
                         *      - delta to gamma line
                         *      - beta to gamma line
                         *      - alpha to gamma line
                         */
                        gamma:function(){},
                        /*
                         *  delta triangle
                         *      - gamma to delta line
                         *      - alpha to delta line
                         *      - beta to delta line
                         */
                        delta:function(){}
                    },
                    geometric:{
                        /* (line alpha to gamma) perpendicular (line delta to beta)*/
                        intersection:function(){},
                        /* 
                         * (line gamma to beta) perpendicular (line gamma to delta)
                         *          both parallel with
                         * (line alpha to beta) perpendicular (line alpha to delta)
                         */
                        quadrilateral:function(){}
                    }
                }
                
            }
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["flyweight"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature=window["flyweight"]();
    feature.calculi("flyweight","basic");
    Function["function"]["final"]["return"]();
}())