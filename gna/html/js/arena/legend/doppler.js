(function(){
    var clean=function(){
        var done=function(){
            window["doppler"]=null,delete window["doppler"];
            Function("html/arena/legend/quadrant");
        }
        done(),clean=null,delete clean;
    }
    var append=function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    
    var create=function(){
        var ROOT=function(variable){
            var constant=variable,path=[],token=[],symbol={
                base:[],
                offset:[],
                unknown:[],
                feedback:[]
            }
            var crypt={
                origin:Date.now(),
                map:function(){
                    return path
                },
                compass:function(){
                    return token
                },
                crypt:function(){
                    return symbol
                },
                ms:function(){
                    constant-=1
                },
                ps:function(){
                    constant+=1
                },
                base:function(){
                    constant/=2
                },
                offset:function(){
                    crypt.base(),crypt.base()
                },
                unknown:function(){
                    crypt.ms(),crypt.offset()
                },
                feedback:function(){
                    crypt.ps(),crypt.offset()
                },
                delta:function(){
                    while(constant>1){
                        crypt.compass().push(constant)
                        switch(constant.toString(4).split('').pop()){
                            case "0":
                                crypt.map().push("gamma")
                                symbol.offset.push(constant)
                                crypt.offset()
                                break
                            case "2":
                                crypt.map().push("resistor")
                                symbol.base.push(constant)
                                crypt.base()
                                break
                            default:
                                if((constant-1).toString(4).split('').pop()==0){
                                    crypt.map().push("delta")
                                    symbol.unknown.push(constant)
                                    crypt.unknown()
                                    break
                                }
                                crypt.map().push("beta")
                                symbol.feedback.push(constant)
                                crypt.feedback()
                        }
                    }
                    crypt.map().push("alpha");
                }
            }
            return crypt;
        },
        init=function(){
            var local = Date.now(),linear = 299792458,exponential = 299792458*299792458,
            GNA.doppler=ROOT;
            var time = GNA.light(local);
            time.delta()
            console.log("MAP",time.map());
            console.log("COMPASS",time.compass());
            console.log("CRYPT",time.crypt());
            var line = GNA.light(linear);
            line.delta()
            console.log("MAP",line.map());
            console.log("COMPASS",line.compass());
            console.log("CRYPT",line.crypt());
            var pert = GNA.light(exponential);
            pert.delta()
            console.log("MAP",pert.map());
            console.log("COMPASS",pert.compass());
            console.log("CRYPT",pert.crypt());
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["doppler"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature=window["doppler"]();
    feature.calculi("doppler","basic");
    Function["function"]["final"]["return"]();
}())