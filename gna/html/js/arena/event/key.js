(function(){
    var clean=function(){
        var done = function(){
            window["direction"]=null,delete window["direction"];
            Function("html/arena/event/size");
        }
        done(),clean=null,delete clean;
    }
    var append = function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create = function(){
        var QUICK = function(effect,code,real){
            var local=[effect,code,real];
            var impl = function(){
                local[0]();
                if(local[2].interval[local[1]].length<10){
                    GNA.intercept()
                    real.canvas.width+=local[2].interval[local[1]].length
                    real.canvas.height+=local[2].interval[local[1]].length
                    real.context.fillStyle="#FFFFFF";
                    real.context.fillRect(0,0,real.canvas.width,real.canvas.height);
                }else{
                    if(local[2].interval[local[1]].length%10==0){
                        GNA.intercept()
                        GNA.resistor.canvas.width-=10
                        GNA.resistor.canvas.height-=10
                        real.context.fillStyle="#FFFFFF";
                        real.context.fillRect(0,0,real.canvas.width,real.canvas.height);
                        local[2].interval[code].push(window.setInterval(impl,(local[2].interval[local[1]].length)))
                    }
                }
            }
            var timer = (real.interval[code].length);
            return window.setInterval(impl,(timer*timer+100))
        },STOP = function(id){
            window.clearInterval(id)
        },KEYDOWN={
            observers:[],
            key:"press"
        },KEYUP={
            key:"impress"
        },OPP=[37,38,39,40],STABLE=function(code,real){
            if(real.interval[code]==null || real.interval[code]==undefined){
                real.interval[code]=[];
            }
            var local=real;
            var effect;
            switch(code){
                case 38:
                    /*up*/
                    effect=function(){
                        GNA.resistor.coordinates[1]+=1;
                        GNA.resistor.canvas.height+=1
                    }
                    break;
                case 37:
                    /*left*/
                    effect=function(){
                        GNA.resistor.coordinates[0]+=1;
                        GNA.resistor.canvas.width+=1
                    }
                    break;
                case 39:
                    /*right*/
                    effect=function(){
                        GNA.resistor.coordinates[0]-=1;
                        GNA.resistor.canvas.width-=1
                    }
                    break;
                case 40:
                    /*down*/
                    effect=function(){
                        GNA.resistor.coordinates[1]-=1;
                        GNA.resistor.canvas.height-=1
                    }
                    break;
            }
            real.interval[code].push(QUICK(effect,code,real))
        }
        var init = function(){
            KEYUP.trigger=GNA.event.custom(KEYUP.key);
            KEYUP.trigger.detail.event=function(event){
                if(event){
                    var code = event.keyCode;
                    if(event.keyCode>36 && event.keyCode<41){
                        KEYUP.trigger.detail.history=[];
                        while(KEYDOWN.observers.length>0){
                            var real = KEYDOWN.observers.shift();
                            while(real.interval[code].length>0){
                                real.canvas.width=0
                                real.canvas.height=0
                                real.context.fillStyle="#FFFFFF";
                                real.context.fillRect(0,0,real.canvas.width,real.canvas.height);
                                STOP(real.interval[code].pop())
                            }
                            KEYUP.trigger.detail.history.push(real)
                        }
                        KEYDOWN.observers = KEYUP.trigger.detail.history.concat(KEYDOWN.observers);
                    }
                }
            }
            GNA.event.global(GNA.event.key.up,KEYUP.trigger.detail.event)
            KEYDOWN.trigger=GNA.event.custom(KEYDOWN.key);
            KEYDOWN.trigger.detail.event=function(event){
                if(event){
                    var code = event.keyCode;
                    if(event.keyCode>36 && event.keyCode<41){
                        KEYDOWN.trigger.detail.history=[];
                        while(KEYDOWN.observers.length>0){
                            var real = KEYDOWN.observers.shift();
                            STABLE(code,real);
                            KEYDOWN.trigger.detail.history.push(real);
                        }
                        KEYDOWN.observers = KEYDOWN.trigger.detail.history.concat(KEYDOWN.observers);
                    }
                }
            }
            GNA.event.global(GNA.event.key.down,KEYDOWN.trigger.detail.event)
            GNA.event.register[GNA.event.key.press]=function(){
                KEYDOWN.observers.push(GNA.transistor);
                GNA.transistor.interval={};
            }
            GNA.key=GNA.event.register[GNA.event.key.press];
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["direction"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature = window["direction"]();
    feature.calculi("direction","basic");
    Function["function"]["final"]["return"]();
}())