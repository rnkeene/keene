(function(){
    var clean=function(){
        var done = function(){
            window["scale"]=null,delete window["scale"];
            Function("html/arena/legend/doppler");
        }
        done(),clean=null,delete clean;
    }
    var append = function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create = function(){
        var RESIZE={
            observers:[],
            key:"resize"
        }
        var init = function(){
            RESIZE.trigger=GNA.event.custom(RESIZE.key);
            RESIZE.trigger.detail.event=function(){
                RESIZE.trigger.detail.history=[];
                while(RESIZE.observers.length>0){
                    var real = RESIZE.observers.shift();
                    real[RESIZE.key]()
                    RESIZE.trigger.detail.history.push(real)
                }
                RESIZE.observers = RESIZE.trigger.detail.history.concat(RESIZE.observers);
            }
            GNA.event.global(RESIZE.key,RESIZE.trigger.detail.event);
            RESIZE.trigger=GNA.event.custom(RESIZE.key);
            GNA.event.register[RESIZE.key]=function(){
                RESIZE.observers.push(GNA.transistor);
            }
            GNA.scalar=GNA.event.register[RESIZE.key];
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["scale"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature = window["scale"]();
    feature.calculi("scale","basic");
    Function["function"]["final"]["return"]();
}())