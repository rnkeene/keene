(function(){
    var clean=function(){
        var done=function(){
            window["action"]=null,delete window["action"];
            Function("html/arena/event/exceptional");
        }
        done(),clean=null,delete clean;
    }
    var append=function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create=function(){
        var LOAD=["load",0],QUIT=function(object,name,action){
            object.removeEventListener(name,action,false);
        },START=function(object,name,reaction,when){
            var event=CUSTOM(name);
            event.detail.title=name;
            event.calculi=object;
            event.reaction=reaction;
            document.addEventListener(name,event.reaction,when);
            return function(){
                event.reaction();
                document.dispatchEvent(event);
            }
        }
        ,KEY={
            up:"keyup",
            down:"keydown",
            press:"press"
        },CUSTOM=function(name){
            return new CustomEvent(name,{
                detail: {
                    time: Date.now(),
                    /*cache: function(event){
                        GNA.action=event;
                        GNA.whole=function(element){
                            var temp=element;
                            return function(){
                                GNA.transistor.top=temp.clientTop,
                                GNA.transistor.left=temp.clientLeft,
                                GNA.transistor.width=temp.clientWidth,
                                GNA.transistor.height=temp.clientHeight;
                                temp=null,delete temp;
                            }
                        },
                        GNA.structure=function(html){
                            var temp=html;
                            return function(){
                                GNA.transistor.whole=[temp.offsetWidth,temp.offsetHeight],
                                GNA.transistor.offset=[temp.offsetLeft,temp.offsetTop];
                                temp=null,delete temp;
                            }
                        },
                        GNA.evidence=function(event){
                            var local=event;
                            return function(){
                                GNA.transistor.compass=[local.clientX,local.clientY],
                                GNA.transistor.atlas=[local.pageX,local.pageY],
                                GNA.transistor.coords=[local.screenX,local.screenY],
                                GNA.transistor.xy=[local.x,local.y],
                                GNA.transistor.offset=[local.offsetX,local.offsetY];
                                local=null,delete local;
                            }
                        },
                        GNA.screen=function(){
                            var local={
                                screen:window.screen,
                                where:[window.screen.width,window.screen.height],
                                whole:[window.screen.availWidth,window.screen.availHeight],
                                density:[window.screen.colorDepth,window.screen.pixelDepth]
                            }
                            return function(){
                                GNA.transistor.screen=local,
                                GNA.transistor.screen.where=local.where,
                                GNA.transistor.screen.whole=local.whole,
                                GNA.transistor.screen.density=local.density;
                                local=null,delete local;
                            }
                        },
                        GNA.client=function(){
                            var local={
                                total:[window.screenX,window.screenY],
                                outside:[window.outerWidth,window.outerHeight]
                            }
                            return function(){
                                GNA.transistor.globe=local.total,
                                GNA.transistor.space=local.outside;
                                local=null,delete local;
                            }
                        }
                    },*/
                    bubbles: true,
                    cancelable: true
                }
            });
        }
        var init=function(){
            GNA.event={
                key:KEY,
                load:LOAD,
                listen:START,
                quit:QUIT,
                custom:CUSTOM,
                global:function(name,reaction,when){
                    window.addEventListener(name,GNA.event.listen(GNA.event,name,reaction,when),when);
                },
                register:{}
            }
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["action"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature=window["action"]();
    feature.calculi("action","basic");
    Function["function"]["final"]["return"]();
    clean=null,delete clean,append=null,delete append,create=null,delete create;
}())