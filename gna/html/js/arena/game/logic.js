(function(){
    var clean=function(){
        var done = function(){
            window["logic"]=null,delete window["logic"];
            Function("html/arena/game/natural");
        }
        done(),clean=null,delete clean;
    }
    var append = function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create = function(){
        var init = function(){
            console.log("Initializing Logic");
        /**
        *
        *  GLOBAL:Self-Evident{
        *      A: function(){
        *          [a-a=0] and [0-a=-a]
        *      }
        *      B: function(){
        *          [b-b=0] and [0-b=-b]
        *      }
        *      C:  function(){
        *          [c-c=0] and [0-c=-c]
        *      }
        *  }
        *      
        *  GLOBAL:PROOF of Self-Evident{
        *      APPLY: function(){
        *          alpha.push(window.setInterval(A,0))
        *      }
        *      TRUST: function(){ 
        *          alpha.push(window.setInterval(A.length==2,0)
        *      }
        *      VERIFY: function(){ 
        *          <-- APPLY + TRUST-->
        *          window.removeInterval(alpha.pop())
        *          A==exists
        *          window.removeInterval(alpha.pop())
        *          A!=exists
        *          <-- ASSERTION TRUE -->
        *      }
        *  }
        *      
        *          window.setInterval(exists, 0)
        *          [a=window.removeInterval(id["a"])]=
        *      [b=window.removeInterval(id["b"])]= "no b"
        *      [c=window.removeInterval(id["c"])]= "no c"
        *
        *  PROOF of "b" THEREFOR "a" (because lemma [-b]=[-a]
        *  [a-b=-1]
        *  [a*a=b-a] (i)
        *  [a*a=a^a] (2i)
        *  [a*a*a*a=1] (i^2)
        *  
        *  PROOF of "c" therefor "b" (therefor "a")
        *  
        *  [b*a=a+a] (2)
        *  [a+a+a+a=4a] (2i)
        *  [4a=c^2] ((2i)^2)
        *  [b*b=b^2]   ((2i)^(2i))
        *  
        *  [b+b+b+b=c^2] (8)
        *  
        *  
        *  [a^2+b^2+c^2+d^2]=e^2
        *  
        *  4n+|-1=e
        *  
        *  Base Pythagorean :: [{a*b = (2*2)(n^-1)} FROM a TO b]
        *      Therefor ::: a*a(a^n) + b*b(b^n) = n*n(n^n)
        *      
        *  3*3(a^n)+4*4(b^n) = 5(n^n)
        *  
        *  10*5=50
        *  50*2=100
        *
        */
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["logic"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature = window["logic"]();
    feature.calculi("logic","basic");
    Function["function"]["final"]["return"]();
    clean=null,delete clean,append=null,delete append,create=null,delete create;
}())