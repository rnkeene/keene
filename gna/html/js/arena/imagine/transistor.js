(function(){
    var clean=function(){
        var done=function(){
            window["transistor"]=null,delete window["transistor"],
            Function("html/arena/imagine/resistor")
        }
        done(),clean=null,delete clean
    }
    var append=function(){
        Function["function"]["final"]["return"](),append=null,delete append
    }
    var FAMILY=["transistor",0]
    var create=function(){
        var FRACTAL=function(){
            GNA.transistor.undo=[GNA.transistor.coordinates[0],GNA.transistor.coordinates[1]];
            switch(GNA.current()){
                case 1:
                    GNA.transistor.index=function(){
                        return GNA.transistor.style.zindex(5)
                    },
                    GNA.transistor.coordinates[0]=parseInt(GNA.field.canvas.width/2),
                    GNA.transistor.coordinates[1]=parseInt(GNA.field.canvas.height/2),
                    /*WHITE*/
                    GNA.transistor.HRGB=["FF","FF","FF"]
                    GNA.transistor.key=GNA.key(),
                    GNA.transistor.scalar=GNA.scalar()
                    break
                case 2:
                    GNA.transistor.index=function(){
                        return GNA.transistor.style.zindex(1)
                    },
                    /*RED*/
                    GNA.transistor.HRGB=["FF","00","00"]
                    break
                case 3:
                    GNA.transistor.index=function(){
                        return GNA.transistor.style.zindex(2)
                    },
                    GNA.transistor.coordinates[0]=parseInt(window.innerWidth),
                    /*GREEN*/
                    GNA.transistor.HRGB=["00","FF","00"]
                    break
                case 4:
                    GNA.transistor.index=function(){
                        return GNA.transistor.style.zindex(3)
                    },
                    GNA.transistor.coordinates[0]=window.innerWidth,
                    GNA.transistor.coordinates[1]=window.innerHeight,
                    
                    /*BLUE*/
                    GNA.transistor.HRGB=["00","00","FF"]
                    break
                case 0:
                    GNA.transistor.index=function(){
                        return GNA.transistor.style.zindex(4)
                    },
                    GNA.transistor.coordinates[1]=parseInt(window.innerHeight),
                    /*YELLOW*/
                    GNA.transistor.HRGB=["FF","FF","00"]
                    break;
            }
        },CURRENT=function(){
            GNA.volume++,
            GNA.transistor.order=GNA.volume;
            var name=FAMILY;
            name[1]=GNA.volume;
            name=name.join(""),
            GNA.transistor.family=function(){
                var local=name;
                GNA.transistor.family=function(){
                    return local
                }
                return local
            }
            GNA.transistor.canvas.className=GNA.transistor.family();
            return GNA.volume%5;
        },
        init=function(){
            console.log("Initialized Current, Fractal, and Circuit");
            GNA.volume=0,
            GNA.current=CURRENT,
            GNA.coordinate=FRACTAL
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["transistor"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real)
    }
    var feature=window["transistor"]();
    feature.calculi("transistor","basic"),
    Function["function"]["final"]["return"](),
    clean=null,delete clean,append=null,delete append,create=null,delete create;
}())
/*
 * 
 * old background-color call:
 * style.push(GNA.transistor.style.background(GNA.transistor.HRGB.join(""))),
 * 
 */