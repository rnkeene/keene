(function(){
    var clean=function(){
        var done = function(){
            window["magnetic"]=null,delete window["magnetic"];
            Function("html/arena/imagine/electro");
        }
        done(),clean=null,delete clean;
    }
    var append = function(){
        Function["function"]["final"]["return"](),append=null,delete append;
    }
    var create = function(){
        var init = function(){
            console.log("Initializing Field");
            GNA.field={
                style:window["style"]["css"](),
                size:function(){
                    GNA.field.canvas.width =window.innerWidth;
                    GNA.field.canvas.height = window.innerHeight;
                },
                arena:function(){
                    GNA.field.size();
                    GNA.field.context.strokeStyle = '#ffffff';
                    GNA.field.context.lineWidth = 1;
                    GNA.field.context.fillStyle="#000000";
                    GNA.field.context.fillRect(0,0,GNA.field.style.innerWidth(),GNA.field.style.innerHeight());
                    GNA.field.css();
                },
                start:function(){
                    GNA.field.canvas = window["canvas"].create();
                    window["element"].add(GNA.field.canvas);
                    GNA.field.canvas.className = "magnetic"
                    GNA.field.context = window["canvas"].context(GNA.field.canvas);
                    GNA.field.arena();
                    GNA.event.global("resize", GNA.field.arena, true);
                    GNA.field.start=null,delete GNA.field.start;
                },
                css:function(){
                    var style = [];
                    style.push(GNA.field.style.stay());
                    style.push(GNA.field.style.overflow());
                    style.push(GNA.field.style.alignment());
                    style.push(GNA.field.style.padding(0)),
                    style.push(GNA.field.style.margin(0)),
                    style.push(GNA.field.style.background("000000"));
                    style.push(GNA.field.style.left(0));
                    style.push(GNA.field.style.top(0));
                    style.push(GNA.field.style.zindex(0));
                    window["style"].update("body",style)
                },
                path:function(from,to,color){
                    GNA.field.context.strokeStyle=color,
                    GNA.field.context.beginPath(),
                    GNA.field.context.moveTo(from.coordinates[0],from.coordinates[1]),
                    GNA.field.context.lineTo(to.coordinates[0],to.coordinates[1]),
                    GNA.field.context.stroke()
                }
            }
        }
        Function["function"]["final"](init),create=null,delete create;
    }
    window["magnetic"]=function(){
        var real=[clean,append,create];
        return Function["function"]["final"]["return"]["void"](real);
    }
    var feature = window["magnetic"]();
    feature.calculi("magnetic","basic");
    Function["function"]["final"]["return"]();
    clean=null,delete clean,append=null,delete append,create=null,delete create;
}())