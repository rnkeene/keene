/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *      Local Document Event
 *      
 *      Concurrent Command Heirarchy
 *          [ Capotolism ]
 *    
 *  
 *  Determine Value/Efficiency
 *      
 *  Speed of Light Probability:
 *      Use the 'speed of light' as a consistent pattern:
 *      
 *      Event to find "odds" of "speed of light" conversion:
 *      
 *      Alpha, Beta, Gamma, Delta
 *          
 *          to measure the 'integral' of random functions and their related 'signature' patterns.
 *              window@interval "Speed of Light".
 *      
 *  HISTORY:
 *      "Build Requests"
 *      Measurements:
 *          Synchronous vs Asynchronous
 *          Total Time to Execute
 *          Reported Exceptions (null pointer; etc)
 */
(function(){
    var id=0;
    //self-call; along with GNA.GNA WILL invoke
    RNA.closure=function(event){
        try{
            while(!DNA.random){
                id++; 
                if (id>1000){
                    return;
                };
            }
            GNA.random=DNA.random(GNA);
            GNA=event;
        }catch(e){
            id--;
            if(id<(-10)){
                RNA.closure=GNA.closure()
            }
        }
        var init=function(morph){
            GNA.init(morph);
        },
        clone={
            plant:function(){
                console.group("RNA :: RNA ** RNA :: DNADNAPLANT PLANT PLANT PLANT PLANT !!(@i):%o",counter++,Date.now());
                GNA={
                    now:Date.now(),
                    wait:function(offset){
                        var temp=offset,
                        wait=function(){
                            return temp;
                        }
                        return 
                    },
                    time:function(){
                        return Date.now()
                    },
                    _context:"Event",
                    event:"event",
                    couple:"couple",
                    decouple:"decouple",
                    trigger:"trigger",
                    destroy:"destroy"
                }
            },
            tree:function(){
                console.group("RNA :: RNA ** RNA :: DNADNATREE TREE TREE TREE TREE TREE TREE TREE TREE !!(@i):%o",counter++,Date.now());
                GNA={
                    _worker:function(feature){
                        if(!GNA[GNA.context]){
                            declare(GNA.context)
                        }
                        return GNA[GNA.context](feature);
                    }
                }
            },
            child:function(){
                console.group("RNA :: RNA ** RNA :: DNADNACHILD CHILD CHILD CHILD CHILD CHILD!!(@i):%o",counter++,Date.now());
                GNA={
                    thread:function(feature,when){
                        console.group("RNA :: RNA ** RNA :: DNADNAGNA THREAD!!(@i):%o",counter++,Date.now());
                        var concurrent={
                            callback:when,
                            ref:feature,
                            handle:function(event){
                                concurrent.handle=function(event){
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE HANDLE HANDLE HANDLE HANDLE HANDLE HANDLE!!(@i):%o",counter++,Date.now());
                                    event.decouple(concurrent.handle);
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                    event.decouple(concurrent.morph);
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                    concurrent.morph=null,delete concurrent.morph;
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                    concurrent.morph=null,delete concurrent.morph;
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                    concurrent.callback=null,delete concurrent.callback;
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                    concurrent.ref=null,delete concurrent.ref;
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                    console.warn("passthrough")
                                    console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                }
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                event.invoke=GNA[event.type].trigger,
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                event.couple=GNA[event.type].couple,
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                event.decouple=GNA[event.type].decouple,
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                event.abort=GNA[event.type].abort,
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                event.couple(concurrent.ref);
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE(@i):%o",counter++,Date.now());
                                event.callback=concurrent.callback;
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE!!(@i):%o",counter++,Date.now());
                                concurrent.callback(event);
                                console.group("RNA :: RNA ** RNA :: DNADNAHANDLE!!(@i):%o",counter++,Date.now());
                            },
                            start:function(){
                                console.group("RNA :: RNA ** RNA :: DNADNASTART START START START START START START!!(@i):%o",counter++,Date.now());
                                if(document.body==null){
                                    console.group("RNA :: RNA ** RNA :: DNADNASTART!!(@i):%o",counter++,Date.now());
                                    window.addEventListener("load",function(){
                                        init(concurrent.morph)
                                    },true)
                                    console.group("RNA :: RNA ** RNA :: DNADNASTART!!(@i):%o",counter++,Date.now());
                                }else{
                                    console.group("RNA :: RNA ** RNA :: DNADNASTART!!(@i):%o",counter++,Date.now());
                                    init(concurrent.morph)
                                    console.group("RNA :: RNA ** RNA :: DNADNASTART!!(@i):%o",counter++,Date.now());
                                }
                                console.group("RNA :: RNA ** RNA :: DNADNASTART!!(@i):%o",counter++,Date.now());
                                concurrent.start=null,delete concurrent.start;
                                console.group("RNA :: RNA ** RNA :: DNADNASTART!!(@i):%o",counter++,Date.now());
                            },
                            morph:function(event){
                                console.group("RNA :: RNA ** RNA :: DNADNAMORPH MORPH MORPH MORPH MORPH MORPH MORPH MORPH MORPH!!(@i):%o",counter++,Date.now());
                                window.setTimeout(concurrent.handle,0,event);
                                console.group("RNA :: RNA ** RNA :: DNADNAMORPH MORPH!!(@i):%o",counter++,Date.now());
                            }
                        }
                        console.group("RNA :: RNA ** RNA :: DNADNAROOT TREE SEED!!(@i):%o",counter++,Date.now());
                        concurrent.start();
                    }
                }
                /*some 'nexted' functions here to work with child 'events' in GNA.*/
            }
        }
        return clone;
    }
    GNA.clone().
    console.warn("RNA :: RNA ** RNA :: DNADNAPROTON PROTON PROTON(@i):%o",GNA());
    /*
    var collect=function(event){
        console.groupCollapsed("PROBABILITY of ",event)
        console.groupCollapsed("ALPHA")
        console.info("RNAROOT",event.alpha)
        console.info("RNAOFFSET",event.alpha.offset)
        console.info("RNATOTAL",event.alpha.total)
        console.info("RNAFLUX",event.alpha.flux)
        console.info("RNAAVERAGE",event.alpha.average)
        console.info("RNALEFTOVER",event.alpha.leftover)
        console.groupEnd()
        console.groupCollapsed("BETA")
        console.info("RNAROOT",event.beta)
        console.info("RNAOFFSET",event.beta.offset)
        console.info("RNATOTAL",event.beta.total)
        console.info("RNAFLUX",event.beta.flux)
        console.info("RNAAVERAGE",event.beta.average)
        console.info("RNALEFTOVER",event.beta.leftover)
        console.groupEnd()
        console.groupCollapsed("GAMMA")
        console.info("RNAROOT",event.gamma)
        console.info("RNAOFFSET",event.gamma.offset)
        console.info("RNATOTAL",event.gamma.total)
        console.info("RNAFLUX",event.gamma.flux)
        console.info("RNAAVERAGE",event.gamma.average)
        console.info("RNALEFTOVER",event.gamma.leftover)
        console.groupEnd()
        console.groupCollapsed("DELTA")
        console.info("RNAROOT",event.delta)
        console.info("RNAOFFSET",event.delta.offset)
        console.info("RNATOTAL",event.delta.total)
        console.info("RNAFLUX",event.delta.flux)
        console.info("RNAAVERAGE",event.delta.average)
        console.info("RNALEFTOVER",event.delta.leftover)
        console.groupEnd()
        console.groupEnd()
    }
    var eventful=function(){
        RNA()
        var logger={
            alpha:function(event){
                var timer=Date.now();
                event.alpha.push(timer)
                while(timer==Date.now()){
                    event.alpha.flux++;
                }
                var offset=timer-event.alpha.pop()
                event.alpha.offset.push(offset)
                event.alpha.total+=offset;
                event.alpha.average=parseInt(event.alpha.total/event.alpha.offset.length);
                event.alpha.leftover=parseInt(event.alpha.flux/event.alpha.offset.length);
            },
            beta:function(event){
                var timer=Date.now();
                event.beta.push(timer)
                while(timer==Date.now()){
                    event.beta.flux++;
                }
                var offset=timer-event.beta.pop()
                event.beta.offset.push(offset)
                event.beta.total+=offset;
                event.beta.average=parseInt(event.beta.total/event.beta.offset.length);
                event.beta.leftover=parseInt(event.beta.flux/event.beta.offset.length);
            },
            gamma:function(event){
                var timer=Date.now();
                event.gamma.push(timer)
                while(timer==Date.now()){
                    event.gamma.flux++;
                }
                var offset=timer-event.gamma.pop()
                event.gamma.offset.push(offset)
                event.gamma.total+=offset;
                event.gamma.average=parseInt(event.gamma.total/event.gamma.offset.length);
                event.gamma.leftover=parseInt(event.beta.flux/event.gamma.offset.length);
            },
            delta:function(event){
                var timer=Date.now();
                event.delta.push(timer)
                while(timer==Date.now()){
                    event.delta.flux++;
                }
                var offset=timer-event.delta.pop()
                event.delta.offset.push(offset)
                event.delta.total+=offset;
                event.delta.average=parseInt(event.delta.total/event.delta.offset.length);
                event.delta.leftover=parseInt(event.beta.flux/event.delta.offset.length);
            },
            dispatch:function(){
                if(RNA.keys.length<4){
                    logger.attach();
                }
                var wall=[].concat(RNA.keys)
                while(wall.length>0){
                    var local=wall.shift()[0];
                    var when=Date.now();
                    local.applied.push(when);
                    local.alpha.push(when);
                    local.beta.push(when);
                    local.gamma.push(when);
                    local.delta.push(when);
                    document.dispatchEvent(local)
                }
            },
            register:function(stub){
                var wall=[].concat(RNA.keys)
                while(wall.length>0){
                    var event=wall.shift()[0];
                    var when=Date.now();
                    event.alpha.push(when);
                    event.beta.push(when);
                    event.gamma.push(when);
                    event.delta.push(when);
                    document.addEventListener(event.type,stub.alpha)
                    document.addEventListener(event.type,stub.beta)
                    document.addEventListener(event.type,stub.gamma)
                    document.addEventListener(event.type,stub.delta)
                }  
            },
            close:function(event,local){
                document.removeEventListener(event,local,true)
            },
            report:function(){
                var wall=[].concat(RNA.keys)
                while(wall.length>0){
                    var local=wall.shift()[0];
                    collect(local);
                }
            }
        }
        RNA.cluster();
        var wall=[].concat(RNA.keys)
        while(wall.length>0){
            var event=wall.shift()[0];
            var when=Date.now();
            event.alpha=[when];
            event.beta=[when];
            event.gamma=[when];
            event.delta=[when];
            event.alpha.offset=[]
            event.beta.offset=[]
            event.gamma.offset=[]
            event.delta.offset=[]
            event.alpha.flux=0
            event.beta.flux=0
            event.gamma.flux=0
            event.delta.flux=0
            event.alpha.total=0
            event.beta.total=0
            event.gamma.total=0
            event.delta.total=0
            document.addEventListener(event.type,logger.alpha)
            document.addEventListener(event.type,logger.beta)
            document.addEventListener(event.type,logger.gamma)
            document.addEventListener(event.type,logger.delta)
        }
        RNA.dispatch=logger.dispatch;
        RNA.register=logger.register;
        RNA.report=logger.report;
        RNA.finished=logger.close;
    }
    window.addEventListener("load",eventful,true);
     */
}())

