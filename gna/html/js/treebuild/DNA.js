/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *   Concurrent Command Heirarchy
 *          [ Capotolism ]
 *    
 *      GNA has 4 major purely logical "events":
 *          Ensure "Registration" of
 *                  4 Distinct processes.
 *  
 *                  
 *  Of:
 *      .elements.[Hydrogen,Oxygen,Nitrogen,Carbon,Phosphorus]
 *      
 *  Of:
 *      .particle
 *      .elements
 *      .{guanine(G),adenine(A),thymine(T),cytosine(C)}
 *      
 *      OF:
 *          (1)[Pyrimidines,Purines](0)
 *              Of:
 *                  (+)[trigger] - Major Groove (large - press),
 *                  (-)[thread] - Minor Groove (small - release)
 *              
 *  Of:
 *      ?? given the "GNA" base; 'how often' and 'how much' of a change ??
 *      
 *      A pairs only with T (of thymine) in DNA. 
 *      
 *          100 - 27 - 27 = 46% of total bases left.
 *      
 *      Given 27% are A (adenine), then there has to be 27% T.
 * 
 *          All bases are paired in DNA, so 46/2 = 23 pairs;
 *      
 *      23% of DNA are paired G-C bases,
 *      23% are G and another 
 *      23% are C bases.
 *          === ANSWER ===
 *          27% = Thymine, 
 *          23% = Guanine, 
 *          and 23% = Cytosine
 *      
 *  Of:
 *      (i) 'some listening' event (therefor 'observable')
 *      == GNA Pattern ==
 *          [1] couple:"couple",
 *          [2] decouple:"decouple",
 *          [3] trigger:"trigger",
 *          [4] destroy:"destroy",
 *      
 *  Of:
 *      == Command Pattern ==
 *          [1] Client,"Agent"
 *          [2] Invoker,"Waitress/StockRequest"
 *          [3] Command,[Concrete Command] 
 *                  {"...an order"}
 *          [4] Receiver "Chef/Fire"
 *          
 *      == Composite Pattern ==
 *          [1] Leaf (Command)
 *          [2] Trunk (Command)
 *              [+] couple (therefor add) +++++++++++ append="----"
 *              [+] register/confirm (event)--------- line ::::
 *              [+] decouple (therefor get/remove)||| wave ||||
 *              [+] (dispatch event)----------------- line ::::
 *      == Observer Pattern ==
 *          [1] Notify ([change])
 *          [2] Regular Expression ("GNA","method/class/etc")
 *      
 *  HISTORY:
 *      "Build Requests"
 *      Measurements:
 *          Synchronous vs Asynchronous
 *          Total Time to Execute
 *          Reported Exceptions (null pointer; etc)
 *          
 *      PATTERN REGISTRY
 *          KEY <title>----security----</title> 
 *                ****Inversion of Control****
 */
(function(){
    /*
    @TRIGGER
    @Child (below)
    
    2 TREES: Competing for "Time"
    
        TIME :: EVENT
    
        create{
            regularExpression
                
    
                "title".split(",")
                "title".split(".")
                "title".split(";")
        }
        
    
     */
    throw({
        GNA:function(event){
            GNA.chain=function(){
                var history=function(location){
                    return document.setTimeout(location)
                }
                throw{
                    race:function(event){
                        var event
                        var o=function(event){
                            
                        }
                        var styleName=function(event){
                            var 
                            GNA[event]=function(){
                                this.styleName===
                                }
                        }
                        var index=0,shadow=event;
                        throw({
                            canvas:function(event){
                                
                                return true,
                            },
                            gradient:function(event){
                                
                            },
                            index:function(event){
                                index++,
                            }
                            shadow:function(event){
                                if(index===0 && )
                                }
                            })
                            this.GNA=[
                                function(event){
                                    throw({
                                        zIndex:function(element){
                                            this.element.style.zIndex=index;
                                            if(shadow===element){
                                                var shadow=function(){
                                                    return function(){
                                                        var lock=[]
                                                        throw({
                                                            grow:function(event){
                                                                lock.unshif(event);
                                                                this.GNA.push(event)
                                                                return lock.push(
                                                                function(event){
                                                                    var local=event
                                                                    throw({
                                                                        shrink:function(event){
                                                                            this.GNA.splice(local,1);
                                                                            this.GNA.unshif(event);
                                                                            while(local!=event){
                                                                                throw({
                                                                                    c:function(){
                                                                                        var ROOT=function(variable){
                                                                                            var constant=variable,path=[],token=[],symbol={
                                                                                                base:[],
                                                                                                offset:[],
                                                                                                unknown:[],
                                                                                                feedback:[]
                                                                                            }
                                                                                            var crypt={
                                                                                                origin:Date.now(),
                                                                                                map:function(){
                                                                                                    return path
                                                                                                },
                                                                                                compass:function(){
                                                                                                    return token
                                                                                                },
                                                                                                crypt:function(){
                                                                                                    return symbol
                                                                                                },
                                                                                                ms:function(){
                                                                                                    constant-=1
                                                                                                },
                                                                                                ps:function(){
                                                                                                    constant+=1
                                                                                                },
                                                                                                base:function(){
                                                                                                    constant/=2
                                                                                                },
                                                                                                offset:function(){
                                                                                                    crypt.base(),crypt.base()
                                                                                                },
                                                                                                unknown:function(){
                                                                                                    crypt.ms(),crypt.offset()
                                                                                                },
                                                                                                feedback:function(){
                                                                                                    crypt.ps(),crypt.offset()
                                                                                                },
                                                                                                delta:function(){
                                                                                                    while(constant>1){
                                                                                                        crypt.compass().push(constant)
                                                                                                        switch(constant.toString(4).split('').pop()){
                                                                                                            case "0":
                                                                                                                crypt.map().push("gamma")
                                                                                                                symbol.offset.push(constant)
                                                                                                                crypt.offset()
                                                                                                                break
                                                                                                            case "2":
                                                                                                                crypt.map().push("resistor")
                                                                                                                symbol.base.push(constant)
                                                                                                                crypt.base()
                                                                                                                break
                                                                                                            default:
                                                                                                                if((constant-1).toString(4).split('').pop()==0){
                                                                                                                    crypt.map().push("delta")
                                                                                                                    symbol.unknown.push(constant)
                                                                                                                    crypt.unknown()
                                                                                                                    break
                                                                                                                }
                                                                                                                crypt.map().push("beta")
                                                                                                                symbol.feedback.push(constant)
                                                                                                                crypt.feedback()
                                                                                                        }
                                                                                                    }
                                                                                                    crypt.map().push("alpha");
                                                                                                }
                                                                                            }
                                                                                            return crypt;
                                                                                        },
                                                                                        init=function(){
                                                                                            var local = Date.now(),
                                                                                            linear = 299792458,
                                                                                            exponential = 299792458*299792458,
                                                                                            GNA.time=light(local)
                                                                                            time.delta();
                                                                                            console.log("MAP",time.map());
                                                                                            console.log("COMPASS",time.compass());
                                                                                            console.log("CRYPT",time.crypt());
                                                                                            GNA.line = GNA.light(linear);
                                                                                            line.delta()
                                                                                            console.log("MAP",line.map());
                                                                                            console.log("COMPASS",line.compass());
                                                                                            console.log("CRYPT",line.crypt());
                                                                                            GNA.record = GNA.light(exponential);
                                                                                            pert.delta()
                                                                                            console.log("MAP",pert.map());
                                                                                            console.log("COMPASS",pert.compass());
                                                                                            console.log("CRYPT",pert.crypt());
                                                                                            return local;
                                                                                        }
                                                                                        Function["function"]["final"](init),create=null,delete create;
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                        
                                                                    }
                                                                })
                                                            }
                                                        )
                                                        )
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    },
                                    function(event){
                                        index++
                                        console.error("SINGLETON !! MEMORY!! Core Event");
                                        return document.createEvent("Event");
                                    },
                                    function(){
                                        return document.createElement("Event");
                                    },
                                    function(){
                                        return document.createEvent("Event");
                                    },
                                    function(){
                                        return document.createAttribute("className");
                                    },function(){
                                        var grow=function(event){
                                            console.log("Grow Tail")
                                            return (GNA.event===this)
                                        }
                                    }
                            
                            
                                ]
                        
                                switch(event){
                                    case (GNA.event===this.event && this.event===GNA[event]):
                                        var function(){
                                    
                                        }
                            
                                }
                                throw{
                                    element:document.body.createElement("<canvas>"),
                                    className:function(){
                                        this.element.backgroundColor=["00FF00"]
                                    },
                                    canvas:function(){
                                        throw
                                        GNA.shadow={
                                            agent:createElement()
                                        }
                                        GNA.3D=function(event){
                                            this.event=
                                                return document.proxy
                                        }
                                        function(event){
                                            var parallax=event
                                            throw({
                                                element:document.body.createElement("<canvas>"),
                                            })
                                            GNA=GNA.3D();
                                            return document.element.style.zIndex=[window.innerHeight,window.innerWidth]
                                        }
                                    }
                                },
                                element:document.body.style.backgroundColor=["#FF00FF"],
                                zindex:function(){
                        
                                }
                            }
                        }
                        var local=event;
                        throw({
                            request:function(event){
                                var second=event;
                                return function(hero){
                                    var backup=hero
                                    local==
                                        throw({
                                        second:function(){
                                            throw({
                                                alpha:[[GNA],[local]]})
                                        }
                                    }
                                }
            
                            })
                            /*@Parent (above)<title>function</title>*/
        
                        }
                        GNA.id.id=function(){
                            return
                        }
                        GNA:{
                            O:function(event){
                                return
                                function(){
                                    return function(){
                                        var chain=[],id=0;
                                        GNA=;
                                        GNA=document.createEvent("event");
                                        red=/"#FF"/
                                        var history=[];
                                        history.push(chain)
                                    }
                                }
                            },
                            G:function(event){
            
                            }
                        }
                        var bufferedRead=function(event){
                            stylus=function(){
            
                            }
    
                            GNA=function(GNA){
                                try{
                                    var events=[]
                                    var linear=function(play){
                                        GNA=[]
                                        var scheduled=function(event){
                                            GNA(event,linear,GNA);
                                        }
                                    }
                                    var history=function(event){
                                        var backup=GNA;
                                        GNA.event=event;
                                        return function(backup){
                                            var restore=backup;
                                            while(GNA.events.length){
                                                if(GNA.event===backup){
                                                    return [].concat(GNA.event.concat(chain))
                                                }
                                                throw({error:function(){
                                                        return  backup;
                                                    }})
                                            }
                                        }
                                    }
                                    var clock=function(time){
                                        switch(history(time)){
                                            case GNA.closure:
                                                return function(event){
                                                    GNA.closure(event)
                                                    while(id>-10000){
                                                        chain.push(function(){
                                                            return{
                                                                alpha:[(GNA.closure===this.closure),(GNA.closure===GNA.closure)],
                                                                gamma:[(RNA.closure===this.closure),(RNA.closure===DNA.autograph)],
                                                                beta:[(GNA.closure===DNA.autograph),(RNA.closure===DNA.autograph)],
                                                                delta:[(GNA.closure===GNA.closure),(GNA.closure===GNA.closure)]
                                                            }
                                                        })
                                                    }
                                                }
                                            case RNA.closure:
                                                return function(event){
                                                    var history=[function(event){
                                                            GNA.event(event);
                                                            [clock(time)],[function(controller){
                                                                    return controller.beta()
                                                                }],[function(controller){
                                                                    return controller.gamma()
                                                                }],[function(controller){
                                                                    return controller.delta()
                                                                }
                                                            }]]
                                                    while(id>-1){
                                                        GNA=GNA();
                                                        var signature=function(event){
                                                            GNA.event=event
                                                            chain.push(function(){
                                                                return{
                                                                    controller:{
                                                                        alpha:function(){
                                                                            throw({alpha:[(controller.alpha===RNA.autograph),(controller.beta===GNA.autograph)]})
                                                                        },
                                                                        beta:function(){
                                                                            throw({beta:[(controller.beta===GNA.autograph),(controller.alpha===RNA.autograph)]})
                                                                        },
                                                                        gamma:function(){
                                                                            throw({gamma:[(delta.closure===DNA.autograph),(this.closure===RNA.autograph)]})
                                                                        },
                                                                        delta:function(){
                                                                            throw({delta:[(gamma.closure===DNA.closure),(this.gamma===FNA.autograph)]})
                                                                        }
                                                                    }
                                                                }
                                                            })
                                                        }
                                
                                                    }
                                                }
                                        }
                                    }
                                    var error=function(){
                                        id++;
                                    }
                                    var globalErrors=function(){
                                        return [[error]],[[globalErrors]]
                                    }
                                    throw({
                                        RNA:error,
                                        couple:function(){
                                            GNA.globalErrors=globalErrors();
                                            return function(event){
                                                if(event===this.RNA){
                                                    if(this.signature()==id++){
                                                        throw{
                                                            autograph:event,
                                                            history:function(name){
                                                                if(name){
                                                                    GNA.globalErrors=function(){
                                                                        return [this.history,this.autograph];
                                                                    }
                                            
                                                                    /*the more 'use of history' the larger the 'snapshot in the chain'.**/
                                                                }
                                                                id++
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    })
                                }catch(e){
            
                                }
                            }
                            while(GNA==undefined){
                                try{
                                    id++;
                                    RNA.closure();
                                    if(id>1000){
                                        DNA.closure();
                                        break;
                                    }
                                }catch(e){
                                    id--;
                                    throw({
                                        error:function(){
                                            var id=[id];
                                            return function(){
                                                throw({
                                                    error:function(){
                                                        RNA.closure();
                                                    }
                                                })    
                                            }
                                        }
                    
                                    }
                                }
                                console.warn("LIVE GNA:",GNA);
                                GNA.init=function(morph){
                                    try{
                                        var mygna=GNA._worker(morph);
                                        mygna()();
                                    }catch(e){
                                        throw({error:morph})
                                    }
                                }
    
        
                                GNA.clone=function(){
                                    return{
                                        proxy:function(RNA){
                                            console.group("DNA :: RNA == RNA :: DNADNAPROXY PROXY PROXY PROXY PROXY PROXY(@i):%o",Date.now());
                                            console.group("DNA :: RNA == RNA :: DNADNAPROXY PROXYPROXY PROXYPROXY PROXY(@i):%o",Date.now());
                                            console.group("DNA :: RNA == RNA :: DNADNATRIGGERTRIGGER(@i):%o",Date.now());
                                            return GNA.init(RNA)
                                        },
                                        agent:function(DNA){
                                            console.group("DNA :: DNA == GNA :: DNADNATRIGGERTRIGGER(@i):%o",Date.now());
                                            console.group("DNA :: DNA == GNA :: DNADNAAGENT AGENT AGENT AGENT AGENT AGENT (@i):%o",Date.now());
                                            console.group("DNA :: GNA == GNA :: DNADNAAGENT AGENT AGENT AGENT AGENT AGENT (@i):%o",Date.now());
                                            return GNA
                                        }
                                    }
                                }
                                /**
                                 *  Button: event 'creation'
                                 *      GNA.time=function(offset);
                                 *      returns
                                 *          backup of GNA,
                                 *          previous: // ref. to the 'button' (just pressed).
                                 *
                                 *
                                 */
                                var trial=function(){
                                    return [[DNA.event(RNA)],[DNA]]
                                }
                                DNA.event=(x,y){
                
                                }
                                DNA.random=function(event){
                                    console.group("DNA :: DNA == DNA :: DNADNATRIGGER(@i):%o",event);
                                    console.group("DNA :: DNA == DNA :: DNADNATRIGGER(@i):%o",event);
                                    console.group("DNA :: DNA == DNA :: DNADNATRIGGER(@i):%o",event);
                                    GNA=function(offset){
                                        Math.floor((Math.random()*event.timeStamp)+offset)
                                        Math.floor((Math.random()*Date.now())+offset)
                                    }
                                    GNA.time=function(offset){
                                        return {
                                            x:Math.floor((Math.random()*event.timeStamp)+offset),
                                            y:Math.floor((Math.random()*Date.now())+offset)
                                        }
                                    }
                                    console.group("DNA :: DNA == DNA :: DNADNATRIGGER(@i):%o",event);
                                    console.group("DNA :: DNA == DNA :: DNADNATRIGGER(@i):%o",event);
                                    console.group("DNA :: DNA == DNA :: DNADNATRIGGER(@i):%o",event);
                                    return{
                                        clone:{
                                            backup:GNA,
                                            previous:DNA.random,
                                            last:event
                                        }
                                    }
                                }
                                var counter=0;
                                GNA=function(GNA){
                                    console.group("DNA :: DNA == DNA :: DNADNAROOT TREE SEED!!(@i):%o",counter++,Date.now());
                                    GNA.local=function(event){
                                        console.group("DNA :: DNA == DNA :: DNADNAROOT TREE SEED!!(@i):%o",counter++,Date.now());
                                        GNA.panic=button(event);
                                        console.group("DNA :: DNA == DNA :: DNADNAPANIC BUTTON!!(@i):%o",counter++,Date.now());
                                        return function(hero){
                                            console.group("DNA :: DNA == DNA :: DNADNAGNA.event=hero(@i):%o",counter++,Date.now());
                                            GNA.event=hero;
                                            console.group("DNA :: DNA == DNA :: DNADNAI can help(@i):%o",counter++,Date.now());
                                            if(hero===GNA.panic){
                                                console.group("DNA :: DNA == DNA :: DNADNAMabye I can help(@i):%o",counter++,Date.now());
                                                GNA.trigger=button(hero);
                                                console.group("DNA :: DNA == DNA :: DNADNAI KNOW HOW TO DO THIS(@i):%o",counter++,Date.now());
                                                return function(object){
                                                    console.group("DNA :: DNA == DNA :: DNADNAFOUND INSTRUCTIONS(@i):%o",counter++,Date.now());
                                                    GNA.real=object;
                                                    console.group("DNA :: DNA == DNA :: DNADNAFOUND EXPLOSIVE(@i):%o",counter++,Date.now());
                                                    return (function(trigger){
                                                        console.group("DNA :: DNA == DNA :: DNADNATRIGGER EXPLOSION(@i):%o",counter++,Date.now());
                                                        var event;
                                                        GNA.couple=function(){
                                                            return event
                                                        }
                                                        console.group("DNA :: DNA == DNA :: DNADNA!!!!! WATCH FOR THE PROBLEMS   !!!(@i):%o",counter++,Date.now());
                                                        return function(){
                                                            console.group("DNA :: DNA == DNA :: DNADNA  ******  ACTIVE EXPLOSION(@i) ******* %o",counter++,Date.now());
                                                            return {
                                                                active:function(){
                                                                    return (this.now==Date.now());
                                                                },
                                                                count:[0],
                                                                backed:trigger,
                                                                now:Date.now(),
                                                                counted:function(poly){
                                                                    console.group("DNA :: DNA == DNA :: DNADNACounted Poly(@i):%o",counter++,Date.now());
                                                                    var rose=poly;
                                                                    console.group("DNA :: DNA == DNA :: DNADNAA Rose by any other name is Poly(@i):%o",counter++,Date.now());
                                                                    var button=GNA.event;
                                                                    console.group("DNA :: DNA == DNA :: DNADNADate.now(@i):%o",counter++,Date.now());
                                                                    var verify=function(event){
                                                                        console.group("DNA :: DNA == DNA :: DNADNAVerify Triggered(@i):%o",counter++,Date.now());
                                                                        button(GNA.event);
                                                                        console.group("DNA :: DNA == DNA :: DNADNAVerify Button(@i):%o",counter++,Date.now());
                                                                        if(button===GNA.event){
                                                                            console.group("DNA :: DNA == DNA :: DNADNAVerify Button&Event(@i):%o",counter++,Date.now());
                                                                            switch(event){
                                                                                case rose:
                                                                                    console.warn("HERO")
                                                                                    console.warn("HERO")
                                                                                    console.warn("HERO")
                                                                                    console.warn("HERO")
                                                                                    return hero
                                                                                case button:
                                                                                    console.warn("GIRL")
                                                                                    console.warn("GIRL")
                                                                                    console.warn("GIRL")
                                                                                    console.warn("GIRL")
                                                                                    return poly
                                                                            }
                                                                            console.group("DNA :: DNA == DNA :: DNADNAVerified Exclusive Event(@i):%o",counter++,Date.now());
                                                                            return {
                                                                                event:hero,
                                                                                agent:poly
                                                                            }
                                                                        }
                                                                        console.group("DNA :: DNA == DNA :: DNADNADate.now(@i):%o",counter++,Date.now());
                                                                        return {}
                                                                    }
                                                                    return function(joe,schmoe){
                                                                        console.log("It's JOE & SCHMOE!(@i):%o",counter++,Date.now());
                                                                        if(!joe & !schmoe){
                                                                            console.group("DNA :: DNA == DNA :: DNADNAGUESTS:%o",counter++,Date.now());
                                                                            return function(event){
                                                                                console.group("DNA :: DNA == DNA :: DNADNAFOR EVENT:%o",counter++,Date.now());
                                                                                return verify(event);
                                                                            }
                                                                        }
                                                                        console.warn("Go with JOE & SCHMOE ONLY!! (@i):%o",counter++,Date.now());
                                                                        return {
                                                                            x:joe,
                                                                            y:schmoe
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })(function(){
                                                        console.group("DNA :: DNA == DNA :: DNADNAOUTSIDE(@i):%o",counter++,Date.now());
                                                        (function(msg){
                                                            console.group("DNA :: DNA == DNA :: DNADNAINSIDE(@i):%o",counter++,Date.now());
                                                            return "<title>----"+msg+"----</title>"   
                                                        })("1337")
                                                        console.group("DNA :: DNA == DNA :: DNADNADate.now(@i):%o",counter++,Date.now());
                                                        return Date.now()
                                                    })
                                                }
                                                console.group("DNA :: DNA == DNA :: DNADNADate.now(@i):%o",counter++,Date.now());
                                                return hero
                                            }
                                            console.group("DNA :: DNA == DNA :: DNADNADate.now(@i):%o",counter++,Date.now());
                                            return hero;
                                        }
                                        console.group("DNA :: DNA == DNA :: DNADNADate.now(@i):%o",counter++,Date.now());
                                    }
                                }
                                console.group("DNA :: DNA == DNA :: DNADNADONE(@i):%o",GNA);
                            }(function(){
                                console.group("DNA :: DNA == DNA :: ",DNA)
                                console.group("DNA :: DNA == DNA :: ",GNA)
                                console.group("DNA :: DNA == DNA :: ",RNA)
                            }))

                            /**
    (function(){
        GNA.tree=function(event,named){
            GNA.key=Date.now();
            GNA.hash=event.timeStamp;
            GNA.className[named]=function(){
                return named;
            }
            GNA.className[named].name=named;
            var secure=function(){
                return {
                    fire:this.className(GNA.className[named],named)
                }
            }
            var backup=GNA.tree;
            var shrink=function(){
                console.group("DNA :: DNA == DNA :: DNADNA<title>----Signature----</title>")
                if(typeof GNA.className===typeof signature){
                    console.error("typeof",signature);
                    GNA.tree=signature
                    return (GNA.tree==signature())
                }
                console.groupEnd();
            }
            GNA.signature=function(){
                if(GNA.prototype===signature.prototype){
                    console.error("prototype of")
                    if(GNA.constructor===signature.constructor){
                        console.error("consructor of")
                        if(GNA===GNA.tree){
                            console.error("SELF")
                            return function(){
                                return GNA.tree;
                            }
                        }
                    }
                }
            }
            var token=function(signature){
                throw exception({named:signature});
                throw exception({className:signature});
                throw exception({error:function(){var fire=signature;}});
                throw exception({fire:secure,check:signature});
            }
            var name=function(){
                var trunk=GNA[named]=function(){
                    var clone={
                        tree:token,
                        H:function(){
                            return "A(adenine)";
                        },
                        G:function(){
                            return "guanine(G)";
                        },
                        T:function(){
                            return "thymine(T)";
                        },
                        C:function(){
                            return "cytosine(C)";
                        },
                        context:GNA[named],
                        apply:leaf
                    }
                    return clone;
                }
            }
            return trunk;
        },
        GNA.elements={
            water:[["Hydrogen"]["Oxygen"]],
            fire:[["Phosphorus"]["Nitrogen"]],
            carbon:Carbon[["GNA"]["<title>----Secure----<title>"]]
        },
        GNA.secure=function(now){
            var right=now;
            GNA.signature=function(){
                var real=Date.now();
                return 
            }
            return verify
            
            return{
                start:function(named){
                    if(GNA.secure===name){
                        var copy=this.secure;
                        var name=named;
                        return function(){
                            if(copy===this.secure){
                                var copy=[named];
                                return {
                                    signature:function(){
                                        return copy===this.secure;
                                    },
                                    named:function(){
                                        name
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        GNA.secure.start={
            
        }
        var crypt={
            origin:Date.now(),
            map:function(){
                return path
            },
            compass:function(){
                return token
            },
            crypt:function(){
                return symbol
            },
            ms:function(){
                constant-=1
            },
            ps:function(){
                constant+=1
            },
            base:function(){
                constant/=2
            },
            offset:function(){
                crypt.base(),crypt.base()
            },
            unknown:function(){
                crypt.ms(),crypt.offset()
            },
            feedback:function(){
                crypt.ps(),crypt.offset()
            },
            delta:function(){
                while(constant>1){
                    crypt.compass().push(constant)
                    switch(constant.toString(4).split('').pop()){
                        case "0":
                            crypt.map().push("gamma")
                            symbol.offset.push(constant)
                            crypt.offset()
                            break
                        case "2":
                            crypt.map().push("resistor")
                            symbol.base.push(constant)
                            crypt.base()
                            break
                        default:
                            if((constant-1).toString(4).split('').pop()==0){
                                crypt.map().push("delta")
                                symbol.unknown.push(constant)
                                crypt.unknown()
                                break
                            }
                            crypt.map().push("beta")
                            symbol.feedback.push(constant)
                            crypt.feedback()
                    }
                }
                crypt.map().push("alpha");
            }
        }
        var language=function(variable){
            var constant=variable,
            path=[],
            token=[],
            symbol={
                base:[],
                offset:[],
                unknown:[],
                feedback:[]
            }
        }
        var eventful=function(){
            var testA=function(){
                var coodinator={
                    particles:[],
                    signature:function(){
                        var local = Date.now(),
                        time = language(local);
                        time.delta()
                    },
                    startup:function(){
                        var mygna=GNA();
                        mygna();
                        GNA([testA,testB,testC,testD]);
                        GNA.gna();
                    },
                    alpha:function(){
                        var local = Date.now(),time = language(local);
                        time.delta()
                        particles.unshift(pert);
                        console.log("alpha sub-system")
                    },
                    beta:function(){
                        var linear = 299792458,line = language(linear);
                        line.delta()
                        console.log("beta sub-system")
                    },
                    gamma:function(){
                        var exponential = 299792458*299792458,pert = language(exponential);
                        pert.delta()
                        particles.unshift(pert);
                        console.log("gamma sub-system")
                    },
                    signal:function(noise){
                        coordinator.particles.unshift(noise);
                        coordinator.delta(noise)
                    },
                    delta:function(feature){
                        coordinator.particles.shift(feature.delta());
                        console.log("delta sub-system")
                    }
                }
                var subsystem=function(){
                    monitor.gna("more",[].concat([coordinator.alpha,coordinator.beta,coordinator.gamma,coordinator.delta]))
                }
                console.group("DNA :: DNA == DNA :: DNADNAbatch");
                monitor.crypt.push([time,linear,exponential])
            
            },testB=function(){
                console.log("test B")
            },testC=function(){
                console.log("test C")
            },testD=function(){
                console.log("test D")
            }
            var monitor={
                alpha:0,
                beta:0,
                gamma:0,
                delta:0,
                init:Date.now(),
                control:[],
                crtyp:[],
                batch:function(){
                    try{
                        //monitor.control.push(window.setInterval(monitor.batch, 5000));
                        if(monitor.alpha>100){
                            while(monitor.control.length>0){
                                window.clearInterval(monitor.control.pop());
                            }
                            monitor.gna("done");
                        }
                    }catch(e){
                        console.log("Batch Exception!",e)
                    }
                    console.log(monitor)
                },
                integrate:function(){
                    monitor.batch();
                }
            }
            monitor.integrate();
        }
        window.addEventListener("load",eventful,true);
                             */
    
