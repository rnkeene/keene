/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *   Global Event pre-processor
 *   Randomization Tests
 *          %time/Math.random/etc
 *           Evaluate Timing on
 *          event.timeStamp; Date.now(); offsets
 *   "Finalize" with Exceptional Programming...
 *      Ensure 
 *          alpha=Event.timeStamp(),
 *          beta=Date.now(),
 *          gamma=Math.random(),
 *          
 *              vs
 *              
 *      <Canvas> max size of 400x400 (run 'test') to create 'optimal' canvas sizes;
 *              This means 'the right' size canvas, 'at the right time'.
 *      
 *      
 *   "Self"RNA@interval spawning:
 *          This makes sure a process for 'controlling'/'auto-correcting'
 *          the code and can process:
 *              --window.setInterval (event driven prototypes)--
 *              
 *              "4 'functions'; required to only run if operational
 *              @interval "Random" (e.g. 'random selection') of 
 *                  'when and where' any particular one of the
 *                  4 functions is available.
 *                  
 *          Ensures code can 'auto-correct' itself in the event of problems
 *              'regardless' of which of the 4 or at what state.
 *              (store by 'fastest' time) ---
 *                  FNA (feedback) is 'black-box' testing;
 *                      
 *             GNA Manages the raw 'threads'/'workers'/'events.
 *             DNA 
 *   
 *   gamma=Math.random(),
 */
(function(){
    var memory=[],history=[],GNA=function(event){
        history.unshift(event)
        function(){
            history.shift(
                function(){
                    
                }
            )
        }
        return{
        half:[parseInt(window.innerWidth/2),parseInt(window.innerHeight/2)],
        origin:[window.innerWidth,window.innerHeight],
        origin:[window.innerWidth*2,window.innerHeight*2],
        relative:function(){
            try{
                
            }catch(e){
                
            }
            GNA:{
                plus:function(event){
                    var local=0;
                    throw{
                        RNA:function(event){
                            var race=0;
                            var timer=event.timeStamp();
                            while(event.timeStamp===Date.now()){
                                var BLACK_HOLE=[function(){
                                        throw({
                                            exception:0
                                        })
                                    }]
                                var WHITE_HOLE=[
                                    function(){
                                        throw({
                                            exception:0
                                        })
                                    }
                                ]
                            }
                            RNA.closureFunction();
                        }
                    }
                },
                minux:function(event){
                    return (GNA.history===event)
                }
            },
            RNA:{
                
            }
        },
        id:function(event){
            var local=event.timeStamp;
            throws({
                proof:function(event){
                    (!event.timeStamp===local)
                }
            })
            x"title".split("")::throw{waiting:}
        },
        event:window,
        GNA:function(){
            
        }
    }
    /*
    var init=function(){
        document.body.style.position="static",
        document.body.style.backgroundColor="#FF0000",
        document.body.style.overflow="hidden",
        document.body.style.zIndex=0;
        document.body.style.margin=0;
        document.body.style.padding=0;
        document.body.width=window.innerWidth;
        document.body.height=window.innerHeight;
    }
    window.addEventListener("load", init, true);
    var scripts=document.getElementsByTagName('script');
    while(scripts[0]){
        scripts[0].parentElement.removeChild(scripts[0]);
        scripts=document.getElementsByTagName('script');
    }
    var counter=0;
    FNA=function(GNA){
        while(GNA!=undefined){
            console.log(GNA)
            try{
                counter++;
                console.log(GNA.counter);
                if(GNA.counter){
                    console.info("RNAMIRACLE MAGIC FAITH MAGIC MIRACLE MAGIC FAITH MAGIC FAITH")
                    return;
                }
            }catch(e){
                console.info("RNASTRANGE STRANGE STRANGE STRANGE STRANGE ")
                console.log(e)
                return;
            }
            if(counter>500){
                return;
            }
            counter++;
        }
        try{
            console.group("RNA(@i):%o",counter++,GNA);
            console.log(GNA)
            var GNA=GNA();
            console.group("RNA(@i):%o",counter++,GNA);
            console.log(GNA);
            GNA=GNA();
            console.group("RNA(@i):%o",counter++,GNA);
            console.log(GNA);
            GNA=GNA();
            console.group("RNA(@i):%o",counter++,GNA);
            console.log(GNA);
            GNA=GNA();
            console.group("RNA(@i):%o",counter++,GNA);
            GNA.closure=closure(GNA);
            console.warn("GNA ==== CLOSURE ==== ",GNA.closure);
            console.info(GNA)
            console.group("CLOSURE==plante(@i):%o",counter++,GNA);
            console.info(GNA.closure.plant())
            console.group("CLOSURE==truee(@i):%o",counter++,GNA);
            console.info(GNA.closure.tree())
            console.group("CLOSURE==childe(@i):%o",counter++,GNA);
            console.info(GNA.closure.child())
            console.info(GNA.closure.child())
            console.group("==== CLOSURE ==== CLOSED(@i):%o",counter++,GNA);
        }catch(e){
            console.error(e)
            console.error("%o",GNA)
        }
    }
/*
    var update=function(){
        
    }
    var plus=function(){
        var location=[0,window.innerHeight];
        var canvas=DNA.html("canvas",".plus");
        var context=canvas.getContext("2d")
        canvas.className="plus";
        canvas.width=parseInt(window.innerWidth/20),
        canvas.height=parseInt(window.innerHeight/20);
        DNA.style.append(".plus","left",location[0]+"px");
        DNA.style.append(".plus","top",location[1]+"px");
        DNA.style.apply(".plus");
        var moveUp=function(){
            if(location[0]>window.innerWidth){
                location[0]=0;
                location[1]=window.innerHeight
            }else{
                location[0]+=1;
            }
            if(location[1]<0){
                location[0]=0;
                location[1]=window.innerHeight
            }else{
                location[1]-=1;
            }
            DNA.style.append(".plus","left",location[0]+"px");
            DNA.style.append(".plus","top",location[1]+"px");
            DNA.style.apply(".plus");
        }
        var upGradient=function(){
            var center=[parseInt(canvas.width/2),parseInt(canvas.height/2)];
            var radial=context.createRadialGradient(center[0],center[1],0,center[0],center[1],canvas.height);
            radial.addColorStop(0,"#ffffff");
            radial.addColorStop(.5,"#000000");
            // Fill with gradient
            context.fillStyle=radial;
            context.fillRect(0, 0, canvas.width, canvas.height);
        }
        var interval=window.setInterval(moveUp,10000);
        upGradient();
    }
    
    DNA("plus",plus,plus);
     */
}())

/*
    var draw=function(){
        
        DNA.html.feature={}
        DNA.html.feature.width=function(){
            return parseInt(DNA.html.canvas.width*(window.innerWidth/DNA.html.lastCanvas[0]))
        }
        DNA.html.feature.height=function(){
            return parseInt(DNA.html.canvas.height*(window.innerHeight/DNA.html.lastCanvas[1]))
        }
        DNA.html.feature.resize=function(){
            DNA.html.canvas.width=DNA.html.feature.width()
            DNA.html.canvas.height=DNA.html.feature.height()
        }
        window.addEventListener("resize",DNA.html.resize,true);
        DNA.html.feature.zoom=function(event){
            if(event.wheelDelta>0){
                return DNA.html.feature.grow()
            }
            return DNA.html.feature.shrink()
        }
        window.addEventListener("mousewheel",DNA.html.feature.zoom,true);
        DNA.html.feature.zoomFire=function(event){
            if(event.detail>0){
                return DNA.html.feature.shrink()
            }
            return DNA.html.feature.grow()
        }
        window.addEventListener("DOMMouseScroll",DNA.html.feature.zoomFire,true);
        DNA.html.feature.grow=function(){
            if(DNA.html.canvas.width<window.innerWidth && DNA.html.canvas.height<window.innerHeight){
                DNA.html.canvas.width*=2,
                DNA.html.canvas.height*=2;
                balance();
            }
        }
        DNA.html.feature.shrink=function(){
            if(DNA.html.canvas.width>4 && DNA.html.canvas.height>4){
                DNA.html.canvas.width=parseInt(DNA.html.canvas.width/2),
                DNA.html.canvas.height=parseInt(DNA.html.canvas.height/2),
                balance();
            }
        }
        var start=function(){
            DNA.html.feature.color=["#ffff00","#000000"]
            DNA.html.canvas.width=parseInt(DNA.html.canvas.width/10)
            DNA.html.canvas.height=parseInt(DNA.html.canvas.height/10)
        }
        var load=function(){
            DNA.html.lastCanvas=[window.innerWidth,window.innerHeight],
            DNA.html.feature.x=parseInt(window.innerWidth/2),
            DNA.html.feature.y=parseInt(window.innerHeight/2);
        }
        var balance=function(){
            DNA.html.feature.xy=[parseInt(DNA.html.canvas.width/2),parseInt(DNA.html.canvas.height/2)]
            offset();
        }
        var offset=function(){
            DNA.html.feature.offx=DNA.html.feature.x
            DNA.html.feature.offy=DNA.html.feature.y
            if(DNA.html.canvas.width>2){
                DNA.html.feature.offx-=parseInt(DNA.html.canvas.offsetWidth/2)
            }
            if(DNA.html.canvas.height>2){
                DNA.html.feature.offy-=parseInt(DNA.html.canvas.offsetHeight/2)
            }
            absolute()
        }
        var absolute=function(){
            DNA.html.canvas.style.left=DNA.html.feature.offx+"px";
            DNA.html.canvas.style.top=DNA.html.feature.offy+"px";
            gradient()
        }
        var position=function(){
            start();
            load();
            balance();
            gradient()
        }
        var gradient=function(event){
            DNA.html.feature.gradient=DNA.html.context.createRadialGradient(DNA.html.feature.xy[0],DNA.html.feature.xy[1],0,DNA.html.feature.xy[0],DNA.html.feature.xy[1],DNA.html.canvas.height);
            DNA.html.feature.gradient.addColorStop(0,DNA.html.feature.color[0]);
            DNA.html.feature.gradient.addColorStop(.5,DNA.html.feature.color[1]);
            DNA.html.context.fillStyle=DNA.html.feature.gradient;
            DNA.html.context.fillRect(0,0,DNA.html.canvas.width,DNA.html.canvas.height);
            DNA.html.context.restore();
        }
        DNA.html.feature.center=function(){
            console.info("CENTER <canvas>");
            DNA("gradient",gradient,position)
            console.info("CENTER <canvas>");
        }
        DNA.canvas()
    }
    var canvas=function(event){
        DNA.html.feature.center()
        console.info(event.timeStamp)
    }
    DNA("canvas",canvas,draw);*/