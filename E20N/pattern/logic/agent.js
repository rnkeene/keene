/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 *  
 *  @Interval Transform/Motion (@<canvas>)
 *  @Mouse-Over 'pause/activate' (disable proxy, activate local)
 *  
 */
(function(){
    var intercept=request;
    request=null,delete request;
    var learn=function(subject){
        var local=subject;
        var proxy=function(name,module){
            return local.behavior(name).behave(module)
        }
        var clone={
            control:function(name,module){
                return proxy(name,module)
            },
            destroy:function(){
                local=null,delete local,
                proxy=null,delete proxy,
                clone.proxy=null,delete clone.proxy,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        return clone
    }
    var concurrent=function(proxy){
        var subject=intercept.subject(proxy);
        var science=learn(subject);
        var controllers=[[],[]]
        var model=function(recursive){
            var button=recursive;
            var press=function(){
                return button()
            }
            var state=false;
            var clone={
                state:function(){
                    return state
                },
                behaviors:function(){
                    return controllers
                },
                concurrent:function(name,module){
                    var key=controllers[0].indexOf(name);
                    key<=0 && !!controllers[0].unshift(name) && (key=0);
                    controllers[1][key]==null && controllers[1].unshift([module]);
                    var pair=controllers[1][key].indexOf(module);
                    pair<=0 && !!controllers[1][key].unshift(module) && (pair=0);
                    console.groupCollapsed("agent behavior controller info")
                    console.info("Key=%i, Pair=%i @%s",key,pair,name)
                    console.info("size: (%i x %i)", controllers[0].length, controllers[1][key].length)
                    console.groupEnd()
                    return science.control(controllers[0][key],controllers[1][key][pair])
                },
                toggle:function(){
                    state=press()
                    return state
                },
                destroy:function(){
                    console.group("Destroy proxy logic")
                    if(press()){
                        press()
                    }
                    science.destroy();
                    science=null,delete science,
                    button=null,delete button,
                    clone.start=null,delete clone.start,
                    clone.collect=null,delete clone.collect,
                    clone.finish=null,delete clone.finish,
                    clone.toggle=null,delete clone.toggle,
                    clone.destroy=null,delete clone.destroy,
                    clone=null,delete clone
                    console.groupEnd()
                }
            }
            return clone
        }
        return function(toggle){
            return model(toggle)
        }
    }
    var logic=function(proxy){
        var subject=proxy
        var instance=concurrent(subject);
        var clone={
            instance:function(toggle){
                instance=instance(toggle);
                clone.instance=function(){
                    return instance
                }
                return instance
            },
            destroy:function(){
                instance.destroy();
                subject=null,delete subject,
                clone.stack=null,delete clone.stack,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        return clone
    }
    var style=intercept.stylesheet();
    var environments=[[],[]]
    var agent={
        _debug:function(){
            //DELETE THIS METHOD LATER -- and 'sub'-debugging
            console.group("@Agent Intercept")
            console.log("Composites:",environments[0].length)
            intercept.debug()
            console.groupEnd()
        },
        condition:function(name){
            return style.rule(name)
        },
        plot:function(start,stop){
            return intercept.coordinate(start,stop)
        },
        proxy:function(subject){
            var key=environments[0].indexOf(subject)
            if(key<0){
                var id=subject
                if(id.instance){
                    id=id.instance()
                }
                var proxy=intercept.subject(id);
                var plane=logic(id)
                var clone={
                    instance:function(){
                        return id
                    },
                    stack:function(){
                        return plane
                    },
                    subject:function(){
                        return proxy
                    },
                    destroy:function(){
                        environments.splice(environments.indexOf(id),1);
                        plane.destroy();
                        id=null,delete id,
                        proxy=null,delete proxy,
                        plane=null,delete plane,
                        clone.id=null,delete clone.id,
                        clone.map=null,delete clone.map,
                        clone.subject=null,delete clone.subject,
                        clone.destroy=null,delete clone.destroy,
                        clone=null,delete clone
                    }
                }
                environments[0].unshift(id),
                environments[1].unshift(clone),
                key=0
            }
            return environments[1][key]
        },
        produce:function(name){
            return intercept.structure(name)
        },
        intercept:function(action,context){
            if(context){
                intercept.context(context)
            }
            if(action){
                intercept.action(action)
            }
            intercept()
        }
    }
    request=function(){
        return agent
    }
    agent.intercept("limits");
})()