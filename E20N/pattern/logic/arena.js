/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var agent=request();
    request=null,delete request;
    var limits=agent.condition("limit");
    var model=function(logic){
        var manager=logic.stack();
        var child=logic.instance();
        var signal="",head=[],state=false;
        var selector=agent.condition("arena"+Date.now());
        selector.property("cursor","crosshair")
        selector.promote()
        selector.toggle(child);
        limits.toggle(child);
        var toggle=function(){
            var restore=toggle;
            toggle=function(){
                toggle=restore;
                state=false,
                mdown.toggle(true,false)
                mup.toggle(true,false)
                mmove.toggle(true,false)
                selector.toggle(child,true)
                if(state==false){
                    return state
                }
                throw{
                    state:"out of synch"
                }
            }
            head=[];
            signal="";
            state=true
            selector.property("cursor","crosshair")
            selector.promote();
            selector.toggle(child)
            mdown.toggle(true,true)
            if(state==true){
                return state
            }
            throw{
                state:"out of synch"
            }
        }
        var down=function(event){
            head=[event.clientX,event.clientY],
            mdown.toggle(true,false),
            mup.toggle(true,true),
            mmove.toggle(true,true);
            return false
        }
        var follow=function(update){
            
            if(head[0]!=update.clientX && head[1]!=update.clientY){
                var space=agent.plot(head.concat(),[update.clientX,update.clientY])
                if(signal!=space.state){
                    signal=space.state;
                    selector.toggle(child,true)
                    switch(space.state){
                        case "nw":
                            selector.property("cursor","nw-resize")
                            break
                        case "sw":
                            selector.property("cursor","sw-resize")
                            break
                        case "se":
                            selector.property("cursor","se-resize")
                            break
                        case "ne":
                            selector.property("cursor","ne-resize")
                            break
                    }
                    selector.promote()
                    selector.toggle(child)
                }
                space.destroy();
            }
        }
        var up=function(event){
            mup.toggle(true,false),
            mmove.toggle(true,false);
            selector.property("cursor","crosshair")
            selector.promote()
            if(event.clientX!=head[0] && event.clientY!=head[1]){
                var locate=agent.plot(head.concat(),[event.clientX,event.clientY])
                locate.transform(window,"part");
                agent.intercept("name","pattern/state")
            }
            head=[];
            signal="";
            mdown.toggle(true,true)
        }
        var loader=manager.instance(function(){
            return toggle()
        });
        var mdown=loader.concurrent("mousedown",down)
        var mmove=loader.concurrent("mousemove",follow)
        var mup=loader.concurrent("mouseup",up)
        var destroy=manager.destroy;
        manager.destroy=function(){
            destroy()
            loader.destroy();
            mdown.destroy()
            mmove.destroy()
            mup.destroy()
            signal=null,delete signal,
            head=null,delete head,
            state=null,delete state,
            down=null,delete down,
            follow=null,delete follow,
            toggle=null,delete toggle,
            up=null,delete up,
            loader=null,delete loader,
            mdown=null,delete mdown,
            mmove=null,delete mmove,
            mup=null,delete mup
        }
        return manager
    }
    var instance=function(element){
        var manager=model(agent.proxy(element));
        var plane=[manager];
        var clients=[]
        var arena={
            client:function(client,remove){
                if(client){
                    var key=clients.indexOf(client);
                    if(key<0){
                        if(remove){
                            return client
                        }
                        clients.unshift(client)
                    }else if(remove){
                        return clients.splice(key,1)
                    }
                    client.instance().toggle();
                    return client
                }
                return clients.concat()
            },
            capture:function(logical){
                console.group("Capture")
                var global=plane[0].instance();
//                console.warn("global-",global)
                if(global.state()){
                    global.toggle()
                    
                }
//                console.info("@State=",global.state())
                plane.unshift(logical)
                var local=logical.instance()
//                console.warn("local-",local)
                local.toggle()
            },
            release:function(){
                console.group("Release")
                plane.shift().instance().toggle();
                console.log("Toggle Off Global State")
                plane[0].instance().toggle()
                console.groupEnd()
                console.groupEnd()
            },
            destroy:function(){
                var wall=plane[1].concat();
                while(wall.length){
                    var logic=wall.shift();
                    if(logic.state()){
                        logic.toggle()
                    }
                    logic.destroy();
                }
                arena.agent=null,delete arena.agent,
                arena.capture=null,delete arena.capture,
                arena.release=null,delete arena.release,
                arena.destroy=null,delete arena.destroy,
                arena=null,delete arena,
                plane=null,delete plane
            }
        }
        manager.arena=function(){
            return arena
        }
        var destroy=manager.destroy;
        manager.destroy=function(){
            plane.splice(plane.instance(arena),1)
            arena.destroy();
            destroy();
            clients=null,delete clients,
            manager=null,delete manager,
            manager.arena=null,delete manager.arena,
            manager=null,delete manager,
            destroy=null,delete destroy,
            model=null,delete model
        }
        manager.instance().toggle()
        return manager
    }
    var containers=[[],[]];
    var override=function(wrapper){
        if(wrapper){
            var key=containers[0].indexOf(wrapper)
            if(key<0){
                containers[0].unshift(wrapper),
                containers[1].unshift(instance(wrapper)),
                key=0
            }
            return containers[1][key]
        }
        return containers[1][0]
    }
    var global=override(agent.produce("div"));
    var proxy=global.instance();
    var space=global.arena();
    
    var arena={
        debug:function(){
            console.groupCollapsed("Arena.js Debug")
            console.warn("wrapper:",global)
            console.warn("proxy:",proxy)
            console.warn("arena:",space)
            console.warn("agent:",agent)
            console.warn("containers:",containers)
        },
        agent:function(){
            return agent
        },
        arena:function(){
            return space
        },
        proxy:function(subject){
            if(subject){
                return override(subject)
            }
            return proxy
        },
        composite:function(){
            return containers
        },
        wrapper:function(){
            return global
        }
    }
    request=function(){
        return arena
    }
})()