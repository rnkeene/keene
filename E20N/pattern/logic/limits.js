/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var agent=request();
    var selector=agent.condition("static");
    selector.property("background-color","#000000")
    selector.property("position","static");
    selector.property("overflow","hidden");
    selector.property("margin","0px");
    selector.property("padding","0px");
    selector.property("z-index","1");
    selector.property("-webkit-touch-callout","none")
    selector.property("-webkit-user-select","none")
    selector.property("-khtml-user-select","moz-none")
    selector.property("-moz-user-select","none")
    selector.property("-ms-user-select","none")
    selector.property("user-select","none")
    selector.promote();
    selector.toggle(document.body)
    
    var style=agent.condition("limit");
    style.property("width",window.innerWidth+"px")
    style.property("height",window.innerHeight+"px")
    style.promote();
    style.toggle(document.body)
    
    var width=window.innerWidth+"px";
    var height=window.innerHeight+"px"
    var update=function(){
        var updated=false
        var xwidth=window.innerWidth+"px"
        if(width!=xwidth){
            width=xwidth,
            style.property("width",width);
            updated=true
        }
        var xheight=window.innerHeight+"px"
        if(height!=xheight){
            height=xheight,
            style.property("height",height);
            updated=true
        }
        return updated
    }
    var resize=function(){
        if(update()){
            style.promote()
        }
    }
    var global=agent.proxy(window)
    var proxy=global.subject().behavior("resize");
    var behave=proxy.behave(resize)
    behave.toggle()
    agent.intercept("arena")
})()