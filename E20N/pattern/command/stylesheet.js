/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 *  
 *  Stylesheet Nomenclature:
 *  
 *      Selector{
 *          property:value;
 *      }
 *  
 *          -OR-
 *  
 *      Rule{
 *          declaration
 *      }
 *  
 *  There are 1-n properties for any Selector/Rule.
 *  Each property is accompanied with a value.
 *  
 *  
 *  Use: request.stylesheet() to access the primary API.
 *  
 */
(function(){
    var style=document.createElement("style");
    var reset=function(){
        document.body.innerHTML="",
        document.head.innerHTML="";
        document.head.appendChild(style)
    }
    reset()
    var proxy=function(title,element,clean){
        if(clean){
            if(element.className.indexOf(title)>0){
                element.className=element.className.replace(" "+title,"")
                return
            }
            if(element.className.indexOf(" ")){
                element.className=element.className.replace(title+" ","")
                return
            }
            element.className=element.className.replace(title+"","")
        }
        if(element.className.indexOf(title)>-1){
            return
        }
        if(element.className.length){
            element.className+=" "
        }
        element.className+=title
    }
    var selectors=[[],[]];
    var create=function(local){
        var key=selectors[0].indexOf(local);
        if(key<0){
            var name=local,
            type=".",
            properties=[[],[]];
            var product=null;
            var promote=function(){
                var backup=promote
                promote=function(){
                    promote=backup;
                    style.removeChild(product);
                    promote();
                }
                product=document.createTextNode(type+name+"{"+properties[1].join("")+"}")
                style.appendChild(product)
            }
            var pattern={
                debug:function(){
                    console.log(type+name+"{"+properties[1].join("")+"}")
                },
                name:function(){
                    return name
                },
                promote:function(){
                    promote()
                },
                property:function(name,value){
                    var key=properties[0].indexOf(name)
                    if(key<0){
                        properties[0].unshift(name),
                        properties[1].unshift(name+":"+value+";")
                        key=0;
                    }
                    if(value){
                        properties[1][key]=name+":"+value+";"
                    }
                    return properties[1][key]
                },
                as:{
                    name:function(){
                        type="."
                    },
                    id:function(){
                        type="#"
                    },
                    context:function(){
                        type=""
                    }
                },
                toggle:function(element,clean){
                    proxy(name,element,clean)
                },
                destroy:function(){
                    style.removeChild(product);
                    var key=selectors[0].indexOf(name);
                    selectors[0].splice(key,1),
                    selectors[1].splice(key,1);
                    name=null,delete name,
                    type=null,delete type,
                    properties=null,delete properties,
                    promote=null,delete promote,
                    pattern.name=null,delete pattern.name,
                    pattern.promote=null,delete pattern.promote,
                    pattern.property=null,delete pattern.property,
                    pattern.as=null,delete pattern.as,
                    pattern.toggle=null,delete pattern.toggle,
                    pattern.destroy=null,delete pattern.destroy,
                    pattern=null,delete pattern
                }
            }
            selectors[0].unshift(name),
            selectors[1].unshift(pattern),
            key=0
        }
        return selectors[1][key]
    }
    var stylesheet={
        rule:function(name){
            return create(name)
        },
        debug:function(){
            console.group("--- Stylesheet ---")
            var wall=selectors[1].concat()
            while(wall.length){
                wall.shift().debug()
            }
            console.groupEnd()
        },
        destroy:function(){
            document.head.removeChild(style)
            var wall=selectors[1].concat();
            while(wall.length){
                wall.shift().destroy();
            }
            create=null,delete create,
            selectors=null,delete selectors,
            proxy=null,delete proxy,
            reset=null,delete reset,
            stylesheet.rule=null,delete stylesheet.rule,
            stylesheet.destroy=null,delete stylesheet.destroy,
            stylesheet=null,delete stylesheet,
            style=null,delete style;
        }
    }
    request.stylesheet=function(){
        return stylesheet
    }
    request.action("coordinates")
    request()
})()