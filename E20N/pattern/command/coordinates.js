/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var coordinate=function(head,tail){
        var state=null,width=tail[0]-head[0],
        height=tail[1]-head[1];
        var up=width<0,down=height<0;
        if(up){
            width*=-1;
        }
        if(down){
            height*=-1;
        }
        if(up){
            if(down){
                head[0]=tail[0],
                head[1]=tail[1],
                state="nw";
            }else{
                head[0]=head[0]-width
                state="sw";
            }
        }else{
            if(down){
                head[1]=head[1]-height
                state="ne";
            }else{
                state="se";
            }
        }
        if(state){
            var dup=function(copy){
                var clone={
                    transform:function(at,name){
                        at[name]=clone
                    },
                    clone:function(){
                        return dup(clone)
                    },
                    left:copy.left,
                    top:copy.top,
                    width:copy.width,
                    height:copy.height,
                    state:copy.state,
                    destroy:function(){
                        clone.save=null,delete clone.save,
                        clone.clone=null,delete clone.clone,
                        clone.left=null,delete clone.left,
                        clone.top=null,delete clone.top,
                        clone.width=null,delete clone.width,
                        clone.height=null,delete clone.height,
                        clone.state=null,delete clone.state,
                        clone.destroy=null,delete clone.destroy,
                        clone=null,delete clone
                    }
                }
            }
            var clone={
                transform:function(at,name){
                    at[name]=clone
                },
                clone:function(){
                    dup(clone)
                },
                left:head[0],
                top:head[1],
                width:width,
                height:height,
                state:state,
                destroy:function(){
                    clone.save=null,delete clone.save,
                    clone.left=null,delete clone.left,
                    clone.top=null,delete clone.top,
                    clone.width=null,delete clone.width,
                    clone.height=null,delete clone.height,
                    clone.state=null,delete clone.state,
                    clone.destroy=null,delete clone.destroy,
                    clone=null,delete clone
                }
            } 
            return clone
        }
        return null
    }
    request.coordinate=function(head,tail){
        return coordinate(head,tail)
    }
    request.action("structure")
    request()
})()