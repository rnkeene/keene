/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var context=function(name){
        var element=document.createElement(name);
        var state=false
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup;
                state=false;
                document.body.removeChild(element)
                return state
            }
            state=true;
            document.body.appendChild(element)
            return state
        }
        toggle()
        var clone={
            debug:function(){
                console.group("--- Structure ---")
                console.log("toggled on?",state)
                console.log("<element>",element.tagName)
                console.groupEnd()
            },
            state:function(){
                return state
            },
            instance:function(){
                return element
            },
            toggle:function(){
                return toggle()
            },
            destroy:function(){
                if(state){
                    toggle()
                }
                element=null,delete element,
                state=null,delete state,
                toggle=null,delete toggle,
                clone.state=null,delete clone.state,
                clone.instance=null,delete clone.instance,
                clone.toggle=null,delete clone.toggle,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        return clone
    }
    request.structure=function(name){
        return context(name)
    }
    request.context("pattern/logic")
    request.action("agent")
    request()
})()