/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *  Basae set of closures for everything to use:
 *  
 *  OVERVIEW
 *  
 *  Environment:
 *  
 *      systems=[[],[]]
 *          [0] -- any native subject that can toggle a listener
 *          [1] -- "supplimentary manager" for a subject
 *          
 *          [subject].addEventListener(title,handler,capture);
 *          the supplimentary manager contains:
 *          
 *      titles=[[],[]]
 *          [0] -- any named event belonging to a subject
 *          [1] -- "complimentary manager" for an event
 *          
 *          the "complimentary manager" contains:
 *          
 *      handlers=[[],[]] 
 *          [0] -- any handler for a particular event
 *          [1] -- the state manager for a handler
 *          
 *  Request/Model/Response:
 *  
 *      To move around to different (.js) modules; use the request object.
 *      "request" is a global variable, that is also function;
 *          invoking it will cause a <script> to load.
 *          
 *      There are only two variables: context and action.
 *      
 *      Example, load: <script src="path/to/dynamic.js">
 *          ...
 *          request.context("path/to")
 *          request.action("dynamic)
 *          request()
 *          ...
 */
(function(){
    
    //environment
    var systems=[[],[]]
    var environment=function(subject){
        var key=systems[0].indexOf(subject);
        if(key<0){
            var element=subject,name=null,module=null,capture=null;
            var disable=function(name,module,capture){
                element.removeEventListener(name,module,capture)
            }
            var inverter=function(name,module,capture){
                element.addEventListener(name,module,capture)
            }
            inverter.name=function(update){
                if(update){
                    name=update
                }
                return name
            }
            inverter.module=function(update){
                if(update){
                    module=update
                }
                return module
            }
            inverter.capture=function(update){
                if(update){
                    capture=update
                }
                return capture
            }
            var titles=[[],[]]
            inverter.behavior=function(name){
                var key=systems[0].indexOf(name);
                if(key<0){
                    var title=name;
                    var handlers=[[],[]]
                    var clone={
                        behave:function(handler,remove){
                            var key=handlers[0].indexOf(handler)
                            if(key<0){
                                var eager=true,
                                state=false,
                                name=title,
                                module=handler;
                                var toggle=function(){
                                    var backup=toggle;
                                    toggle=function(){
                                        toggle=backup;
                                        state=false;
                                        disable(name,module,eager)
                                        return state
                                    }
                                    state=true;
                                    inverter(name,module,eager)
                                    return state
                                }
                                var child={
                                    debug:function(){
                                        console.log("[subject].add|removeEventListener(%s,module,%s)",name,eager)
                                        console.log("Current running ??", state)
                                    },
                                    lazy:function(update,value){
                                        if(update){
                                            eager=value
                                            if(state){
                                                toggle(),
                                                toggle()
                                            }
                                        }
                                        return eager
                                    },
                                    state:function(){
                                        return state
                                    },
                                    toggle:function(change,state){
                                        if(change){
                                            if(toggle()==state){
                                                return state
                                            }
                                            return toggle()
                                        }
                                        return toggle()
                                    },
                                    destroy:function(){
                                        if(state){
                                            toggle()
                                        }
                                        var key=handlers[0].indexOf(module),pair=handlers[1].indexOf(child);
                                        handlers[0].splice(key,1)
                                        handlers[1].splice(pair,1)
                                        key=null,delete key,
                                        eager=null,delete eager,
                                        state=null,delete state,
                                        name=null,delete name,
                                        module=null,delete module,
                                        toggle=null,delete toggle,
                                        child.debug=null,delete child.debug,
                                        child.lazy=null,delete child.lazy,
                                        child.state=null,delete child.state,
                                        child.toggle=null,delete child.toggle,
                                        child.destroy=null,delete child.destroy,
                                        child=null,delete child
                                    }
                                }
                                handlers[0].unshift(module),
                                handlers[1].unshift(child),
                                key=0;
                            }else{
                                if(remove){
                                    var proxy=handlers[key];
                                    if(proxy.state()){
                                        proxy.toggle()
                                    }
                                    return handlers.splice(key,1).pop()
                                }
                            }
                            return handlers[1][key]
                        },
                        destroy:function(){
                            var wall=handlers[1].concat();
                            while(wall.length){
                                var empty=wall.shift()
                                if(empty.state()){
                                    empty.toggle()
                                }
                                empty.destroy()
                            }
                            var key=titles[0].indexOf(title),
                            pair=titles[1].indexOf(clone);
                            titles[0].splice(key,1)
                            titles[1].splice(pair,1)
                            title=null,delete title,
                            handlers=null,delete handlers,
                            clone.behave=null,delete clone.behave,
                            clone.destroy=null,delete clone.destroy,
                            clone=null,delete clone
                        }
                    }
                    titles[0].unshift(name),
                    titles[1].unshift(clone),
                    key=0
                }
                return titles[1][key]
            }
            inverter.destroy=function(){
                var key=systems[0].indexOf(element);
                if(key>-1){
                    systems[0].splice(key,1)
                }
                var pair=systems[0].indexOf(inverter)
                if(pair>-1){
                    systems[0].splice(pair,1)
                }
                if(key==pair){
                    element=null,delete element,
                    name=null,delete name,
                    module=null,delete module,
                    capture=null,delete capture,
                    inverter.name=null,delete inverter.name,
                    inverter.module=null,delete inverter.module,
                    inverter.capture=null,delete inverter.capture,
                    inverter.behavior=null,delete inverter.behavior,
                    inverter.destroy=null,delete inverter.destroy,
                    inverter=null,delete inverter
                }
            }
            systems[0].unshift(element),
            systems[1].unshift(inverter),
            key=0;
        }
        return systems[1][key]
    }
    
    
    var forward=function(context,action){
        var script=document.createElement("script");
        script.type="text/javascript";
        script.asynch=false;
        script.src=context+"/"+action+".js";
        document.head.appendChild(script)
        document.head.removeChild(script);
        script=null,delete script;
        document.head.title.innerHTML=context+" > "+action;
    }
    var model=function(){
        var context="pattern/command",
        action="stylesheet",
        next=function(){
            forward(context,action)
        }
        next.debug=function(){
            console.group("--- request ---")
            console.info("context:",context)
            console.info("action:",action)
            console.groupEnd()
        }
        next.context=function(update){
            if(update){
                context=update
            }
            return context
        }
        next.action=function(update){
            if(update){
                action=update
            }
            return action
        }
        next.destroy=function(){
            context=null,delete context,
            action=null,delete action,
            next.context=null,delete next.context,
            next.action=null,delete next.action,
            next.destroy=null,delete next.destroy,
            next=null,delete next
        }
        return next
    }
    var response=function(){
        var origin=model(),
        replay=[],
        history=[[],[]];
        replay.unshift(function(){
            origin()
        })
        var proxy=function(){
            var context=origin.context(),
            action=origin.action();
            var key=history[0].indexOf(context),
            pair=history[1].indexOf(action)
            if(key<0 || pair<0){
                if(key>-1){
                    history[1].unshift(action)
                }else{
                    history[0].unshift(context),
                    history[1].unshift(action)
                }
                replay.unshift(function(){
                    origin.context(context),
                    origin.action(action);
                    origin()
                })
            }
            origin()
        }
        proxy.debug=function(){
            console.group("===  Current Response ===")
            origin.debug()
            console.log("@Replay Depth (e.g. step #):",replay.length)
            console.group("History:")
            var wall=history.concat();
            var contexts=wall[0].concat()
            console.group("Contexts")
            while(contexts.length){
                console.log(contexts.shift())
            }
            console.groupEnd()
            var actions=wall[1].concat()
            console.group("Actions")
            while(actions.length){
                console.log(actions.shift())
            }
            console.groupEnd()
            console.groupEnd()
            console.groupEnd()
        }
        proxy.context=function(update){
            return origin.context(update)
        }
        proxy.action=function(update){
            return origin.action(update)
        }
        proxy.history=function(){
            return history.concat()
        }
        proxy.replay=function(){
            return replay.concat()
        },
        proxy.destroy=function(){
            origin.destroy()
            replay=null,delete replay,
            history=null,delete history,
            proxy.debug=null,delete proxy.debug,
            proxy.context=null,delete proxy.context,
            proxy.action=null,delete proxy.action,
            proxy.history=null,delete proxy.history,
            proxy.replay=null,delete proxy.replay,
            proxy.destroy=null,delete proxy.destroy,
            proxy=null,delete proxy
        }
        return proxy
    }
    var kernel=function(){
        loader.destroy();
        proxy=null,delete proxy,
        loader=null,delete loader,
        kernel=null,delete kernel;
        request=response();
        request.controller=function(){
            return controller
        }
        request.subject=function(subject){
            return environment(subject)
        }
        var destroy=request.destroy;
        request.destroy=function(){
            controller.destroy();
            controller=null,delete controller;
            request.controller=null,delete request.controller,
            request.subject=null,delete request.subject,
            destroy();
            request=null,delete request
        }
        request()
    }
    var controller=environment(window);
    var loader=controller.behavior("load")
    var proxy=loader.behave(kernel);
    proxy.toggle()
})()