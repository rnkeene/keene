/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var shape=window.part;
    window.part=null,delete window.part;
    var global=request();
    var arena=global.arena();
    var agent=global.agent();
    var title=arena.name;
    arena.name=null,delete arena.name;
    
    var origin=[window.innerWidth,window.innerHeight],
    dimension=[shape.width,shape.height],
    offset=[parseInt(dimension[0]/2),parseInt(dimension[1]/2)],
    position=[shape.left,shape.top],
    scale=[0,0],
    state=shape.state;
    
    shape.destroy();
    shape=null,delete shape;
    
    var selector=agent.condition(title);
    selector.property("cursor","help");
    selector.property("position","absolute");
    selector.property("left",position[0]+"px");
    selector.property("top",position[1]+"px");
    
    selector.promote()
    var reset=function(){
        origin=[window.innerWidth,window.innerHeight],
        offset=[parseInt(dimension[0]/2),parseInt(dimension[1]/2)],
        selector.property("left",position[0]+"px");
        selector.property("top",position[1]+"px");
        selector.promote()
    }
    var resize=function(){
        var px=window.innerWidth/origin[0],py=window.innerHeight/origin[1];
        var delta=false;
        if(px!=1){
            position[0]=parseInt(position[0]*px);
            dimension[0]=parseInt(dimension[0]*px);
            if(dimension[0]<1){
                dimension[0]=1
            }
            scale[0]=px
            delta=true
        }
        if(py!=1){
            position[1]=parseInt(position[1]*py);
            dimension[1]=parseInt(dimension[1]*py);
            if(dimension[1]<1){
                dimension[1]=1
            }
            scale[1]=py;
            delta=true
        }
        if(delta){
            reset()
        }
    }
    
    var subject=agent.proxy(window).subject();
    var proxy=subject.behavior("resize");
    var behave=proxy.behave(resize)
    behave.toggle()
    
    var space={
        debug:function(){
            console.group("--- Space %s ---",title)
            console.log("state:",state)
            console.log("origin:",origin)
            console.log("dimensions:",dimension)
            console.log("position:",position)
            console.log("offset:",offset)
            console.log("scale:",scale)
            console.group("<style>")
            selector.debug()
            console.groupEnd()
            console.groupEnd()
        },
        name:function(){
            return title
        },
        state:function(){
            return state
        },
        rule:function(){
            return selector
        },
        origin:function(){
            return origin.concat()
        },
        dimensions:function(x,y){
            var change=false;
            if(x){
                dimension[0]=x,
                change=true
            }
            if(y){
                dimension[1]=y,
                change=true
            }
            if(change){
                offset=[parseInt(dimension[0]/2),parseInt(dimension[1]/2)]
            }
            return dimension.concat()
        },
        position:function(x,y){
            var change=false;
            if(x){
                position[0]=x
                selector.property("left",position[0]+"px");
                change=true
            }
            if(y){
                position[1]=y
                selector.property("top",position[1]+"px");
                change=true
            }
            if(change){
                selector.promote()
            }
            return position.concat()
        },
        offset:function(){
            return offset.concat()
        },
        scale:function(){
            return scale.concat()
        },
        destroy:function(){
            behave.destroy()
            selector.destroy()
            origin=null,delete origin,
            dimension=null,delete dimension,
            position=null,delete position,
            offset=null,delete offset,
            scale=null,delete scale,
            selector=null,delete selector,
            title=null,delete title,
            shape=null,delete shape,
            reset=null,delete reset,
            resize=null,delete resize,
            subject=null,delete subject,
            proxy=null,delete proxy,
            behave=null,delete behave,
            space.rule=null,delete space.rule,
            space.origin=null,delete space.origin,
            space.dimension=null,delete space.dimension,
            space.position=null,delete space.position,
            space.offset=null,delete space.offset,
            space.scale=null,delete space.scale,
            space.destroy=null,delete space.destroy,
            space=null,delete space
        }
    }
    arena.relative=function(){
        return space
    }
    agent.intercept("shape")
})()