/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var global=request();
    var arena=global.arena();
    var agent=global.agent();
    var active=null,contrast=null;
    var key={
        nw:function(){
            active="rgb(255,0,0)"
            return active
        },
        ne:function(){
            active="rgb(0,255,0)"
            return active
        },
        se:function(){
            active="rgb(0,0,255)"
            return active
        },
        sw:function(){
            active="rgb(255,255,0)"
            return active
        }
    }
    var opposite={
        se:function(){
            contrast="rgb(255,0,0)"
            return contrast
        },
        sw:function(){
            contrast="rgb(0,255,0)"
            return contrast
        },
        nw:function(){
            contrast="rgb(0,0,255)"
            return contrast
        },
        ne:function(){
            contrast="rgb(255,255,0)"
        }
    }
    var space=arena.space();
    arena.space=null,delete arena.space;
    var shape=space.relative();
    var gradient=space.gradient();
    var structure=space.structure();
    var canvas=structure.instance();
    var context=canvas.getContext("2d")
    var state=shape.state();
    var background="rgb(0,0,0)";
    var dimensions=shape.dimensions();
    var offset=shape.offset();
    key[state]();
    opposite[state]();
    
    var refresh=function(){
        dimensions=shape.dimensions(),
        offset=shape.offset()
    }
    
    var resize=document.createEvent("Event");
    resize.initEvent(shape.name()+Date.now(),true,true)
    var update=function(){
        var backup=dimensions;
        refresh()
        if(backup[0]!=dimensions[0] || backup[1]!=dimensions[1]){
            document.dispatchEvent(resize);
        }
    }
    var subject=agent.proxy(window).subject();
    var proxy=subject.behavior("resize");
    var behave=proxy.behave(update)
    behave.toggle()
    var register=function(notify){
        document.addEventListener(resize.type,notify,true)
    }
    var notify=function(){
        document.dispatchEvent(resize)
    }
    var scheme=function(update){
        if(update){
            state=update;
            key[state](),
            opposite[state]()
        }
        return state
    }
    var base=function(update){
        if(update){
            background=update
        }
        return background
    }
    var square=function(){
        var linear=gradient.linear[state]();
        linear.addColorStop(0, background);
        linear.addColorStop(.5, active);
        linear.addColorStop(1, background);
        var draw=function(){
            context.save();
            context.fillStyle=linear;
            context.fillRect(0, 0, dimensions[0], dimensions[1]);
            context.restore();
        }
        register(draw);
        return draw
    }
    
    var sign=function(){
        var text=gradient.linear[state]();
        text.addColorStop(0, contrast);
        text.addColorStop(.5, background);
        text.addColorStop(1, contrast);
        var draw=function(){
            context.save();
            context.fillStyle=text;
            context.translate(offset[0],offset[1])
            context.font = parseInt(dimensions[1]*.8)+"px sans-serif";
            context.textAlign = "center"; 
            context.textBaseline = "middle";
            context.fillText(shape.name(),0,parseInt(offset[1]*.1));
            context.restore();
        }
        register(draw);
        return draw
    }
    var sphere=function(){
        var radial=gradient.radial()
        radial.addColorStop(0, background);
        radial.addColorStop(.5, active);
        radial.addColorStop(1, background);
        var draw=function(){
            context.save();
            context.fillStyle=radial;
            context.fillRect(0, 0, dimensions[0], dimensions[1]);
            context.restore();
        }
        register(draw);
        return draw
    }
    var scribe={
        debug:function(){
            console.group("--- Color ---")
            console.log("State:",state)
            console.log("Active:",active)
            console.log("Background:",background)
            space.debug()
            console.groupEnd()
        },
        space:function(){
            return space
        },
        refresh:function(){
            update()
        },
        active:function(update){
            if(update){
                active=update
            }
            return active
        },
        contrast:function(update){
            if(update){
                contrast=update
            }
            return contrast
        },
        state:function(update){
            return scheme(update)
        },
        background:function(update){
            return base(update)
        },
        square:function(){
            return square()
        },
        sign:function(){
            return sign()
        },
        round:function(){
            return sphere()
        }
    }
    arena.scribe=function(){
        return scribe
    }
    agent.intercept("subject","pattern/transform")
})()