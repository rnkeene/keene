/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var global=request();
    var arena=global.arena();
    var agent=global.agent();
    var relative=arena.relative();
    arena.relative=null,delete arena.relative;
    var structure=agent.produce("canvas");
    
    var canvas=structure.instance();
    var size=relative.dimensions()
    canvas.width=size[0],
    canvas.height=size[1];
    
    relative.rule().toggle(canvas)
    var context=canvas.getContext("2d");
    var gradient={
        radial:function(){
            var offset=relative.offset();
            var radius=0;
            offset[1]>offset[2]?radius=offset[2]:radius=offset[1];
            return context.createRadialGradient(offset[0],offset[1],0,offset[0],offset[1],radius)
        },
        linear:{
            nw:function(){
                return context.createLinearGradient(0, canvas.height,canvas.width, 0)  
            },
            ne:function(){
                return context.createLinearGradient(canvas.width,canvas.height,0, 0)
            },
            se:function(){
                return context.createLinearGradient(canvas.width, 0, 0, canvas.height)
            },
            sw:function(){
                return context.createLinearGradient(0, 0, canvas.width, canvas.height)
            }
        },
        destroy:function(){
            gradient.radial=null,delete gradient.radial,
            gradient.linear=null,delete gradient.linear,
            gradient.destroy=null,delete gradient.destroy,
            gradient=null,delete gradient
        }
    }
    var resize=function(){
        size=relative.dimensions();
        if(size[0]!=canvas.width){
            canvas.width=size[0]
        }
        if(size[1]!=canvas.height){
            canvas.height=size[1];
        }
    }
    var subject=agent.proxy(window).subject();
    var proxy=subject.behavior("resize");
    var behave=proxy.behave(resize)
    behave.toggle()
    
    var shape={
        debug:function(){
            console.group("--- SHAPE ---")
            structure.debug()
            relative.debug()
            console.groupEnd()
        },
        relative:function(){
            return relative
        },
        structure:function(){
            return structure
        },
        gradient:function(){
            return gradient
        },
        destroy:function(){
            relative.destroy();
            behave.destroy();
            gradient.destroy();
            structure.destroy();
            subject=null,delete subject,
            proxy=null,delete proxy,
            relative=null,delete relative,
            behave=null,delete behave,
            resize=null,delete resize,
            structure=null,delete structure,
            canvas=null,delete canvas,
            context=null,delete context,
            shape.relative=null,delete shape.relative,
            shape.structure=null,delete shape.structure,
            shape.gradient=null,delete shape.gradient,
            shape.destroy=null,delete shape.destroy,
            shape=null,delete shape
        }
    }
    arena.space=function(){
        return shape
    }
    agent.intercept("color")
})()