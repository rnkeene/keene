/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var shape=window.part;
    var global=request();
    var agent=global.agent();
    var arena=global.arena();
    var structure=agent.produce("input");
    var input=structure.instance()
    var screen=agent.produce("input");
    var official=screen.instance()
    official.disabled=true;
    
    
    
    var style=agent.condition("title");
    style.property("position","absolute")
    style.property("left",shape.left+"px")
    style.property("top",shape.top+"px")
    style.property("width",shape.width+"px")
    style.property("height",shape.height+"px")
    style.property("font-size",parseInt(shape.height*.8)+"px")
    style.promote()
    style.toggle(official)
    style.toggle(input)
    
    var hide=agent.condition("hidden");
    hide.property("z-index","-1")
    hide.promote()
    hide.toggle(input)
    input.focus();
    
    var press=function(event){
        var value=(event.charCode || event.keyCode);
        switch(value){
            case 13:
                console.warn("pressed enter -- submit")
                submit();
                return
            case 27:
                console.warn("pressed escape -- submit")
                submit();
                return
            case 32:
                console.warn("space pressed -- submit")
                submit()
                return
            default:
                window.setTimeout(function(){
                    official.value=input.value.replace(/[0-9]/g, '');
                },0)
        }
    }
    var onchange=function(){
        submit()
    }
    var submit=function(){
        input.removeEventListener("blur",submit,true),
        input.removeEventListener("keydown",press,true);
        input.removeEventListener("change",onchange,true);
        structure.toggle();
        if(official.value.length>0){
            arena.name=official.value;
            agent.intercept("relative","pattern/state")
        }
        structure.destroy();
        style.destroy();
        screen.destroy();
        hide.destroy();
    }
    input.addEventListener("change",onchange,true)
    input.addEventListener("keydown",press,true)
    
})()