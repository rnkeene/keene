/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 *  
 */
(function(){
    var global=request()
    var arena=global.arena()
    var agent=global.agent()
    var current=arena.scribe()
    var space=current.space();
    var child=space.structure()
    var logic=agent.proxy(child)
    var manager=logic.stack()
    var loader=manager.instance(function(){
        toggle()
    })
    var stop=function(){
        arena.release()
    }
    var state=false;
    var toggle=function(){
        var backup=toggle;
        toggle=function(){
            toggle=backup
            state=false;
            mup.toggle(true,false)
            mdown.toggle(true,false)
            return state
        }
        state=true;
        mdown.toggle(true,true)
        mup.toggle(true,true)
        return state
    }
    var start=function(){
        var state=false;
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup
                proxy.toggle()
                cursor.toggle()
                state=false
                return state
            }
            proxy.toggle()
            cursor.toggle()
            state=true
            return state;
        }
        var clone={
            state:function(){
                return state
            },
            toggle:function(){
                toggle()
            },
            destroy:function(){
                state=null,delete state,
                toggle=null,delete toggle,
                stub=null,delete stub,
                clone.state=null,delete clone.state,
                clone.toggle=null,delete clone.toggle,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        var stub={
            instance:function(){
                return clone
            },
            destroy:function(){
                clone.destroy();
                stub.instance=null,delete stub.instance,
                stub.destroy=null,delete stub.destroy,
                stub=null,delete stub
            }
        }
        arena.capture(stub)
    }
    
    var mdown=loader.concurrent("mouseover",start)
    var mup=loader.concurrent("mouseout",stop)
    var destroy=manager.destroy
    manager.destroy=function(){
        destroy()
        mdown.destroy();
        mup.destroy();
        loader.destroy();
        start=null,delete start,
        stop=null,delete stop,
        toggle=null,delete toggle,
        loader=null,delete loader,
        mdown=null,delete mdown,
        mup=null,delete mup,
        destroy=null,delete destroy
    }
    
    var subject=logic.subject();
    var when=subject.behavior("mousedown");
    var during=subject.behavior("mousemove");
    var done=subject.behavior("mouseup");
    
    var place=space.relative();
    var position=place.position();
    var dimensions=place.dimensions();
    var range=[parseInt(dimensions[0]*.1),parseInt(dimensions[1]*.1)]
    var style=agent.condition(place.name())
    var head=[0,0]
    var update=false;
    var proxy=when.behave(function(event){
        head=[event.clientX, event.clientY];
        console.log("Down At:", head)
        update=true;
        cursor.toggle()
        end.toggle()
        if(sizing){
            console.log("Click on manual resize area")
            return
        }
        console.log("Click -OR - Starting Drag on Shape")
        moving.toggle()
    })
    var moved=false;
    var previous=null
    var moving=during.behave(function(event){
        if(!previous){
            previous=head
        }
        var x=previous[0]-event.clientX
        var y=previous[1]-event.clientY
        previous=[event.clientX,event.clientY]
        var update=false;
        if(x!=0){
            position[0]-=x;
            update=true
        }
        if(y!=0){
            position[1]-=y
            update=true
        }
        if(update){
            position=place.position(position[0],position[1])
        }
        style.property("cursor","move")
        style.promote()
        moved=true
    })
    var sizing=false;
    var stated=""
    var cursor=during.behave(function(event){
        var run=event.clientX-position[0],
        rise=event.clientY-position[1]
        var change=false, restate=stated;
        if(run<range[0]){
            rise<range[1]?style.property("cursor","nw-resize") && (stated="nw"):
            rise+range[1]>dimensions[1]?style.property("cursor","sw-resize") && (stated="sw"):
            style.property("cursor","w-resize") && (stated="w")
            change=true
        }else if(rise<range[1]){
            run+range[0]>dimensions[0]?style.property("cursor","ne-resize") && (stated="ne"):
            style.property("cursor","n-resize") && (stated="n");
            change=true
        }else if(run+range[0]>dimensions[0]){
            rise+range[1]>dimensions[1]?style.property("cursor","se-resize") && (stated="se"):
            style.property("cursor","e-resize") && (stated="e")
            change=true
        }else if(rise+range[1]>dimensions[1]){
            style.property("cursor","s-resize") && (stated="s");
            change=true
        }
        if(restate!=stated && sizing){
            style.promote()
        }
        if(change && !sizing){
            style.promote()
            sizing=true
        }
        if(!change && sizing){
            style.property("cursor","help")
            style.promote()
            sizing=false
        }
    })
    var end=done.behave(function(event){
        if(head[0]!=event.clientX || head[1]!=event.clientY){
            if(sizing){
                console.log("RESIZE")
            }else{
                console.info("MOVED!")
                style.property("cursor","help")
                style.promote()
            }
        }else{
            if(!sizing){
                console.log("A CLICK! (rename)")
            }
        }
        if(moved){
            moving.toggle()
        }
        moved=false;
        finish()
    })
    var finish=function(){
        if(update){
            update=false;
            end.toggle()
            cursor.toggle()
        }
    }
    var swap=function(){
        var backup=swap;
        swap=function(){
            swap=backup;
            current.round()()
        }
        current.square()()
        current.sign()()
    }
    swap()
    arena.client(manager)
})()