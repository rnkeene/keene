/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var instance=request.instance();
    var activate=function(target,name,module){
        var element=target,type=name,handler=module,state=false;
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup;
                state=false
                element.removeEventListener(type,handler,true)
                return state
            }
            state=true
            element.addEventListener(type,handler,true)
            return state
        }
        toggle()
        var clone={
            state:function(){
                return state
            },
            toggle:function(){
                toggle()
            },
            destroy:function(){
                if(state){
                    toggle()
                }
                element=null,delete element,
                type=null,delete type,
                handler=null,delete handler,
                state=null,delete state,
                toggle=null,delete toggle,
                clone.state=null,delete clone.state,
                clone.toggle=null,delete clone.toggle,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        return clone
    }
    
    var behaviors=[[],[]];
    var register=function(named){
        var key=behaviors[0].indexOf(named);
        if(key<0){
            var name=named,modules=[[],[]],target=window;
            var clone={
                debug:function(){
                    console.log("Behavior @",name)
                    console.log("   registered:",modules[0])
                },
                decorate:function(module){
                    var key=modules[0].indexOf(module)
                    if(key>-1){
                        modules[0].splice(key,1);
                        var transform=modules[1].splice(key,1).pop();
                        transform.destroy();
                        console.log("Removed Old Instance")
                    }
                    modules[0].unshift(module),
                    modules[1].unshift(activate(target,name,module))
                    return modules[1][0]
                },
                at:{
                    window:function(){
                        target=window
                    },
                    document:function(){
                        target=document
                    },
                    proxy:function(element){
                        target=element
                    }
                }
            }
            behaviors[0].unshift(name),
            behaviors[1].unshift(clone),
            key=0
        }
        return behaviors[1][key]
    }
    instance.behavior=function(name,structure){
        return register(name,structure)
    }
    instance.response.request("body")
})()