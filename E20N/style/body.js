/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var instance=request.instance();
    var stylesheet=instance.style();
    var selector=stylesheet.rule("static");
    selector.declare("background-color","#FF0000")
    selector.declare("position","static");
    selector.declare("overflow","hidden");
    selector.declare("margin","0px");
    selector.declare("padding","0px");
    stylesheet.apply(selector)
    
    var style=stylesheet.rule("limit");
    style.declare("width",window.innerWidth+"px")
    style.declare("height",window.innerHeight+"px")
    stylesheet.apply(style)
    
    var resize=function(){
        console.log("Resize Decorator --- @ROOT")
        style.declare("width",window.innerWidth+"px")
        style.declare("height",window.innerHeight+"px")
        stylesheet.alter(style,0,0)
        stylesheet.alter(style,1,0)
    }
    var limit=instance.behavior("resize")
    limit.decorate(resize)
    
    selector.proxy(document.body)
    style.proxy(document.body)
    
    instance.response.request("container","load")
})()