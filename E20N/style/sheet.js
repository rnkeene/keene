/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var instance=request.instance();
    var element=instance.style();
    var rule=function(selector,values){
        var selected=0;
        var name=selector;
        var options=values;
        var clone={
            debug:function(){
                console.groupCollapsed(clone.promote())
                console.log("{%o}@[%i]",options,selected)
                console.groupEnd()
            },
            selected:function(){
                return selected
            },
            rule:function(){
                return name
            },
            options:function(){
                return options
            },
            select:{
                active:function(number){
                    if(number){
                        selected=number
                    }
                },
                activate:function(value){
                    if(value){
                        selected=options.indexOf(value)
                    }
                }
            },
            promote:function(){
                return name+":"+options[selected]+";"
            },
            destroy:function(){
                selected=null,delete selected,
                name=null,delete name,
                options=null,delete options,
                clone.debug=null,delete clone.debug,
                clone.selected=null,delete clone.selected,
                clone.rule=null,delete clone.rule,
                clone.options=null,delete clone.options,
                clone.select.active=null,delete clone.select.active,
                clone.select.activate=null,delete clone.select.activate,
                clone.select=null,delete clone.select,
                clone.promote=null,delete clone.promote,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        return clone
    }
    var properties=function(){
        var declarations=[[],[]];
        var declare=function(property,value){
            var key=declarations[0].indexOf(property)
            if(key<0){
                declarations[0].unshift(property),
                declarations[1].unshift([]),
                key=0
            }
            var at=declarations[1][key].indexOf(value)
            if(at<0){
                declarations[1][key].unshift(value)
            }
        }
        var remove=function(property,value){
            var key=declarations[0].indexOf(property)
            if(key<0){
                return
            }
            if(value){
                declarations[1].splice(declarations[1].indexOf(value),1)
                return
            }
            declarations[0].splice(key,1),
            declarations[1].splice(key,1)
        }
        var promote=function(){
            var wall=declarations[0].concat();
            var data=declarations[1].concat();
            var product=[]
            while(wall.length){
                product.unshift(rule(wall.shift(),data.shift().concat()))
            }
            return product
        }
        var proxy={
            debug:function(){
                var selector=promote();
                while(selector.length){
                    selector.shift().debug()
                }
            },
            declare:function(property,value){
                declare(property,value)
            },
            remove:function(property,value){
                remove(property,value)
            },
            promote:function(){
                return promote()
            },
            destroy:function(){
                declarations=null,delete declarations,
                declare=null,delete declare,
                remove=null,delete remove,
                promote=null,delete promote,
                proxy.debug=null,delete proxy.debug,
                proxy.declare=null,delete proxy.declare,
                proxy.remove=null,delete proxy.remove,
                proxy.promote=null,delete proxy.promote,
                proxy.destroy=null,delete proxy.destroy,
                proxy=null,delete proxy
            }
        }
        return proxy
    }
    var selector=function(name){
        var local=properties(),
        title=name,
        type=".";
        var verify=function(properties){
            var results=[];
            while(properties.length){
                var rule=properties.shift();
                results.unshift(rule.promote());
                rule.destroy()
            }
            return document.createTextNode(type+title+"{"+results.join("")+"}")
        }
        var quick=function(){
            return verify(local.promote())
        }
        var proxy=function(element,clean){
            if(clean){
                element.className.replace(" "+title,"")
                element.className.replace(title,"")
                return
            }
            if(element.className.indexOf(title)>-1){
                return
            }
            if(element.className.length){
                element.className+=" "
            }
            element.className+=title
        }
        var clone={
            proxy:function(element,clean){
                proxy(element,clean)
            },
            debug:function(){
                console.groupCollapsed("%s%s{...}",type,title)
                local.debug()
                console.groupEnd()
            },
            promote:function(){
                return local.promote()
            },
            verify:function(properties){
                return verify(properties)
            },
            trusted:function(){
                return quick()
            },
            declare:function(property,value){
                local.declare(property,value)
            },
            remove:function(name,value){
                local.remove(name,value)
            },
            as:{
                name:function(){
                    type="."
                },
                id:function(){
                    type="#"
                },
                context:function(){
                    type=""
                }
            },
            destroy:function(){
                local.destroy();
                local=null,delete local,
                title=null,delete title,
                type=null,delete type,
                clone.rule=null,delete clone.rule,
                clone.promote=null,delete clone.promote,
                clone.declare=null,delete clone.declare,
                clone.as=null,delete clone.as,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone
            }
        }
        return clone
    }
    
    var alter=function(selector,property,selected){
        var promote=selector.promote()
        promote[property].select.active(selected);
        selector.undo();
        apply(selector,selector.verify(promote))
    }
    
    var apply=function(selector,local){
        var product=local;
        if(!product){
            product=selector.trusted()
        }
        var restore=selector.undo;
        selector.undo=function(){
            selector.undo=restore;
            element.removeChild(product);
        }
        var destroy=selector.destroy;
        selector.destroy=function(){
            while(selector.undo){
                selector.undo()
            }
            destroy()
        }
        element.appendChild(product)
    }
    var selectors=[[],[]];
    var create=function(local){
        var name=local;
        if(!name){
            name="at"+Date.now();
        }
        var key=selectors[0].indexOf(name);
        if(key<0){
            selectors[0].unshift(name)
            var pattern=selector(name);
            pattern.name=function(){
                return name
            }
            pattern.stylesheet=function(){
                return stylesheet
            }
            var debug=pattern.debug;
            var report=instance.debug;
            instance.debug=function(){
                console.group("<style> Selector")
                debug()
                report()
                console.groupEnd()
            }
            pattern.debug=function(){
                instance.debug()
            }
            var destroy=pattern.destroy;
            var undo=instance.destroy;
            pattern.destroy=function(){
                console.group("Override Destroy @Context!")
                var key=selectors[0].indexOf(name);
                selectors[0].splice(key,1)
                selectors[1].splice(key,1)
                destroy()
                instance.debug=report;
                instance.destroy=undo;
                pattern=null,delete pattern,
                report=null,delete report,
                debug=null,delete debug,
                undo=null,delete undo;
                console.groupEnd()
            }
            instance.destroy=function(){
                console.group("---- Selector Nested Destroy ---- ")
                pattern.destroy();
                undo()
                console.groupEnd()
            }
            selectors[1].unshift(pattern)
            key=0;
        }
        return selectors[1][key]
    }
    var coordinate=function(head,tail){
        var state=null,width=tail[0]-head[0],
        height=tail[1]-head[1];
        var up=width<0,down=height<0;
        if(up){
            width*=-1;
        }
        if(down){
            height*=-1;
        }
        if(up){
            if(down){
                var temp=[tail[0],tail[1]]
                tail[0]=head[0],
                tail[1]=head[1],
                head[0]=temp[0],
                head[1]=temp[1],
                state="nw";
            }else{
                head[0]=head[0]-width,
                tail[0]=tail[0]+width,
                state="sw";
            }
        }else{
            if(down){
                head[1]=head[1]-height,
                tail[1]=tail[1]+height,
                state="ne";
            }else{
                state="se";
            }
        }
        if(state){
            var leaf=function(){
                var clone={
                    head:head,
                    tail:tail,
                    width:width,
                    height:height,
                    state:state,
                    destroy:function(){
                        clone.head=null,delete clone.head,
                        clone.tail=null,delete clone.tail,
                        clone.width=null,delete clone.width,
                        clone.height=null,delete clone.height,
                        clone.state=null,delete clone.state,
                        clone.destroy=null,delete clone.destroy,
                        clone=null,delete clone
                    }
                } 
                return clone
            }
            var tree={
                instance:function(){
                    return leaf()
                },
                destroy:function(){
                    tree.instance=null,delete tree.instance,
                    tree.destroy=null,delete tree.destroy,
                    tree=null,delete tree;
                    leaf=null,delete leaf,
                    state=null,delete state,
                    width=null,delete width,
                    up=null,delete up,
                    down=null,delete down
                }
            }
            return tree
        }
        return null
    }
    var stylesheet={
        data:function(){
            return selectors.concat()
        },
        selectors:function(){
            return selectors[0].concat()
        },
        declarations:function(){
            return selectors[1].concat()
        },
        coordinate:function(head,tail){
            return coordinate(head,tail)
        },
        alter:function(selector,property,selected){
            alter(selector,property,selected)
        },
        apply:function(selector,local){
            apply(selector,local)
        },
        rule:function(name){
            return create(name)
        }
    }
    var destroy=instance.destroy;
    instance.destroy=function(){
        console.group("Destroy Stylesheet Builder")
        element=null,delete element,
        rule=null,delete rule,
        properties=null,delete properties,
        selector=null,delete selector,
        selectors=null,delete selectors,
        alter=null,delete alter,
        apply=null,delete apply,
        create=null,delete create,
        stylesheet.data=null,delete stylesheet.data,
        stylesheet.selectors=null,delete stylesheet.selectors,
        stylesheet.declarations=null,delete stylesheet.declarations,
        stylesheet.alter=null,delete stylesheet.alter,
        stylesheet.apply=null,delete stylesheet.apply,
        stylesheet.rule=null,delete stylesheet.rule,
        stylesheet.destroy=null,delete stylesheet.destroy,
        stylesheet=null,delete stylesheet;
        destroy();
        console.groupEnd()
    }
    stylesheet.destroy=function(){
        instance.destroy()
    }
    instance.style=function(){
        return stylesheet
    }
    instance.response.request("universe")
})()