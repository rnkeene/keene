/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var reflect={
        nw:function(head,walls){
            var x=false,y=false;
            if(head[0] < walls[0][0]){
                x=true
            }
            if(head[1] < walls[0][1]){
                y=true
            }
            if(x && y){
                return "se"
            }else if(x){
                return "ne"
            }else if(y){
                return "sw"
            }
            return "nw"
        },
        ne:function(head,walls,width){
            var x=false,y=false;
            if(head[0] + width > walls[1][0]){
                x=true
            }
            if(head[1] < walls[0][1]){
                y=true
            }
            if(x && y){
                return "sw";
            }else if(x){
                return "nw";
            }else if(y){
                return "se";
            }
            return "ne"
        },
        se:function(head,walls,width,height){
            var x=false,y=false;
            if(head[0] + width > walls[1][0]){
                x=true;
            }
            if(head[1] + height > walls[1][1]){
                y=true;
            }
            if(x && y){
                return "nw";
            }else if(x){
                return "sw";
            }else if(y){
                return "ne";
            }
            return "se"
        },
        sw:function(head,walls,ignore,height){
            var x=false,y=false;
            if(head[0] < walls[0][0]){
                x=true
            }
            if(head[1] + height > walls[1][1]){
                y=true
            }
            if(x && y){
                return "ne";
            }else if(x){
                return "se";
            }else if(y){
                return "nw";
            }
            return "sw"
        }
    }
    var fundamental={
        nw:function(position,scale){
            position[0]-=scale,position[1]-=scale
        },
        ne:function(position,scale){
            position[0]+=scale,position[1]-=scale
        },
        se:function(position,scale){
            position[0]+=scale,position[1]+=scale
        },
        sw:function(position,scale){
            position[0]-=scale,position[1]+=scale
        }
    }
    Arena.motion=function(gui,magnitude,frequency){
        console.log("Arena Motion!")
        var shape=gui;
        var vector=shape.vector();
        var scale=magnitude,rate=frequency;
        var offset=function(){
            var event=fundamental[vector.state];
            return function(current){
                event(current,scale)
            }
        }
        var confirm=function(){
            var origin=vector.state;
            vector.state=reflect[origin](vector.head,vector.walls,vector.width,vector.height);
            return (origin!=vector.state)
        }
        var time=function(){
            delta(vector.head),
            delta(vector.tail);
            gui.re.position();
        }
        var delta=offset();
        var motion={
            shape:function(){
                return shape
            },
            vector:function(){
                return vector
            },
            magnitude:function(at){
                if(at){
                    scale=at
                }
                return scale
            },
            frequency:function(at){
                if(at){
                    rate=at
                }
                return rate
            },
            re:{
                shape:function(replace){
                    if(replace){
                        shape=replace;
                        vector=shape.vector();
                    }
                },
                place:function(replace){
                    if(replace){
                        shape.re.place(replace)
                    }
                    vector=shape.vector()
                }
            },
            toggle:function(){
                var backup=motion.toggle;
                motion.toggle=function(cache){
                    console.warn("Clear Interval",motion.toggle);
                    window.clearInterval(clear);
                    console.groupEnd()
                    motion.toggle=backup;
                    console.info("Clean! Ready to Restart")
                }
                console.group("Start Interval")
                var clear=window.setInterval(function(cache){
                    if(confirm()){
                        motion.toggle(cache);
                        delta=offset();
                        gui.re.load();
                        motion.toggle(cache);
                    }
                    time()
                },rate,cache)
            }
        }
        var destroy=gui.destroy;
        gui.destroy=function(){
            console.log("DESTROY Motion");
            destroy();
            shape=null,delete shape,
            vector=null,delete vector,
            scale=null,delete scale,
            rate=null,delete rate,
            offset=null,delete offset,
            confirm=null,delete confirm,
            time=null,delete time,
            delta=null,delete delta,
            motion.shape=null,delete motion.toggle,
            motion.vector=null,delete motion.vector,
            motion.magnitude=null,delete motion.magnitude,
            motion.frequency=null,delete motion.frequency,
            motion.re.shape=null,delete motion.re.shape,
            motion.re.place=null,delete motion.re.place,
            motion.toggle=null,delete motion.toggle,
            motion.destroy=null,delete motion.destroy,
            motion.debug=null,delete motion.debug,
            motion=null,delete motion;
        }
        motion.destroy=function(){
            gui.destroy()
        }
        var debug=gui.debug;
        gui.debug=function(){
            console.groupCollapsed("@Interval,@Amount")
            console.log("Magnitude (amount):",motion.magnitude())
            console.log("Frequency (interval):",motion.frequency())
            debug();
            console.groupEnd()
        }
        motion.debug=function(){
            gui.debug()
        }
        GNA.force(motion)
    }
})()