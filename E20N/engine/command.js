/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var counter=0;
    var encounter=function(proxy){
        var engine=proxy;
        console.log("New Encounter (proxy)",engine)
        var gui=engine.shape();
        var motion=engine.move();
        var vector=engine.vector();
        var canvas=engine.canvas();
        console.log("Loaded shape, vector, & canvas")
        var head=[],tail=[];
        var drag=function(event){
            canvas.removeEventListener("mousedown",drag,true),
            window.addEventListener("mousemove",move,true),
            window.addEventListener("mouseup",click,true),
            head=[event.clientX,event.clientY];
        }
        var drop=function(){
            window.removeEventListener("mousemove",quick,true),
            window.removeEventListener("mouseup",drop,true),
            canvas.addEventListener("mousedown",drag,true);
        }
        var click=function(){
            window.removeEventListener("mousemove",move,true),
            window.removeEventListener("mouseup",click,true);
            console.log("A Click! -- PAUSE!",motion)
            motion.toggle();
            gui.rename(function(){
                motion.toggle()
            })
            canvas.addEventListener("mousedown",drag,true);
        }
        var move=function(event){
            window.removeEventListener("mousemove",move,true)
            window.removeEventListener("mouseup",click,true)
            window.addEventListener("mouseup",drop,true)
            window.addEventListener("mousemove",quick,true)
            tail=[event.clientX,event.clientY]
        }
        var quick=function(event){
            var delta=[tail[0]-head[0],tail[1]-head[1]]
            vector.head[0]+=delta[0],
            vector.head[1]+=delta[1],
            vector.tail[0]+=delta[0],
            vector.tail[1]+=delta[1],
            head=[tail[0],tail[1]],
            tail=[event.clientX,event.clientY];
            gui.re.position();
        }
        canvas.addEventListener("mousedown",drag,true);
        document.body.appendChild(canvas);
        var clone={
            destroy:function(){
                vector=null,delete vector,
                canvas=null,delete canvas,
                head=null,delete head,
                tail=null,delete tail,
                drag=null,delete drag,
                drop=null,delete drop,
                click=null,delete click,
                move=null,delete move,
                quick=null,delete quick;
            }
        }
        return clone
    }
    GNA.coordinate=function(command){
        console.groupCollapsed("Coordinate %i",counter++)
        console.log("Signal Commander:",command)
        console.log("  [STATUS=%o]",command.status())
        encounter(command.feedback());
        console.groupEnd()
        GNA.fire.toggle();
    }
})()
