/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var save=[[],[]];
    GNA.command.feedback=[[],[]];
    GNA.force=function(emote){
        var motion=emote;
        var shape=motion.shape();
        var name=shape.name();
        var canvas=shape.canvas();
        var vector=motion.vector();
        var proxy={
            stop:function(){
                canvas.removeEventListener("mouseover",proxy.stop,true),
                canvas.addEventListener("mouseout",proxy.go,true);
                motion.toggle(),GNA.builder.toggle();
            },
            go:function(){
                canvas.removeEventListener("mouseout",proxy.go,true),
                canvas.addEventListener("mouseover",proxy.stop,true);
                motion.toggle(),GNA.builder.toggle();
            }
        }
        
        var feedback={
            signal:function(){
                GNA.coordinate(proxy);
            },
            move:function(){
                return motion
            },
            shape:function(){
                return shape
            },
            name:function(){
                return name
            },
            canvas:function(){
                return canvas
            },
            vector:function(){
                return vector
            },
            re:{
                move:function(origin){
                    if(origin){
                        motion=origin;
                    }
                    feedback.re.shape();
                    feedback.re.name();
                    feedback.re.canvas();
                    feedback.re.vector();
                },
                shape:function(){
                    shape=motion.shape();
                    return shape
                },
                name:function(){
                    name=shape.name();
                    return name
                },
                canvas:function(){
                    canvas=shape.canvas();
                    return canvas
                },
                vector:function(){
                    vector=motion.vector();
                    return vector
                }
            },
            debug:function(){
                console.groupCollapsed("Debug Feedback Proxy: %s", name)
                console.log("motion-",motion)
                console.log("shape-",shape)
                console.log("name-",name)
                console.log("canvas-",canvas)
                console.log("vector-",vector)
                motion.debug()
                console.groupEnd()
            }
        }
        var finish=function(){
            finish=null,delete finish;
            feedback.basis=null,delete feedback.basis,
                
            feedback.move=null,delete feedback.move,
            feedback.shape=null,delete feedback.shape,
            feedback.name=null,delete feedback.name,
            feedback.canvas=null,delete feedback.canvas,
            feedback.vector=null,delete feedback.vector,
                
            feedback.re.move=null,delete feedback.re.move,
            feedback.re.shape=null,delete feedback.re.shape,
            feedback.re.name=null,delete feedback.re.name,
            feedback.re.canvas=null,delete feedback.re.canvas,
            feedback.re.vector=null,delete feedback.re.vector,
                
            feedback.destroy=null,delete feedback.destroy,
            feedback.debug=null,delete feedback.debug,
            feedback=null,delete feedback;
        }
        var destroy=motion.destroy;
        motion.destroy=function(){
            console.warn("DESTROY %s",name)
            var at=GNA.command[0].indexOf(name)
            GNA.command[0].splice(at,1),
            GNA.command[1].splice(at,1);
            var of=GNA.command.feedback[0].indexOf(name)
            while(of>-1){
                GNA.command.feedback[0].splice(of,1),
                GNA.command.feedback[1].splice(of,1);
                of=GNA.command.feedback[0].indexOf(name)
            }
            destroy();
            destroy=null,delete destroy,
            motion=null,delete motion,
            shape=null,delete shape,
            name=null,delete name,
            canvas=null,delete canvas,
            vector=null,delete vector,
            proxy.stop=null,delete proxy.stop,
            proxy.go=null,delete proxy.go,
            proxy.start=null,delete proxy.start,
            proxy.finish=null,delete proxy.finish,
            proxy=null,delete proxy,
            finish();
        }
        
        proxy.go();
    }
})()