/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var cleanup=function(){
        document.head.innerHTML="<title><!---- LOCKED ----></title>";
        document.body.innerHTML="";
        document.body.style="";
        document.body.style.position="static";
        document.body.style.overflow="hidden",
        document.body.style.margin="0px",
        document.body.style.padding="0px";
        document.body.style.backgroundColor="#ff0000";
        startup();
    }
    window.addEventListener("load", cleanup, true)
    
    var startup=function(){
        var builder=Arena();
        var resize=function(){
            var current=builder.instance.limits().instance().instance();
            console.log("Current",current)
            var element=builder.GNA.element();
            console.log("Element",element)
            element.style.width=current.tail[0]+"px";
            element.style.height=current.tail[1]+"px";
        }
        resize();
        window.addEventListener("resize",resize,true);
        
        console.log("Proxy builder instance forward")
        builder.instance.forward(named);
        
        console.log("Toggle on builder instance")
        builder.instance.toggle();
        
        console.log("Toggle on builder GNA")
        builder.GNA.toggle();
        
        console.log("Add naming array")
        builder.named=[];
        
        Arena.instance=builder;
        console.log("Arena Completed")
    }
    
    var named=function(proxy){
        var gna=proxy;
        console.log("Start Named module; GNA=",gna)
        var instance=gna.instance();
        console.log("Local Instance:",instance)
        var input=document.createElement("input");
        input.style.position="absolute";
        var replace=function(){
            gna=GNA.instance.clone();
            instance=gna.instance();
            reload()
        }
        var reload=function(){
            resize(),reposition()
        }
        var resize=function(){
            input.style.width=instance.width+"px",
            input.style.height=instance.height+"px",
            input.style.fontSize=instance.height+"px";
        }
        var reposition=function(){
            input.style.left=instance.head[0]+"px",
            input.style.top=instance.head[1]+"px";
        }
        var parent=Arena.GNA.element();
        console.log("Parent:",parent)
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup;
                parent.removeChild(input);
            }
            parent.appendChild(input);
            input.focus();
            console.log("Input Ready:",input)
        }
        var singleton=function(){
            wrapper.gna.re.input=null, delete wrapper.gna.re.input,
            singleton=null,delete singleton;
            input(wrapper,GNA.shape);
            wrapper.gna.re.input=named;
        }
        var named=function(){
            GNA.named.unshift(input.value)
        }
        var wrapper={
            GNA:function(){
                return gna
            },
            instance:function(){
                return instance
            },
            element:{
                input:function(){
                    return input
                },
                toggle:function(){
                    toggle()
                    console.log("New Key!")
                    console.log(" wrapper-",wrapper)
                    console.log(" shape-",Arena.shape)
                    key(wrapper,Arena.shape)
                }
            },
            re:{
                input:function(){
                    singleton()
                },
                place:function(){
                    replace()
                },
                size:function(){
                    resize()
                },
                position:function(){
                    reposition()
                },
                load:function(){
                    reload()
                }
            }
            
        }
        var destroy=gna.destroy;
        gna.destroy=function(){
            console.log("Destroy <input>")
            destroy();
            gna=null,delete gna,
            instance=null,delete instance,
            input=null,delete input,
            parent=null,delete parent,
            replace=null,delete replace,
            reload=null,delete reload,
            resize=null,delete resize,
            reposition=null,delete reposition,
            toggle=null,delete toggle,
            wrapper.toggle=null,delete wrapper.toggle;
            wrapper.destroy=null,delete wrapper.destroy;
            wrapper.instance=null,delete wrapper.instance;
            wrapper.debug=null,delete wrapper.debug;
            wrapper.input=null,delete wrapper.input;
            wrapper.re.name=null,delete wrapper.re.name;
            wrapper.re.place=null,delete wrapper.re.place;
            wrapper.re.size=null,delete wrapper.re.size;
            wrapper.re.position=null,delete wrapper.re.position;
            wrapper.re.load=null,delete wrapper.re.load;
            wrapper.re=null,delete wrapper.re;
            wrapper=null,delete wrapper;
        }
        wrapper.destroy=function(){
            gna.destroy()
        }
        var debug=instance.debug;
        instance.debug=function(){
            console.groupCollapsed("<input> Debug");
            console.log(input)
            debug();
            console.groupEnd();
        }
        wrapper.debug=function(){
            instance.debug();
        }
        reload();
        wrapper.element.toggle()
    }
    var key=function(proxy,response){
        var wrapper=proxy;
        wrapper.re.load();
        var form=wrapper.element.input();
        var press=function(event){
            var value=(event.charCode || event.keyCode);
            console.log('key',value)
            switch(value){
                case 27:
                    console.warn("pressed escape")
                    submit();
                    return
                case 13:
                    console.warn("pressed enter")
                    submit();
                    return
            }
        }
        
        var submit=function(){
            form.removeEventListener("blur",submit,true),
            form.removeEventListener("keydown",press,true);
            console.log("Wrapper:",wrapper)
            console.log("Arena:",Arena)
            console.log("Response:",response)
            response(wrapper,Arena.motion);
        }
        form.addEventListener("blur",submit,true)
        form.addEventListener("keydown",press,true)
    }
})()