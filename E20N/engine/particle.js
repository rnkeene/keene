/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    Arena.shape=function(proxy,change){
        console.log("New Particle")
        var callback=change;
        if(proxy.input().value){
            callback(gui(proxy),1,0)
            return
        }
        proxy.destroy()
    }
    var color={
        nw:"#FF0000",
        se:"#FF00FF",
        ne:"#00FF00",
        sw:"#FFFF00"
    };
    var gui=function(proxy){
        console.log("NEW GUI!!")
        var input=proxy;
        var vector=input.position();
        var canvas=document.createElement("canvas");
        canvas.style.position="absolute";
        var context=canvas.getContext("2d");
        var name=input.input().value;
        
        var clone={
            vector:function(){
                return vector
            },
            canvas:function(){
                return canvas
            },
            reset:function(){
                context.fillStyle=color[vector.state];
                context.fillRect(0,0,vector.width,vector.height)
            },
            write:function(){
                context.fillStyle="#000000";
                context.font = vector.height+"px sans-serif";
                context.textBaseline = "top"; 
                context.fillText(clone.name(), parseInt(vector.width*.1), parseInt(vector.height*.1));
                canvas.style.cursor="help";
            },
            name:function(){
                return name
            },
            rename:function(toggle){
                var motion=toggle;
                input.re.place(vector);
                input.toggle();
                var element=input.input();
                var name=clone.name();
                var backup=Arena.shape;
                Arena.shape=function(){
                    if(element.value!=name){
                        clone.write()
                    }
                    motion();
                    Arena.shape=backup;
                }
                input.rename();
            },
            re:{
                place:function(replace){
                    if(replace){
                        vector=replace;
                        clone.re.load();
                    }
                },
                size:function(){
//                    console.log("<< canvas >> re-size")
                    canvas.width=vector.width,
                    canvas.height=vector.height
                },
                position:function(){
//                    console.log("<< canvas >> re-position")
                    canvas.style.left=vector.head[0]+"px",
                    canvas.style.top=vector.head[1]+"px";
                },
                load:function(){
//                    console.log("<< canvas >> re-load")
                    clone.re.position();
                    clone.reset();
                    clone.write();
                }
            }
        }
        var report=input.debug;
        input.debug=function(){
            console.groupCollapsed("<canvas> Debug")
            console.log("GUI (clone):",clone)
            console.log(canvas)
            report();
            console.groupEnd()
        }
        clone.debug=function(){
            input.debug();
        }
        var destroy=input.destroy;
        clone.destroy=function(){
            console.log("GUI Destroy <canvas>")
            destroy();
            input=null,delete input,
            vector=null,delete vector,
            canvas=null,delete canvas,
            context=null,delete context,
            clone.canvas=null,delete clone.canvas,
            clone.destroy=null,delete clone.destroy,
            clone.vector=null,delete clone.vector,
            clone.resize=null,delete clone.resize,
            clone.position=null,delete clone.position,
            clone.rename=null,delete clone.rename,
            clone.reload=null,delete clone.reload,
            clone.reset=null,delete clone.reset,
            clone.spell=null,delete clone.spell,
            clone.debug=null,delete clone.debug;
        }
        Arena.instance.html.element().appendChild(canvas);
        console.log("Success!", clone)
        return clone;
    }
})()