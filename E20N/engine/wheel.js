/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var wheel=function(engine){
        var gui=engine.shape();
        var vector=engine.vector();
        var canvas=engine.canvas();
        var resize=function(bigger){
            if(bigger){
                vector.head[0]-=1,
                vector.head[1]-=1,
                vector.tail[0]+=1,
                vector.tail[1]+=1
                return
            }
            vector.head[0]+=1,
            vector.head[1]+=1,
            vector.tail[0]-=1,
            vector.tail[1]-=1
        }
        var wheel=function(event){
            console.groupCollapsed("Mouse Wheel Resolution")
            engine.toggle()
            var direction=(event.detail || event.wheelDelta);
            resize(direction>-1);
            gui.re.find(vector.head,vector.tail);
            vector=gui.vector();
            gui.re.size();
            gui.re.load();
            engine.toggle()
            console.groupEnd()
        }
        canvas.addEventListener("mousewheel", wheel, false);
        canvas.addEventListener("DOMMouseScroll", wheel, false);
        var clone={
            destroy:function(){
                vector=null,delete vector,
                canvas=null,delete canvas,
                gui=null,delete gui,
                resize=null,delete resize,
                wheel=null,delete wheel
            }
        }
        return clone
    }
})()
