/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    Chaos=function(observer){
        console.group("Chaos:",observer)
        console.log("--space():",observer.space())
        console.log("--walls():",observer.walls())
        console.log("--element():",observer.element())
        console.log("--toggle():",observer.toggle)
        var geometry=observer.fractals().concat();
        console.group("FRACTALS to date...")
        while(geometry.length){
            var fractal=geometry.shift();
            console.group("Fractal:",fractal)
            console.log("--space():",fractal.space())
        }
        console.groupEnd();
//        observer.destroy();
//        observer=null,delete observer
        console.groupEnd()
    }
})()