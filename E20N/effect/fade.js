/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var prepare=function(decimal){
        var hex=decimal.toString(16)
        if(hex.length==1){
            hex="0"+hex
        }
        return "#"+hex+hex+hex;
    }
    var clear=function(id,verify,hold,counter){
        if(id && verify){
            window.clearInterval(id)
        }
        hold.style.backgroundColor=prepare(counter);
    }
    var lighter=function(element){
        var counter=0,hold=element;
        var clone={
            next:function(rate,id){
                console.log("NExt -- lighter")
                if(rate){
                    counter+=rate;
                }else{
                    counter++;
                }
                clear(id,counter>255,hold)
            }
        }
        return clone;
    }
    var darker=function(element){
        var counter=255,hold=element;
        var clone={
            next:function(rate,id){
                console.log("NExt -- darker")
                if(rate){
                    counter-=rate;
                }else{
                    counter--;
                }
                clear(id,counter<0,hold,counter)
            }
        }
        return clone;
    }
    var dark=function(element){
        return darker(element);
    }
    var light=function(element){
        return lighter(element)
    }
    Fade=function(speed,element,inner){
        var id=null;
        var scale=speed;
        var local=null;
        if(inner){
            local=light(element)
        }else{
            local=dark(element)
        }
        var clone={
            start:function(){
                id=window.setInterval(local.next,scale,scale,id)
            },
            speed:function(update){
                if(update){
                    scale=update
                }
                return scale
            }
        }
        return clone
    }
})()