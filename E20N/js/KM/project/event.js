/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Event Algorithm for Knowledge Management.
 *  
 *  
 *  ORDER (From Spreadsheet!)
 */
(function(){
    var mouse=function(event){
        var local=event;
        var clone={
            up:function(){
                console.info("==add mouseup")
                window.addEventListener("mouseup",local,true)
            },
            down:function(){
                console.info("==add mousedown")
                window.addEventListener("mousedown",local,true)
            },
            left:function(){
                console.info("==remove mouseup")
                window.removeEventListener("mouseup",local,true)
            },
            right:function(){
                console.info("==remove mousedown")
                window.removeEventListener("mousedown",local,true)
            }
        }
        return clone;
    },keyboard=function(event){
        var local=event;
        var clone={
            up:function(){
                console.info("==add keyup")
                window.addEventListener("keyup",local,true)
            },
            down:function(){
                console.info("==add keydown")
                window.addEventListener("keydown",local,true)
            },
            left:function(){
                console.info("==remove keyup")
                window.removeEventListener("keyup",local,true)
            },
            right:function(){
                console.info("==remove keydown")
                window.removeEventListener("keydown",local,true)
            }
        }
        return clone;
    },action=function(local){
        var cardinal=local;
        return{
            up:Chaos._pattern(cardinal.up,cardinal.left),
            down:Chaos._pattern(cardinal.down,cardinal.right)
        }
    },event=function(){
        var local={
            current:function(direction){
                var wall=[].concat(direction);
                while(wall.length){
                    wall.shift()();
                }
            }
        }
        var clone={
            register:function(click,press){
                clone.pattern=Chaos._pattern(click,press)
                clone.pattern.circuit=function(){
                    console.groupCollapsed("CHAOS Register PATTERN")
                    local.current(clone.pattern.direction)
                    local.current(clone.pattern.direction)
                    local.current(clone.pattern.direction)
                    local.current(clone.pattern.direction)
                    console.groupEnd()
                }
                clone.mouse=mouse.chaos(click);
                clone.mouse.circuit={
                    up:function(){
                        console.groupCollapsed("CHAOS Register Mouse Up")
                        local.current(clone.mouse.pattern.up.direction)
                        local.current(clone.mouse.pattern.up.direction)
                        local.current(clone.mouse.pattern.up.direction)
                        local.current(clone.mouse.pattern.up.direction)
                        console.groupEnd()
                    },
                    down:function(){
                        console.groupCollapsed("CHAOS Register Mouse Down")
                        local.current(clone.mouse.pattern.down.direction)
                        local.current(clone.mouse.pattern.down.direction)
                        local.current(clone.mouse.pattern.down.direction)
                        local.current(clone.mouse.pattern.down.direction)
                        console.groupEnd()
                    }
                }
                clone.keyboard=keyboard.chaos(press);
                clone.keyboard.circuit={
                    up:function(){
                        console.groupCollapsed("CHAOS Register Keyboard Up")
                        local.current(clone.keyboard.pattern.up.direction)
                        local.current(clone.keyboard.pattern.up.direction)
                        local.current(clone.keyboard.pattern.up.direction)
                        local.current(clone.keyboard.pattern.up.direction)
                        console.groupEnd()
                    },
                    down:function(){
                        console.groupCollapsed("CHAOS Register Keyboard Down")
                        local.current(clone.keyboard.pattern.down.direction)
                        local.current(clone.keyboard.pattern.down.direction)
                        local.current(clone.keyboard.pattern.down.direction)
                        local.current(clone.keyboard.pattern.down.direction)
                        console.groupEnd()
                    }
                }
            }
        }
        return clone
    }
    Chaos._event=function(){
        var chaos=event();
        Chaos._event.memory.unshift(chaos)
        var circuit={
            logic:{
                category:chaos,
                at:function(){
                    return Chaos._event.memory.indexOf(chaos)
                },
                remove:function(){
                    Chaos._event.memory.splice(circuit.logic.at(),1)
                    chaos=null,delete chaos,
                    circuit=null,delete circuit;
                }
            },
            mouse:function(){
                chaos.mouse.circuit.up(),
                chaos.mouse.circuit.down()
            },
            keyboard:function(){
                chaos.keyboard.circuit.up(),
                chaos.keyboard.circuit.down()
            },
            pattern:function(){
                chaos.pattern.circuit()
            }
        }
        return circuit;
    }
    Chaos._event.memory=[];
    
    mouse.chaos=function(event){
        var local=mouse(event);
        return {
            mouse:local,
            pattern:action(local)
        }
    }
    keyboard.chaos=function(event){
        var local=keyboard(event);
        return {
            keyboard:local,
            pattern:action(local)
        }
    }
})()
