/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Event into KM.
 */
(function(){
    
    Chaos.logic=function(x,y){
        var clone=square(x,y);
        var local=log(clone)
        return local
    }
    
    var memory=[]
    var square=function(x,y){
        var chaos=Chaos._event();
        memory.unshift(chaos)
        var hold=[x,y]
        var clone=function(){
            console.warn("Chaos:",chaos)
            var logic=chaos.logic.category;
            logic.register(hold.shift(),hold.shift());
            chaos.mouse();
            chaos.keyboard();
            chaos.pattern();
            return logic;
        }
        var local={
            logic:{
                category:chaos,
                remove:function(){
                    memory.splice(clone.logic.at())
                },
                at:function(){
                    memory.indexOf(chaos)
                }
            },
            create:clone
        }
        return local
    }
    var _memory=function(direction){
        console.log("Memory=[%o]",direction.memory)
        console.log("type=",direction.type)
        console.log("action()=",direction.action)
        console.log("extend()=",direction.extend)
        console.log("toggle()=",direction.toggle)
    },_hash=function(local){
        var where=local
        var bottom=function(){
            console.group("Bottom")
            _memory(where.logic.bottom)
            console.groupEnd()
        }
        var direction=function(){
            console.group("Direction")
            console.log("direction:",where.direction)
            console.groupEnd()
        }
        var top=function(){
            console.group("Top")
            _memory(where.logic.top)
            console.groupEnd()
        }
        bottom();
        direction();
        top();
        _actions(where);
    },_actions=function(direction){
        console.log("A:",direction.A(direction.C,direction.B));
        console.log("B:",direction.B(direction.A,direction.D));
        console.log("C:",direction.C(direction.D,direction.A));
        console.log("D:",direction.D(direction.B,direction.C));
    },_action=function(who){
        console.groupCollapsed("Keyboard (Down,Left,Right,Up)",who)
        who.down()
        who.left()
        who.right()
        who.up()
        console.groupEnd()
    },_down=function(who){
        console.groupCollapsed("Down",who.pattern.down)
        _hash(who.pattern.down)
        console.groupEnd();
    },_up=function(who){
        console.groupCollapsed("Up")
        _hash(who.pattern.up)
        console.groupEnd();
    },
    _pattern=function(who){
        console.groupCollapsed("Pattern :: Up:Down")
        _down(who);
        _up(who);
        console.groupEnd()
    },_circuit=function(who){
        console.groupCollapsed("Pattern :: Circuit")
        who.circuit.up()
        who.circuit.down()
        console.groupEnd();
    }
    var history=[],log=function(chaos){
        var backup=chaos
        history.unshift(chaos);
        backup.create();
        var local=chaos.logic.category;
        var _closed=function(){
            var clone={
                log:function(){
                    console.groupCollapsed("local.circuit")
                    local.keyboard()
                    local.mouse()
                    local.pattern()
                    console.groupEnd()
                }
            }
            clone.log();
            return clone
        }
        var clone={
            circuit:function(){
                return _closed()
            },
            keyboard:function(){
                var who=local.logic.category.keyboard;
                var keyboard={
                    log:function(){
                        console.groupCollapsed("Keyboard ::: Debug")
                        _circuit(who)
                        _action(who.keyboard)
                        _pattern(who);
                        console.groupEnd();
                    }
                }
                keyboard.log();
                return keyboard;
            },
            mouse:function(){
                var who=local.logic.category.mouse;
                var mouse={
                    log:function(){
                        console.groupCollapsed("Mouse ::: Debug")
                        _circuit(who);
                        _action(who.mouse)
                        _pattern(who);
                        console.groupEnd();
                    }
                }
                mouse.log();
                return mouse
            },
            pattern:function(){
                var clone={
                    log:function(){
                        console.groupCollapsed("Pattern ::: Debug")
                        _hash(local.logic.category.pattern)
                        console.groupEnd()
                    }
                }
                clone.log();
                return clone;
            },
            report:function(){
                var scope={
                    report:function(){
                        console.group("Chaos Square: Log(ging) Report")
                        var local={
                            circuit:clone.circuit(),
                            keyboard:clone.keyboard(),
                            mouse:clone.mouse(),
                            pattern:clone.pattern()
                        }
                        console.groupEnd()
                        return local;
                    }
                }
                return scope
            },
            event:function(){
                return local;
            },
            logic:{
                at:function(){
                    console.info("At:",history.indexOf(local))
                    return history.indexOf(local)
                },
                remove:function(){
                    history.splice(clone.logic.at(),1);
                    local=null,delete local,
                    clone=null,delete clone;
                }    
            }
            
        }
        clone.report()
        return clone;
    }
})()
