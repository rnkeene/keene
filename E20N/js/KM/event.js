/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function() {
    var name=function(){
        var local=document.createEvent("Event");
        local.initEvent(Date.now().toString(2), true, true);
        return local
    },
    start=function(basic){
        var clone=basic,
        local=name(),
        cleanup=function(){
            time.action=null,delete time.action,
            time.toggle=null,delete time.toggle,
            time.forget=null,delete time.forget,
            time=null,delete time,
            clone=null,delete clone,
            local=null,delete local,
            toggle=null,delete toggle,
            cleanup=null,delete cleanup
        },
        toggle=function(){
            var tree=clone;
            var backup=toggle;
            document.addEventListener(local.type,tree,true)
            toggle=function(){
                document.removeEventListener(local.type,tree,true)
                toggle=backup,time.forget=cleanup
            }
            time.forget=null,delete time.forget;
        },
        time={
            action:function(){
                document.dispatchEvent(local)
            },
            toggle:function(){
                toggle()
            }
        };
        toggle()
        return time;
    }
    Chaos=function(event){
        return start(event)
    }
})()