/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var fractal=function(x,y){
        var a=Chaos(x),b=Chaos(y),
        cleanup=function(){
            clone=null,delete clone;
            if(a.forget==null){
                a.toggle()
            }
            if(b.forget==null){
                b.toggle()
            }
            a.forget(),b.forget();
            a=null, delete a,
            b=null, delete b
        },
        clone={
            left:function(){
                console.log("a:action")
                a.action()
            },
            right:function(){
                console.log("b:action")
                b.action()
            },
            up:function(){
                console.log("a:toggle")
                a.toggle()
            },
            down:function(){
                console.log("b:toggle")
                b.toggle()
            },
            remove:function(){
                cleanup()
            }
        }
        return clone;
    }
    Fractal=function(x,y){
        var local=fractal(x,y)
        var cardinal={
            order:[local.left,local.right,local.up,local.down],
            A:function(){
                return {
                    origin:[cardinal.C,cardinal.B],
                    action:cardinal.order[0]
                }
            },
            B:function(){
                return {
                    origin:[cardinal.A,cardinal.D],
                    action:cardinal.order[1]
                }
            },
            C:function(){
                return {
                    origin:[cardinal.D,cardinal.A],
                    action:cardinal.order[2]
                }
            },
            D:function(){
                return {
                    origin:[cardinal.B,cardinal.C],
                    action:cardinal.order[3]
                }
            },
            remove:function(){
                local.remove();
                cardinal.remove=null,delete cardinal.remove,
                cardinal.order=null,delete cardinal.order,
                cardinal.A=null,delete cardinal.A,
                cardinal.B=null,delete cardinal.B,
                cardinal.C=null,delete cardinal.C,
                cardinal.D=null,delete cardinal.D,
                cardinal=null,delete cardinal
            }
        }
        return cardinal
    }
})()