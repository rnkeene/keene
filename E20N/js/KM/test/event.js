/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Event into KM.
 */
(function(){
    var local=function(){
        console.info("Some Local Proxy")
    }
    var count=0;
    var run=function(local,time){
        while(count<time){
            console.groupCollapsed("Test #%i",count)
            local.action();
            local.toggle();
            count++;
            console.groupEnd()
        }
        if(local.forget==null){
            local.toggle();
        }
        local.forget();
    }
    var toggleTest=function(){
        console.groupCollapsed("Event.js Test")
        run(Chaos(local),6);
        console.groupEnd()
    }
    toggleTest();
})()
