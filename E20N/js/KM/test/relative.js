/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var alpha=function(){
        console.info("A!")
    },
    beta=function(){
        console.info("B!")
    }
    var count=0;
    var run=function(local,time){
        var a=local.A();
        console.warn("a=%o",a)
        var b=local.B();
        console.warn("b=%o",b)
        var c=local.C();
        console.warn("c=%o",c)
        var d=local.D();
        console.warn("d=%o",d)
        while(count<time){
            console.groupCollapsed("Test #%i",count)
            a.action();
            b.action();
            c.action();
            d.action();
            console.groupCollapsed("References")
            console.log("A",a.origin);
            console.log("B",b.origin);
            console.log("C",c.origin);
            console.log("D",d.origin);
            console.groupEnd()
            count++;
            console.groupEnd()
        }
        local.remove();
    }
    var toggleTest=function(){
        console.group("Relative.js Test")
        run(Fractal(alpha,beta),6);
        console.groupEnd();
    }
    toggleTest();
})()
