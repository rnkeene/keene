/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    GNA.log={
        event:function(event){
            console.groupCollapsed(" === EVENT === ")
            console.warn("Type:",event.type)
            console.info("Phase: ",event.eventPhase)
            switch(event.eventPhase){
                case event.CAPTURING_PHASE:
                    console.log("   CAPTURING_PHASE:",event.CAPTURING_PHASE)
                    break;
                case event.AT_TARGET:
                    console.log("   AT_TARGET:",event.AT_TARGET)
                    break;
                case event.BUBBLING_PHASE:
                    console.log("   BUBBLING_PHASE:",event.BUBBLING_PHASE)
                    break;
            }
            console.info("Bubbles?",event.bubbles)
            console.info("Cancellable?",event.cancelable)
            console.warn("Position (%i, %i):",event.clientX,event.clientY)
            console.info("Time:",event.timeStamp)
            console.groupCollapsed("<document> Targets")
            console.warn("Target:",event.target)
            console.warn("Current Target:",event.currentTarget)
            console.groupEnd()
            console.groupCollapsed("functions(){ ...//... }")
            console.warn("Stop Propogation:",event.stopPropagation)
            console.warn("Prevent Default:",event.preventDefault)
            console.warn("Init Event! ==",event.initEvent)
            console.groupEnd()
            console.groupCollapsed("Objects{ ...//... }")
            console.info("Event!!",event)
            console.groupEnd()
            console.groupEnd()
        }
    }
})()
