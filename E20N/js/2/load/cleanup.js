/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    console.warn("Load Cleanup <script>")
    var cleanup=function(){
        var scripts = document.getElementsByTagName( 'script' );
        while(scripts.length){
            scripts[0].parentElement.removeChild(scripts[0])
            scripts=document.getElementsByTagName( 'script' );
        }
        console.log("Scripts cleaned")
    }
    cleanup()
})()
