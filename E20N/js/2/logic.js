/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var matrix=function(){
        return {
            drag:[[],[]],
            drop:[[],[]]
        }
    }
    var move=function(){
        var history=matrix();
        var solid=true;
        var clone={
            dump:function(){
                if(solid){
                    var clone=matrix()
                    clone.drag[0].concat(history.drag[0]),
                    clone.drag[1].concat(history.drag[1]),
                    clone.drop[0].concat(history.drop[0]),
                    clone.drop[1].concat(history.drop[1]);
                    history=matrix();
                    return clone;
                }
                return matrix();
            },
            drag:{
                capture:function(event){
                    solid=false;
                    history.drag[0].unshift(event)
                },
                bubble:function(event){
                    history.drag[1].unshift(event)
                }
            },
            drop:{
                capture:function(event){
                    history.drop[0].unshift(event)
                },
                bubble:function(event){
                    history.drop[1].unshift(event)
                    solid=true;
                }
            }
        }
        return clone;
    }
    var chain=function(move){
        var module=move;
        var clone={
            dump:function(){
                return module.dump();
            },
            start:function(){
                var clone={
                    capture:function(event){
                        console.group("Global Start");
                        console.groupCollapsed("Start!")
                        console.warn(event)
                        window.addEventListener("mousemove",module.drag.capture,true)
                        window.addEventListener("mousemove",module.drop.capture,true)
                    },
                    bubble:function(event){
                        console.groupCollapsed("~~ BUBBLE ~~")
                        console.warn(event)
                        window.addEventListener("mousemove",module.drag.bubble,false)
                        window.addEventListener("mousemove",module.drop.bubble,false)
                        console.groupEnd()
                        console.groupEnd()
                    }
                }
                return clone
            },
            stop:function(){
                var clone={
                    capture:function(event){
                        console.groupCollapsed("Stop!")
                        console.warn(event)
                        window.removeEventListener("mousemove",module.drag.capture,true)
                        window.removeEventListener("mousemove",module.drop.capture,true)
                    },
                    bubble:function(event){
                        console.groupCollapsed("~~ BUBBLE ~~")
                        console.warn(event)
                        window.removeEventListener("mousemove",module.drag.bubble,false)
                        window.removeEventListener("mousemove",module.drop.bubble,false)
                        console.groupEnd()
                        console.groupEnd()
                        console.groupEnd()
                    }
                }
                return clone;
            }
        }
        return clone;
    }
    var command=function(link){
        var chain=link;
        var start=chain.start();
        var stop=chain.stop();
        var clone={
            start:function(){
                window.addEventListener("mousedown",start.capture,true)
                window.addEventListener("mousedown",start.bubble,false)
                window.addEventListener("mouseup",stop.capture,true)
                window.addEventListener("mouseup",stop.bubble,false)
            },
            stop:function(){
                window.removeEventListener("mousedown",start.capture,true)
                window.removeEventListener("mousedown",start.bubble,false)
                window.removeEventListener("mouseup",stop.capture,true)
                window.removeEventListener("mouseup",stop.bubble,false)
            },
            reset:function(){
                var memory=chain.dump();
                start=chain.start()
                stop=chain.stop();
                return memory
            }
        }
        return clone;
    }
    GNA=function(){
        var module=move();
        console.log("Module:",module)
        var link=chain(module);
        console.log("Chain:",link)
        var proxy=command(link);
        console.log("Proxy:",proxy)
        var element={
            get:{
                log:function(){
                    console.groupCollapsed("Logical Element")
                    console.log("Move:",element.get.move())
                    console.log("Chain:",element.get.chain())
                    console.groupEnd()
                },
                move:function(){
                    return module
                },
                chain:function(){
                    return link
                }
            },
            act:{
                start:function(){
                    proxy.start()
                },
                stop:function(){
                    proxy.stop()
                },
                reset:function(){
                    return proxy.reset()
                }
            }
        }
        return element
    }
})()
