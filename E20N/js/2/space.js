/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    GNA.dimension=function(){
        var dimension={
            grow:GNA.DNA.plant.relative(),
            start:function(){
                dimension.grow.event.debug()
                dimension.grow=GNA.dimension.attach(dimension.grow);
            },
            stop:function(){
                dimension.grow.destroy()
                dimension.grow=null,delete dimension.grow,
                dimension.start=null,delete dimension.start,
                dimension.stop=null,delete dimension.stop,
                dimension=null,delete dimension;
            }
        }
        return dimension
    }
    GNA.dimension.attach=function(grow){
        var local=grow;
        var mirror={
            before:[[],[]],
            after:[[],[]]
        }
        local.history=[[],[]];
        local.report=function(data){
            local.history[0].unshift(data.before)
            local.history[1].unshift(data.after)
        }
        var backup=[];
        var close=function(){
            var cache=local.report;
            local.locked=true;
            local.report=function(data){
                var save=data
                backup.unshift(function(){
                    cache(save)
                })
                var closed=false;
                var close=function(){
                    if(!closed){
                        closed=true,local.report=cache;
                    }
                }
                mouse.queue.unshift(function(){
                    while(backup.length){
                        backup.pop()()
                    }
                    close()
                }
                )
            }
        }
        var open=function(){
            while(mouse.queue.length){
                mouse.queue.pop()()
            }
            local.locked=false;
        }
        var blink=function(){
            close();
            local.report(mirror);
            mirror={
                before:[[],[]],
                after:[[],[]]
            }
            open();
        }
        var mouse={
            queue:[],
            before:function(event){
                var place=document.createEvent("Event");
                place.initEvent(event.type,true,true)
                mirror.before[0].unshift(event);
                mirror.before[1].unshift(place);
            },
            after:function(event){
                var place=document.createEvent("Event");
                place.initEvent(event.type,true,true)
                mirror.after[0].unshift(event);
                mirror.after[1].unshift(place);
            },
            drag:{
                start:function(){
                    window.removeEventListener("mousedown",mouse.drag.start,true)
                    window.addEventListener("mousemove",mouse.before,true)
                    window.addEventListener("mousedown",mouse.drag.stop,false)
                },
                stop:function(){
                    window.removeEventListener("mousedown",local.relative.capture,true)
                    window.removeEventListener("mousedown",mouse.drag.stop,false)
                    window.addEventListener("mousemove",mouse.after,false)
                    window.addEventListener("mouseup",local.relative.bubble,true)
                    window.addEventListener("mouseup",mouse.drop.start,true)
                }
            },
            drop:{
                start:function(){
                    window.removeEventListener("mousemove",mouse.before,true)
                    window.removeEventListener("mousemove",mouse.after,false)
                    window.addEventListener("mouseup",mouse.drop.stop,false)
                },
                stop:function(){
                    window.removeEventListener("mouseup",mouse.drop.stop,false)
                    window.removeEventListener("mouseup",local.relative.bubble,true)
                    window.removeEventListener("mouseup",mouse.drop.start,true)
                    window.addEventListener("mousedown",mouse.drag.start,true)
                    window.addEventListener("mousedown",local.relative.capture,true)
                    blink()
                }
            }
        }
        mouse.drop.stop()
        return local
    }
    var global=GNA.dimension();
    global.start();
    var index=1;
    var counter=[];
    var reset=function(){
        while(counter.length){
            window.clearInterval(counter.pop())
        }
        console.warn("reset")
    }
    var restart=function(){
        console.groupCollapsed("Details")
        while(global.grow.history[0].length){
            console.log("event A:",global.grow.history[0].pop());
            console.log("event B:",global.grow.history[1].pop());
        }
        console.groupEnd()
        index=1;
        console.info("RESTART")
        start();
    }
    var toggle={
        capture:function(){
            console.log(toggle)
            console.warn("toggle.capture")
            var backup=toggle.capture;
            console.warn("toggle.backup")
            toggle.capture=function(){
                var local=global.grow.relative.capture;
                global.grow.relative.capture=function(){
                    console.info("OVERRIDE CAPTURE! ...//add code here")
                    toggle.capture=backup;
                    global.grow.relative.capture=local;
                }
            }
            console.warn("toggled")
            reset();
        },
        bubble:function(){
            console.warn("toggle.bubble")
            var backup=toggle.capture;
            console.warn("toggle.backup")
            toggle.capture=function(){
                var local=global.grow.relative.bubble;
                global.grow.relative.bubble=function(){
                    console.info("OVERRIDE CAPTURE! ...//add code here")
                    toggle.capture=backup;
                    global.grow.relative.bubble=local;
                }
            }
            console.warn("toggled")
            restart();
        }
    }
    var pulse=function(){
        console.info("counter:",index)
        if(!global.grow.locked){
            toggle.capture();
            toggle.bubble();
        }
    }
    var start=function(){
        index+=(index*index)
        console.warn("---- Start [%i] ----",index);
        counter.unshift(window.setInterval(pulse,index))
    }
    start();
})()
