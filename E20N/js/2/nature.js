/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var element=function(local){
        var secret=local;
        var clone={
            element:function(){
                return secret
            },
            type:function(){
                return typeof secret
            },
            target:function(){
                return secret.constructor
            },
            parent:function(){
                return secret.prototype
            }
        }
        return clone
    }
    var local=function(){
        var extend=function(){
            var spawn=[[],[].concat(clone.key)],
            items=clone.key.length;
            while(items--){
                spawn[0].unshift(element(spawn[1][items]))
            }
            return spawn
        }
        var clone={
            key:[function(){},{},0,true,[],""],
            memory:function(){
                var system=function(local){
                    var copy=local;
                    var items=clone.key.length;
                    while(items--){
                        system.memory.elements.unshift(copy[0][items].element());
                        system.memory.types.unshift(copy[0][items].type());
                        system.memory.targets.unshift(copy[0][items].target())
                        system.memory.parents.unshift(copy[0][items].parent())
                    }
                    return system.memory
                }
                system.memory={
                    clone:function(){
                        return local()
                    },
                    elements:[],
                    types:[],
                    targets:[],
                    parents:[],
                    destroy:function(){
                        clone.key=null,delete clone.key,
                        clone.memory=null,delete clone.memory,
                        clone=null,delete clone,
                        system.memory.clone=null,delete system.memory.clone,
                        system.memory.parents=null,delete system.memory.parents,
                        system.memory.types=null,delete system.memory.types,
                        system.memory.targets=null,delete system.memory.targets,
                        system.memory.elements=null,delete system.memory.elements,
                        system.memory.destroy=null,delete system.memory.destroy,
                        system.memory=null,delete system.memory,
                        system=null,delete system;
                        return true
                    }
                }
                return system(extend())
            }
        }
        return clone;
    }
    var natural=function(){
        var system=local();
        system.debug=function(){
            console.groupCollapsed("System:",system)
            console.warn("key=",system.key)
            console.groupEnd()
            console.groupEnd()
        }
        system.register=function(local){
            var module=element(local);
            var history=system.memory();
            var backup=system.debug;
            system.debug=function(){
                console.groupCollapsed("Memory Function Output")
                console.info("memory(){return %o}",history)
                console.log("elements...",history.elements)
                console.log("types...",history.types)
                console.log("targets...",history.targets)
                console.log("parents...",history.parents)
                backup()
            }
            var map=[module,history]
            var bubble=system.destroy;
            system.destroy=function(){
                if(history.destroy()){
                    module=null,delete module,
                    history.destroy=null,delete history.destroy,
                    history=null,delete history,
                    backup=null,delete backup,
                    system.debug=null,delete system.debug,
                    system.register=null,delete system.register,
                    system.destroy=null,delete system.destroy,
                    system=null,delete system;
                    if(bubble){
                        return bubble()
                    }
                    bubble=null,delete bubble;
                    return true;
                }
                return false;
            }
            return map
        }
        return system
    }
    var nature=function(feature){
        var system=natural();
        var flyweight={
            hash:system.register(feature),
            element:0,
            type:0,
            target:0,
            parent:0,
            debug:function(){
                console.groupCollapsed("Test!")
                console.group("Position",flyweight)
                console.log("Element:",flyweight.element)
                console.log("Type:",flyweight.type)
                console.log("Target:",flyweight.target)
                console.log("Parent:",flyweight.parent)
                console.groupEnd()
                console.warn("... hash ...")
                system.debug()
                console.groupEnd()
            },
            update:function(){
                flyweight.element=flyweight.hash[1].elements.indexOf(flyweight.hash[0].element())
                flyweight.type=flyweight.hash[1].types.indexOf(flyweight.hash[0].type())
                flyweight.target=flyweight.hash[1].targets.indexOf(flyweight.hash[0].target())
                flyweight.parent=flyweight.hash[1].parents.indexOf(flyweight.hash[0].parent())
                return flyweight
            },
            destroy:function(){
                if(system.destroy()){
                    flyweight.hash=null,delete flyweight.hash,
                    flyweight.element=null,delete flyweight.element,
                    flyweight.type=null,delete flyweight.type,
                    flyweight.target=null,delete flyweight.target,
                    flyweight.parent=null,delete flyweight.parent,
                    flyweight.update=null,delete flyweight.update,
                    flyweight.destroy=null,delete flyweight.destroy,
                    flyweight=null,delete flyweight;
                    return true
                }
                return false
            }
        }
        return flyweight.update()
    }
    var except=function(id,message){
        var local=message;
        var number=id;
        throw {
            id:number,
            message:local,
            handle:function(){
                console.warn("%i: %s",number,local)
            }
        }
    }
    var handle=function(now){
        var position=now;
        var clone={
            element:function(){
                switch(position.element){
                    case -1:
                        return except(-1,"type-dne")
                    case 0:
                        return except(0,"type-function")
                    case 1:
                        return except(1,"type-object")
                    case 2:
                        return except(2,"type-number")
                    case 3:
                        return except(3,"type-boolean")
                    case 4:
                        return except(4,"type-array")
                    case 5:
                        return except(5,"type-name")
                }
            },
            type:function(){
                switch(position.type){
                    case -1:
                        return except(-1,"type-dne")
                    case 0:
                        return except(0,"type-function")
                    case 1:
                        return except(1,"type-object")
                    case 2:
                        return except(2,"type-number")
                    case 3:
                        return except(3,"type-boolean")
                    case 4:
                        return except(4,"type-array")
                    case 5:
                        return except(5,"type-name")
                }
            },
            target:function(){
                switch(position.target){
                    case -1:
                        return except(-1,"type-dne")
                    case 0:
                        return except(0,"type-function")
                    case 1:
                        return except(1,"type-object")
                    case 2:
                        return except(2,"type-number")
                    case 3:
                        return except(3,"type-boolean")
                    case 4:
                        return except(4,"type-array")
                    case 5:
                        return except(5,"type-name")
                }
            },
            parent:function(){
                switch(position.parent){
                    case -1:
                        return except(-1,"type-dne")
                    case 0:
                        return except(0,"type-function")
                    case 1:
                        return except(1,"type-object")
                    case 2:
                        return except(2,"type-number")
                    case 3:
                        return except(3,"type-boolean")
                    case 4:
                        return except(4,"type-array")
                    case 5:
                        return except(5,"type-name")
                }
            }
        }
        var backup=position.debug;
        position.debug=function(){
            console.groupCollapsed("Handlers")
            clone.element();
            clone.type();
            clone.target();
            clone.parent();
            backup();
            console.groupEnd()
        }
        var restore=position.destroy;
        position.destroy=function(){
            if(restore()){
                position=null,delete position,
                clone.element=null,delete clone.element,
                clone.type=null,delete clone.type,
                clone.target=null,delete clone.target,
                clone.parent=null,delete clone.parent,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone,
                backup=null,delete backup;
                return true
            }
            return false
        }
        return clone;
    }
    Relative=function(composite,local,count,toggle,memory,name){
        var clone={
            feature:nature(composite),
            structure:nature(local),
            place:nature(count),
            configuration:nature(toggle),
            history:nature(memory),
            title:nature(name),
            handle:function(){
                return [handle(clone.feature),handle(clone.structure),handle(clone.place),handle(clone.configuration),handle(clone.history),handle(clone.title)]
            },
            debug:function(){
                console.groupCollapsed("Relative Debug")
                clone.feature.debug();
                clone.structure.debug();
                clone.place.debug();
                clone.configuration.debug();
                clone.history.debug();
                clone.title.debug();
                console.groupEnd()
            },
            destroy:function(){
                console.info("Clean?",clone.feature.destroy());
                console.info("Clean?",clone.structure.destroy());
                console.info("Clean?",clone.place.destroy());
                console.info("Clean?",clone.configuration.destroy());
                console.info("Clean?",clone.history.destroy());
                console.info("Clean?",clone.title.destroy());
            },
            cycle:function(){
                var path=[];
                var wall=clone.handle();
                while(wall.length){
                    var module=wall.pop();
                    var copy={};
                    try{
                        module.element()
                    }catch(e){
                        copy.element=e;
                        try{
                            module.type()
                        }catch(e){
                            copy.type=e;
                            try{
                                module.target()
                            }catch(e){
                                copy.target=e;
                                try{
                                    module.parent()
                                }catch(e){
                                    copy.parent=e;
                                }
                            }
                        }
                    }
                    path.unshift(copy)
                }
                return path;
            }
        }
        return clone;
    }
})()
