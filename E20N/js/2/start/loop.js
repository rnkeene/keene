/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var loop=function(event){
        console.group("Close Loop")
        console.log("Event:",event);
        console.log("Local Time:",event.timeStamp);
        var local=memory();
        console.info("Local Cache:",local)
        console.info("Local Cache-Memory:",local.memory())
        console.groupEnd()
    }
    window.addEventListener("load",loop,false)
})()
