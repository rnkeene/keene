/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var matrix=function(){
        var populate=function(arena,plane,trigger){
            var local=arena,position=plane;
            var xid=position.x[0].indexOf(local.clientX);
            if(xid<0){
                position.x[0].unshift(local.clientX);
                position.x[1].unshift([trigger])
            }else{
                position.x[1][xid].unshift(trigger)
            }
            var yid=position.y[0].indexOf(local.clientY);
            if(yid<0){
                position.y[0].unshift(local.clientY);
                position.y[1].unshift([trigger])
            }else{
                position.y[1][yid].unshift(trigger)
            }
        }
        var action=function(){
            var form={
                detect:{
                    x:[[],[]],
                    y:[[],[]],
                    event:function(event,detect){
                        var local=event,proxy=detect;
                        populate(local,clone.form.detect,function(){
                            proxy(local)
                        })
                    }
                },
                report:{
                    x:[[],[]],
                    y:[[],[]],
                    event:function(event,report){
                        var local=event,proxy=report;
                        populate(local,clone.form.report,function(){
                            proxy(local)
                        })
                    }
                }
            }
            return form;
        }
        var clone={
            form:action(),
            restart:function(){
                clone.form=action();
            }
        }
        return clone;
    }
    var probe=function(){
        var global=matrix();
        var at=[];
        var collect=function(event){
            try{
                var local=event;
                at.unshift(local);
                global.form.detect.event(event,collect)
            }catch(e){
                console.warn("Collection ERROR!")
                console.warn("Collection ERROR!")
                console.warn("Collection ERROR!")
            }
        }
        var observe=function(event){
            try{
                var local=event;
                at.unshift(local);
                global.form.report.event(event,observe)
            }catch(e){
                console.warn("Observation ERROR!")
                console.warn("Observation ERROR!")
                console.warn("Observation ERROR!")
            }
        }
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                window.removeEventListener("mousemove",collect,true),
                window.removeEventListener("mousemove",observe,false);
                toggle=backup
            }
            window.addEventListener("mousemove",collect,true),
            window.addEventListener("mousemove",observe,false);
        }
        var space={
            dump:function(){
                var data={
                    events:[].concat(at),
                    head:{
                        x:[].concat(global.form.detect.x),
                        y:[].concat(global.form.detect.y)
                    },
                    tail:{
                        x:[].concat(global.form.report.x),
                        y:[].concat(global.form.report.y)
                    }
                }
                global=matrix();
                at=[];
                return data;
            },
            toggle:function(){
                toggle()
            }
        }
        return space
    }
    var resistor=function(){
        var module=probe();
        var before=function(event){
            try{
                clone._abstract.start.down(event)
            }catch(e){
                console.warn("REAL EVENT: before:",e.artifact)
            }
        }
        var during=function(event){
            try{
                clone._abstract.stop.down(event)
            }catch(e){
                console.warn("REAL EVENT: during:",e.artifact)
            }
        }
        var verify=function(event){
            try{
                clone._abstract.start.up(event)
            }catch(e){
                console.warn("REAL EVENT: verify:",e.artifact)
            }
        }
        var validate=function(event){
            try{
                clone._abstract.stop.up(event)
            }catch(e){
                console.warn("REAL EVENT: validate:",e.artifact)
            }
        }
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup;
                module.toggle();
                window.removeEventListener("mousedown",before,true),
                window.removeEventListener("mousedown",during,false),
                window.removeEventListener("mouseup",verify,true),
                window.removeEventListener("mouseup",validate,false)
            }
            window.addEventListener("mousedown",before,true),
            window.addEventListener("mousedown",during,false),
            window.addEventListener("mouseup",verify,true),
            window.addEventListener("mouseup",validate,false);
            module.toggle();
        }
        var clone={
            dump:function(){
                return module.dump();
            },
            _abstract:{
                start:{
                    down:function(event){
                        var handle=event;
                        throw{
                            agent:function(){
                                clone._abstract.start.down(handle)
                            },
                            artifact:handle
                        }
                    },
                    up:function(event){
                        var handle=event;
                        throw{
                            agent:function(){
                                clone._abstract.start.up(handle)
                            },
                            artifact:handle
                        }
                    }
                },
                stop:{
                    down:function(event){
                        var handle=event;
                        throw{
                            agent:function(){
                                clone._abstract.stop.down(handle)
                            },
                            artifact:handle
                        }
                    },
                    up:function(event){
                        var handle=event;
                        throw{
                            agent:function(){
                                clone._abstract.stop.up(handle)
                            },
                            artifact:handle
                        }
                    }
                }   
            },
            toggle:function(){
                toggle()
            }
        }
        return clone;
    }
    var observer=function(){
        var data=resistor();
        var history=[];
        var report=function(){
            window.removeEventListener("mouseup",report,false);
            var finish=function(){
                data.toggle();
                window.removeEventListener("mouseup",finish,false);
                history.unshift(data.dump())
                window.addEventListener("mouseup",report,false)
            }
            data.toggle();
            window.addEventListener("mouseup",finish,false);
        }
        var status={
            override:{
                start:{
                    down:function(fresh){
                        data._abstract.start.down=fresh;
                    },
                    up:function(fresh){
                        data._abstract.start.up=fresh;
                    }
                },
                stop:{
                    down:function(fresh){
                        data._abstract.stop.down=fresh;
                    },
                    up:function(fresh){
                        data._abstract.stop.up=fresh;
                    }
                }
            },
            dump:function(){
                var clean=[].concat(history);
                history=[];
                return clean;
            }
        }
        window.addEventListener("mouseup",report,false)
        return status;
    }
    GNA={
        logic:{
            drag:{
                start:function(event){
                    var handle=event;
                    throw{
                        agent:function(){
                            GNA.logic.drag.start(handle)
                        },
                        artifact:handle
                    }
                },
                stop:function(event){
                    var handle=event;
                    throw{
                        agent:function(){
                            GNA.logic.drag.stop(handle)
                        },
                        artifact:handle
                    }
                }
            },
            drop:{
                start:function(event){
                    var handle=event;
                    throw{
                        agent:function(){
                            GNA.logic.drop.start(handle)
                        },
                        artifact:handle
                    }
                },
                stop:function(event){
                    var handle=event;
                    throw{
                        agent:function(){
                            GNA.logic.drop.stop(handle)
                        },
                        artifact:handle
                    }
                }
            },
            click:function(){
                var astart=GNA.logic.drag.start,
                astop=GNA.logic.drag.stop,
                ostart=GNA.logic.drop.start,
                ostop=GNA.logic.drop.stop;
                return {
                    drag:function(){
                        var clone={
                            start:astart,
                            stop:astop,
                            before:true
                        };
                        astart=null,delete astart;
                        astop=null,delete astop;
                        return clone;
                    },
                    drop:function(){
                        var clone={
                            start:ostart,
                            stop:ostop,
                            before:true
                        };
                        ostart=null,delete ostart;
                        ostop=null,delete ostop;
                        return clone;
                    }
                }
            },
            fly:function(){
                var proxy=observer();
                var logic=GNA.logic.click();
                proxy.weight=function(local){
                    var scale=proxy.weight;
                    var clone=local;
                    proxy.weight=function(clone){
                        var weight=proxy.weight,local=clone;
                        if(local.before){
                            local.before=null,delete local.before;
                            proxy.override.start.up(local.start);
                            local.start=null,delete local.start;
                            proxy.override.start.down(local.stop);
                            local.stop=null,delete local.stop;
                            local=null,delete local;
                            proxy.override.start=null,delete proxy.override.start;
                            proxy.weight=scale;
                        }
                        if(proxy.weight===scale){
                            return true
                        }
                        console.warn("---THREADED COLLISION ----!")
                        console.warn("---THREADED COLLISION ----!")
                        console.warn("---THREADED COLLISION ----!")
                        throw{
                            weight:weight,
                            local:local
                        }
                    }
                    if(clone.before){
                        clone.before=null,delete clone.before;
                        proxy.override.stop.down(clone.start);
                        clone.start=null,delete clone.start;
                        proxy.override.stop.up(clone.stop);
                        clone.stop=null,delete clone.stop;
                        clone=null,delete clone;
                        proxy.override.stop=null,delete proxy.override.stop;
                        return true;
                    }
                    if(proxy.weight===scale){
                        console.warn("DUPLICATE EXCEPTION!")
                        console.warn("DUPLICATE EXCEPTION!")
                        console.warn("DUPLICATE EXCEPTION!")
                        throw{
                            weight:scale,
                            local:clone
                        }
                    }
                    console.warn("THROW AWAY EXCEPTION!")
                    console.warn("THROW AWAY EXCEPTION!")
                    console.warn("THROW AWAY EXCEPTION!")
                    proxy.weight=scale;
                    return false;
                }
                var weight={
                    clone:function(){
                        var origin={
                            capture:logic.drag(),
                            bubble:logic.drop()
                        }
                        var drag=function(){
                            console.info("mouse-drag:",clone.origin)
                            origin.capture.start(),
                            origin.bubble.start()
                        }
                        var drop=function(){
                            console.info("mouse-drop:",clone.origin)
                            origin.capture.stop(),
                            origin.bubble.stop()
                        }
                        try{
                            var clone={
                                click:function(){
                                    console.info("mouse-click:",clone.origin)
                                    drag(),drop();
                                    return proxy.dump()
                                }
                            }
                            try{
                                if(proxy.weight(origin.capture)){
                                    if(proxy.weight(origin.bubble)){
                                        var data=proxy.dump();
                                        console.info("success:",data)
                                        var local=function(){
                                            return data
                                        }
                                        return local()
                                    }
                                }
                            }catch(e){
                                throw{
                                    clone:function(){
                                        return GNA.logic.weight(GNA.logic.fly())
                                    }
                                }
                            }
                        }catch(e){
                            return e;
                        }
                    }
                }
                return weight;
            },
            weight:function(fly){
                var shadow=fly;
                var create=function(id){
                    if(shadow===id){
                        create=null,delete create;
                        var mirror=GNA.logic.fly(),proxy=shadow.clone();
                        shadow.override=null,delete shadow.override;
                        proxy.repeat=function(shadow){
                            if(proxy===shadow){
                                var seed=GNA.DNA(mirror)
                                var grow=function(){
                                    console.warn("GROW!! GROW!! GROW!!",seed)
                                    return seed.repeat(seed)
                                }
                                return grow;
                            }
                            console.warn("FAILED RECURSION!! shadow not authenticated!")
                            return proxy.repeat;
                        }
                        return proxy
                    }
                    console.warn("FAILED AUTHENTICATION!! shadow not authenticated!")
                    return create;
                }
                throw{
                    limit:create
                }
            }
        },
        DNA:function(clone){
            var key=clone;
            try{
                GNA.logic.weight(key);
            }catch(e){
                key=e.limit(key)
            }
            return key
        }
    }
})()
