/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var garbage=[];
    var thread=function(source){
        console.warn("A New Thread")
        var script=document.createElement("script")
        script.type="text/javascript";
        script.asynch=true;
        script.src=source;
        document.head.appendChild(script);
        garbage.unshift(function(){
            try{
                document.head.removeChild(script);
            }catch(e){
                console.warn("Error with cleanup:",e)
            }
            script=null,delete script;
            return true;
        })
        GNA.callback(connection)
    }
    var connection=function(key){
        var local=Date.now().toString("2");
        if(!collision(local)){
            window.localStorage.setItem(local,key)
            return true;
        }
        var delay=0;
        while(Date.now().toString("2")==local){
            delay++;
        }
        return connection(key)
    }
    var collision=function(key){
        return (window.localStorage.getItem(key)!=null)
    }
    var dump=function(){
        while(garbage.length){
            garbage.pop()()
        }
    }
    var clean=function(){
        window.localStorage.clear()
    }
    var process=function(){
        console.log("Process")
        GNA.callback();
        console.log("... processed ...")
    }
    GNA=function(url){
        console.log("New GNA")
        thread(url);
        window.setTimeout(process,0)
    }
    GNA.callback=function(feature){
        if(feature!=null){
            var backup=GNA.callback,module=feature;
            console.group("New Callback")
            GNA.callback=function(){
                console.info("Feedback:")
                module();
                console.groupEnd()
                GNA.callback=backup;
            }
            return
        }
        console.warn("Error State")
    }
})()
