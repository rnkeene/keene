/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var child=document.getElementsByTagName('script');
    while(child.length){
        child[0].parentNode.removeChild(child[0])
    }
    var startup=function(){
        var ready=function(){
            console.warn("Running block.js: ready()")
            if(document.getElementsByTagName('script').length){
                console.warn("THROW error")
            }
            ready=null,delete ready;
        }
        GNA.callback(ready)
        GNA("js/2/create/block.js")
    }
    startup()
})()