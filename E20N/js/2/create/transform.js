/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var history=[];
    var backup=GNA.transform;
    var active=window.localStorage.key(0);
    var object=window.localStorage.getItem(active);
    window.localStorage.removeItem(active);
    if(backup==null){
        console.group("Feedback Loop")
    }
    var confirm=function(){
        var stable={
            siblings:window.sessionStorage.length,
            key:window.sessionStorage.key(0),
            item:function(){
                return window.sessionStorage.getItem(stable.key)
            },
            debug:function(){
                console.group("Promote")
                console.info("Family:",stable.siblings)
                console.info("Key:",stable.key)
                console.info("Item:",stable.item())
                console.groupEnd()
            },
            use:function(){
                var local=window.removeItem(stable.key);
                var clone={
                    key:stable.key,
                    item:local
                }
                return clone;
            }
        }
        return stable;
    }
    var promoted=false;
    var promote=function(){
        if(window.localStorage.length===0 && !promoted){
            promoted=true;
            var singleton=confirm();
            singleton.debug();
            history.unshift(singleton);
            console.info("Promote!",history)
            console.groupEnd();
        }
    }
    var verify=function(key,value){
        var id=key,object=value;
        if(GNA.transform!=null){
            GNA.transform()
            var removed=window.localStorage.getItem(key)
            var confirm=function(){
                window.sessionStorage.setItem(id,object)
                var confirm=window.sessionStorage.getItem(id);
                var finish=function(){
                    var validate=window.sessionStorage.getItem(id);
                    if(validate===confirm && !promoted){
                        window.setTimeout(promote, 0)
                    }else{
                        if(!promoted){
                            window.setTimeout(finish,0,id,object)
                        }   
                    }
                }
                if(confirm===object){
                    finish();
                }
            }
            if(removed===null && !promoted){
                confirm(key,value);
            }else{
                console.warn("Already Promoted?",promoted)
                console.warn("Not Removed?",removed===null)
            }
            return
        }
        window.setTimeout(verify,0,id,object)
    }
    var local=window.localStorage.length,
    session=window.sessionStorage.length;
    GNA.transform=function(){
        console.log("Local:",local)
        console.log("Session:",session)
        GNA.transform=backup;
        if(GNA.transform==null){
            console.info(".... root ....")
            console.groupEnd();
        }
    }
})()