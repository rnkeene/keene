/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var debug=function(DNA){
        console.warn("DNA",DNA)
        console.group("Traversal")
        console.info("Clone:",DNA.clone)
        console.groupCollapsed("---- Children ----")
        console.log("Logic:",DNA.clone.logic);
        console.log("Start:",DNA.clone.start);
        console.log("Stop:",DNA.clone.stop);
        console.groupEnd();
        console.info("Current:",DNA.current)
        console.groupCollapsed("---- Children ----")
        console.log("Start:",DNA.current.start);
        console.log("Stop:",DNA.current.stop);
        console.log("Threads:",DNA.current.threads);
        console.groupEnd();
        console.info("Register:",DNA.register)
        console.groupCollapsed("---- Children ----")
        console.log("Action:",DNA.register.action);
        console.log("Name:",DNA.register.name);
        console.log("Position:",DNA.register.position);
        console.groupEnd();
        console.groupCollapsed("=== Exposed Functions ===")
        console.log("Function:: destroy()",DNA.destroy)
        console.log("Function:: process()",DNA.process)
        console.log("Function:: thread()",DNA.thread)
        console.groupEnd();
        console.groupEnd()
    }
    var transform=function(name){
        if(name!=null && name!=undefined){
            var dna=GNA();
            var proxy=name;
            var clone={
                invoke:function(){
                    return proxy()
                },
                process:function(){
                    return dna.process()
                },
                thread:function(){
                    return dna.thread()
                },
                details:{
                    clone:function(){
                        return  {
                            logic:dna.clone.logic,
                            start:dna.clone.start,
                            stop:dna.clone.stop
                        }
                    },
                    current:function(){
                        return  {
                            threads:dna.current.threads,
                            start:dna.current.start,
                            stop:dna.current.stop
                        }
                    },
                    register:function(){
                        return {
                            action:dna.register.action,
                            name:dna.register.name,
                            position:dna.register.position
                        }
                    }    
                },
                proxy:function(){
                    return proxy
                },
                destroy:function(){
                    if(dna.destroy()){
                        dna=null,delete dna,
                        proxy=null,delete proxy,
                        clone.proxy=null,delete clone.proxy,
                        clone.destroy=null,delete clone.destroy,
                        clone.debug=null,delete clone.debug,
                        clone=null,delete clone;
                        return true
                    }
                    return false
                },
                debug:function(){
                    console.groupCollapsed("----- Debug DNA  -----")
                    debug(dna)
                    console.groupEnd();
                }
            }
            return clone;
        }
    }
    GNA.DNA=function(fire){
        var click=transform(fire);
        var clone={
            debug:function(){
                click.debug()
            },
            proxy:function(){
                var self=click.proxy();
                return self(click)
            },
            register:function(listener){
                return transform(listener)
            },
            destroy:function(){
                if(click.destroy()){
                    fire=null,delete fire,
                    click=null,delete click,
                    clone.debug=null,delete clone.debug,
                    clone.proxy=null,delete clone.proxy,
                    clone.register=null,delete clone.register,
                    clone.destroy=null,delete clone.destroy,
                    clone=null,delete clone;
                    return true;
                }
                return false;
            }
        }
        return clone
    }
    GNA.DNA.plant=function(seed,ground){
        var capture=seed,bubble=ground;
        var start=function(){
            return GNA.DNA(capture);
        }
        var grow=function(){
            var seed=start();
            var bubble=[];
            var clone={
                debug:function(){
                    seed.debug();
                },
                capture:function(){
                    console.groupCollapsed("CAPTURE -- capture()")
                    bubble.unshift(seed.proxy());
                    console.groupEnd()
                    return [].concat(bubble)
                },
                bubble:function(){
                    console.groupCollapsed("BUBBLE -- bubble()")
                    var results=[]
                    while(bubble.length){
                        results.unshift(bubble.pop()())
                    }
                    console.log("Results:",results)
                    console.groupEnd()
                    return results
                },
                destroy:function(){
                    if(seed.destroy()){
                        seed=null,delete seed,
                        bubble=null,delete bubble,
                        clone.debug=null,delete clone.debug,
                        clone.capture=null,delete clone.capture,
                        clone.bubble=null,delete clone.bubble,
                        clone.destroy=null,delete clone.destroy,
                        clone=null,delete clone;
                        console.warn("cleaned")
                        return true
                    }
                    return false
                }
            }
            return clone
        }
        return grow()
    }
    GNA.DNA.plant.relative=function(){
        var relative={
            capture:function(){
                console.info("CAPTURE! ~~ start")
                return relative.bubble
            },
            bubble:function(){
                console.warn("BUBBLE! ~~ stop")
                return relative.capture
            }   
        }
        var plant=GNA.DNA.plant(relative.capture,relative.bubble);
        var clone={
            event:plant,
            relative:relative,
            destroy:function(){
                plant.destroy();
                relative.capture=null,delete relative.capture,
                relative.bubble=null,delete relative.bubble,
                relative=null,delete relative,
                plant=null,delete plant,
                clone.event=null,delete clone.event,
                clone.relative=null,delete clone.relative,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone;
                return true;
            }
        }
        return clone
    }
})()
