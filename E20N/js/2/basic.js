/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var flyweight=function(){
        return {
            start:{
                capture:true,
                bubble:false
            },
            stop:{
                capture:true,
                bubble:false
            }    
        }
    }
    var process=function(){
        var kernel=function(){
            var linear=[];
            var find=function(local){
                return linear.indexOf(local)
            }
            var thread={
                current:function(){
                    return [].concat(linear)
                },
                clone:function(){
                    return kernel()
                },
                trigger:function(){
                    var wall=[].concat([].concat(linear))
                    var record=[];
                    while(wall.length){
                        record.unshift(wall.pop()())
                    }
                    return record
                },
                find:function(local){
                    return find(local)
                },
                register:function(feature){
                    linear.unshift(feature)
                },
                restart:function(){
                    var backup=thread.clone();
                    thread.destroy();
                    return backup;
                },
                destroy:function(){
                    find=null,delete find,
                    linear=null,delete linear,
                    thread.clone=null,delete thread.clone,
                    thread.trigger=null,delete thread.trigger,
                    thread.find=null,delete thread.find,
                    thread.register=null,delete thread.register,
                    thread.destroy=null,delete thread.destroy,
                    thread=null,delete thread,
                    kernel=null,delete kernel;
                    return true;
                }
            }
            return thread
        }
        return kernel;
    }
    var memory={
        register:function(){
            var history=[[],[]];
            var named=function(name){
                if(history[0].indexOf(name)<0){
                    history[0].unshift(name)
                    return false;
                }
                return true;
            }
            var module=function(process){
                if(history[1].indexOf(process)<0){
                    history[1].unshift(process)
                    return false;
                }
                return true;
            }
            var register=function(name,mod){
                var truth=[named(name),module(mod)];
                if(truth.pop() && truth.pop()){
                    var position=[history[0].indexOf(name),history[1].indexOf(mod)];
                    return function(){
                        return {
                            name:history[0][position[0]],
                            module:history[1][position[1]]
                        }
                    }
                }
            }
            return {
                name:function(create){
                    if(!named(create)){
                        if(named(create)){
                            return true
                        }
                    }
                    return false;
                },
                action:function(create){
                    if(!module(create)){
                        if(module(create)){
                            return true
                        }
                    }
                    return false;
                },
                position:function(name,action){
                    return register(name,action)
                }
            }
        },
        clone:function(){
            var local={
                logic:flyweight(),
                start:{
                    capture:function(first){
                        local.start.capture=first
                    },
                    bubble:function(first){
                        local.start.bubble=first
                    }
                },
                stop:{
                    capture:function(first){
                        local.stop.capture=first
                    },
                    bubble:function(first){
                        local.stop.bubble=first
                    }
                }
            }
            return local;
        }
    }
            
    var mediator=function(){
        var proxy={
            register:memory.register(),
            clone:memory.clone(),
            process:process(),
            destroy:function(){
                proxy.register=null,delete proxy.register,
                proxy.clone=null,delete proxy.clone,
                proxy.process=null,delete proxy.process,
                proxy.destroy=null,delete proxy.destroy,
                proxy=null,delete proxy;
                return true;
            }
        }
        return proxy;
    }
            
    GNA=function(){
        var system=mediator();
        var snapshot={
            start:system.clone.logic.start,
            stop:system.clone.logic.stop,
            threads:{
                start:{
                    capture:system.clone.start.capture,
                    bubble:system.clone.start.bubble
                },
                stop:{
                    capture:system.clone.stop.capture,
                    bubble:system.clone.stop.bubble
                }
            }
        }
        var run=function(){
            if(snapshot.start.capture){
                snapshot.threads.start.capture();
            }
            if(snapshot.start.bubble){
                snapshot.threads.start.bubble();
            }
            if(snapshot.stop.capture){
                snapshot.threads.stop.capture();
            }
            if(snapshot.stop.bubble){
                snapshot.threads.stop.bubble();
            }
        }
        var clone={
            current:snapshot,
            thread:system.process,
            clone:system.clone,
            register:system.register,
            process:function(){
                run()
            },
            destroy:function(){
                system.destroy();
                snapshot=null,delete snapshot,
                run=null,delete run,
                clone.current=null,delete clone.current,
                clone.thread=null,delete clone.thread,
                clone.clone=null,delete clone.clone,
                clone.register=null,delete clone.register,
                clone.process=null,delete clone.process,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone,
                system=null,delete system;
                return true;
            }
        }
        return clone
    }
})()
