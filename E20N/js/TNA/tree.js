/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var body=function(){
        document.body.style.position="static",
        document.body.style.backgroundColor="#FF0000",
        document.body.style.overflow="hidden",
        document.body.style.zIndex=0;
        document.body.style.margin=0;
        document.body.style.padding=0;
        document.body.width=window.innerWidth;
        document.body.height=window.innerHeight;
        body=null,delete body;
    }
    window.addEventListener("load",body,true)
    TNA=function(compass,crystal,dna){
        console.group("TNA!!")
        console.warn("Compass:",compass)
        console.warn("Crystal:",crystal)
        console.warn("DNA:",dna)
        dna.log.all()
        var tna={
            compass:compass,
            crystal:crystal,
            dna:dna
        }
        TNA.cluster(tna);
        console.groupEnd()
    }
})()
