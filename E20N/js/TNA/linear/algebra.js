/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *       
 */
(function(){
    var lemma=function(event){
        var logical={
            event:event,
            identity:1
        },
        template={
            fractal:{
                LTR:1,
                RTL:event.timeStamp
            },
            legend:{
                LTR:{
                    decimate:["4x","2x","x4+1","x2-1"],
                    compass:["N","S","E","W"],
                    rose:["NW","NE","SW","SE"],
                    data:["0","1","2","3"],
                    color:["R","G","B","A"]
                },
                RTL:{
                    decimate:["x/4","x/2","(x-1)/4","(x+1)/2"],
                    compass:["N","S","E","W"],
                    rose:["NW","NE","SW","SE"],
                    data:["0","1","2","3"],
                    color:["R","G","B","A"]
                }
            },
            map:{
                LTR:{
                    decimate:[],
                    compass:[],
                    rose:[],
                    data:[],
                    color:[]
                },
                RTL:{
                    decimate:[],
                    compass:[],
                    rose:[],
                    data:[],
                    color:[]
                }
            },
            logic:{
                RTL:{
                    minus:function(){
                        template.fractal.RTL-=1
                    },
                    plus:function(){
                        template.fractal.RTL+=1
                    },
                    alpha:function(){
                        template.fractal.RTL/=2
                    },
                    beta:function(){
                        template.logic.RTL.alpha(),template.logic.RTL.alpha()
                    },
                    gamma:function(){
                        template.logic.RTL.minus(),template.logic.RTL.beta()
                    },
                    delta:function(){
                        template.logic.RTL.plus(),template.logic.RTL.alpha()
                    },
                    ordinal:function(){
                        return [template.logic.RTL.alpha,template.logic.RTL.beta,template.logic.RTL.gamma,template.logic.RTL.delta];
                    }
                },
                LTR:{
                    minus:function(){
                        template.fractal.LTR+=1
                    },
                    plus:function(){
                        template.fractal.LTR-=1
                    },
                    alpha:function(){
                        template.fractal.LTR*=2
                    },
                    beta:function(){
                        template.logic.LTR.alpha(),template.logic.LTR.alpha()
                    },
                    gamma:function(){
                        template.logic.LTR.beta(),template.logic.LTR.minus()
                    },
                    delta:function(){
                        template.logic.LTR.alpha(),template.logic.LTR.plus()
                    },
                    ordinal:function(){
                        return [template.logic.LTR.alpha,template.logic.LTR.beta,template.logic.LTR.gamma,template.logic.LTR.delta]
                    }
                }
            },
            rigor:function(){
                var check={
                    evidence:{
                        LTR:[[],[]],
                        RTL:[[],[]]
                    },
                    base:{
                        LTR:template.logic.LTR.ordinal(),
                        RTL:template.logic.RTL.ordinal()
                    }
                }
                var local;
                while(template.fractal.RTL>logical.identity){
                    switch(template.fractal.RTL.toString(4).split('').pop()){
                        case "0":
                            local=check.base.RTL.indexOf(template.logic.RTL.beta)
                            break
                        case "2":
                            local=check.base.RTL.indexOf(template.logic.RTL.alpha)
                            break
                        default:
                            if((template.fractal.RTL-1).toString(4).split('').pop()==0){
                                local=check.base.RTL.indexOf(template.logic.RTL.gamma)
                                break
                            }
                            local=check.base.RTL.indexOf(template.logic.RTL.delta)
                    }
                    check.base.RTL[local]();
                    check.evidence.RTL[0].unshift(local),
                    check.evidence.RTL[1].unshift(check.base.RTL[local]),
                    template.map.RTL.decimate.unshift(template.legend.RTL.decimate[local]),
                    template.map.RTL.compass.unshift(template.legend.RTL.compass[local]),
                    template.map.RTL.rose.unshift(template.legend.RTL.rose[local]),
                    template.map.RTL.data.unshift(template.legend.RTL.data[local]),
                    template.map.RTL.color.unshift(template.legend.RTL.color[local])
                }
                var wall=[].concat(check.evidence.RTL[0])
                while(wall.length){
                    local=wall.shift();
                    check.base.LTR[local]();
                    check.evidence.LTR[0].push(local);
                    check.evidence.LTR[1].push(check.base.LTR[local]);
                    template.map.LTR.decimate.push(template.legend.LTR.decimate[local]),
                    template.map.LTR.compass.push(template.legend.LTR.compass[local]),
                    template.map.LTR.rose.push(template.legend.LTR.rose[local]),
                    template.map.LTR.data.push(template.legend.LTR.data[local]),
                    template.map.LTR.color.push(template.legend.LTR.color[local])
                }
                template.evidence=check.evidence;
                delete template.rigor,template.rigor=null;
            }
        }
        template.rigor();
        return template;
    } 
    var crypt=function(event){
        document.removeEventListener(event,crypt)
        event.agent.logic=lemma(event);
    }
    TNA(crypt);
})()

