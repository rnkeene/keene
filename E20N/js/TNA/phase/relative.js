/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Add/Remove ** Patterns ** on inteval ; 
 *  
 *  "Best Logical Pattern" Known ---> DNA 
 *          (All Life Pattenr)
 *          
 *          Therefor ++Chemestry.
 *              (Groups; Elements, etc)
 *  
 *          'manage 'system'
 *          
 *           Concurrent Command Heirarchy
 *          [ Capotolism ]
 *    
 *      GNA has 4 major purely logical "events":
 *          Ensure "Registration" of
 *                  4 Distinct processes.
 *  
 *                  
 *  Of:
 *      .elements.[Hydrogen,Oxygen,Nitrogen,Carbon,Phosphorus]
 *      
 *  Of:
 *      .particle
 *      .elements
 *      .{guanine(G),adenine(A),thymine(T),cytosine(C)}
 *      
 *      OF:
 *          (1)[Pyrimidines,Purines](0)
 *              Of:
 *                  (+)[trigger] - Major Groove (large - press),
 *                  (-)[thread] - Minor Groove (small - release)
 *              
 *  Of:
 *      ?? given the "GNA" base; 'how often' and 'how much' of a change ??
 *      
 *      A pairs only with T (of thymine) in DNA. 
 *      
 *          100 - 27 - 27 = 46% of total bases left.
 *      
 *      Given 27% are A (adenine), then there has to be 27% T.
 * 
 *          All bases are paired in DNA, so 46/2 = 23 pairs;
 *      
 *      23% of DNA are paired G-C bases,
 *      23% are G and another 
 *      23% are C bases.
 *          === ANSWER ===
 *          27% = Thymine, 
 *          23% = Guanine, 
 *          and 23% = Cytosine
 */
(function(){
    console.group("Startup Sequence")
    QED.relative();
    var collapse= QED.legend.pool.gravity.shift();
    console.log("RELATIVE",collapse)
    window.addEventListener("keydown", collapse.faster, true)
    window.addEventListener("keyup", collapse.slower, true)
    window.addEventListener("mousedown", collapse.stop, true)
    window.addEventListener("mouseup", collapse.faster, true)
})()
/*TNA(debug);
        console.groupCollapsed("DEBUG event.agent.logic",event.agent)
        console.table([event.agent.logic.map.RTL.decimate,event.agent.logic.map.RTL.compass,event.agent.logic.map.RTL.rose,event.agent.logic.map.RTL.data,event.agent.logic.map.RTL.color])
        console.table([event.agent.logic.map.LTR.decimate,event.agent.logic.map.LTR.compass,event.agent.logic.map.LTR.rose,event.agent.logic.map.LTR.data,event.agent.logic.map.LTR.color])
        console.table(event.agent.logic.evidence.LTR)
        console.table(event.agent.logic.evidence.RTL)
        console.info(event.agent.logic)
        console.info(event.timeStamp)
        console.groupEnd();    
    }
 */