/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *            PHOTON
 */
(function(){
    var space=function(local){
        var module=local;
        console.warn("NEW SPACE!!")
        console.info("MODULE:",module)
        module.start();
        if(module.name!=null){
            module.location.height(module.delta.square.height)
            module.location.width(module.delta.square.width)
            module.location.top(module.delta.position.top)
            module.location.left(module.delta.position.left)
            module.name.left(parseInt(module.delta.square.width*.01))
            module.name.top(module.delta.square.height-parseInt(module.delta.square.height/7.5))
            module.name.index(Universe.history.length)
            module.name.scale(module.delta.square.height)
            module.name.font("sans-serif")
            module.name.reset("1");
            module.name.refresh();
        }
    }
    var move=function(){
        return {
            left:false,
            up:false,
            right:false,
            down:false
        }
    }
    var detect=function(local){
        var module=local,current=move();
        var press={
            left:module.location.move.left,
            up:module.location.move.up,
            right:module.location.move.right,
            down:module.location.move.down,
            move:function(event){
                switch(event.keyCode){
                    case 37:
                        press.left()
                        current.left=true;
                        if(current.up){
                            press.up()
                        }
                        if(current.down){
                            press.down()
                        }
                        return
                    case 38:
                        module.location.move.up()
                        current.up=true;
                        if(current.left){
                            press.left()
                        }
                        if(current.right){
                            press.right()
                        }
                        return
                    case 39:
                        module.location.move.right()
                        current.right=true;
                        if(current.up){
                            press.up()
                        }
                        if(current.down){
                            press.down()
                        }
                        return
                    case 40:
                        module.location.move.down()
                        current.down=true;
                        if(current.left){
                            press.left()
                        }
                        if(current.right){
                            press.right()
                        }
                        return
                }
            },
            stop:function(event){
                switch(event.keyCode){
                    case 37:
                        current.left=false;
                        return
                    case 38:
                        current.up=false;
                        return
                    case 39:
                        current.right=false;
                        return
                    case 40:
                        current.down=false;
                        return
                }
            },
            action:function(){
                var backup=press.action;
                document.addEventListener("keydown",press.move,true)
                document.addEventListener("keyup",press.stop,true)
                press.action=function(){
                    document.removeEventListener("keydown",press.move,true)
                    document.removeEventListener("keyup",press.stop,true)
                    press.action=backup;
                }
            }
        }
        return press
    }
    Universe.golen=function(local){
        var module=local;
        var consider=detect(module);
        if(module.delta.square.height>0 && module.delta.square.width>0){
            space(consider)
        }
        return {
            start:module.start,
            module:module
        }
    }
})()
