/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *
 *  
 */
(function(){
    var arena=document.createElement("canvas");
    arena.style.position="absolute"
    var context=arena.getContext("2d")
    Universe.ruler=function(){
        var name=function(){
            var secret={
                content:[],
                font:[1,"px"," ","sans-serif"],
                primary:["00","00","00"],
                secondary:["ff","ff","ff"],
                offset:{
                    x:1,y:1
                },
                plane:0
            }
            ,out=function(){
                context.fillStyle = "#"+secret.secondary.join("")
                context.fillText(secret.content.join(""),secret.x,secret.y)
                secret.content.pop();
                write();
            },write=function(){
                arena.style.backgroundColor = "#"+secret.secondary.join("")
                context.fillStyle = "#"+secret.primary.join("")
                context.fillText(secret.content.join(""),secret.x,secret.y)
            },clone={
                reset:function(name){
                    if(name){
                        secret.content=[name]
                    }
                    write()
                },
                less:function(){
                    out()
                },
                more:function(content){
                    if(content){
                        secret.content.push(content)
                    }else{
                        secret.content.push([].concat(secret.content))
                    }
                    write()
                },
                left:function(x){
                    if(!!x){
                        secret.x=x
                    }
                    return secret.x
                },
                top:function(y){
                    if(!!y){
                        secret.y=y
                    }
                    return secret.y
                },
                index:function(i){
                    secret.z=i
                    arena.style.zIndex=secret.z
                },
                scale:function(size){
                    if(!!size){
                        secret.font[0]=size
                        context.font=secret.font.join("")
                    }
                    return secret.font[0]
                },
                font:function(family){
                    if(family){
                        secret.font[3]=family
                        context.font=secret.font.join("")
                    }
                    return secret.font[3]
                },
                red:function(red){
                    if(!!red){
                        var eq=red.toString(16),opp=(255-red).toString(16);
                        if(eq.length==1){
                            eq="0"+eq;
                        }
                        if(opp.length==1){
                            opp="0"+opp;
                        }
                        secret.primary[0]=eq;
                        secret.secondary[0]=opp
                        clone.write()
                    }
                    return secret.primary[0]
                },
                green:function(green){
                    if(!!green){
                        var eq=green.toString(16),opp=(255-green).toString(16);
                        if(eq.length==1){
                            eq="0"+eq;
                        }
                        if(opp.length==1){
                            opp="0"+opp;
                        }
                        secret.primary[1]=eq;
                        secret.secondary[1]=opp
                        clone.write()
                    }
                    return secret.primary[1]
                },
                blue:function(blue){
                    if(!!blue){
                        var eq=blue.toString(16),opp=(255-blue).toString(16);
                        if(eq.length==1){
                            eq="0"+eq;
                        }
                        if(opp.length==1){
                            opp="0"+opp;
                        }
                        secret.primary[2]=eq;
                        secret.secondary[2]=opp
                        clone.write()
                    }
                    return secret.primary[2]
                },
                write:function(){
                    write()
                },
                toggle:function(){
                    var restore=clone.toggle;
                    document.body.appendChild(arena)
                    clone.toggle=function(){
                        clone.toggle=restore;
                        document.body.removeChild(arena)
                    }
                }
            }
            return clone
        }
        var location=function(){
            var secret={
                width:1,
                height:1,
                left:0,
                top:0
            }
            var clone={
                height:function(h){
                    if(!!h){
                        secret.height=h;
                        clone.write()
                    }
                    return secret.height;
                },
                width:function(w){
                    if(!!w){
                        secret.width=w;
                        clone.write()
                    }
                    return secret.width;
                },
                top:function(t){
                    if(!!t){
                        secret.top=t;
                        clone.write()
                    }
                    return secret.top;
                },
                left:function(l){
                    if(!!l){
                        secret.left=l;
                        clone.write()
                    }
                    return secret.left;
                },
                bottom:function(b){
                    if(!!b){
                        secret.top=b-secret.height;
                        clone.write()
                    }
                    return secret.top+secret.height
                },
                right:function(r){
                    if(!!r){
                        secret.left=r-secret.width
                    }
                    return secret.left+secret.width
                },
                move:{
                    up:function(){
                        secret.top-=secret.height;
                        arena.style.top=secret.top+"px"
                    },
                    down:function(){
                        secret.top+=secret.height;
                        arena.style.top=secret.top+"px"
                    },
                    left:function(){
                        secret.left-=secret.width;
                        arena.style.left=secret.left+"px"
                    },
                    right:function(){
                        secret.left+=secret.width;
                        arena.style.left=secret.left+"px"
                    }
                },
                write:function(){
                    clone.write.position();
                    clone.write.dimension();
                }
            }
            clone.write.position=function(){
                arena.style.top=secret.top+"px",
                arena.style.left=secret.left+"px"
            }
            clone.write.dimension=function(){
                arena.width=secret.width,
                arena.height=secret.height
            }
            return clone
        }
        var clone={
            name:name(),
            location:location()
        }
        clone.start=function(){
            clone.name.toggle();
        }
        return clone
    }
    Universe.behind=function(){
        var local=Universe.ruler();
        var arena=document.createElement("canvas");
        arena.style.position="absolute"
        var player=arena.getContext("2d")
        document.body.appendChild(arena)
        var clone={
            at:arena,
            behind:context,
            player:player,
            ruler:local
        }
        clone.basis=local;
        return clone
    }
})()
