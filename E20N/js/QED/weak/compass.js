/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *       
 *       LANGUAGE PATTERN
 */
(function(){
    Universe.history=[];
    var module=function(space){
        var imagine=Universe.ruler();
        imagine.basis=space;
        imagine.delta={
            square:{
                width:space.x-space.inner.x,
                height:space.y-space.inner.y
            },
            round:function(){
                if(imagine.delta.spin.width){
                    imagine.delta.square.width*=-1
                }
                if(imagine.delta.spin.height){
                    imagine.delta.square.height*=-1
                }
            }
        }
        imagine.delta.spin={
            width:imagine.delta.square.width<0,
            height:imagine.delta.square.height<0
        }
        imagine.delta.height={
            gt:(imagine.delta.square.height>0),
            eq:(imagine.delta.square.height==0),
            lt:(imagine.delta.square.height<0)
        }
        imagine.delta.width={
            gt:(imagine.delta.square.width>0),
            eq:(imagine.delta.square.width==0),
            lt:(imagine.delta.square.width<0)
        }
        imagine.delta.point=imagine.delta.width.eq && imagine.delta.height.eq
        imagine.delta.flat={
            n:(imagine.delta.width.eq && imagine.delta.height.gt),
            s:(imagine.delta.width.eq && imagine.delta.height.lt),
            e:(imagine.delta.width.lt && imagine.delta.height.eq),
            w:(imagine.delta.width.gt && imagine.delta.height.eq)
        }
        imagine.delta.top={
            ne:(imagine.delta.width.lt && imagine.delta.height.gt),
            nw:(imagine.delta.width.gt && imagine.delta.height.gt)
        }
        imagine.delta.bottom={
            se:(imagine.delta.width.lt && imagine.delta.height.lt),
            sw:(imagine.delta.width.gt && imagine.delta.height.lt)
        }
        imagine.delta.position={
            top:imagine.basis.inner.y,
            left:imagine.basis.inner.x
        }
        imagine.delta.round();
        if(imagine.delta.spin.width){
            imagine.delta.position.left-=imagine.delta.square.width
        }
        if(imagine.delta.spin.height){
            imagine.delta.position.top-=imagine.delta.square.height
        }
        imagine.delta.square.area=imagine.delta.square.width*imagine.delta.square.height
        imagine.delta.color={
            primary:(imagine.delta.square.area-imagine.delta.square.area%255)/255
        }
        imagine.delta.color.overflow=(imagine.delta.color.primary-imagine.delta.color.primary%255)/255;
        if(imagine.delta.color.overflow){
            imagine.delta.color.primary-=(255*imagine.delta.color.overflow)
        }
        imagine.delta.color.primary=255-imagine.delta.color.primary
        imagine.delta.color.overflow=255-imagine.delta.color.overflow
        if(imagine.delta.top.nw){
            imagine.delta.color.type="black"
        }else if(imagine.delta.top.ne){
            imagine.delta.color.type="red"
        }else if(imagine.delta.bottom.se){
            imagine.delta.color.type="green"
        }else if(imagine.delta.bottom.sw){
            imagine.delta.color.type="blue"
        }
        imagine.name.refresh=function(){
            switch(imagine.delta.color.type){
                case "black":
                    imagine.name.red(imagine.delta.color.primary)
                    imagine.name.green(imagine.delta.color.primary)
                    imagine.name.blue(imagine.delta.color.primary)
                    break;
                case "red":
                    imagine.name.red(imagine.delta.color.primary)
                    imagine.name.green(imagine.delta.color.overflow)
                    imagine.name.blue(255)
                    break;
                case "green":
                    imagine.name.green(imagine.delta.color.primary)
                    imagine.name.blue(imagine.delta.color.overflow)
                    imagine.name.red(255)
                    break;
                case "blue":
                    imagine.name.blue(imagine.delta.color.primary)
                    imagine.name.red(imagine.delta.color.overflow)
                    imagine.name.green(255)
                    break;
            }
        }
        imagine.tree=Universe.behind();
        if(!imagine.delta.point){
            Universe.history.unshift(imagine)
        }
        return imagine;
    }
    var create=function(){
        return document.createEvent("Event").timeStamp.toString(2);
    }
    var now=function(){
        return Date.now().toString(2)
    }
    var prepare=function(event){
        var clone={
            x:event.clientX,
            y:event.clientY,
            identity:event.timeStamp.toString(2),
            relative:create(),
            now:now()
        }
        return clone;
    }
    var ready=function(inner,outer){
        var clone={
            x:outer.x,
            y:outer.y,
            time:{
                origin:outer.identity,
                limit:outer.relative,
                offset:outer.now
            },
            inner:{
                x:inner.x,
                y:inner.y,
                time:{
                    origin:inner.identity,
                    limit:inner.relative,
                    offset:inner.now
                }
            }
        }
        return clone;
    }
    
    var inertia=function(){
        var down=function(event){
            document.removeEventListener("mousedown",down,true)
            var outer=prepare(event);
            var up=function(event){
                console.info("Turning on Compass!")
                document.removeEventListener("mouseup",up,true)
                field(module(ready(prepare(event),outer)))
                document.addEventListener("mousedown",down,true)
            }
            document.addEventListener("mouseup",up,true)
        }
        document.addEventListener("mousedown",down,true)
    }
    inertia.create=function(){
        return document.createEvent("Event").timeStamp.toString(2);
    },
    inertia.now=function(){
        return Date.now().toString(2)
    }
    inertia();
    var contained=function(){
        if(memory.length){
            var flux=memory.shift();
            flux.event.pause();
            var clear=function(){
                document.addEventListener("mouseup",flux.stop,true),
                document.removeEventListener("keyup",clear,true);
            },add=function(event){
                switch(event.keyCode){
                    case 8:
                        Universe.history[0].name.less();
                        break;
                    case 37:
                        Universe.history[0].name.left(Universe.history[0].name.left()-1)
                        Universe.history[0].name.write();
                        break;
                    case 38:
                        Universe.history[0].name.top(Universe.history[0].name.top()-1)
                        Universe.history[0].name.write();
                        break;
                    case 39:
                        Universe.history[0].name.left(Universe.history[0].name.left()+1)
                        Universe.history[0].name.write();
                        break;
                    case 40:
                        Universe.history[0].name.top(Universe.history[0].name.top()+1)
                        Universe.history[0].name.write();
                        break;
                    default:
                        var local=String.fromCharCode(event.keyCode);
                        if(!event.shiftKey){
                            local=local.toLowerCase()
                        }
                        Universe.history[0].name.more(local);
                }
            },
            stop=function(){
                document.removeEventListener("keyup",add,true),
                document.removeEventListener("mouseup",stop,true)
                flux.event.resume();
                memory.unshift(flux)
            }
            document.addEventListener("keyup",clear,true),
            document.addEventListener("keydown",add,true)
        }
    }
    var memory=[]
    var field=function(local){
        console.groupCollapsed("Compass Field (Memory)")
        var flux=Universe.golen(local);
        debug(local)
        if(flux.module.delta.point){
            console.info(" ==== Special ==== ")
            contained()
        }else{
            memory.unshift(flux)
        }
        TNA(local,flux,DNA());
        console.groupEnd();
    }
    var debug=function(module){
        console.groupCollapsed("MODULE (PRESS/Drag & Drop)")
        console.info(module)
        console.log("Basis: %o",module.basis)
        console.log("Spin:",module.delta.spin)
        console.log("Absolute: [%i,%i]",module.delta.square.width,module.delta.square.height)
        console.log("Point? %i",module.delta.point)
        console.info("Flat:",module.delta.flat)
        console.info("Top:",module.delta.top)
        console.info("Bottom:",module.delta.bottom)
        console.groupEnd();
    }
})()