/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    DNA=function(){
        var gradient=function(){
            var basis={
                linear:{
                    start:{
                        x:0,
                        y:0,
                        length:0
                    },
                    end:{
                        x:0,
                        y:0,
                        length:0
                    }
                },
                radial:{
                    start:0,
                    end:0
                }
            }
            return basis;
        }
        var click=function(event){
            var canvas=document.createElement("canvas")
            var context=canvas.getContext("2d")
            console.warn("Click Canvas and Context (for %o)! %o, %o",event,canvas,context,event)
        }
        var press=function(event){
            var canvas=document.createElement("canvas")
            var context=canvas.getContext("2d")
            console.warn("Press Canvas and Context (for %o)! %o, %o",event,canvas,context)
        }
        var activate=function(){
            console.groupCollapsed("Activating")
            var local=Chaos.logic(click,press)
            var global=local.event()
            var context=local.report();
            var report=context.report();
            console.groupEnd();
            return {
                logic:local,
                event:global,
                log:{
                    circuit:report.circuit.log,
                    keyboard:report.keyboard.log,
                    pattern:report.pattern.log,
                    mouse:report.mouse.log,
                    all:context.report
                }
            }
        }
        var clone=activate();
        clone.gradient=gradient();
        console.warn("A CLONE!",clone);
        console.warn("A CLONE gradient!",clone.gradient);
        return clone;
    }
    
    
})()
