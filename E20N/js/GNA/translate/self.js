/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var map=function(when){
        var at=when;
        var clone={
            N:"000",
            NW:function(at){
                return at+clone.N
            },
            E:"100",
            NE:function(at){
                return at+clone.E
            },
            S:"010",
            SE:function(at){
                return at+clone.S
            },
            W:"110",
            SW:function(at){
                return at+clone.W
            },
            suppliment:{
                N:"001",
                NW:function(at){
                    return at+clone.suppliment.N
                },
                E:"101",
                NE:function(at){
                    return at+clone.suppliment.E
                },
                S:"011",
                SE:function(at){
                    return at+clone.suppliment.S
                },
                W:"111",
                SW:function(at){
                    return at+clone.suppliment.W
                }
            },
            grow:function(){
                return map(at);
            }
        }
        return clone;
    }
    Universe.linear=Universe();
    Universe.plane=[Universe.linear.definition.AD,Universe.linear.definition.DA,Universe.linear.definition.BC,Universe.linear.definition.CB];    
    Universe.surface=function(name,number,map){
        var decimate=parseInt(number),id=parseInt(number,10).toString(2),real=name;
        var weight={
            NW:[map.NW(id),Universe.plane[decimate][0][0][0].eq],
            NE:[map.NE(id),Universe.plane[decimate][1][0][0].eq],
            SW:[map.SW(id),Universe.plane[decimate][0][1][0].eq],
            SE:[map.SE(id),Universe.plane[decimate][1][1][0].eq],
            suppliment:{
                NW:[map.suppliment.NW(id),Universe.plane[decimate][0][0][1].eq],
                NE:[map.suppliment.NE(id),Universe.plane[decimate][1][0][1].eq],
                SW:[map.suppliment.SW(id),Universe.plane[decimate][0][1][1].eq],
                SE:[map.suppliment.SE(id),Universe.plane[decimate][1][1][1].eq]
            },
            eq:function(){
                return decimate
            },
            compliment:function(){
                return Universe.plane[decimate]
            },
            lore:function(){
                return [id.charCodeAt(0),real.charCodeAt(0)]
            },
            proof:function(){
                return {
                    NW:weight.NW[0],
                    NE:weight.NE[0],
                    SW:weight.SW[0],
                    SE:weight.SE[0],
                    suppliment:{
                        NW:weight.suppliment.NW[0],
                        NE:weight.suppliment.NE[0],
                        SW:weight.suppliment.SW[0],
                        SE:weight.suppliment.SE[0]
                    }
                }
            },
            hash:function(){
                var memory={
                    at:id,
                    of:real,
                    NW:weight.NW[1],
                    NE:weight.NE[1],
                    SW:weight.SW[1],
                    SE:weight.SE[1],
                    suppliment:{
                        NW:weight.suppliment.NW[1],
                        NE:weight.suppliment.NE[1],
                        SW:weight.suppliment.SW[1],
                        SE:weight.suppliment.SE[1]
                    }
                }
                return memory
            }
        }
        return weight;
    }    
    Universe.particle={
        A:Universe.surface("A","0",map("0")),
        B:Universe.surface("B","2",map("2")),
        C:Universe.surface("C","3",map("3")),
        D:Universe.surface("D","1",map("1"))
    }
    var ABCD=[Universe.time(Universe.plane[0][0][0][0]),Universe.time(Universe.plane[0][1][0][0]),Universe.time(Universe.plane[0][0][1][0]),Universe.time(Universe.plane[0][1][1][0])]
    Universe.game=function(of){
        var when=ABCD[of.eq()]
        var game={
            world:of.hash(),
            region:{
                nw:when.nw(),
                ne:when.ne(),
                se:when.se(),
                sw:when.sw()
            },
            lore:{
                magic:of.lore(),
                field:of.compliment(),
                observe:of.proof()
            }
        }
        return game;
    }
    Universe.game.A=Universe.game(Universe.particle.A)
    Universe.game.B=Universe.game(Universe.particle.B)
    Universe.game.C=Universe.game(Universe.particle.C)
    Universe.game.D=Universe.game(Universe.particle.D)
})()
