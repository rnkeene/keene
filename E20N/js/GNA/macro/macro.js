/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    console.group("GNA/macro/macro.js (@DNA,[@Event])")
    var history=[],
    GNA=[[history],[[Date.now()],[0]]];
    GNA.paradigm=function(event){
        
        GNA.shift(event)
    },
    GNA.concurrent=function(event){
        console.log("GNA-start")
        
        var origin=document.createEvent("Event");
        document.initEvent(origin.timeStamp,true,true)
        
        GNA.unshift(window.setInterval(GNA.paradigm,0,event))
    },
    GNA.kill=function(event){
        console.log("GNA-crash")
        window.clearInterval(GNA.shift())
        history.push([Date.now()-event.type,event])
        console.groupEnd();
    }
    var trigger=document.createEvent("Event");
    trigger.initEvent(Date.now(),true,true);
    document.addEventListener(trigger.type,GNA.start)
    document.dispatchEvent(trigger);
})()
