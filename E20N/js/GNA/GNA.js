/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 */
(function(){
    var metric={
        next:function(part){
            return part.eq.eq+4;
        },
        previous:function(Universe){
            return Universe.eq.eq-4;
        },
        current:function(Universe){
            var local=Universe;
            local.next=function(){
                return metric.next(local)
            }
            local.previous=function(){
                return metric.previous(local)
            }
            return local
        },
        normal:function(part){
            part.eq.normal=part.eq()
            part.gt.normal=part.gt()
            part.lt.normal=part.lt()
            
        },
        zero:function(){
            var clone={
                eq:{
                    eq:0,
                    gt:1,
                    lt:-2
                },
                gt:{
                    eq:2,
                    gt:2
                },
                lt:{
                    eq:1,
                    lt:-1
                }
            }
            return metric.current(clone);
        },
        one:function(){
            var clone={
                eq:{
                    eq:1,
                    gt:-2,
                    lt:-1
                },
                gt:{
                    eq:0,
                    gt:1
                },
                lt:{
                    eq:3,
                    lt:2
                }
                
            }
            return metric.current(clone);
        },
        two:function(){
            var clone={
                eq:{
                    eq:2,
                    gt:2,
                    lt:1
                },
                gt:{
                    eq:3,
                    gt:-1
                    
                },
                lt:{
                    eq:0,
                    lt:-2
                }
            }
            return metric.current(clone);
        },
        three:function(){
            var clone={
                eq:{
                    eq:3,
                    gt:-1,
                    lt:2
                },
                gt:{
                    eq:1,
                    gt:-2
                    
                },
                lt:{
                    eq:2,
                    lt:1
                }
            }
            return metric.current(clone);
        }
    },
    time=function(magnet){
        var local=magnet;
        var body={
            id:local.eq,
            ne:function(){
                return 4+body.id.gt
            },
            se:function(){
                return 4+body.id.lt
            },
            nw:function(){
                return -4+body.id.gt
            },
            sw:function(){
                return -4+body.id.lt
            }
        }
        return body;
    },
    memory={
        map:function(){
            var composite=function(){
                var local={
                    zero:time(clone.zero),
                    one:time(clone.one),
                    two:time(clone.two),
                    three:time(clone.three),
                    map:function(){
                        var hash={
                            zero:{
                                nw:local.zero.nw(),
                                ne:local.zero.ne(),
                                se:local.zero.se(),
                                sw:local.zero.sw()
                            },
                            one:{
                                nw:local.one.nw(),
                                ne:local.one.ne(),
                                se:local.one.se(),
                                sw:local.one.sw()
                            },
                            two:{
                                nw:local.two.nw(),
                                ne:local.two.ne(),
                                se:local.two.se(),
                                sw:local.two.sw()
                            },
                            three:{
                                nw:local.three.nw(),
                                ne:local.three.ne(),
                                se:local.three.se(),
                                sw:local.three.sw()
                            }
                        }
                        return hash;
                    }
                }
                return local;
            }
            var clone={
                zero:metric.zero(),
                one:metric.one(),
                two:metric.two(),
                three:metric.three(),
                composite:composite
            }
            return clone;
        },
        hash:function(){
            var local=memory.map();
            var composite=local.composite(),
            clone={
                even:{
                    plus:[local.one,local.two],
                    minus:[local.two,local.one]
                },
                odd:{
                    plus:[local.zero,local.three],
                    minus:[local.three,local.zero]
                },
                hash:composite.map
            }
            return clone;
        },
        order:function(){
            var local=memory.hash();
            var clone={
                basis:{
                    A:[[0,3],[1,2]],
                    B:[[1,2],[3,0]],
                    C:[[2,1],[0,3]],
                    D:[[3,0],[2,1]]
                },
                A:[local.odd.plus,local.even.plus],
                B:[local.even.plus,local.odd.minus],
                C:[local.even.minus,local.odd.plus],
                D:[local.odd.minus,local.even.minus]
            }
            return clone;
        }
    }
    Universe=function(){
        var local=memory.order();
        var clone={
            definition:{
                AD:[local.A,local.D],
                BC:[local.B,local.C],
                DA:[local.D,local.A],
                CB:[local.C,local.B]
            },
            implication:{
                AD:local.B,
                BC:local.D,
                DA:local.C,
                CB:local.A
            },
            basis:local.basis
        }
        return clone;
    }
    Universe.time=time
})()
