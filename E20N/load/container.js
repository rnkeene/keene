/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var instance=request.instance();
    var stylesheet=instance.style();
    var selector=stylesheet.rule("static")
    var style=stylesheet.rule("limit")
    var div=instance.element("div");
    selector.proxy(div.instance())
    style.proxy(div.instance())
    var at=null;
    var down=function(event){
        at=[event.clientX,event.clientY]
    }
    var start=instance.behavior("mousedown")
    start.at.proxy(div.instance())
    start.decorate(down)
    
    var up=function(event){
        if(at[0]!=event.clientX && at[1]!=event.clientY){
            console.warn("Coordinates:",stylesheet.coordinate(at,[event.clientX,event.clientY]).instance())
        }
    }
    var stop=instance.behavior("mouseup")
    stop.at.proxy(div.instance())
    stop.decorate(up)
})()

/**
*  <body> static style (done)
*  
*  (1) Create a <div>
*  (2) Add styles (copy of <body>?)--- need maxX,maxY for width/height.
*          ;;;; //assume (0,0)min to {x,y}max; unless (x,y)min is overridden
*  (3) Add behaviors/chain (mousedown -> mouseup)
*  (4) Handle: { Forward vs Reset }
*/