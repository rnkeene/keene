/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var clean=function(title,head,tail){
        head.innerHTML="<title>"+title+"</title>";
        tail.innerHTML="";
        tail.style="";
    }
    var loading=function(){
        window.removeEventListener("load",loading,true);
        loading=null,delete loading;
        var root=document.head;
        var tail=document.body;
        tail.style.position="static";
        tail.style.overflow="hidden",
        tail.style.margin="0px",
        tail.style.padding="0px";
        clean("Arena",root,tail)
        var space=Space(tail);
        var forward=function(intercept){
            var coordinates=intercept;
            var destroy=[handle.destroy,coordinates.destroy];
            handle.destroy=function(){
                destroy.shift()(),destroy.pop()();
            }
            coordinates.destroy=function(){
                handle.destroy()
            }
            fractals.unshift(coordinates)
            Chaos(handle)
        }
        
        space.law.register(forward)
        var limits=Limits(tail,space.universe.particle());
        var fractals=[];
        var handle={
            fractals:function(){
                return fractals
            },
            space:function(){
                return space
            },
            walls:function(){
                return limits.walls()
            },
            element:function(){
                return space.universe.particle()
            },
            toggle:function(){
                space.law.toggle();
            },
            destroy:function(){
                space.destroy();
                limits.destroy();
                clean("DESTROYED",root,tail);
                space=null,delete space,
                limits=null,delete limits,
                forward=null,delete forward,
                fractals=null,delete fractals,
                handle.space=null,delete handle.space,
                handle.walls=null,delete handle.walls,
                handle.element=null,delete handle.element,
                handle.toggle=null,delete handle.toggle,
                handle.destroy=null,delete handle.destroy,
                handle.fractals=null,delete handle.fractals,
                handle=null,delete handle
            }
        }
        
    }
    window.addEventListener("load",loading,true)
})()