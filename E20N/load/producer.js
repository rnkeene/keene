/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var process=horizon.instance();
    var stylesheet=process.stylesheet()
    var selector=stylesheet.selector();
    selector.declare("width","0px")
    selector.declare("height","0px")
    stylesheet.apply(selector)
    
    var limits=function(container){
        var limit=null,
        data=null,
        hold=container;
        var transform=function(){
            selector.undo()
            selector.declare("width",walls.width+"px")
            selector.declare("height",walls.height+"px")
            stylesheet.apply(selector)
        }
        var collect=function(){
            if(limit){
                limit.destroy()
            }
            limit=stylesheet.structure(0,0,window.innerWidth,window.innerHeight)
            data=limit.data();
            transform()
        }
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup;
                window.removeEventListener("resize",collect,true);
            }
            window.addEventListener("resize",collect,true);
        }
        collect()
        toggle()
        
        var agent=function(target){
            var element=target;
            var follow=function(){
                selector.proxy(element)
            }
            var ignore=function(){
                selector.proxy(element,true)
            }
            var toggle=function(){
                var backup=toggle;
                toggle=function(){
                    toggle=backup;
                    ignore()
                }
                follow()
            }
            return clone
        }
        var proxy={
            data:function(){
                return data
            },
            source:function(){
                return limit
            },
            register:function(element){
                return agent(element)
            }
        }
        proxy.decorate(hold)
        var clone={
            toggle:function(){
                toggle()
            },
            instance:function(){
                return proxy
            },
            destroy:function(){
                window.removeEventListener("resize",collect,true);
                limit.destroy();
                walls=null,delete walls,
                transform=null,delete transform,
                collect=null,delete collect,
                toggle=null,delete toggle,
                proxy.walls=null,delete proxy.walls,
                proxy.limit=null,delete proxy.limit,
                proxy.decorate=null,delete proxy.decorate,
                proxy=null,delete proxy,
                clone.toggle=null,delete clone.toggle,
                clone.instance=null,delete clone.instance,
                clone.destroy=null,delete clone.destroy,
                clone=null,delete clone,
                limit=null,delete limit;
            }
        }
        return clone;
    }
    stylesheet.limits=function(container){
        return limits(container)
    }
    horizon.forward("space")
})()