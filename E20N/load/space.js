/***
 *                )          
 *     (       ( /(   (      
 *     )\ )    )\())  )\     
 *    (()/(   ((_)\((((_)(   
 *     /(_))_  _((_))\ _ )\  
 *    (_)) __|| \| |(_)_\(_) 
 *      | (_ || .` | / _ \   
 *       \___||_|\_|/_/ \_\
 *      
 *  Total Nonstop Action (Browser/User)
 */
(function(){
    var process=horizon.instance();
    var stylesheet=process.stylesheet()
    var selector=stylesheet.selector()
    selector.declare("cursor","ne-resize")
    selector.declare("cursor","se-resize")
    selector.declare("cursor","sw-resize")
    selector.declare("cursor","nw-resize")
    selector.declare("cursor","move")
    stylesheet.apply(selector);
    
    
    var alter=function(to){
        stylesheet.alter(selector,0,to)
    }
    
    console.warn("Creating NEW space!")
    
    var space=function(container){
        var signal=null;
        var parent=container;
        var child=document.createElement("div");
        selector.proxy(child)
        stylesheet.limits(child);
        var head=null;
        var structure=[];
        var module=null;
        var follow=function(update){
            if(head[0]!=update.clientX && head[1]!=update.clientY){
                var space=stylesheet.space(head[0],head[1],update.clientX,update.clientY).space()
                if(signal!=space.state){
                    signal=space.state;
                    switch(space.state){
                        case "nw":
                            console.log('nw')
                            alter(1)
                            break
                        case "sw":
                            console.log('sw')
                            alter(2)
                            break
                        case "se":
                            console.log('se')
                            alter(3)
                            break
                        case "ne":
                            console.log('ne')
                            alter(4)
                            break
                    }
                }
                space.destroy();
            }
        }
        structure.unshift(function(event){
            head=[event.clientX,event.clientY];
            child.removeEventListener("mousedown",proxy.down,true),
            child.addEventListener("mouseup",proxy.up,true),
            child.addEventListener("mousemove",follow,true);
        });
        structure.unshift(function(event){
            child.removeEventListener("mousemove",follow,true)
            child.removeEventListener("mouseup",proxy.up,true)
            toggle()
            alter(0);
            if(event.clientX!=head[0] && event.clientY!=head[1]){
                module(stylesheet.space(head[0],head[1],event.clientX,event.clientY))
            }else{
                toggle()
            }
            head=null;
            signal=null;
        })
        var toggle=function(){
            var backup=toggle;
            toggle=function(){
                toggle=backup;
                child.removeEventListener("mousemove",follow,true),
                child.removeEventListener("mousedown",proxy.down,true),
                child.removeEventListener("mouseup",proxy.up,true);
            }
            child.addEventListener("mousedown",proxy.down,true);
        }
        var registered=function(){
            var swap={
                down:structure.pop(),
                up:structure.shift()
            }
            structure.unshift(swap.down);
            structure.push(swap.up);
            return swap
        }
        var proxy=registered(structure);
        toggle();
        
        var exist=function(){
            var backup=exist;
            exist=function(){
                exist=backup;
                parent.removeChild(child);
            }
            parent.appendChild(child);
        }
        exist();
        var space={
            universe:{
                element:function(){
                    return child
                },
                toggle:function(){
                    exist()
                }
            },
            law:{
                toggle:function(){
                    toggle()
                },
                handler:function(chain){
                    if(chain){
                        module=chain
                    }
                }
            },
            destroy:function(){
                parent.removeChild(child);
                child=null,delete child,
                structure=null,delete structure,
                head=null,delete head,
                structure=null,delete structure,
                module=null,delete module,
                follow=null,delete follow,
                registered=null,delete registered,
                proxy=null,delete proxy,
                toggle=null,delete toggle,
                exist=null,delete exist,
                space.universe.clone=null,delete space.universe.clone,
                space.universe.toggle=null,delete space.universe.toggle,
                space.universe.particle=null,delete space.universe.particle,
                space.universe=null,delete space.universe,
                space.law.toggle=null,delete space.law.toggle,
                space.law.register=null,delete space.law.register,
                space.law=null,delete space.law,
                space.destroy=null,delete space.destroy,
                space=null,delete space;
            }
        }
        return space
    }
    var gravity=space(process.structure());
    console.log("Gravity:",gravity)
})()