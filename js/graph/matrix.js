function(event){
    console.clear();
    var trigger = event;        
    var linear = event.timeStamp;
    var latency = new Date().getTime();
    console.log("Trigger: %o",trigger);
    console.log("Linear: %o",linear);
    console.log("Latency: %o",latency);
    var knowledge = [ linear , {
        space : function(){
            return [ 
            this.latency,
            function(){
                this[linear]=event;
            }
            ];
        },
        connected : function(){
            console.log("Building Knowledge Graph");
            return [ knowledge.space, {
                category:Category,
                location:Location,
                start:Start,
                verticies:Verticies,
                edges:Edges,
                inputs:Inputs,
                outputs:Outputs,
                states:States
            } ];
        },
        state : function(){
            console.log("Knowledge State");
            return knowledge.location[knowledge.scale];
        },
        vertex : function(){
            console.log("Knowledge Vertex");
            return 0;
        },
        events : function(name, ref, ordinal){
            console.log("Knowledge Events");
            return new CustomEvent(
                name,
                {
                    detail: {
                        message: ref,
                        time: ordinal
                    },
                    bubbles: true,
                    cancelable: true
                }
                );
        }
    }];
    function Category(){
        console.log("New Category");
    }
    function Start(){
        console.log("New Start");
    }
    function Verticies(){
        console.log("New Verticies");
    }
    function Edges(){
        console.log("New Edges/Arcs");
    }
    function Inputs(){
        console.log("New Inputs");
    }
    function Outputs(){
        console.log("New Outputs");
    }
    function States(){
        console.log("New (valid) States");
    }
    
    function Register(knowledge, space, triggers){
        console.log("Knowledge %o", knowledge);
        var space = knowledge.space();
        console.log("Space %o", space);
        console.log("Physics %o", space());
    }
}