exception driven framework.
/**
 *  |<|first|>|/|<|second|>|;|<|third|>?<|fourth|>|
 *  
 *  
 *  Pipes designate alternatives
 *  
 *  rules are separated from definitions by an equal "="
 *  
 *  literals are quoted with ""
 *  
 *  "(" and ")" are used to group elements
 *  
 *  "[" and "]"
 *  
 *  elements with <n>* to designate n or more repetitions of the following element; n defaults to 0
 *  
 *  alpha    = lowalpha | upalpha

      lowalpha = "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" |
                 "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" |
                 "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z"

      upalpha  = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" |
                 "J" | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" |
                 "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z"

      digit    = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" |
                 "8" | "9"

      alphanum = alpha | digit


My current plan for the "security considerations" section is that it will contain the following:

    Non-normative statements of fact about various security vulnerabilities that may affect Window implementations (unauthorized data access, phishing/pharming, frame injection, etc).
    A normative requirement that UAs implement some security policy that is in line with some general principles of cross-site scripting security, with exemptions allowed for "trusted" content. Some rough cuts at it (this language is not nearly precise as it should be, just trying to get the rules across):
        "A script executing in a browsing context currently presenting a document from one origin MUST NOT be allowed to access remote content from a different origin in such a way that the script can then access the contents." (where "same origin" is defined as same URI scheme, host and port, modulo differences for document.domain which I guess needs to be in the spec).
        "A non-trusted document MUST NOT be allowed to navigate a browsing context unless one of the following holds: the browsing contexts have the same top-level browsing context; the target browsing context has the same origin as the source browsing context; one of the ancestor browsing contexts of the target browsing context has the same origin as the source; OR the target browsing context is a top-level browsing context and the user can see the context's current location."
        "A non-trusted document MUST NOT be allowed to access or navigate to content from a file: URI in any way."
    Specific cases where access and navigation must be allowed, notwithstanding any additional security restrictions (anything from the same origin).
    Normative restrictions on what may be considered "trusted" content (MUST be from a specific known location, MUST NOT be transmitted over a network via an insecure protocol -- so you can't just say "my UA considers all content to be trusted" or "my company's homepage sent over non-SSL http is trusted".)
    An example of a specific security model that is believed to satisfy these constraints.

I would rather not have normative requirements on when UAs must or must not throw exceptions or return null or whatever, since these would merely represent our best current understanding of how to implement the security policy.

It may be that some of this belongs in the main body of the spec instead of in Security Considerations, let's figure that out when the security part is actually written.
// behavior is always special in ECMAScript, this is defined only for the benefit
// of other languages
interface TimerListener {
    // what to put here?
};


interface Location {
    attribute dom::DOMString href;

    // pieces of the URI, per the generic URI syntax
    attribute dom::DOMString hash;
    attribute dom::DOMString host;
    attribute dom::DOMString hostname;
    attribute dom::DOMString pathname;
    attribute dom::DOMString port;
    attribute dom::DOMString protocol;
    attribute dom::DOMString search;

    void assign(in dom::DOMString url);
    void replace(in dom::DOMString url);
    void reload();

    dom::DOMString toString();
};


interface DocumentWindow  : views::DocumentView {
    // ...
    // same special JS assignment as window.location
    readonly attribute Location location;
    // ...
};


interface Window : views::AbstractView {
    // ...

    // assigning this has special behavior in ECMAScript, but it is otherwise
    // read only. specifically, in ES a string URI can be assigned to location,
    // having the same effect as location.assign(URI)
    readonly attribute Location location;

    // ...
};

interface DocumentWindow : views::DocumentView {
    // ...
};

interface Window : views::AbstractView {
    // ...

    // self-references
    readonly attribute Window window;
    readonly attribute Window self;

    // ...
};



interface Window {
    // should timers allow more than long, maybe a floating point type?
    // don't think anyone's timers have more precision

    // one-shot timer
    long setTimeout(in TimerListener listener, in long milliseconds);
    void clearTimeout(in long timerID);

    // repeating timer
    long setInterval(in TimerListener listener, in long milliseconds);
    void clearInterval(in long timerID);
};


// must be on an object that also implements dom::Element
interface EmbeddingElement {
    readonly attribute dom::Document contentDocument;
    readonly attribute Window contentWindow;
};

interface Window {
    // name attribute of referencing frame/iframe/object, or name passed to
    // window.open
    attribute dom::DOMString name;

    // global object of containing document
    readonly attribute Window parent;

    // global object of outermost containing document
    readonly attribute Window top;

    // referencing <html:frame>, <html:iframe>, <html:object>, <svg:foreignObject>,
    // <svg:animation> or other embedding point, or null if none
    readonly attribute dom::Element frameElement;
};
 *
 *
 *  Reserved 
 *  
 *      uric          = reserved | unreserved | escaped
 *      
 *      URI character sequence->octet sequence->original character sequence
 *      
 *      
 *      A URI is represented as a sequence of characters, not as a sequence
        of octets. That is because URI might be "transported" by means that
        are not through a computer network, e.g., printed on paper, read over
        the radio, etc.


I'd recommend to use some standard symmetric cypher that is widely available like DES, 3DES or AES. While that is not the most secure algorithm, there are loads of implementations and you'd just need to give the key to anyone that is supposed to decrypt the information in the barcode. javax.crypto.Cipher is what you want to work with here.

Let's assume the bytes to encrypt are in
*
*
*   A URI scheme may define a mapping from URI characters to octets;
   whether this is done depends on the scheme. Commonly, within a
   delimited component of a URI, a sequence of characters may be used to
   represent a sequence of octets. For example, the character "a"
   represents the octet 97 (decimal), while the character sequence "%",
   "0", "a" represents the octet 10 (decimal).
*
*
*      In the simplest case, the original character sequence contains only
*      
*      characters that are defined in US-ASCII, and the two levels of
   mapping are simple and easily invertible: each 'original character'
   is represented as the octet for the US-ASCII code for it, which is,
   in turn, represented as either the US-ASCII character, or else the
   "%" escape sequence for that octet.

   For original character sequences that contain non-ASCII characters,
   however, the situation is more difficult. Internet protocols that
   transmit octet sequences intended to represent character sequences
   are expected to provide some way of identifying the charset used, if
   there might be more than one [RFC2277].  However, there is currently
   no provision within the generic URI syntax to accomplish this
   identification. An individual URI scheme may require a single
   charset, define a default charset, or provide a way to indicate the
   charset used.

   It is expected that a systematic treatment of character encoding
   within URI will be developed as a future modification of this
   specification.
*
*2.2. Reserved Characters

   Many URI include components consisting of or delimited by, certain
   special characters.  These characters are called "reserved", since
   their usage within the URI component is limited to their reserved
   purpose.  If the data for a URI component would conflict with the
   reserved purpose, then the conflicting data must be escaped before
   forming the URI.

      reserved    = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" |
                    "$" | ","

   The "reserved" syntax class above refers to those characters that are
   allowed within a URI, but which may not be allowed within a
   particular component of the generic URI syntax; they are used as
   delimiters of the components described in Section 3.







Berners-Lee, et. al.        Standards Track                     [Page 8]

RFC 2396                   URI Generic Syntax                August 1998


   Characters in the "reserved" set are not reserved in all contexts.
   The set of characters actually reserved within any given URI
   component is defined by that component. In general, a character is
   reserved if the semantics of the URI changes if the character is
   replaced with its escaped US-ASCII encoding.

2.3. Unreserved Characters

   Data characters that are allowed in a URI but do not have a reserved
   purpose are called unreserved.  These include upper and lower case
   letters, decimal digits, and a limited set of punctuation marks and
   symbols.

      unreserved  = alphanum | mark

      mark        = "-" | "_" | "." | "!" | "~" | "*" | "'" | "(" | ")"

   Unreserved characters can be escaped without changing the semantics
   of the URI, but this should not be done unless the URI is being used
   in a context that does not allow the unescaped character to appear.

2.4. Escape Sequences

   Data must be escaped if it does not have a representation using an
   unreserved character; this includes data that does not correspond to
   a printable character of the US-ASCII coded character set, or that
   corresponds to any US-ASCII character that is disallowed, as
   explained below.

2.4.1. Escaped Encoding

   An escaped octet is encoded as a character triplet, consisting of the
   percent character "%" followed by the two hexadecimal digits
   representing the octet code. For example, "%20" is the escaped
   encoding for the US-ASCII space character.

      escaped     = "%" hex hex
      hex         = digit | "A" | "B" | "C" | "D" | "E" | "F" |
                            "a" | "b" | "c" | "d" | "e" | "f"

2.4.2. When to Escape and Unescape

   A URI is always in an "escaped" form, since escaping or unescaping a
   completed URI might change its semantics.  Normally, the only time
   escape encodings can safely be made is when the URI is being created
   from its component parts; each component may have its own set of
   characters that are reserved, so only the mechanism responsible for
   generating or interpreting that component can determine whether or



Berners-Lee, et. al.        Standards Track                     [Page 9]

RFC 2396                   URI Generic Syntax                August 1998


   not escaping a character will change its semantics. Likewise, a URI
   must be separated into its components before the escaped characters
   within those components can be safely decoded.

   In some cases, data that could be represented by an unreserved
   character may appear escaped; for example, some of the unreserved
   "mark" characters are automatically escaped by some systems.  If the
   given URI scheme defines a canonicalization algorithm, then
   unreserved characters may be unescaped according to that algorithm.
   For example, "%7e" is sometimes used instead of "~" in an http URL
   path, but the two are equivalent for an http URL.

   Because the percent "%" character always has the reserved purpose of
   being the escape indicator, it must be escaped as "%25" in order to
   be used as data within a URI.  Implementers should be careful not to
   escape or unescape the same string more than once, since unescaping
   an already unescaped string might lead to misinterpreting a percent
   data character as another escaped character, or vice versa in the
   case of escaping an already escaped string.

2.4.3. Excluded US-ASCII Characters

   Although they are disallowed within the URI syntax, we include here a
   description of those US-ASCII characters that have been excluded and
   the reasons for their exclusion.

   The control characters in the US-ASCII coded character set are not
   used within a URI, both because they are non-printable and because
   they are likely to be misinterpreted by some control mechanisms.

   control     = <US-ASCII coded characters 00-1F and 7F hexadecimal>

   The space character is excluded because significant spaces may
   disappear and insignificant spaces may be introduced when URI are
   transcribed or typeset or subjected to the treatment of word-
   processing programs.  Whitespace is also used to delimit URI in many
   contexts.

   space       = <US-ASCII coded character 20 hexadecimal>

   The angle-bracket "<" and ">" and double-quote (") characters are
   excluded because they are often used as the delimiters around URI in
   text documents and protocol fields.  The character "#" is excluded
   because it is used to delimit a URI from a fragment identifier in URI
   references (Section 4). The percent character "%" is excluded because
   it is used for the encoding of escaped characters.

   delims      = "<" | ">" | "#" | "%" | <">



Berners-Lee, et. al.        Standards Track                    [Page 10]

RFC 2396                   URI Generic Syntax                August 1998


   Other characters are excluded because gateways and other transport
   agents are known to sometimes modify such characters, or they are
   used as delimiters.

   unwise      = "{" | "}" | "|" | "\" | "^" | "[" | "]" | "`"

   Data corresponding to excluded characters must be escaped in order to
   be properly represented within a URI.

3. URI Syntactic Components

   The URI syntax is dependent upon the scheme.  In general, absolute
   URI are written as follows:

      <scheme>:<scheme-specific-part>

   An absolute URI contains the name of the scheme being used (<scheme>)
   followed by a colon (":") and then a string (the <scheme-specific-
   part>) whose interpretation depends on the scheme.

   The URI syntax does not require that the scheme-specific-part have
   any general structure or set of semantics which is common among all
   URI.  However, a subset of URI do share a common syntax for
   representing hierarchical relationships within the namespace.  This
   "generic URI" syntax consists of a sequence of four main components:

      <scheme>://<authority><path>?<query>

   each of which, except <scheme>, may be absent from a particular URI.
   For example, some URI schemes do not allow an <authority> component,
   and others do not use a <query> component.

      absoluteURI   = scheme ":" ( hier_part | opaque_part )

   URI that are hierarchical in nature use the slash "/" character for
   separating hierarchical components.  For some file systems, a "/"
   character (used to denote the hierarchical structure of a URI) is the
   delimiter used to construct a file name hierarchy, and thus the URI
   path will look similar to a file pathname.  This does NOT imply that
   the resource is a file or that the URI maps to an actual filesystem
   pathname.

      hier_part     = ( net_path | abs_path ) [ "?" query ]

      net_path      = "//" authority [ abs_path ]

      abs_path      = "/"  path_segments




Berners-Lee, et. al.        Standards Track                    [Page 11]

RFC 2396                   URI Generic Syntax                August 1998


   URI that do not make use of the slash "/" character for separating
   hierarchical components are considered opaque by the generic URI
   parser.

      opaque_part   = uric_no_slash *uric

      uric_no_slash = unreserved | escaped | ";" | "?" | ":" | "@" |
                      "&" | "=" | "+" | "$" | ","

   We use the term <path> to refer to both the <abs_path> and
   <opaque_part> constructs, since they are mutually exclusive for any
   given URI and can be parsed as a single component.

3.1. Scheme Component

   Just as there are many different methods of access to resources,
   there are a variety of schemes for identifying such resources.  The
   URI syntax consists of a sequence of components separated by reserved
   characters, with the first component defining the semantics for the
   remainder of the URI string.

   Scheme names consist of a sequence of characters beginning with a
   lower case letter and followed by any combination of lower case
   letters, digits, plus ("+"), period ("."), or hyphen ("-").  For
   resiliency, programs interpreting URI should treat upper case letters
   as equivalent to lower case in scheme names (e.g., allow "HTTP" as
   well as "http").

      scheme        = alpha *( alpha | digit | "+" | "-" | "." )

   Relative URI references are distinguished from absolute URI in that
   they do not begin with a scheme name.  Instead, the scheme is
   inherited from the base URI, as described in Section 5.2.

3.2. Authority Component

   Many URI schemes include a top hierarchical element for a naming
   authority, such that the namespace defined by the remainder of the
   URI is governed by that authority.  This authority component is
   typically defined by an Internet-based server or a scheme-specific
   registry of naming authorities.

      authority     = server | reg_name

   The authority component is preceded by a double slash "//" and is
   terminated by the next slash "/", question-mark "?", or by the end of
   the URI.  Within the authority component, the characters ";", ":",
   "@", "?", and "/" are reserved.



Berners-Lee, et. al.        Standards Track                    [Page 12]

RFC 2396                   URI Generic Syntax                August 1998


   An authority component is not required for a URI scheme to make use
   of relative references.  A base URI without an authority component
   implies that any relative reference will also be without an authority
   component.

3.2.1. Registry-based Naming Authority

   The structure of a registry-based naming authority is specific to the
   URI scheme, but constrained to the allowed characters for an
   authority component.

      reg_name      = 1*( unreserved | escaped | "$" | "," |
                          ";" | ":" | "@" | "&" | "=" | "+" )

3.2.2. Server-based Naming Authority

   URL schemes that involve the direct use of an IP-based protocol to a
   specified server on the Internet use a common syntax for the server
   component of the URI's scheme-specific data:

      <userinfo>@<host>:<port>

   where <userinfo> may consist of a user name and, optionally, scheme-
   specific information about how to gain authorization to access the
   server.  The parts "<userinfo>@" and ":<port>" may be omitted.

      server        = [ [ userinfo "@" ] hostport ]

   The user information, if present, is followed by a commercial at-sign
   "@".

      userinfo      = *( unreserved | escaped |
                         ";" | ":" | "&" | "=" | "+" | "$" | "," )

   Some URL schemes use the format "user:password" in the userinfo
   field. This practice is NOT RECOMMENDED, because the passing of
   authentication information in clear text (such as URI) has proven to
   be a security risk in almost every case where it has been used.

   The host is a domain name of a network host, or its IPv4 address as a
   set of four decimal digit groups separated by ".".  Literal IPv6
   addresses are not supported.

      hostport      = host [ ":" port ]
      host          = hostname | IPv4address
      hostname      = *( domainlabel "." ) toplabel [ "." ]
      domainlabel   = alphanum | alphanum *( alphanum | "-" ) alphanum
      toplabel      = alpha | alpha *( alphanum | "-" ) alphanum



Berners-Lee, et. al.        Standards Track                    [Page 13]

RFC 2396                   URI Generic Syntax                August 1998


      IPv4address   = 1*digit "." 1*digit "." 1*digit "." 1*digit
      port          = *digit

   Hostnames take the form described in Section 3 of [RFC1034] and
   Section 2.1 of [RFC1123]: a sequence of domain labels separated by
   ".", each domain label starting and ending with an alphanumeric
   character and possibly also containing "-" characters.  The rightmost
   domain label of a fully qualified domain name will never start with a
   digit, thus syntactically distinguishing domain names from IPv4
   addresses, and may be followed by a single "." if it is necessary to
   distinguish between the complete domain name and any local domain.
   To actually be "Uniform" as a resource locator, a URL hostname should
   be a fully qualified domain name.  In practice, however, the host
   component may be a local domain literal.

      Note: A suitable representation for including a literal IPv6
      address as the host part of a URL is desired, but has not yet been
      determined or implemented in practice.

   The port is the network port number for the server.  Most schemes
   designate protocols that have a default port number.  Another port
   number may optionally be supplied, in decimal, separated from the
   host by a colon.  If the port is omitted, the default port number is
   assumed.

3.3. Path Component

   The path component contains data, specific to the authority (or the
   scheme if there is no authority component), identifying the resource
   within the scope of that scheme and authority.

      path          = [ abs_path | opaque_part ]

      path_segments = segment *( "/" segment )
      segment       = *pchar *( ";" param )
      param         = *pchar

      pchar         = unreserved | escaped |
                      ":" | "@" | "&" | "=" | "+" | "$" | ","

   The path may consist of a sequence of path segments separated by a
   single slash "/" character.  Within a path segment, the characters
   "/", ";", "=", and "?" are reserved.  Each path segment may include a
   sequence of parameters, indicated by the semicolon ";" character.
   The parameters are not significant to the parsing of relative
   references.





Berners-Lee, et. al.        Standards Track                    [Page 14]

RFC 2396                   URI Generic Syntax                August 1998


3.4. Query Component

   The query component is a string of information to be interpreted by
   the resource.

      query         = *uric

   Within a query component, the characters ";", "/", "?", ":", "@",
   "&", "=", "+", ",", and "$" are reserved.

4. URI References

   The term "URI-reference" is used here to denote the common usage of a
   resource identifier.  A URI reference may be absolute or relative,
   and may have additional information attached in the form of a
   fragment identifier.  However, "the URI" that results from such a
   reference includes only the absolute URI after the fragment
   identifier (if any) is removed and after any relative URI is resolved
   to its absolute form.  Although it is possible to limit the
   discussion of URI syntax and semantics to that of the absolute
   result, most usage of URI is within general URI references, and it is
   impossible to obtain the URI from such a reference without also
   parsing the fragment and resolving the relative form.

      URI-reference = [ absoluteURI | relativeURI ] [ "#" fragment ]

   The syntax for relative URI is a shortened form of that for absolute
   URI, where some prefix of the URI is missing and certain path
   components ("." and "..") have a special meaning when, and only when,
   interpreting a relative path.  The relative URI syntax is defined in
   Section 5.

4.1. Fragment Identifier

   When a URI reference is used to perform a retrieval action on the
   identified resource, the optional fragment identifier, separated from
   the URI by a crosshatch ("#") character, consists of additional
   reference information to be interpreted by the user agent after the
   retrieval action has been successfully completed.  As such, it is not
   part of a URI, but is often used in conjunction with a URI.

      fragment      = *uric

   The semantics of a fragment identifier is a property of the data
   resulting from a retrieval action, regardless of the type of URI used
   in the reference.  Therefore, the format and interpretation of
   fragment identifiers is dependent on the media type [RFC2046] of the
   retrieval result.  The character restrictions described in Section 2



Berners-Lee, et. al.        Standards Track                    [Page 15]

RFC 2396                   URI Generic Syntax                August 1998


   for URI also apply to the fragment in a URI-reference.  Individual
   media types may define additional restrictions or structure within
   the fragment for specifying different types of "partial views" that
   can be identified within that media type.

   A fragment identifier is only meaningful when a URI reference is
   intended for retrieval and the result of that retrieval is a document
   for which the identified fragment is consistently defined.

4.2. Same-document References

   A URI reference that does not contain a URI is a reference to the
   current document.  In other words, an empty URI reference within a
   document is interpreted as a reference to the start of that document,
   and a reference containing only a fragment identifier is a reference
   to the identified fragment of that document.  Traversal of such a
   reference should not result in an additional retrieval action.
   However, if the URI reference occurs in a context that is always
   intended to result in a new request, as in the case of HTML's FORM
   element, then an empty URI reference represents the base URI of the
   current document and should be replaced by that URI when transformed
   into a request.

4.3. Parsing a URI Reference

   A URI reference is typically parsed according to the four main
   components and fragment identifier in order to determine what
   components are present and whether the reference is relative or
   absolute.  The individual components are then parsed for their
   subparts and, if not opaque, to verify their validity.

   Although the BNF defines what is allowed in each component, it is
   ambiguous in terms of differentiating between an authority component
   and a path component that begins with two slash characters.  The
   greedy algorithm is used for disambiguation: the left-most matching
   rule soaks up as much of the URI reference string as it is capable of
   matching.  In other words, the authority component wins.

   Readers familiar with regular expressions should see Appendix B for a
   concrete parsing example and test oracle.

5. Relative URI References

   It is often the case that a group or "tree" of documents has been
   constructed to serve a common purpose; the vast majority of URI in
   these documents point to resources within the tree rather than





Berners-Lee, et. al.        Standards Track                    [Page 16]

RFC 2396                   URI Generic Syntax                August 1998


   outside of it.  Similarly, documents located at a particular site are
   much more likely to refer to other resources at that site than to
   resources at remote sites.

   Relative addressing of URI allows document trees to be partially
   independent of their location and access scheme.  For instance, it is
   possible for a single set of hypertext documents to be simultaneously
   accessible and traversable via each of the "file", "http", and "ftp"
   schemes if the documents refer to each other using relative URI.
   Furthermore, such document trees can be moved, as a whole, without
   changing any of the relative references.  Experience within the WWW
   has demonstrated that the ability to perform relative referencing is
   necessary for the long-term usability of embedded URI.

   The syntax for relative URI takes advantage of the <hier_part> syntax
   of <absoluteURI> (Section 3) in order to express a reference that is
   relative to the namespace of another hierarchical URI.

      relativeURI   = ( net_path | abs_path | rel_path ) [ "?" query ]

   A relative reference beginning with two slash characters is termed a
   network-path reference, as defined by <net_path> in Section 3.  Such
   references are rarely used.

   A relative reference beginning with a single slash character is
   termed an absolute-path reference, as defined by <abs_path> in
   Section 3.

   A relative reference that does not begin with a scheme name or a
   slash character is termed a relative-path reference.

      rel_path      = rel_segment [ abs_path ]

      rel_segment   = 1*( unreserved | escaped |
                          ";" | "@" | "&" | "=" | "+" | "$" | "," )

   Within a relative-path reference, the complete path segments "." and
   ".." have special meanings: "the current hierarchy level" and "the
   level above this hierarchy level", respectively.  Although this is
   very similar to their use within Unix-based filesystems to indicate
   directory levels, these path components are only considered special
   when resolving a relative-path reference to its absolute form
   (Section 5.2).

   Authors should be aware that a path segment which contains a colon
   character cannot be used as the first segment of a relative URI path
   (e.g., "this:that"), because it would be mistaken for a scheme name.




Berners-Lee, et. al.        Standards Track                    [Page 17]

RFC 2396                   URI Generic Syntax                August 1998


   It is therefore necessary to precede such segments with other
   segments (e.g., "./this:that") in order for them to be referenced as
   a relative path.

   It is not necessary for all URI within a given scheme to be
   restricted to the <hier_part> syntax, since the hierarchical
   properties of that syntax are only necessary when relative URI are
   used within a particular document.  Documents can only make use of
   relative URI when their base URI fits within the <hier_part> syntax.
   It is assumed that any document which contains a relative reference
   will also have a base URI that obeys the syntax.  In other words,
   relative URI cannot be used within a document that has an unsuitable
   base URI.

   Some URI schemes do not allow a hierarchical syntax matching the
   <hier_part> syntax, and thus cannot use relative references.

5.1. Establishing a Base URI

   The term "relative URI" implies that there exists some absolute "base
   URI" against which the relative reference is applied.  Indeed, the
   base URI is necessary to define the semantics of any relative URI
   reference; without it, a relative reference is meaningless.  In order
   for relative URI to be usable within a document, the base URI of that
   document must be known to the parser.

   The base URI of a document can be established in one of four ways,
   listed below in order of precedence.  The order of precedence can be
   thought of in terms of layers, where the innermost defined base URI
   has the highest precedence.  This can be visualized graphically as:

      .----------------------------------------------------------.
      |  .----------------------------------------------------.  |
      |  |  .----------------------------------------------.  |  |
      |  |  |  .----------------------------------------.  |  |  |
      |  |  |  |  .----------------------------------.  |  |  |  |
      |  |  |  |  |       <relative_reference>       |  |  |  |  |
      |  |  |  |  `----------------------------------'  |  |  |  |
      |  |  |  | (5.1.1) Base URI embedded in the       |  |  |  |
      |  |  |  |         document's content             |  |  |  |
      |  |  |  `----------------------------------------'  |  |  |
      |  |  | (5.1.2) Base URI of the encapsulating entity |  |  |
      |  |  |         (message, document, or none).        |  |  |
      |  |  `----------------------------------------------'  |  |
      |  | (5.1.3) URI used to retrieve the entity            |  |
      |  `----------------------------------------------------'  |
      | (5.1.4) Default Base URI is application-dependent        |
      `----------------------------------------------------------'
*
**
*

For each URI reference, the following steps are performed in order:

   1) The URI reference is parsed into the potential four components and
      fragment identifier, as described in Section 4.3.

   2) If the path component is empty and the scheme, authority, and
      query components are undefined, then it is a reference to the
      current document and we are done.  Otherwise, the reference URI's
      query and fragment components are defined as found (or not found)
      within the URI reference and not inherited from the base URI.

   3) If the scheme component is defined, indicating that the reference
      starts with a scheme name, then the reference is interpreted as an
      absolute URI and we are done.  Otherwise, the reference URI's
      scheme is inherited from the base URI's scheme component.

      Due to a loophole in prior specifications [RFC1630], some parsers
      allow the scheme name to be present in a relative URI if it is the
      same as the base URI scheme.  Unfortunately, this can conflict
      with the correct parsing of non-hierarchical URI.  For backwards
      compatibility, an implementation may work around such references
      by removing the scheme if it matches that of the base URI and the
      scheme is known to always use the <hier_part> syntax.  The parser

  can then continue with the steps below for the remainder of the
      reference components.  Validating parsers should mark such a
      misformed relative reference as an error.

   4) If the authority component is defined, then the reference is a
      network-path and we skip to step 7.  Otherwise, the reference
      URI's authority is inherited from the base URI's authority
      component, which will also be undefined if the URI scheme does not
      use an authority component.

   5) If the path component begins with a slash character ("/"), then
      the reference is an absolute-path and we skip to step 7.

   6) If this step is reached, then we are resolving a relative-path
      reference.  The relative path needs to be merged with the base
      URI's path.  Although there are many ways to do this, we will
      describe a simple method using a separate string buffer.

      a) All but the last segment of the base URI's path component is
         copied to the buffer.  In other words, any characters after the
         last (right-most) slash character, if any, are excluded.

      b) The reference's path component is appended to the buffer
         string.

      c) All occurrences of "./", where "." is a complete path segment,
         are removed from the buffer string.

      d) If the buffer string ends with "." as a complete path segment,
         that "." is removed.

      e) All occurrences of "<segment>/../", where <segment> is a
         complete path segment not equal to "..", are removed from the
         buffer string.  Removal of these path segments is performed
         iteratively, removing the leftmost matching pattern on each
         iteration, until no matching pattern remains.

      f) If the buffer string ends with "<segment>/..", where <segment>
         is a complete path segment not equal to "..", that
         "<segment>/.." is removed.

      g) If the resulting buffer string still begins with one or more
         complete path segments of "..", then the reference is
         considered to be in error.  Implementations may handle this
         error by retaining these components in the resolved path (i.e.,
         treating them as part of the final URI), by removing them from
         the resolved path (i.e., discarding relative levels above the
         root), or by avoiding traversal of the reference.

  h) The remaining buffer string is the reference URI's new path
         component.

   7) The resulting URI components, including any inherited from the
      base URI, are recombined to give the absolute form of the URI
      reference.  Using pseudocode, this would be

         result = ""

         if scheme is defined then
             append scheme to result
             append ":" to result

         if authority is defined then
             append "//" to result
             append authority to result

         append path to result

         if query is defined then
             append "?" to result
             append query to result

         if fragment is defined then
             append "#" to result
             append fragment to result

         return result


 Note that we must be careful to preserve the distinction between a
      component that is undefined, meaning that its separator was not
      present in the reference, and a component that is empty, meaning
      that the separator was present and was immediately followed by the
      next component separator or the end of the reference.
 The above algorithm is intended to provide an example by which the
   output of implementations can be tested -- implementation of the
   algorithm itself is not required.  For example, some systems may find
   it more efficient to implement step 6 as a pair of segment stacks
   being merged, rather than as a series of string pattern replacements.

      Note: Some WWW client applications will fail to separate the
      reference's query component from its path component before merging
      the base and reference paths in step 6 above.  This may result in
      a loss of information if the query component contains the strings
      "/../" or "/./".


 *
 *URI-reference = [ absoluteURI | relativeURI ] [ "#" fragment ]
      absoluteURI   = scheme ":" ( hier_part | opaque_part )
      relativeURI   = ( net_path | abs_path | rel_path ) [ "?" query ]

      hier_part     = ( net_path | abs_path ) [ "?" query ]
      opaque_part   = uric_no_slash *uric

      uric_no_slash = unreserved | escaped | ";" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","

      net_path      = "//" authority [ abs_path ]
      abs_path      = "/"  path_segments
      rel_path      = rel_segment [ abs_path ]

      rel_segment   = 1*( unreserved | escaped |
                          ";" | "@" | "&" | "=" | "+" | "$" | "," )

      scheme        = alpha *( alpha | digit | "+" | "-" | "." )

      authority     = server | reg_name
      reg_name      = 1*( unreserved | escaped | "$" | "," |
                          ";" | ":" | "@" | "&" | "=" | "+" )
      server        = [ [ userinfo "@" ] hostport ]
      userinfo      = *( unreserved | escaped |
                         ";" | ":" | "&" | "=" | "+" | "$" | "," )
      hostport      = host [ ":" port ]
      host          = hostname | IPv4address
      hostname      = *( domainlabel "." ) toplabel [ "." ]
      domainlabel   = alphanum | alphanum *( alphanum | "-" ) alphanum
      toplabel      = alpha | alpha *( alphanum | "-" ) alphanum
      IPv4address   = 1*digit "." 1*digit "." 1*digit "." 1*digit
      port          = *digit
      path          = [ abs_path | opaque_part ]
      path_segments = segment *( "/" segment )
      segment       = *pchar *( ";" param )
      param         = *pchar
      pchar         = unreserved | escaped |
                      ":" | "@" | "&" | "=" | "+" | "$" | ","
      query         = *uric
      fragment      = *uric
      uric          = reserved | unreserved | escaped
      reserved      = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" | "$" | ","
      unreserved    = alphanum | mark
      mark          = "-" | "_" | "." | "!" | "~" | "*" | "'" | "(" | ")"

      escaped       = "%" hex hex
      hex           = digit | "A" | "B" | "C" | "D" | "E" | "F" | "a" | "b" | "c" | "d" | "e" | "f"

      alphanum      = alpha | digit
      alpha         = lowalpha | upalpha

      lowalpha = "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" |
                 "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" |
                 "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z"
      upalpha  = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" |
                 "J" | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" |
                 "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z"
      digit    = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" |
                 "8" | "9"
 *
 */
(function(){
    window.metalang = {
        history:{
            feature:function(){
                return {
                    A:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],
                    B:["a","b","c","d","e","f","g","h","i","j",11,12,function(){return Language.Periodic.G()},function(){return Language.Periodic.F()},function(){return Language.Periodic.E()},function(){return Language.Periodic.D()},function(){return Language.Periodic.C()},function(){return Language.Periodic.B()},function(){return Language.Periodic.A()}],
                    C:["IA","IIA","IIIA","IVA","VA","VIA","VIIA","VIII","VIII","VIII","IB","IIB","IIIB","IVB","IVB","VB","VIB","VIIB",0],
                    D:["IA","IIA","IIIB","IVB","VB","VIB","VIIB","VIIIB","VIIIB","VIIIB","IB","IIB","IIIA","IVA","VA","VIA","VIIA","VIIIA"]
                }
            },
            G:function(){
                var gna = Language.Periodic.H();
                return {
                    F: Language.Periodic.F,
                    G: Language.Periodic.G,
                    H: Language.Periodic.Pnictogen,Language.Periodic.Chalcogen,Language.Periodic.Halogen,Language.Periodic.Noble]
                }
            },
            F:function(){
                
            },
            E:function(){
                
            },
            D:function(){
                
            },
            C:function(){
                
            },
            B:function(){
                
            },
            A:function(){
                
            }
        }
    }
    Language.Manifold = {
        e:[],
        m:[],
        c:[],
        loge:[],
        Symmetry:[{A:{id:[1,2,3,4],R:{id:[2,3,1,4],R:{id:[3,1,2,4],L:{id:[4,3,1,2],R:{id:[3,1,4,2],R:{id:[1,4,3,2]}}}}}},B:{id:[4,1,2,3],R:{id:[1,2,4,3],R:{id:[2,4,1,3],L:{id:[3,2,4,1],R:{id:[2,4,3,1],R:{id:[4,3,2,1]}}}}}},C:{id:[3,4,1,2],R:{id:[4,1,3,2],R:{id:[1,3,4,2],L:{id:[2,1,3,4],R:{id:[1,3,2,4],R:{id:[3,2,1,4]}}}}}},D:{id:[2,3,4,1],R:{id:[3,4,2,1],R:{id:[4,2,3,1],L:{id:[1,4,2,3],R:{id:[3,1,4,2],R:{id:[2,3,1,4]}}}}}}}],
        half:function(){
            var a=[function(){prime.A.R.R.L},function(){prime.A}]
            var b=[function(){prime.B.R.R.L},function(){prime.B}]
            var c=[function(){prime.C.R.R.L},function(){prime.B}]
            var c=[y: prime.,prime.C],
            D:{
                y: prime.A.R.R.L,
                dyer: prime.D
            }
        }
    }
    Manifold:function(){
        var bang = function(){
            var time = function(){
                var clone = Date.now();
                return function(){
                    return clone;
                }
            }
            var language = function(time){
                Language.LTR.push(time());
                Language.RTL.push(1/Language.LTR[Language.LTR.length-1]);
                Language.BLTR.push(Language.LTR[Language.LTR.length-1].toString(2));
                Language.BRTL.push(Language.RTL[Language.RTL.length-1].toString(2));
                return Language.BLTR[Language.BLTR.length-1];
            }
            var natural = function(){
                return language(time());
            }
            var grammar = function(natural){
                var syntax = natural();
                var clone = {
                    category:syntax.split(''),
                    full:syntax.split('1'),
                    empty:syntax.split('0')
                }
                return clone
            }
            return function(){
                var relative = function(){
                    return grammar(natural)
                }
                return function(){
                    return relative()
                }
            }
        }
        var pattern = function(){
            var atomic = bang()();
            var electron = atomic();
            var clone = {
                sqr:"+",
                log:"-",
                e:1,
                E:1,
                frequency:"/",
                time:"t",
                toAtomic:function(){
                    return electron
                },
                toState:function(){
                    return {
                        square:clone.sqr,
                        half:clone.log,
                        exponent:clone.e,
                        energy:clone.E,
                        resistance:clone.frequency,
                        current:clone.time
                    }
                },
                toSquare:function(){
                    return {
                        x:[clone.sqr,clone.log,clone.e,clone.E,clone.frequency],
                        y:[clone.time,clone.sqr,clone.log,clone.e,clone.E,clone.frequency],
                        dx:[clone.sqr,clone.log,clone.e,clone.frequency,clone.E],
                        dy:[clone.time,clone.sqr,clone.log,clone.E,clone.e,clone.frequency],
                        dt:[clone.sqr,clone.log,clone.E,clone.e,clone.frequency]
                    }
                },
                toRound:function(){
                    var r = 0;
                    var current = {
                        dxdt:[r++,clone.sqr,clone.log,clone.e,clone.frequency,clone.E],
                        dydt:[clone.time,clone.sqr,clone.log,clone.e,clone.frequency,clone.E],
                        dsdt:[r++,clone.sqr,clone.log,clone.e,clone.frequency,clone.E]
                    }
                    return current;
                },
                Clone:function(){
                    return pattern()
                }
            }
            return clone
        }
        return pattern();
    },
    Pulse:function(){
        var g = Language.Manifold();
        var emp = {
            pattern:g,
            atom:g.toAtomic(),
            electron:g.toSquare(),
            from:g.toState(),
            to:g.toSquare(),
            when:g.toRound()
        }
        return emp;
    },
    Observe:function(emp){
        var observer = emp()
        var reading = observer.atom.category
        var log = observer.atom.full;
        var emc2 = observer.atom.empty;
        var dimension = {
            square:[],
            round:[]
        }
        var half = {
            empty:{
                dimension:[],
                size:[]
            },
            full:{
                dimension:[],
                size:[]
            }
        }
        for(var x=0; x<reading.length; x++){                    
            switch(reading[x]){
                case "0": 
                    dimension.square.push(["-",x])
                    break;
                case "1":
                    dimension.round.push(["+",x])
                    break;
            }
        }
        for(var dx=0; dx<emc2.length; dx++){
            var check = emc2[dx].length;
            if(check>0){
                half.full.dimension.push(dx)
                half.full.size.push(check)
            }
        }
        for(var dy=0; dy<log.length; dy++){
            var empty = log[dy].length;
            if(empty>0){
                half.empty.dimension.push(dy)
                half.empty.size.push(empty)
            }
        }
        var symmetry = {
            identity:dimension,
            gravity:observer,
            ground:half.empty,
            excited:half.full,
            steady:reading
        }
        return symmetry;
    },
    Memory:function(){
        var grammar = Language.Observe(Language.Pulse);
        Language.Debug(grammar);
    },
    Debug:function(symmetry){
        console.log("Angle: ",symmetry.gravity.pattern)
        console.log("Vector: ",symmetry.gravity.atom)
        console.log("Convex: ",symmetry.gravity.electron)
        console.log("Concave: ",symmetry.gravity.from)
        console.log("Euclidean: ",symmetry.gravity.to)
        console.log("Scalar: ",symmetry.gravity.when)
            
        console.log("Identity-Square: ",symmetry.identity)
        console.log("Ground-Dimension: ",symmetry.ground)
        console.log("Excited-Dimension: ",symmetry.excited)
            
        console.log("Readings: ",symmetry.steady)
            
    },
    Cayley:function()
    var complete = {
        algorithm:[prime.A,prime.B,prime.C,prime.D],
        sinusoid:half,
        cosign:2,
        tangent:0,
        fx:function(){
            var symmetry = [[complete.algorithm[0]],[complete.algorithm[1]],[complete.algorithm[2]],[complete.algorithm[3]]];
            var category = [];
            var balance = [];
            console.groupCollapsed("F(x)");
            while(symmetry.length>0){
                category.push((symmetry.pop())[0])
                console.groupCollapsed("RTL",category.length)
                balance.push([category.length,symmetry.length]);
                while(category[category.length-1].R){
                    console.groupCollapsed("Category",category.length)
                    console.log(category[category.length-1].id);
                    console.groupEnd();
                    category.push(category[category.length-1].R)
                }
                console.groupCollapsed("Symmetry",category.length)
                console.log(category[category.length-1].id);
                console.groupEnd();                                
                category.push(category[category.length-1].L)
                while(category[category.length-1].R){
                    console.groupCollapsed("LTR",category.length)
                    console.log(category[category.length-1].id);
                    console.groupEnd();
                    category.push(category[category.length-1].R)
                }
                console.groupEnd();
            }
            console.groupEnd();
            var clone = {
                beta:balance,
                alpha:category
            }
            symmetry = null;
            category=null;
            balance=null;
            complete.fx=null;
            delete symmetry;
            delete category;
            delete balance;
            delete complete.fx;
            return clone;
        }
    }
    var plane = {
        x:prime,
        y:half,                
        z:complete,
        debugFx:function(x){
            var flux = {
                field:["D","C","B","A"],
                sub:function(able){
                    var name = flux.field.pop();
                    var group = able[name];
                    var speed = name.length;
                    var density = group.length;
                    while(field.length){
                        var curlypi = {
                            id: name,
                            length:size,
                            attrib:group
                        }
                        var prettypoly = {
                            R: group.R,
                            RR: Group.R.R,
                            RRL: Group.R.R.L
                        }
                        console.groupCollapsed("Category X.%s-t%i",name,field.length)
                        console.log("%s-t%{}%o ",name,field.length,group);
                        console.log("%s-t%i.r: ",name,field.length,group.id);
                        console.log("%s-t%i:R ",name,field.length,group.R.id);
                        console.log("%s-t%i:RR ",name,field.length,group.R.R.id);
                        console.log("%s-t%i:RR| ",name,field.length,group.R.R.L.id);
                        console.groupEnd();
                        group = able[field.pop()]
                        return {
                            f:name,
                            g:group,
                            CurlyPi:curlypi,
                            PrettyPoly:prettypoly
                        }
                    }
                    console.groupCollapsed("Category Debug");
                    console.groupEnd();
                },
                able:function(){
                    sub(x);
                }
            }
            return flux;
        },
        debugFy:function(y){
            var field = ["D","C","B","A"]
            var sub = function(able){
                while(field.length){
                    console.groupCollapsed("Category Y");
                    console.log("y%s: ",group,(able[(group)]));
                    console.log(group,(able[(group)]));
                    console.log("\t%sa: ",group,(able[(group)]).id);
                    console.log("\t\ta: ",group,(able[(group)]).L.id);
                    console.log("\t\t\ta%s: ",group,(able[(group)]).L.R.id);
                    console.log("\t\t\t\t%sy: ",group,(able[(group)]).L.R.R.id);
                    console.groupEnd();
                }
                count = 0;
            }
            console.groupCollapsed("Category Debug");
            console.log(y);
            sub(y)
            console.groupEnd();
        },
        fx:function(){
            return plane.z.fx();
        }
    }
    var digital = function(){
        var field = plane;
        var fox = field.fx();
        Cayley = null;
        delete Cayley;
        return {
            r: field,
            R: fox
        }
    }
    prime = null;
    half = null;
    delete prime;
    delete half;
    return digital
}
}
})()
var compute = Language.Cayley()();
console.groupCollapsed("S {r,R} //symmetry");
console.log(compute);
console.groupCollapsed("dr{(x,y,z)}");
compute.r.debugFx(compute.r.x);
compute.r.debugFy(compute.r.y);
compute.r.debugFdy(compute.r.y);
compute.r.debug(compute.r.z);
console.groupCollapsed("dR{alpha,beta}: ");
console.log(compute.R)
console.log("alpha: ",compute.R.alpha.length);
console.log("beta: ",compute.R.beta.length);
console.groupEnd();
console.groupEnd();
console.groupEnd();
