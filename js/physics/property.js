function Property(event){
    this.view = function(){
        Scalar(this.space.activeText, this.space.feedback);
    }
    this.declare = function(){
        ignore("blur", window.knowledge, this.ignoreClick);
        this.space.removeChild(this.legend);
        this.space.feedback = prompt("Please add one Letter, Character or \"Word\" (or \"Expression\")?");
        this.space.removeChild(this.metadata);
        this.space.appendChild(this.legend);
        this.space.appendChild(this.metadata);
    }
    this.meta = function(){
        this.metadata = this.create("div");
        this.metadata.style.color="#0000FF";
        this.metadata.style.position="absolute";
        styleTip(this.metadata);
        this.metadata.innerHTML = "A <b>Letter</b> could be: a-z or A-Z";
        this.metadata.innerHTML += "<br /><br />A <b>Character</b> could be any Number or Symbol (+,-,=,?, etc).";
        this.metadata.innerHTML += "<br /><br />A <b>Word</b> such as a proper Noun, Verb, etc";
        this.metadata.innerHTML += "<br /><br />An <b>Expression</b> is likely a mathematical or other scientific statement (1+1=2).";
        this.metadata.style.border = "3px solid Black";
        this.metadata.style.backgroundColor = "#33FF00";
        this.quicktip.resize = function(){
            if(this.metadata){
                styleTip(this.metadata);
            }
        }
    }
    this.tip();
    
}
function DragKnowledge(event){
    /**
     * Some variables to make referencing some data simpler.
     */
    var elements = window.knowledge.space.elements[2];
    var index = elements.indexOf(event.target);
    var origX = window.knowledge.space.elements[0][index];
    var origY = window.knowledge.space.elements[1][index];
    var newX = event.clientX;
    var newY = event.clientY;
    /**
     * Add a reference to this Element and it's 'mouse-click' offset from the top-left corner.
     * If this information isn't saved then when the drop function is called the position will be off by some amount depending on the mouse click.
     */
    window.knowledge.space.elements[3] = [event.target];
    window.knowledge.space.elements[3].push(newX - origX);
    window.knowledge.space.elements[3].push(newY - origY);
    /**
     * This defines what will be seen (moved) when an Element (and its children) are being moved.
     */
    event.dataTransfer.setData("Element", event.target);
}
function IgnoreClick(){
    ignore("click", window.knowledge, Property);
    listen("click", window.knowledge, RestartKnowledge, true);    
}
function RestartKnowledge(){
    ignore("click", window.knowledge, RestartKnowledge);
    listen("click", window.knowledge, KnowledgeClick, false);
}
function KnowledgeClick(){
    ignore("click", window.knowledge, RestartKnowledge);
    listen("click", window.knowledge, Property, false);
}