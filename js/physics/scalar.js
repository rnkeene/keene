function Scalar(inputText, event){
    if(inputText){
        this.input = window.knowledge.create("input");
        this.input.type = "text";
        this.input.value= inputText;
        this.input.style.width = (inputText.length+4)*14;
        this.input.style.textAlign = "center";  
        this.input.style.fontFamily="monospace";
        this.input.style.fontSize=scaleFont();
        this.input.style.border="1px solid #000000";
        this.input.style.padding=scaleFont()*.5;
        this.input.style.backgroundColor="#FFFFFF";
        this.input.style.position="absolute";
        this.input.style.left = event.clientX;
        this.input.style.top = event.clientY;
        ListenKnowledgeEvents(this.input);
        window.knowledge.space.appendChild(this.input);
        /**
         * Add the element details to the space.elements array.
         */
        window.knowledge.space.elements[0].push(event.clientX);
        window.knowledge.space.elements[1].push(event.clientY);
        window.knowledge.space.elements[2].push(this.input);
    }
}