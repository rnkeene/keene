function ListenKnowledgeEvents(input){
    input.draggable = "true";
    listen("dragstart", input, DragKnowledge, true);
    listen("drop", input, DropKnowledge, true);
    listen("dragover", input, AllowDrop, true);
    listen("click", input, EditKnowledge, true);
}
function EditKnowledge(event){
    console.log("Edit Knowledge");
    console.log(event.target);
    IgnoreKnowledgeEvents(event);
    listen("keypress", event.target, UpdateKnowledgeLength, false);
    listen("blur", event.target, UpdateKnowledgeStyle, false);
    event.stopPropagation();
}
function IgnoreKnowledgeEvents(event){
    console.log("Ignore Knowledge");
    console.log(event.target);
    var knowledge = event.target;
    knowledge.style.backgroundColor="#FFFFCC";
    knowledge.style.fontWeight="bold";
    knowledge.draggable = "";
    ignore("dragstart", knowledge, DragKnowledge);
    ignore("drop", knowledge, DropKnowledge);
    ignore("dragover", knowledge, AllowDrop);
    ignore("click", knowledge, EditKnowledge);
    listen("change", knowledge, UpdateKnowledge, false);
}
function UpdateKnowledgeLength(event){
    console.log("Update Knowledge");
    console.log(event.target);
    var knowledge = event.target;
    knowledge.style.width = (event.target.value.length+4)*14;
}
function UpdateKnowledgeStyle(event){
    console.log("blur! knowledge style");
    console.log(event.target);
    var knowledge = event.target;
    knowledge.style.backgroundColor="#FFFFFF";
    knowledge.style.fontWeight="normal";
    ignore("blur", knowledge, UpdateKnowledgeStyle);
    ListenKnowledgeEvents(knowledge);
}
function UpdateKnowledge(event){
    console.log("An Update to the Knowledge!");
    console.log(event.target);
    UpdateKnowledgeStyle(event);
    ListenKnowledgeEvents(event.target);
    ignore("keypress", event.target, UpdateKnowledgeLength);
    IgnoreClick();
}