function Logger(){
    console.clear();
    this.object = window.document;
    this.name = "Logging Test";
    this.logHistory = [];
    this.process = "execute";
    function LogEvent(func, name, group, profile){
        this.func = func;
        this.name = name;
        this.group = group;
        this.profile = profile;
        this.start = function(){
            this.startGroup();
            this.logStart();
            this.profileStart();
        }
        this.finish = function(){
            this.profileFinish();
            this.logFinish();
            this.endGroup();
        }
        this.startGroup = function(){
            console.groupCollapsed(this.group);
        }
        this.endGroup = function(){
            console.groupEnd();
        }
        this.logStart = function(){
            console.time(this.name);
        }
        this.logFinish = function(){
            console.timeEnd(this.name);
        }
        this.profileStart = function(){
            console.profile(this.profile);
        }
        this.profileFinish = function(){
            console.profileEnd();
        }
        this.execute = function(){
            this.start();
            console.timeStamp("Executing:");
            console.info("%s", this.func);
            this.func();
            console.timeStamp("Finished Execution");
            this.finish();
        }
    }
    this.logLevels = function(){
        console.timeStamp("START log level function");
        console.log("This is an example of console.log");
        console.warn("This is an example of console.warn");
        console.debug("This is an example of console.debug");
        console.error("This is an example of console.error");
        console.info("This is an example of console.info");
        console.timeStamp("FINISH log level function");
    }
    this.logMETA = function(){
        console.info("Constructor: %o", obj.constructor);
        console.info("Prototype: %o", obj.prototype);
        console.info("__proto__: %o", obj.__proto__);
    }
    this.logAttributes = function(){
        /**
         *  Too Slow, no Use... just commenting out for now.
         */
        for(var prop in obj){
            if(prop){
                console.info("obj.%s=%s", obj[prop], prop);
            }
        }
    }
    this.inspect = function(){
        console.dir(obj);
    }
    this.trace = function(){
        console.trace();
    }
    this.watchAsyncScript = function(){
        console.timeStamp("In Timeout/Async Function");
        
        var script = document.createElement("script");
        
        console.timeStamp("Creating <script> Element");
        
        script.src = "js/script/script.js";
        
        console.timeStamp("Appending to <head>");
        
        document.getElementsByTagName("head")[0].appendChild(script);
        
        console.timeStamp("DOM Updated");
    }
    
    this.dynamicScriptCallback = function(){
        console.timeStamp("Success!");
    }
    this.printDocument = printDocument;
    this.sync = function(){
        var items = this.logHistory.concat();
        do{
            var item = items.shift();
            console.log(item);
            item[this.process]();
        }while(items.length>0);
    }
    this.async = function(eventsList, process){
        var events = eventsList.concat();
        setTimeout(function(){
            var event = events.shift();
            event[process]();
            if (events.length > 0){
                setTimeout(arguments.callee, 300);
            }
        }, 300);
    }
    this.tableView = function(){
        var columns = [
        {
            property:"name", 
            label: "Function Title"
        },

        {
            property:"group", 
            label: "Group Name"
        },

        {
            property:"profile", 
            label: "Profile Name"
        }
        ];
        console.table(this.logHistory, columns);
    }
    console.clear();
    this.logHistory.push(new LogEvent(this.logLevels, "Logging Overview","Log Level Examples","Logging Example Profile"));
    this.logHistory.push(new LogEvent(this.inspect, "Simple Full Object","Firebug Standard Details","Example Print Profile"));
    this.logHistory.push(new LogEvent(this.logMETA, "META Object","META-Properties","Meta Example Profile"));
    this.logHistory.push(new LogEvent(this.printDocument, "Custom Document View","Selected Document Logs","Custom Logging Profile"));
    this.logHistory.push(new LogEvent(this.watchAsyncScript, "Async <script> Load","Async <script> Loading Details","Async <script> Loading Profile"));
    
    //this.sync(this.logHistory, this.process, this);
    //this.async(this.logHistory, this.process, this);
}
function preventLoad(){
    var known = (typeof evt !== "undefined")? evt : event;
    if (known.preventDefault) {
        known.preventDefault();
    }
    else {
        event.returnValue = false;
    }
}
function printDocument(){
    console.info(document);
    console.info(document.constructor); /* HTMLBodyElement{} */
    console.info(document.URL); /* as it appears in the browser */
    console.info(document.body); /* <body> */
    console.info(document.body.constructor); /* HTMLDocument{} */
    console.info(document.firstChild); /* <html> */
    console.info(document.lastChild); /* <html> */
    console.info(document.implementation); /* createDocument(); createDocumentType(); createHTMLDocument(); hasFeature() */
    console.info(document.implementation.constructor); /* DOMImplementation{} */
    console.info(document.childNodes); /* NodeList[ html ] */
    console.info(document.body.childNodes); /* NodeList[ <TextNode textContent="\n   \n"> ] */
    
    console.info(document.attributes); /* null */
    console.info(document.anchors); /* HTMLCollection [] */
    console.info(document.applets); /* HTMLCollection [] */
    console.info(document.childNodes); /* NodeList[ html ] */
    console.info(document.contentType);  /* text/html */
    console.info(document.cookie); /* (an empty string) */
    console.info(document.documentElement); /* <html> */
    
    console.info(document.readyState);
}
function errorAssertException(){
    console.exception(this);
    console.assert(false, this);    
    console.error(this);
}
function bestPractice(){
    console.log("BEST PRACTICE (don't use 'new')");
    console.log("var obj = {} ...//new Object()");
    console.log("var array = [] ...//new Array()");
    console.log("var regExp = /[a-z]/gmi ...//new RegExp('[a-z]', 'gmi');");
    console.log("var func = function(a,b){return a+b;} ...//new Function('a,b','return a+b');");
    
    console.log("typeof is a variable: 'string' 'number' 'boolean' 'undefined' 'object' 'function'")
    console.log("instanceof Constructor returns true for any constors in the chain of inheritance");
    console.log(".constructor == the constructor used to Create This Object!");
    console.log(".isPrototypeOf == based on prototype chain");
    console.log(".hasOwnProperty == local vs inherited properties");
    console.log(".propertyIsEnumerable('prop') == will this show up in a for loop .. true/false");
    
    console.log("Objects are Hashes!");
    console.log("Arrays are Objects!");
    console.log("Functions are Objects that can be invoked! -- call(), apply()!");
    console.log("Functions contain a prototype property which is an Object! (e.g. Constructors)");
    console.log("the 'new' keyword simply/only creates a new Object: return {}");
    
    console.log("TYPES TYPES TYPES")
    console.log([].constructor); /* Array() */
    console.log([].constructor===Array); /*true*/
    console.log({}.constructor); /* Object() */
    console.log({}.constructor===Object); /*true*/
    console.log("".constructor); /* String() */
    console.log("".constructor===String); /*true*/
}
function complexTypes(){
    console.log("   -   -   TYPE OF");
    console.log(typeof new Number(1)); /* object */
    console.log(typeof 1); /* number */
    
    console.log("PRIMITIVES AS OBJECT (and 'toFixed example)");
    
    console.log("STRING-LENGTH".length); /* 13 */
    console.log((332211).toFixed(2)); /* 332211.00 */
    
    console.log("PROTOTYPE PROTOTYPE PROTOTYPE");
    function Randomly(){
        this.name = "Fun";
    }
    console.log(Randomly); /*copy of the last 3 lines of JavaScript */
    console.log(Randomly.constructor); /* Function() */
    console.log(Randomly.prototype); /* Randomly{} */
    console.log(Randomly.prototype.constructor); /*copy of the last 3 lines of JavaScript */
    
    console.log("   -   -   PROTOTYPE (continued)");
    var func = new Randomly(); 
    console.log(func); /* Randomly { name="Fun" } */
    console.log(func.constructor); /* function Randomly(){ this.name="Fun"; } */
    console.log(func.constructor.prototype); /* Randomly{ } */
    console.log(func.__proto__); /* Randomly{ } */
    console.log(func.__proto__.__proto__); /* Object{ } */
    
    console.log("CONSTRUCTOR PROTOTYPE");
    func.privvy = "password";
    var listOf = new Array();
    console.log(listOf.constructor); /*Array()*/
    console.log(listOf.constructor.prototype); /*[]*/
    
    console.log("HAS OWN PROPERTY");
    console.log(func.hasOwnProperty("name")); /*true*/
    console.log(func.hasOwnProperty("nick")); /*false*/
    console.log(func.hasOwnProperty("privvy")); /*true*/
    
    console.log("IS PROTOTYPE OF");
    console.log(Randomly.prototype.isPrototypeOf(func)); /*true*/
    console.log(Array.prototype.isPrototypeOf(listOf)); /*true*/
    console.log(Object.prototype.isPrototypeOf(listOf)); /*true*/
    console.log(Function.prototype.isPrototypeOf(listOf)); /*false*/
}
function inheritance(){
    console.log("INHERITANCE INHERITANCE INHERITANCE");
    var Father = function(){
        this.surname = "winner";
    }
    
    var Son = function(){}
    Son.prototype = new Father();
    var child = new Son();
    
    console.log(child.constructor); /* function() */
    console.log(Father.prototype); /* Object {} */
    console.log(Father.constructor); /* Function() */
    console.log(Son.prototype); /* Object {} */    
    console.log(Son.constructor); /* Function() */
    
    var GrandChild = function(){};
    GrandChild.prototype = child;
    var grandchild = new GrandChild();
    
    console.log("   -   -   INHERITANCE");
    console.log(grandchild.surname); /* winner */
    console.log(Son.prototype); /* Object { surname="winner" } */
    console.log(grandchild.__proto__);/* Object { surname="winner" } */
    
    console.log(Son.constructor); /* Function() */
    console.log(grandchild.constructor); /* function */
    console.log(grandchild.constructor.prototype); /* Object {} */
    console.log(grandchild.constructor.constructor); /* Function() */
    
    console.log(grandchild.hasOwnProperty("surname")); /*false*/
    console.log(grandchild.__proto__.hasOwnProperty("surname")); /*false*/
    console.log(grandchild.__proto__.__proto__.hasOwnProperty("surname")); /*true*/
    
    console.log("   -   -   Child/Inheritance is a Reference");
    console.log(child.surname); /* winner */
    console.log(grandchild.surname); /* winner */
    child.surname = "married name";
    console.log(child.surname); /* married name */
    console.log(grandchild.surname); /* married name */
    
    console.log("   -   -   -   -   ONE MORE TEST");
    var dad = new Father(); 
    console.log(dad.surname); /* winner */
    var sibling = new GrandChild(); 
    console.log(sibling.surname); /* married name */
    console.log(sibling.isPrototypeOf(child)); /* winner */
    sibling.prototype = dad;
    console.log(sibling.surname); /* married name */
    console.log(sibling.isPrototypeOf(child)); /* winner */
    var relation = new GrandChild();
    console.log(relation.surname); /* married name */
    
    console.log("   -   -   PARENT CONSTRUCTOR");
    console.log(dad.constructor===Father); /* true */
    console.log(child.constructor===Son); /* false */
    console.log(child.constructor===Father); /* true */
    console.log(grandchild.constructor===GrandChild); /* false */
    console.log(grandchild.constructor===Son); /* false */
    console.log(grandchild.constructor===Father); /* true */
    
    console.log("   -   -   INSTANCE OF");
    console.log(grandchild instanceof GrandChild); /* true */
    console.log(grandchild instanceof Son); /* true */
    console.log(grandchild instanceof Father); /* true */
    
    console.log("   -   -   FIXING the Deck");
    grandchild.constructor=GrandChild;
    console.log(grandchild.constructor===GrandChild); /* true (no longer false) */
    console.log(grandchild.constructor===Father); /* false (no longer true) */
}
function namesAndIdentity(){
    console.log("NAMES NAMES NAMES");
    
    function Root(){}
    
    var names = function Wicked(a,b,c){
        this.name = "gooooaaal";
    }
    
    var Parent = function Life(){}
    var Child = function(){
        this.baby="gaga"
    }
    
    console.log(names.prototype); /* names.prototype: Wicked{} ... named */
    console.log(names.constructor); /* names.constructor:  Function() ... anonymous */
    console.log(names.prototype.constructor); /* full JavaScript */
    
    console.log(Parent.prototype); /* Parent.prototype: Life{} ... named */
    console.log(Parent.constructor); /* Parent.constructor: Function() ... anonymous */
    console.log(Parent.prototype.constructor); /* full JavaScript */
    console.log(Parent.isPrototypeOf(names)); /* false */    
    
    
    console.log(Child.prototype); /* Child.prototype: Object() ... anonymous */
    console.log(Child.constructor); /* Child.constructor:  Function() ... anonymous */
    console.log(Child.prototype.constructor); /* Child.prototype.constructor: function() ... anonymous */
    console.log(Child.isPrototypeOf(Parent)); /* false */
    
    Child.prototype = new Parent();
    console.log(Child.prototype); /* Child.prototype: Life{} ... named*/
    console.log(Child.constructor); /* Child.constructor:  Function() ... anonymous */
    console.log(Child.prototype.constructor); /* JavaScript */
    console.log(Child.isPrototypeOf(Parent)); /* false */
    
    Parent.prototype = names.prototype;
    console.log(Parent.prototype); /* Parent.prototype: Wicked{} ... named  */
    console.log(Parent.constructor); /* Parent.constructor: Function() ... anonymous */
    console.log(Parent.prototype.constructor); /*SOURCE CHANGED!  full JavaScript */
    console.log(Parent.isPrototypeOf(names)); /* false */
    console.log(names.isPrototypeOf(Parent)); /* false */
        
    
}
function OO(){
    function inherit(child, parent){
        child.prototype = parent.prototype;
    }
    function inherit(child,parent){
        var func = function(){};
        func.prototype = parent.prototype;
        child.prototype = new func();
    }
    
    function inherit(child,parent){
        var func = function(){};
        func.prototype = parent.prototype;
        child.prototype = new func();
        child.uber = parent.prototype; //super
    }
    
    function inherit(child,parent){
        var func = function(){};
        func.prototype = parent.prototype;
        child.prototype = new func();
        child.uber = parent.prototype; //super
        child.prototype.constructor = child; //reset
    }
    
    function extend(parent){
        var attrib, child={};
        for(attrib in parent){
            child[attrib] = parent[attrib];
        }
        return child;
    }
    var parent = {
        a:1
    };
    var child = extend(parent);
    console.log(child.a);
    
    function object(o){
        function anon(){}
        anon.prototype = o;
        return new anon();
    }
    parent.a=2;
    child = object(parent);
    console.log(child.a);
    console.log(child.hasOwnProperty("a")); /* false */
    console.log(parent.hasOwnProperty("a")); /* true */
}
function closureOne(){
    console.log("ALL VARIABLES are Global unless declared in a function or with the var keyword.");
    function outer(){
        var local = 1;
        return function(){
            return local
        };
    }
    var inner = outer();
    console.log(inner());
}
function closureTwo(){
    var inner;
    function outer(){
        var local=1;
        inner = function(){
            return local;
        };
    }
    console.log(typeof inner); /* undefined */
    outer();
    console.log(typeof inner); /* function */
    inner();
}
function closureThree(){
    function change(arg){        
        var inner = function(){
            return arg;
        };
        arg++;
        return inner;
    }
    var val = change(5);
    console.log(val()); /* 6 */
    console.log(val()); /* 6 */
}
function closureFour(){
    function makeit(){
        var i, a=[];
        for(i=0; i<3;i++){
            a[i] = function(){
                return i;
            }
        }
        return a;
    }
    var funcs = makeit();
    console.log(funcs[0]()); /* 3 */
    console.log(funcs[1]()); /* 3 ... Ooops! */
    console.log(funcs[2]()); /* 3 ... Ooops! */
    function makeit(){
        var i, a=[];
        for(i=0; i<3;i++){
            a[i] = (function(local){
                return function(){
                    return local
                };
                
            })(i);
        }
        return a;
    }
    console.log(funcs[0]()); /* 0 */
    console.log(funcs[1]()); /* 1 */
    console.log(funcs[2]()); /* 2 */
}
function closureFive(){
    var getValue, setValue;
    (function(){
        var secret = 0;
        getValue = function(){
            return secret;
        };
        setValue = function(v){
            secret = v;
        };
    })();
    console.log(getValue()); /* 0 */
    setValue(123); /* 123 */
    console.log(getValue());
}
function closureIterator(){
    function setup(x){
        var i=0;
        return function(){
            return x[i++];
        };
    }
    var next = setup(['a','b','c']);
    console.log(next()); /* a */
    console.log(next()); /* b */
}
function badLoop(){
    var events = ["onload","onclick"];
    for(var x=0; x<events.length; x++){
        console.log("loading " + events[x]);
        console.log(events[x]);
        window[events[x]] = function(){
            console.log(x);
        }
        console.log(events[x]);
    }
}
function goodLoop(){
    var events = ["onreload", "onresize","onclick"];
    for(var x=0; x<events.length; x++){
        window[events[x]] = (function(save){
            return function(){
                console.log(save)
            };
        })(x);
        console.log(events[x]);
    }
}
function CallOrApplyExample(){
    function theFunction(name, profession) {
        console.log("My name is " + name + " and I am a " + profession + ".");
    }
    theFunction("John", "fireman");
    theFunction.apply(undefined, ["Susan", "school teacher"]);
    theFunction.call(undefined, "Claude", "mathematician");
    object = {
        name: "object",
  
        action: function() {
            nestedAction = function(greeting) {
                console.log(greeting + " " + this.name);
            }
            nestedAction("hello");
        }
    }
    object.action("hello");
    object.action = function() {
        nestedAction = function(greeting) {
            console.log(greeting + " " + this.name);
        }
        nestedAction.call(this, "hello");
        nestedAction.apply(this, ["hello"]);
    }
    object.action("hello");
    alice = {
        name: "Alice"
    }

    eve = {
        name: "Eve",
  
        talk: function(greeting) {
            console.log(greeting + ", my name is " + this.name);
        }
    }

    eve.talk("yo");
    eve.talk.apply(alice, ["hello"]);
    eve.talk.apply(window, ["hi"]);
}
function BindExample(){
    Function.prototype.bind = function(scope) {
        var _function = this;
  
        return function() {
            return _function.apply(scope, arguments);
        }
    }

    alice = {
        name: "alice"
    }

    eve = {
        talk: function(greeting) {
            console.log(greeting + ", my name is " + this.name);
        }.bind(alice) // <- bound to "alice"
    }

    eve.talk("hello");
    
    Function.prototype.bind = function(scope) {
        var _function = this;
  
        return function() {
            return _function.apply(scope, arguments);
        }
    }

    object = {
        name: "object",
  
        action: function() {
            nestedAction = function(greeting) {
                console.log(greeting + " " + this.name);
            }.bind(this) // <- bound to "object"
    
            nestedAction("hello");
        }
    }

    object.action("hello");
}
function AnonymousScope() {
    console.log(this + "0");
    (function () {
        console.log(this + "1");
    // > Foo
    }).call(this);

    (function () {
        console.log(this + "2");
    // > undefined in strict mode, or Window in non strict mode
    })();
    console.log(this + "3");
}
function logVariablesInScope(obj){
    console.log(arguments.length);
    console.log(arguments);
    console.log("typeof this = " + typeof this);
    console.log("===DETAILS of SCOPE===");
    var name, arg, n;
    for (name in this) {
        console.log("this[" + name + "]=" + this[name]);
    }
    for (n = 0; n < arguments.length; ++n) {
        arg = arguments[n];
        console.log("typeof arguments[" + n + "] = " + typeof arg);
        for (name in arg) {
            console.log("arguments[" + n + "][" + name + "]=" + arg[name]);
        }
    }
}
function DocumentAndWatchInfo(){    
    document.readyState = function(){
        console.log("I'm listening for the readyState to change!");
    }
    console.log(document.documentElement);
    var html = document.documentElement;
    console.log(html.length);
    console.log(html.parentNode);
    console.log(document.implementation);
    var impl = document.implementation;
    var custom = {
        winner: 0
    }
    custom.watch( "winner", function( id, oldVal, newVal ){
        console.log("===WATCH WATCH WATCH===");
        console.log( id+' changed from '+oldVal+' to '+newVal );
        return newVal;
    });
    custom.winner=10;
}
function dynamicExample(){
    var dynamicScript = "console.log('this would be awesome');";
    eval(dynamicScript);
}
function logGlobal(){
    console.log(document.readyState);
    console.log("===DETAILS of INVOKER===");
    console.log(this);
    console.log(this===window);
    console.log("===DETAILS of DOM===");
    console.log("===WINDOW===");
    console.log(window);
    console.log("===NAVIGATOR===");
    console.log(navigator);
    console.log("===SCREEN===");
    console.log(screen);
    console.log("===HISTORY===");
    console.log(history);
    console.log("===LOCATION===");
    console.log(location);
    console.log("===DOCUMENT===");
    console.log(document);
}

/**
 console.log(arguments.callee.constructor);
        console.log(this);
        console.log(possibleState.indexOf(currentState));
        universe=[function(){
            return new Date().getTime()
        }, [new Date().getTime(), doc, currentState]];
        
        console.log(possibleState);
        console.log(currentState);
        //event = new Event();
        //watch(document.readyState);
        console.log(space.history);
        console.log(space.history.length);
        console.log(space.history.state);
        //space.history.go();
        //space.back();
        //space.forward();
        //space.pushState();
        //space.replaceState();
        console.log(space.location);
        console.log(space.location.href);
        //console.log(space.location.assign());
        //console.log(space.location.replace());
        //console.log(space.location.reload());
        console.log(space.navigator);
        //MORE! console.log(space.navigator);
        console.log(doc.nodeName);
        console.log(doc.documentElement);
        console.log("Active Element: %o",doc.activeElement);
        console.log(doc.body);
        console.log(doc.head);
        console.log(doc.title);
        console.log(doc.lastModified);
        //console.log(doc.referrer);
        console.log(doc.URL);
        console.log(doc.readyState);
        //console.log(doc.ownerDocument);
        console.log("Has Focus? %s",doc.hasFocus());
        console.log(doc.constructor);
        console.log(doc.firstChild.nodeName);
        console.log(doc.firstChild.children);
        console.log(doc.firstChild.childElementCount);
        console.log(doc.head.childElementCount);
        console.log(doc.head.children);
        console.log(doc.body.childElementCount);
        console.log(doc.body.children);
        console.log(doc.lastChild.nodeName);
        console.log(doc.body.attributes);
        console.log(doc.body.dataset); //hyphenated-case becomes camelCase! (e.g. CSS, etc)
        console.log(doc.body.classList);
        console.log(space.btoa("Hello-World"));
        console.log(space.atob(3243));
        console.log(space.getComputedStyle(doc.body));
 */

/**
 *
 *    Let input be the string being parsed.

    Let position be a pointer into input, initially pointing at the start of the string.

    Remove all space characters from input.

    If the length of input divides by 4 leaving no remainder, then: if input ends with one or two "=" (U+003D) characters, remove them from input.

    If the length of input divides by 4 leaving a remainder of 1, throw an InvalidCharacterError exception and abort these steps.

    If input contains a character that is not in the following list of characters and character ranges, throw an InvalidCharacterError exception and abort these steps:
        "+" (U+002B)
        "/" (U+002F)
        ASCII digits
        uppercase ASCII letters
        lowercase ASCII letters 

    Let output be a string, initially empty.

    Let buffer be a buffer that can have bits appended to it, initially empty.

    While position does not point past the end of input, run these substeps:

        Find the character pointed to by position in the first column of the following table. Let n be the number given in the second cell of the same row.
        A	0
        B	1
        C	2
        D	3
        E	4
        F	5
        G	6
        H	7
        I	8
        J	9
        K	10
        L	11
        M	12
        N	13
        O	14
        P	15
        Q	16
        R	17
        S	18
        T	19
        U	20
        V	21
        W	22
        X	23
        Y	24
        Z	25
        a	26
        b	27
        c	28
        d	29
        e	30
        f	31
        g	32
        h	33
        i	34
        j	35
        k	36
        l	37
        m	38
        n	39
        o	40
        p	41
        q	42
        r	43
        s	44
        t	45
        u	46
        v	47
        w	48
        x	49
        y	50
        z	51
        0	52
        1	53
        2	54
        3	55
        4	56
        5	57
        6	58
        7	59
        8	60
        9	61
        +	62
        /	63

        Append to buffer the six bits corresponding to number, most significant bit first.

        If buffer has accumulated 24 bits, interpret them as three 8-bit big-endian numbers. Append the three characters with code points equal to those numbers to output, in the same order, and then empty buffer.

        Advance position by one character.

    If buffer is not empty, it contains either 12 or 18 bits. If it contains 12 bits, discard the last four and interpret the remaining eight as an 8-bit big-endian number. If it contains 18 bits, discard the last two and interpret the remaining 16 as two 8-bit big-endian numbers. Append the one or two characters with code points equal to those one or two numbers to output, in the same order.

    The discarded bits mean that, for instance, atob("YQ") and atob("YR") both return "a".

    Return output.

Some base64 encoders add newlines or other whitespace to their output. The atob() method throws an exception if its input contains characters other than those described by the regular expression bracket expression [+/=0-9A-Za-z], so other characters need to be removed before atob() is used for decoding.
 */
/**
 *
 *
 *There are five different kinds of elements: void elements, raw text elements, RCDATA elements, foreign elements, and normal elements.

Void elements
    area, base, br, col, command, embed, hr, img, input, keygen, link, meta, param, source, track, wbr
Raw text elements
    script, style
RCDATA elements
    textarea, title
Foreign elements
    Elements from the MathML namespace and the SVG namespace.
Normal elements
    All other allowed HTML elements are normal elements. 

 * 4.6 Dispatching events

To dispatch an event to a given object run these steps:

    Let event be the event that is dispatched.

    Set event's dispatch flag.

    Initialize event's target attribute to the object to which event is dispatched.

    If event's target attribute value is participating in a tree, let event path be a static ordered list of all its ancestors in tree order, or let event path be the empty list otherwise.

    Initialize event's eventPhase attribute to CAPTURING_PHASE.

    For each object in the event path invoke its event listeners with event event, as long as event's stop propagation flag is unset.

    Initialize event's eventPhase attribute to AT_TARGET.

    Invoke the event listeners of event's target attribute value with event, if event's stop propagation flag is unset.

    If event's bubbles attribute value is true, run these substeps:

        Reverse the order of event path.

        Initialize event's eventPhase attribute to BUBBLING_PHASE.

        For each object in the event path invoke its event listeners, with event event as long as event's stop propagation flag is unset. 

    Unset event's dispatch flag.

    Initialize event's eventPhase attribute to NONE.

    Initialize event's currentTarget attribute to null.

    Return false if event's canceled flag is set, or true otherwise. 

To invoke the event listeners for an object with an event run these steps:

    Let event be the event for which the event listeners are invoked.

    Let listeners be a static list of the event listeners associated with the object for which these steps are run.

    Initialize event's currentTarget attribute to the object for which these steps are run.

    Then run these substeps for each event listener in listeners:

        If event's stop immediate propagation flag is set, terminate the invoke algorithm.

        Let listener be the event listener.

        If event's type attribute value is not listener's type, terminate these substeps (and run them for the next event listener).

        If event's eventPhase attribute value is CAPTURING_PHASE and listener's capture is false, terminate these substeps (and run them for the next event listener).

        If event's eventPhase attribute value is BUBBLING_PHASE and listener's capture is true, terminate these substeps (and run them for the next event listener).

        If listener's callback is a Function object, its callback this value is the event's currentTarget attribute value.

        Call listener's callback, with the event passed to this algorithm as the first argument. 

 */