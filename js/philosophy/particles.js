function Particles(){
    UniverseParticles();
    SpaceParticles();
}
/**
 * UniverseParticles() captures the current size of the "Universe" (<body>)
 *      -  knowledge.universe.locate() requires "some" event in process without errors.
 */
function UniverseParticles(){
    knowledge.universe.particles = [];
    knowledge.universe.locate = function(particle){
        if(particle){
            this.particles.push([knowledge.universe, particle]);
        }else{
            this.particles.push([knowledge.universe, knowledge.space]);
        }
        this.particles[this.particles.length-1].push(this.universeWidth());
        this.particles[this.particles.length-1].push(this.universeHeight());
    }
}
/**
 *  SpaceParticles() captures the current size of "Space" (<div> a.k.a the knowledge.context reference)
 *      -  knowledge.space.locate() requires "some" event in process without errors.
 */
function SpaceParticles(){
    knowledge.space.particles = [];
    knowledge.space.locate = function(event, particle){
        if(particle){
            this.particles.push([particle, event.target]);
        }else{
            this.particles.push([knowledge.space, event.target]);
        }
        this.particles[this.particles.length-1].push(event.clientX);
        this.particles[this.particles.length-1].push(event.clientY);
    }
}