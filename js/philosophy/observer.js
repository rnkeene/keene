function Observe(event){    
    Context(event);
    Universe();
    Particles();
    Forces();
    Legend();
    Transform();
    this.mobile = function(event){
        event.preventDefault();
    }
    this.open = function(event){
        event.preventDefault();
    }
    this.vector = function(event){
        var elements = window.knowledge.space.elements[2];
        var child = window.knowledge.space.elements[3][0];
        var newX = event.clientX - window.knowledge.space.elements[3][1];
        var newY = event.clientY - window.knowledge.space.elements[3][2];
        child.style.left=newX;
        window.knowledge.space.elements[0][elements.indexOf(child)]=newX;
        child.style.top=newY;
        window.knowledge.space.elements[1][elements.indexOf(child)]=newY;
    }
    this.translate = function(event){
        event.preventDefault();
        Heirarchy(event.target, window.knowledge.space.elements[3]);
    }
    this.resizeScale = function(originalLocation, previousMaxSize, newMaxSize){    
        return (originalLocation * newMaxSize)/previousMaxSize;    
    }
    this.resize = function(){
        /**
        * Capture the resized window size (x,y).
        */
        this.space.style.width = pageWidth();
        this.space.style.height = pageHeight();
        /**
        * Now, get the previous window sizes (x,y) along with related elements and their locations.
        */
        var xray = this.space.elements[0];
        var yray = this.space.elements[1];    
        var elements = this.space.elements[2];
        var previousXSize = xray[0];
        var previousYSize = yray[0];
        /**
        * Adjust all of the elements and scale their size to fit into the new window size.
        */
        for(var x=1; x<xray.length; x++){
            xray[x] = this.resizeScale(xray[x], previousXSize, pageWidth());
            yray[x] = this.resizeScale(yray[x], previousYSize, pageHeight());        
            elements[x].style.left = xray[x];
            elements[x].style.top = yray[x];
            elements[x].style.fontSize = scaleFont();
        }
        /**
        * Overwrite the previous screen size now that the elements have been adjusted.
        * Now the entire system is scaled and stored (the previous version is lost, for now)
        * There can be some loss of precision here.
        */
        xray[0] = pageWidth();
        yray[0] = pageHeight();
        
        this.space.elements[0]=xray;
        this.space.elements[1]=yray;
        
        if(this.universe){
            this.universe.resize();
        }
        if(this.quickTip){
            this.quickTip.resize();
        }
    }
    window.knowledge.space.activate();
}
listen("load", window, Observe, false);

