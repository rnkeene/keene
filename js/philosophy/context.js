/**
* Context(event) will create the global 'knowledge' variable and attached functions as describe here:
*   - e.g. (implementation code) window.knowledge = {...}
*   - e.g. (execution or reference) knowledge.Xxxx
* 
* - The final line of this function 'self creates' so that various references should always exist in the page.
*   - knowledge.create(knowledge.context, event);
*   
* 
*  - initialize the 'knowledge' global variable
*      - e.g. knowledge.function
*      - e.g. use like 'window' or 'document' (in any javascript reference)
*          - e.g. window.onload
*          - e.g. document.body
*          
*  - knowledge.context is initialized to "div" (but could be any valid variable)
*  
*  - function knowledge.create(type, event) will exist, and have two side effects:
*      - create will cause knowledge.space to exist
*      - create will cause knowledge.space.history (a parallel array; a.k.a a 2D-matrix) to exist.
*          - knowledge.history holds the element in the first Array (relates to an 'unknown' partner event)
*          - knowledge.history holds the event in the second Array (relates to an 'unknown' partner element)     
*      - knowledge.create() has no optional parameters; errors will be created if missing
*  
*  - function knowledge.elements() and knowledge.elements(element)
*      - knowledge.elements() returns the history matrix of the root element (paired with event).
*      - knowledge.elements(element) returns the history matrix of a particular element (paired with event).
*      - element is an optional parameter, either version of the function will work
*      
*  - function knowledge.events() and knowledge.events(element)
*      - knowledge.events() returns the history matrix of the root events (paired with element).
*      - knowledge.elements(element) returns the history matrix of a particular element (paired with element).
*      - element is an optional parameter, either version of the function will work
*
*  - two functions also exist to get the most recent (active) Element and paired Event.
*      - knowledge.activeElement(element) 
*      - knowledge.activeEvent(element)
*      - element is an optional parameter, either version of the function will work
*      
*  - function knowledge.root() and knowledge.root(element)
*      - element is an optional parameter, either version of the function will work
*/
function Context(event){
    window.knowledge = {
        context : "div",
        create : function(type, event){
            var element = document.createElement(type);
            element.history = [[element],[event]];
            if(!this.space){
                this.space = element;
            }else{
                this.space.history.push(element.history);
            }
            return element;
        },
        elements : function(element){
            if(element){
                return element.history[0][0];
            }
            return this.space.history[0][0];
        },
        events : function(element){
            if(element){
                return element.history[1][0];
            }
            return this.space.history[1][0];
        },
        activeElement : function(element){
            if(element){
                return element.history[0][element.history[0].length-1];
            }
            return this.space.history[0][this.space.history[0].length-1];
        },
        activeEvent : function(element){
            if(element){
                return element.history[1][element.history[1].length-1];
            }
            return this.space.history[1][this.space.history[1].length-1];
        },
        root : function(element){
            if(element){
                return element===this.space;
            }
            return this.space;
        }
    }
    knowledge.create(knowledge.context, event);
}
function ignore(type, element, func){
    /**
    *  Any function being notified by an event firing may be removed from notifications.
    *   - The first parameter is the name of the event: 'click', 'load', etc.
    *   - The second parameter is the name of the function no longer needing to execute.
    */
    element.removeEventListener(type, func);
}
function listen(type, element, func, propagate){
    /**
    *  Any HTML Element may add appropriate event listeners based on its node type.
    *   - The first parameter is the name of the event: 'click', 'load', etc.
    *   - The second parameter is the name of the function to be executed when the event fires.
    *   - the final parameter signals if this event should stop propogating to 'higher-level' listeners.
    */
    element.addEventListener(type, func, propagate);
}