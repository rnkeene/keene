function Universe(){
    knowledge.universe = document.body;
    knowledge.universe.space = function(){
        this.style.fontSize = this.scaleBase();
        this.style.margin = 0;
        this.style.padding = 0;
        this.style.textAlign = "center";
        this.style.verticalAlign = "middle";
    }
    knowledge.universe.exist = function(element){
        element.style.width = this.universeWidth();
        element.style.height = this.universeHeight();
        this.appendChild(element);
        element.style.width = this.universeWidth();
        element.style.height = this.universeHeight();
    }
    knowledge.universe.scaleBase = function(base){
        if(base){
            return base*this.universeHeight()/768;
        }
        return 20*this.universeHeight()/768;
    }    
    knowledge.universe.metaView = function(universe){
        universe.style.fontSize = this.scaleBase();
        universe.style.width = this.universeWidth()*.84;
        universe.style.top = this.universeHeight()*.6;
        universe.style.left = this.universeWidth()*.08;
    }
    knowledge.universe.universeWidth = function(){
        return this.clientWidth;
    }
    knowledge.universe.universeHeight = function(){
        return this.clientHeight;
    }
    knowledge.universe.spaceView();
    knowledge.universe.exist(knowledge.space);
    console.log(knowledge.universe);
}