function Transform(event){
    /**
     * Some variables to make referencing some data simpler.
     */
    var elements = window.knowledge.space.elements[2];
    var index = elements.indexOf(event.target);
    var origX = window.knowledge.space.elements[0][index];
    var origY = window.knowledge.space.elements[1][index];
    var newX = event.clientX;
    var newY = event.clientY;
    /**
     * Add a reference to this Element and it's 'mouse-click' offset from the top-left corner.
     * If this information isn't saved then when the drop function is called the position will be off by some amount depending on the mouse click.
     */
    window.knowledge.space.elements[3] = [event.target];
    window.knowledge.space.elements[3].push(newX - origX);
    window.knowledge.space.elements[3].push(newY - origY);
    /**
     * This defines what will be seen (moved) when an Element (and its children) are being moved.
     */
    event.dataTransfer.setData("Element", event.target);
}
function IgnoreClick(){
    ignore("click", window.knowledge, Property);
    listen("click", window.knowledge, RestartKnowledge, true);    
}
function RestartKnowledge(){
    ignore("click", window.knowledge, RestartKnowledge);
    listen("click", window.knowledge, KnowledgeClick, false);
}
function KnowledgeClick(){
    ignore("click", window.knowledge, RestartKnowledge);
    listen("click", window.knowledge, Property, false);
}