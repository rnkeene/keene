function Forces(){
    UniversalForce();
    GravitationalForce();
    ElementaryUniversalForce();    
    ElementaryGravitationalForce();
    knowledge.space.zoom = function(event){
        if(event.ctrlKey){
            switch(event.keyCode){
                case 61:
                    window.knowledge.space.baseHeight *= 1.1;
                    break;
                case 173:
                    window.knowledge.space.baseHeight *= .9;
                    break;
            }
        }
    }
}
function UniversalForces(){    
    var resize = ["resize", window, knowledge.resize, false];
    
    knowledge.universe.forces = [resize];
    
    knowledge.universe.activate = Activate;
    knowledge.universe.disable = Disable;
}
function GravitationalForce(){
    var property = ["click", knowledge.space, Property, false];
    var property = ["drop", knowledge.space, knowledge.vector, false];
    var detect = ["dragover", knowledge.space, knowledge.open, false];
    var open = ["keydown", window, knowledge.resize, false];
    var inspect = ["blur", window, IgnoreClick, false];
    
    knowledge.space.forces = [property, detect, open, inspect];
    
    knowledge.space.activate = Activate;
    knowledge.space.disable = Disable;
}
function ElementaryUniversalForce(){
    knowledge.universe.particles.forces = [];

    knowledge.universe.particles.activate = Activate;
    knowledge.universe.particles.disable = Disable;
}
function ElementaryGravitationalForce(){
    knowledge.space.particles.forces = [];
    
    knowledge.space.particles.activate = Activate;
    knowledge.space.particles.disable = Disable;
}
function Activate(){
    for(var x=0; x<this.forces.length; x++){
        listen(this.forces[x][0], this.forces[x][1], this.forces[x][2], this.forces[x][3]);
    }
}
function Disable(){
    for(var x=0; x<this.forces.length; x++){
        ignore(this.forces[x][0], this.forces[x][1], this.forces[x][2]);
    }
}