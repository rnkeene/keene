function Legend(event){
    this.legend = this.create("div");
    this.legend.style.color="#0000FF";
    this.legend.style.position="absolute";
    styleTip(this.legend);
    this.legend.onclick = function(){
        console.log("Disable for Legend");
        window.knowledge.disable();
        console.log("Adding blur listener");
        listen("blur", window.knowledge.legend, window.knowledge.activate, false);
    }
    this.legend.innerHTML = "Single Left Click: New Knowledge";
    this.legend.innerHTML += "<br /><br />Drag and Drop: Move Knowledge Around Open Space <br />**Coming Soon! Create Sentences and Questions<br />**Coming Soon! Create Heirarchy and <a onclick='return false;' target='_blank' href='http://en.wikipedia.org/wiki/Block_diagram'>Block Diagrams</a>";
    this.legend.innerHTML += "<br /><br />Resize: Scale the Page <br/>**Content of Knowledge and Tips will Resize with the Page<br />**Knowledge in particular will reposition with the Page";
    this.legend.innerHTML += "<br /><br />Zooming In and Out: Ctrl- ('Control-Minus') and Ctrl+ ('Control-Plus')";
    this.legend.style.border = "3px solid Black";
    this.legend.style.backgroundColor = "#FFFFCC";
    this.space.appendChild(this.legend);
    this.legend.resize = function(){
        if(window.knowledge.legend){
            styleTip(window.knowledge.legend);
        }
    }
}