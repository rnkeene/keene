/**
 *  The count at time T will be relative to the space.
 *      The space is 'some' array.
 *          The base space is this function.
 */
function t(){
    var time = function(){
        return function(){
            return {
                count : 0,
                time : Date.now(),
                relative : function(){
                    return [function(){
                        return this.count++/Date.now();
                    },function(){
                        return this.count++/Date.now();
                    }];
                }
            }
        }
    }
    return [time(),time()()];
    
}
/**
*   The value at some time T should be 'some' Real value R.
*       A Real value will be quadratic; or it is not real
*           a2 + b2 + c2 +d2 can find any Real value.
*/
function r(){
    var radius=t();
    return function(){
        return [radius[0](),radius[1].count,radius[1].time,radius[1].relative()]
    }
}
/**
 *  There will always be some derivative value (dy) of some Real value of x
 *      the derivative value proves some logical value of x exists.
 *          {assuming (dy) is logical to start}
 *  Given some Real x; there is a derivative 'dx'
 *      There is a count 'c' that represents the time
 *          that time is equal to the count of steps to prove 'c'
 */
function i(){
    var c = [r(),r()()];
    return function(){
        return [c[1][0],c[1][1],c[1][2],c[1][3]]
    }
}
/**
 *  for a real value, there is always:
 *      some value for counted time (a constant)
 *      and some function that proves both the count of time
 *          and the real value
 *  therefor, for any real value "E" there is some time "T"
 *      E can be proven by x, through derivative y
 *      T can be proven by e, through a derivative of x (E)
 *  in such a way E and T (value at time) is always relative to:
 *      two derivative functions dx, and dt.
 *          dx, additionally, requires the derivative of dy.
*/
function real(object){
    var r = r(object);
    return [r[1],r.length]
}
/**
 * There must be some constant C from which set [E, T, dx, dy, dt] all form their basis.
 *  therefor; the all Real values must be of the set C=[E,T,dx,dy,dt]
 *      the value of C should be represented by a unique Real value.
 *          for any initial proof the value for C should be 0
 * For any normalized language (grammar):
 *      the value of E,T,dx,dy, and dt cannot be equal to the constant value
 */
function O(){
    var imagine = i();
    //the result of the imaginary value against itself is:
    //      i = di (i) = x
    //      which means there is some constant dt that can be applied to x:
    //          dt(x) = c
    //      there is also some some dy that leads back x:
    //          dy(dt(c)) = x
    //
    return [imagine,imagine(imagine)];
}
/**
 * Every Normal Real will have some value relative to the constant
 *      Therefor, there is some ring that connects any two values through
 *          each Real's relationship to the constant.
 *      Therefor; if some value "X" and some value "Y" exist;
 *          There is some function "dx" and some function "dy" 
 *              and some constant "dt" that is unique.
 *                  "dx" does not need to be unique, but "X" does.
 *                  "dy" does not need to be unique, but "Y" does.
 *                  "dt" does not need to be unique, but "C" does.
 * There should be some binary logic to represent this.
 */
function B(){
    var ring = O();
    console.log("Imagine 0: %o",ring);
    console.log("Imagine 0: %o",ring.length);
    console.log("Imagine 0: %o",ring[0]);
    console.log("Imagine 0: %o",ring[1]);
    console.log()
}
function A(){
    
}
