/* Logical Interface :: all functions should take parameters 0 and 1 (1 and 0);
 *      The Thread move from the 'current' (pre-execution) State
 *          To a 'future' (post-execution) State.
 *      This is conceptualized by moving from a "0" state (pre-execution)
 *          To a "1" State (post execution)
 *      However; the Controller currently views the pre-execution state as "1" (Stable)
 *          And the future state as "0" (Unstable)
 *      Prove 'closure' to the Controller by running the current state ("1")
 *          To a ("0") future state.
 *      The Controller will perform the following tasks
 *         To verify the thread is successful from [0 to 1] and [1 to 0]:
 *              ++ (Stable) run the Thread from [1 to 1] (e.g. 1=1)
 *              -- (Reliable) run the Thread from [0 to 0] (e.g. 0=0)
 *              == (Accurate) run the Thread from [0 to 1] and validate 1=1==1
 *              === (Consistent) run the Thread from [1 to 0] and validate 0=0==0
 */

function Closure(){
    var logical = function(thread){
        //create a [4x1] matrix of the binary [thread] states
        var matrix = [thread(0,1),thread(1,0),thread(1,1),thread(0,0)];
        //the transform holds the [state 0] and [state 1] case
        var transform = [];
        //the parity holds the [state -1] and [state +1] case
        var parity = [];
        var closure = function(event){
            var result = event();
            transform.push(result[1](result[1+1]));
        }
        var closed = function(event){
            var result = event();
            parity.push(result[1](result[1+1]));
        }
        var exceptional = function(){
            try{
                closure(matrix[0]);
                closure(matrix[1]);
                closed(matrix[2]);
                closed(matrix[3]);
                if(transform[0]==transform[1]){
                    if(transform[0]==parity[0]){
                        if(transform[1]==parity[0]){
                            if(parity[0]==parity[1]){
                                return false;
                            }
                            throw [parity[0],parity[1]]
                        }
                        throw [transform[1],parity[1]]
                    }
                    throw [transform[0],parity[0]]
                }
                throw [transform[0],transform[1]]
            }catch(err){
                return err;
            }
        }
        return exceptional;
    }
    var controller = function(thread,instrument,measure,latency){
        console.groupCollapsed("Controller");
        console.log(thread);
        console.log(instrument);
        console.log(measure);
        console.groupEnd();
        latency(thread(),instrument,measure,latency);
    }
    return [controller,logical];
}
var synchronous = function(){
    var lock = [arguments[0],Closure()];
    var instrument = function(){
        console.log("Setting up Measurement Instrument");
    }
    var measure = function(){
        console.log("Measuring with Instrument");
    }
    var latency = function(){
        console.log("Hello Space! %o",arguments);
        console.log("Run lock[1][1] to verify");
    }
    var proxy = function(){
        return function(){
            lock[1][0](lock[0],instrument,measure,latency);
        }
    }
    return proxy(lock[0]);
}
var thread = function(){
    return Date.now();
}
var time = synchronous(thread);
console.log(time());
