/**
 * Every Normal Real will have some value relative to the constant
 *      Therefor, there is some t that connects any two values through
 *          each Real's relationship to the constant.
 *      Therefor; if some value "X" and some value "Y" exist;
 *          There is some function "dx" and some function "dy" 
 *              and some constant "dt" that is unique.
 *                  "dx" does not need to be unique, but "X" does.
 *                  "dy" does not need to be unique, but "Y" does.
 *                  "dt" does not need to be unique, but "C" does.
 * There should be some binary logic to represent this.
 */
function y(){
    var t = O();
//    console.log("Ring: %o",t);
//    console.log("x= %o",t.length);
//    console.log("for function: %o",t[0]);
//    console.log("derived from (y): %o",t[1]);
//    console.log("\t x.y=%o",t[1].length);
//    console.log("\t x.y.x=%o",t[1][0].count);
//    console.log("\t x.y.x.time=%o",t[1][0].time);
    return [t.length,t[1].length,t[1][0].count,t[1][0].time];
}
/**
 * e is the base exponent (dimension)
 *      the normal (natural log) of e is e.length
 *  This can be read as follows:
 *      e = some exponent
 *      e[0] = the constant; in this case:
 *          == the natural time of event (Date.now())
 *      e[1] = the values [a,b,c,d]
 *          == values used to derive e.
 */
function de(){
    return [Date.now(),y()];    
}
/**
 * There is some natural error calculating time (complexity)
 *      Time is a constant of the system being observed.
 *      
 *  Observe the time until an error case.
 *  
 *  For any function, its complexity is dictated time.
 *      A stable (static) system will have complexity 0 at all Time.
 *  
 *  This is an observation of faith, and can always be retested.
 *      Therefor, this is 'only' a relatively accurate framework.
 */
function c(){
    var constant = [de()];
    var timer = -1;
    var flux = constant[0];
    while(flux[1][3]==constant[0][1][3]){
        flux=de();
        timer++;        
    }
    constant.push(flux);
    constant.push(timer);
    return constant;
}
/**
 * f() returns deviation = 
 *      [0] results of function,
 *      [1] change in function results (Date.now()),
 *      [2] change in local results (counter)
 *      [3] change in local results vs function results.
 */
function f(){
    var flux = c();
    var deviation = [flux,flux[1][1][3]-flux[0][1][3],flux[2]];
    deviation.push(deviation[2]-deviation[1]);
    return deviation;
}
/**
 * s() returns deviation flux = 
 *      [0] results of flux
 *          [0] natural time (Date.now())
 *          [1] natural flux (time between Date.now() and each f(x))
 *          [2] local flux (counter time to flux)
 *          [3] delta local (counter time - flux time)
 *      [1] time to "know" results
 */
function s(){
    var time = [f()];
    var symetric = 1000;
    while(symetric && time[time.length-1][1]<2){
        time.push(f());
        symetric--;
    }
    return [time, time.length+symetric]
}