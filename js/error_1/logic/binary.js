var frame = s();
var results = frame[0];
var timeTotal = 0;
var localTotal = 0;
var fluxTotal = 0;
var diffTotal = 0;
for(var x=0; x<results.length;x++){
    console.log("%i) Time=%o, Flux=%o, Local=%o, Diff=%o",x,results[x][0][0][0],results[x][1],results[x][2],results[x][3]);
    timeTotal+=results[x][0][0][0];
    localTotal+=results[x][1];
    fluxTotal+=results[x][2];
    diffTotal+=results[x][3];
}
console.log("Start time: %o; Count to threshold: %i",results[0][0][0][0],frame[1]);
console.log("Time Total Average: %i", timeTotal/results[0][0][0][0]);
console.log("Local Total Average: %i", localTotal/results.length);
console.log("Flux Total Average: %i", fluxTotal/results.length);
console.log("Diff Time Total: %i", diffTotal/results.length);