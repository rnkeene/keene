(function(){
    /*
     * This is an approach to solving the 3 body problem that exists within naturally growing Models.
     *      
     *      ** Example State **
     *      
     *      A="11" therefor map to "3"
     *      B="100" therefor map to "4"
     *      C="101" therefor map to "5"
     *      
     */
    Language = function(){
        var perfect = {
            triangles : [[3,4,5],[ 5, 12, 13],[ 7, 24, 25],[ 8, 15, 17],[ 9, 40, 41],[11, 60, 61],[12, 35, 37],[13, 84, 85],[16, 63, 65],[20, 21, 29],[28, 45, 53],[33, 56, 65],[36, 77, 85],[39, 80, 89],[48, 55, 73],[65, 72, 97]],
            line: [1,1,1,1],
            dimensions : [[4,24],[6,24],[8,48],[20,120]],
            alphabet : {
                alpha : "a",
                beta : "b",
                gamma : "c",
                delta : "d",
                epsilon : "e",
                zeta : "z",
                eta : "h",
                theta : "f",
                iota : "i",
                kappa : "k",
                koppa : "0",
                lambda : "l",
                mu : "m",
                nu : "n",
                ksi : "k",
                omicron : "o",
                pi : "p",
                rho : "r",
                sigma : "s",
                tau : "t",
                upsilon : "u",
                phi : "v",
                chi : "w",
                psi : "x",
                omega : "y",
                sampi: "z"
            }
        }
        perfect.numerals = {
            Units : {
                "alpha":perfect.alphabet.alpha,
                "beta":perfect.alphabet.beta,
                "gamma":perfect.alphabet.gamma,
                "delta":perfect.alphabet.delta,
                "epsilon":perfect.alphabet.epsilon,
                "digamma":perfect.alphabet.theta,
                "zeta":perfect.alphabet.zeta,
                "eta":perfect.alphabet.eta,
                "theta":perfect.alphabet.theta
            },
            Tens : {
                "iota":perfect.alphabet.iota,
                "kappa":perfect.alphabet.kappa,
                "lambda":perfect.alphabet.lambda,
                "mu":perfect.alphabet.mu,
                "nu":perfect.alphabet.nu,
                "xi":perfect.alphabet.eta,
                "omicron":perfect.alphabet.omicron,
                "pi":perfect.alphabet.pi,
                "koppa":perfect.alphabet.koppa
            },
            Hundreds : {
                "rho":perfect.alphabet.rho,
                "sigma":perfect.alphabet.sigma,
                "tau":perfect.alphabet.tau,
                "upsilon":perfect.alphabet.upsilon,
                "phi":perfect.alphabet.phi,
                "chi":perfect.alphabet.chi,
                "psi":perfect.alphabet.psi,
                "omega":perfect.alphabet.omega,
                "sampi":perfect.alphabet.sampi
            }
        }
        perfect.numbers = [[perfect.numerals.Units.alpha,perfect.numerals.Tens.iota,perfect.numerals.Hundreds.rho],
                [perfect.numerals.Units.beta,perfect.numerals.Tens.kappa,perfect.numerals.Hundreds.sigma],
                [perfect.numerals.Units.gamma,perfect.numerals.Tens.lambda,perfect.numerals.Hundreds.tau],
                [perfect.numerals.Units.delta,perfect.numerals.Tens.mu,perfect.numerals.Hundreds.upsilon],
                [perfect.numerals.Units.epsilon,perfect.numerals.Tens.nu,perfect.numerals.Hundreds.phi],
                [perfect.numerals.Units.digamma,perfect.numerals.Tens.xi,perfect.numerals.Hundreds.chi],
                [perfect.numerals.Units.zeta,perfect.numerals.Tens.omicron,perfect.numerals.Hundreds.psi],
                [perfect.numerals.Units.eta,perfect.numerals.Tens.pi,perfect.numerals.Hundreds.omega],
                [perfect.numerals.Units.theta,perfect.numerals.Tens.koppa,perfect.numerals.Hundreds.sampi]]
            return perfect
    }
})()