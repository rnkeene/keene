(function(){
    Greek.Knowledge = function(){
        var time = Greek();
        var speak = Language();
        time.Decay.flip();
        time.invert = function(){
            time.Decay.flip();
        }
        time.apply = function(){
            time.Decay.clock();
        }
        time.loop = function(){
            time.apply();
            time.apply();
            time.apply();
            time.apply();
        }
        var clone = {
            language : speak,
            logical : time
        }
        Greek = null;
        delete Greek;
        Language = null;
        delete Language;
        return clone;
    }
    Greek.Knowledge.Time = function(){
        var time = Greek.Knowledge();
        var construct = "";
        var population = [time.logical.Decay.id];
        var offset = [0];
        var binary = population[0].toString(2);
        var cloud = [];
        while(population[population.length-1]>1){
            var temp = population[population.length-1];
            var able = temp.toString(2);
            able = able.charAt(able.length-1);
            offset.push(parseInt(able,2));
            if(offset[offset.length-1]){
                cloud.push(offset.length-1)
                temp--
            }
            population.push(temp/2);
        }
        var recheck = 1
        var orbit = 0
        construct = 1
        for(var x=1; x<population.length;x++){
            orbit += offset[x]
            construct += ""+offset[x]
            recheck += population[x]
        }
        var natural=(orbit+recheck==population[0]);
        if(natural){
            return function(){
                var clone = {
                    greek : time,
                    digital : binary,
                    reflection : construct,
                    degenerates : population,
                    orbits : cloud,
                    number : orbit
                }
                return clone;
            }
        }
        return Greek.Knowledge.Time();
    }
    Greek.Knowledge.Management = function(){
        var time = Greek.Knowledge.Time();
        time.debug = function(){
            console.groupCollapsed("State: ",time.Decay.state());
            console.log("Spin: ",time.Decay.spin);
            console.log("Flow: ",time.Decay.flow);
            console.log("x: ",time.x);
            console.log("y: ",time.y);
            console.log("z: ",time.z);
            console.groupEnd();
        }
        time.slow = {
            partial: function(){
                console.groupCollapsed("Traversal: ",time.Decay.id);
                time.slow.forward();            
                time.invert();
                time.slow.forward();
                console.groupEnd();
            },
            forward : function(){
                console.groupCollapsed("Flow: ",time.Decay.id);
                time.apply();
                time.debug();
                console.groupEnd();
            }
        }
        return time;
    }
    var space = Greek.Knowledge.Management();
    Pond = function(){
        var frog = space;
        var lilly = space;
        var pond = {
            x : frog,
            y : lilly
        }
        var reveal = function(){
            var clone = pond;
            Pond = null;
            delete Pond;
            return clone;
        }
        return reveal;
    }
})()