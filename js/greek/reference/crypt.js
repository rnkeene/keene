(function(){
    Greek = function(){
        var crypt = {
            Electron : {
                Logical : {
                    odd : 1,
                    even : 0,
                    know : [1,0],
                    think : [0,1]
                },
                Count : {
                    odd : [0,1,1,1],
                    even : [1,0,0,0]
                },
                Direction : {
                    up: [[0,1],[1,0]],
                    down: [[1,0],[0,1]],
                    left: [[0,1],[1,0]],
                    right: [[1,0],[0,1]]
                },
                State : {
                    excited : [1,0,1],
                    resting : [0,1,0]
                }
            },
            RTL : {
                North : {
                    x : [1,1,1,1],
                    y : [1,1,1,0],
                    z : [0,0,0,1]
                },
                East : {
                    x : [0,0,0,0],
                    y : [0,0,0,1],
                    z : [0,1,1,1]
                },
                South : {
                    x : [1,0,0,0],
                    y : [1,1,1,0],
                    z : [1,1,1,1]
                },
                West : {
                    x : [1,1,1,0],
                    y : [0,0,0,1],
                    z : [0,0,0,0]
                }
            },
            LTR : {
                North : {
                    x : [1,1,1,0],
                    y : [0,0,0,1],
                    z : [0,0,0,0]
                },
                East : {
                    x : [1,0,0,0],
                    y : [1,1,1,0],
                    z : [1,1,1,1]
                },
                South : {
                    x : [0,0,0,0],
                    y : [0,0,0,1],
                    z : [0,1,1,1]
                },
                West : {
                    x : [1,1,1,1],
                    y : [1,1,1,0],
                    z : [0,0,0,1]
                }
            },
            Hash : {
                Compass : {
                    Decimate : [10,5,1],
                    Obliterate : [3,6,9],
                    Even : ["1010","101","1"],
                    Odd : ["11","110","1001"]
                },
                Magick : {
                    DRTL : [[180],[90],[45]],
                    DLTR : [[30],[45],[90]],
                    BRTL : [["10110100"],["1011010"],["101101"]],
                    BLTR : [["11110"],["101101"],["1011010"]],
                    HRTL : [["B4"],["5A"],["2D"]],
                    HLTR : [["1E"],["2D"],["5A"]],
                    HDRTL: [[["1011-100",["0"]],[["0"],["101-1010"]],["0"],["0"],"10-1110"]],
                    HDLTR: [[["0"],["0"],["0"],["1-1010"]],[["0"],["0"],["10-1110"]],[["0"],["101-1010"]]]
                }
            }
        }
        crypt.atom = {
            alpha : function(){            
                crypt.alternate=function(){
                    switch (crypt.Decay.spin){
                        case 3:
                            crypt.x=crypt.RTL.North.x,
                            crypt.y=crypt.RTL.North.y,
                            crypt.z=crypt.RTL.North.z,
                            crypt.Decay.spin=0;
                            break;
                        case 0:
                            crypt.x=crypt.RTL.East.x,
                            crypt.y=crypt.RTL.East.y,
                            crypt.z=crypt.RTL.East.z;
                            crypt.Decay.spin=1;
                            break;
                        case 1:
                            crypt.x=crypt.RTL.South.x,
                            crypt.y=crypt.RTL.South.y,
                            crypt.z=crypt.RTL.South.z;
                            crypt.Decay.spin=2;
                            break;
                        case 2:
                            crypt.x=crypt.RTL.West.x,
                            crypt.y=crypt.RTL.West.y,
                            crypt.z=crypt.RTL.West.z;
                            crypt.Decay.spin=3;
                            break;
                    }
                }
            },
            beta : function(){
                crypt.alternate=function(){
                    switch (crypt.Decay.spin){
                        case 3:
                            crypt.x=crypt.LTR.South.x,
                            crypt.y=crypt.LTR.South.y,
                            crypt.z=crypt.LTR.South.z;
                            crypt.Decay.spin=0;
                            break;
                        case 0:
                            crypt.x=crypt.LTR.East.x,
                            crypt.y=crypt.LTR.East.y,
                            crypt.z=crypt.LTR.East.z;
                            crypt.Decay.spin=1;
                            break;
                        case 1:
                            crypt.x= crypt.LTR.North.x,
                            crypt.y= crypt.LTR.North.y,
                            crypt.z= crypt.LTR.North.z,
                            crypt.Decay.spin=2;
                            break;
                        case 2:
                            crypt.x=crypt.LTR.West.x,
                            crypt.y=crypt.LTR.West.y,
                            crypt.z=crypt.LTR.West.z;
                            crypt.Decay.spin=3;
                            break;
                    }
                }
            }
        }
        crypt.Decay = {
            direction : [["East","South","West","North"],["West","North","East","South"]],
            spin : 0,
            flow : 1,
            current : [crypt.atom.alpha,crypt.atom.beta],
            id : Date.now(),
            clock : function(){
                crypt.alternate();
            },
            flip : function(){
                switch(crypt.Decay.flow){
                    case 0:
                        crypt.Decay.flow=1;
                        break;
                    case 1:
                        crypt.Decay.flow=0;
                        break;
                }
                crypt.Decay.charge=crypt.Decay.current[crypt.Decay.flow];
                crypt.Decay.charge();
                crypt.Decay.clock();
            },
            state : function(){
                return crypt.Decay.direction[crypt.Decay.flow][crypt.Decay.spin]
            }
        }
        return crypt;
    }
})()