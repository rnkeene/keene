(function(){
    GameJedi = function(){
        var dimension = Mind()();
        var debugJedi = function(){
            console.log("Jedi Mind Trick:: ",dimension);
            console.log("JMT-Energy 0:: ",dimension.energy[0]);
            console.log("JMT-Energy 1-A:: ",dimension.energy[1][0]);
            console.log("JMT-Energy 1-a:: ",dimension.energy[1][1]);
            console.log("JMT-Energy 1-1:: ",dimension.energy[1][2]);
            console.log("JMT-Energy 1-0:: ",dimension.energy[1][3]);
            console.log("JMT-Energy 2-A:: ",dimension.energy[2][0]);
            console.log("JMT-Energy 2-a:: ",dimension.energy[2][1]);
            console.log("JMT-Energy 2-1:: ",dimension.energy[2][2]);
            console.log("JMT-Energy 2-0:: ",dimension.energy[2][3]);
        }
        var chaos = {
            source : function(){
                return chaos.dimension;
            },
            dimension : dimension,
            unreal : dimension.energy[0],
            aReal : dimension.energy[1][0],
            iReal : dimension.energy[1][1],
            IReal : dimension.energy[1][2],
            AReal : dimension.energy[1][3],
            aNormal : dimension.energy[2][0],
            iNormal : dimension.energy[2][1],
            INormal : dimension.energy[2][2],
            ANormal : dimension.energy[2][3],
            order : function(){
                var clone = {
                    box : chaos.source,
                    G : chaos.dimension,
                    T : chaos.unreal,
                    a : chaos.aReal,
                    i : chaos.iReal,
                    I : chaos.IReal,
                    A : chaos.AReal,
                    x : chaos.aNormal,
                    y : chaos.iNormal,
                    z : chaos.INormal,
                    dz : chaos.ANormal
                }
                clone.xfly = clone.x.chickenEgg[0];
                clone.xland = clone.x.chickenEgg[1];
                clone.yjump = clone.y.chickenEgg[0];
                clone.yland = clone.y.chickenEgg[1];
                clone.parallel = {
                    linear : clone.G.Magnetic.energy.proof,
                    balance : clone.G.Magnetic.energy.symmetry,
                    steps : clone.G.Magnetic.energy.repeat,
                    reflect : function(){
                        while(clone.parallel.steps.length>0){
                            clone.parallel.steps.pop();
                        }
                    }
                }
                GameJedi = null;
                delete GameJedi;
                return clone;
            }
        }
        return chaos.order;
    }
    var debugOrder = function(){
        Order = GameJedi()();
        console.log(Order);
        console.log(Order.a());
        console.log(Order.a());
        console.log(Order.a());
        console.log(Order.i());
        console.log(Order.i());
        console.log(Order.i());
        console.log(Order.I());
        console.log(Order.I());
        console.log(Order.I());
        console.log(Order.A());
        console.log(Order.A());
        console.log(Order.A());
        console.log(Order.parallel);
        Order.parallel.reflect();
        console.log();
    }
})()