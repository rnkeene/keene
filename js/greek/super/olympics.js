(function(){
    Olympics = GameJedi()();
    Olympics.calculi = function(){
        var golden = Olympics;
        return function(){
            var metric = {
                fx : function(){
                    this.dxt=golden.xfly();
                    golden.a();
                    this.dxdt = golden.xland();
                },
                fy : function(){
                    this.dy=golden.yjump();
                    golden.i();
                    this.dyt = golden.yland();
                },
                fyx : function(){
                    this.dy=golden.yjump();
                    golden.I();
                    this.dyt = golden.yland();
                    this.dxt= golden.xfly();
                    this.dxdt = golden.xland();
                },
                fxyx : function(){
                    this.dy= golden.yjump();
                    this.dyt = golden.yland();
                    golden.A();
                    this.dxt= golden.xfly();
                    this.dxdt = golden.xland();
                }
            }
            Olympics = null;
            delete Olympics;
            return metric;
        }
    }
})()