(function(){
    DarkJedi = function(){
        var knowledge = Olympics;
        knowledge.strong = function(){
            return Olympics.calculi()();
        }
        var knight = Olympics.box();
        var element = {
            self : function(){
                var destruct = knowledge.strong();
                element.know = destruct,
                element.friend = destruct,
                element.defeat = destruct,
                element.enemy = destruct;
            },
            control : function(){
                element.know.fx();
                element.friend.fy();
                element.defeat.fyx();
                element.enemy.fxyx();
                element.self = null;
                delete element.self;
            }
        }
        element.self();
        element.control();
        knight.control = element;
        knight.control.control=null;
        delete knight.control.control;
        return knight;
    }
    var fear = DarkJedi();
    LightJedi = function(){
        var element = {
            base : fear
        }
        DarkJedi = null;
        delete DarkJedi;
        return function(){
            var clone = element.base;
            LightJedi = null;
            delete LightJedi;
            return clone
        }
    };
    var observe = LightJedi()()
    console.log(observe);
    console.log(observe.Gravity());
})()