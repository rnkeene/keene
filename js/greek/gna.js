
////var spark = Greek.TreeLifeSource.Lorenz.Model();
//console.log(spark);
//console.log(spark.length);
/*
 *
 *  E=MC2 should be modeled as a Cyclic Graph; 
 *      with head attached to tail, length 4.
 *      
 *  This will create a natural "pulse" based on frequency of time change it takes to traverse from node 1 to node 4 (which is back to the start state)
 *  
 *  Details (rough idea):
 *  
 *      Some spark = [[A,A'],[T,[B,[B'],[A',A]];
 *      
 *      A = function(E){return MC2}
 *      B = function(E){return partialState(8 or 3) of MC2}
 *      B' = function(E){return oppositePartialState(8 or 3) of MC2}
 *      A' = the original function;
 *  
 *  The Start of the Array will Point to a Radial point T (Time=dt)
 *  
 *  The Radial point (Time) will "Lift" (be aware of) A' and B (or B')
 *  
 *  This creates a "Normal Grammar" at Time T based on the "Time Hash" (index).
 *      The Time Hash can be reduced using euclidean geometry 'cleaning' circular references.
 *  
 *  Normal Flow: 
 *      [A -> B] therefor [A -> T -> B; because B' -> A']
 *          == Tower of Hanoi 'Small Disk' Move ==
 *          (At Beta attractor = 8)
 *          T -> B -> A
 *          
 *          == Tower of Hanoi 'Even Disk' Move (e.g. +-1 afterward) ==
 *          (At Beta attractor = 3)
 *          T -> B' -> A' which maps through the a 'logical' (designed in) transitive association.
 *          
 *  == Continued Explanation == 
 *      That Lorenz equations fits perfectly into the Hanoi Tower Problem.
 * 
 *      There are two possible iterative behaviors:
 * 
 *          1. l,t,r,l,t,r,etc for the "smallest disk" (odd/prime)
 *          2. r,t,l,r,t,l,etc for the "largest disk" (even)
 *  
 *      In binary: 
 *  
 *          1. The pattern 11, 01 are odd states; 
 *              1B. only one is smallest.
 *          2. The pattern 10  is always even.
 *  
 *      The timestamps have already been divided by 2 where even;
 *          otherwise "-1" spin was applied to the number, and it was divided by two.
 *  
 *      In this way all timestamps are divided "naturally" into 42 patterns (timestamps don't decay)
 *  
 *      This time can be used to measure the frequency (disruption in time) caused using E=mc2
 *  
 *  
 *  ==== "Appears Like" ====
 *  
 *  LEVEL 1 (s-orbital): [A][A']
 *      //therefor sOrbit[0]() references "A" above and leads to:
 *  LEVEL 2 (e.g. transition/move-space-time): [T][B]
 *      //therefor transition[0]() references "T" above and leads to
 *  LEVEL 3 (p-orbital): [B][B']
 *      //therefor transition[0]() references "B" above and leads to :      
 *  FEEDBACK Loop (Partial State/Spin): [A]|[A']
 *      //in which case; calling the butterfly function (e.g. resetting spin state)
 *      
 *  ==== BINARY Loop "Appearance" ====
 *  The Simplest Model would be:
 *      10=A (e.g. "Ready")
 *      00=T (e.g. "Go")
 *      11=B (e.g. "1 more" than A: 2 moved to 3)
 *      01=A' (e.g. "A'" is the reflection of A
 *      flipping bits for A' returns it to A.
 *      
 *  ==== Stack this into a Steady Pattern ====
 *  
 *  Therefor manipulating the bits on the following would be sufficient:
 *          [10,00,11,01]
 *          
 *  imagine you could enter this loop and traverse it like a 'mobius strip' at any point;
 *      this in turn is mapped to "some function" that could be invoked.
 */