(function(){
    var natural = Pond()();
    var prime = {
        left : function(){
            return prime.x-1
        },
        right:function(){
            return prime.y+1
        }
    }
    natural.A = prime;
    natural.a = prime;
    natural.A.x=1
    natural.A.y=0
    natural.a.x=0
    natural.a.y=1
    var frog = {
        chickenEgg : [natural.x,natural.y],
        back: [0,0],
        forward: [1,1],
        LTR: natural.A,
        RTL: natural.a
    }
    var pond = {
        lilly : [frog,frog],
        natural : function(){
            var clone = {
                nature : natural,
                life : pond.lilly
            }
            natural = null;
            frog = null;
            pond = null;
            delete natural;
            delete frog;
            delete pond;
            return clone;
        }
    }
    Nature = pond.natural()
    var debugNature = function(){
        console.log("Nature: ",Nature);
        console.log("Life 0: ",Nature.life[0])
        console.log("Life 1: ",Nature.life[1])
        console.log("nature A: ",Nature.nature.A)
        console.log("nature A.left: ",Nature.nature.A.left())
        console.log("nature A.right: ",Nature.nature.A.right())
        console.log("nature a: ",Nature.nature.a)
        console.log("nature a.left: ",Nature.nature.a.left())
        console.log("nature a.right: ",Nature.nature.a.right())
        console.log("Life 0 = LTR(x): ",Nature.life[0].LTR.x)
        console.log("Life 0 = LTR(y): ",Nature.life[0].LTR.y)
        console.log("Life 0 = LTR.left(): ",Nature.life[0].LTR.left())
        console.log("Life 0 = LTR.right(): ",Nature.life[0].LTR.right())
        console.log("Life 1 = LTR(x): ",Nature.life[1].LTR.x)
        console.log("Life 1 = LTR(y): ",Nature.life[1].LTR.y)
        console.log("Life 1 = LTR.left(): ",Nature.life[1].LTR.left())
        console.log("Life 1 = LTR.right(): ",Nature.life[1].LTR.right())
    }
    Language = {
        expression : function(){
            var clone = {
                x : Nature.life[0],
                y : Nature.life[1],
                dx : Nature.nature.A,
                dy : Nature.nature.a,
                xpath : function(){
                    return [clone.x.LTR.left(),clone.x.LTR.right(),clone.x.RTL.left(),clone.x.RTL.right()]
                },
                ypath : function(){
                    return [clone.y.LTR.left(),clone.y.LTR.right(),clone.y.RTL.left(),clone.y.RTL.right()]
                },
                dxpath : function(){
                    return [clone.x.LTR.x,clone.x.LTR.y,clone.x.RTL.x,clone.x.RTL.y]
                },
                dypath : function(){
                    return [clone.y.LTR.x,clone.y.LTR.y,clone.y.RTL.x,clone.y.RTL.y]
                }
            }
            Nature = null;
            Language = null;
            delete Nature;
            delete Language;
            return clone;
        }
    }
    var infinite = Language.expression();
    var debugInfinite = function(){
        console.log("Infinite %o", infinite);
        console.log("Infinite x: ", infinite.x)
        console.log("Infinite y: ", infinite.y)
        console.log("Infinite x-Path: ", infinite.xpath())
        console.log("Infinite y-Path: ", infinite.ypath())
        console.log("Infinite dx-Path: ", infinite.dxpath())
        console.log("Infinite dy-Path: ", infinite.dypath())
    }
    
    var normal = function(){
        var count = 0;
        var pattern = [];
        var time = [];
        var play = [];
        var complete = infinite;
        var mobius = function(result){
            count++;
            return [count,pattern.push(result)];
        }
        var aRTL = function(){
            return time.push(mobius(complete.xpath()));
        }
        var aLTR = function(){
            return time.push(mobius(complete.ypath()));
        }
        var ARTL = function(){
            return time.push(mobius(complete.dxpath()));
        }
        var ALTR = function(){
            return time.push(mobius(complete.dypath()));
        }
        var at = function(){
            play.push(aRTL);
            return aRTL();
        }
        var bt = function(){
            play.push(aLTR);
            return aLTR();
        }
        var ct = function(){
            play.push(ARTL);
            return ARTL();
        }
        var dt = function(){
            play.push(ALTR);
            return ALTR();
        }
        var circuit = function(){
            var clone = {
                size : count,
                symmetry : pattern,
                proof : time,
                repeat : play,
                universe : complete,
                map : mobius,
                fx : at,
                gx : bt,
                At : ct,
                Dt : dt
            }
            normal = null;
            delete normal;
            return clone;
        }
        return circuit;
    }
    var balance = normal();
    Force = function(){
        var center = balance();
        var debugForce = function(){
            console.log(balance);
            console.log(center);
        }
        var history = [];
        var action = [];
        var magick = [];
        var power = [];
        var push = function(){
            history.push([center.universe.xpath(),center.universe.ypath()]);
            action.push([center.universe.xpath,center.universe.ypath]);
            magick.push([center.universe.dxpath,center.universe.dypath]);
        }
        var pull = function(){
            history.push([center.universe.dxpath(),center.universe.dypath()]);
            action.push([center.universe.dxpath,center.universe.dypath]);
            magick.push([center.universe.xpath,center.universe.ypath]);
        }
        var trick = function(){
            power.push([push,pull]);
        }
        var reveal = function(){
            var controller = [];
            controller.push(center.map);
            controller.push([center.At,center.Dt,center.fx,center.gx]);
            controller.push([center.universe.x,center.universe.y,center.universe.dx,center.universe.dy]);
            return controller;
        }
        var hash = function(){
            var clone = {
                supply : function(){
                    clone.alternate[0]();
                    clone.alternate[1]();
                    return clone.unit[1];
                },
                alternate : [push,pull],
                current : [history,action,magick,power],
                energy : center,
                unit : [trick,reveal]
            }
            Force = null;
            delete Force;
            return clone;
        }
        return hash;
    }
    Mind = function(){
        var vanishing = Force();
        var reality = vanishing();
        var charge = reality.supply();
        var measure = charge();
        var debugMind = function(){
            console.log(vanishing);
            console.log(reality);
            console.log(charge);
            console.log(measure);
            console.log("Alternate, Push and Pull",reality.alternate);
            console.log("Current: [[history-real],[action-inverse],[magick-(all:history+action)],[power[push,pull]] == ",reality.current);
            console.log("Energy: [center]",reality.energy);
            console.log("Unit: [trick,reveal]",reality.unit);
        }
        var Jedi = function(){
            var trick =  {
                power : vanishing,
                field : reality,
                polar : charge,
                energy : measure,
                Gravity : reality.supply,
                Density : {
                    Real : reality.current[0],
                    Normal : reality.current[1],
                    Degenerate : reality.current[2],
                    Identity : reality.current[3]
                },
                Magnetic : {
                    energy : reality.energy
                },
                Measure : {
                    part : reality.unit
                }
            }
            Mind = null;
            delete Mind;
            return trick;
        }
        return Jedi;
    }
})()