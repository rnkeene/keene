(function(){
    var kernel = arguments[0][0];
    function clone(){
        return [kernel,3];
    }
    graph = [clone,-3];
}(
    (function(){
        var kernel = arguments[0][0];
        function clone(){
            return [kernel,1];
        }
        return [clone,-1];
    }(
        (function(){
            var kernel = arguments[0][0];
            function clone(){
                return [kernel,-1];
            }
            return [clone,1];
        }(
            (function(){
                function clone(){
                    return [function(){
                        return 0
                        },-3];
                }
                return [clone,3];
            }()
            )
        ))
    ))
));
(function(){
    var n0 = [graph[0](),graph[1]];
    var v0 = [graph[0],5];
    var n1 = [n0[0][0](),n0[0][1]];
    var v1 = [n0[0],n1[0][1]];
    var n2 = [n1[0][0](),n1[0][1]];
    var v2 = [n1[0],n2[0][1]];
    var n3 = [n2[0][0](),n2[0][1]];
    var v3 = [n2[0],n3[0][1]];
    delete(graph);
    var axiom = function(){
        var clone = [[v0,v1,v2,v3],[n0,n1,n2,n3]];
        return clone;
    }
    var biquinaryQuadruplex = function(){
        return {
            root : axiom(),
            edges : function(){
                return this.root[0];
            },
            nodes : function(){
                return this.root[1];
            },
            vectors : function(){
                var clone = this.edges();
                return [clone[0][1],clone[1][1],clone[2][1],clone[3][1]];
            },
            scalars : function(){
                var clone = this.nodes();
                return [clone[0][1],clone[1][1],clone[2][1],clone[3][1]];
            }
        }
    }
    var counter = {
        system: biquinaryQuadruplex(),        
        hasse : function(){
            var matrix=[];
            var edge = [];
            matrix.push([[0,1],[0,2],[0,4],[0,8]]);
            edge.push([1,2,4,8]);//[+1,+2,+4,+8] normal
            
            matrix.push([[1,3],[1,5],[1,9],[1,0]]);
            edge.push([2,4,8,-1]);//[+2,+4,+8,-1] normal
            
            matrix.push([[2,3],[2,6],[2,10],[2,0]]);
            edge.push([1,4,8,-2]);//[+1,+4,+8,-2] 2x degenerate
            
            matrix.push([[3,7],[3,11],[3,2],[3,1]]);
            edge.push([4,8,-1,-2]);//[+4,+8,-1,-2] normal
            
            matrix.push([[4,5],[4,6],[4,12],[4,0]]);
            edge.push([1,1,8,-4]);//[+1,+1,+8,-4] 2x degenerate
            
            matrix.push([[5,7],[5,13],[5,4],[5,1]]);
            edge.push([2,8,-1,-4]);//[+2,+8,-1,-4] 1x degenerate
            
            matrix.push([[6,7],[6,14],[6,4],[6,2]]);
            edge.push([1,8,-2,-4]);//[+1,+8,-2,-4] 1x degenerate
            
            matrix.push([[7,15],[7,6],[7,5],[7,3]]);
            edge.push([8,-1,-2,-4]);//[+8,-1,-2,-4] normal
            
            /**
             * Transition to higher Order.             
                matrix.push([[8,9],[8,10],[8,12],[8,0]]);
                matrix.push([[9,11],[9,13],[9,8],[9,1]]);
                matrix.push([[10,11],[10,14],[10,8],[10,2]]);
                matrix.push([[11,15],[11,10],[11,9],[11,3]]);
                matrix.push([[12,13],[12,14],[12,8],[12,4]]);
                matrix.push([[13,15],[13,12],[13,9],[13,5]]);
                matrix.push([[14,15],[14,12],[14,10],[14,6]]);
                matrix.push([[15,14],[15,13],[15,11],[15,7]]);//[-1,-2,-4,-8] reflection of "matrix[0]"
            */
            return [matrix,edge];
        },
        lineOfSight : function(relative, position){
            console.log(position);
            
            var x= position[0];
            var y= position[1];
            console.log("Position{ x:%i, y:%i }",x,y);
            
            var observe = relative[0][x][y];
            var present = relative[1][x][y];
            var past = observe[0];
            var future = observe[1];
            
            console.log("\tPast: %o", past);
            console.log("\tPresent: %o", present);
            console.log("\tFuture: %o", future);
        },
        countTime: function(){
            var linear = this.hasse();
            for( var x=0, max=linear[0].length; x<max ; x++){
                for( var y=0, check=linear[1][0].length; y<check;y++){
                    this.lineOfSight(linear,[x,y]);
                }
            }
        }
    }
    counter.countTime();
}())

var biZero = "0000";
var biOne = "0001";
var biTwo = "0010";
var biThree = "0011";
var biFour = "0100";
var biFive = "0101";
var biSix = "0110";
var biSeven = "0111";

var biEight = "1000";
var biNine = "1001";
var biTen = "1010";
var biEleven = "1011";
var biTwelve = "1100";
var biThirteen = "1101";
var biFourteen = "1110";
var biFifteen = "1111";

/**
 * Match to equivalent decimal representation
 */

var hex1 = "1";
var hex2 = "2";
var hex3 = "3";
var hex4 = "4";
var hex5 = "5";
var hex6 = "6";
var hex7 = "7";
var hex8 = "8";
var hex9 = "9";
var hex10 = "a";
var hex11 = "b";
var hex12 = "c";
var hex13 = "d";
var hex14 = "e";
var hex15 = "f";

/**
 * Match to equivalent 'Alpha'-bet representation
 * abcdef-[ghijklmno]-[pqrst]-uvwxyz
 * ABCDEFGHIJKLMNOPQRSTUVWXYZ
 * 
 * > Grammar (Normalized)
 * > Language (ref Grammar)
 * 
 */
/**
 * Match to equivalent 'Geometry' representation
 */
/**
 * Match to equivalent 'Algebra' representation
 */
/**
 *
 * Physics (E=mc^2), Chemestry (Periodic Table), Biology, "Nature/Natural"
 *  
 * Graph Theory, Graph Theory, Graph Theory (See Helmut Hasse; to start)
 */
/**
 * binary tree will hold proper grammers.
 */