function errorTryCatch(){
    try{
        var MyDOMException = {            
            name:        "System Error", 
            level:       "Show Stopper", 
            message:     "Error detected. Please contact the system administrator.", 
            htmlMessage: "Error detected. Please contact the <a href=\"mailto:sysadmin@acme-widgets.com\">system administrator</a>."
        }
        throw MyDOMException;
    }catch(err){
        printObj(err);
    }
    try{
        document.appendChild(window)
    }catch(np){
        printObj(np);
    }
    try{
        throw new Error("Using a 'new' Error")
    }catch(err){
        console.log(err);
        for(var propc in err){
            console.log(propc);
        }
    }
}