(function(){
    ____$._$("input/drag");
    ____$.html = {
        global : this,
        document : document.firstChild,
        head : document.head,
        tail : document.body,
        next : function(){
            return arguments[0].nextSibling
        }
    }
    ____$.input = function(){
        var type="text"
        return function(name){
            var title = name
            var frame = {
                x : document.createElement("div"),
                y : document.createElement("input")
            }
            var place = function(){
                this.frame.y.type = type;
                this.frame.y.value = title
                this.frame.y.onclick=function(){
                    frame.y.value="";
                    frame.y.onclick=null;
                }
                this.frame.x.appendChild(this.frame.y);
            }
            return {
                title : title,
                frame : frame,
                place : place,
                current : function(){
                    return this.frame.y.value;
                }
            }
        }
    }
    var theWidth= function(){
        return parseInt(window.innerWidth*.97);
    }
    var theHeight= function(){
        return parseInt(window.innerHeight*.94);
    }
    document.body.ondrop = function(ev){
        ev.preventDefault();
        var data=ev.dataTransfer.getData("Text");
        var target = ____$.html.target[data];
        target.style.left = ev.clientX+"px";
        target.style.top = ev.clientY+"px";
    }
    document.body.ondragover = function(ev){ev.preventDefault();}
    document.body.style.width = theWidth(),document.body.style.height = theHeight();
    
    ____$.$_("input/style")();
})()