(function(){
    function Constant(scale){
        var line = {
            x : scale,
            max : max,
            y : max
        };
        return line;
    }

    function Linear(){
        return Constant(1);
    }
    function Time(){
        return [Linear(), Constant(2)];
    }
    function Perfect(){
        return [Constant(3),Constant(4),Constant(5)];
    }
    function Real(){
        return [Time(), Perfect()];
    }
    var max = 100;
    var offset = 50;
    var whereat;
    function hiClick(event){
        var xclick = event.clientX + offset
        var yclick = event.clientY + offset
        whereat = [xclick,yclick];
    }
    document.addEventListener("click",hiClick, true);
            
    function cleanStyle(){
        var css = 'canvas { background-color: #ffffff; display: inline; position: fixed; top: ' + whereat[1] + 'px; left: ' + whereat[0] + 'px }',head = document.getElementsByTagName('head')[0],style = document.createElement('style');
        style.type = 'text/css';
        if (style.styleSheet){
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        head.appendChild(style);
    }
            
    function cleanCanvas(){
        var area = document.getElementById('pop');
        if(!area){
            area = document.createElement("div");
            area.id = "pop";
            document.body.appendChild(area);
        }
        area.className = "canvas";
        area.innerHTML='';
        cleanStyle();
                
        var canvas = document.createElement("canvas");
        canvas.id = "active";
        canvas.width=max
        canvas.height=max
        area.appendChild(canvas);
        return canvas;
    }
            
    function drawReal(){
        var area = cleanCanvas();
        var graph = Real();
                
        var linear = graph[0][0].x * graph[0][0].max;
        drawLine(linear, graph[0][0].y);
                
        var time = graph[0][1].x * graph[0][1].max;
        drawLine(time, graph[0][1].y);
                
        var x = graph[1][0].x * graph[1][0].max;
        drawLine(x, graph[1][0].y);
                
        var y = graph[1][1].x * graph[1][1].max;
        drawLine(y, graph[1][1].y);
                
        var z = graph[1][2].x * graph[1][2].max;
        drawLine(z, graph[1][2].y);
                
        function drawLine(x, y){
            var ctx=area.getContext("2d");
            console.log("Draw Line: x=%i, y=%i", x,y)
            ctx.beginPath();
            ctx.moveTo(0,0);
            ctx.lineTo(x,y);
            ctx.stroke();
        }
                
        drawIntercept(1/5*graph[0][0].max);
        drawIntercept(1/2*graph[0][1].max);
        drawIntercept(1/3*graph[1][0].max);
        drawIntercept(1/4*graph[1][1].max);
        drawIntercept(graph[1][1].max);
                
        function drawIntercept(y){
            var ctx=area.getContext("2d");
            console.log("Draw Line: x=%i, y=%i", x,y)
            ctx.beginPath();
            ctx.moveTo(0,y);
            ctx.lineTo(y,0);
            ctx.stroke();
        }
    }
            
    function drawRectangle(){
        var c = cleanCanvas();
        var ctx=c.getContext("2d");
        ctx.fillStyle="#FF0000";
        ctx.fillRect(0,0,150,75);
        return false;
    }
    function drawLine(){
        var c = cleanCanvas();
        var ctx=c.getContext("2d");
        ctx.moveTo(0,0);
        ctx.lineTo(300,150);
        ctx.stroke();
    }
    function drawCircle(){
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");
        ctx.beginPath();
        ctx.arc(95,50,40,0,2*Math.PI);
        ctx.stroke();
    }
    function drawText(){
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");
        ctx.font="30px Arial";
        ctx.fillText("Hello World",10,50);
    }
    function drawStrokeText(){
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");
        ctx.font="30px Arial";
        ctx.strokeText("Hello World",10,50);
    }
    function drawLinearGradient(){
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");

        // Create gradient
        var grd=ctx.createLinearGradient(0,0,max,0);
        grd.addColorStop(0,"red");
        grd.addColorStop(1,"white");

        // Fill with gradient
        ctx.fillStyle=grd;
        ctx.fillRect(10,10,150,80);
    }
    function drawCircularGradient(){
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");

        // Create gradient
        var grd=ctx.createRadialGradient(75,50,5,90,60,100);
        grd.addColorStop(0,"red");
        grd.addColorStop(1,"white");

        // Fill with gradient
        ctx.fillStyle=grd;
        ctx.fillRect(10,10,150,80);
    }
    function drawImage(){
        var c=document.getElementById("myCanvas");
        var ctx=c.getContext("2d");
        var img=document.getElementById("scream");
        ctx.drawImage(img,10,10);
    }
}())
object[object.identity[object.identity.length-1]] = {
    close : function(){
        object.disorient(object.morphism[object.morphism.length-1]);
    }
}
console.log(object);


