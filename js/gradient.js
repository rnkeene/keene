(function(){
    window["canvas"]=function(){
        return {
            create: function(){
                this.canvas = window["element"].create("canvas")
            },
            context: function(){
                return this.canvas.getContext('2d')
            },
            add:function(element){
                document.body.appendChild(element)
            }
        }
    }
    window["canvas"]["color"]={
        COLORS:['#FFFFCE','#00385A'],
        set:function(min,max){
            this.COLORS=['#',min,'#',max].join("");
        },
        visible:function(){
            return this.COLORS;
        }
    }
    window["canvas"]["gradient"]=function(){
        var local = window["canvas"]();
        local.create();
        local.add(local.canvas)
        var css = arguments[0];
        css = css.css;
        var scale = function(){
            return parseInt(css.halfWidth()*.1);
        }
        var canvas = local.canvas,context = local.context();
        var gradient = function(){
            try{
                var origin = function(scale){
                    return{
                        scale:scale,
                        startX:css.halfWidth()-scale,
                        startY:css.halfHeight()-scale,
                        startRadius:scale,
                        endX:css.halfWidth()+scale,
                        endY:css.halfHeight()+scale,
                        endRadius:scale*10,
                        colorstop:window["canvas"]["color"].visible()
                    }
                }
                return origin(scale());
            }catch(e){
                console.log(e)
            }
        }
        var reload= function(){
            try{
                css = window["style"]["css"].css();
                canvas.height=css.css.innerHeight();
                canvas.width=css.css.innerWidth();
                var style = [];
                style.push(css.theBorder(1,"solid","black"));
                style.push(css.thePosition());
                style.push(css.theLeft(0));
                style.push(css.theTop(0));
                style.push(css.theIndex(-1));
                style.push(css.theMargin(5));
                css.update(canvas.name,style);
            }catch(e){
                console.log(e)
            }
        }
        var react = function(event){
            try{
                var radial = gradient();
                var scale= radial.scale;
                if(event!=undefined){
                    radial.startX=event.clientX-scale,
                    radial.startY=event.clientY-scale,
                    radial.endX=event.clientX+scale,
                    radial.endY=event.clientY+scale
                }
                var update = function(){
                    context.rect(0, 0, canvas.width, canvas.height);
                    var grd = context.createRadialGradient(radial.startX, radial.startY, radial.startRadius, radial.endX, radial.endY, radial.endRadius);
                    grd.addColorStop(0, radial.colorstop[0]);
                    grd.addColorStop(1, radial.colorstop[1]);
                    context.fillStyle = grd;
                    context.fill();
                }
                update();
            }catch(e){
                console.log(e)
            }
        }
        var eventful = {
            gradient:canvas,
            resize: function(){
                reload();
                react();
            },
            event: function(event){
                react(event);
            }
        }
        eventful.resize()
        return eventful;
    }
}())