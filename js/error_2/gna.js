(function show(){
    window.addEventListener("load",function(){
        window.graph.load();
        window.graph.survey.thread=Date.now;
        var results = window.graph.survey.survey(true);
        var poll = window.graph.survey.poll(results);
        window.graph.survey.chart(poll);
    })
})();