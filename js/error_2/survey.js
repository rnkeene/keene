(function survey(){
    this.graph.survey={
        population : 1000,
        sampleSize : 10,
        trials:0,
        _active:0,
        survey: function(game){
            var survey = [];
            this._active=0;
            if(!game){
                this._game=window.graph.agent.aether;
            }else{
                this._game=window.graph.agent.game;
            }
            for(this._active;this._active<this.sampleSize;this._active++){
                survey.push(this._game(this.thread));
            }
            return survey;
        },
        poll: function(survey){
            this._active=0;
            var results = [];
            for(this._active;this._active<this.sampleSize;this._active++){
                var thread = survey[this._active];                
                var txy = [thread[0][0](),thread[0][1]()];
                txy[0](),txy[1]();
                results.push(thread[1][1]());
            }
            return results;
        },
        chart: function(survey){
            //the range is the min/average/max
            var range = [5000,0,0];
            var start = Date.now();        
            for(var x=0;x<survey.length;x++){
                var apoll = survey[x];
                for(var y=0;y<apoll.length;y++){
                    apoll[y][1]=0-(apoll[y][1]-start);
                    range[1]+=apoll[y][0];
                    if(apoll[y][0]<range[0]){
                        range[0]=apoll[y][0];
                    }else if(apoll[y][0]>range[2]){
                        range[2]=apoll[y][0];
                    }                
                }
            }
            var scaleY=survey[0][0][1]+survey[survey.length-1][3][1];
            range[1]=parseInt(range[1]/(4*(survey.length-1)));
            var scaleX = range[2]/window.graph.theHeight();
            var startX = [0,0];
            var startY = [window.graph.theWidth(),window.graph.theHeight()];
            for(var x=0;x<survey.length;x++){
                var nextX = [startX[0]+(survey[x][0][0]/scaleY),startX[1]+(survey[x][2][0]/scaleY)];
                var nextY = [startY[0]-(survey[x][1][0]/scaleY),startY[1]-(survey[x][3][0]/scaleY)];
                window.graph.drawLine(startX,nextX);
                window.graph.drawLine(startY,nextY);
                window.graph.drawCircle(startX,nextX);
                window.graph.drawCircle(startY,nextY);
                startX=nextX;
                startY=nextY;
            }
        }
    }
})();