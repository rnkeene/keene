(function theGraph(){
    this.graph= {
        getCenter : function(){
            return [this.halfWidth(),this.halfHeight()];
        },
        load: function(){
            this.lattice= document.createElement("canvas");
            this.context=this.lattice.getContext("2d");
            this.theContainer();
            this.display.appendChild(this.lattice);
            this.midVertical();
            this.midHorizon();
            this.size();
            window.addEventListener("resize",this.resize);
        },
        midVertical: function (){
            this.drawLine([this.halfWidth(),0],[this.halfWidth(),this.theHeight()]);
        },
        midHorizon: function (){
            this.drawLine([0,this.halfHeight()],[this.theWidth(),this.halfHeight()]);
        },
        drawLine: function(from,to){
            this.context.moveTo(from[0],from[1]);
            this.context.lineTo(to[0],to[1]);
            this.context.stroke();
        },
        drawCircle: function(from,to){
            var xrad = (from[0]+to[0])/2;
            var yrad = (from[1]+to[1])/2;
            this.context.moveTo(from[0],from[1]);
            this.context.arc(xrad,yrad,window.graph.theHeight()/80,0,2*Math.PI);
            this.context.stroke();
        },
        resize: function(event){
            event.target.graph.size();
        },
        size: function(){
            this.lattice.width=this.theWidth();
            this.lattice.height=this.theHeight();
            document.body.style.width=this.theWidth();
            document.body.style.height=this.theHeight();
            this.display.style.width=this.theWidth();
            this.display.style.height=this.theHeight();
            this.display.style.border="1px solid black";
            this.midVertical();
            this.midHorizon();
        },
        theWidth: function(){
            return parseInt(window.innerWidth*.97);
        },
        theHeight: function(){
            return parseInt(window.innerHeight*.94);
        },
        halfWidth: function(){
            return parseInt(this.theWidth()/2);
        },
        halfHeight: function(){
            return parseInt(this.theHeight()/2);
        },
        theContainer: function(){
            this.display = document.createElement("div");
            document.body.appendChild(this.display);
        }
    }
})();