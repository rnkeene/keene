(function(){
    this.graph.agent = {
        aether: function(thread){
            var count=0;
            var result = [Date.now(),thread(),thread()];
            do{
                result[2]=thread();
                count++;
            }
            while(result[1]==result[2])
            result[0]-=Date.now();
            return [count,result];
        },
        game: function(thread){
            function flux(thread){
                var measure = 0;
                do{
                    var now=thread();
                    measure++;
                }while(now==thread())
                return measure;
            }
            var shape = [];
            var agent = thread;
            var form = function(){
                return shape;
            }
            var constant = function(){
                return shape.push([flux(agent),agent(),arguments[0]-1]);
            }
            var timed = function(){        
                var id=window.setInterval(constant,0,constant(1));
                return function(){
                    window.clearInterval(id);
                    constant(2);
                }
            }
            var delayed = function(){
                var id=window.setTimeout(constant,0,constant(3));
                return function(){
                    window.clearTimeout(id);
                    constant(4);
                }
            }
            return [[timed,delayed],[constant,form]];
        }
    }
})()