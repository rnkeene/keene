/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import com.itextpdf.text.pdf.PdfReader;
import com.keene.pdfparser.mepbm.derived.CharacterSkillGrowth;
import com.keene.pdfparser.mepbm.derived.service.CharacterSkillGrowthService;
import com.keene.pdfparser.mepbm.service.SVNService;
import com.keene.pdfparser.mepbm.service.TurnPDFService;
import java.util.List;
import java.util.Map;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author nate
 */
public class PDFParserTest extends TestCase
{

    private static final String svnDirectory = "/Users/Galen/Desktop/mepbm/mepbm/fourthAge";

    public PDFParserTest(String testName)
    {
        super(testName);

    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testPasser()
    {
        assertTrue(true);
    }

    public void xtestPDFParser()
    {
        Map<String, Game> games = SVNService.parseGames(SVNService.getPDFFilePaths(svnDirectory));
        Set<String> keys = games.keySet();
        for (String key : keys) {
            System.out.println(games.get(key));
        }
        assertTrue(true);
    }

    

    public void xtestPDFParserGet()
    {
        try {

            PdfReader g042n05t011 = new PdfReader("/home/rnkeene/Desktop/keene/mepbm/mavenProject/pdfparser/g042n05t011.pdf");
            Game game = new Game();

            TurnPDFService.parseTurnPDF(g042n05t011, game);

            //System.out.println(game);

            //System.out.println(SerializeUtil.serializeObject(game));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertTrue(true);
    }
}
