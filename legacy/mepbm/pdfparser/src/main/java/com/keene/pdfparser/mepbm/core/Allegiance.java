/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.core;

/**
 *
 * @author nate
 */
public enum Allegiance {
    Free_People ("Free People"),
    Neutral ("Neutral"),
    Dark_Servants ("Dark Servants");
    private final String allegiance;
    
    Allegiance(String allegiance){
        this.allegiance = allegiance;
    }
}
