/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.derived;

import com.keene.pdfparser.mepbm.PopulationCenter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nate
 */
public class PopulationCenterGrowth implements Serializable {

    private String nationName;
    private List<PopulationCenter> populationCenters;
    private int turn;    
    private long date;

    public PopulationCenterGrowth() {
        this.populationCenters = new ArrayList<PopulationCenter>();
    }

    public String getNationName()
    {
        return nationName;
    }

    public void setNationName(String nationName)
    {
        this.nationName = nationName;
    }

    public List<PopulationCenter> getPopulationCenters()
    {
        return this.populationCenters;
    }

    public void addPopulationCenter(PopulationCenter populationCenter)
    {
        this.populationCenters.add(populationCenter);
    }

    public void addPopulationCenters(List<PopulationCenter> populationCenters)
    {
        this.populationCenters.addAll(populationCenters);
    }

    public int getTurn()
    {
        return turn;
    }

    public void setTurn(int turn)
    {
        this.turn = turn;
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date)
    {
        this.date = date;
    }
    
    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("Population Center Growth:\n");
        toString.append("\tNation: ");
        toString.append(this.nationName);
        toString.append("\n");
        toString.append("\tTurn: ");
        toString.append(this.turn);
        toString.append("\n");
        for(PopulationCenter pc : populationCenters){
            toString.append(pc.toString());
        }
        return toString.toString();
    }

}
