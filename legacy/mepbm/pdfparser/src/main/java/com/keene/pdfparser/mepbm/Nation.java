/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nate
 */
public class Nation implements Serializable {

    private Player player;
    private int nationNumber;
    private String nationName;
    private Map<Integer,SNA> SNAMap;
    private Map<Integer,NationTurn> nationTurnMap;
    private List<VictoryCondition> victoryConditions;

    public Nation() {
        this.player = new Player();                
        SNAMap = new HashMap<Integer,SNA>();
        nationTurnMap = new HashMap<Integer,NationTurn>();
        victoryConditions = new ArrayList<VictoryCondition>();
    }   

    public void addSNA(SNA sna){
        SNAMap.put(sna.getNumber(), sna);
    }

    public SNA getSNA(int number){
        return SNAMap.get(number);
    }

    public void addVictoryCondition(VictoryCondition victoryCondition){
        victoryConditions.add(victoryCondition);
    }

    public List<VictoryCondition> getVictoryConditions(){
        return this.victoryConditions;
    }

    public void addNationTurn(NationTurn nationTurn){        
        nationTurnMap.put(nationTurn.getTurnNumber(), nationTurn);
    }

    public NationTurn getNationTurn(int turn){
        return nationTurnMap.get(turn);
    }
   
    public int getNationNumber() {
        return nationNumber;
    }

    public void setNationNumber(int nationNumber) {
        this.nationNumber = nationNumber;
    }

    public String getNationName() {
        return nationName;
    }

    public void setNationName(String nationName) {
        this.nationName = nationName;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("<h3>Nation INFO:</h3>");
        toString.append("<table>");
        
        toString.append(this.player.toString());
        toString.append("\t\tNation Name: ");
        toString.append(this.getNationName());
        toString.append("\n");
        toString.append("\t\tNation Number: ");
        toString.append(this.getNationNumber());
        toString.append("\n");        
        toString.append("\tVictory Conditions:\n");
        for(VictoryCondition vc : victoryConditions){
            toString.append("\t\t");
            toString.append(vc.getDescription());
            toString.append("\n");
        }
        Set<Integer> snakeys = SNAMap.keySet();
        for(Integer snakey : snakeys){
            toString.append(SNAMap.get(snakey).toString());
        }
        Set<Integer> ntkeys = nationTurnMap.keySet();
        for(Integer ntkey : ntkeys){
            toString.append(nationTurnMap.get(ntkey).toString());
        }
        return toString.toString();
    }

}
