/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class SNA implements Serializable {

    private int number;
    private String description;

    public SNA() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tSNA: ");
        toString.append(this.description);
        toString.append("\n");
        return toString.toString();
    }

}
