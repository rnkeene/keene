/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nate
 */
public class NationTurn implements Serializable {

    boolean won;
    boolean eliminated;
    private int turnNumber;
    private int taxRate;
    private int goldReserves;
    private int victoryPoints;
    private String season;
    private List<PopulationCenter> populationCenters;
    private List<Character> characters;
    private List<Relation> relations;
    private List<Army> armies;
    private List<Artifact> artifacts;
    private List<NationMessage> nationMessages;
    private List<Order> orders;
    private VictoryPoints victoryPointSheet;
    private long runOnDate;

    public NationTurn() {
        this.populationCenters = new ArrayList<PopulationCenter>();
        this.characters = new ArrayList<Character>();
        this.relations = new ArrayList<Relation>();
        this.armies = new ArrayList<Army>();
        this.artifacts = new ArrayList<Artifact>();
        this.nationMessages = new ArrayList<NationMessage>();
        this.orders = new ArrayList<Order>();
        this.won = false;
        this.eliminated = false;
    }

    public Character getCharacter(String characterName){
        for(Character character : characters){
            if(character.getName().equals(characterName)){
                //System.out.println("FOUND CHARACTER!");
                return character;
            }
        }
        //System.out.println("COULDN'T FIND CHARACTER: " + characterName );
        return null;
    }

    public void addRelation(Relation relation){
        this.relations.add(relation);
    }

    public List<Relation> getRelations(){
        return relations;
    }

    public void addOrder(Order order){
        this.orders.add(order);
    }

    public List<Order> getOrders(){
        return orders;
    }

    public void addArmy(Army army){
        this.armies.add(army);
    }

    public List<Army> getArmies(){
        return armies;
    }

    public void addArtifact(Artifact artifact){
        this.artifacts.add(artifact);
    }

    public List<Artifact> getArtifacts(){
        return this.artifacts;
    }

    public void addCharacter(Character character){
        characters.add(character);
    }

    public List<Character> getCharacters(){
        return characters;
    }

    public void addNationMessage(NationMessage nationMessage){
        nationMessages.add(nationMessage);
    }

    public List<NationMessage> getNationMessages(){
        return nationMessages;
    }

    public void addPopulationCenter(PopulationCenter populationCenter){
        populationCenters.add(populationCenter);
    }

    public List<PopulationCenter> getPopulationCenters(){
        return populationCenters;
    }

    public int getVictoryPoints() {
        return victoryPoints;
    }

    public void setVictoryPoints(int victoryPoints) {
        this.victoryPoints = victoryPoints;
    }

    public int getTurnNumber() {
        return turnNumber;
    }

    public void setTurnNumber(int turnNumber) {
        this.turnNumber = turnNumber;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public int getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(int taxRate) {
        this.taxRate = taxRate;
    }

    public int getGoldReserves() {
        return goldReserves;
    }

    public void setGoldReserves(int goldReserves) {
        this.goldReserves = goldReserves;
    }

    public boolean isEliminated()
    {
        return eliminated;
    }

    public void setEliminated(boolean eliminated)
    {
        this.eliminated = eliminated;
    }

    public boolean isWon()
    {
        return won;
    }

    public void setWon(boolean won)
    {
        this.won = won;
    }

    public VictoryPoints getVictoryPointSheet()
    {
        return victoryPointSheet;
    }

    public void setVictoryPointSheet(VictoryPoints victoryPointSheet)
    {
        this.victoryPointSheet = victoryPointSheet;
    }

    public long getRunOnDate()
    {
        return runOnDate;
    }

    public void setRunOnDate(long runOnDate)
    {
        this.runOnDate = runOnDate;
    }
    
    
    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tNation Turn:\n");
        toString.append("\t\tTurn Number: ");
        toString.append(this.turnNumber);
        toString.append("\n");
        toString.append("\t\tDate: ");
        toString.append(this.runOnDate);
        toString.append("\n");
        toString.append("\t\tEliminated? ");
        toString.append(this.eliminated);
        toString.append("\n");
        toString.append("\t\tWon? ");
        toString.append(this.won);
        toString.append("\n");
        toString.append("\t\tVictory Points: ");
        toString.append(this.victoryPoints);
        toString.append("\n");
        toString.append("\t\tSeason: ");
        toString.append(this.season);
        toString.append("\n");
        toString.append("\t\tArtifacts: ");
        toString.append("\n");
        for(Artifact artifact : artifacts){
            toString.append(artifact.toString());
        }
        toString.append("\t\tNation Messages: ");
        toString.append("\n");
        for(NationMessage message : nationMessages){
            toString.append(message.toString());
        }
        toString.append("\n");
        toString.append("\tRelations: ");
        toString.append("\n");
        for(Relation relation : relations){
            toString.append(relation.toString());
        }
        for(PopulationCenter pc : populationCenters){
            toString.append(pc.toString());
        }
        for(Army army : armies){
            toString.append(army.toString());
        }
        toString.append("\tOrders: ");
        toString.append("\n");
        for(Order order : orders){
            toString.append(order.toString());
        }
        if(victoryPointSheet!=null){
            toString.append("\tVICTORY POINTS");
            toString.append(victoryPointSheet);
        }
        return toString.toString();
    }

}
