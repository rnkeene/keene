/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class VictoryCondition implements Serializable {

    private String description;
    private boolean achieved;

    public VictoryCondition() {
    }

    public boolean isAchieved() {
        return achieved;
    }

    public void setAchieved(boolean achieved) {
        this.achieved = achieved;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("Victory Condition: ");
        toString.append(this.description);
        toString.append(" (completed? ");
        toString.append(this.achieved);
        toString.append(")");
        return toString.toString();
    }
}
