/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.service;

import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfReader;
import com.keene.pdfparser.mepbm.Army;
import com.keene.pdfparser.mepbm.Artifact;
import com.keene.pdfparser.mepbm.Game;
import com.keene.pdfparser.mepbm.Market;
import com.keene.pdfparser.mepbm.MarketPrices;
import com.keene.pdfparser.mepbm.Nation;
import com.keene.pdfparser.mepbm.NationMessage;
import com.keene.pdfparser.mepbm.NationTurn;
import com.keene.pdfparser.mepbm.Order;
import com.keene.pdfparser.mepbm.Player;
import com.keene.pdfparser.mepbm.PopulationCenter;
import com.keene.pdfparser.mepbm.Relation;
import com.keene.pdfparser.mepbm.ResourceDetails;
import com.keene.pdfparser.mepbm.SNA;
import com.keene.pdfparser.mepbm.Troops;
import com.keene.pdfparser.mepbm.VictoryCondition;
import com.keene.pdfparser.mepbm.Character;
import com.keene.pdfparser.mepbm.CharacterTurn;
import com.keene.pdfparser.mepbm.VictoryPoints;
import java.io.IOException;

/**
 *
 * @author nate
 */
public class TurnPDFService
{
    public static boolean process(Game game, String file)
    {
        if (game != null) {
            try {
                PdfReader turn = new PdfReader(file);
                //System.out.println("INITIALIZED " + file);
                TurnPDFService.parseTurnPDF(turn, game);
                //System.out.println("LOADED " + file);
                return true;
            } catch (InvalidPdfException e) {
                System.out.println("Error Parsing: " + file);
                //e.printStackTrace();
            } catch (Exception e) {
                System.out.println("Error Parsing: " + file);
                //e.printStackTrace();
            }
        }
        return false;
    }
    
    public static void parseTurnPDF(PdfReader reader, Game game) throws IOException
    {
        //System.out.println("== NEW PDF ==");
        game.initParser(reader);
        //System.out.println("\tParse Page One");
        TurnPDFService.parsePageOne(game);
        //System.out.println("\tParse Relations");
        TurnPDFService.parseRelations(game);
        //System.out.println("\tParse Population Centers");
        TurnPDFService.parsePopulationCentersNextPage(game);
        //System.out.println("\tParse Armies");
        TurnPDFService.parseArmies(game);
        //System.out.println("\tParse Market");
        TurnPDFService.parseMarket(game);
        //System.out.println("\tParse Miscellaneous");
        TurnPDFService.parseMiscellaneous(game);
        while ((game.getParser().getCurrentToken()) != null) {
            //System.out.println(game.getParser().getCurrentToken());
            if (game.getParser().getCurrentToken().indexOf("NATION MESSAGES") > -1) {
                //System.out.println("\tNATION MESSAGES");
                TurnPDFService.parseNationMessages(game);
            }
            if (game.getParser().getCurrentToken().indexOf("ORDERS GIVEN") > -1) {
                //System.out.println("\tORDERS GIVEN");
                TurnPDFService.parseOrdersGiven(game);
                //System.out.println("\tCHARACTERS");
                TurnPDFService.parseCharacters(game);
            }
            game.getParser().nextToken();
        }
        //System.out.println("\tDONE");
        game.setParser(null);
    }

    public static void parsePageOne(Game game) throws IOException
    {
        Nation nation = new Nation();
        NationTurn nationTurn = new NationTurn();
        Player player = new Player();

        String token = game.getParser().nextToken();
        //System.out.println("\t\tNEW PAGE 1");
        //System.out.println("1\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("2\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("3\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("4\t\t\t" + token);
        game.setType(token);
        token = game.getParser().nextToken();
        //System.out.println("5\t\t\t" + token);
        game.setNumber(PDFUtil.isolateRightColumnNumber(token, "GAME #"));
        token = game.getParser().nextToken();
        //System.out.println("6\t\t\t" + token);
        if (token.indexOf("Eliminated") > -1) {
            nationTurn.setEliminated(true);
            nation.setNationName(token.substring(token.indexOf("--- ") + "--- ".length(), token.indexOf(" Eliminated")));
            token = game.getParser().nextToken();
        } else if (token.indexOf("Won") > -1) {
            nationTurn.setWon(true);
            token = game.getParser().nextToken();
        } else {
            nation.setNationName(token);

            token = game.getParser().nextToken();
        }
        //System.out.println("7\t\t\t" + token);
        if (token.indexOf("Special Service Turn") > -1) {
            token = game.getParser().nextToken();
            //System.out.println("SS\t\t\t" + token);
        }
        if (token.indexOf("Victory points      :") > -1) {
            nationTurn.setVictoryPoints(PDFUtil.isolateRightColumnNumber(token, "Victory points      :"));
        } else {
            nationTurn.setVictoryPoints(PDFUtil.isolateRightColumnNumber(token, "Final Victory Points:"));
        }
        while (token.indexOf("Special Nation Abilities") < 0) {
            if (token.indexOf("To ") > 0) {
                VictoryCondition vc = new VictoryCondition();
                vc.setDescription(token);
                nation.addVictoryCondition(vc);
            }
            token = game.getParser().nextToken();
            //System.out.println("NEXT - \t\t\t" + token);
            if (token.indexOf("Top") > -1) {
                token = game.getParser().nextToken();
                //System.out.println("PTS - \t\t\t" + token);
                token = game.getParser().nextToken();
                //System.out.println("PTS - \t\t\t" + token);
                break;
            }
        }
        VictoryPoints vps = new VictoryPoints();
        if (token.indexOf(" Victory Points") > -1) {
            while (token.indexOf(" Victory Points") > -1) {
                if (token.indexOf("Army") > -1) {
                    token = game.getParser().nextToken();
                    //System.out.println("Army\t\t\t" + token);
                    vps.setArmy(Integer.parseInt(token));
                } else if (token.indexOf("Character") > -1) {
                    token = game.getParser().nextToken();
                    //System.out.println("Character\t\t\t" + token);
                    vps.setCharacter(Integer.parseInt(token));
                } else if (token.indexOf("Wealth") > -1) {
                    token = game.getParser().nextToken();
                    //System.out.println("Wealth\t\t\t" + token);
                    vps.setCharacter(Integer.parseInt(token));
                } else if (token.indexOf("Population Center") > -1) {
                    token = game.getParser().nextToken();
                    //System.out.println("Population Center\t\t\t" + token);
                    vps.setPopulationCenter(Integer.parseInt(token));
                } else if (token.indexOf("Individual") > -1) {
                    token = game.getParser().nextToken();
                    //System.out.println("Individual\t\t\t" + token);
                    vps.setIndividual(Integer.parseInt(token));
                }
                token = game.getParser().nextToken();
                //System.out.println("--\t\t\t" + token);
            }
        }
        if (token.indexOf("Special Nation Abilities") > -1) {
            //System.out.println("11\t\t\t" + token);
            token = game.getParser().nextToken();
            //System.out.println("--\t\t\t" + token);
            while (token.indexOf("Game #          :") < 0) {
                SNA sna = new SNA();
                sna.setNumber(Integer.parseInt(token.substring(token.indexOf("#") + 1, token.indexOf("#") + 3)));
                sna.setDescription(token.substring("          #XX ".length(), token.length()));
                nation.addSNA(sna);
                token = game.getParser().nextToken();
                //System.out.println("11--\t\t\t" + token);
            }
        }
        game.setNumber(PDFUtil.isolateRightColumnNumber(token, "Game #          :"));
        token = game.getParser().nextToken();
        //System.out.println("14\t\t\t" + token);
        nation.setNationNumber(PDFUtil.isolateRightColumnNumber(token, "Player #        :"));
        if(game.getNation(nation.getNationNumber())!=null){
            //NEED TO COPY OVER LOST DATA FROM FINAL TURN PDFS!!
            //System.out.println("LOADING MULTIPLE TURNS FOR NATION: " + nation.getNationName() + "!!");
            nation = game.getNation(nation.getNationNumber());
        }
        token = game.getParser().nextToken();
        //System.out.println("15\t\t\t" + token);
        game.getParser().setActiveTurn(PDFUtil.isolateRightColumnNumber(token, "Turn #          :"));
        nationTurn.setTurnNumber(game.getParser().getActiveTurn());
        token = game.getParser().nextToken();
        //System.out.println("16\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("17\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("18\t\t\t" + token);
        token = PDFUtil.removeIndent(token);
        player.setName(token.substring(0, token.lastIndexOf(" ")));
        player.setAccountNumber(PDFUtil.isolateRightColumnNumber(token, player.getName()));
        token = game.getParser().nextToken();
        //System.out.println("19\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("20\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("21\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("22\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("23\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("24\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("25\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("26\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("27\t\t\t" + token);
        nationTurn.setSeason(PDFUtil.isolateRightColumnString(token, "Season :"));
        token = game.getParser().nextToken();
        //System.out.println("28\t\t\t" + token);
        nation.setPlayer(player);
        nationTurn.setRunOnDate(game.getParser().getActivePDFDate().getTime());
        nation.addNationTurn(nationTurn);
        game.getParser().setActiveNation(nation);
        game.getParser().setActiveNationTurn(nationTurn);
        game.addNation(nation);
    }

    public static void parseRelations(Game game) throws IOException
    {
        boolean popCenters = false;
        //System.out.println("Adding Relations:");
        while (!popCenters) {
            String token = game.getParser().nextToken();
            Relation relation = new Relation();
            //System.out.println(token);
            if (token.indexOf("#") > -1) {
                if (token.indexOf("# ") > -1) {
                    relation.setNationNumber(Integer.parseInt(token.substring((token.indexOf("# ") + 2), token.indexOf(" ", 3))));
                } else if (token.indexOf("#") > -1) {
                    relation.setNationNumber(Integer.parseInt(token.substring((token.indexOf("#") + 1), token.indexOf(" ", 3))));
                }
                relation.setNationName(token.substring((token.indexOf(" ", 3) + 1), token.length()));
                token = game.getParser().nextToken();
                String relationship = token.substring(3, token.length());
                relation.setRelationship(relationship);
                //System.out.println("\t(" + relation.getNationNumber() + ") " + relation.getNationName() + " - " + relation.getRelationship());
                game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).addRelation(relation);
            }
            if (token.indexOf("POPULATION CENTERS") > -1) {
                popCenters = true;
            }
        }
        game.getParser().nextToken();
    }

    public static void parsePopulationCentersNextPage(Game game) throws IOException
    {
        String token = null;
        while ((token = game.getParser().getCurrentToken()) != null) {
            if (token.indexOf("ARMIES AND NAVIES") > -1) {
                break;
            }
            TurnPDFService.nextPopulationCenter(game);
        }
        game.getParser().nextToken();
    }

    public static PopulationCenter nextPopulationCenter(Game game)
    {
        PopulationCenter popCenter = new PopulationCenter();
        popCenter.setTurn(game.getParser().getActiveTurn());
        game.getParser().setActivePopCenter(popCenter);
        String token = game.getParser().getCurrentToken();
        popCenter.setName(token);
        //System.out.println("Pop Center: " + popCenter.getName());
        //System.out.println(token);
        token = game.getParser().nextToken();
        //System.out.println(token);
        if (token.indexOf("(") > -1) {
            popCenter.setCapital(true);
            token = game.getParser().nextToken();
        }
        if (token.indexOf("    Location : @") > -1) {
            popCenter.setHex(PDFUtil.parseLocation(token));
        }
        token = game.getParser().nextToken();
        //System.out.println(token);
        popCenter.setSize(token.substring("Size : ".length(), token.length()));
        //System.out.println("Size: " + popCenter.getSize());

        token = game.getParser().nextToken();
        //System.out.println(token);
        popCenter.setFortification(token.substring("Fortifications : ".length(), token.length()));
        //System.out.println("Fortifications: " + popCenter.getFortification());

        token = game.getParser().nextToken();
        //System.out.println(token);
        popCenter.setLoyalty(Integer.parseInt(token.substring("Loyalty : ".length(), token.length())));
        //System.out.println("Loyalty: " + popCenter.getLoyalty());

        token = game.getParser().nextToken();
        //System.out.println(token);
        popCenter.setDocks(token.substring("Docks : ".length(), token.length()));
        //System.out.println("Docks: " + popCenter.getDocks());

        token = game.getParser().nextToken();
        //System.out.println(token);
        if (token.substring("Hidden ? : ".length(), token.length()).equals("No")) {
            popCenter.setHidden(false);
        } else {
            popCenter.setHidden(true);
        }
        //System.out.println("Hidden? " + popCenter.isHidden());

        token = game.getParser().nextToken();
        //System.out.println(token);
        if (token.substring("Sieged ? : ".length(), token.length()).equals("No")) {
            popCenter.setSieged(false);
        } else {
            popCenter.setSieged(true);
        }
        //System.out.println("Sieged? " + popCenter.isSieged());

        token = game.getParser().nextToken();
        token = game.getParser().nextToken();
        token = token.substring("Expected production".length(), token.length());
        //System.out.println(token);
        boolean moreResources = true;
        int count = 0;
        while (moreResources) {
            while (token.indexOf(" ") == 0) {
                token = token.substring(1, token.length());
            }
            //System.out.println(token);
            int productionValue = 0;
            if (count < 7) {
                productionValue = Integer.parseInt(token.substring(0, token.indexOf(" ")));
            } else if (!(token.indexOf("-") > 0)) {
                productionValue = Integer.parseInt(token.substring(0, token.length()));
            }
            //System.out.println("Production Value: " + productionValue);
            switch (count) {
                case 0:
                    popCenter.getResourceDetails().setLeatherProduction(productionValue);
                    break;
                case 1:
                    popCenter.getResourceDetails().setBronzeProduction(productionValue);
                    break;
                case 2:
                    popCenter.getResourceDetails().setSteelProduction(productionValue);
                    break;
                case 3:
                    popCenter.getResourceDetails().setMithrilProduction(productionValue);
                    break;
                case 4:
                    popCenter.getResourceDetails().setFoodProduction(productionValue);
                    break;
                case 5:
                    popCenter.getResourceDetails().setTimberProduction(productionValue);
                    break;
                case 6:
                    popCenter.getResourceDetails().setMountsProduction(productionValue);
                    break;
                case 7:
                    if (token.indexOf("-") > -1) {
                        popCenter.getResourceDetails().setGoldProduction(0);
                    } else {
                        popCenter.getResourceDetails().setGoldProduction(productionValue);
                    }
                    moreResources = false;
                    break;
                default:
                    break;
            }
            if (moreResources) {
                token = token.substring(token.indexOf(" "), token.length());
                count++;
            }
        }
        token = game.getParser().nextToken();
        token = token.substring("Current stores".length(), token.length());
        moreResources = true;
        count = 0;
        while (moreResources) {
            while (token.indexOf(" ") == 0) {
                token = token.substring(1, token.length());
            }
            //System.out.println(token);
            int productionValue = 0;
            if (count < 7) {
                productionValue = Integer.parseInt(token.substring(0, token.indexOf(" ")));
            } else if (!(token.indexOf("-") > -1)) {
                productionValue = Integer.parseInt(token);
            }
            //System.out.println("Production Value: " + productionValue);
            switch (count) {
                case 0:
                    popCenter.getResourceDetails().setLeatherStore(productionValue);
                    break;
                case 1:
                    popCenter.getResourceDetails().setBronzeStore(productionValue);
                    break;
                case 2:
                    popCenter.getResourceDetails().setSteelStore(productionValue);
                    break;
                case 3:
                    popCenter.getResourceDetails().setMithrilStore(productionValue);
                    break;
                case 4:
                    popCenter.getResourceDetails().setFoodStore(productionValue);
                    break;
                case 5:
                    popCenter.getResourceDetails().setTimberStore(productionValue);
                    break;
                case 6:
                    popCenter.getResourceDetails().setMountsStore(productionValue);
                    break;
                case 7:
                    if (token.indexOf("-") > -1) {
                        popCenter.getResourceDetails().setGoldStore(0);
                    } else {
                        popCenter.getResourceDetails().setGoldStore(productionValue);
                    }
                    moreResources = false;
                    break;
            }
            if (moreResources) {
                token = token.substring(token.indexOf(" "), token.length());
                count++;
            }
        }
        token = game.getParser().nextToken();
        if (token.indexOf("Foreign characters reported") > -1) {
            while (!(token.indexOf(".") > -1)) {
                token = game.getParser().nextToken();
                popCenter.addForeignCharacter(PDFUtil.isolateRightColumnString(token, " "));
            }
            token = game.getParser().nextToken();
        }
        //System.out.println("---ARMY: " + token);
        while (token.indexOf("army") > -1 || token.indexOf("navy") > -1) {
            popCenter.addArmy(token);
            token = game.getParser().nextToken();
            if (token.indexOf(".") == 0) {
                token = game.getParser().nextToken();
            }
        }
        if (token.indexOf(".") > -1) {
            token = game.getParser().nextToken();
        }
        //System.out.println("--- BREAK: " + token);
        game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).addPopulationCenter(popCenter);
        return popCenter;
    }

    public static void parseArmies(Game game) throws IOException
    {
        String token = null;
        while ((token = game.getParser().getCurrentToken()) != null) {
            if (token.indexOf("COMPANY COMMANDERS :") > -1) {
                break;
            }
            TurnPDFService.parseArmy(game);
        }
    }

    public static void parseArmy(Game game) throws IOException
    {
        //System.out.println("PARSE Army!@");
        Army army = new Army();
        String token = game.getParser().getCurrentToken();
        //System.out.println(token);
        if (!("None").equals(token)) {
            army.setArmyCommander(PDFUtil.isolateRightColumnString(token, "Army Commander : "));
            //System.out.println(army.getArmyCommander());
            token = game.getParser().nextToken();
            //System.out.println(token);
            army.setHex(PDFUtil.parseLocation(token));
            //System.out.println(army.getHex().toString());
            token = game.getParser().nextToken();
            //System.out.println(token);
            String morale = token.substring(0, token.indexOf("  Warships :"));
            String warships = token.substring(token.indexOf("Warships :"), token.indexOf("  Transports  :"));
            String transports = token.substring(token.indexOf("Transports  :"), token.indexOf("  ("));
            String travelMode = token.substring(token.indexOf("Travel mode  :"), token.length());
            army.setMorale(PDFUtil.isolateRightColumnNumber(morale, "Army morale :"));
            army.setWarships(PDFUtil.isolateRightColumnNumber(warships, "Warships :"));
            army.setTransports(PDFUtil.isolateRightColumnNumber(transports, "Transports  :"));
            army.setTravelMode(PDFUtil.isolateRightColumnString(travelMode, "Travel mode  :"));
            //System.out.println(army);

            token = game.getParser().nextToken();
            token = game.getParser().nextToken();
            token = game.getParser().nextToken();

            while (!(token.indexOf("Baggage Train") > -1)) {
                //System.out.println(token);
                TurnPDFService.parseTroops(game, army);
                token = game.getParser().getCurrentToken();
            }
            //System.out.println(token);
            TurnPDFService.parseBaggageTrain(game, army);
            //System.out.println(army);
            game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).addArmy(army);
        } else {
            token = game.getParser().nextToken();
        }
    }

    public static void parseTroops(Game game, Army army) throws IOException
    {
        //System.out.println("PARSE TROOPS!@");
        Troops troops = new Troops();
        String token = game.getParser().getCurrentToken();
        //System.out.println(token);
        troops.setDescription(token);
        token = game.getParser().nextToken();
        //System.out.println(token);
        troops.setTraining(PDFUtil.isolateRightColumnNumber(token, " "));
        token = game.getParser().nextToken();
        //System.out.println(token);
        troops.setWeapons(PDFUtil.isolateRightColumnNumber(token, " "));
        token = game.getParser().nextToken();
        //System.out.println(token);
        troops.setArmor(PDFUtil.isolateRightColumnNumber(token, " "));
        token = game.getParser().nextToken();
        //System.out.println(token);
        troops.setNumbers(PDFUtil.isolateRightColumnNumber(token, " "));
        token = game.getParser().nextToken();
        //System.out.println(token);
        troops.setTroopType(token);
        //System.out.println(troops);
        army.addTroops(troops);
        game.getParser().nextToken();
    }

    public static void parseBaggageTrain(Game game, Army army) throws IOException
    {
        String token = game.getParser().nextToken();
        //System.out.println(token);

        while (!"Food".equals(token)) {
            token = game.getParser().nextToken();
            //System.out.println(token);
        }
        token = game.getParser().nextToken();
        army.setFood(PDFUtil.isolateRightColumnNumber(token, " "));
        //System.out.println(token);

        token = game.getParser().nextToken();
        //System.out.println(token);

        if (token.indexOf("Low Supplies") > -1 || token.indexOf("Out of Food") > -1) {
            token = game.getParser().nextToken();
            //System.out.println(token);
        }

        if (token.indexOf("War machines") > -1) {
            token = game.getParser().nextToken();
            army.setWarMachines(PDFUtil.isolateRightColumnNumber(token, " "));
            token = game.getParser().nextToken();
            //System.out.println(token);
        }

        if (token.indexOf("traveling with army") > -1) {
            while (!(token.indexOf(".") > -1)) {
                PDFUtil.parseCharactersWithArmy(token, army);
                token = game.getParser().nextToken();
            }
            token = game.getParser().nextToken();
        }
        StringBuilder armyDetails = new StringBuilder();
        while (token.indexOf("army bearing") > -1 || token.indexOf("navy bearing") > -1) {
            armyDetails.append(token);
            token = game.getParser().nextToken();
        }
        if (token.indexOf("The") == 0) {
            while (!(token.indexOf("Army Commander") > -1) && !(token.indexOf("COMPANY COMMANDERS") > -1)) {
                //System.out.println(token);
                armyDetails.append(token);
                token = game.getParser().nextToken();
            }
        }
        army.setArmyDetails(armyDetails.toString());
    }

    public static void parseMarket(Game game) throws IOException
    {
        String token = null;
        while ((token = game.getParser().getCurrentToken()) != null) {
            if (token.indexOf("MISCELLANEOUS") > -1) {
                break;
            }
            if (token.indexOf("Market units available") > -1) {
                ResourceDetails details = new ResourceDetails();
                token = game.getParser().nextToken();
                details.setLeatherStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                details.setBronzeStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                details.setSteelStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                details.setMithrilStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                details.setFoodStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                details.setTimberStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                details.setMountsStore(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                MarketPrices prices = new MarketPrices();
                token = game.getParser().nextToken();
                prices.setLeatherBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setBronzeBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setSteelBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setMithrilBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setFoodBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setTimberBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setMountsBuyPrice(PDFUtil.isolateRightColumnNumber(token, " "));

                token = game.getParser().nextToken();
                token = game.getParser().nextToken();
                prices.setLeatherSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setBronzeSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setSteelSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setMithrilSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setFoodSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setTimberSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));
                token = game.getParser().nextToken();
                prices.setMountsSellPrice(PDFUtil.isolateRightColumnNumber(token, " "));

                Market market = new Market();
                market.setTurnNumber(game.getParser().getActiveTurn());
                market.setMarketPrices(prices);
                market.setUnitsAvailable(details);

                game.addMarketDetails(market);
            }
            game.getParser().nextToken();
        }
        //System.out.println(game.getMarketDetails(game.getParser().getActiveTurn()));
    }

    public static void parseMiscellaneous(Game game) throws IOException
    {
        String token = null;
        while ((token = game.getParser().getCurrentToken()) != null) {
            if (token.indexOf("Current Tax rate") > -1) {
                token = game.getParser().nextToken();
                game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).setTaxRate(PDFUtil.isolateRightColumnNumber(token, " "));
            }
            if (token.indexOf("You possess the following artifacts:") > -1) {
                token = game.getParser().nextToken();
                //System.out.println(token);
                if ("None".equals(token)) {
                    break;
                } else {
                    token = game.getParser().nextToken();
                    //System.out.println(token);
                    token = game.getParser().nextToken();
                    //System.out.println(token);
                    token = game.getParser().nextToken();
                    //System.out.println(token);
                    token = game.getParser().nextToken();
                    //System.out.println(token);
                    token = game.getParser().nextToken();
                    //System.out.println(token);
                    token = game.getParser().nextToken();
                    while (!(game.getParser().getCurrentToken().indexOf("You have hidden the following additional artifacts:") > -1)) {
                        TurnPDFService.parseArtifact(game);
                    }
                }
                break;
            }
            //System.out.println(game.getParser().getCurrentToken());
            game.getParser().nextToken();
        }
        //System.out.println(game.getMarketDetails(game.getParser().getActiveTurn()));
    }

    public static void parseArtifact(Game game) throws IOException
    {
        Artifact artifact = new Artifact();
        String token = null;
        token = game.getParser().getCurrentToken();
        //System.out.println(token);
        artifact.setName(token);
        token = game.getParser().nextToken();
        //System.out.println(token);
        artifact.setType(PDFUtil.isolateRightColumnString(token, " "));
        token = game.getParser().nextToken();
        //System.out.println(token);
        artifact.setNumber(PDFUtil.isolateRightColumnNumber(token, ""));
        token = game.getParser().nextToken();
        //System.out.println(token);
        artifact.setKnownPowers(token);
        token = game.getParser().nextToken();
        //System.out.println(token);
        artifact.setKnownPowers(token);
        token = game.getParser().nextToken();
        //System.out.println(token);
        PDFUtil.isolateArtifactSkill(token, artifact);
        //System.out.println(artifact);
        token = game.getParser().nextToken();
        game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).addArtifact(artifact);
    }

    public static void parseNationMessages(Game game) throws IOException
    {
        while ((game.getParser().getCurrentToken()) != null) {
            String token = game.getParser().nextToken();
            if (game.getParser().getCurrentToken().indexOf("ENCOUNTER MESSAGES") > -1) {
                break;
            }
            NationMessage nm = new NationMessage();
            nm.setMessage(token);
            game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).addNationMessage(nm);
        }
    }

    public static void parseOrdersGiven(Game game) throws IOException
    {
        String token = game.getParser().nextToken();
        //System.out.println("\t\t\t" + token);
        if (token.indexOf("Character") < 0) {
            //System.out.println("NO ORDERS: \t\t\t" + token);
            game.getParser().nextToken();
            return;
        }
        token = game.getParser().nextToken();
        //System.out.println("\t\t\t" + token);
        token = game.getParser().nextToken();
        //System.out.println("\t\t\t" + token);
        if (token.length() < 2) {
            //System.out.println("NO ORDERS: \t\t\t" + token);
            return;
        }
        Order previousOrder = new Order();
        while ((game.getParser().getCurrentToken()) != null) {
            if (game.getParser().getCurrentToken().length() == 1) {
                //System.out.println("Possible Skip-\t\t\t" + token);
                char tokenP = token.charAt(0);
                if (previousOrder.getCharacter() != null) {
                    if (tokenP >= 'A' && tokenP <= previousOrder.getCharacter().charAt(0)) {
                        //System.out.println("Last Order for: " + previousOrder.getCharacter());
                        //System.out.println("EVALUATE: " + token + " vs " + previousOrder.getCharacter().charAt(0));
                        if (previousOrder.getCharacter().indexOf("Magdal") > -1 && tokenP == 'B') {
                            //System.out.println("  RANDOM EXCEPTION Magdal B");
                        } else if (previousOrder.getCharacter().indexOf("Solomon") > -1 && tokenP == 'F') {
                            //System.out.println("  RANDOM EXCEPTION Solomon F");
                        } else {
                            //System.out.println("BREAK!!");
                            break;
                        }
                        //System.out.println("STAY!!");
                    }
                    if (tokenP >= previousOrder.getCharacter().charAt(0) && tokenP <= 'Z') {
                        //System.out.println("UNCERTAIN ACTION!!");
                    }
                }
            } else if (token.length() > 35) {
                char tokenP = token.charAt(0);
                if (tokenP >= 'A' && tokenP <= 'Z') {
                    //System.out.println("ORDER: \t\t\t" + token);
                    Order order = PDFUtil.parseOrder(token);
                    game.getParser().getActiveNation().getNationTurn(game.getParser().getActiveTurn()).addOrder(order);
                    //System.out.println(order);
                    previousOrder = order;
                }
            }
            token = game.getParser().nextToken();
        }
    }

    public static void parseCharacters(Game game) throws IOException
    {
        NationTurn nationTurn = game.getParser().getActiveNationTurn();
        //System.out.println("\tPARSE CHARACTERS");
        while ((game.getParser().getCurrentToken()) != null) {
            Character character = new Character();
            String token = game.getParser().getCurrentToken();
            if (token.length() == 1) {
                char tokenP = token.charAt(0);
                if (tokenP >= 'A' && tokenP <= 'Z') {
                    character.setName(PDFUtil.parseCharacterName(token, game));
                } else {
                    if ((tokenP >= '/' && tokenP <= '9')) {
                        return;
                    }
                }
            }
            if (character.getName() != null) {
                if(nationTurn.getCharacter(character.getName())!=null){
                    character = nationTurn.getCharacter(character.getName());
                }else{
                    nationTurn.addCharacter(character);
                }
                //System.out.println("\t\t" + token);
                //System.out.println("Character: " + character.getName());
                CharacterTurn ct = new CharacterTurn();
                ct.setTurnNumber(game.getParser().getActiveTurn());
                token = game.getParser().nextToken();
                //System.out.println("Command: " + token);
                ct.setCommandRank(PDFUtil.isolateRightColumnNumber(token, "Command"));
                token = game.getParser().nextToken();
                //System.out.println("Agent: " + token);
                ct.setAgentRank(PDFUtil.isolateRightColumnNumber(token, "Agent"));
                token = game.getParser().nextToken();
                //System.out.println("Emissary: " + token);
                ct.setEmissaryRank(PDFUtil.isolateRightColumnNumber(token, "Emissary"));
                token = game.getParser().nextToken();
                //System.out.println("Mage: " + token);
                ct.setMageRank(PDFUtil.isolateRightColumnNumber(token, "Mage"));
                token = game.getParser().nextToken();
                //System.out.println("Health: " + token);
                ct.setHealth(PDFUtil.isolateRightColumnNumber(token, "Health"));
                token = game.getParser().nextToken();
                //System.out.println("Stealth: " + token);
                ct.setStealth(PDFUtil.isolateRightColumnNumber(token, "Stealth"));
                token = game.getParser().nextToken();
                //System.out.println("Challenge: " + token);
                ct.setChallengeRank(PDFUtil.isolateRightColumnNumber(token, "Challenge"));
                token = game.getParser().nextToken();
                //System.out.println("Token: " + token);
                PDFUtil.parseCharacterArtifacts(game.getParser().nextToken(), ct, game);
                PDFUtil.parseCharacterSpells(game.getParser().nextToken(), ct, game);
                PDFUtil.parseCharacterOrderDetails(game.getParser().getCurrentToken(), ct, game);
                character.addCharacterTurn(ct);
                //System.out.println(character);                
            }else{
                //System.out.println("NO MORE CHARACTERS!");
                return;
            }
        }
    }
}
