/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Order implements Serializable {

    private String character;
    private int orderNumber;
    private String orderCode;
    private String additionalInfo;

    public Order() {
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tORDER:\n");
        toString.append("\t\t");
        toString.append(this.character);
        toString.append("\n");
        toString.append("\t\t");
        toString.append(this.orderNumber);
        toString.append("\n");
        toString.append("\t\t");
        toString.append(this.orderCode);        
        toString.append("\n");
        toString.append("\t\t");
        toString.append(this.additionalInfo);
        toString.append("\n");
        return toString.toString();
    }

}
