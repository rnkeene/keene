/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.service;

import com.keene.pdfparser.mepbm.Game;
import com.organick.persist.Persist;

/**
 *
 * @author nate
 */
public class PersistenceService
{
    public static boolean saveGame(Game game) throws Exception
    {
        Persist.save(game, game.getType() + game.getNumber());
        return true;
    }

    public static Game loadGame(String gameId) throws Exception
    {
        return Persist.get(gameId, Game.class.getName());
    }
}
