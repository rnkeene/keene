/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nate
 */
public class Character implements Serializable {

    private String name;
    private Map<Integer, CharacterTurn> characterTurnMap;

    public Character() {
        characterTurnMap = new HashMap<Integer, CharacterTurn>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharacterId() {
        return name.toLowerCase().substring(0, 4);
    }

    public void addCharacterTurn(CharacterTurn characterTurn) {
        characterTurnMap.put(characterTurn.getTurnNumber(), characterTurn);
    }

    public CharacterTurn getCharacterTurn(int turnNumber) {
        return characterTurnMap.get(turnNumber);
    }
    
    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tCharacter:\n");
        toString.append("\tName:");
        toString.append(this.name);
        toString.append("\n");
        Set<Integer> ntkeys = characterTurnMap.keySet();
        for(Integer ntkey : ntkeys){
            toString.append(characterTurnMap.get(ntkey).toString());
        }
        return toString.toString();
    }

}
