/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Hex implements Serializable {

    private String terrain;
    private int hex;
    private String climate;

    public Hex() {
    }

    public int getHex() {
        return hex;
    }

    public void setHex(int hex) {
        this.hex = hex;
    }

    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tHex Info:\n");
        toString.append("\t\tTerrain: ");
        toString.append(this.terrain);
        toString.append("\n");
        toString.append("\t\tHex: ");
        toString.append(this.hex);
        toString.append("\n");
        toString.append("\t\tClimate: ");
        toString.append(this.climate);
        toString.append("\n");

        return toString.toString();
    }
}
