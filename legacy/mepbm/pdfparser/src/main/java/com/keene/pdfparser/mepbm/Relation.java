/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Relation implements Serializable {

    private int nationNumber;
    private String nationName;
    private String relationship;

    public Relation() {
    }

    public String getNationName() {
        return nationName;
    }

    public void setNationName(String nationName) {
        this.nationName = nationName;
    }

    public int getNationNumber() {
        return nationNumber;
    }

    public void setNationNumber(int nationNumber) {
        this.nationNumber = nationNumber;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t\t");
        toString.append(this.getNationName());      
        toString.append(" (");
        toString.append(this.getNationNumber());
        toString.append(") - ");
        toString.append(this.getRelationship());
        toString.append("\n");
        return toString.toString();
    }
}
