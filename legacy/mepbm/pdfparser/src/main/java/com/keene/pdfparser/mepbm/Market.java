/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Market implements Serializable {

    private int turnNumber;
    private MarketPrices marketPrices;
    private ResourceDetails unitsAvailable;

    public Market() {
    }

    public MarketPrices getMarketPrices() {
        return marketPrices;
    }

    public void setMarketPrices(MarketPrices marketPrices) {
        this.marketPrices = marketPrices;
    }

    public ResourceDetails getUnitsAvailable() {
        return unitsAvailable;
    }

    public void setUnitsAvailable(ResourceDetails unitsAvailable) {
        this.unitsAvailable = unitsAvailable;
    }

    public int getTurnNumber() {
        return turnNumber;
    }

    public void setTurnNumber(int turnNumber) {
        this.turnNumber = turnNumber;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("MARKET INFO:\n");
        toString.append("\tTurn Number:");
        toString.append(this.getTurnNumber());
        toString.append("\n");
        toString.append(this.getMarketPrices());
        toString.append("\n");
        toString.append(this.getUnitsAvailable());
        return toString.toString();
    }

    public String toHTML(){
        StringBuilder toString = new StringBuilder();
        toString.append("<h3>MARKET INFO:</h3>");
        toString.append("<table>");

        toString.append("<tr>");
        toString.append("<td style=\"text-align:right;\">");
        toString.append("<b>Turn Number: </b>");
        toString.append("</td>");
        toString.append("<td>");
        toString.append(this.getTurnNumber());
        toString.append("</td>");
        toString.append("</tr>");

        toString.append("<tr>");
        toString.append("<td style=\"text-align:right;\">");
        toString.append("<b>Market Prices: </b>");
        toString.append("</td>");
        toString.append("<td>");
        toString.append(this.getMarketPrices());
        toString.append("</td>");
        toString.append("</tr>");

        toString.append("<tr>");
        toString.append("<td style=\"text-align:right;\">");
        toString.append("<b>Units Available: </b>");
        toString.append("</td>");
        toString.append("<td>");
        toString.append(this.getUnitsAvailable());
        toString.append("</td>");
        toString.append("</tr>");

        toString.append("</table>");
                
        return toString.toString();
    }

}
