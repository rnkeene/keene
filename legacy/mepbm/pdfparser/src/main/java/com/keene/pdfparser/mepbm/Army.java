/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nate
 */
public class Army implements Serializable {

    private String armyCommander;
    private Hex hex;
    private int morale;
    private int food;
    private int warships;
    private int transports;
    private int warMachines;
    private String travelMode;
    private String armyDetails;
    private List<Troops> troops;
    private List<String> charactersInArmy;

    public Army() {
        this.troops = new ArrayList<Troops>();
        this.charactersInArmy = new ArrayList<String>();
    }

    public String getArmyCommander() {
        return armyCommander;
    }

    public void setArmyCommander(String armyCommander) {
        this.armyCommander = armyCommander;
    }

    public Hex getHex() {
        return hex;
    }

    public void setHex(Hex hex) {
        this.hex = hex;
    }

    public int getMorale() {
        return morale;
    }

    public void setMorale(int morale) {
        this.morale = morale;
    }

    public int getTransports() {
        return transports;
    }

    public void setTransports(int transports) {
        this.transports = transports;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    public int getWarships() {
        return warships;
    }

    public void setWarships(int warships) {
        this.warships = warships;
    }

    public void addTroops(Troops troops) {
        this.troops.add(troops);
    }

    public List<Troops> getTroops() {
        return this.troops;
    }

    public void addCharacterInArmy(String name) {
        this.charactersInArmy.add(name);
    }

    public List<String> getCharactersInArmy() {
        return this.charactersInArmy;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public int getWarMachines() {
        return warMachines;
    }

    public void setWarMachines(int warMachines) {
        this.warMachines = warMachines;
    }

    public String getArmyDetails() {
        return armyDetails;
    }

    public void setArmyDetails(String armyDetails) {
        this.armyDetails = armyDetails;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tARMY:");
        toString.append("\r\t\tArmy Commander: ");
        toString.append(this.armyCommander);
        toString.append("\n");
        toString.append("\t\tMorale: ");
        toString.append(this.morale);
        toString.append("\n");
        toString.append("\t\tFood: ");
        toString.append(this.food);
        toString.append("\n");
        toString.append("\t\tWar Machines: ");
        toString.append(this.warMachines);
        toString.append("\n");
        toString.append("\t\tTransports: ");
        toString.append(this.transports);
        toString.append("\n");
        toString.append("\t\tTravel Mode: ");
        toString.append(this.travelMode);
        toString.append("\n");
        toString.append("\t\tWarships: ");
        toString.append(this.warships);
        toString.append("\n");
        toString.append(this.hex);
        toString.append("\n");
        toString.append("\t\tMisc Details: ");
        toString.append(this.armyDetails);
        toString.append("\n");
        for (Troops troop : troops) {
            toString.append(troop.toString());
        }
        toString.append("\t\tCharacters in Army: ");
        for (String character : charactersInArmy) {
            toString.append("\t\t\t");
            toString.append(character.toString());
            toString.append("\n");
        }
        return toString.toString();
    }
}
