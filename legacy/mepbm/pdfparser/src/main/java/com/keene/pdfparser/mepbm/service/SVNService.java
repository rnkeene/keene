/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.service;

import com.keene.pdfparser.mepbm.Game;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author nate
 */
public class SVNService
{

    public static Map<String, Game> parseGames(List<String> files)
    {
        Map<String, Game> games = new HashMap<String, Game>();
        try {
            int success = 0;
            int failure = 0;
            for (String file : files) {
                //System.out.println(file);
                Game game = null;
                if (file.indexOf("game48") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("game48")) {
                        game = games.get("game48");
                    } else {
                        games.put("game48", new Game());
                        game = games.get("game48");
                    }
                } else if (file.indexOf("grudge44") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("grudge44")) {
                        game = games.get("grudge44");
                    } else {
                        games.put("grudge44", new Game());
                        game = games.get("grudge44");
                    }
                } else if (file.indexOf("game43") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("game43")) {
                        game = games.get("game43");
                    } else {
                        games.put("game43", new Game());
                        game = games.get("game43");
                    }
                } else if (file.indexOf("game45") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("game45")) {
                        game = games.get("game45");
                    } else {
                        games.put("game45", new Game());
                        game = games.get("game45");
                    }
                } else if (file.indexOf("game49") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("game49")) {
                        game = games.get("game49");
                    } else {
                        games.put("game49", new Game());
                        game = games.get("game49");
                    }
                } else if (file.indexOf("game42") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("game42")) {
                        game = games.get("game42");
                    } else {
                        games.put("game42", new Game());
                        game = games.get("game42");
                    }
                } else if (file.indexOf("game41") > -1) {
                    //System.out.println(file);
                    if (games.containsKey("game41")) {
                        game = games.get("game41");
                    } else {
                        games.put("game41", new Game());
                        game = games.get("game41");
                    }
                } 
                if (TurnPDFService.process(game, file)) {
                    success++;
                } else {
                    failure++;
                }
            }
            int total = (success + failure);
            double percentSuccessful = ((double) success / (double) total) * 100;
            double percentFailure = ((double) failure / (double) total) * 100;

            System.out.println("REPORT SUMMARY:");
            System.out.println("\tTOTAL PDFs: " + total);
            System.out.println("\tTotal Success: " + success);
            System.out.println("\tTotal Failure: " + failure);
            System.out.println("\t%Successful: " + percentSuccessful);
            System.out.println("\t%Failure: " + percentFailure);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return games;
    }

    

    public static List<String> getPDFFilePaths(String svnDirectory)
    {
        File basedir = new File(svnDirectory);
        List<String> gamePDFs = new ArrayList<String>();
        String[] games = basedir.list();
        if (games == null) {
            // Either dir does not exist or is not a directory
        } else {
            for (int i = 0; i < games.length; i++) {
                // Get filename of file or directory
                String directory = games[i];
                if (directory.indexOf("game") > -1 || directory.indexOf("grudge") > -1) {
                    File subdir = new File(svnDirectory + "/" + directory);
                    String[] turns = subdir.list();
                    if (turns == null) {
                        // Either dir does not exist or is not a directory
                    } else {
                        for (int x = 0; x < turns.length; x++) {
                            String turn = turns[x];
                            if (turn.toLowerCase().indexOf("turn") > -1) {
                                turn = svnDirectory + "/" + directory + "/" + turn;
                                File pdfdir = new File(turn);
                                String[] pdfs = pdfdir.list();
                                if (pdfs == null) {
                                    // Either dir does not exist or is not a directory
                                } else {
                                    for (int y = 0; y < pdfs.length; y++) {
                                        String pdf = pdfs[y];
                                        if (pdf.indexOf(".pdf") > -1) {
                                            String fullPath = turn + "/" + pdf;
                                            gamePDFs.add(fullPath);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return gamePDFs;
    }
}
