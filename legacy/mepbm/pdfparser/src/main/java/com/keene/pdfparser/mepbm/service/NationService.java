/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.service;

import com.keene.pdfparser.mepbm.Game;
import com.keene.pdfparser.mepbm.Nation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author rnkeene
 */
public class NationService {

    public static Nation getNation(String nationName, Game game) {
        {
            Nation nation = new Nation();
            nation.setNationName("ERROR NATION");
            Set<Integer> keys = game.getNationKeys();
            for (Integer key : keys) {
                if (game.getNation(key).getNationName().equals(nationName)) {
                    nation = game.getNation(key);
                }
            }
            return nation;
        }
    }

    public static Map<String, Nation> getNations(Game game) {
        {
            Map<String, Nation> nations = new HashMap<String, Nation>();
            Set<Integer> keys = game.getNationKeys();
            for (Integer key : keys) {
                Nation nation = game.getNation(key);
                if (!nations.containsKey(nation.getNationName())) {
                    nations.put(nation.getNationName(), nation);
                }
            }
            return nations;
        }
    }
}
