/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.derived.service;

import com.keene.pdfparser.mepbm.*;
import com.keene.pdfparser.mepbm.derived.PopulationCenterGrowth;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nate
 */
public class PopulationCenterGrowthService {

    public static List<PopulationCenterGrowth> findPopulationCenterGrowth(Map<String, Nation> nations, int startTurn, int endTurn) {
        List<PopulationCenterGrowth> growth = new ArrayList<PopulationCenterGrowth>();
        Set<String> keys = nations.keySet();
        for (String key : keys) {
            Nation nation = nations.get(key);
            int count = startTurn;
            //System.out.println("NATION: " + nation.getNationName());
            while (count <= endTurn) {
                NationTurn turn = nation.getNationTurn(count);
                if (turn != null) {
                    List<PopulationCenter> popCenters = turn.getPopulationCenters();
                    //System.out.println("\tTURN: " + count);
                    PopulationCenterGrowth grow = new PopulationCenterGrowth();
                    grow.setDate(turn.getRunOnDate());
                    grow.addPopulationCenters(popCenters);                                        
                    grow.setNationName(nation.getNationName());
                    //System.out.println("\tTURN TOTAL: " + skillTotal);                    
                    grow.setTurn(count);
                    //System.out.println(grow);
                    growth.add(grow);
                } else {
                    //System.out.println("\tNO TURN " + count);
                }
                count++;
            }
        }
        return growth;
    }
}
