/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Artifact implements Serializable {

    private String name;
    private String type;
    private int number;
    private int mageSkill;
    private int agentSkill;
    private int emissarySkill;
    private int commandSkill;
    private int stealth;
    private String alignment;
    private String knownPowers;
    private int combatDamage;

    public Artifact() {
    }

    public String getAlignment() {
        return alignment;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public String getKnownPowers() {
        return knownPowers;
    }

    public void setKnownPowers(String knownPowers) {
        this.knownPowers = knownPowers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getAgentSkill() {
        return agentSkill;
    }

    public void setAgentSkill(int agentSkill) {
        this.agentSkill = agentSkill;
    }

    public int getCombatDamage() {
        return combatDamage;
    }

    public void setCombatDamage(int combatDamage) {
        this.combatDamage = combatDamage;
    }

    public int getCommandSkill() {
        return commandSkill;
    }

    public void setCommandSkill(int commandSkill) {
        this.commandSkill = commandSkill;
    }

    public int getEmissarySkill() {
        return emissarySkill;
    }

    public void setEmissarySkill(int emissarySkill) {
        this.emissarySkill = emissarySkill;
    }

    public int getMageSkill() {
        return mageSkill;
    }

    public void setMageSkill(int mageSkill) {
        this.mageSkill = mageSkill;
    }

    public int getStealth() {
        return stealth;
    }

    public void setStealth(int stealth) {
        this.stealth = stealth;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t\tNEW ARTIFACT:\n");
        toString.append("\t\t\tName:");
        toString.append(this.name);
        toString.append("\n");
        toString.append("\t\t\tNumber: ");
        toString.append(this.number);
        toString.append("\n");
        toString.append("\t\t\tAlignment: ");
        toString.append(this.alignment);
        toString.append("\n");
        toString.append("\t\t\tKnown Powers: ");
        toString.append(this.knownPowers);
        toString.append("\n");
        toString.append("\t\t\tCombat Damage: ");
        toString.append(this.combatDamage);
        toString.append("\n");
        toString.append("\t\t\tAgent Skill: ");
        toString.append(this.agentSkill);
        toString.append("\n");
        toString.append("\t\t\tMage Skill: ");
        toString.append(this.mageSkill);
        toString.append("\n");
        toString.append("\t\t\tCommand Skill: ");
        toString.append(this.commandSkill);
        toString.append("\n");
        toString.append("\t\t\tEmissary Skill: ");
        toString.append(this.emissarySkill);
        toString.append("\n");
        toString.append("\t\t\tStealth: ");
        toString.append(this.stealth);
        toString.append("\n");

        return toString.toString();
    }

}
