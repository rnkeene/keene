/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.service;

import com.keene.pdfparser.mepbm.Army;
import com.keene.pdfparser.mepbm.Artifact;
import com.keene.pdfparser.mepbm.CharacterTurn;
import com.keene.pdfparser.mepbm.Game;
import com.keene.pdfparser.mepbm.Hex;
import com.keene.pdfparser.mepbm.Order;

/**
 *
 * @author nate
 */
public class PDFUtil
{

    public static int parseGameNumber(String token)
    {
        return PDFUtil.isolateRightColumnNumber(token, "Game #          :");
    }

    public static int parseTurnNumber(String token)
    {
        return PDFUtil.isolateRightColumnNumber(token, "Turn #          :");
    }

    public static int parsePlayerNumber(String token)
    {
        return PDFUtil.isolateRightColumnNumber(token, "Player #        :");
    }

    public static int isolateRightColumnNumber(String token, String delete)
    {
        //System.out.println("isolateRightColumnNumber");
        //System.out.println(token);
        token = token.replaceAll(delete, "");
        //System.out.println(token);
        token = token.replaceAll(" ", "");
        //System.out.println(token);
        if (token.indexOf("(") > -1) {
            token = token.substring(0, token.indexOf("("));
        }
        //System.out.println(token);
        if (token.indexOf("+") > -1) {
            token = token.substring(0, token.indexOf("+"));
        }
        //System.out.println(token);
        char[] nbspCheck = token.substring(token.length() - 1).toCharArray();
        if (((int) nbspCheck[0]) == 160) {
            token = token.substring(0, token.length() - 1);
        }
        return Integer.parseInt(token);
    }

    public static String isolateRightColumnString(String token, String delete)
    {
        token = token.replaceAll(delete, "");
        return token;
    }

    public static int isolateCombatDamage(String token)
    {
        token = token.replaceAll("COMBAT - Increases damage by ", "");
        token = token.replaceAll(" points.", "");
        return Integer.parseInt(token);
    }

    public static int isolateSkillBonus(String token, String type)
    {
        token = token.replaceAll("Increases " + type + " Rank by ", "");
        token = token.replace(".", "");
        return Integer.parseInt(token);
    }

    public static void isolateArtifactSkill(String token, Artifact artifact)
    {
        if (token.indexOf("COMBAT") > -1) {
            artifact.setCombatDamage(PDFUtil.isolateCombatDamage(token));
        }
        if (token.indexOf("Command") > -1) {
            artifact.setCommandSkill(PDFUtil.isolateSkillBonus(token, "Command"));
        }
        if (token.indexOf("Mage") > -1) {
            artifact.setMageSkill(PDFUtil.isolateSkillBonus(token, "Mage"));
        }
        if (token.indexOf("Agent") > -1) {
            artifact.setAgentSkill(PDFUtil.isolateSkillBonus(token, "Agent"));
        }
        if (token.indexOf("Emissary") > -1) {
            artifact.setEmissarySkill(PDFUtil.isolateSkillBonus(token, "Emissary"));
        }

    }

    public static String removeIndent(String token)
    {
        while (token.indexOf(" ") == 0) {
            token = token.substring(1, token.length());
        }
        return token;
    }

    public static Hex parseLocation(String token)
    {
        //System.out.println("PARSE LOCATION:");
        //System.out.println("\t\t" + token);
        Hex hex = new Hex();
        hex.setHex(Integer.parseInt(token.substring(token.indexOf("Location : @ ") + 13, token.indexOf(" in "))));
        hex.setTerrain(token.substring(token.indexOf(" in ") + 4, token.indexOf("  Climate is")));
        hex.setClimate(token.substring(token.indexOf("Climate is ") + 11, token.length()));
        return hex;
    }

    public static Order parseOrder(String token)
    {
        //System.out.println("PARSER ORDER: ");
        //System.out.println(token);
        Order order = new Order();
        String name = token.substring(0, token.indexOf("  "));
        order.setCharacter(name);
        token = token.replace(name, "");
        while (token.indexOf(" ") == 0) {
            token = token.replaceFirst(" ", "");
        }
        int orderNumber = 0;
        if (token.indexOf("NoOrder") < 0) {
            orderNumber = Integer.parseInt(token.substring(0, 3));
            token = token.replaceFirst(Integer.toString(orderNumber), "");
        } else {
            token = token.replaceFirst("NoOrder", "");
        }
        order.setOrderNumber(orderNumber);

        while (token.indexOf(" ") == 0) {
            token = token.replaceFirst(" ", "");
        }
        //System.out.println(token);
        String orderShort = token.substring(0, token.indexOf(" "));
        order.setOrderCode(orderShort);
        token = token.replaceFirst(orderShort, "");
        while (token.indexOf(" ") == 0) {
            token = token.replaceFirst(" ", "");
        }
        order.setAdditionalInfo(token);
        return order;
    }

    public static String parseCharacterName(String token, Game game)
    {
        //System.out.println("PARSE CHARACTER NAME");
        //System.out.println("\t" + token);
        StringBuilder characterName = new StringBuilder();
        while (token.indexOf("Ranks       :") < 0) {
            if (token.length() == 1) {
                char tokenP = token.charAt(0);
                characterName.append(tokenP);
            }
            token = game.getParser().nextToken();
            //System.out.println("NEXT: \t\t" + token);
        }
        //System.out.println("\tCharacter Name: " + characterName);
        return characterName.toString();
    }

    public static void parseCharactersWithArmy(String token, Army army)
    {
        while (token.indexOf(" - ") > -1) {
            token = token.substring((token.indexOf(" - ") + " - ".length()), token.length());
            String name = "";
            if (token.indexOf(" - ") > -1) {
                name = token.substring(0, token.indexOf(" - "));
            } else {
                name = token.substring(0, token.length());
            }
            //System.out.println("Adding Character: " + name);
            army.addCharacterInArmy(name);
        }
        //System.out.println("Character Name: " + characterName);        
    }

    public static void parseCharacterArtifacts(String token, CharacterTurn ct, Game game)
    {
        //System.out.println("Char Artifacts: " + token);
        if (token.indexOf("None") > -1) {
            game.getParser().nextToken();
        } else {
            while (!(token.indexOf("Spells") > -1) && (!(token.indexOf("None") > -1))) {
                token = token.substring((token.indexOf("#") + 1), token.length());
                token = token.substring(0, token.indexOf(" "));
                //System.out.println(token);
                ct.addArtifact(Integer.parseInt(token));
                token = game.getParser().nextToken();
                while (token.length() == 1) {
                    token = game.getParser().nextToken();
                }
            }
        }
        game.getParser().nextToken();
    }

    public static void parseCharacterSpells(String token, CharacterTurn ct, Game game)
    {
        //System.out.println("Char Spells: " + token);
        if (token.indexOf("None") > -1) {
            game.getParser().nextToken();
        } else {
            while (!(token.indexOf(" was ordered ") > -1) && !(token.indexOf(" commands ") > -1) && !(token.indexOf("was located") > -1) && !(token.indexOf("is currently") > -1) && !(token.indexOf("special ability") > -1)) {
                if (token.indexOf("(+0)") > -1) {
                    token = game.getParser().nextToken();
                }
                //System.out.println(token);
                token = token.substring((token.indexOf("#") + 1), token.length());
                token = token.substring(0, token.indexOf(" "));
                ct.addSpell(Integer.parseInt(token));
                token = game.getParser().nextToken();
                if (token.equals("")) {
                    token = game.getParser().nextToken();
                    //System.out.println(token);
                }
            }
        }
    }

    public static void parseCharacterOrderDetails(String token, CharacterTurn ct, Game game)
    {
        //System.out.println(token);
        while (token.equals(" ") || token.equals(".")) {
            token = game.getParser().nextToken();
            //System.out.println(token);
        }
        StringBuilder orderSummary = new StringBuilder();
        while (!(token.length() == 1)) {
            //System.out.println(token);
            orderSummary.append(token);
            token = game.getParser().nextToken();
            while (token.equals(" ") || token.equals(".")) {
                token = game.getParser().nextToken();
            }
        }
        String os = orderSummary.toString();
        //System.out.println(os);
        String hex = "0";
        if (!(os.indexOf("held hostage") > -1)) {
            //System.out.println("ACTIVE CHARACTER");
            if (os.indexOf(" is currently ") > -1) {
                hex = os.substring(os.indexOf(" is currently "), os.length());
            } else if (os.indexOf(" is traveling ") > -1) {
                hex = os.substring(os.indexOf(" is traveling "), os.length());
            } else if (os.indexOf(" commands ") > -1) {
                hex = os.substring(os.indexOf(" commands "), os.length());
            } else if (os.indexOf(" was located ") > -1) {
                hex = os.substring(os.indexOf(" was located "), os.length());
            }
            hex = hex.substring((hex.indexOf(" at ") + 4), hex.length());
            hex = hex.substring(0, hex.indexOf("."));
        }
        //System.out.println(game.getParser().getCurrentToken());
        ct.setHex(Integer.parseInt(hex));
        ct.setOrderSummary(orderSummary.toString());
    }
}