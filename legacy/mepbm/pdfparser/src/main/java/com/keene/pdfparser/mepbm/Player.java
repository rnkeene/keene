/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Player implements Serializable {

    private String name;
    private int accountNumber;

    public Player() {
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tPlayer Info:\n");        
        toString.append("\t\tName: ");
        toString.append(this.name);
        toString.append("\n");
        toString.append("\t\tAccount Number: ");
        toString.append(this.accountNumber);
        toString.append("\n");
        return toString.toString();
    }

}
