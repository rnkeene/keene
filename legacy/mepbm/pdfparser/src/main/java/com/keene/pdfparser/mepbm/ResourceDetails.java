/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class ResourceDetails implements Serializable {

    private int leatherStore;
    private int leatherProduction;

    private int bronzeStore;
    private int bronzeProduction;

    private int steelStore;
    private int steelProduction;

    private int mithrilStore;
    private int mithrilProduction;

    private int foodStore;
    private int foodProduction;

    private int timberStore;
    private int timberProduction;

    private int mountsStore;
    private int mountsProduction;

    private int goldStore;
    private int goldProduction;

    public ResourceDetails() {
    }


    public int getBronzeStore() {
        return bronzeStore;
    }

    public void setBronzeStore(int bronzeStore) {
        this.bronzeStore = bronzeStore;
    }

    public int getFoodProduction() {
        return foodProduction;
    }

    public void setFoodProduction(int foodProduction) {
        this.foodProduction = foodProduction;
    }

    public int getFoodStore() {
        return foodStore;
    }

    public void setFoodStore(int foodStore) {
        this.foodStore = foodStore;
    }

    public int getGoldProduction() {
        return goldProduction;
    }

    public void setGoldProduction(int goldProduction) {
        this.goldProduction = goldProduction;
    }

    public int getGoldStore() {
        return goldStore;
    }

    public void setGoldStore(int goldStore) {
        this.goldStore = goldStore;
    }

    public int getLeatherStore() {
        return leatherStore;
    }

    public void setLeatherStore(int leatherStore) {
        this.leatherStore = leatherStore;
    }

    public int getMithrilProduction() {
        return mithrilProduction;
    }

    public void setMithrilProduction(int mithrilProduction) {
        this.mithrilProduction = mithrilProduction;
    }

    public int getMithrilStore() {
        return mithrilStore;
    }

    public void setMithrilStore(int mithrilStore) {
        this.mithrilStore = mithrilStore;
    }

    public int getMountsProduction() {
        return mountsProduction;
    }

    public void setMountsProduction(int mountsProduction) {
        this.mountsProduction = mountsProduction;
    }

    public int getMountsStore() {
        return mountsStore;
    }

    public void setMountsStore(int mountsStore) {
        this.mountsStore = mountsStore;
    }

    public int getSteelProduction() {
        return steelProduction;
    }

    public void setSteelProduction(int steelProduction) {
        this.steelProduction = steelProduction;
    }

    public int getSteelStore() {
        return steelStore;
    }

    public void setSteelStore(int steelStore) {
        this.steelStore = steelStore;
    }

    public int getTimberProduction() {
        return timberProduction;
    }

    public void setTimberProduction(int timberProduction) {
        this.timberProduction = timberProduction;
    }

    public int getTimberStore() {
        return timberStore;
    }

    public void setTimberStore(int timberStore) {
        this.timberStore = timberStore;
    }

    public int getBronzeProduction() {
        return bronzeProduction;
    }

    public void setBronzeProduction(int bronzeProduction) {
        this.bronzeProduction = bronzeProduction;
    }

    public int getLeatherProduction() {
        return leatherProduction;
    }

    public void setLeatherProduction(int leatherProduction) {
        this.leatherProduction = leatherProduction;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tResource Details: \n");
        toString.append("\tProduction: \n");
        toString.append("\t\tLeather - ");
        toString.append(this.leatherProduction);
        toString.append("\tBronze - ");
        toString.append(this.bronzeProduction);
        toString.append("\tSteel - ");
        toString.append(this.steelProduction);
        toString.append("\tMithril - ");
        toString.append(this.mithrilProduction);
        toString.append("\tFood - ");
        toString.append(this.foodProduction);
        toString.append("\tTimber - ");
        toString.append(this.timberProduction);
        toString.append("\tMounts - ");
        toString.append(this.mountsProduction);
        toString.append("\tGold - ");
        toString.append(this.goldProduction);
        toString.append("\n");
        toString.append("\tStores: \n");
        toString.append("\t\tLeather - ");
        toString.append(this.leatherStore);
        toString.append("\tBronze - ");
        toString.append(this.bronzeStore);
        toString.append("\tSteel - ");
        toString.append(this.steelStore);
        toString.append("\tMithril - ");
        toString.append(this.mithrilStore);
        toString.append("\tFood - ");
        toString.append(this.foodStore);
        toString.append("\tTimber - ");
        toString.append(this.timberStore);
        toString.append("\tMounts - ");
        toString.append(this.mountsStore);
        toString.append("\tGold - ");
        toString.append(this.goldStore);
        toString.append("\n");
        return toString.toString();
    }

}
