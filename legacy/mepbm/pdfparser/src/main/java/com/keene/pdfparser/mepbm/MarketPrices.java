/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class MarketPrices implements Serializable {

    private int leatherBuyPrice;
    private int leatherSellPrice;

    private int bronzeBuyPrice;
    private int bronzeSellPrice;

    private int steelBuyPrice;
    private int steelSellPrice;

    private int mithrilBuyPrice;
    private int mithrilSellPrice;

    private int foodBuyPrice;
    private int foodSellPrice;

    private int timberBuyPrice;
    private int timberSellPrice;

    private int mountsBuyPrice;
    private int mountsSellPrice;

    private int goldBuyPrice;
    private int goldSellPrice;

    public MarketPrices() {
    }

    public int getBronzeBuyPrice() {
        return bronzeBuyPrice;
    }

    public void setBronzeBuyPrice(int bronzeBuyPrice) {
        this.bronzeBuyPrice = bronzeBuyPrice;
    }

    public int getBronzeSellPrice() {
        return bronzeSellPrice;
    }

    public void setBronzeSellPrice(int bronzeSellPrice) {
        this.bronzeSellPrice = bronzeSellPrice;
    }

    public int getFoodBuyPrice() {
        return foodBuyPrice;
    }

    public void setFoodBuyPrice(int foodBuyPrice) {
        this.foodBuyPrice = foodBuyPrice;
    }

    public int getFoodSellPrice() {
        return foodSellPrice;
    }

    public void setFoodSellPrice(int foodSellPrice) {
        this.foodSellPrice = foodSellPrice;
    }

    public int getGoldBuyPrice() {
        return goldBuyPrice;
    }

    public void setGoldBuyPrice(int goldBuyPrice) {
        this.goldBuyPrice = goldBuyPrice;
    }

    public int getGoldSellPrice() {
        return goldSellPrice;
    }

    public void setGoldSellPrice(int goldSellPrice) {
        this.goldSellPrice = goldSellPrice;
    }

    public int getLeatherBuyPrice() {
        return leatherBuyPrice;
    }

    public void setLeatherBuyPrice(int leatherBuyPrice) {
        this.leatherBuyPrice = leatherBuyPrice;
    }

    public int getLeatherSellPrice() {
        return leatherSellPrice;
    }

    public void setLeatherSellPrice(int leatherSellPrice) {
        this.leatherSellPrice = leatherSellPrice;
    }

    public int getMithrilBuyPrice() {
        return mithrilBuyPrice;
    }

    public void setMithrilBuyPrice(int mithrilBuyPrice) {
        this.mithrilBuyPrice = mithrilBuyPrice;
    }

    public int getMithrilSellPrice() {
        return mithrilSellPrice;
    }

    public void setMithrilSellPrice(int mithrilSellPrice) {
        this.mithrilSellPrice = mithrilSellPrice;
    }

    public int getMountsBuyPrice() {
        return mountsBuyPrice;
    }

    public void setMountsBuyPrice(int mountsBuyPrice) {
        this.mountsBuyPrice = mountsBuyPrice;
    }

    public int getMountsSellPrice() {
        return mountsSellPrice;
    }

    public void setMountsSellPrice(int mountsSellPrice) {
        this.mountsSellPrice = mountsSellPrice;
    }

    public int getSteelBuyPrice() {
        return steelBuyPrice;
    }

    public void setSteelBuyPrice(int steelBuyPrice) {
        this.steelBuyPrice = steelBuyPrice;
    }

    public int getSteelSellPrice() {
        return steelSellPrice;
    }

    public void setSteelSellPrice(int steelSellPrice) {
        this.steelSellPrice = steelSellPrice;
    }

    public int getTimberBuyPrice() {
        return timberBuyPrice;
    }

    public void setTimberBuyPrice(int timberBuyPrice) {
        this.timberBuyPrice = timberBuyPrice;
    }

    public int getTimberSellPrice() {
        return timberSellPrice;
    }

    public void setTimberSellPrice(int timberSellPrice) {
        this.timberSellPrice = timberSellPrice;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tMARKET PRICES:\n");
        toString.append("\tSellPrice: \n");
        toString.append("\t\tLeather - ");
        toString.append(this.leatherSellPrice);
        toString.append("\tBronze - ");
        toString.append(this.bronzeSellPrice);
        toString.append("\tSteel - ");
        toString.append(this.steelSellPrice);
        toString.append("\tMithril - ");
        toString.append(this.mithrilSellPrice);
        toString.append("\tFood - ");
        toString.append(this.foodSellPrice);
        toString.append("\tTimber - ");
        toString.append(this.timberSellPrice);
        toString.append("\tMounts - ");
        toString.append(this.mountsSellPrice);
        toString.append("\tGold - ");
        toString.append(this.goldSellPrice);
        toString.append("\n");
        toString.append("\tBuyPrices: \n");
        toString.append("\t\tLeather - ");
        toString.append(this.leatherBuyPrice);
        toString.append("\tBronze - ");
        toString.append(this.bronzeBuyPrice);
        toString.append("\tSteel - ");
        toString.append(this.steelBuyPrice);
        toString.append("\tMithril - ");
        toString.append(this.mithrilBuyPrice);
        toString.append("\tFood - ");
        toString.append(this.foodBuyPrice);
        toString.append("\tTimber - ");
        toString.append(this.timberBuyPrice);
        toString.append("\tMounts - ");
        toString.append(this.mountsBuyPrice);
        toString.append("\tGold - ");
        toString.append(this.goldBuyPrice);
        toString.append("\n");
        return toString.toString();
    }
    public String toHTML(){
        StringBuilder toString = new StringBuilder();
        toString.append("<h3>MARKET PRICES:</h3>");

        toString.append("<b>Sell Prices: <b/><br />");
        toString.append("<table>");
        toString.append("<tr>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Leather</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Bronze</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Steel</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Food</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Timber</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Mounts</b>");
        toString.append("</td>");

        toString.append("</tr>");
        toString.append("<tr>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.leatherSellPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.bronzeSellPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.steelSellPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.mithrilSellPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.foodSellPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.timberSellPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.mountsSellPrice);
        toString.append("</td>");

        toString.append("</tr>");
        toString.append("</table>");

        toString.append("<b>Buy Prices: <b/><br />");
        
        toString.append("<table>");
        toString.append("<tr>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Leather</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Bronze</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Steel</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Food</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Timber</b>");
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append("<b>Mounts</b>");
        toString.append("</td>");

        toString.append("</tr>");
        toString.append("<tr>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.leatherBuyPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.bronzeBuyPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.steelBuyPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.mithrilBuyPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.foodBuyPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.timberBuyPrice);
        toString.append("</td>");

        toString.append("<td style=\"text-align:center;\">");
        toString.append(this.mountsBuyPrice);
        toString.append("</td>");

        toString.append("</tr>");
        toString.append("</table>");

        return toString.toString();
    }

}
