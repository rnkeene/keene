/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nate
 */
public class PopulationCenter implements Serializable {

    private String name;
    private Hex hex;
    private int turn;
    private int loyalty;
    private String size;
    private String fortification;
    private boolean capital;
    private String climate;
    private String docks;
    private boolean hidden;
    private boolean sieged;
    private ResourceDetails resourceDetails;
    private List<String> foreignCharacters;
    private List<String> armies;

    public PopulationCenter() {
        this.resourceDetails = new ResourceDetails();
        this.foreignCharacters = new ArrayList<String>();
        this.armies = new ArrayList<String>();
    }

    public String getFortification() {
        return fortification;
    }

    public void setFortification(String fortification) {
        this.fortification = fortification;
    }

    public int getLoyalty() {
        return loyalty;
    }

    public void setLoyalty(int loyalty) {
        this.loyalty = loyalty;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public boolean isCapital() {
        return capital;
    }

    public void setCapital(boolean capital) {
        this.capital = capital;
    }

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public String getDocks() {
        return docks;
    }

    public void setDocks(String docks) {
        this.docks = docks;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isSieged() {
        return sieged;
    }

    public void setSieged(boolean sieged) {
        this.sieged = sieged;
    }

    public ResourceDetails getResourceDetails() {
        return resourceDetails;
    }

    public void setResourceDetails(ResourceDetails resourceDetails) {
        this.resourceDetails = resourceDetails;
    }

    public Hex getHex() {
        return hex;
    }

    public void setHex(Hex hex) {
        this.hex = hex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getForeignCharacters() {
        return foreignCharacters;
    }

    public void addForeignCharacter(String foreignCharacter) {
        this.foreignCharacters.add(foreignCharacter);
    }

    public List<String> getArmies() {
        return armies;
    }

    public void addArmy(String army) {
        this.armies.add(army);
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("Population Center\n");
        toString.append("\tName: ");
        toString.append(this.name);
        toString.append("\n");
        toString.append(hex.toString());
        toString.append("\tPopulation Center Info\n");
        toString.append("\t\tTurn: ");
        toString.append(this.turn);
        toString.append("\n");
        toString.append("\t\tLoyalty: ");
        toString.append(this.loyalty);
        toString.append("\n");
        toString.append("\t\tSize: ");
        toString.append(this.size);
        toString.append("\n");
        toString.append("\t\tFortifications: ");
        toString.append(this.fortification);
        toString.append("\n");
        toString.append("\t\tClimate: ");
        toString.append(this.climate);
        toString.append("\n");
        toString.append("\t\tCapital? ");
        toString.append(this.capital);
        toString.append("\n");
        toString.append("\t\tDocks: ");
        toString.append(this.docks);
        toString.append("\n");
        toString.append("\t\tHidden: ");
        toString.append(this.hidden);
        toString.append("\n");
        toString.append("\t\tSieged: ");
        toString.append(this.sieged);
        toString.append("\n");
        toString.append(resourceDetails.toString());
        toString.append("\t\tForeign Characters: ");
        for (String character : foreignCharacters) {
            toString.append("\t\t\t");
            toString.append(character);
            toString.append("\n");
        }
        toString.append("\t\tArmies: ");
        for (String army : armies) {
            toString.append("\t\t\t");
            toString.append(army);
            toString.append("\n");
        }
        return toString.toString();
    }
}
