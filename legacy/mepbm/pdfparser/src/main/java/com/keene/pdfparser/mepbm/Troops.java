/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class Troops implements Serializable {

    private int numbers;
    private int training;
    private int weapons;
    private int armor;
    private String troopType;
    private String description;

    public Troops() {
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getTraining() {
        return training;
    }

    public void setTraining(int training) {
        this.training = training;
    }

    public String getTroopType() {
        return troopType;
    }

    public void setTroopType(String troopType) {
        this.troopType = troopType;
    }

    public int getWeapons() {
        return weapons;
    }

    public void setWeapons(int weapons) {
        this.weapons = weapons;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tTROOPS:");
        toString.append("\n");
        toString.append("\t\tTroop Number: ");
        toString.append(this.numbers);
        toString.append("\n");
        toString.append("\t\tTroop Type: ");
        toString.append(this.troopType);
        toString.append("\n");
        toString.append("\t\tDescription: ");
        toString.append(this.description);
        toString.append("\n");
        toString.append("\t\tTraining: ");
        toString.append(this.training);
        toString.append("\n");
        toString.append("\t\tWeapons: ");
        toString.append(this.weapons);
        toString.append("\n");
        toString.append("\t\tArmor: ");
        toString.append(this.armor);
        toString.append("\n");
        return toString.toString();
    }

}
