/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class NationMessage implements Serializable {

    private String message;

    public NationMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t\t");
        toString.append(this.message);
        toString.append("\n");
        return toString.toString();
    }
}
