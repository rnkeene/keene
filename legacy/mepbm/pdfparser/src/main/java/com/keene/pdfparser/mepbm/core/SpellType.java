/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.core;

/**
 *
 * @author nate
 */
public enum SpellType {

    OFFENSIVE,
    DEFENSIVE,
    HEALING;
}
