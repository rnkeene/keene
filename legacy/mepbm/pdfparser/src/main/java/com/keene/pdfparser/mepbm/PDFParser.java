/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PRTokeniser;
import com.itextpdf.text.pdf.PdfReader;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nate
 */
public class PDFParser implements Serializable {

    PdfReader reader;
    PRTokeniser tokenizer;
    String currentToken;
    Game activeGame;
    private int activeTurn;
    private byte[] streamBytes;
    private Nation activeNation;
    private NationTurn activeNationTurn;
    private int activePage;
    private PopulationCenter activePopCenter;
    private Date activePDFDate;

    public PDFParser(PdfReader reader, Game game) throws IOException {
        this.reader = reader;
        this.activeGame = game;
        this.activePage = 0;
        this.nextPage();
    }

    public Date getActivePDFDate(){
        return activePDFDate;
    }

    public void overrideActiveToken(String newToken){
        this.currentToken = newToken;
    }

    public String nextToken() {
        try {
            while (tokenizer.nextToken()) {
                if (tokenizer.getTokenType() == PRTokeniser.TokenType.STRING) {
                    String token = tokenizer.getStringValue();                    
                    if (token != null) {
                        if (token.indexOf(" Game ") > -1 && token.indexOf(" Player ") > -1 && token.indexOf(" Turn ") > -1 && token.indexOf(" Page ") > -1) {
                            if(activePDFDate==null){
                                String date = token.substring(token.indexOf("  "), token.indexOf("  Game "));
                                //System.out.println(date);
                                date = date.replaceAll(" ", "");
                                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
                                activePDFDate=format.parse(date);
                                //System.out.println(activePDFDate);
                            }
                            return this.nextToken();
                        } else {
                            this.setCurrentToken(token);
                            //System.out.println(token);
                            return this.getCurrentToken();
                        }
                    }
                }
            }
            this.nextPage();
            if (this.tokenizer != null) {
                return this.nextToken();
            }
        } catch (InvalidPdfException e) {
            return this.nextToken();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        this.currentToken = null;
        this.reader = null;
        this.tokenizer = null;
        return null;
    }

    public void nextPage() {
        try {
            this.streamBytes = reader.getPageContent(this.activePage);
            this.tokenizer = new PRTokeniser(this.streamBytes);
            this.activePage++;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public PdfReader getReader() {
        return reader;
    }

    public void setReader(PdfReader reader) {
        this.reader = reader;
    }

    public PRTokeniser getTokenizer() {
        return tokenizer;
    }

    public void setTokenizer(PRTokeniser tokenizer) {
        this.tokenizer = tokenizer;
    }

    public Game getActiveGame() {
        return activeGame;
    }

    public void setActiveGame(Game activeGame) {
        this.activeGame = activeGame;
    }

    public Nation getActiveNation() {
        return activeNation;
    }

    public void setActiveNation(Nation activeNation) {
        this.activeNation = activeNation;
    }

    public int getActivePage() {
        return activePage;
    }

    public void setActivePage(int activePage) {
        this.activePage = activePage;
    }

    public PopulationCenter getActivePopCenter() {
        return activePopCenter;
    }

    public void setActivePopCenter(PopulationCenter activePopCenter) {
        this.activePopCenter = activePopCenter;
    }
    
    public int getActiveTurn() {
        return activeTurn;
    }

    public void setActiveTurn(int activeTurn) {
        this.activeTurn = activeTurn;
    }

    public String getCurrentToken() {
        return currentToken;
    }

    public void setCurrentToken(String currentToken) {
        this.currentToken = currentToken;
    }

    public NationTurn getActiveNationTurn()
    {
        return activeNationTurn;
    }

    public void setActiveNationTurn(NationTurn activeNationTurn)
    {
        this.activeNationTurn = activeNationTurn;
    }
    
}
