/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.derived.service;

import com.keene.pdfparser.mepbm.*;
import com.keene.pdfparser.mepbm.Character;
import com.keene.pdfparser.mepbm.core.Skill;
import com.keene.pdfparser.mepbm.derived.CharacterSkillGrowth;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nate
 */
public class CharacterSkillGrowthService {

    public static List<CharacterSkillGrowth> findCharacterGrowth(Map<String, Nation> nations, int startTurn, int endTurn, Skill skill) {
        List<CharacterSkillGrowth> growth = new ArrayList<CharacterSkillGrowth>();
        Set<String> keys = nations.keySet();
        for (String key : keys) {
            Nation nation = nations.get(key);
            int count = startTurn;
            //System.out.println("NATION: " + nation.getNationName());
            while (count <= endTurn) {
                NationTurn turn = nation.getNationTurn(count);
                if (turn != null) {

                    List<Character> characters = turn.getCharacters();
                    //System.out.println("\tTURN: " + count);
                    if (skill.equals(Skill.Core)) {
                        for (Skill growSkill : Skill.getCoreList()) {
                            int skillTotal = 0;
                            CharacterSkillGrowth grow = new CharacterSkillGrowth();
                            grow.setDate(turn.getRunOnDate());
                            grow.setSkillName(growSkill);
                            for (Character character : characters) {
                                //System.out.println("\t\tCharacter: " + character.getName());
                                CharacterTurn ct = character.getCharacterTurn(count);
                                if (grow.getSkillName().equals(Skill.Command)) {
                                    skillTotal += ct.getCommandRank();
                                } else if (grow.getSkillName().equals(Skill.Agent)) {
                                    skillTotal += ct.getAgentRank();
                                } else if (grow.getSkillName().equals(Skill.Emissary)) {
                                    skillTotal += ct.getEmissaryRank();
                                } else if (grow.getSkillName().equals(Skill.Mage)) {
                                    skillTotal += ct.getMageRank();
                                }
                            }
                            grow.setNationName(nation.getNationName() + " - " + growSkill);
                            //System.out.println("\tTURN TOTAL: " + skillTotal);
                            grow.setSkillValue(skillTotal);
                            grow.setTurn(count);
                            //System.out.println(grow);
                            growth.add(grow);
                        }
                    } else {
                        CharacterSkillGrowth grow = new CharacterSkillGrowth();
                        grow.setDate(turn.getRunOnDate());
                        grow.setSkillName(skill);
                        int skillTotal = 0;
                        for (Character character : characters) {
                            //System.out.println("\t\tCharacter: " + character.getName());
                            CharacterTurn ct = character.getCharacterTurn(count);
                            if (grow.getSkillName().equals(Skill.Command)) {
                                skillTotal += ct.getCommandRank();
                            } else if (grow.getSkillName().equals(Skill.Agent)) {
                                skillTotal += ct.getAgentRank();
                            } else if (grow.getSkillName().equals(Skill.Emissary)) {
                                skillTotal += ct.getEmissaryRank();
                            } else if (grow.getSkillName().equals(Skill.Mage)) {
                                skillTotal += ct.getMageRank();
                            }
                        }
                        grow.setNationName(nation.getNationName());
                        //System.out.println("\tTURN TOTAL: " + skillTotal);
                        grow.setSkillValue(skillTotal);
                        grow.setTurn(count);
                        //System.out.println(grow);
                        growth.add(grow);
                    }
                } else {
                    //System.out.println("\tNO TURN " + count);
                }
                count++;
            }
        }
        return growth;
    }
}
