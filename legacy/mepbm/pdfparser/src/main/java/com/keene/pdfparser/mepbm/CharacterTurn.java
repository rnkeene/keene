/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nate
 */
public class CharacterTurn implements Serializable {

    private int turnNumber;
    private int mageRank;
    private int emissaryRank;
    private int commandRank;
    private int agentRank;
    private int hex;
    private int stealth;
    private int health;
    private int challengeRank;
    private List<Integer> artifacts;
    private String orderSummary;
    private List<Integer> spells;

    public CharacterTurn() {
        artifacts = new ArrayList<Integer>();
        spells = new ArrayList<Integer>();
    }

    public int getAgentRank() {
        return agentRank;
    }

    public void setAgentRank(int agentRank) {
        this.agentRank = agentRank;
    }

    public List<Integer> getArtifacts() {
        return artifacts;
    }

    public void addArtifact(Integer artifactNumber) {
        this.artifacts.add(artifactNumber);
    }

    public List<Integer> getSpells() {
        return spells;
    }

    public void addSpell(Integer spellNumber) {
        this.spells.add(spellNumber);
    }

    public String getOrderSummary() {
        return orderSummary;
    }

    public void setOrderSummary(String orderSummary) {
        this.orderSummary = orderSummary;
    }


    public int getCommandRank() {
        return commandRank;
    }

    public void setCommandRank(int commandRank) {
        this.commandRank = commandRank;
    }

    public int getEmissaryRank() {
        return emissaryRank;
    }

    public void setEmissaryRank(int emissaryRank) {
        this.emissaryRank = emissaryRank;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHex() {
        return hex;
    }

    public void setHex(int hex) {
        this.hex = hex;
    }

    public int getMageRank() {
        return mageRank;
    }

    public void setMageRank(int mageRank) {
        this.mageRank = mageRank;
    } 

    public int getStealth() {
        return stealth;
    }

    public void setStealth(int stealth) {
        this.stealth = stealth;
    }

    public int getTurnNumber() {
        return turnNumber;
    }

    public void setTurnNumber(int turnNumber) {
        this.turnNumber = turnNumber;
    }

    public int getChallengeRank() {
        return challengeRank;
    }

    public void setChallengeRank(int challengeRank) {
        this.challengeRank = challengeRank;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tCharacter Info: \n");
        toString.append("\t\tTurn Number - ");
        toString.append(this.turnNumber);
        toString.append("\n");
        toString.append("\t\tHex - ");
        toString.append(this.hex);
        toString.append("\n");
        toString.append("\t\tARTIFACTS HELD:\n");
        for(Integer artifactId : artifacts){
            toString.append("\t\t\t#");
            toString.append(artifactId);
            toString.append("\n");
        }
        toString.append("\t\tSPELLS KNOWN:\n");
        for(Integer spellNumber : spells){
            toString.append("\t\t\t#");
            toString.append(spellNumber);
            toString.append("\n");
        }
        toString.append("\t\tHealth - ");
        toString.append(this.health);
        toString.append("\n");
        toString.append("\t\tChallenge Rank - ");
        toString.append(this.challengeRank);
        toString.append("\n");
        toString.append("\t\tAgent Rank - ");
        toString.append(this.agentRank);
        toString.append("\n");
        toString.append("\t\tCommand Rank - ");
        toString.append(this.commandRank);
        toString.append("\n");
        toString.append("\t\tEmissary Rank - ");
        toString.append(this.emissaryRank);
        toString.append("\n");
        toString.append("\t\tMage Rank - ");
        toString.append(this.mageRank);
        toString.append("\n");
        toString.append("\t\tOrder Summary:\n");
        toString.append("\t\t");
        toString.append(this.orderSummary);
        toString.append("\n");
        return toString.toString();
    }

}
