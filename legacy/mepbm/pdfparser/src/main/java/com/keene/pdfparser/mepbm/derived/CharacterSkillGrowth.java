/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm.derived;

import com.keene.pdfparser.mepbm.core.Skill;
import java.io.Serializable;

/**
 *
 * @author nate
 */
public class CharacterSkillGrowth implements Serializable {

    private String nationName;
    private Skill skill;
    private int turn;
    private int skillValue;
    private long date;

    public CharacterSkillGrowth() {
    }

    public String getNationName()
    {
        return nationName;
    }

    public void setNationName(String nationName)
    {
        this.nationName = nationName;
    }

    public Skill getSkillName()
    {
        return skill;
    }

    public void setSkillName(Skill skill)
    {
        this.skill = skill;
    }

    public int getSkillValue()
    {
        return skillValue;
    }

    public void setSkillValue(int skillValue)
    {
        this.skillValue = skillValue;
    }

    public int getTurn()
    {
        return turn;
    }

    public void setTurn(int turn)
    {
        this.turn = turn;
    }

    public long getDate()
    {
        return date;
    }

    public void setDate(long date)
    {
        this.date = date;
    }


    public String toHTML(){
        StringBuilder toString = new StringBuilder();
        toString.append("Character Growth:<br /><br />");
        toString.append("<b>Nation: </b>");
        toString.append(this.nationName);
        toString.append("<br />");
        toString.append("<b>Turn: </b>");
        toString.append(this.turn);
        toString.append("<br />");
        toString.append("<b>Skill: </b>");
        toString.append(this.skill);
        toString.append("<br />");
        toString.append("<b>Value: </b>");
        toString.append(this.skillValue);
        toString.append("<hr />");
        return toString.toString();
    }
    
    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("Character Growth:\n");
        toString.append("\tNation: ");
        toString.append(this.nationName);
        toString.append("\n");
        toString.append("\tTurn: ");
        toString.append(this.turn);
        toString.append("\n");
        toString.append("\tSkill: ");
        toString.append(this.skill);
        toString.append("\n");
        toString.append("\tValue: ");
        toString.append(this.skillValue);
        toString.append("\n");
        return toString.toString();
    }

}
