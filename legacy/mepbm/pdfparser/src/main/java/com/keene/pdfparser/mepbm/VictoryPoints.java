/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.pdfparser.mepbm;

import java.io.Serializable;

/**
 *
 * @author nate
 */
public class VictoryPoints implements Serializable {

    private int army;
    private int character;
    private int wealth;
    private int populationCenter;
    private int individual;

    public VictoryPoints() {
    }

    public int getArmy()
    {
        return army;
    }

    public void setArmy(int army)
    {
        this.army = army;
    }

    public int getCharacter()
    {
        return character;
    }

    public void setCharacter(int character)
    {
        this.character = character;
    }

    public int getIndividualVictoryPoints()
    {
        return individual;
    }

    public void setIndividual(int individual)
    {
        this.individual = individual;
    }

    public int getPopulationCenter()
    {
        return populationCenter;
    }

    public void setPopulationCenter(int populationCenter)
    {
        this.populationCenter = populationCenter;
    }

    public int getWealth()
    {
        return wealth;
    }

    public void setWealth(int wealth)
    {
        this.wealth = wealth;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t\tArmy: ");
        toString.append(this.army);
        toString.append("\n");
        toString.append("\t\tCharacter: ");
        toString.append(this.character);
        toString.append("\n");
        toString.append("\t\tIndividual: ");
        toString.append(this.individual);
        toString.append("\n");
        toString.append("\t\tPopulation Center: ");
        toString.append(this.populationCenter);
        toString.append("\n");
        toString.append("\t\tWealth: ");
        toString.append(this.wealth);
        toString.append("\n");
        return toString.toString();
    }
}
