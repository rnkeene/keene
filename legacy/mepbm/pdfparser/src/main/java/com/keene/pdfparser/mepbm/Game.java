/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.pdfparser.mepbm;

import com.itextpdf.text.pdf.PdfReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author nate
 */
public class Game implements Serializable {

    private transient PDFParser parser;
    private int number;
    private int turnsRun;
    private String type;
    private Map<Integer, Nation> nationMap;
    private Map<Integer, Market> marketMap;

    public Game() {
        nationMap = new HashMap<Integer, Nation>();
        marketMap = new HashMap<Integer, Market>();
        turnsRun = 0;
    }

    public int getTurnsRun() {
        if (this.turnsRun == 0) {
            Set<Integer> keys = marketMap.keySet();
            for (int key : keys) {
                if (this.turnsRun < key) {
                    this.turnsRun = key;
                }
            }
        }
        return this.turnsRun;
    }

    public void initParser(PdfReader reader) throws IOException {
        this.parser = new PDFParser(reader, this);
        this.parser.nextPage();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int gameNumber) {
        this.number = gameNumber;
    }

    public void addNation(Nation nation) {
//        System.out.println("Adding Nation:");
//        System.out.println(nation);
        nationMap.put(nation.getNationNumber(), nation);
    }

    public Nation getNation(int nationNumber) {
        return nationMap.get(nationNumber);
    }

    public Set<Integer> getNationKeys() {
        return nationMap.keySet();
    }

    public void addMarketDetails(Market market) {
        marketMap.put(market.getTurnNumber(), market);
    }

    public Market getMarketDetails(int turnNumber) {
        return marketMap.get(turnNumber);
    }

    public PDFParser getParser() {
        return parser;
    }

    public void setParser(PDFParser parser) {
        this.parser = parser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String toHTML() {
        StringBuilder toString = new StringBuilder();
        toString.append("<h1>Game Number: ");
        toString.append(this.getNumber());
        toString.append("</h1>");
        toString.append("<table>");

        toString.append("<tr>");
        toString.append("<td style=\"text-align:right;\">");
        toString.append("<b>Type:</b>");
        toString.append("</td>");
        toString.append("<td>");
        toString.append(this.getType());
        toString.append("</td>");
        toString.append("</tr>");

        Set<Integer> mkeys = marketMap.keySet();
        for (Integer key : mkeys) {
            toString.append("<tr>");
            toString.append("<td style=\"text-align:right;\">");
            toString.append("<b>Market Info for Turn ");
            toString.append(key);
            toString.append(":</b> ");
            toString.append("</td>");
            toString.append("<td>");
            toString.append(marketMap.get(key).toHTML());
            toString.append("</td>");
            toString.append("</tr>");
        }

        Set<Integer> nkeys = nationMap.keySet();
        for (Integer key : nkeys) {
            toString.append("<tr>");
            toString.append("<td style=\"text-align:right;\">");
            toString.append("<b>Nation ");
            toString.append(key);
            toString.append(":</b> ");
            toString.append("</td>");
            toString.append("<td>");
            toString.append("nationMap.get(key).toHTML()");
            toString.append("</td>");
            toString.append("</tr>");
        }
        toString.append("</table>");

        return toString.toString();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("GAME INFO:\n");
        toString.append("\t\tNumber: ");
        toString.append(this.getNumber());
        toString.append("\n");
        toString.append("\t\tType: ");
        toString.append(this.getType());
        toString.append("\n");
        Set<Integer> nkeys = nationMap.keySet();
        for (Integer key : nkeys) {
            toString.append(nationMap.get(key).toString());
        }
        Set<Integer> mkeys = marketMap.keySet();
        for (Integer key : mkeys) {
            toString.append(marketMap.get(key).toString());
        }
        return toString.toString();
    }
}
