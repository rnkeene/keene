package com.organick.servlet;

import com.keene.pdfparser.mepbm.Nation;
import com.keene.pdfparser.mepbm.derived.PopulationCenterGrowth;
import com.keene.pdfparser.mepbm.derived.service.PopulationCenterGrowthService;
import com.organick.googlecharts.visualization.ScriptBuilder;
import com.organick.googlecharts.visualization.Visualization;
import com.organick.googlecharts.visualization.decorator.chart.AnnotatedTimeLineDecorator;
import com.organick.googlecharts.visualization.decorator.chart.structure.TimeLineData;
import com.organick.googlecharts.visualization.factory.AnnotatedTimeLineVisualizationFactory;
import com.organick.googlecharts.visualization.factory.ScriptBuilderFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author nkeene
 */
public class PCGrowthVisualService {

    public static ScriptBuilder getVisual(Map<String, Nation> gnations, int startTurn, int endTurn, String reportType) {

        AnnotatedTimeLineDecorator tldecorator = new AnnotatedTimeLineDecorator();
        tldecorator = (AnnotatedTimeLineDecorator) tldecorator.getDefaultDecorator();

        List<String> legend = new ArrayList<String>();

        Map<String, Integer> nations = new HashMap<String, Integer>();
        Map<Integer, TimeLineData> turns = new HashMap<Integer, TimeLineData>();
        Map<Integer, String> nationOrder = new HashMap<Integer, String>();
        if (reportType.equals("count")) {
            List<PopulationCenterGrowth> growth = PopulationCenterGrowthService.findPopulationCenterGrowth(gnations, startTurn, endTurn);
            for (PopulationCenterGrowth grow : growth) {
                if (!nations.containsKey(grow.getNationName())) {
                    System.out.println("LEGEND: " + grow.getNationName());
                    legend.add(grow.getNationName());
                    nations.put(grow.getNationName(), nationOrder.size());
                    nationOrder.put(nationOrder.size(), grow.getNationName());
                }
                TimeLineData data = new TimeLineData();
                List<String> labels = new ArrayList<String>();
                List<String> details = new ArrayList<String>();
                List<Long> values = new ArrayList<Long>();
                if (turns.containsKey(grow.getTurn())) {
                    System.out.println("\tUPDATING: " + grow.getTurn());
                    data = turns.get(grow.getTurn());
                    labels = data.getLabels();
                    details = data.getDetails();
                    values = data.getValues();
                } else {
                    data.setDate(new Date(grow.getDate()));
                    data.setLabels(labels);
                    data.setDetails(details);
                    data.setValues(values);
                    System.out.println("ADDING: " + grow.getTurn());
                    turns.put(grow.getTurn(), data);
                }
                System.out.println("\t\tNation: " + grow.getNationName());
                System.out.println("\t\tPopulation Centers: " + grow.getPopulationCenters().size());
                labels.add(grow.getNationName());
                details.add("Turn " + grow.getTurn());
                values.add((long) grow.getPopulationCenters().size());
            }
        }

        List<TimeLineData> timeLineData = new ArrayList<TimeLineData>();

        int count = endTurn - startTurn;
        for (int x = 0; x <= count; x++) {
            TimeLineData data = turns.get(x);
            timeLineData.add(data);
        }
        System.out.println("TIME LINE DATA");
        TimeLineData previous = null;
        for (TimeLineData data : timeLineData) {
            System.out.println("\tFor Date: " + data.getDate());
            List<String> details = data.getDetails();
            List<String> label = data.getLabels();
            List<Long> values = data.getValues();
            if (details.size() < count) {
                System.out.println("\t\t\t" + details.size() + " < " + count);
            }
            for (int x = 0; x < details.size(); x++) {
                String nation = nationOrder.get(x);
                System.out.println("\t\t\t" + nation + "=?=" + label.get(x));
                if (nation.equals(label.get(x))) {
                    System.out.println("\t\t- " + details.get(x));
                    System.out.println("\t\t- " + label.get(x));
                    System.out.println("\t\t- " + values.get(x));
                    System.out.println();
                } else {
                    System.out.println("\t\t\tUPDATE FOR : " + nation);
                    String tempDetail = details.get(x);
                    String tempLabel = nation;
                    Long tempvalue = new Long(0);
                    if (previous != null) {
                        List<Long> tempValues = previous.getValues();
                        tempvalue = tempValues.get(x);
                    }
                    details.add(x, tempDetail);
                    label.add(x, tempLabel);
                    values.add(x, tempvalue);
                    System.out.println("\t\t- " + details.get(x));
                    System.out.println("\t\t- " + label.get(x));
                    System.out.println("\t\t- " + values.get(x));
                    System.out.println();
                }

            }
            //}
        }
        System.out.println("-- DONE --");
        List<Visualization> visuals = new ArrayList<Visualization>();
        visuals.add(AnnotatedTimeLineVisualizationFactory.getAnnotatedTimeLine(legend, timeLineData, tldecorator));
        ScriptBuilder scriptBuilder = ScriptBuilderFactory.getScriptBuilder(visuals);
        return scriptBuilder;
    }
}
