/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.smevent.persistence.entity;

import com.organick.smevent.model.Detail;
import com.organick.smevent.model.Event;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="detail")
public class DetailEntity implements Detail,Serializable{
    
    @Id
    @Column(name="detailId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column
    private String detail;
    
    /*@ManyToMany(mappedBy="details")
    private List<Event> events;

    
    
    public List<Event> getEvents() {
        if (events == null) {
            events = new ArrayList<Event>();
        }
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }*/

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getId() {
        return this.id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetailEntity other = (DetailEntity) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.detail != other.detail && (this.detail == null || !this.detail.equals(other.detail))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + (this.detail != null ? this.detail.hashCode() : 0);
        return hash;
    }

    
        

}
