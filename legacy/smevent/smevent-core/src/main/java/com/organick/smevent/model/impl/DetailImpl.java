/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.smevent.model.impl;

import com.organick.smevent.model.Detail;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="detail")
public class DetailImpl implements Detail,Serializable{
    
    private long id;
    private String detail;
    
    /*@ManyToMany(mappedBy="details")
    private List<Event> events;

    
    
    public List<Event> getEvents() {
        if (events == null) {
            events = new ArrayList<Event>();
        }
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }*/

    @XmlElement
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlElement
    public long getId() {
        return this.id;
    }

}
