/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.model;

/**
 *
 * @author nate
 */
public interface Recommendation extends Base {

    public String getRecommendation();

    public void setRecommendation(String recommendation);
}
