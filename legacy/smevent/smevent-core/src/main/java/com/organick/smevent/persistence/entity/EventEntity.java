/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.persistence.entity;

import com.organick.smevent.model.Account;
import com.organick.smevent.model.Detail;
import com.organick.smevent.model.Event;
import com.organick.smevent.model.Recommendation;
import com.organick.smevent.model.impl.EventImpl;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.apache.commons.beanutils.BeanUtils;

@Entity
@Table(name = "event")
public class EventEntity implements Event, Serializable {

    @Id
    @Column(name="eventId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String title;
    @Column
    private String description;
    @Column
    private long startDate;
    @Column
    private long endDate;
    @ManyToMany(fetch = FetchType.LAZY, targetEntity=AccountEntity.class)
    @JoinTable(name = "eventAttendees", joinColumns = {
        @JoinColumn(name = "eventId")
    },
    inverseJoinColumns = {
        @JoinColumn(name = "attendeeId")
    })
    private List<Account> attendees;
    @ManyToMany(fetch = FetchType.LAZY, targetEntity=DetailEntity.class)
    @JoinTable(name = "eventDetails", joinColumns = {
        @JoinColumn(name = "eventId")
    },
    inverseJoinColumns = {
        @JoinColumn(name = "detailId")
    })
    private List<Detail> details;
    @ManyToMany(fetch = FetchType.LAZY, targetEntity=RecommendationEntity.class)
    @JoinTable(name = "eventRecommendations", joinColumns = {
        @JoinColumn(name = "eventId")
    },
    inverseJoinColumns = {
        @JoinColumn(name = "recommendationId")
    })
    private List<Recommendation> recommendations;
    
    public EventEntity() {}
    
    public EventEntity(Event event){
        try
        {
            BeanUtils.copyProperties(this, event);
            System.out.println("this.title=" + this.title + "; event.title=" + event.getTitle());
        }
        catch (Exception e)
        {
            e.printStackTrace();;
        }
    }
    
    public Event transform() {
        Event event = new EventImpl();
        try {
            
            BeanUtils.copyProperties(event, this);
           
        } catch (Exception e) {
            e.printStackTrace();
        } 
         return event;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public List<Account> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<? extends Account> attendees) {
        this.attendees = (List<Account>) attendees;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<? extends Detail> details) {
        this.details = (List<Detail>) details;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<? extends Recommendation> recommendations) {
        this.recommendations = (List<Recommendation>) recommendations;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EventEntity other = (EventEntity) obj;
        if (this.title != other.title && (this.title == null || !this.title.equals(other.title))) {
            return false;
        }
        if (this.description != other.description && (this.description == null || !this.description.equals(other.description))) {
            return false;
        }
        if (this.startDate != other.startDate) {
            return false;
        }
        if (this.endDate != other.endDate) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 71 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 71 * hash + (int) (this.startDate ^ (this.startDate >>> 32));
        hash = 71 * hash + (int) (this.endDate ^ (this.endDate >>> 32));
        return hash;
    }
    
    
}
