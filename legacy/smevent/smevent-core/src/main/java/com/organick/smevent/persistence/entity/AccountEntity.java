/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.persistence.entity;

import com.organick.smevent.model.Account;
import com.organick.smevent.model.SocialMediaType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.apache.commons.beanutils.BeanUtils;

@Entity
@Table(name = "account")
public class AccountEntity implements Account, Serializable {

    @Id
    @Column(name="accountId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    @Enumerated
    private SocialMediaType socialMediaType;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String email;
    @Column
    private String cellPhone;
    
    public AccountEntity() {}
    
    public AccountEntity(Account account)
    {
        try
        {
            BeanUtils.copyProperties(this, account);
        }
        catch (Exception e)
        {
            e.printStackTrace();;
        }
    }
    
    /*@ManyToMany(mappedBy="attendees")
    private List<Event> events;

    public List<Event> getEvents() {
        if (events == null) {
            events = new ArrayList<Event>();
        }
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SocialMediaType getSocialMediaType() {
        return socialMediaType;
    }

    public void setSocialMediaType(SocialMediaType socialMediaType) {
        this.socialMediaType = socialMediaType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AccountEntity other = (AccountEntity) obj;
        if (this.socialMediaType != other.socialMediaType) {
            return false;
        }
        if (this.username != other.username && (this.username == null || !this.username.equals(other.username))) {
            return false;
        }
        if (this.password != other.password && (this.password == null || !this.password.equals(other.password))) {
            return false;
        }
        if (this.firstName != other.firstName && (this.firstName == null || !this.firstName.equals(other.firstName))) {
            return false;
        }
        if (this.lastName != other.lastName && (this.lastName == null || !this.lastName.equals(other.lastName))) {
            return false;
        }
        if (this.email != other.email && (this.email == null || !this.email.equals(other.email))) {
            return false;
        }
        if (this.cellPhone != other.cellPhone) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.socialMediaType != null ? this.socialMediaType.hashCode() : 0);
        hash = 53 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 53 * hash + (this.password != null ? this.password.hashCode() : 0);
        hash = 53 * hash + (this.firstName != null ? this.firstName.hashCode() : 0);
        hash = 53 * hash + (this.lastName != null ? this.lastName.hashCode() : 0);
        hash = 53 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 53 * hash + (this.cellPhone != null ? this.cellPhone.hashCode() : 0);
        return hash;
    }
    
    
}
