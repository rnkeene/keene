/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.model.impl;

import com.organick.smevent.model.Recommendation;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="recommendation")
public class RecommendationImpl implements Recommendation, Serializable {

    private long id;
    private String recommendation;
    /*@ManyToMany(mappedBy="recommendations", targetEntity=EventImpl.class)
    private List<Event> events;

    public List<Event> getEvents() {
        if (events == null) {
            events = new ArrayList<Event>();
        }
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }*/

    @XmlElement
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlElement
    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

   
    
}
