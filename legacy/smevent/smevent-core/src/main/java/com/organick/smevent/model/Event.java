/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.model;

import java.util.List;

/**
 *
 * @author nate
 */
public interface Event extends Base{

    public List<? extends Account> getAttendees();

    public void  setAttendees(List<? extends Account> attendees);

    public String getDescription();

    public void setDescription(String description);

    public List<? extends Detail> getDetails();

    public void setDetails(List<? extends Detail> details);

    public long getEndDate();

    public void setEndDate(long endDate);

    public List<? extends Recommendation> getRecommendations();

    public void setRecommendations(List<? extends Recommendation> recommendations);

    public long getStartDate();

    public void setStartDate(long startDate);

    public String getTitle();

    public void setTitle(String title);
}
