/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.model;

/**
 *
 * @author nate
 */
public interface Account extends Base {

    public String getCellPhone();

    public void setCellPhone(String cellPhone);

    public String getEmail();

    public void setEmail(String email);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public String getPassword();

    public void setPassword(String password);

    public SocialMediaType getSocialMediaType();

    public void setSocialMediaType(SocialMediaType socialMediaType);

    public String getUsername();

    public void setUsername(String username);
}
