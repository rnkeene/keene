/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.persistence.entity;

import com.organick.smevent.model.Event;
import com.organick.smevent.model.Recommendation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="recommendation")
public class RecommendationEntity implements Recommendation, Serializable {

    @Id
    @Column(name="recommendationId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String recommendation;
    /*@ManyToMany(mappedBy="recommendations", targetEntity=EventImpl.class)
    private List<Event> events;

    public List<Event> getEvents() {
        if (events == null) {
            events = new ArrayList<Event>();
        }
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RecommendationEntity other = (RecommendationEntity) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.recommendation != other.recommendation && (this.recommendation == null || !this.recommendation.equals(other.recommendation))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 19 * hash + (this.recommendation != null ? this.recommendation.hashCode() : 0);
        return hash;
    }

    
    
    
}
