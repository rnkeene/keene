/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.smevent.persistence.dao;

import com.organick.smevent.model.Account;
import com.organick.smevent.model.Event;

/**
 *
 * @author nate
 */
public interface EventDAO {
    public Event saveEvent(Event event);
    
    public Account createAccount(Account account);

    public Account authenticate(String username, String password);
}
