/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.model.impl;

import com.organick.smevent.model.Account;
import com.organick.smevent.model.Detail;
import com.organick.smevent.model.Event;
import com.organick.smevent.model.Recommendation;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="event")
public class EventImpl implements Event, 
        Serializable {

    
    private long id;
    private String title;
    private String description;
    private long startDate;
    private long endDate;
    private List<AccountImpl> attendees;
    private List<DetailImpl> details;
    private List<RecommendationImpl> recommendations;

    @XmlElement
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlElement
    public List<AccountImpl> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<? extends Account> attendees) {
        this.attendees = (List<AccountImpl>)attendees;
    }

    @XmlElement
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlElement
    public List<DetailImpl> getDetails() {
        return details;
    }

    public void setDetails(List<? extends Detail> details) {
        this.details = (List<DetailImpl>) details;
    }

    @XmlElement
    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    @XmlElement
    public List<RecommendationImpl> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<? extends Recommendation> recommendations) {
        this.recommendations = (List<RecommendationImpl>) recommendations;
    }

    @XmlElement
    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    @XmlElement
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
}
