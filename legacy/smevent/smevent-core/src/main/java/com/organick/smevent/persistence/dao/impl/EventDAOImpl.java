/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.smevent.persistence.dao.impl;

import com.organick.smevent.model.Account;
import com.organick.smevent.model.Event;
import com.organick.smevent.persistence.dao.EventDAO;
import com.organick.smevent.persistence.entity.AccountEntity;
import com.organick.smevent.persistence.entity.EventEntity;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 *
 * @author nate
 */
public class EventDAOImpl extends HibernateDaoSupport implements EventDAO {

    @Override
    public Event saveEvent(Event event) {
        System.out.println("event: " + event.getTitle());
        EventEntity entity = new EventEntity(event);
        Long eventId = (Long) getHibernateTemplate().save(entity);
        event.setId(eventId);
        return event;
    }

    @Override
    public Account createAccount(Account account) {
        AccountEntity entity = new AccountEntity(account);
        Long accountId = (Long) getHibernateTemplate().save(entity);
        account.setId(accountId);
        return account;
    }

    @Override
    public Account authenticate(String username, String password) {
        AccountEntity account = new AccountEntity();
        account.setUsername(username);
        account.setPassword(password);

        System.out.println(username);
        System.out.println(password);

        Session session = SessionFactoryUtils.getSession(getSessionFactory(), true);
        Criteria criteria = session.createCriteria(AccountEntity.class);
        criteria.add(Example.create(account));
        List<Account> accounts = criteria.list();
        System.out.println("Account List Size: " + accounts.size());
        if (accounts.size() == 1) {
            return accounts.get(0);
        }

        return null;
    }
    
}
