DROP TABLE IF EXISTS account ;
CREATE TABLE account (
    accountId bigint NOT NULL AUTO_INCREMENT,
    socialMediaType character varying(250) NOT NULL,
    username character varying(250) NOT NULL,
    password character varying(250) NOT NULL,
    firstName character varying(250) NOT NULL,
    lastName character varying(250) NOT NULL,
    email character varying(250) NOT NULL,
    cellPhone character varying(100),
    PRIMARY KEY (accountId)
);

DROP TABLE IF EXISTS roles;
CREATE TABLE roles (
    username character varying(250) NOT NULL,
    role character varying(250) NOT NULL,
    PRIMARY KEY (username, role)
);

DROP TABLE IF EXISTS detail;
CREATE TABLE detail (
    detailId bigint NOT NULL AUTO_INCREMENT,
    detail character varying(250) NOT NULL,
    PRIMARY KEY (detailId)
);

DROP TABLE IF EXISTS recommendation;
CREATE TABLE recommendation (
    recommendationId bigint NOT NULL AUTO_INCREMENT,
    recommendation character varying(250) NOT NULL,
    PRIMARY KEY (recommendationId)
);

DROP TABLE IF EXISTS event;
CREATE TABLE event (
    eventId bigint NOT NULL AUTO_INCREMENT,
    title character varying(250) NOT NULL,
    description character varying(250) NOT NULL,
    startDate bigint NOT NULL,
    endDate bigint NOT NULL,
    PRIMARY KEY (eventId)
);

DROP TABLE IF EXISTS eventAccounts;
CREATE TABLE eventAccounts (
    eventAccountsId bigint NOT NULL AUTO_INCREMENT,
    eventId bigint NOT NULL,
    accountId bigint NOT NULL,
    FOREIGN KEY (eventId) REFERENCES event (eventId),
    FOREIGN KEY (accountId) REFERENCES account (accountId),
    PRIMARY KEY (eventAccountsId)
);

DROP TABLE IF EXISTS eventDetails;
CREATE TABLE eventDetails (
    eventDetailsId bigint NOT NULL AUTO_INCREMENT,
    eventId bigint NOT NULL,
    detailId bigint NOT NULL,
    FOREIGN KEY (eventId) REFERENCES event (eventId),
    FOREIGN KEY (detailId) REFERENCES detail (detailId),
    PRIMARY KEY (eventDetailsId)
);

DROP TABLE IF EXISTS eventRecommendations;
CREATE TABLE eventRecommendations (
    eventRecommendationsId bigint NOT NULL AUTO_INCREMENT,
    eventId bigint NOT NULL,
    recommendationId bigint NOT NULL,
    FOREIGN KEY (eventId) REFERENCES event (eventId),
    FOREIGN KEY (recommendationId) REFERENCES recommendation (recommendationId),
    PRIMARY KEY (eventRecommendationsId)
);