$(function()
{ 
    $('#form').submit(function(e){
        e.preventDefault();
        
        var url = "/smevent/services/event/"
                + $('#title').val() + "/" 
                + $('#description').val();
        $.ajax({
            url: url,
            type: "POST",
            success: function(data) {
                $('#feedback').html("Event saved succesfully");
            }
        });
            
    });
});