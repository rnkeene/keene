$(function(){
    $('#form').submit(function(e){
        e.preventDefault();
        var pwEncrypt = $().crypt({
            method: 'md5',
            source: $('#password').val()
        });

        var url = "/smevent/services/authenticate/" + $('#username').val() + "/" + pwEncrypt;
        $.ajax({
            url: url,
            success: function(data){
                location.href="/secure/Home.html";
            },
            statusCode: {401: function(){
                location.href="/Login.html?invalid";
            }}
       });
    });
});
