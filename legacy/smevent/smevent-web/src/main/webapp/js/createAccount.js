$(function()
{ 
    $('#form').submit(function(e){
        e.preventDefault();
        
        var pwEncrypt = $().crypt({
            method: 'md5',
            source: $('#password').val()
        });
        
        var url = "/smevent/services/account/" 
                + $('#firstName').val() + "/" 
                + $('#lastName').val() + "/"
                + $('#email').val() + "/"
                + $('#cellPhone').val() + "/"
                + $('#username').val() + "/"
                + pwEncrypt;
        $.ajax({
            url: url,
            type: "POST",
            success: function(data) {
                location.href="Home.html";
            }
        });
            
    });
});