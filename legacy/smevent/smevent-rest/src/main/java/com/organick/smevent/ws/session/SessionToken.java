/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.smevent.ws.session;

import com.organick.smevent.model.Account;
import java.util.Date;

/**
 *
 * @author matt
 */
public class SessionToken {

    private long lastActivity;
    private Account account;

    public SessionToken() {
        lastActivity = new Date().getTime();
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public long getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }

}
