/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.smevent.ws;

import com.organick.smevent.model.Account;
import com.organick.smevent.model.Event;
import com.organick.smevent.model.impl.AccountImpl;
import com.organick.smevent.model.impl.EventImpl;
import com.organick.smevent.ws.session.SessionToken;
import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.annotations.ClientResponseType;

/**
 *
 * @author nate
 */
@Path("services")
public class SMEventREST extends BaseService {
    
    @POST
    @Path("/event/{title}/{description}")
    @Produces("application/json")
    @ClientResponseType(entityType=Event.class)
    public Response createEvent(@PathParam("title") String title,
            @PathParam("description") String description)
    {
        System.out.println("Principal: " + context.getUserPrincipal().getName());
        //System.out.println("Principal class: " + p.getClass());
        System.out.println(request.getSession().getId());

        Event event = new EventImpl();
        event.setTitle(title);
        event.setDescription(description);
        event = eventDAO.saveEvent(event);
        return Response.ok(event).build();
    }
    
    @POST
    @Path("/account/{firstName}/{lastName}/{email}/{cell}/{username}/{password}")
    @Produces("application/json")
    @ClientResponseType(entityType=Account.class)
    public Response createAccount(@PathParam("firstName") String firstName,
            @PathParam("lastName") String lastName,
            @PathParam("email") String email,
            @PathParam("cell") String cell,
            @PathParam("username") String username,
            @PathParam("password") String password)
    {
        Account account = new AccountImpl();
        account.setFirstName(firstName);
        account.setLastName(lastName);
        account.setEmail(email);
        account.setCellPhone(cell);
        account.setUsername(username);
        account.setPassword(password);
        account = eventDAO.createAccount(account);
        return Response.ok(account).build();
    }

    @GET
    @Path("authenticate/{username}/{password}")
    @Produces("text/plain")
    @ClientResponseType(entityType=String.class)
    public Response authenticate(@PathParam("username") String username, @PathParam("password") String password)
    {
        System.out.println(request.getSession().getId());
        Account account = eventDAO.authenticate(username, password);
        if (account != null)
        {
            System.out.println("Found an account!");
            SessionToken token = new SessionToken();
            token.setAccount(account);
            sessionMap.put(request.getSession().getId(), token);
            return Response.ok(request.getSession().getId()).build();
        }

        return Response.status(401).build();
    }

    @GET
    @Path("validate")
    @Produces("text/plain")
    @ClientResponseType(entityType=String.class)
    public Response validate()
    {
        System.out.println(request.getSession().getId());
        SessionToken token = sessionMap.get(request.getSession().getId());
        if (token != null)
        {
            long currTime = new Date().getTime();
            if ((currTime - token.getLastActivity()) <= (20*60*1000))
            {
                token.setLastActivity(currTime);
                return Response.ok("true").build();
            }
            else
            {
                sessionMap.remove(request.getSession().getId());
            }
        }

        return Response.status(401).build();
    }
}
