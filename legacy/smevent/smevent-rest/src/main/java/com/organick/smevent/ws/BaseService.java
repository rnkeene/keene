/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.smevent.ws;

import com.organick.smevent.persistence.dao.EventDAO;
import com.organick.smevent.ws.session.SessionToken;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 *
 * @author nate
 */
public class BaseService {

    protected static EventDAO eventDAO;
    
    @Context
    protected HttpServletRequest request;
    @Context
    protected SecurityContext context;

    protected static Hashtable<String, SessionToken> sessionMap;

    static {
        try
        {
            XmlBeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource(
                    "beans.xml"));

            System.out.println(beanFactory.toString());
            eventDAO = (EventDAO) beanFactory.getBean("eventDAO");

            sessionMap = new Hashtable<String, SessionToken>();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
