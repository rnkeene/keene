package com.keene.mepbm.natural.science.chemestry;

import com.keene.mepbm.natural.science.physics.Particle;
import java.util.List;

public interface Atom<P extends Particle> extends List<P> {
}
