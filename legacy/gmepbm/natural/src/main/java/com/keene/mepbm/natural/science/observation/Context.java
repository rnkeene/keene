package com.keene.mepbm.natural.science.observation;

public interface Context extends Name {

    public String getCurrentContextName();

    public String getParentContextName();
}
