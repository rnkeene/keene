/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.mepbm.combatsimulator.constants;

/**
 *
 * @author Galen
 */
public enum Terrain {
    
    SHORE, PLAINS, FOREST, ROUGH, MOUNTAIN, COASTAL_WATER, SEAS, SWAMP, DESERT;

}
