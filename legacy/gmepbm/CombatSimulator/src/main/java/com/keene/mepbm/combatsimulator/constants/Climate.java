/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.mepbm.combatsimulator.constants;

/**
 *
 * @author Galen
 */
public enum Climate {
    
    HOT, WARM, MILD, COOL, COLD, SEVERE, POLAR;

}
