/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.mepbm.combatsimulator.troops;

/**
 *
 * @author Galen
 */
public class Weapons {

    private int weaponRank;

    public Weapons(int weaponRank) {
        this.weaponRank = weaponRank;
    }

    public int getWeaponRank() {
        return weaponRank;
    }

    public void setWeaponRank(int weaponRank) {
        this.weaponRank = weaponRank;
    }
}
