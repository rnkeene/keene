/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.mepbm.combatsimulator.troops;

/**
 *
 * @author Galen
 */
public class Armor {

    private int armorRank;

    public Armor(int armorRank) {
        this.armorRank = armorRank;
    }

    public int getArmorRank() {
        return armorRank;
    }

    public void setArmorRank(int armorRank) {
        this.armorRank = armorRank;
    }
}
