/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.keene.mepbm.combatsimulator.characters;

/**
 *
 * @author Galen
 */
public class Artifacts {
    
    private int combatDamage;
    private int commandRankModifier;

    public int getCombatDamage() {
        return combatDamage;
    }

    public void setCombatDamage(int combatDamage) {
        this.combatDamage = combatDamage;
    }

    public int getCommandRankModifier() {
        return commandRankModifier;
    }

    public void setCommandRankModifier(int commandRankModifier) {
        this.commandRankModifier = commandRankModifier;
    }

    
}
