/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keene.mepbm.combatsimulator.troops;

/**
 *
 * @author Galen
 */
public class Training {

    private int trainingRank;

    public Training(int trainingRank) {
        this.trainingRank = trainingRank;
    }

    public int getTrainingRank() {
        return trainingRank;
    }

    public void setTrainingRank(int trainingRank) {
        this.trainingRank = trainingRank;
    }
}
