package com.cisco.swtg.sasi.dao;

import java.util.List;
import java.util.Map;

public interface SmartFilterDao {

	Map<String, String> getUserFilterSetting(String userId);
	
	List<Map<String, Object>> searchForTechSubtech(String searchContent, Map<String, String> filterSetting);
	
	void updateSmartFilterSettings(String userId, Map<String, String> newFilterSetting);
}
