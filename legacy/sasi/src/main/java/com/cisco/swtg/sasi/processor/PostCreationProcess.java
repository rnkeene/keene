package com.cisco.swtg.sasi.processor;

import java.util.Map;

public interface PostCreationProcess {
	void execute(String newServiceRequestNumber,  Map<String, String> statusObject) throws Exception;
}
