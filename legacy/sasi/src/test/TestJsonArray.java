package com;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class TestJsonArray {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			JSONArray array = new JSONArray("[{filename:'test'}, {filename:'another'}]");

			for(int i= 0 ; i < array.length(); i++) {
				JSONObject obj = array.optJSONObject(i);
				System.out.println(obj.optString("filename"));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
