package com;

import java.io.IOException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.cisco.swtg.sasi.web.controller.fileservice.FileBrokerServiceController;
import com.cisco.swtg.sasi.web.controller.requestproxy.CachedProxyServiceController;
import com.cisco.swtg.sasi.web.controller.requestproxy.ProxyGetServiceController;
import com.cisco.swtg.sasi.web.controller.technologyfilter.TechnologyFilterServiceController;


public class TestFileBrokerService {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
//		MockHttpServletRequest httpRequest = new MockHttpServletRequest();
//		MockHttpServletResponse httpResponse = new MockHttpServletResponse();
//		FileBrokerServiceController controller = new FileBrokerServiceController();
//		controller.setTenantCode("TAC");
//		controller.setTenantSubCode("SASI");
//		controller.setTenantSubscriptionKey("A62E12FB3784583F82220872A7BE7597");
//		controller.setUsername("sasi.gen");
//		controller.setPassword("welcome");
//		controller.setWsgURL("https://wsgx-dev.cisco.com/csc/broker/storage/");
//		clientIPAddress	null
//		dojo.preventCache	1289239324212
//		fileCategory	LOG_FILE
//		fileDesc	this is a test
//		fileName	dojopath.txt
//		tenantSourceKey	TR1098290822
//		userEmail	tpaultai@cisco.com
//		userName	tpaultai
//		<categoryInfo>
//        <categoryName>log_values</categoryName>
//        <categoryValue>Log/Trace File</categoryValue>
//    </categoryI
// <categoryInfo>
//        <categoryName>crash_core</categoryName>
//        <categoryValue>Crash/Core Dump</categoryValue>
//    </categoryInfo>
//    <categoryInfo>
//        <categoryName>debug output</categoryName>
//        <categoryValue>Debug Output</categoryValue>
//    </categoryInfo>
//    <categoryInfo>
//        <categoryName>diagram</categoryName>
//        <categoryValue>Diagram</categoryValue>
//    </categoryInfo>
//    <categoryInfo>
//        <categoryName>error_message</categoryName>
//        <categoryValue>Error Message</categoryValue>
//    </categoryInfo>
//    <categoryInfo>
//        <categoryName>log_values</categoryName>
//        <categoryValue>Log/Trace File</categoryValue>
//    </categoryInfo>
//    <categoryInfo>
//        <categoryName>other</categoryName>
//        <categoryValue>Other</categoryValue>
//    </categoryInfo>
//    <categoryInfo>
//        <categoryName>packet capt</categoryName>
//        <categoryValue>Packet Capture</categoryValue>
//    </categoryInfo>
//    <cat

		
		ApplicationContext applicationContext = 
			new FileSystemXmlApplicationContext(
					new String[]{"/WebContent/WEB-INF/applicationContext.xml",
							"/WebContent/WEB-INF/jsonService-servlet.xml"});

//		ProxyGetServiceController controller = (ProxyGetServiceController) applicationContext.getBean("proxyGetController");
//		
//		CachedProxyServiceController controller1 = (CachedProxyServiceController) applicationContext.getBean("cachedProxyController");
		
		FileBrokerServiceController controller = (FileBrokerServiceController) applicationContext.getBean("fileBrokerServiceController");
		
		
		MockHttpServletRequest httpRequest = new MockHttpServletRequest();
		MockHttpServletResponse httpResponse = new MockHttpServletResponse();
		httpRequest.setPathInfo("/requestproxy/get/sasi/ws/rest/entitlement/contract/CcoId/berne/LoggedId/PRJAGTAP?dojo.preventCache=1289441811187");
//		httpRequest.setPathInfo("/requestproxy/cache/sasi/ws/rest/util/timezone");
		
		httpRequest.setParameter("fileCategory", "debug output");
		httpRequest.setParameter("fileDesc", "this is a test");
		httpRequest.setParameter("fileName", "dojopath.txt");
		httpRequest.setParameter("tenantSourceKey", "12898794387431");
		httpRequest.setParameter("userEmail", "tpaultai@cisco.com");
		httpRequest.setParameter("userName", "tpaultai");
		System.out.println("About to run");
		controller.getUploadProperties(httpRequest, httpResponse);
		httpRequest = new MockHttpServletRequest();
		httpResponse = new MockHttpServletResponse();
		httpRequest.setParameter("fileCategory", "log_values");
		httpRequest.setParameter("fileDesc", "this is a test 2");
		httpRequest.setParameter("fileName", "file.zip");
		httpRequest.setParameter("tenantSourceKey", "12898794387431");
		httpRequest.setParameter("userEmail", "tpaultai@cisco.com");
		httpRequest.setParameter("userName", "tpaultai");
		System.out.println("About to run");
		controller.getUploadProperties(httpRequest, httpResponse);
		httpRequest = new MockHttpServletRequest();
		httpResponse = new MockHttpServletResponse();
		httpRequest.setParameter("tenantSourceKey", "12898794387431");
		httpRequest.setParameter("userEmail", "tpaultai@cisco.com");
		httpRequest.setParameter("userName", "tpaultai");
		controller.getUploadedFileList(httpRequest, httpResponse);
		//
		
//		httpRequest.setPathInfo("");
//		controller.handleRequestInternal(httpRequest, httpResponse);
		
		System.out.println("response --" + httpResponse.getContentAsString());
		//		System.out.println(controller.getServiceUrl());

	}

}
