/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.googlecharts.visualization.script;

/**
 *
 * @author nkeene
 */
public interface LoadableStylesheet {

    public String getLinkElement();

    public void setHref(String href);

}
