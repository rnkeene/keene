/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.googlecharts.visualization.script;

/**
 *
 * @author nkeene
 */
public interface LoadableScript {

    public String getScriptElement();

    public void setSrc(String src);

}
