/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.googlecharts.base;

/**
 *
 * @author nkeene
 */
public interface Column {

    public String getColumnName();

    public void setColumnName(String columnName);

    public String getColumnType();

    public void setColumnType(String columnType);
}
