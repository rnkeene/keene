package com.organick.guitartabber.ws;

import com.organick.guitartabber.model.ChordList;
import com.organick.guitartabber.model.Song;
import com.organick.guitartabber.model.SongList;
import com.organick.guitartabber.model.TabAccount;
import com.organick.guitartabber.model.TuningList;
import com.organick.guitartabber.model.utils.AccountUtils;
import com.organick.guitartabber.persistence.GuitarTabberPersist;
import com.organick.guitartabber.ws.response.TabCreatorResponse;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.ClientResponseType;

import com.organick.guitartabber.ws.session.SessionToken;
import org.apache.commons.lang.StringUtils;

@Path("act")
public class OGTabRest extends BaseService {

    @POST
    @Path("/song/create")
    @Produces("application/json")
    @ClientResponseType(entityType = Song.class)
    public Response createSong() {
        System.out.println("OGTabRest.createSong()");
        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null) {
            String artist = request.getParameter("artist");
            String title = request.getParameter("title");
            String notes = request.getParameter("notes");
            if (!StringUtils.isEmpty(artist) && !StringUtils.isEmpty(title)) {
                TabAccount account = token.getTabAccount();
                Song song = new Song();
                song.setArtist(artist);
                song.setTitle(title);
                song.setNotes(notes);
                account.addSong(song);
                try {
                    GuitarTabberPersist.updateAccount(account);
                    token.setCurrentSong(song);
                    return Response.ok(song).build();
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(404).build();
                }
            }

            return Response.status(405).build();
        }

        return Response.status(401).build();
    }

    /*@GET
    @Path("/songs")
    //@Produces("application/xml")
    @Produces("application/json")
    @ClientResponseType(entityType = SongList.class)
    public Response getSongs()
    {
    Response response = null;

    try
    {
    List<Song> songs = guitarTabberDAO.getAllSongs();
    SongList songsList = new SongList();
    songsList.getSongs().addAll(songs);

    response = Response.ok(songsList).build();
    }
    catch (Exception e)
    {
    e.printStackTrace();
    response = Response.status(404).build();
    }

    return response;
    }*/
    @GET
    @Path("/songs/user")
    //@Produces("application/xml")
    @Produces("application/json")
    @ClientResponseType(entityType = SongList.class)
    public Response getSongsFromUser() {
        Response response = null;

        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null) {
            try {
                SongList songsList = new SongList();
                songsList.getSongs().addAll(AccountUtils.getSongs(token.getAccount()));

                response = Response.ok(songsList).build();
            } catch (Exception e) {
                e.printStackTrace();
                response = Response.status(404).build();
            }

            return response;
        }

        return Response.status(401).build();
    }

    @POST
    @Path("/tabCreator")
    @Produces("application/json")
    @ClientResponseType(entityType=TabCreatorResponse.class)
    public Response getTabCreatorResponse() throws Exception{
        SessionToken token = validateSession();
        if (token != null && token.getCurrentSong() != null) {
            TabCreatorResponse response = new TabCreatorResponse();
            response.setSong(token.getCurrentSong());

            TuningList tunings = GuitarTabberPersist.getTunings();
            response.getTunings().addAll(tunings.getTunings());

            ChordList chords = GuitarTabberPersist.getChords();
            response.getChords().addAll(chords.getChords());
            return Response.ok(response).build();
        }

        return Response.status(401).build();
    }

    @GET
    @Path("/song")
    @Produces("application/json")
    @ClientResponseType(entityType = Song.class)
    public Response getSongFromSession() {
        SessionToken token = validateSession();
        if (token != null && token.getCurrentSong() != null) {
            return Response.ok(token.getCurrentSong()).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("/profile")
    @Produces("text/plain")
    public Response updateProfile() throws Exception {
        System.out.println("OGTabRest.updateProfile()");
        SessionToken token = validateSession();
        if (token != null && token.getTabAccount() != null) {
            String firstname = request.getParameter("firstname");
            String lastname = request.getParameter("lastname");
            String email = request.getParameter("email");
            token.getTabAccount().setFirstName(firstname);
            token.getTabAccount().setLastName(lastname);
            token.getTabAccount().setEmail(email);
            GuitarTabberPersist.updateAccount(token.getTabAccount());
            return Response.ok("success").build();
        }

        return Response.status(401).build();
    }

    @GET
    @Path("/song/{id}")
    @Produces("application/json")
    @ClientResponseType(entityType = Song.class)
    public Response setSongForEditing(@PathParam("id") Long songId) {
        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null) {
            try {
                Song song = AccountUtils.getSongs(token.getAccount()).get(songId.intValue());
                token.setCurrentSong(song);
                return Response.ok(song).build();
            } catch (Exception e) {
                return Response.status(404).build();
            }
        }

        return Response.status(401).build();
    }

    @GET
    @Path("/user")
    @Produces("application/json")
    @ClientResponseType(entityType = TabAccount.class)
    public Response getUser() {
        SessionToken token = validateSession();
        if (token != null && token.getTabAccount() != null) {
            return Response.ok(token.getTabAccount()).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("/save")
    @Produces("text/plain")
    @ClientResponseType(entityType = String.class)
    public Response updateSong(String body) {
        SessionToken token = validateSession();
        if (token != null && token.getCurrentSong() != null) {
            try {
                Song song = (Song) token.getCurrentSong();
                song.setBodyText(body);
                AccountUtils.getSongs(token.getAccount()).set(song.getId().intValue(), song);
                GuitarTabberPersist.updateAccount(token.getTabAccount());
                return Response.ok("<span style='float: left; margin-right: 0.3em;' class='ui-icon ui-icon-check'></span> Song saved succesfully!").build();
            } catch (Exception e) {
                return Response.status(404).build();
            }
        }

        return Response.status(401).build();
    }

    @POST
    @Path("/tunings")
    @Produces("application/json")
    public Response getTunings() throws Exception
    {
        SessionToken token = validateSession();
        if (token != null && token.getCurrentSong() != null)
        {
            TuningList tunings = GuitarTabberPersist.getTunings();
            return Response.ok(tunings).build();
        }

        return Response.status(401).build();
    }

    /*@GET
    @Path("/initTabExtras")
    @Produces("text/plain")
    public Response initTabExtras() throws Exception{
        try
        {
            OGTabSeedData.init();
            return Response.ok("Success!").build();
        } catch (Exception e){
            e.printStackTrace();
            return Response.ok("ERROR!").build();
        }
    }*/

    @POST
    @Path("/logout")
    @Produces("text/plain")
    public Response logOut()
    {
        invalidateSession();
        return Response.ok().build();
    }
}
