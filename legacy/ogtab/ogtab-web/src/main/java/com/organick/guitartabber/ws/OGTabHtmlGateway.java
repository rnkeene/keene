/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.guitartabber.ws;

import com.organick.guitartabber.ws.session.SessionToken;
import com.organick.ogsecurity.js.ReadFile;
import java.util.HashMap;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author matt
 */
@Path("serve")
public class OGTabHtmlGateway extends BaseService {

    @POST
    @Path("/updateProfile.html")
    @Produces("text/html")
    public Response updateProfile() {
        System.out.println("entering OGTabHtmlGateway.updateProfile()");
        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null)
        {
            return Response.ok(ReadFile.readForm("updateProfile.html", this.getClass().getClassLoader(), token.getTabAccount().getUpdateProfileProps())).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("/newTab.html")
    @Produces("text/html")
    public Response newTab() {
        System.out.println("entering OGTabHtmlGateway.updateProfile()");
        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null)
        {
            return Response.ok(ReadFile.readFileFromClasspath("newTab.html", this.getClass().getClassLoader())).build();
        }

        return Response.status(401).build();
    }

    @POST
    @Path("/chordDialog.html")
    @Produces("text/html")
    public Response chordDialog() {
        System.out.println("entering OGTabHtmlGateway.chordDialog()");
        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null)
        {
            return Response.ok(ReadFile.readFileFromClasspath("chordDialog.html", this.getClass().getClassLoader())).build();
        }

        return Response.status(401).build();
    }

    @GET
    @Path("/tabCreator.html")
    @Produces("text/html")
    public Response tabCreator() {
        System.out.println("entering OGTabHtmlGateway.tabCreator()");
        SessionToken token = validateSession();
        if (token != null && token.getAccount() != null)
        {
            return Response.ok(ReadFile.readFileFromClasspath("tabCreator.html", this.getClass().getClassLoader())).build();
        }

        return Response.status(401).build();
    }
}
