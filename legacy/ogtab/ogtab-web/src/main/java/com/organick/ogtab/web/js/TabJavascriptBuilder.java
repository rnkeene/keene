/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogtab.web.js;

import com.organick.ogsecurity.OGToken.LoginState;
import com.organick.ogsecurity.client.OGClientToken;
import com.organick.ogsecurity.js.JavascriptUtil;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 *
 * @author matt
 */
@Path("ident")
public class TabJavascriptBuilder {

    @Context
    HttpServletRequest request;

    @Context
    ServletContext context;

    private static final String BASE_URL = "/ogtab/ident/";

    @GET
    @Path("home.js")
    @Produces("text/javascript")
    public Response getHomePageJS() throws Exception {
        System.out.println("entering /ident/home.js on ogtab");
        HttpSession session = request.getSession(true);
        System.out.println("\tsession ID: " + session.getId());
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        System.out.println("\ttoken: " + token);
        StringBuilder builder = new StringBuilder();
        if (token != null && token.getState().equals(LoginState.AUTHENTICATED)) {
            System.out.println("\tsession state: " + token.getState());
            builder.append("function setSong(id) {");
            builder.append(JavascriptUtil.buildLoadJS("'" + BASE_URL + "song/' + id", "location.href = '/ogsecurity/s/guitartabber/secure/TabCreator.html';", null));
            builder.append("}");

            builder.append("function buildSongLink(song) {");
            builder.append("var results = \"div class='songLink'><div>\" + song.title + \" by \" + ");
            builder.append("song.artist + \" <a href='#' id='editLink' onclick='setSong(\\\"\"");
            builder.append("+ song.id + \"\\\")'>[Edit]</a> <a href='#'>[View]</a>\"");
            builder.append("+ \"</div></div>\";");
            builder.append("return results;");
            builder.append("}");

            builder.append("$(function(){");
                StringBuilder success = new StringBuilder();
                success.append("var results = \"<fieldset><legend>Your created tabs</legend>\";");
                success.append("if (data.songList.songs.length > 1) {");
                success.append("$.each(data.songList.songs, function(key, val) {");
                success.append("results += buildSongLink(val);");
                success.append("});");
                success.append("} else {");
                success.append("results += buildSongLink(data.songList.songs);");
                success.append("}");
                success.append("results += \"</fieldset>\";");
                success.append("$(\"#songs\").append(results);");
            builder.append(JavascriptUtil.buildLoadJS(BASE_URL + "songs/user", success.toString(), null));
            
                success = new StringBuilder();
                success.append("var welcome = \"<h2>Welcome \" + data.account.firstName + \" \"");
                success.append("+ data.account.lastName + \"</h2>\";");
                success.append("$('#welcome').html(welcome);");
            builder.append(JavascriptUtil.buildLoadJS(BASE_URL + "user", success.toString(), null));

            builder.append("$('#songLink').hover(");
            builder.append("function(e) {");
            builder.append("$('#' + e.target.parentNode.id).css(\"background-color\",\"yellow\");");
            builder.append("},");
            builder.append("function(e) {");
            builder.append("$('#' + e.target.parentNode.id).css(\"background-color\",\"white\");");
            builder.append("});");

            builder.append("$('#logOut').click(function(){");
            builder.append(JavascriptUtil.buildLoadJS(BASE_URL + "logout", "location.href='/ogsecurity/s/guitartabber/Login.html';", null));

            builder.append("});");
            builder.append("});");

            System.out.println("leaving /ident/home.js on ogtab - built real js");
            System.out.println();
            return Response.ok(builder.toString()).build();
        }

        System.out.println("leaving /ident/home.js on ogtab - built reload js");
        System.out.println();
        //return Response.ok(JavascriptUtil.buildReloadScript("/ogtab/ident/home.js")).build();
        return Response.ok().build();
    }
}
