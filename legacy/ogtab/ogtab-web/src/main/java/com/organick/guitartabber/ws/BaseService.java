package com.organick.guitartabber.ws;


import javax.servlet.http.HttpServletRequest;

import com.organick.guitartabber.model.TabAccount;
import com.organick.guitartabber.ws.session.SessionToken;
import com.organick.ogsecurity.account.Account;
import com.organick.ogsecurity.client.OGClientToken;
import com.organick.persist.Persist;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

public class BaseService
{
    @javax.ws.rs.core.Context 
    protected HttpServletRequest request;

    @javax.ws.rs.core.Context
    protected ServletContext context;
    
    protected SessionToken validateSession()
    {
        System.out.println("entering BaseService.validateSession()");
        HttpSession session = request.getSession(true);

        SessionToken token = checkSessionToken(session.getId());
        if (token != null)
        {
            long currTime = new Date().getTime();
            if ((currTime - token.getLastActivity()) <= (20*60*1000))
            {
                token.setLastActivity(currTime);
                return token;
            }
            else
            {
                context.setAttribute(session.getId(), null);
            }
        }
    	
        return null;
    }

    private SessionToken checkSessionToken(String sessionId)
    {
        System.out.println("entering BaseService.checkSessionToken()");
        OGClientToken token = (OGClientToken) context.getAttribute(sessionId);//sessionMap.get(request.getSession().getId());
        /**
         * Checking if the token on the ServletContext has been transformed into a OGTab SessionToken
         */
        if (token != null) {
            System.out.println("\tfound a token");
            if (!(token instanceof SessionToken)) {
                System.out.println("\tfound a non-SessionToken");
                try {
                    String username = token.getUsername();
                    Account account = null;
                    if (username != null) {
                        account = Persist.get(username.trim(), Account.class.getName());
                    }

                    //Account account = token.getAccount();
                    SessionToken sessionToken = new SessionToken(token);
                    System.out.println("\taccount: " + account);
                    if (account != null) {
                        /*if (!(account instanceof TabAccount)) {
                            sessionToken.setAccount(TabAccount.transform(account));
                            System.out.println("generated a new TabAccount: " + sessionToken.getAccount().toString());
                        }*/
                        sessionToken.setAccount(new TabAccount(account));

                    } else {
                        /*System.out.println("\tno account on the token");
                        String username = Post.excutePost(Constants.VERIFY_USERNAME + "/" + token.getUserClientId());
                        System.out.println("\tusername retrieved from server: '" + username.trim() + "'");
                        if (account != null)
                        {
                            account = Persist.get(username.trim(), "com.organick.ogsecurity.jaas.Accounttype");
                            System.out.println(account);
                            token.setAccount(account);
                        }
                        else
                        {*/
                            System.out.println("\tERROR: Couldn't find the username that was fetched from the server in the database");
                            return null;
                        //}
                    }
                    context.setAttribute(sessionId, sessionToken);
                    return sessionToken;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            } else {

                return (SessionToken)token;
            }
        }

        return null;
    }
    
    protected void invalidateSession()
    {
    	context.setAttribute(request.getSession(true).getId(), null);
    }
    
    protected String buildSessionToken(TabAccount account)
    {
    	SessionToken token = new SessionToken();
        token.setAccount(account);
        HttpSession session = request.getSession(true);
        String sessionId = session.getId();
        context.setAttribute(session.getId(), token);
        System.out.println("buildSessionToken() sessionId: " + sessionId);
        return sessionId;
    }
}