/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.guitartabber.ws.session;

import java.util.Date;

import com.organick.guitartabber.model.Song;
import com.organick.guitartabber.model.TabAccount;
import com.organick.ogsecurity.OGToken;
import com.organick.ogsecurity.account.Account;
import com.organick.ogsecurity.client.OGClientToken;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author matt
 */
public class SessionToken extends OGClientToken {

    private long lastActivity;
    private TabAccount account;
    private Song currentSong;
    
    public SessionToken() {
        lastActivity = new Date().getTime();
    }

    public SessionToken(OGToken token) {
        try {
            BeanUtils.copyProperties(this, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastActivity = new Date().getTime();
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(Song currentSong) {
        this.currentSong = currentSong;
    }

    public TabAccount getTabAccount() {
        return account;
    }

    public Account getAccount() {
        return account.getAccount();
    }

    public void setAccount(TabAccount account) {
        this.account = account;
    }

    public long getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }
}
