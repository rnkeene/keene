/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.guitartabber.ws.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 *
 * @author matt
 */
public class OGTabFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        /*System.out.println("In OGTabFilter doFilter()");

        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession(true);

        //session.getServletContext().getAttribute("");
        OGClientToken token = (OGClientToken) session.getServletContext().getAttribute(session.getId());//sessionMap.get(request.getSession().getId());
        System.out.println("\tsessionId: " + request.getSession().getId());
        if (token != null) {
            System.out.println("\tfound a token");
            System.out.println(token);
            if (!(token instanceof SessionToken)) {
                System.out.println("\tfound a non-SessionToken");
                try {
                    Account account = token.getAccount();
                    SessionToken sessionToken = new SessionToken(token);
                    System.out.println("\taccount: " + account);
                    if (account != null) {
                        if (!(account instanceof TabAccount)) {
                            sessionToken.setAccount(TabAccount.transform(account));
                            System.out.println("generated a new TabAccount: " + sessionToken.getAccount().toString());
                        }
                        
                    } else {
                        System.out.println("\tno account on the token");
                        String username = Post.excutePost(Constants.VERIFY_USERNAME + "/" + token.getUserClientId());
                        System.out.println("\tusername retrieved from server: '" + username.trim() + "'");
                        account = Persist.get(username.trim(), Account.class.getName());
                        System.out.println(account);
                        sessionToken.setAccount(account);
                    }
                    session.getServletContext().setAttribute(session.getId(), sessionToken);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        System.out.println("leaving OGTabFilter doFilter()");
        System.out.println();*/
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {

    }


}
