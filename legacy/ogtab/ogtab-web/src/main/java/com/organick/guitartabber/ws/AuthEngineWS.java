package com.organick.guitartabber.ws;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.annotations.ClientResponseType;

import com.organick.guitartabber.model.TabAccount;
import com.organick.guitartabber.persistence.GuitarTabberPersist;
import com.organick.guitartabber.ws.session.SessionToken;

@Path("services")
public class AuthEngineWS extends BaseService
{
    /*@GET
    @Path("/account/{firstName}/{lastName}/{email}/{username}/{password}")
    @Produces("application/json")
    @ClientResponseType(entityType=String.class)
    public Response createAccount(@PathParam("username") String username, @PathParam("password") String password,
            @PathParam("firstName") String firstName, @PathParam("lastName") String lastName, @PathParam("email") String email)
    {
        TabAccount account = new TabAccount();
        account.setFirstName(firstName);
        account.setLastName(lastName);
        account.setUsername(username);
        account.setPassword(password);
        account.setEmail(email);
        try
        {
            boolean success = GuitarTabberPersist.createAccount(account);
            if (success)
            {
                String sessionId = buildSessionToken(account);
                return Response.ok(sessionId).build();
            }

            return Response.status(401).build();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return Response.status(404).build();
        }
    }
    
    @GET
    @Path("/authenticate/{username}/{password}")
    @Produces("text/plain")
    @ClientResponseType(entityType=SessionToken.class)
    public Response authenticate(@PathParam("username") String username, @PathParam("password") String password)
    {
        try
        {
        	TabAccount account = GuitarTabberPersist.authenticate(username, password);
        	if (account != null)
        	{
        		System.out.println(account.getFirstName());
        		String sessionId = buildSessionToken(account);
        		return Response.ok(sessionId).build();
        	}
        }
        catch (Exception e)
        {
            e.printStackTrace(); //Log error and return 401 status
        }
        
        return Response.status(401).build();
    }*/
    
    /*@GET
    @Path("/auth")
    @Produces("application/xml")
    @ClientResponseType(entityType=PrincipalClient.class)
    public Response auth()
    {
        PrincipalClient principal = new PrincipalClient();
        principal.setUsername("merwine");
        principal.setPassword("password");
        return Response.ok(principal).build();
    }*/
    
    
    
    @GET
    @Path("validate")
    @Produces("text/plain")
    @ClientResponseType(entityType=String.class)
    public Response validate()
    {
        SessionToken token = validateSession();
        if (token != null)
        {
            return Response.ok("true").build();
        }

        return Response.status(401).build();
    }
    
    @GET
    @Path("logout")
    @Produces("text/plain")
    public Response logOut()
    {
    	invalidateSession();
    	return Response.ok().build();
    }
}