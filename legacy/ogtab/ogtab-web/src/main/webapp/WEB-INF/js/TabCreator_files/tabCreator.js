var guitarStrings = new Array("L","B","G","D","A","E");
var guitarist = 0;
var currentId = null;
var NUM_MEASURES = 75;

$(function()
{ 
    $(document).click(function(e){
        //They clicked a note
        var target = e.target ? e.target : e.srcElement;
        var clearOld = true;
        if (target.nodeName.toLowerCase() == 'div')
        {
            var id = target.parentNode.id;
            if (id.match("[EAGDBL][0-9][0-9]?")) {
                $(".selected").removeClass("selected");
                currentId = target.parentNode.id;
                $('#' + currentId + " div").addClass("selected");
                clearOld = false;
            }
        }
        if (clearOld) {
            $(".selected").removeClass("selected");
            currentId = null;
        }

    });

   $(document).keyup(function(e){
        if (currentId != null){
            if ((e.which>=48 && e.which<=90) || (e.which>=96 && e.which<=105))
            {
                var keyPressed = String.fromCharCode(e.which);
                var currentValue = $('#' + currentId + " div").text();
                if (currentValue == "-"){
                    $('#' + currentId + " div").html(keyPressed);
                }
                else if ((isNumeric(currentValue) && currentValue<4) || (!isNumeric(currentValue) && currentValue.length<3)) {
                    $('#' + currentId + " div").append(keyPressed);
                }
           }
           //Plus key
           else if (e.which == 107) {
               var value = $('#' + currentId + " div").text();
               
               if (isNumeric(value) && value <=35) {
                   value++;
                   $('#' + currentId + " div").html(value);
               }
               //TODO: cycle through add ons
           }
           //Minus key
           else if (e.which == 109) {
               var value = $('#' + currentId + " div").text();
               
               if (isNumeric(value) && value > 0) {
                   value--;
                   $('#' + currentId + " div").html(value);
               }
               //TODO: cycle through add ons
           }
           //Backspace
           else if (e.which == 8) {
               var value = $('#' + currentId + " div").text();
               
               if (value.length > 0) {
                   value = value.substring(0,value.length-1);
                   value = value.length>0 ? value : "-";
                   $('#' + currentId + " div").html(value);
               }
           }
           //Delete
           else if (e.which == 46) {
               $('#' + currentId + " div").html("-");
           }
           //Up arrow
           else if (e.which == 38) {
               var string = currentId.substring(0,1);
               var index = guitarStrings.indexOf(string);
               if (index > 0)
                   newString = guitarStrings[index-1];
               currentId = currentId.replace(string,newString);
               
               $(".selected").removeClass("selected");
               $('#' + currentId + " div").addClass("selected");
           }
           //Down arrow
           else if (e.which == 40) {
               var string = currentId.substring(0,1);
               var index = guitarStrings.indexOf(string);
               if (index < 5)
                   newString = guitarStrings[index+1];
               currentId = currentId.replace(string,newString);
               
               $(".selected").removeClass("selected");
               $('#' + currentId + " div").addClass("selected");
           }
           //Left arrow
           else if (e.which == 37) {
               var measure = currentId.substring(1);
               if (measure > 0)
                   measure--;
               currentId = currentId.replace(measure+1,measure);
               
               $(".selected").removeClass("selected");
               $('#' + currentId + " div").addClass("selected");
           }
           //Right arrow
           else if (e.which == 39) {
               var measure = currentId.substring(1);
               if (measure < NUM_MEASURES)
                   measure++;
               currentId = currentId.replace(measure-1,measure);
               
               $(".selected").removeClass("selected");
               $('#' + currentId + " div").addClass("selected");
           }
        }
   });

    $.getJSON("guitartabber-ws/services/song", function(data) {
        document.title = data.song.title + " by " + data.song.artist + " Tab";
        var tabTitle = data.song.title + "<br/><small>By " + data.song.artist + "</small>";
        $('#title').html(data.song.title);
        $('#artist').html("By " + data.song.artist);
        if (data.song.body != null){
            $('#tab').html(data.song.body);
        }
        else {
            $('#saveTab').hide();
        }
    });
    
    $('#saveTab').click(function(){
        $(".selected").removeClass("selected");
        var url = "guitartabber-ws/services/save"
        $.ajax({
            url: url,
            type: "PUT",
            data: $('#tab').html(),
            success: function(data) {
                $('#feedback').html("Song saved succesfully")
            }
        });
    });

    $('#newGuitarForm').submit(function(e){
        e.preventDefault();
        var markup = "<table><tr><td colspan='100'><strong>Guitar " 
            + guitarist + " - " + $('#guitarTypes').val();
        if ($('#guitarist').val() != ""){
            markup += " (" +$('#guitarist').val() + ")";
        }    
        markup += "</strong></td></tr>";
        guitarist++;
        for (x=0;x<6;x++) {
            var stringLabel = guitarStrings[x] != 'L' ? guitarStrings[x] : 'e';
            var string = guitarStrings[x];
            markup += "<tr><td>" + stringLabel + "</td>";
            for (y=0;y<=NUM_MEASURES;y++) {
                var id = '"' + string + y + '"';
                markup += "<td class='note' id=" + id + "><div>-</div></td>";
            }
            markup += "</tr>";
        }
        markup += "<tr><td></td><td colspan='" + (NUM_MEASURES-1) + "'><div id=copySlider" + guitarist + "></div></td></tr>";
        markup += "</table>";
        $('#tab').append(markup);
        $('#saveTab').show();
        //setTimeout("addSlider(" + guitarist + ")", 5);
        $("form")[0].reset();
    });
});

function addSlider(guitar)
{
    $("#copySlider" + guitar).slider({
            range: true,
            min: 1,
            max: NUM_MEASURES+1,
            step: 1,
            values: [ 1, 1 ],
            slide: function( event, ui ) {
                    //$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            }
    });

}

function setCurrentNote(id)
{

}

function isNumeric(input)
{
   return (input - 0) == input && input.length > 0;
}