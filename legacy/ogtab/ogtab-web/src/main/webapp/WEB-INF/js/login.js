$(function(){
    $('#form').submit(function(e){
        e.preventDefault();
        var pwEncrypt = $().crypt({
            method: 'md5',
            source: $('#password').val()
        });

        var url = "/ogsecurity/s/guitartabber/services/authenticate/" + $('#username').val() + "/" + pwEncrypt;
        $.ajax({
            url: url,
            success: function(data){
                location.href="/ogsecurity/s/guitartabber/secure/Home.html";
            },
            statusCode: {401: function(){
                $('#feedback').html("Invalid Username or Password");
            }}
       });
    });
});
