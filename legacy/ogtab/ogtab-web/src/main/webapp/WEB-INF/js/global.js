(function ($){
    var settings = {
        'type'		: 'get',
        'action'	: '',
        'params'	: null
    }
	
    var methods = {
        success: function (data) {
            return data;
        },
        error: function(jqXHR, status, errorThrown) {
				
        }
    }
    $.extend({
        callREST: function(options) {
            if (options) {
                $.extend(settings, options);
            }
			
            var url = "/ogsecurity/s/guitartabber/services/" + settings.action;
            if (settings.params != null){
                url += "/";
                for (x=0;x<settings.params.length;x++){
                    url += settings.params[x];
                }
            }
			
            $.ajax({
                url: url,
                type: settings.type,
                success: function(data) {
                    return methods.success(data);
                },
                statusCode: {
                    401: function(){
                        $('#login-dialog').dialog("open");
                    }
                },
            error: function(jqXHR, status, errorThrown) {
                return methods.error(jqXHR, status, errorThrown);
            }
				
            });
				
    }
    });
		
//}
})(jQuery);

$(function(){
    /*$.callRest: function(options) {
			var settings = {
				'type'		: 'get',
				'action'	: '',
				'params'	: null
			}
			
			var methods = {
					success: function (data) {
						return data;
					},
					error: function(jqXHR, status, errorThrown) {
						if (status == 401) {
							$('#login-dialog').dialog("open");
						}
					}
			}
			
			return function(){
				if (options) {
					$.extend(settings, options);
				}
				
				var url = "/guitartabber/" + settings.action;
				if (params != null){
					url += "/";
					for (x=0;x<params.length;x++){
						url += params[x];
					}
				}
				
				$.ajax({
					url: url,
					type: settings.type,
					success: function(data) {
						return methods.success(data);
					},
					error: function(jqXHR, status, errorThrown) {
						return methods.error(jqXHR, status, errorThrown);
					}
					
				});
					
			};
		}
	// });
    $('body').prepend("<div id='header'></div>");
    $('#header').load("../common/_header.html");
	
    $( "#login-dialog" ).dialog({
        autoOpen: false,
        height: 300,
        width: 350,
        modal: true,
        buttons: {
            "Login": function() {
                var pwEncrypt = $().crypt({
                    method: 'md5',
                    source: password
                });
				
                var url = "/ogsecurity/s/guitartabber/services/authenticate/" + username + "/" + pwEncrypt;
                $.ajax({
                    url: url,
                    success: function(data){
                        $( this ).dialog( "close" );
                    },
                    statusCode: {
                        401: function(){
                            $('#feedback').html("Invalid Username or Password");
                        }
                    }
                });
        },
        Cancel: function() {
            $( this ).dialog( "close" );
        }
    },
    close: function() {
        // allFields.val( "" ).removeClass( "ui-state-error" );
        $( this ).dialog( "close" );
    }
    });*/
});

