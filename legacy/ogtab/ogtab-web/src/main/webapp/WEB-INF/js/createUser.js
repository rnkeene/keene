$(function()
{ 
    $('#form').submit(function(e){
        e.preventDefault();
        
        var pwEncrypt = $().crypt({
            method: 'md5',
            source: $('#password').val()
        });
        
        var url = "/ogsecurity/s/guitartabber/services/account/"
                + $('#firstName').val() + "/" 
                + $('#lastName').val() + "/"
                + $('#email').val() + "/"
                + $('#username').val() + "/"
                + pwEncrypt;
        $.ajax({
            url: url,
            success: function(data) {
                location.href="/ogsecurity/s/guitartabber/secure/Home.html";
            }
        });
            
    });
});