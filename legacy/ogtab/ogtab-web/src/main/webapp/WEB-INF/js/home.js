function setSong(id) {
	var url = "/ogsecurity/s/guitartabber/services/song/" + id;
	$.getJSON(url, function(data) {
		location.href = "/ogsecurity/s/guitartabber/secure/TabCreator.html";
	});
}

function buildSongLink(song) {
	var results = "<div class='songLink'><div>" + song.title + " by "
			+ song.artist + " <a href='#' id='editLink' onclick='setSong(\""
			+ song.id + "\")'>[Edit]</a> <a href='#'>[View]</a>"
			+ "</div></div>";
	return results;
}

$(function(){
	var url = "/ogsecurity/s/guitartabber/services/songs/user";
	//$.getJSON(url, function(data) {
	$.callREST({ 
		action: "songs/user",
		success: function(data) {
			var results = "<fieldset><legend>Your created tabs</legend>";
			if (data.songList.songs.length > 1) {
				$.each(data.songList.songs, function(key, val) {
					results += buildSongLink(val);
				});
			}
			else {
				results += buildSongLink(data.songList.songs);
			}
			
			results += "</fieldset>";

			$("#songs").append(results);
		}
	});

	url = "/ogsecurity/s/guitartabber/services/user";
	$.getJSON(url, function(data) {
		var welcome = "<h2>Welcome " + data.account.firstName + " "
				+ data.account.lastName + "</h2>";
		$('#welcome').html(welcome);
	});

	$('#songLink').hover(
			function(e) {
				$('#' + e.target.parentNode.id).css("background-color",
						"yellow");
			},
			function(e) {
				$('#' + e.target.parentNode.id).css("background-color",
						"white");
			});
	
	$('#logOut').click(function(){
		url = "/ogsecurity/s/guitartabber/services/logout";
		$.getJSON(url, function(data) {
			location.href="/ogsecurity/s/guitartabber/Login.html";
		});
	});
});