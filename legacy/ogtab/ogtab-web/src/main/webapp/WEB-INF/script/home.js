tabIndex = 0;
currentSong = "";
currentTab = 0;
returnTab = 0;

function setSong(id) {
    var url = "act/song/" + id;
    $.getJSON(url, function(data) {
        displayTab(data);
    });
}

function buildSongLink(song) {
    var results = "<div class='songLink'><div>" + song.title + " by "
    + song.artist + " <a href='#' id='editLink' onclick='setSong(\""
    + song.id + "\")'>[Edit]</a> <a href='#'>[View]</a>"
    + "</div></div>";
    return results;
}


function displayTab(data) {
    tabIndex++
    title = data.song.title;
    titleFormat = title.replace(/ /g, "_").replace("'","").replace('"',"");
    artist = data.song.artist;
    artistFormat = artist.replace(/ /g, "_").replace("'","").replace('"',"");
    divId = titleFormat + "_" + artistFormat;
    display = title + "<br>" + artist;
    currentSong = divId;
    $("#tabs").append("<div id='" + divId + "'></div");
    $("#" + divId).load("serve/tabCreator.html");
    $("#tabs").tabs("add", "#" + divId, display, tabIndex);
    $("#tabs").tabs("option", "selected", tabIndex);
    getSongs();
}

function getSongs() {
    $.get('/ogtab/act/songs/user', function( data, status, jqXHR ) {
        if (jqXHR.status==200) {
            var results = "<strong style='font-size: 24px'>Your created tabs</strong><br/><br/>";
            if (data.songList != null && data.songList.songs != null) {
                if (data.songList.songs.length > 1) {
                    $.each(data.songList.songs, function(key, val) {
                        results += buildSongLink(val);
                    });
                } else {
                    results += buildSongLink(data.songList.songs);
                }
            } else {
                results += "<strong style='color: red;'>No Tabs Created</strong>";
            }
            results += "</fieldset>";
            $("#currentSongs").html(results);
        }
    });
}

$(function(){
    $("#adminToolbar").hide();

    //$.loading({onAjax:true, text: 'Waiting...'});

    $('#working').show();
    $('#working').bind('ajaxStart', function(){
        $(this).show();
    });
    $('#working').bind('ajaxStop', function(){
        $(this).hide();
    });

    $('#loadingDiv').hide().ajaxStart(function() {
        $(this).show();
    }).ajaxStop(function() {
        $(this).hide();
    });

    getSongs();

    $.get('/ogtab/act/user', function( data, status, jqXHR ) {
        if (jqXHR.status==200) {
            msg = "<div class='welcomeMsg'><h2>Welcome";
            if (data.tabAccount.firstName != null) {
                msg += ", " + data.tabAccount.firstName + " "+ data.tabAccount.lastName;
            }
            msg += "</h2></div>";
            $('#welcome').prepend(msg);
        }
    });
    $('#songLink').hover(function(e) {
        $('#' + e.target.parentNode.id).css("background-color","yellow");
    },function(e) {
        $('#' + e.target.parentNode.id).css("background-color","white");
    });
    $('#logOut').click(function(){
        $.post('act/logout', function( data, status, jqXHR ) {
            if (jqXHR.status==200) {
                location.href='';
            }
        });
    });

    $( "#tabs" ).tabs({
        ajaxOptions: {
            error: function( xhr, status, index, anchor ) {
                $( anchor.hash ).html(
                    "Couldn't load this tab. We'll try to fix this as soon as possible. " +
                    "If this wouldn't be a demo." );
            }
        },
        show: function(event,ui) {
            returnTab = currentTab;
            currentTab = ui.index;
            if (ui.index != 0){
                currentSong = ui.panel.id;
            }
        },
        tabTemplate: '<li class="tabHeight ui-state-default ui-corner-top"><a href="#{href}"><span class="tabEntry">#{label}</span></a></li>'
    });
});