var guitarStrings = new Array("E","A","D","G","B","L");//("L","B","G","D","A","E");
var stringNames = new Array("lowEString","aString","dString","gString","bString","highEString");
var notes = new Array("Ab","A","Bb","B","C","Db","D","Eb","E","F","Fb","G","Gb");
//var guitarist = 0;
var currentId = null;
var NUM_MEASURES = 40;

guitarIndices = new Object();
chords = new Object();

var shiftMap = new Object();
shiftMap["54"] = "^";
shiftMap["57"] = "(";
shiftMap["48"] = ")";

newGuitarFields = $( [] );

$(function()
{

    $(document).click(function(e){
        //They clicked a note
        var target = e.target ? e.target : e.srcElement;
        var clearOld = true;
        if (target.nodeName.toLowerCase() == 'div')
        {
            var id = target.parentNode.id;
            if (id.match("[EAGDBL]_[0-9]*")) {
                $(".selected").removeClass("selected");
                currentId = target.parentNode.id;
                $('#' + currentSong + " #" + currentId + " div").addClass("selected");
                clearOld = false;
            } else if (id.match("col_[0-9]*")){
                currentId = target.parentNode.id;
                $('#' + currentSong + " #" + currentId + " div").addClass("selected");
                $("#chordDialog").data("divId",currentId).dialog("open");
            }

        }
        if (clearOld) {
            $(".selected").removeClass("selected");
            currentId = null;
        }

    });

    $(document).keydown(function(e){
        if (currentId != null && (e.which == 38 || e.which == 40)) {
            e.preventDefault();
        }
    });

    $(document).keyup(function(e){
        if (currentId != null){
            e.preventDefault();
            if ((e.which>=48 && e.which<=90) || (e.which>=96 && e.which<=105))
            {
                if (e.shiftKey){
                    var keyPressed = shiftMap[e.which];
                }
                else {
                    var key = e.which;
                    if (key>=96 && key<=105) {
                        key = key - 48; //numpad was pressed, updating value to be it's # equivalent
                    }
                    var keyPressed = String.fromCharCode(key);
                }
                if (keyPressed != null){
                    var currentValue = $('#' + currentSong + " #" + currentId + " div").text();
                    if (currentValue == "-"){
                        $('#' + currentSong + " #" + currentId + " div").html(keyPressed);
                    }
                    else if ((isNumeric(currentValue) && currentValue<4) || (!isNumeric(currentValue) && currentValue.length<3)) {
                        $('#' + currentSong + " #" + currentId + " div").append(keyPressed);
                    }
                }
            }
            //Plus key
            else if (e.which == 107) {
                var value = $('#' + currentSong + " #" + currentId + " div").text();

                if (isNumeric(value) && value <=35) {
                    value++;
                    $('#' + currentSong + " #" + currentId + " div").html(value);
                }
            //TODO: cycle through add ons
            }
            //Minus key
            else if (e.which == 109) {
                var value = $('#' + currentSong + " #" + currentId + " div").text();

                if (isNumeric(value) && value > 0) {
                    value--;
                    $('#' + currentSong + " #" + currentId + " div").html(value);
                }
            //TODO: cycle through add ons
            }
            //Backspace
            else if (e.which == 8) {
                var value = $('#' + currentSong + " #" + currentId + " div").text();

                if (value.length > 0) {
                    value = value.substring(0,value.length-1);
                    value = value.length>0 ? value : "-";
                    $('#' + currentSong + " #" + currentId + " div").html(value);
                }
            }
            //Delete
            else if (e.which == 46) {
                $('#' + currentSong + " #" + currentId + " div").html("-");
            }
            //Up arrow
            else if (e.which == 38) {
                var string = currentId.substring(0,1);
                var index = guitarStrings.indexOf(string);
                if (index > 0)
                    newString = guitarStrings[index-1];
                currentId = currentId.replace(string,newString);

                $(".selected").removeClass("selected");
                $('#' + currentSong + " #" + currentId + " div").addClass("selected");
            }
            //Down arrow
            else if (e.which == 40) {
                var string = currentId.substring(0,1);
                var index = guitarStrings.indexOf(string);
                if (index < 5)
                    newString = guitarStrings[index+1];
                currentId = currentId.replace(string,newString);

                $(".selected").removeClass("selected");
                $('#' + currentSong + " #" + currentId + " div").addClass("selected");
            }
            //Left arrow
            else if (e.which == 37) {
                var string = currentId.substring(0,1);
                var measure_guitar = currentId.substring(2);
                var measure_guitar_array = measure_guitar.split("_");
                var measure = measure_guitar_array[0];
                var guitar = measure_guitar_array[1];
                if (measure > 0)
                    measure--;
                currentId = string+"_"+measure+"_"+guitar;

                $(".selected").removeClass("selected");
                $('#' + currentSong + " #" + currentId + " div").addClass("selected");
            }
            //Right arrow
            else if (e.which == 39) {
                var string = currentId.substring(0,1);
                var measure = currentId.substring(2).split("_")[0];
                var guitar = currentId.substring(2).split("_")[1];
                //var measure = measure_guitar_array[0];
                //var guitar = measure_guitar_array[1];
                if (measure < NUM_MEASURES)
                    measure++;
                currentId = string+"_"+measure+"_"+guitar;

                $(".selected").removeClass("selected");
                $('#' + currentSong + " #" + currentId + " div").addClass("selected");
            }
        }
    });

    $("body").append("<div id='chordDialog'></div>");
    $.post("serve/chordDialog.html", function (data){
        $("#chordDialog").html(data);
    });
    $("#chordDialog").dialog({
        autoOpen: false,
        height: 350,
        width: 350,
        modal: true,
        buttons: {
            "Add Chord": function() {
                if ($("#chords").val() != null && $("#chords").val() != "") {
                    for (x=0;x<6;x++){
                        var divId = $(this).data('divId').replace("col", guitarStrings[x]);
                        var val = $("#chords").val().substring(x, x+1);
                        $("#" + currentSong + " #" + divId + " div").html(val);
                    }
                    $( "#chordDialog" ).dialog( "close" );

                }
                $('#' + currentSong + " #chordFeedback").html("Please choose a chord to add.");
            },
            Cancel: function() {
                $( '#' + currentSong + " #chordDialog" ).dialog( "close" );
            }
        },
        close: function(event, ui) {
            $("#chords").val( "" );
        }
    });

    $("#chords").change(function(){
        if ($("#chords").val() != null && $("#chords").val() != "") {
            for (x=0;x<6;x++){
                var val = $("#chords").val().substring(x, x+1);
                $("#" + stringNames[x] + " span").html(val);
            }
        }
    });

});

function saveTab(){
    $('#feedback').hide();
    $(".selected").removeClass("selected");
    $.ajax({
        url: "act/save",
        type: "POST",
        data: $('#' + currentSong + ' #tab').html(),
        success: function(data) {
            $('#feedback').html(data)
            $('#feedback').show();
        }
    });

}

function addGuitar(){
    var section = 0;
    guitarist = guitarIndices[currentTab];
    if (guitarist == null) {
        guitarist = 0;
    }
    guitarist++;
    var markup = "<div id='guitarist_" + guitarist + "_" + section + "'><div style='width: 100%'><strong>Guitar " + guitarist + " - " + $('#guitarTypes').val();
    if ($('#' + currentSong + ' #guitarist').val() != ""){
        markup += " (" +$('#' + currentSong + ' #guitarist').val() + ")";
    }
    markup += "</strong></div>";

    var tuningArray = new Array();


    markup += "<div class='guitarBody'>";
    for (x=0; x<NUM_MEASURES+1;x++)
    {
        markup += "<div class='tabColumn' id='col_" + x + "_" + guitarist + "'>"
        if (x == 0) {
            tuning = $('#' + currentSong + " #tuningList").val();
            for (y=0;y<tuning.length;y++){
                var stringLabel = tuning.substring(y,y+1);
                if (tuning.substring(y+1,y+2) == "b"){
                    stringLabel += tuning.substring(y+1,y+2);
                    y++
                }
                tuningArray[y] = stringLabel;
                markup += "<div class='noteLabel'>" + stringLabel + "</div>";
            }
            markup += "<div class='noteLabel'></div>";
        }
        else {
            for (y=0;y<7;y++){
                if (y!=6) {
                    var id = "'" + guitarStrings[y] +  "_" + x + "_" + guitarist + "'";
                    markup += "<div class='note' id=" + id + "><div>-</div></div>";
                }
                else{
                    var id = '"chord_' + x + '_' + guitarist + '"';
                    var mouse = "onmouseover='highlightCol(this)' onmouseout='unhighlightCol(this)'";
                    markup += "<div class='chord' " + mouse + " id=" + id + ">+</div>";
                }
            }

        }

        markup += "</div>";
    }
    markup += "</div></div>";
    $('#' + currentSong + ' #tab').append(markup);
    $('#' + currentSong + ' #saveTab').show();
    // for each select field on the page
    $('#' + currentSong + " #guitarist").val("");
    guitarIndices[currentTab] = guitarist;
}

function highlightCol(div){
    $(div.parentNode).addClass("highlighted");
}

function unhighlightCol(div){
    $(div.parentNode).removeClass("highlighted");
}

function tabCreatorInit() {

    $.post("act/tabCreator", function(data){
        //Updating page with song info
        document.title = data.response.song.title + " by " + data.response.song.artist + " Tab";
        $('#' + currentSong + ' #title').html(data.response.song.title);
        $('#' + currentSong + ' #artist').html("By " + data.response.song.artist);
        if (data.response.song.bodyText != null){
            $('#' + currentSong + ' #tab').html(data.response.song.bodyText);
            var guitarRegex = new RegExp("Guitar [0-9]");
            var guitars = guitarRegex.exec(data.response.song.bodyText);
            guitarist = 0;
            for (x=0; x<guitars.length; x++) {
                if (guitars[x].substring(guitars[x].length-1,guitars[x].length)>guitarist) {
                    guitarist = guitars[x].substring(guitars[x].length-1,guitars[x].length);
                }
            }
            guitarIndices[currentTab] = guitarist;
        }
        else {
            $('#' + currentSong + ' #saveTab').hide();
        }

        //Filling in the tuning drop down
        $.each(data.response.tunings, function(key, val){
            $("#" + currentSong + " #tuningList").append("<option value='" + val.tuning + "'>" + val.name + "</option>");
        });
        setTuning(data.response.tunings[0].tuning);

        //Filling in the chord drop down
        if ($("#chords").length == 1) {
            $.each(data.response.chords, function(key, val){
                var optVal = "";
                if (val.lowEstring != null)
                    optVal = val.lowEstring;
                else
                    optVal = "-";
                if (val.astring != null)
                    optVal += val.astring;
                else
                    optVal += "-";
                optVal += val.dstring.toString() + val.gstring.toString() + val.bstring.toString() + val.highEstring.toString();
                $("#chords").append("<option value='" + optVal + "'>" + val.chordName + "</option>");
            });
        }
    });


    $('#' + currentSong + " #tuningContainer select").each(function (){
        selectTag = $(this);
        $.each(notes, function(key, val) {
            selectTag.append("<option value='" + val + "'>" + val + "</option>");
        });
    });

    $('#' + currentSong + " #tuningList").change(function (){
        setTuning($('#' + currentSong + " #tuningList").val());
    });



}

function setTuning(tuning)
{
    order=0;
    for (x=0;x<tuning.length;x++){
        note = tuning.substring(x,x+1);
        if (tuning.substring(x+1,x+2) == "b"){
            note += tuning.substring(x+1,x+2);
            x++
        }
        $("#" + currentSong + " #" + stringNames[order]).val(note);
        order++;
    }
}

function setCurrentNote(id)
{

}

function isNumeric(input)
{
    return (input - 0) == input && input.length > 0;
}