$(function(){
    //Variable to hold fields for new tab dialog
    newTabFields = $( [] );
    editProfileFields = $( [] );

    $("#newTab").click(function(e) {
        e.preventDefault();
        $( "#newTabDialog" ).dialog( "open" );
    });

    $( "#newTabDialog" ).dialog({
        autoOpen: false,
        height: 415,
        width: 350,
        modal: true,
        buttons: {
            "Create tab": function() {
                var valid = false;
                if ($("#title").val() != null && $("#artist").val() != null) {
                    $.post("act/song/create", {
                        "title": $("#title").val(),
                        "artist": $("#artist").val(),
                        "notes": $("#notes").val()
                    }, function(data) {
                        displayTab(data);
                    });

                    valid = true;
                }


                if ( valid ) {
                    $( "#newTabDialog" ).dialog( "close" );
                }
            },
            Cancel: function() {
                $("#tabs").tabs("option", "selected", returnTab);
                $( "#newTabDialog" ).dialog( "close" );
            }
        },
        open: function(event, ui) {
            if ($("#newTabDialog").html().length == 0) {
                $.post("serve/newTab.html", function (data){
                    $("#newTabDialog").html(data);
                });
                newTabFields = $( [] ).add( $("#title") ).add( $("#artist") ).add( $("#notes") );
            }
        },
        close: function(event, ui) {
            if(event.type == "dialogclose") {
                $("#tabs").tabs("option", "selected", returnTab);
            }
            newTabFields.val( "" ).removeClass( "ui-state-error" );
        }
    });

    $("#editProfile").click(function(e) {
        e.preventDefault();
        $( "#editProfileDialog" ).dialog( "open" );
    });

    $( "#editProfileDialog" ).dialog({
        autoOpen: false,
        height: 350,
        width: 350,
        modal: true,
        buttons: {
            "Edit Profile": function() {
                if ($("#firstname").val() != null && $("#lastname").val() != null) {
                    $.post("act/profile", {
                        "firstname": $('#firstname').val(),
                        "lastname": $('#lastname').val(),
                        "email": $('#email').val()
                    }, function(data) {
                        if (data == "success") {
                            $( "#editProfileDialog"  ).dialog( "close" );
                        }
                    });
                }
                $("#profileFeedback").html("Error!");
            },
            Cancel: function() {
                $( "#editProfileDialog" ).dialog( "close" );
            }
        },
        open: function(event, ui) {
            $.post("serve/updateProfile.html", function (data){
                $("#editProfileDialog").html(data);
            });
            editProfileFields = $( [] ).add( $("#firstname") ).add( $("#lastname") ).add( $("#email") );
        },
        close: function(event, ui) {
            editProfileFields.val( "" ).removeClass( "ui-state-error" );
        }
    });
});

