/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.guitartabber.model.utils;

import com.organick.guitartabber.model.Song;
import com.organick.ogsecurity.account.Account;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author matt
 */
public class AccountUtils {

    private static final String SONGS = "songs";

    public static List<Song> getSongs(Account account) {
        List<Song> songs = (List<Song>) account.getProperties().get(SONGS);
        if (songs == null) {
            songs = new ArrayList<Song>();
            account.getProperties().put(SONGS, songs);
        }
        return songs;
    }
}
