/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.guitartabber.model;

import com.organick.ogsecurity.account.Account;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nate
 */
@XmlRootElement
public class TabAccount implements Serializable {

    private static final String CELLPHONE = "cellphone";
    private static final String EMAIL = "email";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final String PASSWORD = "password";
    private static final String SONGS = "songs";
    private static final String USERNAME = "username";

    public TabAccount(Account account) {
        this.account = account;
    }

    public TabAccount() {
        this.account = new Account();
    }

    private transient Account account;

    public Account getAccount() {
        return account;
    }

    @XmlElement
    public String getCellPhone() {
        return account.getProperty(CELLPHONE);
    }

    public void setCellPhone(String cellPhone) {
        account.setProperty(CELLPHONE, cellPhone);
    }

    @XmlElement
    public String getEmail() {
        return account.getProperty(EMAIL);
    }

    public void setEmail(String email) {
        account.setProperty(EMAIL, email);
    }

    @XmlElement
    public String getFirstName() {
        return account.getProperty(FIRSTNAME);
    }

    public void setFirstName(String firstName) {
        account.setProperty(FIRSTNAME, firstName);
    }

    @XmlElement
    public String getLastName() {
        return account.getProperty(LASTNAME);
    }

    public void setLastName(String lastName) {
        account.setProperty(LASTNAME, lastName);
    }

    @XmlElement
    public String getPassword() {
        return account.getProperty(PASSWORD);
    }

    public void setPassword(String password) {
        account.setProperty(PASSWORD, password);
    }

    @XmlElement
    public String getUsername() {
        return account.getName();
    }

    public void setUsername(String username) {
        account.setProperty(USERNAME, username);
    }

    @XmlElement
    public List<Song> getSongs() {
        return initSongs();
    }

    public void addSong(Song song) {
        List<Song> songs = initSongs();
        song.setId(Long.valueOf(songs.size()));
        songs.add(song);
    }

    private List<Song> initSongs() {
        List<Song> songs = (List<Song>) account.getProperties().get(SONGS);
        if (songs == null) {
            songs = new ArrayList<Song>();
            account.getProperties().put(SONGS, songs);
        }
        return songs;
    }

    public Map<String, String> getUpdateProfileProps() {
        Map<String, String> props = new HashMap<String, String>();
        props.put(FIRSTNAME, getFirstName());
        props.put(LASTNAME, getLastName());
        props.put(EMAIL, getEmail());
        return props;
    }

    @Override
    public String toString() {
        return "\tTabAccount{\n" + "\t\tusername=" + getUsername() + "\n\t\tpassword=" + getPassword() + "\n\t\tfirstName=" + getFirstName() + "\n\t\tlastName=" + getLastName() + "\n\t\temail=" + getEmail() + "\n\t\tcellPhone=" + getCellPhone() + "\n\t\tsongs=" + getSongs() + "\t}";
    }


}
