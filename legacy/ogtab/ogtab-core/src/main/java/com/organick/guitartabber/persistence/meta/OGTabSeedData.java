/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.guitartabber.persistence.meta;

import com.organick.guitartabber.model.Chord;
import com.organick.guitartabber.model.ChordList;
import com.organick.guitartabber.model.Tuning;
import com.organick.guitartabber.model.TuningList;
import com.organick.guitartabber.model.utils.Constants;
import com.organick.persist.Persist;

/**
 *
 * @author matt
 */
public class OGTabSeedData {

    public static void init() throws Exception {

        //Seeding tunings
        TuningList tunings = getTunings();
        TuningList currTunings = Persist.get(Constants.TUNINGS.toString(), TuningList.class.getName());
        if (currTunings == null) {
            Persist.save(tunings, Constants.TUNINGS.toString());
        } else {
            Persist.update(tunings, Constants.TUNINGS.toString());
        }

        //Seeding chords
        ChordList chords = getChords();
        ChordList currentChords = Persist.get(Constants.CHORDS.toString(), ChordList.class.getName());
        if (currentChords == null){
            Persist.save(chords, Constants.CHORDS.toString());
        } else {
            Persist.update(chords, Constants.CHORDS.toString());
        }
    }

    private static TuningList getTunings() {
        TuningList tunings = new TuningList();
        tunings.getTunings().add(initTuning("EADGBE", "Standard Tuning", 1));
        tunings.getTunings().add(initTuning("DADGBE", "Drop D", 2));
        tunings.getTunings().add(initTuning("EbAbDbGbBbEb", "Eb Tuning", 3));
        tunings.getTunings().add(initTuning("DbAbDbGbBbEb", "Drop Db", 4));
        tunings.getTunings().add(initTuning("CGCFAD", "Drop C", 5));
        tunings.getTunings().add(initTuning("BGbBEAbD", "Drop B", 6));
        tunings.getTunings().add(initTuning("DGCFAD", "D Tuning", 7));
        tunings.getTunings().add(initTuning("CGCGCE", "Open C", 8));
        tunings.getTunings().add(initTuning("DADGbAD", "Open D", 9));
        tunings.getTunings().add(initTuning("EBEAbBE", "Open E", 10));
        tunings.getTunings().add(initTuning("FACFCF", "Open F", 11));
        tunings.getTunings().add(initTuning("DGDGBD", "Open G", 12));
        tunings.getTunings().add(initTuning("EADbEAE", "Open A", 13));

        return tunings;
    }

    private static Tuning initTuning(String tuningStr, String name, long order) {
        Tuning tuning = new Tuning(tuningStr);
        tuning.setName(name);
        tuning.setSortOrder(order);
        tuning.setPrivatebit(false);
        return tuning;
    }

    private static ChordList getChords() {
        ChordList chords = new ChordList();

        chords.getChords().add(initChord(null, "3", "2", "0", "1", "0", "C"));
        chords.getChords().add(initChord("0", "2", "2", "1", "0", "0", "E"));
        chords.getChords().add(initChord("3", "2", "0", "0", "3", "3", "G"));
        chords.getChords().add(initChord("3", "2", "0", "0", "0", "3", "G"));
        chords.getChords().add(initChord(null, "0", "2", "2", "2", "0", "A"));
        chords.getChords().add(initChord(null, null, "0", "2", "3", "2", "D"));
        chords.getChords().add(initChord("1", "3", "3", "2", "1", "1", "F"));
        chords.getChords().add(initChord(null, "2", "4", "4", "4", "2", "B"));

        return chords;
    }

    private static Chord initChord(String E, String A, String D, String G, String B, String e, String name) {
        Chord chord = new Chord();
        chord.setAstring(A);
        chord.setBstring(B);
        chord.setDstring(D);
        chord.setGstring(G);
        chord.setHighEstring(e);
        chord.setLowEstring(E);
        chord.setChordName(name);
        chord.setPrivatebit(false);

        return chord;
    }
}
