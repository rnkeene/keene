package com.organick.guitartabber.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Song implements Serializable
{
	private Long id;

        @XmlElement
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}
	
	private String artist;
	
        @XmlElement
	public String getArtist()
	{
		return artist;
	}

	public void setArtist(String artist)
	{
		this.artist = artist;
	}

	private String title;

        @XmlElement
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}
	
	private String notes;

        @XmlElement
	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}
        
        private String bodyText;

        @XmlElement
        public String getBodyText() 
        {
            return bodyText;
        }

        public void setBodyText(String bodyText) 
        {
            this.bodyText = bodyText;
        }
        
        
        
        
    /*@Override
    public Song getNewInstance() 
    {
        return new Song();
    }*/
}
