package com.organick.guitartabber.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="songList")
public class SongList implements Serializable
{
    private List<Song> songs;

    @XmlElement
    public List<Song> getSongs()
    {
        init();
        return songs;
    }

    public void addSong(Song song)
    {
        init();
        this.songs.add(song);
    }
    
    private void init()
    {
        if (songs == null)
        {
            songs = new ArrayList<Song>();
        }
    }
    
    /*@Override
    public SongList getNewInstance() 
    {
        return new SongList();
    }*/
}