/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.guitartabber.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author matt
 */
@XmlRootElement(name="tuningList")
public class TuningList implements Serializable {

    private List<Tuning> tunings;

    @XmlElement
    public List<Tuning> getTunings() {
        if (tunings == null) {
            tunings = new ArrayList<Tuning>();
        }
        return tunings;
    }
}
