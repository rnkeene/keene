/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.guitartabber.model.utils;

/**
 *
 * @author matt
 */
public enum Constants {
    
    TUNINGS("tunings"),
    CHORDS("chords");

    private String name;

    Constants(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
