/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.guitartabber.persistence;

//import com.organick.guitartabber.model.TabAccount;
import com.organick.guitartabber.model.ChordList;
import com.organick.guitartabber.model.TabAccount;
import com.organick.guitartabber.model.TabAccountList;
import com.organick.guitartabber.model.TuningList;

/**
 *
 * @author matt
 */
public class GuitarTabberPersist
{

    protected static GuitarTabberDAO guitarTabberDAO;
    protected static AuthDAO authDAO;

    static
    {
        try
        {
            /*XmlBeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("beans.xml"));

            guitarTabberDAO = (GuitarTabberDAO) beanFactory.getBean("guitarTabberDAO");

            authDAO = (AuthDAO) beanFactory.getBean("authDAO");*/
            guitarTabberDAO = new GuitarTabberDAO();
            authDAO = new AuthDAO();
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /*public static boolean createAccount(Account account)
            throws Exception
    {
        return authDAO.createAccount(account);
    }

    public static TabAccount authenticate(String username, String password)
            throws Exception
    {
        return authDAO.authenticate(username, password);
    }*/

    public static void updateAccount(TabAccount account)
            throws Exception
    {
        guitarTabberDAO.updateAccount(account);
    }

    public static TuningList getTunings() throws Exception
    {
        return guitarTabberDAO.getPublicTunings();
    }

    public static ChordList getChords() throws Exception
    {
        return guitarTabberDAO.getPublicChords();
    }

    public static TabAccountList getTabAccounts() throws Exception
    {
        return guitarTabberDAO.getOGTabUsers();
    }
}
