package com.organick.guitartabber.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="chordList")
public class ChordList implements Serializable
{
    private List<Chord> songs;

    @XmlElement
    public List<Chord> getChords()
    {
        if (songs == null)
        {
            songs = new ArrayList<Chord>();
        }
        return songs;
    }
    
    /*@Override
    public SongList getNewInstance() 
    {
        return new SongList();
    }*/
}