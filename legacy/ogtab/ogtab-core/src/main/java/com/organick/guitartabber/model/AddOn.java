package com.organick.guitartabber.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="addon")
public class AddOn implements Serializable
{
	private long id;
	
	@XmlElement
	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}

	private String name;

        @XmlElement
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	private String symbol;

        @XmlElement
	public String getSymbol()
	{
		return symbol;
	}

	public void setSymbol(String symbol)
	{
		this.symbol = symbol;
	}

    /*@Override
    public AddOn getNewInstance() 
    {
        return new AddOn();
    }*/
        
        
}
