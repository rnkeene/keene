package com.organick.guitartabber.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="chord")
public class Chord implements Serializable
{
	private long id;

        @XmlElement
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}
	
	private String highEstring;
	
	@XmlElement
	public String getHighEstring()
	{
		return highEstring;
	}

	public void setHighEstring(String highEstring)
	{
		this.highEstring = highEstring;
	}

	private String Astring;

        @XmlElement
	public String getAstring()
	{
		return Astring;
	}

	public void setAstring(String astring)
	{
		Astring = astring;
	}
	
	private String Dstring;

        @XmlElement
	public String getDstring()
	{
		return Dstring;
	}

	public void setDstring(String dstring)
	{
		Dstring = dstring;
	}
	
	private String Gstring;
	
        @XmlElement
	public String getGstring()
	{
		return Gstring;
	}

	public void setGstring(String gstring)
	{
		Gstring = gstring;
	}

	private String Bstring;

        @XmlElement
	public String getBstring()
	{
		return Bstring;
	}

	public void setBstring(String bstring)
	{
		Bstring = bstring;
	}
	
	private String lowEstring;

        @XmlElement
	public String getLowEstring()
	{
		return lowEstring;
	}

	public void setLowEstring(String lowEstring)
	{
		this.lowEstring = lowEstring;
	}
	
	private boolean privatebit = true;

        @XmlElement
	public boolean isPrivatebit()
	{
		return privatebit;
	}

	public void setPrivatebit(boolean privatebit)
	{
		this.privatebit = privatebit;
	}
	
	private String chordName;

        @XmlElement
	public String getChordName()
	{
		return chordName;
	}

	public void setChordName(String chordName)
	{
		this.chordName = chordName;
	}
	
	private long order;

        @XmlElement
	public Long getOrder() 
	{
		return order;
	}

	public void setOrder(Long order) 
	{
		this.order = order;
	}

    /*@Override
    public Chord getNewInstance() 
    {
        return new Chord();
    }*/
        
        
}
