package com.organick.guitartabber.persistence;



import com.organick.guitartabber.model.ChordList;
import com.organick.guitartabber.model.TabAccount;
import com.organick.guitartabber.model.TabAccountList;
import com.organick.guitartabber.model.TuningList;
import com.organick.guitartabber.model.utils.Constants;
import com.organick.ogsecurity.account.Account;
import com.organick.persist.Persist;
import java.util.List;



class GuitarTabberDAO
{

	//@PersistenceContext(unitName="guitartabber-pu")
	//private EntityManager entityManager;

    public void updateAccount(TabAccount account) throws Exception
    {
        Persist.update(account.getAccount(), account.getAccount().getName());
    }

	/*public List<ChordImpl> getPublicChords() throws Exception
	{
		List<ChordEntity> results = (List<ChordEntity>)getHibernateTemplate().findByNamedQuery(ChordEntity.GET_PUBLIC_CHORDS);
		//Query query = entityManager.createNamedQuery(ChordEntity.GET_PUBLIC_CHORDS);
		//List<ChordEntity> results = query.getResultList();
		List<ChordImpl> chords = 
		    new ArrayList<ChordImpl>();
		for (ChordEntity chord : results)
		{
		    chords.add(chord.transform());
		}
		
		return chords;
	}*/
	
	/*public List<AddOnImpl> getAllAdOns()  throws Exception
	{
		// TODO Auto-generated method stub
		return null;
	}*/

	/*public Long saveSong( SongImpl song)
                throws Exception
	{
		//Want to see if this tuning exists before we create one
		/*if (song.getTuning() != null && song.getTuning().getTuning() != null)
		{
			Query query = entityManager.createNamedQuery(Tuning.FIND_BY_TUNING_CD);
			query.setParameter("tuning", song.getTuning().getTuning());
			query.setMaxResults(0);
			
			List<Tuning> tunings = query.getResultList();
			//Found a tuning 
			if (tunings != null && tunings.size() == 1)
			{
				song.setTuning(tunings.get(0));
			}
			
			
		}*/
	    /*SongEntity entity = new SongEntity(song, getHibernateTemplate());
	    getHibernateTemplate().save(entity);
		//entityManager.flush();
		
		return entity.getId();
	}*/
	
	/*public SongImpl updateSong(SongImpl clientSong) throws Exception
	{
		SongEntity song = getHibernateTemplate().get(SongEntity.class, clientSong.getId());
	    song.merge(clientSong, getHibernateTemplate());
            song = getHibernateTemplate().merge(song);
            //entityManager.flush();
            return song.transform();
        }*/
	
	public TuningList getPublicTunings() throws Exception
	{
            TuningList tunings = Persist.get(Constants.TUNINGS.toString(), TuningList.class.getName());

            return tunings;
	}

	public ChordList getPublicChords() throws Exception
	{
            ChordList chords = Persist.get(Constants.CHORDS.toString(), ChordList.class.getName());

            return chords;
	}

        public TabAccountList getOGTabUsers() throws Exception
        {
            List<Account> accounts = Persist.getByClass(Account.class.getName());
            TabAccountList tabAccounts = new TabAccountList();
            if (accounts != null)
            {
                for (Account account : accounts)
                {
                    TabAccount tabAccount = new TabAccount(account);
                    if (tabAccount.getSongs().size() > 0)
                    {
                        tabAccounts.getTabAccounts().add(tabAccount);
                    }
                }
            }

            return tabAccounts;
        }
	
	/*public AddOnImpl getAddonById(Long addOnId) throws Exception
	{
		AddOnEntity addOn = getHibernateTemplate().get(AddOnEntity.class, addOnId);
		return addOn.transform();
	}*/
	
	/*public SongImpl getSongById(Long id) throws Exception
	{
	    SongEntity song = getHibernateTemplate().get(SongEntity.class, id);
		return song.transform();
	}*/
	
	/*public List<SongImpl> searchSongs(String songName, String artist) throws Exception
	{
		String namedQuery = null;
		List<String> params = new ArrayList<String>();
		List<String> values = new ArrayList<String>();
		
		if (!isEmpty(songName))
		{
			params.add("title");
			values.add(songName.toLowerCase() + "%");
			namedQuery = SongEntity.FIND_BY_TITLE;
		}
		if (!isEmpty(artist))
		{
			params.add("artist");
			values.add(artist.toLowerCase() + "%");
			namedQuery = namedQuery==null ? SongEntity.FIND_BY_ARTIST : SongEntity.FIND_BY_ARTIST_AND_TITLE;
		}
		
		if (namedQuery == null)
			return null;
		
		//return hibernateTemplate.findByNamedQueryAndNamedParam(namedQuery, "title", songName.toLowerCase() + "%");
		List<SongEntity> results = getHibernateTemplate().findByNamedQueryAndNamedParam(namedQuery, 
				params.toArray(new String[0]), values.toArray(new String[0]));
		/*Query query = entityManager.createNamedQuery(namedQuery);
		if (!isEmpty(songName))
			query.setParameter("title", songName.toLowerCase() + "%");
		if (!isEmpty(artist))
			query.setParameter("artist", artist.toLowerCase() + "%");
		
		List<SongEntity> results = query.getResultList();*/ 
		/*List<SongImpl> songs =
		    new ArrayList<SongImpl>();
		for (SongEntity song : results)
		{
		    songs.add(song.transform());
		}
		
		return songs;
	}*/

        /*public List<SongImpl> getAllSongs() throws Exception
        {
        	List<SongEntity> results = getHibernateTemplate().execute(new HibernateCallback<List<SongEntity>>()
        	{
        		@Override
        		public List<SongEntity> doInHibernate(Session session)
        				throws HibernateException, SQLException 
				{
        			SQLQuery query = session.createSQLQuery("select * from song");
        			return (List<SongEntity>) query.list();
        		}
        	});
            //Query query = entityManager.createNativeQuery("select * from song", Song.class);
            //List<SongEntity> results = query.getResultList();
            List<SongImpl> songs = 
                new ArrayList<SongImpl>();
            for (SongEntity song : results)
            {
                songs.add(song.transform());
            }
            
            return songs;
        }*/

    /*public List<SongImpl> getSongsForAccount(Long accountId) throws Exception
    {
    	List<SongEntity> results = getHibernateTemplate().findByNamedQueryAndNamedParam(SongEntity.FIND_BY_ACCOUNT, "accountId", accountId);
        //Query query = entityManager.createNamedQuery(SongEntity.FIND_BY_ACCOUNT);
        //query.setParameter("accountId", accountId);
        //List<SongEntity> results = query.getResultList();
        List<SongImpl> songs = new ArrayList<SongImpl>();
        if (results != null)
        {
            for (SongEntity song : results)
            {
                songs.add(song.transform());
            }
        }
        
        return songs;
    }
      */
        



	private boolean isEmpty(String value)
	{
		if (value == null || value.equals(""))
			return true;
		return false;
	}

}
