package com.organick.guitartabber.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="tuning")
public class Tuning implements Serializable
{
	private long id;

        @XmlElement
	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}
	
	private String tuning;

        @XmlElement
	public String getTuning()
	{
		return tuning;
	}

	public void setTuning(String tuning)
	{
		this.tuning = tuning;
	}
	
	private String name;
	
        @XmlElement
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	private boolean privatebit = true; //default to true

        @XmlElement
	public boolean isPrivatebit()
	{
		return privatebit;
	}

	public void setPrivatebit(boolean privatebit)
	{
		this.privatebit = privatebit;
	}
	
	private long sortOrder;

        @XmlElement
	public long getSortOrder()
	{
		return sortOrder;
	}

	public void setSortOrder(long sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public Tuning() {}
	
	public Tuning(String tuning)
	{
		this.tuning = tuning;
	}
        
    /*@Override
    public Tuning getNewInstance() 
    {
        return new Tuning();
    }*/
}
