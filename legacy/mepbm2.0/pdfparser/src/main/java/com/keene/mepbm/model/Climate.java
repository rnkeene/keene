package com.keene.mepbm.model;

public interface Climate{

    public String getName();

    public void setName(String name);

    public int getClimateId();

    public void setClimateId(int terrainId);
    
}
