package com.keene.mepbm.model;

public interface Game {

    public int getGameId();

    public void setGameId(int gameId);

    public int getNumber();

    public void setNumber(int gameNumber);

    public String getModule();

    public void setModule(String gameModule);

    public String getDescription();

    public void setDescription(String description);
}
