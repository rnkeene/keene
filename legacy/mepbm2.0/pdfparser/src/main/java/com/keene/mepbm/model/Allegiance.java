package com.keene.mepbm.model;

public interface Allegiance {

    public int getAllegianceId();

    public void setAllegianceId(int allegianceId);

    public String getAllegianceName();

    public void setAllegianceName(String allegianceName);

}
