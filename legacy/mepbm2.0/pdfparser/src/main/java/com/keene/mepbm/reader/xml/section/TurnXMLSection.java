package com.keene.mepbm.reader.xml.section;

import com.keene.mepbm.core.Section;
import com.keene.mepbm.reader.xml.XMLReaderData;

public abstract class TurnXMLSection extends Section<XMLReaderData> {

    public TurnXMLSection() {
    }

}
