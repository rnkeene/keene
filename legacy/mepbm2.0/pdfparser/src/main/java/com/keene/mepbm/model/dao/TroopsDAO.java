package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.Troops;

public interface TroopsDAO<T extends Troops> extends DAO<T> {

}
