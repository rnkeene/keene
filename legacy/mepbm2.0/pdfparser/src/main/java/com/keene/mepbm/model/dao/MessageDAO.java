package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.Message;

public interface MessageDAO<N extends Message> extends DAO<N>{

}
