package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.VictoryPoints;

public interface VictoryPointsDAO<V extends VictoryPoints> extends DAO<V> {

}
