package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.ResourceDetails;

public interface ResourceDetailsDAO<R extends ResourceDetails> extends DAO<R>{
}
