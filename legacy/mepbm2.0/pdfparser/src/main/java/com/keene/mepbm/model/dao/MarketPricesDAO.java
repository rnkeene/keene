package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.MarketPrices;

public interface MarketPricesDAO<M extends MarketPrices> extends DAO<M> {

}
