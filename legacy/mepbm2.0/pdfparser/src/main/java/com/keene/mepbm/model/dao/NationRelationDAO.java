package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.NationRelation;

public interface NationRelationDAO<N extends NationRelation> extends DAO<N>{

}
