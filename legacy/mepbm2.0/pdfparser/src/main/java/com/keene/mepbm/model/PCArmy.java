package com.keene.mepbm.model;

public interface PCArmy{

    public String getName();

    public void setName(String name);

    public int getPCArmyId();

    public void setPCArmyId(int pcArmyId);
    
}
