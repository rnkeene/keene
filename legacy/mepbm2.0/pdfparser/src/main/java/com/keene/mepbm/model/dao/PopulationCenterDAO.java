package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.PopulationCenter;

public interface PopulationCenterDAO<P extends PopulationCenter> extends DAO<P>{
}
