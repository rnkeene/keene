package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.Order;

public interface OrderDAO<O extends Order> extends DAO<O>{

}
