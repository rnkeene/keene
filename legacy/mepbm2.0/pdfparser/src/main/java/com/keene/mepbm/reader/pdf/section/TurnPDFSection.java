package com.keene.mepbm.reader.pdf.section;

import com.keene.mepbm.core.Section;
import com.keene.mepbm.reader.pdf.PDFReaderData;

public abstract class TurnPDFSection extends Section<PDFReaderData> {

    public TurnPDFSection() {
    }
}
