package com.keene.mepbm.model;

public interface Terrain{

    public String getName();

    public void setName(String name);

    public int getTerrainId();

    public void setTerrainId(int terrainId);
    
}
