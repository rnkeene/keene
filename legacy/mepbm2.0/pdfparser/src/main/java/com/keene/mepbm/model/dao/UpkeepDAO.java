package com.keene.mepbm.model.dao;

import com.keene.dao.DAO;
import com.keene.mepbm.model.Upkeep;

public interface UpkeepDAO<U extends Upkeep> extends DAO<U>{

}
