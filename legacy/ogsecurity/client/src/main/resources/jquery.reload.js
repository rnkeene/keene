/**
 *  @description Allows you to reload specific Js or Css during developpement phase
 *  Ease you towards developpement and prevent you from hitting F5 on each change
 *  @author Jp Siffert
 *  @email plugin hostedAt chezouam.fr feel free to send comments
 *  @version 1.0
 *  @param object optional object of options, see $.fn.reload.defaults
 *  @return the jQuery object for chaining
 *  @require identify plugin (example : http://plugins.jquery.com/project/identifyII)
 *  @example $('LINK').reload({interval: 10000});
 *  $('script[src$=debug.js]').reload();
 */

(function($){
    $.fn.reload = function (options){
        var opts = $.extend(true,{}, $.fn.reload.defaults, options);
        var _this = this;

        function getVoidUrl(url){

            if(opts.preventCache=="false")
                return url;

            if(opts.cacheVoidArg==null || opts.cacheVoidArg=="") opts.cacheVoidArg="void";

            if (url.indexOf('?')==-1)
                url = url+'?'+opts.cacheVoidArg+'='+(Date.parse(new Date()));
            else
                url = url+'&'+opts.cacheVoidArg+'='+(Date.parse(new Date()));

            return(url);
        }

        function _reload(obj){
            obj.each(function(){
                $this = $(this);

                var id = $this.identify({prefix:'debugStyleReloader'}).attr('id');

                if($this.attr('tagName')=='LINK'){
                    $.get(getVoidUrl($this.attr('href')),[],function(css){
                        // IE needs remove and insert, no simple ajax load in container
                        $('#'+id).remove();
                        $('body:first').append('<style id="'+id+'">'+css+'</style>');
                    });
                }

                if($this.attr('tagName')=='SCRIPT'){
                    $.getScript(getVoidUrl($this.attr('src')),function(){
                    });
                }
            });
         }

        if(opts.interval==0)
            _reload(_this);
        else
            setInterval(function(){_reload(_this)}, opts.interval);

        return _this;
    }

 $.fn.reload.defaults = {
      interval : 0              // Set the interval for automatique refresh
      ,preventCache: true       // Try to prevent cache
      ,cacheVoidArg: 'void'     // Void argument passed to try to avoid cache
 }
})(jQuery);