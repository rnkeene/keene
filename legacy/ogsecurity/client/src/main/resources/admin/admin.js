$(document).ready(function(){
    $('body').prepend("<div id='adminToolbar'></div>");
    $('#adminToolbar').load("ogclient/admin/admin.html");
});

function createUserOrRole(type) {
    $('body').append("<div id='createCb'></div>");
    $("#createCb").colorbox({href:"ogclient/admin/" + type + "/create.html", open:true });
    if ($('#usernames').html() != null) {
        $.post('/ogsecurity/admin/subjects', function(data) {
            $('#usernames').html(data);
        });
    }
    if ($('#principals').html() != null) {
        $.post('/ogsecurity/admin/principals', function(data) {
            $('#principals').html(data);
        });
    }
    setTimeout('$("#createCb").colorbox.resize();',500);
    //$('#createCb').colorbox.resize();
}

function createPrincipal() {
    $.post('ogclient/admin/principal/' + $("#principal").val(), function(data) {
        $('#create').html(data);
        setTimeout('$("#createCb").colorbox.resize();',200);
    });
}