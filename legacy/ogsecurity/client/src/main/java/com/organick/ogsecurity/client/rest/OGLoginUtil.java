/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.client.rest;

import com.organick.ogsecurity.Constants;
import com.organick.ogsecurity.OGToken;
import com.organick.ogsecurity.http.MethodType;
import com.organick.ogsecurity.http.Post;
import com.organick.ogsecurity.js.JavascriptUtil;
import com.organick.ogsecurity.js.ReadFile;
import com.organick.ogsecurity.js.StatusCode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author rnkeene
 */
public class OGLoginUtil {

    public static String getClientTokenFromServerForUser(String sessionId, String domain) {
        try {
            return Post.excutePost("http://" + domain + Constants.OG_CLIENT_TOKEN_BASE.toString() + sessionId + "/");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "INVALID";
    }

    public static String getUserTokenForWebpage(String sessionId) {
        StringBuilder builder = new StringBuilder();
        builder.append("var userImage='");
        builder.append(sessionId);
        builder.append("';");
        return builder.toString();
    }

    public static String buildAuthJS(ClassLoader classLoader) {
        StringBuilder builder = new StringBuilder();

        Map<String, String> requestParams = new HashMap<String, String>();
        //requestParams.put(Constants.J_USERNAME.toString(), "$('#username').val()");
        requestParams.put(Constants.J_USERNAME.toString(), "userImage");
        requestParams.put(Constants.J_PASSWORD.toString(), "$('#password').val()");

        Map<StatusCode, List<String>> functions = new HashMap<StatusCode, List<String>>();
        List<String> successFunction = new ArrayList<String>();
        successFunction.add(ReadFile.readFileFromClasspath("authFunction.js", classLoader));

        functions.put(StatusCode.OK, successFunction);
        builder.append(JavascriptUtil.buildLoadJS(Constants.J_SECURITY_CHECK_ACTION.toString(), requestParams, functions, MethodType.POST));

        return builder.toString();
    }

    public static String buildServerCall(String userId, String server, String domain, String context, ClassLoader classLoader) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(JavascriptUtil.buildLoadJS("http://" + domain + Constants.OG_USER_TOKEN_BASE.toString() + userId + "/" + server + "/" + context + "/", MethodType.POST));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    public static String buildCreateJS() {
        StringBuilder builder = new StringBuilder();

        Map<String, String> requestParams = new HashMap<String, String>();
        //requestParams.put("firstName", "$('#firstName').val()");
        //requestParams.put("lastName", "$('#lastName').val()");
        //requestParams.put("email", "$('#email').val()");
        requestParams.put("username", "$('#username').val()");
        requestParams.put("password", "$('#password').val()");

        Map<StatusCode, List<String>> functions = new HashMap<StatusCode, List<String>>();

        List<String> successFunction = new ArrayList<String>();
        successFunction.add("submitLogin();");
        functions.put(StatusCode.OK, successFunction);

        List<String> errorFunction = new ArrayList<String>();
        errorFunction.add("$('#loading2').hide();$('#lerror').show();");
        functions.put(StatusCode.UNAUTHORIZED, errorFunction);
        try {
            builder.append(JavascriptUtil.buildLoadJS(Constants.ACCOUNT_CREATE.toString(), requestParams, functions, MethodType.POST));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return builder.toString();
    }
    public static String buildUsernameJS() {
        StringBuilder builder = new StringBuilder();
        builder.append("var url='");
        builder.append(Constants.SIGNON.toString());
        builder.append("/'+userImage+'/'+$('#username').val()+'/';\n");

        try {
            builder.append(JavascriptUtil.buildLoadJSWithVar("url", MethodType.POST));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    public static String buildTokenCredentials(OGToken token) {
        return "/" + token.getUserClientId() + "/" + token.getUserServerId() + "/" + token.getServerId() + "/" + token.getClientId();
    }
}
