/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.client.rest;

import com.organick.ogsecurity.Constants;
import com.organick.ogsecurity.client.OGClientToken;
import com.organick.ogsecurity.http.Post;
import com.organick.ogsecurity.js.ReadFile;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author rnkeene
 */
@Path("ogclient/admin")
public class OGAdminRest extends OGClientBaseRest {
    
    @GET
    @Path("/admin.html")
    @Produces("text/plain")
    public Response adminToolbar() throws Exception {
        System.out.println("OGAdminRest.adminToolbar()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        StringBuilder builder = new StringBuilder();
        if (token != null && token.isAuthenticated()) {
            //TODO: check if admin
            builder.append(ReadFile.readFileFromClasspath(Constants.ADMIN_HTML.toString(), this.getClass().getClassLoader()));
            //System.out.println(builder.toString());
            return Response.ok(builder.toString()).build();
        }
        return Response.ok("ERROR").build();
    }

    @GET
    @Path("/{type}/create.html")
    @Produces("text/plain")
    public Response createUserOrRole(@PathParam("type") String type) throws Exception {
        System.out.println("OGAdminRest.createUserOrRole()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        StringBuilder builder = new StringBuilder();
        if (token != null && token.isAuthenticated()) {
            //TODO: check if admin
            if (type.equals(Constants.SUBJECT.toString())) {
                builder.append(ReadFile.readFileFromClasspath(Constants.ADMIN_CREATE_SUBJECT.toString(), this.getClass().getClassLoader()));
            } else if (type.equals(Constants.PRINCIPAL.toString())) {
                builder.append(ReadFile.readFileFromClasspath(Constants.ADMIN_CREATE_PRINCIPAL.toString(), this.getClass().getClassLoader()));
            } else if (type.equals(Constants.SUBJECTPRINCIPAL.toString())) {
                builder.append(ReadFile.readFileFromClasspath(Constants.ADMIN_CREATE_SUBJECTPRINCIPAL.toString(), this.getClass().getClassLoader()));
            }
            //System.out.println(builder.toString());
            return Response.ok(builder.toString()).build();
        }
        return Response.ok("ERROR").build();
    }

    @POST
    @Path("/principal/{name}")
    @Produces("text/html")
    public Response createPrincipal(@PathParam("name") String name) throws Exception {
        System.out.println("OGAdminRest.createPrincipal()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        String response = Constants.ERROR_HTML.toString();
        if (token != null && token.isAuthenticated()) {
            response = Post.excutePost("http://" + request.getServerName() + "/ogsecurity/admin/principal/" + name + OGLoginUtil.buildTokenCredentials(token));
        }
        return Response.ok(response).build();
    }
}
