/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.client;

import com.organick.ogsecurity.OGToken;
import com.organick.ogsecurity.client.rest.OGLoginUtil;
import java.io.Serializable;

/**
 *
 * @author rnkeene
 */
public class OGClientToken extends OGToken implements Serializable {

    public OGClientToken() {
        this.ready = false;
    }

    public void preloginUser(String sessionId, String domain) {
        this.userClientId = sessionId;
        this.clientId = OGLoginUtil.getClientTokenFromServerForUser(sessionId, domain);
        this.prelogin = true;
        this.state = LoginState.PRELOGIN;
        this.authenticated = false;
        this.error = false;
        this.attempts++;
    }
    public void preloginServer( String userId, String sessionId) {
        this.userServerId = userId;
        this.serverId = sessionId;        
        this.ready = true;
    }

    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tOGClientToken INFO:\n");
        toString.append(super.toString());
        return toString.toString();
    }
}
