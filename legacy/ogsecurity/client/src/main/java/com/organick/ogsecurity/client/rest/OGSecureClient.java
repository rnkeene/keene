package com.organick.ogsecurity.client.rest;

import com.organick.ogsecurity.Constants;
import com.organick.ogsecurity.OGToken;
import com.organick.ogsecurity.client.OGClientToken;
import com.organick.ogsecurity.js.ReadFile;
import java.io.InputStream;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("ogclient")
public class OGSecureClient extends OGClientBaseRest {

    
    @GET
    @Path("load.css")
    @Produces("text/css")
    public Response loadCSS() throws Exception {
        System.out.println("OGSecureClient.loadCSS()");
        HttpSession session = request.getSession();
        StringBuilder builder = new StringBuilder();
        builder.append(ReadFile.readFileFromClasspath(Constants.JQUERY_POPUP_MODAL_STYLE.toString(), this.getClass().getClassLoader()));
        builder.append(ReadFile.getFilesToRead(ReadFile.getBaseUrl(request) + "/style", context));
        builder.append(ReadFile.readFileFromClasspath(Constants.ADMIN_STYLE.toString(), this.getClass().getClassLoader()));
        return Response.ok(builder.toString()).build();
    }

    @GET
    @Path("load.js")
    @Produces("text/javascript")
    public Response loadScripts() throws Exception {
        System.out.println("OGSecureClient.loadScripts()");
        HttpSession session = request.getSession();
        StringBuilder builder = new StringBuilder();
        builder.append(ReadFile.readFileFromClasspath(Constants.JQUERY.toString(), this.getClass().getClassLoader()));
        builder.append(ReadFile.readFileFromClasspath(Constants.JQUERY_POPUP_MODAL_SCRIPT.toString(), this.getClass().getClassLoader()));
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        if (token != null) {
            if (token.isAuthenticated()) {
                //TODO: check if admin
                builder.append(ReadFile.readFileFromClasspath(Constants.ADMIN_SCRIPT.toString(), this.getClass().getClassLoader()));
                builder.append(ReadFile.getFilesToRead(ReadFile.getBaseUrl(request) + "/script", context));
                System.out.println("Loading!");
                return Response.ok(builder.toString()).build();
            } else if (token.isPrelogin()) {
                token.addAttempt();
            }
        } else {
            token = new OGClientToken();
            token.preloginUser(session.getId(), request.getServerName());
            context.setAttribute(token.getUserClientId(), token);
            System.out.println("NEW CLIENT TOKEN CREATED:");
            System.out.println(token);
        }
        builder.append(ReadFile.readFileFromClasspath(Constants.JQUERY_IDENTIFY.toString(), this.getClass().getClassLoader()));
        builder.append(ReadFile.readFileFromClasspath(Constants.JQUERY_RELOAD.toString(), this.getClass().getClassLoader()));
        builder.append(OGLoginUtil.getUserTokenForWebpage(token.getUserClientId()));
        builder.append(this.buildSecureLoadJS());
        builder.append(OGLoginUtil.buildServerCall(token.getUserClientId(), request.getServerName(), request.getServerName(), request.getContextPath().substring(1, request.getContextPath().length()), this.getClass().getClassLoader()));
        //System.out.println(builder.toString());
        return Response.ok(builder.toString()).build();
    }

    @GET
    @Path("/login")
    @Produces("text/plain")
    public Response loginPage() throws Exception {
        System.out.println("OGSecureClient.loginPage()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        StringBuilder builder = new StringBuilder();
        if (token.isPrelogin()) {
            builder.append(ReadFile.readFileFromClasspath("index.html", this.getClass().getClassLoader()));
            //System.out.println(builder.toString());
            return Response.ok(builder.toString()).build();
        }
        return Response.ok("ERROR").build();
    }

    @GET
    @Path("/create")
    @Produces("text/plain")
    public Response create() throws Exception {
        System.out.println("OGSecureClient.create()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        StringBuilder builder = new StringBuilder();
        if (token.isPrelogin()) {
            builder.append(ReadFile.readFileFromClasspath("create.html", this.getClass().getClassLoader()));
            //System.out.println(builder.toString());
            return Response.ok(builder.toString()).build();
        }
        return Response.ok("ERROR").build();

    }

    @GET
    @Path("/auth")
    @Produces("text/plain")
    public Response auth() throws Exception {
        System.out.println("OGSecureClient.auth()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        StringBuilder builder = new StringBuilder();
        if (token.isPrelogin()) {
            builder.append(ReadFile.readFileFromClasspath("login.html", this.getClass().getClassLoader()));
            //System.out.println(builder.toString());
            return Response.ok(builder.toString()).build();
        }
        return Response.ok("ERROR").build();
    }

    @GET
    @Path("loading.gif")
    @Produces("image/gif")
    public void loadingImage() throws Exception {
        InputStream in = this.getClass().getClassLoader().getResourceAsStream(Constants.LOADING_IMAGE.toString());
        int nextChar;
        while ((nextChar = in.read()) != -1) {
            response.getOutputStream().write(nextChar);
        }
        response.getOutputStream().flush();
    }

    @GET
    @Path("/home.js")
    @Produces("text/javascript")
    public Response getHomeJs() throws Exception {
        System.out.println("OGSecureClient.getHomeJs()");
        HttpSession session = request.getSession();
        OGClientToken token = (OGClientToken) context.getAttribute(session.getId());
        if (token.isPrelogin()) {
            String js = ReadFile.readFileFromClasspath("home.js", this.getClass().getClassLoader());
            if (request.getAttribute("create") != null) {
                js = js.replace("[[[ LOAD_CREATE_OR_LOGIN ]]]", "loadCreate();");
            } else {
                js = js.replace("[[[ LOAD_CREATE_OR_LOGIN ]]]", "loadLogin();");
            }
            if (token != null && !token.isAllowCreate()) {
                js = js.replace("[[[ CREATE_LINK ]]]", "setTimeout('$(\"#createLink\").html(\"\");', 300);");
            } else {
                js = js.replace("[[[ CREATE_LINK ]]]", "");
            }
            //System.out.println(js.toString());
            return Response.ok(js.toString()).build();
        }
        return Response.ok("ERROR").build();
    }

    private String buildSecureLoadJS() {
        String js = ReadFile.readFileFromClasspath(Constants.LOCAL_SECURITY.toString(), this.getClass().getClassLoader());
        js = js.replace("[[[ LOGINTOKEN ]]]", OGLoginUtil.buildAuthJS(this.getClass().getClassLoader()));
        js = js.replace("[[[ SIGNON ]]]", OGLoginUtil.buildUsernameJS());
        js = js.replace("[[[ CREATETOKEN ]]]", OGLoginUtil.buildCreateJS());
        return js;
    }

    @POST
    @Path("/identify/server/{userId}/{userServerId}/{clientId}/")
    @Produces("text/javascript")
    public Response identifyOGServer(@PathParam("userId") String userId, @PathParam("userServerId") String userServerId, @PathParam("clientId") String clientId) throws Exception {
        System.out.println("OGSecureClient.identifyOGServer()");
        HttpSession session = request.getSession(true);
        System.out.println("\tFinding userId: " + userId);
        System.out.println("\tuserServerId: " + userServerId);
        System.out.println("\tclientId: " + clientId);
        OGClientToken token = (OGClientToken) context.getAttribute(userId);
        if (token != null) {
            System.out.println("\tfound a token");
            System.out.println(token);
            try {
                System.out.println("\tuserServerId: '" + clientId + "'");
                System.out.println("\tuserServerId: '" + token.getClientId() + "'");
                if (token.getClientId().equals(clientId)) {
                    System.out.println("\tclient ID's match");
                    token.preloginServer(userServerId, session.getId());
                    token.setReady(true);
                    System.out.println("=== CLIENT READY FOR LOGIN ===");
                    System.out.println(token);
                    return Response.ok(token.getServerId()).build();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //System.out.println("ERROR");
        return Response.ok("ERROR").build();
    }

    @POST
    @Path("/client/signon/{userId}/{username}/")
    @Produces("text/javascript")
    public Response setUsername(@PathParam("userId") String userId, @PathParam("username") String username) throws Exception {
        System.out.println("\tOGServerRest.setUsername()");
        HttpSession session = request.getSession(true);
        OGClientToken token = (OGClientToken) context.getAttribute(userId);
        if (token != null) {
            System.out.println("SETTING username: " + username);
            token.setUsername(username);
            System.out.println(token);
            return Response.ok().build();
        }else{
            System.out.println("INVALID TOKEN - Could not set username for signon -- ERROR ERROR!!");
        }
        return Response.ok().build();
    }

    @POST
    @Path("/client/username/{userClientId}/{userServerId}/{serverId}/{clientId}")
    @Produces("text/javascript")
    public Response getUsername(@PathParam("userClientId") String userClientId, @PathParam("userServerId") String userServerId, @PathParam("serverId") String serverId, @PathParam("clientId") String clientId) throws Exception {
        System.out.println("\tOGServerRest.getUsername()");
        HttpSession session = request.getSession(true);
        OGClientToken token = (OGClientToken) context.getAttribute(userClientId);
        if (token != null && token.getUsername() != null) {
            if (token.verifyToken(userClientId, userServerId, serverId, clientId)) {
                return Response.ok(token.getUsername()).build();
            }
        } else {
            System.out.println("\tUsername is null!");
        }
        return Response.ok().build();
    }

    @POST
    @Path("/login/{userClientId}/{userServerId}/{serverId}/{clientId}")
    @Produces("text/javascript")
    public Response verifyUser(@PathParam("userClientId") String userClientId, @PathParam("userServerId") String userServerId, @PathParam("serverId") String serverId, @PathParam("clientId") String clientId) throws Exception {
        System.out.println("OGSecureClient.identifyOGServer()");
        HttpSession session = request.getSession(true);
        System.out.println("Finding: " + userClientId);
        OGClientToken token = (OGClientToken) context.getAttribute(userClientId);
        if (token != null) {
            if (token.getUserServerId().equals(userServerId) && token.getServerId().equals(serverId) && token.getClientId().equals(clientId)) {
                token.setState(OGToken.LoginState.AUTHENTICATED);
                System.out.println("=== CLIENT AUTHENTICATED ===");
                System.out.println(token);
                return Response.ok(token.getServerId()).build();
            }else {
                System.out.println("\tID's did not match!");
                System.out.println("\t"+token.getUserServerId() + " != " + userServerId +" && "+ token.getServerId() + " != " + serverId  + " && " + token.getClientId()+ " != " + clientId);
            }
        }else{
            System.out.println("TOKEN IS NULL FOR : " + userClientId);
        }
        //System.out.println("ERROR");
        return Response.ok("ERROR").build();
    }
}
