/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity;

/**
 *
 * @author matt
 */
public enum Constants {

    J_USERNAME("j_username"),
    J_PASSWORD("j_password"),
    PASSWORD("password"),
    SUBJECT("subject"),
    PRINCIPAL("principal"),
    SUBJECTPRINCIPAL("subjectprincipal"),
    SERVER_IDENTIFY("/ogsecurity/server/identify/client/user"),
    //J_SECURITY_CHECK_ACTION("/ogsecurity/ident/login"),
    J_SECURITY_CHECK_ACTION("/ogsecurity/ident/server/login/"),
    SIGNON("ogclient/client/signon"),
    ACCOUNT_CREATE("/ogsecurity/ident/account/create"),
    SERVER_ID("serverId"),
    USER_ID("userId"),
    CLIENT_ID("clientId"),
    LOADING_IMAGE("loading.gif"),
    LOCAL_SECURITY("load.js"),
    JQUERY("jquery.min.js"),
    JQUERY_POPUP_MODAL_STYLE("colorbox.css"),
    JQUERY_POPUP_MODAL_SCRIPT("jquery.colorbox.js"),
    JQUERY_RELOAD("jquery.reload.js"),
    JQUERY_IDENTIFY("jquery.identify.js"),
    ADMIN_STYLE("admin/admin.css"),
    ADMIN_SCRIPT("admin/admin.js"),
    ADMIN_HTML("admin/admin.html"),
    ADMIN_CREATE_SUBJECT("admin/createSubject.html"),
    ADMIN_CREATE_PRINCIPAL("admin/createPrincipal.html"),
    ADMIN_CREATE_SUBJECTPRINCIPAL("admin/createSubjectPrincipal.html"),
    OG_USER_TOKEN_BASE("/ogsecurity/ident/server/identify/user/browser/"),
    SUCCESS_HTML("<div class='success'>Success!</div>"),
    ERROR_HTML("<div class='error'>Error!</div>"),
    OG_CLIENT_TOKEN_BASE("/ogsecurity/ident/server/identify/client/user/");

    
    private final String name;

    Constants(String name) {
        this.name = name;
    }

    /*public String getName() {
    return name;
    }*/
    @Override
    public String toString() {
        return name;
    }
}
