/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class OGHashUtil {

    private static MessageDigest md;

    static {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
    }

    public static String hash(String source) {
        try {
            byte[] bytes = md.digest(source.getBytes());
            return getString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getString(byte[] bytes) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            sb.append((int) (0x00FF & b));
        }
        return sb.toString();
    }
}
