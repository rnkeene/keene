/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.account;

import java.io.Serializable;
import java.security.Principal;

/**
 *
 * @author rnkeene
 */
public class OGSubject implements Principal, Serializable {

    private String name;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
