/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.account;

import com.organick.ogsecurity.account.OGPrincipal;
import com.organick.ogsecurity.account.OGSubject;
import java.io.Serializable;

/**
 *
 * @author rnkeene
 */
public class OGAuth  implements Serializable{

    private OGPrincipal principal;
    private OGSubject subject;

    public OGAuth() {
    }

    public OGPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(OGPrincipal principal) {
        this.principal = principal;
    }

    public OGSubject getSubject() {
        return subject;
    }

    public void setSubject(OGSubject subject) {
        this.subject = subject;
    }
}
