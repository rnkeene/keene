/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.account;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rnkeene
 */
@XmlRootElement
public class Account implements Principal,Serializable {

    private List<OGSubject> subjects;
    private List<OGPrincipal> principals;
    private Properties props;

    public List<OGSubject> getSubjects() {
        if (subjects == null) {
            subjects = new ArrayList<OGSubject>();
        }

        return subjects;
    }

    public List<OGPrincipal> getPrincipals() {
        if (principals == null) {
            principals = new ArrayList<OGPrincipal>();
        }

        return principals;
    }

    @Override
    @XmlElement
    public String getName() {
        return getProperty("username");
    }

    @XmlAnyAttribute
    public Properties getProperties() {
        return props;
    }
    
    /*@XmlElement
    public List getElements() {
        List<Map> elements = new ArrayList<Map>();

        for (Object obj : props.keySet())
        {
            Map map = new HashMap();
            map.put(obj, props.get(obj));
            elements.add(map);
        }
        return elements;
    }*/

    public String getProperty(String key) {
        initProperties();

        return props.getProperty(key);
    }

    public void setProperty(String key, String property) {
        initProperties();
        
        props.setProperty(key, property);
    }

    private void initProperties() {
        if (props == null) {
            props = new Properties();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Account{\n");
        builder.append("\tsubjects={\n");
        if (subjects != null) {
            for (OGSubject subject : subjects) {
                builder.append("\t\t");
                builder.append(subject.toString());
                builder.append("\n");
            }
        }
        builder.append("\t}\n");
        builder.append("\tprincipals={\n");
        if (principals != null) {
            for (OGPrincipal principal : principals) {
                builder.append("\t\t");
                builder.append(principal.toString());
                builder.append("\n");
            }
        }
        builder.append("\t}\n");
        builder.append("\tprops={\n");
        if (props != null) {
            for (Object property : props.keySet()) {
                builder.append("\t\t");
                builder.append(property);
                builder.append(": ");
                builder.append(props.get(property));
                builder.append("\n");
            }
        }
        builder.append("\t}\n");
        builder.append("}\n");

        return builder.toString();
    }


}
