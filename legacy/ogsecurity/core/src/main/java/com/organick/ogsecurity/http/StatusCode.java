/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.http;

/**
 *
 * @author rnkeene
 */
public enum StatusCode {
    OK(200),
    UNAUTHORIZED(401),
    NOTFOUND(404);

    private int code;

    StatusCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
