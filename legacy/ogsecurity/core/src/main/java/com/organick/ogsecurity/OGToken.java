/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity;

import com.organick.ogsecurity.account.Account;
import java.io.Serializable;

/**
 *
 * @author rnkeene
 */
public class OGToken implements Serializable {


    protected String username;
    protected String serverId;
    protected String clientId;
    protected LoginState state;
    protected int attempts = 0;
    protected boolean allowCreate = true;
    protected boolean authenticated;
    protected boolean prelogin;
    protected boolean error;
    protected String userClientId;
    protected String userServerId;
    protected boolean ready;
    protected Account account;

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public String getUserClientId() {
        return userClientId;
    }

    public void setUserClientId(String userClientId) {
        this.userClientId = userClientId;
    }

    public String getUserServerId() {
        return userServerId;
    }

    public void setUserServerId(String userServerId) {
        this.userServerId = userServerId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String appId) {
        this.clientId = appId;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public LoginState getState() {
        return state;
    }

    public void setState(LoginState state) {
        this.state = state;
        if (this.state.equals(LoginState.AUTHENTICATED)) {
            this.authenticated = true;
            this.prelogin = false;
            this.error = false;
        } else if (this.state.equals(LoginState.PRELOGIN)) {
            this.prelogin = true;
            this.authenticated = false;
            this.error = false;
        } else {
            this.authenticated = false;
            this.prelogin = false;
            this.error = true;
        }
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public void addAttempt() {
        this.attempts++;
    }

    public boolean isAllowCreate() {
        return allowCreate;
    }

    public void setAllowCreate(boolean allowCreate) {
        this.allowCreate = allowCreate;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void authenticate() {
        this.authenticated = true;
        this.state = LoginState.AUTHENTICATED;
        this.error = false;
        this.prelogin = false;
    }

    public void invalidate() {
        this.authenticated = false;
        this.state = LoginState.PRELOGIN;
        this.error = false;
        this.prelogin = true;
    }

    public boolean isError() {
        return error;
    }

    public boolean isPrelogin() {
        return prelogin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean verifyToken(String userClientId, String userServerId, String serverId, String clientId) {
        if (this.getUserServerId().equals(userServerId) && this.getServerId().equals(serverId) && this.getClientId().equals(clientId) && this.getUserClientId().equals(userClientId)) {
                return true;
            }else {
                System.out.println("\tID's did not match!");
                System.out.println("\t"+this.getUserServerId() + " != " + userServerId +" && "+ this.getServerId() + " != " + serverId  + " && " + this.getClientId()+ " != " + clientId);
            }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();

        toString.append("\t\tstate: ");
        toString.append(this.state.toString());
        toString.append("\n");

        toString.append("\t\tserverId: ");
        toString.append(this.serverId);
        toString.append("\n");

        toString.append("\t\tclientId: ");
        toString.append(this.clientId);
        toString.append("\n");

        toString.append("\t\tuserClientId: ");
        toString.append(this.userClientId);
        toString.append("\n");

        toString.append("\t\tuserServerId: ");
        toString.append(this.userServerId);
        toString.append("\n");

        toString.append("\t\tusername: ");
        toString.append(this.username);
        toString.append("\n");

        toString.append("\t\tauthenticated: ");
        toString.append(this.authenticated);
        toString.append("\n");

        toString.append("\t\tallow create: ");
        toString.append(this.allowCreate);
        toString.append("\n");

        toString.append("\t\tattempts: ");
        toString.append(this.attempts);
        toString.append("\n");

        toString.append("\t\tready: ");
        toString.append(this.ready);
        toString.append("\n");

        return toString.toString();
    }

    public enum LoginState {

        PRELOGIN("prelogin"),
        AUTHENTICATED("authenticated"),
        ERROR("error");
        private final String state;

        LoginState(String state) {
            this.state = state;
        }

        public String toString() {
            return this.state;
        }
    }
}
