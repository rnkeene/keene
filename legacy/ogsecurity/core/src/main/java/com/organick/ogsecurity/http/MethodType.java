/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.http;

/**
 *
 * @author rnkeene
 */
public enum MethodType {

    GET("get"),
    POST("post"),
    PUT("put");

    private final String name;

    MethodType(String name){
        this.name = name;
    }

    public String getName(){
        return name.toUpperCase();
    }



}
