/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author rnkeene
 */
public class Post {

    public static String excutePost(String targetURL) {
        StringBuilder response = new StringBuilder();
        URL url;
        URLConnection conn;
        //System.out.println("About to post to : " + targetURL);
        try {
            // Send data
            url = new URL(targetURL);
            conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;            
            while ((line = rd.readLine()) != null) {
                //System.out.println("ADDING: " + line);
                response.append(line);
                //response.append("\r");
            }
            wr.close();
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
           return null;
        }
        return response.toString();
    }
}
