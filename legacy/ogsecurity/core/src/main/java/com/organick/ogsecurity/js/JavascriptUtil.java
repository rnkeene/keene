/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.js;

import com.organick.ogsecurity.http.MethodType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author rnkeene
 */
public class JavascriptUtil {

    public static String buildLoadJS(String url) throws Exception {
        return buildLoadJS(url, new HashMap<String, String>(), new HashMap<StatusCode, List<String>>(), MethodType.GET);
    }

    public static String buildLoadJS(String url, String successFunction, String errorFunction) throws Exception {
        Map<StatusCode, List<String>> functions = new HashMap<StatusCode, List<String>>();
        if (successFunction != null) {
            List<String> successList = new ArrayList<String>();
            successList.add(successFunction);
            functions.put(StatusCode.OK, successList);
        }
        if (errorFunction != null) {
            List<String> errorList = new ArrayList<String>();
            errorList.add(errorFunction);
            functions.put(StatusCode.UNAUTHORIZED, errorList);
        }
        return buildLoadJS(url, new HashMap<String, String>(), functions, MethodType.GET);
    }

    public static String buildLoadJS(String url, Map<String, String> requestParams, Map<StatusCode, List<String>> functions, MethodType type) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(JavascriptUtil.evaluateMethod(type));
            builder.append(JavascriptUtil.wrapURL(url));
            builder.append(JavascriptUtil.appendParams(requestParams));
            builder.append(JavascriptUtil.appendFunctions(functions));
            builder.append(");\n");
        } catch (Exception e) {
        }
        return builder.toString();
    }

    public static String buildLoadJS(String url, Map<String, String> requestParams, MethodType type) throws Exception {
        StringBuilder builder = new StringBuilder();
        builder.append(JavascriptUtil.evaluateMethod(type));
        builder.append(JavascriptUtil.wrapURL(url));
        builder.append(JavascriptUtil.appendParams(requestParams));
        builder.append(JavascriptUtil.defaultFunction());
        return builder.toString();
    }

    public static String buildLoadJS(String url, MethodType type) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(JavascriptUtil.evaluateMethod(type));
            builder.append(JavascriptUtil.wrapURL(url));
            builder.append(JavascriptUtil.defaultFunction());
        } catch (Exception e) {
        }
        return builder.toString();
    }

    public static String buildLoadJSWithVar(String url, MethodType type) {
        StringBuilder builder = new StringBuilder();
        try {
            builder.append(JavascriptUtil.evaluateMethod(type));
            builder.append(url);
            builder.append(");");
        } catch (Exception e) {
        }
        return builder.toString();
    }

    private static String defaultFunction() {
        StringBuilder builder = new StringBuilder();
        builder.append("function( data, status, jqXHR ) {\n");
        builder.append("}\n");
        builder.append(");\n");
        return builder.toString();
    }

    private static String appendFunctions(Map<StatusCode, List<String>> functions) {
        StringBuilder builder = new StringBuilder();
        builder.append("function( data, status, jqXHR ) {\n");
        for (StatusCode status : functions.keySet()) {
            builder.append("if (jqXHR.status==");
            builder.append(status.getCode());
            builder.append(") {\n");
            for (String function : functions.get(status)) {
                builder.append(function + "\n");
            }
            builder.append("}\n");
        }
        builder.append("}");
        return builder.toString();
    }

    private static String appendParams(Map<String, String> requestParams) {
        StringBuilder builder = new StringBuilder();
        if (requestParams.size() > 0) {
            builder.append("{");
            boolean first = true;
            String tmpStr = "";
            for (String key : requestParams.keySet()) {
                builder.append(tmpStr);
                builder.append(key);
                builder.append(": ");
                builder.append(requestParams.get(key));
                if (first) {
                    tmpStr = ",";
                    first = false;
                }
            }
            builder.append("},\n");
        }
        return builder.toString();
    }

    private static String wrapURL(String url) {
        StringBuilder builder = new StringBuilder();
        if (!url.startsWith("'") && !url.startsWith("\"")) {
            //Only add the quotes around URL if they're not already there
            builder.append("'");
        }
        builder.append(url);
        if (!url.startsWith("'") && !url.startsWith("\"")) {
            //Only add the quotes around URL if they're not already there
            builder.append("'");
        }
        builder.append(", ");
        return builder.toString();
    }

    private static String evaluateMethod(MethodType type) {
        StringBuilder builder = new StringBuilder();
        if (type.equals(MethodType.POST)) {
            builder.append("$.post(");
        } else if (type.equals(MethodType.GET)) {
            builder.append("$.get(");
        }
        return builder.toString();
    }
}
