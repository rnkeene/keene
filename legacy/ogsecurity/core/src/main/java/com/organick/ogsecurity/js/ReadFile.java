/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.js;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class ReadFile {

    public static String getBaseUrl(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append("../webapps");
        builder.append(request.getContextPath());
        builder.append("/WEB-INF");
        return builder.toString();
    }

    public static String getFilesToRead(List<String> directories, ServletContext context) {
        List<File> scriptFiles = new ArrayList<File>();
        for (String dir : directories) {
            File folder = new File(dir);
            File[] listOfFiles = folder.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    scriptFiles.add(listOfFiles[i]);
                }
            }
        }
        return readFile(scriptFiles, context);
    }

    public static String getFilesToRead(String directory, ServletContext context) {
        System.out.println("looking in: " + directory);
        List<File> scriptFiles = new ArrayList<File>();
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    scriptFiles.add(listOfFiles[i]);
                }
            }
        }
        return readFile(scriptFiles, context);
    }

    public static String readFile(String fileName, ServletContext context) {
        StringBuilder writer = new StringBuilder();
        try {
            InputStream is = context.getResourceAsStream(fileName);
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);
                String text = "";
                while ((text = reader.readLine()) != null) {
                    writer.append(text);
                    writer.append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public static String readForm(String fileName, ClassLoader loader, Map<String, String> props) {
        InputStream in = loader.getResourceAsStream(fileName);
        return readForm(in, props);
    }

    public static String readForm(String fileName, ServletContext context, Map<String, String> props) {
        InputStream is = context.getResourceAsStream(fileName);
        return readForm(is, props);
    }

    private static String readForm(InputStream is, Map<String, String> props) {
        StringBuilder writer = new StringBuilder();
        try {
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);
                String text = "";
                while ((text = reader.readLine()) != null) {
                    writer.append(text);
                    writer.append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringBuilder formFields = new StringBuilder();
        if (props != null && !props.isEmpty()) {
            formFields.append("function initForm() {");
            for (String property : props.keySet()) {
                if (props.get(property) != null) {
                    formFields.append("$('#");
                    formFields.append(property);
                    formFields.append("').val('");
                    formFields.append(props.get(property));
                    formFields.append("');");
                }
            }
            formFields.append("}");
            formFields.append("initForm();");
        }

        System.out.println("\thtml: " + writer);
        System.out.println("\treplacement js: " + formFields);
        return writer.toString().replace("[[[POPULATEFORM]]]", formFields);
    }

    public static String readFileFromClasspath(String fileName, Class clazz) {
        return readFileFromClasspath(fileName, clazz.getClassLoader());
    }

    public static String readFileFromClasspath(String fileName, ClassLoader loader) {
        StringBuilder writer = new StringBuilder();
        InputStream in = loader.getResourceAsStream(fileName);
        try {
            if (in != null) {
                InputStreamReader isr = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(isr);
                String text = "";
                while ((text = reader.readLine()) != null) {
                    writer.append(text);
                    writer.append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }
    
    public static String readFile(File fileName, ServletContext context) {
        StringBuilder writer = new StringBuilder();
        try {
            InputStream is = new FileInputStream(fileName);
            if (is != null) {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);
                String text = "";
                while ((text = reader.readLine()) != null) {
                    writer.append(text);
                    writer.append("\n");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public static String readFile(List<File> files, ServletContext context) {
        StringBuilder writer = new StringBuilder();
        try {
            for (File fileName : files) {
                InputStream is = new FileInputStream(fileName);
                if (is != null) {
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader reader = new BufferedReader(isr);
                    String text = "";
                    while ((text = reader.readLine()) != null) {
                        writer.append(text);
                        writer.append("\n");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public static String readFile(List<String> urls) {
        StringBuilder builder = new StringBuilder();
        try {
            for (String url : urls) {
                builder.append(readFile(new URL(url)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    public static String readFile(URL url) {
        StringBuilder writer = new StringBuilder();
        try {
            URLConnection webpage = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(webpage.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                writer.append(inputLine);
                writer.append("\n");
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public static Image readImage(String fileName, ServletContext context) {
        System.out.println("ReadFile.readImage()");
        Image image = null;
        try {
            InputStream in = context.getResourceAsStream(fileName);
            image = ImageIO.read(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return image;
    }
}
