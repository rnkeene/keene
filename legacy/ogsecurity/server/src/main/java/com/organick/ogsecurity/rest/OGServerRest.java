package com.organick.ogsecurity.rest;

import com.organick.ogsecurity.Constants;
import com.organick.ogsecurity.OGToken.LoginState;
import com.organick.ogsecurity.account.Account;
import com.organick.ogsecurity.http.Post;
import com.organick.ogsecurity.server.OGServerToken;
import com.organick.persist.Persist;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.springframework.util.StringUtils;

@Path("ident")
public class OGServerRest extends OGServerBaseRest {

    

    @POST
    @Path("/server/identify/client/user/{userId}/")
    @Produces("text/javascript")
    public Response identifyUserAndClientApplication(@PathParam("userId") String userId) throws Exception {
        System.out.println("\tOGServerRest.identifyUserAndClientApplication()");
        HttpSession session = request.getSession(true);
        System.out.println("\t\tLooking for : " + userId);
        OGServerToken token = (OGServerToken) context.getAttribute(userId);
        if (token == null) {
            token = new OGServerToken();
            token.preloginClient(userId, session.getId());
            context.setAttribute(userId, token);
            System.out.println("\ttoken " + token);
            return Response.ok(session.getId()).build();
        }
        return Response.ok(session.getId()).build();
    }

    @POST
    @Path("/server/identify/user/browser/{userId}/{server}/{context}/")
    @Produces("text/javascript")
    public Response identifyBrowserAndUser(@PathParam("userId") String userId, @PathParam("server") String server, @PathParam("context") String path) throws Exception {
        System.out.println("\tOGServerRest.identifyBrowserAndUser()");
        HttpSession session = request.getSession(true);
        System.out.println("\t\tsession ID: " + session.getId());
        OGServerToken token = (OGServerToken) context.getAttribute(userId);
        if (token != null) {
            token.preloginUser(session.getId());
            token.setRedirectURL("http://" + server + "/" + path);
            String response = Post.excutePost(token.getRedirectURL() + "/ogclient/identify/server/" + token.getUserClientId() + "/" + token.getUserServerId() + "/" + token.getClientId() + "/");
            if (!"ERROR".equals(response)) {
                token.setServerId(response);
                token.setReady(true);
                System.out.println("=== SERVER READY FOR LOGIN ===");
                System.out.println(token);
                return Response.ok().build();
            }
        }
        //System.out.println("ERROR");
        return Response.ok("ERROR").build();
    }

    @POST
    @Path("/server/login/")
    @Produces("text/plain")
    public Response login() throws Exception {
        System.out.println("Server Login!");
        String clientTokenId = request.getParameter(Constants.J_USERNAME.toString());
        System.out.println("Client Token Id: " + clientTokenId);
        String pwd = request.getParameter(Constants.J_PASSWORD.toString());
        System.out.println("PWD: " + pwd);
        HttpSession session = request.getSession(true);
        OGServerToken token = (OGServerToken) context.getAttribute(clientTokenId);
        if (token != null) {
            System.out.println(token);
            if (token.isReady()) {
                if (token.getUserClientId().equals(clientTokenId) && session.getId().equals(token.getUserServerId())) {
                    String username = Post.excutePost(token.getRedirectURL() + "/ogclient/client/username/" + clientTokenId + "/" + token.getUserServerId() + "/" + token.getServerId() + "/" + token.getClientId());
                    if (StringUtils.hasLength(username)) {
                        if (token.getRedirectURL().contains("ogtab")) {
                            Account account = Persist.get(username, Account.class.getName());
                            if (account == null || !(account.getProperty(Constants.PASSWORD.toString()) != null && account.getProperty("password").equals(pwd))) {
                                System.out.println("\t\tauthentication failure!");
                                return Response.ok("denied").build();
                            }
                            System.out.println("Succesfully authenticated!");
                            System.out.println(account);
                        } else {
                            if (!username.equals("mepbmuser")) {
                                System.out.println("BAD USERNAME denied!!");
                                return Response.ok("denied").build();
                            } else {
                                System.out.println("password: " + pwd);
                                if (!pwd.equals("mepbm1337")) {
                                    System.out.println("BAD PASSWORD - denied!!");
                                    return Response.ok("denied").build();
                                }
                            }
                        }
                        token.setUsername(username);
                        String response = Post.excutePost(token.getRedirectURL() + "/ogclient/login/" + token.getUserClientId() + "/" + token.getUserServerId() + "/" + token.getServerId() + "/" + token.getClientId());
                        System.out.println("RESPONSE from client: " + response);
                        if (response.equals(token.getServerId())) {
                            token.setState(LoginState.AUTHENTICATED);
                            System.out.println(token);
                            System.out.println("success!!!");
                            return Response.ok("success").build();
                        } else {
                            System.out.println("ERROR AUTHENTICATING WITH CLIENT!");
                            System.out.println(response + " != " + token.getServerId());
                        }
                    } else {
                        System.out.println("ERROR AUTHENTICATING WITH CLIENT!");
                        System.out.println("Username is empty");
                    }
                } else {
                    System.out.println("BAD CLIENT TOKEN ID! or BAD SESSION ID");
                    System.out.println(token.getUserClientId() + "!=" + clientTokenId);
                    System.out.println(session.getId() + "!=" + token.getUserServerId());
                }
            } else {
                System.out.println("TOKEN NOT READY");
                System.out.println(token);
            }
        } else {
            System.out.println("NULL TOKEN!!");
        }
        System.out.println("denied!!");
        return Response.ok("denied").build();
    }

    @POST
    @Path("/account/create")
    @Produces("text/plain")
    public Response createAccount() throws Exception {
        System.out.println("\tOGServerRest.createAccount()");
        //String firstName = request.getParameter("firstName");
        //String lastName = request.getParameter("lastName");
        //String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Account account = new Account();
        //account.setProperty("firstName", firstName);
        //account.setProperty("lastName", lastName);
        //account.setProperty("email", email);
        account.setProperty("username", username);
        account.setProperty("password", password);

        System.out.println(account.toString());
        //TODO: make sure no duplicate username's exist, etc.
        try {
            Persist.save(account, username);
            return Response.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(401).build();
        }
    }
}
