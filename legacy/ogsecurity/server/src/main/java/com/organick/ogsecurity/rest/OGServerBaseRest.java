/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.ogsecurity.rest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

/**
 *
 * @author rnkeene
 */
public class OGServerBaseRest {
    @Context
    protected ServletContext context;
    @Context
    protected HttpServletRequest request;
}
