/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.rest;

import com.organick.ogsecurity.Constants;
import com.organick.ogsecurity.account.OGPrincipal;
import com.organick.ogsecurity.persistence.OGPrincipalList;
import com.organick.ogsecurity.server.OGServerToken;
import com.organick.persist.Persist;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author rnkeene
 */
@Path("admin")
public class OGAdminServerRest extends OGServerBaseRest {

    @POST
    @Path("/subjects")
    @Produces("text/html")
    public Response getSubjects() {
        HttpSession session = request.getSession();
        OGServerToken token = (OGServerToken) context.getAttribute(session.getId());
        if (token != null && token.isAuthenticated()) {
            if (token.isAdmin()) {
            }

        }

        return Response.ok("<select name='subjects'><option value='tom'>Tom</option></select>").build();
    }

    @POST
    @Path("/principals")
    @Produces("text/html")
    public Response getPrincipals() {
        HttpSession session = request.getSession();
        OGServerToken token = (OGServerToken) context.getAttribute(session.getId());
        if (token != null && token.isAuthenticated()) {
            if (token.isAdmin()) {
            }


        }

        return Response.ok("<select name='principals'><option value='tom'>Tom</option></select>").build();
    }

    @POST
    @Path("/principal/{name}/{userClientId}/{userServerId}/{serverId}/{clientId}")
    @Produces("text/html")
    public Response createPrincipal(@PathParam("name") String name, @PathParam("userClientId") String userClientId, @PathParam("userServerId") String userServerId, @PathParam("serverId") String serverId, @PathParam("clientId") String clientId) throws Exception {
        System.out.println("OGAdminServerRest.createPrincipal()");
        try {
            HttpSession session = request.getSession();
            OGServerToken token = (OGServerToken) context.getAttribute(userClientId);
            if (token != null && token.isAuthenticated()) {
                if (token.verifyToken(userClientId, userServerId, serverId, clientId)) {
                    System.out.println("\tfound authenticated and verified token");
                    System.out.println(token);
                    if (token.isAdmin()) {
                        System.out.println("\tfound admin user");
                        OGPrincipalList principalList = Persist.get(token.getRedirectURL() + "principal", OGPrincipalList.class.getName());
                        if (principalList != null) {
                            OGPrincipal principal = new OGPrincipal(name);
                            if (!principalList.contains(principal)) {
                                System.out.println("\tAdding a new principal");
                                principalList.add(principal);
                                Persist.save(principalList, token.getRedirectURL() + "principal");
                                return Response.ok(Constants.SUCCESS_HTML.toString()).build();
                            } else {
                                System.out.println("\tPrincipal already exists");
                            }
                        } else {
                            System.out.println("\tprincipalList is null");
                        }
                    } else {
                        if (name.equalsIgnoreCase("ogsecurity")) {
                            System.out.println("\twe found a new admin");
                            System.out.println("\t\tcontext: " + token.getRedirectURL());
                            OGPrincipal principal = OGPrincipal.getAdminPrincipal();
                            OGPrincipalList principalList = Persist.get(token.getRedirectURL() + "principal", OGPrincipalList.class.getName());
                            if (principalList == null) {
                                principalList = new OGPrincipalList();
                                principalList.add(principal);
                                Persist.save(principalList, token.getRedirectURL() + "principal");
                                return Response.ok(Constants.SUCCESS_HTML.toString()).build();
                            } else {
                                System.out.println("\tprincipalList is not null");
                            }
                        } else {
                            System.out.println("\tdoesn't have permission to add a role");
                        }
                    }
                } else {
                    System.out.println("\ttoken was not verified with ID's passed in from client");
                }

            } else {
                System.out.println("\ttoken is null or not authenticated");
                System.out.println(token);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(Constants.ERROR_HTML.toString()).build();
    }
}
