/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.ogsecurity.server;

import com.organick.ogsecurity.OGToken;
import com.organick.ogsecurity.account.OGPrincipal;
import java.io.Serializable;

/**
 *
 * @author rnkeene
 */
public class OGServerToken extends OGToken implements Serializable {

    private String redirectURL;

    public OGServerToken() {
        this.ready = false;
    }

    public void preloginClient(String userId, String sessionId) {
        this.userClientId = userId;
        this.clientId = sessionId;
        this.prelogin = true;
        this.state = LoginState.PRELOGIN;
        this.authenticated = false;
        this.error = false;
        this.attempts++;
    }

    public void preloginUser(String sessionId) {
        this.userServerId = sessionId;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public boolean isAdmin() {
        if (account != null) {
            return account.getPrincipals().contains(OGPrincipal.getAdminPrincipal());
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tOGServerToken INFO:\n");
        toString.append(super.toString());

        toString.append("\t\tredirect: ");
        toString.append(this.redirectURL);
        toString.append("\n");

        return toString.toString();
    }
}
