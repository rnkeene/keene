/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fgi.dag;

import java.io.Serializable;

/**
 *
 * @author rnkeene
 */
public class Page implements Serializable {

    private String pageName;

    public Page() {
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    @Override
    public String toString() {
        return this.pageName;
    }
}
