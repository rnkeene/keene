/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fgi.dag;

import java.io.Serializable;

/**
 *
 * @author rnkeene
 */
public class PageVertex implements Serializable {

    private Page page;

    public PageVertex(){
        this.page= new Page();
    }

    public PageVertex(String pageName){
        this.page= new Page();
        this.page.setPageName(pageName);
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    @Override
    public String toString(){
        return page.toString();
    }

    public static enum VertexLocation{
        PRE(new PageVertex("PRE")),
        START(new PageVertex("START")),
        TERM(new PageVertex("TERM")),
        POST(new PageVertex("POST"));
        private final PageVertex vertex;
        VertexLocation(PageVertex avertex){
            vertex = avertex;
        }
        public PageVertex getPageVertex(){
            return vertex;
        }
    }

}
