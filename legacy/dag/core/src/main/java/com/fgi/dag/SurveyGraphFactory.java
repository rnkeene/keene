/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fgi.dag;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 *
 * @author rnkeene
 */
public class SurveyGraphFactory {

    public static SimpleDirectedGraph<PageVertex, DefaultEdge> getSurveyGraph(int surveyId) {

        SimpleDirectedGraph<PageVertex, DefaultEdge> g = new SimpleDirectedGraph<PageVertex, DefaultEdge>(DefaultEdge.class);

        PageVertex START = PageVertex.VertexLocation.START.getPageVertex();
        PageVertex Q1 = new PageVertex("page1");
        PageVertex Q2 = new PageVertex("page2");
        PageVertex Q3 = new PageVertex("page3");
        PageVertex Q4 = new PageVertex("page4");
        PageVertex Q5 = new PageVertex("page5");
        PageVertex Q6 = new PageVertex("page6");
        PageVertex Q7 = new PageVertex("page7");
        PageVertex Q8 = new PageVertex("page8");
        PageVertex Q9 = new PageVertex("page9");
        PageVertex END = PageVertex.VertexLocation.TERM.getPageVertex();

        // add the vertices
        g.addVertex(START);
        g.addVertex(Q1);
        g.addVertex(Q2);
        g.addVertex(Q3);
        g.addVertex(Q4);
        g.addVertex(Q5);
        g.addVertex(Q6);
        g.addVertex(Q7);
        g.addVertex(Q8);
        g.addVertex(Q9);        
        g.addVertex(END);

        // add edges to create a circuit
        g.addEdge(START, Q1);
        g.addEdge(Q1, Q2);
        g.addEdge(Q2, Q3);
        g.addEdge(Q3, Q4);
        g.addEdge(Q4, Q5);
        g.addEdge(Q5, Q6);
        g.addEdge(Q6, Q7);
        g.addEdge(Q7, Q8);
        g.addEdge(Q8, Q9);
        g.addEdge(Q9, END);

//        

        return g;        
    }
}
