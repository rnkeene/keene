/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fgi.dag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author rnkeene
 */
public class SurveyPath implements Serializable {

    private String surveyId;
    private List<PageVertex> pathList;

    public SurveyPath() {
        this.pathList = new ArrayList<PageVertex>();
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public List<PageVertex> getPathList() {
        return pathList;
    }

    public void addPath(List<PageVertex> pathList) {
        this.pathList = pathList;
    }

    public void init(Stack<PageVertex> path) {
        while (!path.empty()) {
            pathList.add(0, path.pop());
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (PageVertex vertex : pathList) {
            builder.append(" -> ");
            builder.append(vertex);
        }
        return builder.toString();
    }
}
