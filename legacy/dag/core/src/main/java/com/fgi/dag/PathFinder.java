/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fgi.dag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 *
 * @author rnkeene
 */
public class PathFinder implements Serializable {
    
    public static List<SurveyPath> findAllPaths(PageVertex root, SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph) {
        List<Stack<PageVertex>> pathList = new ArrayList<Stack<PageVertex>>();
        Stack<PageVertex> thisStack = new Stack<PageVertex>();
        thisStack.push(root);
        pathList.add(thisStack);
        PathFinder.findAllPaths(thisStack, surveyGraph, pathList);
        List<SurveyPath> paths = new ArrayList<SurveyPath>();
        for (Stack<PageVertex> sqv : pathList) {
            SurveyPath path = new SurveyPath();
            path.init(sqv);
            paths.add(path);
        }
        return paths;
    }

    private static void findAllPaths(Stack<PageVertex> thisStack, SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph, List<Stack<PageVertex>> paths) {
        PageVertex activeVertex = thisStack.peek();
        Set<DefaultEdge> edges = surveyGraph.outgoingEdgesOf(activeVertex);
        int activeBranches = surveyGraph.outDegreeOf(activeVertex);
        for (DefaultEdge edge : edges) {
            Stack<PageVertex> activeStack = thisStack;
            PageVertex destinationVertex = surveyGraph.getEdgeTarget(edge);
            if (activeBranches > 1) {
                activeStack = (Stack<PageVertex>) thisStack.clone();
                paths.add(activeStack);
            }
            activeStack.push(destinationVertex);
            activeBranches--;
            PathFinder.findAllPaths(activeStack, surveyGraph, paths);
        }
    }
}
