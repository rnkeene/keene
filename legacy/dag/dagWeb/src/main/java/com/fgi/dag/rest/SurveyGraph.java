package com.fgi.dag.rest;

import com.fgi.dag.PageVertex;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;
import com.fgi.dag.SurveyGraphFactory;
import com.fgi.dag.SurveyPath;
import com.fgi.dag.PathFinder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("surveygraph")
public class SurveyGraph {

    @Context
    private ServletContext context;
    @Context
    private HttpServletRequest request;

    @POST
    @Path("/edge/new")
    @Produces("text/plain")
    public Response newVertex() throws Exception {
        HttpSession session = request.getSession(true);
        String toQuestionP = request.getParameter("toQuestion");
        String fromQuestionP = request.getParameter("fromQuestion");
        SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = (SimpleDirectedGraph<PageVertex, DefaultEdge>) session.getAttribute("surveygraph");
        Map<String, PageVertex> vertexes = (Map<String, PageVertex>) session.getAttribute("vertexes");
        if (surveyGraph != null) {
            StringBuilder toString = new StringBuilder();
            PageVertex fromQuestion = vertexes.get(fromQuestionP);
            PageVertex toQuestion = vertexes.get(toQuestionP);
            surveyGraph.addEdge(fromQuestion, toQuestion);
            Response.ok(toString.toString()).build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/graph/load")
    @Produces("text/plain")
    public Response loadGraph() throws Exception {
        HttpSession session = request.getSession(true);
        int surveyId = Integer.parseInt(request.getParameter("surveyId"));
        SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = SurveyGraphFactory.getSurveyGraph(surveyId);
        session.setAttribute("surveygraph", surveyGraph);
        Map<String, PageVertex> vertexes = new HashMap<String, PageVertex>();
        StringBuilder toString = new StringBuilder();
        int count = 1;
        for (PageVertex v : surveyGraph.vertexSet()) {
            toString.append(this.buildDraggableVertex(v, count));
            count++;
            vertexes.put(v.toString(), v);
        }
        session.setAttribute("vertexes", vertexes);
        return Response.ok(toString.toString()).build();
    }
    
    @GET
    @Path("/graph/reload")
    @Produces("text/plain")
    public Response reloadGraph() throws Exception {
        HttpSession session = request.getSession(true);
        SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = (SimpleDirectedGraph<PageVertex, DefaultEdge>) session.getAttribute("surveygraph");
        session.setAttribute("surveygraph", surveyGraph);
        Map<String, PageVertex> vertexes = new HashMap<String, PageVertex>();
        StringBuilder toString = new StringBuilder();
        int count = 1;
        for (PageVertex v : surveyGraph.vertexSet()) {
            toString.append(this.buildDraggableVertex(v, count));
            count++;
            vertexes.put(v.toString(), v);
        }
        session.setAttribute("vertexes", vertexes);
        return Response.ok(toString.toString()).build();
    }

    @POST
    @Path("/graph/limited")
    @Produces("text/plain")
    public Response loadLimitedGraph() throws Exception {
        //System.out.println("loading limited graph!");
        HttpSession session = request.getSession(true);
        SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = (SimpleDirectedGraph<PageVertex, DefaultEdge>) session.getAttribute("surveygraph");
        StringBuilder toString = new StringBuilder();
        int limited = Integer.parseInt(request.getParameter("limited"));
        int count = 1;
        boolean minimum = Boolean.parseBoolean(request.getParameter("minimum"));
        //System.out.println(limited + " > remaining? " + minimum);
        for (PageVertex v : surveyGraph.vertexSet()) {
            if (minimum && count > limited) {
                toString.append(this.buildDraggableVertex(v, count));
            }else if (!minimum && count < limited){
                toString.append(this.buildDraggableVertex(v, count));
            }
            count++;
        }
        return Response.ok(toString.toString()).build();
    }

    private String buildDraggableVertex(PageVertex v, int count) {
        StringBuilder toString = new StringBuilder();
        if (count % 9 == 0) {
            toString.append("<br /><br /><br /><br /><br />");
        }
        toString.append("<span class=\"page\">");
        toString.append(v);
        toString.append("</span>");
        toString.append("<input type=\"hidden\" value=\"");
        toString.append(count);
        toString.append("\" ");
        toString.append("id=\"");
        toString.append(v);
        toString.append("\" />");
        count++;
        return toString.toString();
    }

    @GET
    @Path("/path/view")
    @Produces("text/plain")
    public Response viewPath() throws Exception {
        HttpSession session = request.getSession(true);
        SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = (SimpleDirectedGraph<PageVertex, DefaultEdge>) session.getAttribute("surveygraph");
        if (surveyGraph != null) {
            StringBuilder toString = new StringBuilder();
            toString.append("<ol>");
            for (SurveyPath path : PathFinder.findAllPaths(PageVertex.VertexLocation.START.getPageVertex(), surveyGraph)) {
                toString.append("<li>");
                toString.append(path);
                toString.append("</li>");
            }
            toString.append("</ol>");
            return Response.ok(toString.toString()).build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("/select/survey")
    @Produces("text/plain")
    public Response selectSurvey() throws Exception {
        StringBuilder toString = new StringBuilder();
        toString.append("<select id=\"surveyId\" onchange=\"showSurveyPages()\">");
        toString.append("<option value=\"-1\"> -- Select Survey --</option>");
        List<String> surveyList = new ArrayList<String>();
        surveyList.add("1");
        surveyList.add("2");
        surveyList.add("3");
        for (String survey : surveyList) {
            toString.append("<option value=\"");
            toString.append(survey);
            toString.append("\">");
            toString.append(survey);
            toString.append("</option>");
        }
        toString.append("</select>");
        return Response.ok(toString.toString()).build();
    }

    @GET
    @Path("/vertex/view")
    @Produces("text/plain")
    public Response viewVertex() throws Exception {
        HttpSession session = request.getSession(true);
        SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = (SimpleDirectedGraph<PageVertex, DefaultEdge>) session.getAttribute("surveygraph");
        if (surveyGraph != null) {
            StringBuilder toString = new StringBuilder();
            for (PageVertex vertex : surveyGraph.vertexSet()) {
                toString.append("<li>");
                toString.append(vertex);
                toString.append(":<ul>");
                for(DefaultEdge edge : surveyGraph.outgoingEdgesOf(vertex)){
                    toString.append("<li>");
                    toString.append(surveyGraph.getEdgeTarget(edge));
                    toString.append("</li>");
                }
                toString.append("</ul>");
                toString.append("</li>");
            }
            return Response.ok(toString.toString()).build();
        }
        return Response.ok().build();
    }

    @GET
    @Path("/js")
    @Produces("text/javascript")
    public Response getJavascript() throws Exception {
        StringBuilder js = new StringBuilder();
        js.append(JQueryUtil.readFile("/WEB-INF/js/fgi-reload.js", context));
        js.append(JQueryUtil.readFile("/WEB-INF/js/fgi.js", context));        
        js.append(JQueryUtil.readFile("/WEB-INF/js/surveygraph.js", context));
        js.append(JQueryUtil.buildLoadJS("/dagWeb/surveygraph/select/survey", "loadSurveys(data);", ""));
        js.append(JQueryUtil.buildReloadScript("/dagWeb/surveygraph/js/reload"));
        return Response.ok(js.toString()).build();
    }
    @GET
    @Path("/js/reload")
    @Produces("text/javascript")
    public Response getJavascriptReload() throws Exception {
        StringBuilder js = new StringBuilder();
        js.append(JQueryUtil.readFile("/WEB-INF/js/fgi-reload.js", context));
        return Response.ok(js.toString()).build();
    }
}
