/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fgi.dag.rest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.swing.text.html.FormSubmitEvent.MethodType;

/**
 *
 * @author rnkeene
 */
public class JQueryUtil {

    public static String buildLoadJS(String url) throws Exception {
        return buildLoadJS(url, new HashMap<String, String>(), new HashMap<StatusCode, List<String>>(), MethodType.GET);
    }

    public static String buildLoadJS(String url, String successFunction, String errorFunction) throws Exception {
        Map<StatusCode, List<String>> functions = new HashMap<StatusCode, List<String>>();

        List<String> successList = new ArrayList<String>();
        successList.add(successFunction);
        functions.put(StatusCode.OK, successList);

        if (errorFunction != null) {
            List<String> errorList = new ArrayList<String>();
            errorList.add(errorFunction);
            functions.put(StatusCode.UNAUTHORIZED, errorList);
        }

        return buildLoadJS(url, new HashMap<String, String>(), functions, MethodType.GET);
    }

    public static String buildLoadJS(String url, Map<String, String> requestParams, Map<StatusCode, List<String>> functions, MethodType type) throws Exception {
        StringBuilder builder = new StringBuilder();
        if (type.equals(MethodType.POST)) {
            builder.append("$.post(");
        } else if (type.equals(MethodType.GET)) {
            builder.append("$.get(");
        }
        if (!url.startsWith("'") && !url.startsWith("\"")) {
            builder.append("'");
        }
        builder.append(url);
        if (!url.startsWith("'") && !url.startsWith("\"")) {
            builder.append("'");
        }
        builder.append(", ");
        if (requestParams.size() > 0) {
            builder.append("{");
            boolean first = true;
            String tmpStr = "";
            for (String key : requestParams.keySet()) {
                builder.append(tmpStr);
                builder.append(key);
                builder.append(": ");
                builder.append(requestParams.get(key));
                if (first) {
                    tmpStr = ",";
                    first = false;
                }
            }
            builder.append("},\n");
        }
        builder.append("function( data, status, jqXHR ) {\n");
        for (StatusCode status : functions.keySet()) {
            builder.append("if (jqXHR.status==");
            builder.append(status.getCode());
            builder.append(") {\n");
            for (String function : functions.get(status)) {
                builder.append(function + "\n");
            }
            builder.append("}\n");
        }
        builder.append("});\n");

        return builder.toString();
    }

    public static String readFile(String fileName, ServletContext context) throws Exception {
        InputStream is = context.getResourceAsStream(fileName);
        StringBuilder writer = new StringBuilder();
        if (is != null) {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(isr);

            String text = "";

            while ((text = reader.readLine()) != null) {
                writer.append(text);
                writer.append("\n");
            }
        }
        return writer.toString();
    }

    public static String buildReloadScript(String script) {
        List<String> scripts = new ArrayList<String>();
        scripts.add(script);
        return buildReloadScript(scripts);
    }

    public static String buildReloadScript(List<String> scripts) {
        StringBuilder builder = new StringBuilder();
        builder.append("function reloadScripts() {\n");
        for (String script : scripts) {
            builder.append("$.getScript('");
            builder.append(script);
            builder.append("');\n");
        }

        builder.append("}\n");

        return builder.toString();
    }

    private enum StatusCode {
        OK(200),
        UNAUTHORIZED(401),
        NOTFOUND(404);
        private int code;
        StatusCode(int code) {
            this.code = code;
        }
        public int getCode() {
            return code;
        }
    }
}
