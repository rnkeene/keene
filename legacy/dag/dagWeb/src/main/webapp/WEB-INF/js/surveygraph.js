function loadSurveys(data){
    $("#surveys").html(data);
}

function showSurveyPages(){
    $.post( "/dagWeb/surveygraph/graph/load", { surveyId: $("#surveyId").val()},
    function( data ) {        
        updateSurveyPagesDisplay(data);
        refreshData();
    });    
}

function updateSurveyPagesDisplay(data){    
    $("#pages").html(data);
    
}
function updateSurveyVertexDisplay(data){
    $("#connections").html(data);
}
function updateSurveyPathDisplay(data){
    $("#paths").html(data);
}

function refreshData(){
    $.get( "/dagWeb/surveygraph/vertex/view", function( data ) {
        updateSurveyVertexDisplay(data);
    });
    $.get( "/dagWeb/surveygraph/path/view", function( data ) {
        updateSurveyPathDisplay(data);
    });
    reloadScripts();
}