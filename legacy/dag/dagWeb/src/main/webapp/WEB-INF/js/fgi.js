var activeQuestion;
var toQuestion;
var fromQuestion;
var toReady = false;
var fromReady = false;

function checkValidFrom(activeQuestion){
    if(activeQuestion=="TERM"){
        alert("You cannot start from a Survey Term point.");
        return false;
    }
    return true;
}
function checkValidTo(activeQuestion){
    if(activeQuestion=="START"){
        alert("You cannot end from a Survey Start point.");
        return false;
    }
    return true;
}

function checkReady(){
    $("#create").removeClass("bsomecommit");
    $("#create").addClass("bcommit");
    $("#create").val( "CLICK! \nto create\n" +fromQuestion + " to " + toQuestion +"\nConnection" );
    $("#create").removeAttr('disabled');
}
function notReady(){
    $("#create").removeClass("bnotcommit");
    $("#create").addClass("bsomecommit");
    $("#create").val( activeQuestion );
    var page = "";
    if(fromReady){
        page = $("#fromQuestion").val();
    }else if(toReady){
        page = $("#toQuestion").val();
    }
    var count = $("#"+page).val();
    $.post( "/dagWeb/surveygraph/graph/limited", { limited: count, minimum: fromReady},function( data ){
            updateSurveyPagesDisplay(data);
    });
    reloadScripts();
}

function addVertex(){   
    $.post( "/dagWeb/surveygraph/edge/new", { toQuestion: $("#toQuestion").val(),fromQuestion: $("#fromQuestion").val()},function( data ){});
    resetConnection();    
}

function resetConnection(){
    $("#create").attr('disabled', 'disabled');
    $("#toQuestion").removeClass("pageActive");
    $("#toQuestion").addClass("toQuestion");
    $("#fromQuestion").removeClass("pageActive");
    $("#fromQuestion").addClass("fromQuestion");
    $("#create").addClass("bnotcommit");
    $("#create").removeClass("bsomecommit");
    activeQuestion = null;
    toQuestion = null;
    fromQuestion = null;
    toReady = false;
    fromReady = false;
    $("#fromQuestion").val( " From " );
    $("#toQuestion").val( " To " );
    $("#create").val( " Make Selection " );
    $.get( "/dagWeb/surveygraph/graph/reload",function( data ) {
        updateSurveyPagesDisplay(data);
    });
    refreshData();
}