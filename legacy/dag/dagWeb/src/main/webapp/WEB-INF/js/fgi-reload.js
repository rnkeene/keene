$(function() {
    $( ".sortable span" ).draggable({
        helper: "clone",
        revert: "invalid",
        start: function(event, ui) {
            activeQuestion = $(this).html();
        }
    });
    $( ".fromQuestion" ).droppable({
        drop: function( event, ui ) {            
            if(checkValidFrom(activeQuestion)){
                fromQuestion = activeQuestion;
                $("#fromQuestion").removeClass("fromQuestion");
                $("#fromQuestion").addClass("pageActive");
                $("#fromQuestion").val(fromQuestion);
                fromReady = true;
                if(toReady && fromReady){
                    checkReady();
                }else{
                    notReady();
                }
            }
        }
    });
    $( ".toQuestion" ).droppable({
        drop: function( event, ui ) {
            if(checkValidTo(activeQuestion)){
                toQuestion = activeQuestion;
                $("#toQuestion").removeClass("toQuestion");
                $("#toQuestion").addClass("pageActive");
                $("#toQuestion").val(toQuestion);
                toReady = true;
                if(toReady && fromReady){
                    checkReady();
                }else{
                    notReady();
                }
            }
        }
    });
});