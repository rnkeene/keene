<html>
    <head>
        <title>Survey QA Tool</title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="/dagWeb/surveygraph/js"></script>
        <link rel="stylesheet" href="style/style.css">
        <link rel="stylesheet" href="style/jquery-ui-blacktie.css" />
        <link rel="stylesheet" href="style/fgi-ui.css" />
    </head>
    <body>
        <table>
            <tr>
                <td>

                    <div class="spacer">
                        <table cellpadding="3" cellspacing="3" class="osurvey">
                            <tr>
                                <td>
                                    <span>Select A Survey:</span>
                                    <span id="surveys"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="spacer">
                        <table cellpadding="3" cellspacing="3">
                            <tr>
                                <td id="pages" class="sortable">

                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="spacer">
                        <table cellpadding="6" cellspacing="6" class ="actions">
                            <tr>
                                <td class="aright">
                                    <input id="fromQuestion" class="fromQuestion" type="button" value=" From "/>
                                </td>
                                <td class="amiddle">
                                    <input id="toQuestion" class="toQuestion" type="button" value=" To "/>
                                </td>
                                <td class="amiddle">
                                    <input id="create" class="bnotcommit" type="button" value=" Make Selection " onclick="addVertex()" disabled />
                                </td>
                                <td class="aleft">
                                    <input id="reset" class="bnotcommit" type="button" value=" Reset " onclick="resetConnection()" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="6" cellspacing="6">
                        <tr>
                            <td class="tabover">

                            </td>
                            <td class="connections">
                                <h2>Connections</h2>
                                <table>
                                    <tr>
                                        <td class="aleft" id="connections">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="paths">
                                <h2>Paths</h2>
                                <table>
                                    <tr>
                                        <td class="aleft" id="paths">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tabover">

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>