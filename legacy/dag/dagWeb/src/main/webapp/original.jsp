<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.jgrapht.graph.DefaultEdge" %>
<%@page import="org.jgrapht.graph.SimpleDirectedGraph" %>
<%@page import="com.fgi.dag.PageVertex" %>
<%@page import="com.fgi.dag.SurveyGraphFactory" %>
<%@page import="com.fgi.dag.SurveyPath" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Set" %>
<%
    SimpleDirectedGraph<PageVertex, DefaultEdge> surveyGraph = SurveyGraphFactory.getSurveyGraph(1);
%>
<html>
    <head>
        <title>Survey QA Tool</title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <link rel="stylesheet" href="style/jquery-ui-blacktie.css">
        <style type="text/css">
            .demo ul { list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px; }
            .demo li { margin: 5px; padding: 5px; width: 150px; }
        </style>
        <style type="text/css">
            #sortable-delete {
                height: 18px;
                overflow: hidden;
            }
            #sortable-delete li {
                height: 0;
                width: 0;
                overflow: hidden;
            }
            ul.trash{
                border: 1px solid black;
                padding: 20px;
                margin: 20px;
            }
        </style>
        <script type="text/javascript">

	$(function() {

                jQuery('ul.sortable').sortable({
                    tolerance: 'pointer',
                    cursor: 'pointer',
                    dropOnEmpty: true,
                });

                jQuery('ul.trash').droppable({
                    tolerance: 'pointer',
                    cursor: 'pointer',
                    dropOnEmpty: true,
                    connectWith: 'ul.sortable',
                    drop: function(event, ui) {
                        if(this.id == 'sortable-delete') {
                            // Remove the element dropped on #sortable-delete
                            jQuery('#'+ui.item.attr('id')).remove();
                        } else {
                            // Update code for the actual sortable lists
                        }
                    }
                });

		$( "#toPage li" ).draggable({
			connectToSortable: "#toList",
			helper: "clone",
			revert: "invalid"
		});

		$( "#fromPage li" ).draggable({
			connectToSortable: "#fromList",
			helper: "clone",
			revert: "invalid"
		});

	});
        </script>
    </head>
    <body>
        <div class="demo">
            <table>
                <tr>
                    <th>
                        FROM QUESTIONS
                    </th>
                    <th>
                        FROM
                    </th>
                    <th>
                        TO
                    </th>
                    <th>
                        TO QUESTIONS
                    </th>
                </tr>
                <tr>
                    <td>
                        <ul id="fromPage">
                            <%
                            int count = 0;
                            for(PageVertex v : surveyGraph.vertexSet()){
                                if((count+1)==surveyGraph.vertexSet().size()){
                                    break;
                                }
                            %>
                                <li class="ui-state-default"><%=v%></li>
                            <%
                                    count++;
                                }
                            %>
                        </ul>
                    </td>
                    <td>
                        <ul id="fromList" class="sortable">
                            <%
                            count = 0;
                            for(PageVertex v : surveyGraph.vertexSet()){
                                if((count+1)==surveyGraph.vertexSet().size()){
                                    break;
                                }
                            %>
                            <li class="ui-state-default"><%= v%></li>
                            <%
                                count++;
                            }
                            %>
                        </ul>
                    </td>
                    <td>
                        <ul id="toList" class="sortable">
                            <%
                            count = 0;
                            for(PageVertex v : surveyGraph.vertexSet()){
                                if(count > 0){
                            %>
                            <li class="ui-state-highlight"><%= v%></li>
                            <%
                                    }
                                    count++;
                                }
                            %>
                        </ul>
                    </td>
                    <td>
                        <ul id="toPage">
                        <%
                            count=0;
                            for(PageVertex v : surveyGraph.vertexSet()){
                                if(count > 0){
                        %>
                                <li class="ui-state-highlight"><%=v%></li>
                        <%
                                }
                                count++;
                            }
                        %>
                        </ul>
                    </td>
                </tr>
            </table>
            <ul id="sortable-delete" class="trash">
            </ul>
        </div>
    </body>
</html>