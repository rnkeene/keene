package com.organick.dom.builder;

import com.organick.dom.impl.Element;

public class ElementBuilder<E extends Element> extends NodeBuilder<E> {

    public ElementBuilder() {
    }
    
    @Override
    public E build() {
        return element;
    }

}
