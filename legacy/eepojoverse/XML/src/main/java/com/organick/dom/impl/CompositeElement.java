package com.organick.dom.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CompositeElement<P extends Element, O extends Object, A extends Attributes, T extends Text> extends Element<P, O, A, T> implements Collection<Element>{

    private List<Element> children;

    public CompositeElement() {
        super();
        this.init();
    }

    public CompositeElement(O value) {
        super(value);
        this.init();
    }

    public CompositeElement(O value, T text) {
        super(value, text);
        this.init();
    }

    public CompositeElement(O value, A attributes) {
        super(value, attributes);
        this.init();
    }

    public CompositeElement(O value, P parent) {
        super(value, parent);
        this.init();
    }

    public CompositeElement(O value, A attributes, T text) {
        super(value, attributes, text);
        this.init();
    }

    public CompositeElement(O value, T text, P parent) {
        super(value, text, parent);
        this.init();
    }

    public CompositeElement(O value, A attributes, P parent) {
        super(value, attributes, parent);
        this.init();
    }

    public CompositeElement(O value, A attributes, T text, P parent) {
        super(value, attributes, text, parent);
        this.init();
    }

    private void init() {
        children = new ArrayList<Element>();
    }
    
    public int size() {
        return children.size();
    }
    
    public boolean isEmpty() {
        return children.isEmpty();
    }
    
    public boolean contains(Object o) {
        return children.contains((Element) o);
    }
    
    public Iterator<Element> iterator() {
        return children.iterator();
    }
    
    public Object[] toArray() {
        return children.toArray();
    }

    
    public <T> T[] toArray(T[] a) {
        return children.toArray(a);
    }
    
    public boolean add(Element e) {
        e.setParent(this);
        return children.add(e);
    }
    
    public boolean remove(Object o) {
        return children.remove((Element) o);
    }
    
    public boolean containsAll(Collection<?> c) {
        return children.containsAll(c);
    }
    
    public boolean addAll(Collection<? extends Element> c) {
        for (Element e : c) {
            this.add(e);
        }
        return true;
    }
    
    public boolean removeAll(Collection<?> c) {
        return children.removeAll(c);
    }
    
    public boolean retainAll(Collection<?> c) {
        return children.retainAll(c);
    }
    
    public void clear() {
        children.clear();
    }
    
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("<");
        toString.append(super.getValue());
        if (this.getAttributes() != null) {
            toString.append(" ");
            toString.append(this.getAttributes().toString());
        }
        if (this.getChildren().size() > 0 || this.getText() != null) {
            toString.append(">");
            if (this.getChildren().size() > 0) {
                for (Element e : this) {
                    toString.append(e);
                }
            } else if (this.getText() != null) {
                toString.append(this.getText());
            }
            toString.append("</");
            toString.append(super.getValue());
            toString.append(">");
        } else {
            toString.append(" />");
        }
        return toString.toString();
    }

    
    public List<Element> getChildren() {
        return children;
    }

    
    public void setChildren(List<Element> children) {
        this.children = children;
    }
}
