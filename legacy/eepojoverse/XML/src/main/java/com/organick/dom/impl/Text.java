package com.organick.dom.impl;

public class Text<O extends Object> extends Node<O> {

    public Text() {
    }

    public Text(O text) {
        super.setValue(text);
    }

    public O getText() {
        return super.getValue();
    }
}
