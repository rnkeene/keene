package com.organick.dom.builder;

public class TextBuilder<O extends Object> extends NodeBuilder<O> {

    public TextBuilder() {
        super();
    }
    
    public TextBuilder(O content) {
        super(content);
    }
    
    @Override
    public O build() {
        return (O)this.getText();
    }

    public O getText() {
        return super.getValue();
    }

    public void setText(O text) {
        super.setValue(text);
    }
    
}
