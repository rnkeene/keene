package com.organick.dom.impl;

public class Attribute<K extends Object, A extends Object> extends Node<A> {

    private K key;

    public Attribute() {
    }

    public void setAttribute(A attribute){
        super.setValue(attribute);
    }

    public A getAttribute(){
        return super.getValue();
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return this.getKey() + "=\"" + super.getValue() +"\"";
    }

}
