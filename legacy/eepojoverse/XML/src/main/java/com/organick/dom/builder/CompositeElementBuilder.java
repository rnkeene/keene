package com.organick.dom.builder;

import com.organick.dom.impl.Element;

public class CompositeElementBuilder<E extends Element> extends ElementBuilder {

    private E element;

    public CompositeElementBuilder() {
    }

    public CompositeElementBuilder(E element) {
        this.element = element;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }
}
