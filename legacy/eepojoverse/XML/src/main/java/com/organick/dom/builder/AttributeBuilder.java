package com.organick.dom.builder;

import com.organick.dom.impl.Attribute;

public class AttributeBuilder<A extends Attribute> extends NodeBuilder<A> {

    private A attribute;

    public AttributeBuilder() {
    }

    @Override
    public A build() {
        return attribute;
    }

    public A getAttribute() {
        return attribute;
    }

    public void setAttribute(A attribute) {
        this.attribute = attribute;
    }
}
