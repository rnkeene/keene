package com.organick.dom.impl;

public class Node<O extends Object> {

    private O value;

    public Node() {
    }

    public Node(O value) {
        this.value = value;
    }

    public O getValue() {
        return value;
    }

    public void setValue(O value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
