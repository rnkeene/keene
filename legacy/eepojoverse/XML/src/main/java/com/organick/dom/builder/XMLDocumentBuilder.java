package com.organick.dom.builder;

import com.organick.dom.Element;
import com.organick.dom.impl.Document;

public class XMLDocumentBuilder<O extends Object, E extends Element, D extends XMLDocumentBuilder> implements NodeBuilder<D> {

    private D document;
    private E element;
    private O value;

    public XMLDocumentBuilder() {
    }

    public XMLDocumentBuilder(O value, E element) {
        this.value = value;
        this.element = element;
    }

    @Override
    public D build() {
        document = (D)new Document(value, element);
        return document;
    }

    public D getDocument() {
        return document;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    public O getValue() {
        return value;
    }

    public void setValue(O value) {
        this.value = value;
    }   

}
