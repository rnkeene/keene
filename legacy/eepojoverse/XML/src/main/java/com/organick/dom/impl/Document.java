package com.organick.dom.impl;

public class Document<D extends Object, E extends Element> extends Node<D>{

    private E root;

    public Document(E root) {
        super();
        this.root = root;
    }

    public Document(D value, E root) {
        super(value);
        this.root = root;
    }

    public E getRoot() {
        return root;
    }

    public void setRoot(E root) {
        this.root = root;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(root);
        return toString.toString();
    }

}
