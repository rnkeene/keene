package com.organick.dom.builder;

public abstract class NodeBuilder<O extends Object> {

    private O node;

    public NodeBuilder() {
    }

    public NodeBuilder(O node) {
        this.node = node;
    }   

    public O getValue() {
        return node;
    }

    public void setValue(O node) {
        this.node = node;
    }

    public abstract O build();
}
