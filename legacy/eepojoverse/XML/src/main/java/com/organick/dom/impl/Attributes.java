package com.organick.dom.impl;

import java.util.HashMap;
import java.util.Map;

public class Attributes<K extends Object> extends HashMap<K,Attribute> implements Map<K,Attribute>{

    private Map<K, Attribute> map;

    public Attributes() {
        super();
        map = new HashMap<K,Attribute>();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        boolean first = true;
        for (K key : this.keySet()) {
            if(!first){
                toString.append(" ");
            }else{
                first = false;
            }
            toString.append(this.get(key).toString());
        }
        return toString.toString();

   }

}
