package com.organick.dom.impl;

public class Element<P extends Element, O extends Object, A extends Attributes, T extends Text> extends Node<O> {

    private A attributes;
    private T text;
    private P parent;

    public Element() {
        super();
    }

    public Element(O value) {
        super(value);
    }

    public Element(O value, T text) {
        super(value);
        this.text = text;
    }

    public Element(O value, A attributes) {
        super(value);
        this.attributes = attributes;
    }

    public Element(O value, P parent) {
        super(value);
        this.parent = parent;
    }

    public Element(O value, A attributes, T text) {
        super(value);
        this.text = text;
        this.attributes = attributes;
    }

    public Element(O value, T text, P parent) {
        this(value, text);
        this.parent = parent;
    }

    public Element(O value, A attributes, P parent) {
        this(value, attributes);
        this.parent = parent;
    }

    public Element(O value, A attributes, T text, P parent) {
        this(value, attributes, text);
        this.parent = parent;
    }

    public P getParent() {
        return parent;
    }

    public void setParent(P parent) {
        this.parent = parent;
    }

    public A getAttributes() {
        return attributes;
    }

    public void setAttributes(A attributes) {
        this.attributes = attributes;
    }

    public T getText() {
        return this.text;
    }

    public void setText(T text) {
        this.text = text;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("<");
        toString.append(super.toString());
        toString.append(" ");
        if (attributes != null) {
            toString.append(attributes.toString());
        }
        if (text != null) {
            toString.append(">");
            toString.append(text.toString());
            toString.append("</");
            toString.append(super.toString());
            toString.append(">");
        } else {
            toString.append(" />");
        }
        return toString.toString();
    }
}
