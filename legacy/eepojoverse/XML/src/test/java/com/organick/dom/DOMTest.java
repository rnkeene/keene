package com.organick.dom;

import com.organick.dom.builder.AttributeBuilder;
import com.organick.dom.builder.CompositeElementBuilder;
import com.organick.dom.builder.XMLDocumentBuilder;
import com.organick.dom.builder.ElementBuilder;
import com.organick.dom.builder.TextBuilder;
import com.organick.dom.impl.Attribute;
import com.organick.dom.impl.Attributes;
import com.organick.dom.impl.CompositeElement;
import com.organick.dom.impl.Document;
import com.organick.dom.impl.Element;
import com.organick.dom.impl.Text;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class DOMTest extends TestCase {

    public DOMTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(DOMTest.class);
    }

    public void testBuilder() {
        System.out.println("--- Start Builder test ---");
        TextBuilder textBuilder = new TextBuilder("Hello Text Builder!!");
//        System.out.println(textBuilder.build());
        TextBuilder textBuilderB = new TextBuilder("Good-Bye Text Builder!!");
//        System.out.println(textBuilderB.build());
        TextBuilder textBuilderC = new TextBuilder("Ping Pong!!");
//        System.out.println(textBuilderC.build());
        TextBuilder textBuilderD = new TextBuilder("Element Content");
//        System.out.println(textBuilderD.build());
        TextBuilder textBuilderE = new TextBuilder("Backup Element!!");
//        System.out.println(textBuilderE.build());
        
        AttributeBuilder<String,TextBuilder,Attribute> attributeBuilder = new AttributeBuilder<String,TextBuilder,Attribute>("good", textBuilder);
        AttributeBuilder<String,TextBuilder,Attribute> attributeBuilderB = new AttributeBuilder<String,TextBuilder,Attribute>("bye", textBuilderB);
        AttributeBuilder<String,TextBuilder,Attribute> attributeBuilderC = new AttributeBuilder<String,TextBuilder,Attribute>("today", textBuilderC);
        AttributeBuilder<String,TextBuilder,Attribute> attributeBuilderD = new AttributeBuilder<String,TextBuilder,Attribute>("surprise", textBuilderD);
        AttributeBuilder<String,TextBuilder,Attribute> attributeBuilderE = new AttributeBuilder<String,TextBuilder,Attribute>("jupiter", textBuilderE);

        Attribute attribute = attributeBuilder.build();
        Attribute attributeB = attributeBuilderB.build();
        Attribute attributeC = attributeBuilderC.build();
        Attribute attributeD = attributeBuilderD.build();
        Attribute attributeE = attributeBuilderE.build();

//        System.out.println(attribute);
//        System.out.println(attributeB);
//        System.out.println(attributeC);
//        System.out.println(attributeD);
//        System.out.println(attributeE);

        Attributes<String,Attribute> attributes = new Attributes<String,Attribute>();
        attributes.put((String)attribute.getValue(), attribute);
        attributes.put((String)attributeB.getValue(), attributeB);
        attributes.put((String)attributeC.getValue(), attributeC);

//        System.out.println(attributes);

        Attributes<String,Attribute> attributesB = new Attributes<String,Attribute>();
        attributesB.put((String)attributeD.getValue(), attributeD);
        attributesB.put((String)attributeE.getValue(), attributeE);

//        System.out.println(attributesB);

        CompositeElementBuilder<CompositeElement,String,Attributes,TextBuilder,CompositeElement> eb1 = new CompositeElementBuilder<CompositeElement,String,Attributes,TextBuilder,CompositeElement>("elementOne", attributes, textBuilderD,null);
        CompositeElement element = eb1.build();

//        System.out.println(element);
        
        ElementBuilder<CompositeElement,String,Attributes,TextBuilder,Element> eb2 = new ElementBuilder<CompositeElement,String,Attributes,TextBuilder,Element>("elementTwo", attributesB, textBuilderE, element);
        Element element2 = eb2.build();

//        System.out.println(element2);

        element.add(element2);

        XMLDocumentBuilder<String,Element,Document> dbA = new XMLDocumentBuilder<String,Element,Document>("Doc1.html", element);
        Document docA = dbA.build();

        System.out.println(docA);

        System.out.println("--- done with Builder test ---");
        assertTrue(true);
    }

    public void testCompositeElementTest() {
        System.out.println("--- start Composite test ---");
        CompositeElement<CompositeElement,String, Attributes, Text> html = (CompositeElement)new CompositeElement<CompositeElement,String, Attributes, Text>("html");
        CompositeElement<CompositeElement,String, Attributes, Text> head = (CompositeElement)new CompositeElement<CompositeElement,String, Attributes, Text>("head",html);
        Element<CompositeElement, String, Attributes, Text> link = new Element<CompositeElement,String, Attributes, Text>("link",head);
        head.add(link);
        html.add(head);
        Document<String, Element> document = new Document<String, Element>("A Document: ", html);
        System.out.println(document);
        System.out.println("--- done with Composite test ---");
        assertTrue(true);
    }

    public void testStart() {
        System.out.println("--- start Impl test ---");
        Text text = new Text("Text Hello");
        //System.out.println(text);

        Text textT = new Text("Second Text");
        //System.out.println(textT);

        Text textF = new Text("Success!!!!");
        //System.out.println(textT);

        Attribute<String, Text> attribute = new Attribute<String, Text>("attrTest", text);
        //System.out.println(attribute);

        Attribute<String, Text> attributeT = new Attribute<String, Text>("secondAttr", textT);
        //System.out.println(attribute);

        Attributes<String, Attribute> attributes = (Attributes)new Attributes<String, Attribute>();
        attributes.put("1", attribute);
        attributes.put("2", attributeT);

        //System.out.println(attributes);

        Element<Element,String, Attributes, Text> element = new Element<Element,String, Attributes, Text>("hello-root", attributes, textF);
        //System.out.println(element);

        Document<String, Element> document = new Document<String, Element>("A Document: ", element);
        System.out.println(document);
        System.out.println("--- End the Impl test ---");
        assertTrue(true);
    }

    public void testHtml() {
        System.out.println("--- start Random HTML test ---");

        CompositeElement<CompositeElement,String, Attributes, Text> html = (CompositeElement)new CompositeElement<CompositeElement,String, Attributes, Text>("html");
        CompositeElement<CompositeElement,String, Attributes, Text> head = (CompositeElement)new CompositeElement<CompositeElement,String, Attributes, Text>("body",html);
        Element<CompositeElement, String, Attributes, Text> link = new Element<CompositeElement,String, Attributes, Text>("link",head);
        head.add(link);
        html.add(head);
        Document<String, Element> document = new Document<String, Element>("A Document: ", html);
        System.out.println(document);

        System.out.println("--- end Random HTML test ---");
    }
}
