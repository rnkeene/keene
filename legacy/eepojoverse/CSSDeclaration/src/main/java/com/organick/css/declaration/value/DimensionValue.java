/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value;

/**
 *
 * @author matt
 */
public interface DimensionValue extends Value {
    public void setDimensionValue(Long dimensionValue);
    public void setPX(boolean px);

}
