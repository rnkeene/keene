/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.styling.factory;

import com.organick.css.declaration.styling.BackgroundDeclaration;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.Value;

/**
 *
 * @author matt
 */
public class StylingDeclarationFactory {

    public static BackgroundDeclaration createBackgroundDeclaration(){
        return new BackgroundDeclarationImpl();
    }

    public static BackgroundDeclaration createBackgroundDeclaration(ColorValue color, Value image, Value repeat, Value attachment, Value position){
        BackgroundDeclaration background = new BackgroundDeclarationImpl();
        background.setColor(color);
        background.setImage(image);
        background.setRepeat(repeat);
        background.setAttachment(attachment);
        background.setPosition(position);
        return background;
    }
}
