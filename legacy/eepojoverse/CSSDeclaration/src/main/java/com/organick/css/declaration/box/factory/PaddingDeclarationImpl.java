/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.box.factory;

import com.organick.css.declaration.Declaration;
import com.organick.css.declaration.box.model.PaddingDeclaration;
import com.organick.css.declaration.value.Value;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author matt
 */
class PaddingDeclarationImpl extends PaddingDeclaration {

    @Override
    public String getType() {
        return "padding";
    }
}
