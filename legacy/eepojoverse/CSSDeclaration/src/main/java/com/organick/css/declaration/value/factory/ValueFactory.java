/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value.factory;

import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.ColorValue.ColorName;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.StyleValue;
import com.organick.css.declaration.value.StyleValue.StyleAttribute;
import com.organick.css.declaration.value.Value;

/**
 *
 * @author matt
 */
public class ValueFactory {
    public static Value createValue(){
        return new ValueImpl();
    }

    public static Value createValue(String value){
        return new ValueImpl(value);
    }
    
    public static DimensionValue createDimensionValue(){
        return new DimensionValueImpl();
    }

    public static DimensionValue createDimensionValue(Long value){
        DimensionValue dimension = new DimensionValueImpl();
        dimension.setDimensionValue(value);
        return dimension;
    }

    public static StyleValue createStyleValue(){
        return new StyleValueImpl();
    }

    public static StyleValue createStyleValue(StyleAttribute style){
        StyleValue styleValue = new StyleValueImpl();
        styleValue.setStyle(style);
        return styleValue;
    }

    public static ColorValue createColorValue(){
        return new ColorValueImpl();
    }

    public static ColorValue createColorValue(ColorName color){
        ColorValue colorValue = new ColorValueImpl();
        colorValue.setColorName(color);
        return colorValue;
    }

    public static ColorValue createColorValue(String hexValue){
        ColorValue colorValue = new ColorValueImpl();
        colorValue.setHexValue(hexValue);
        return colorValue;
    }
}
