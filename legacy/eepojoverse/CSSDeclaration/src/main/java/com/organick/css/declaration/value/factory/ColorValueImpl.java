/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value.factory;

import com.organick.css.declaration.value.ColorValue;

/**
 *
 * @author matt
 */
class ColorValueImpl implements ColorValue {

    private ColorName color;
    private String hexValue;

    @Override
    public void setColorName(ColorName color) {
        this.color = color;
        this.hexValue = null;
    }

    @Override
    public void setHexValue(String hexValue) {
        this.hexValue = hexValue;
        this.color = null;
    }

    @Override
    public String getValue() {
        if (color != null){
            return color.color();
        }
        if (hexValue != null){
            StringBuilder builder = new StringBuilder();
            builder.append("#");
            builder.append(hexValue.replace("#", ""));;
            return builder.toString();
        }

        return "";
    }

}
