/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.css.declaration.box;

import com.organick.css.declaration.Boundary;
import com.organick.css.declaration.Declaration;
import com.organick.css.declaration.DeclarationSet;
import com.organick.css.declaration.TypeAttribute;
import com.organick.css.declaration.factory.DeclarationFactory;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.factory.ValueFactory;
import java.util.Map;

/**
 *
 * @author matt
 */
public abstract class DimensionDeclaration extends DeclarationSet {

    public void setTopDimension(DimensionValue dimension) {
        setAttribute(dimension, Boundary.TOP, null);
    }

    public void setBottomDimension(DimensionValue dimension) {
        setAttribute(dimension, Boundary.BOTTOM, null);
    }

    public void setRightDimension(DimensionValue dimension) {
        setAttribute(dimension, Boundary.RIGHT, null);
    }

    public void setLeftDimension(DimensionValue dimension) {
        setAttribute(dimension, Boundary.LEFT, null);
    }

    public void setAllDimensions(DimensionValue dimension) {
        setAttribute(dimension, Boundary.TOP, null);
        setAttribute(dimension, Boundary.BOTTOM, null);
        setAttribute(dimension, Boundary.RIGHT, null);
        setAttribute(dimension, Boundary.LEFT, null);
    }

    public void setAllDimensions(DimensionValue top, DimensionValue bottom, DimensionValue right, DimensionValue left) {
        setAttribute(top, Boundary.TOP, null);
        setAttribute(bottom, Boundary.BOTTOM, null);
        setAttribute(right, Boundary.RIGHT, null);
        setAttribute(left, Boundary.LEFT, null);
    }

    @Override
    public String getKey(Boundary boundary, TypeAttribute type) {
        StringBuilder key = new StringBuilder();
        key.append(getType());
        key.append("-");
        key.append(boundary.boundary());
        return key.toString();
    }

    public abstract String getType();
}
