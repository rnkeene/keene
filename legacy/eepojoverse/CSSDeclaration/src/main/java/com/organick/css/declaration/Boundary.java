/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.css.declaration;

/**
 *
 * @author matt
 */
public enum Boundary {

    TOP("top"),
    BOTTOM("bottom"),
    LEFT("left"),
    RIGHT("right");
    
    private String name;

    Boundary(String name) {
        this.name = name;
    }

    public String boundary() {
        return name;
    }
}
