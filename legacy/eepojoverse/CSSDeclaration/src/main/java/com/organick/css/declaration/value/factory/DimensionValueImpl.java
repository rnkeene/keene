/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value.factory;

import com.organick.css.declaration.value.DimensionValue;

/**
 *
 * @author matt
 */
class DimensionValueImpl implements DimensionValue {

    private Long dimensionValue;
    private boolean px;

    @Override
    public void setDimensionValue(Long dimensionValue) {
        this.dimensionValue = dimensionValue;
    }

    @Override
    public void setPX(boolean px) {
        this.px = px;
    }

    @Override
    public String getValue() {
        StringBuilder builder = new StringBuilder();
        builder.append(dimensionValue);
        if (px){
            builder.append("px");
        } else {
            builder.append("em");
        }

        return builder.toString();
    }
}
