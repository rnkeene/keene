/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value;

/**
 *
 * @author matt
 */
public interface StyleValue extends Value {

    public void setStyle(StyleAttribute style);

    public enum StyleAttribute {

    NONE("none"),
    HIDDEN("hidden"),
    DOTTED("dotted"),
    DASHED("dashed"),
    SOLID("solid"),
    DOUBLE("double"),
    GROOVE("groove"),
    RIDGE("ridge"),
    INSET("inset"),
    OUTSET("outset"),
    INHERIT("inherit");
    private String name;

    StyleAttribute(String name) {
        this.name = name;
    }

    public String style() {
        return name;
    }
}
}
