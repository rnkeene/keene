/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value.factory;

import com.organick.css.declaration.value.Value;

/**
 *
 * @author matt
 */
class ValueImpl implements Value {

    private String value;

    ValueImpl(){
        value = "";
    }

    ValueImpl(String value){
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }


}
