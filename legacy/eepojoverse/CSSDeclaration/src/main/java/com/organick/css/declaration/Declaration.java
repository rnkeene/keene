/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration;

import com.organick.css.declaration.value.Value;
import java.util.Map;

/**
 * Represents a key:value pair defining a single style.
 * i.e. background-color:yellow
 *
 * @author matt
 */
public abstract class Declaration {
    public abstract String getKey();
    public abstract void setKey(String key);
    public abstract Value getValue();
    public abstract void setValue(Value value);

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(getKey());
        builder.append(":");
        builder.append(getValue().getValue());
        builder.append(";");
        return builder.toString();
    }
}
