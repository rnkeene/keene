/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value.factory;

import com.organick.css.declaration.value.StyleValue;

/**
 *
 * @author matt
 */
class StyleValueImpl implements StyleValue {

    private StyleAttribute style;

    @Override
    public void setStyle(StyleAttribute style) {
        this.style = style;
    }

    @Override
    public String getValue() {
        return style.style();
    }
}
