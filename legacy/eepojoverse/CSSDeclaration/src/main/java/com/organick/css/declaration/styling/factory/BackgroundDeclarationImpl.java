/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.styling.factory;

import com.organick.css.declaration.Boundary;
import com.organick.css.declaration.TypeAttribute;
import com.organick.css.declaration.styling.BackgroundDeclaration;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.Value;

/**
 *
 * @author matt
 */
public class BackgroundDeclarationImpl extends BackgroundDeclaration {

    @Override
    public void setColor(ColorValue color) {
        setAttribute(color, null, TypeAttribute.COLOR);
    }

    @Override
    public void setImage(Value image) {
        setAttribute(image, null, TypeAttribute.IMAGE);
    }

    @Override
    public void setRepeat(Value repeat) {
        setAttribute(repeat, null, TypeAttribute.REPEAT);
    }

    @Override
    public void setAttachment(Value attachment) {
        setAttribute(attachment, null, TypeAttribute.ATTACHMENT);
    }

    @Override
    public void setPosition(Value position) {
        setAttribute(position, null, TypeAttribute.POSITION);
    }

    @Override
    protected String getKey(Boundary boundary, TypeAttribute type) {
        StringBuilder key = new StringBuilder();
        key.append("background-");
        key.append(type.type());
        return key.toString();
    }
}
