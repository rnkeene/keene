/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.box.factory;

import com.organick.css.declaration.Declaration;
import com.organick.css.declaration.box.model.MarginDeclaration;
import com.organick.css.declaration.value.Value;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author matt
 */
class MarginDeclarationImpl extends MarginDeclaration {

    @Override
    public String getType() {
        return "margin";
    }
}
