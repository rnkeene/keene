/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.box.factory;

import com.organick.css.declaration.box.model.BorderDeclaration;
import com.organick.css.declaration.box.model.OutlineDeclaration;
import com.organick.css.declaration.box.model.MarginDeclaration;
import com.organick.css.declaration.box.model.PaddingDeclaration;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.StyleValue;

/**
 *
 * @author matt
 */
public class BoxModelDeclarationFactory {
    

    public static BorderDeclaration createBorderDeclaration(){
        return new BorderDeclarationImpl();
    }

    public static MarginDeclaration createMarginDeclaration(){
        return new MarginDeclarationImpl();
    }

    public static MarginDeclaration createMarginDeclaration(DimensionValue top, DimensionValue bottom, DimensionValue right, DimensionValue left){
        MarginDeclaration margin = new MarginDeclarationImpl();
        margin.setAllDimensions(top, bottom, right, left);
        return margin;
    }

    public static PaddingDeclaration createPaddingDeclaration(){
        return new PaddingDeclarationImpl();
    }

    public static PaddingDeclaration createPaddingDeclaration(DimensionValue top, DimensionValue bottom, DimensionValue right, DimensionValue left){
        PaddingDeclaration padding = new PaddingDeclarationImpl();
        padding.setAllDimensions(top, bottom, right, left);
        return padding;
    }

    public static OutlineDeclaration createOutlineDeclaration(){
        return new OutlineDeclarationImpl();
    }

    public static OutlineDeclaration createOutlineDeclaration(StyleValue style, ColorValue color, DimensionValue width){
        OutlineDeclaration outline = new OutlineDeclarationImpl();
        outline.setAllAttributes(style, color, width);
        return outline;
    }
}
