/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.css.declaration.box.model;

import com.organick.css.declaration.Boundary;
import com.organick.css.declaration.DeclarationSet;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.StyleValue;
import com.organick.css.declaration.value.StyleValue.StyleAttribute;
import java.util.List;
import java.util.Map;

/**
 *
 * @author matt
 */
public abstract class BorderDeclaration extends DeclarationSet {

    public abstract void setBorderWidth(Boundary boundary, DimensionValue width);

    public abstract void setBorderWidths(List<Boundary> boundaries, DimensionValue widths);

    public abstract void setBorderWidths(Map<Boundary, DimensionValue> widths);

    public abstract void setBorderStyle(Boundary boundary, StyleValue style);

    public abstract void setBorderStyles(List<Boundary> boundaries, StyleValue styles);

    public abstract void setBorderStyles(Map<Boundary, StyleValue> styles);

    public abstract void setBorderColor(Boundary boundary, ColorValue color);

    public abstract void setBorderColors(List<Boundary> boundaries, ColorValue colors);

    public abstract void setBorderColors(Map<Boundary, ColorValue> colors);

    public abstract void setTopAttributes(DimensionValue width, StyleValue style, ColorValue color);

    public abstract void setBottomAttributes(DimensionValue width, StyleValue style, ColorValue color);

    public abstract void setRightAttributes(DimensionValue width, StyleValue style, ColorValue color);
    
    public abstract void setLeftAttributes(DimensionValue width, StyleValue style, ColorValue color);

    public abstract void setAllBoundaries(DimensionValue width, StyleValue style, ColorValue color);
}
