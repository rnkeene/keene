/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.box.factory;

import com.organick.css.declaration.Boundary;
import com.organick.css.declaration.TypeAttribute;
import com.organick.css.declaration.box.model.OutlineDeclaration;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.StyleValue;

/**
 *
 * @author matt
 */
public class OutlineDeclarationImpl extends OutlineDeclaration {

    @Override
    public void setStyle(StyleValue style) {
        setAttribute(style, null, TypeAttribute.STYLE);
    }

    @Override
    public void setColor(ColorValue color) {
        setAttribute(color, null, TypeAttribute.COLOR);
    }

    @Override
    public void setWidth(DimensionValue width) {
        setAttribute(width, null, TypeAttribute.WIDTH);
    }

    @Override
    public void setAllAttributes(StyleValue style, ColorValue color, DimensionValue width) {
        setAttribute(style, null, TypeAttribute.STYLE);
        setAttribute(color, null, TypeAttribute.COLOR);
        setAttribute(width, null, TypeAttribute.WIDTH);
    }

    @Override
    protected String getKey(Boundary boundary, TypeAttribute type) {
        StringBuilder builder = new StringBuilder();
        builder.append("outline-");
        builder.append(type.type());
        return builder.toString();
    }
}
