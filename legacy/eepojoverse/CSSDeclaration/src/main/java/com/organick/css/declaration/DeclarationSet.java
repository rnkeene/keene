/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration;

import com.organick.css.declaration.factory.DeclarationFactory;
import com.organick.css.declaration.value.Value;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class should be extended to create multi-functioning CSS Declarations (i.e. border, margin, background, etc.)
 * When extending, you should call the setAttribute(Value,Boundary,TypeAttribute) method anytime you need to update an attribute of the Declaration Set
 *
 * This class implements List because all of the extending objects can generate multiple Declarations.
 * i.e. border-top-style: solid; border-top-width: 2px; background-color: red; background-repeat: repeat-x;
 *
 * @author matt
 */
public abstract class DeclarationSet extends Declaration implements Map<String, Declaration>{

    private Map<String, Declaration> declarations;

    public DeclarationSet(){
        declarations = new HashMap<String, Declaration>();
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for (String declaration : keySet()){
            builder.append(get(declaration));
        }

        return builder.toString();
    }

    /**
     * This method should be called by all extending methods when applying an edit to a Declaration Type
     *
     * @param value
     * @param boundary
     * @param type
     */
    protected void setAttribute(Value value, Boundary boundary, TypeAttribute type){
        if (value != null){
            Declaration declaration = DeclarationFactory.createDeclaration(getKey(boundary, type), value);
            put(declaration.getKey(), declaration);
        }
    }

    /**
     * Based on the boundary and attribute type, returns the CSS property name you are editing.
     * Neither boundary nor type is required, but there must be at least one present.
     *
     * Example of an implementation with both boundary and type: "border-top-width"
     * Example of an implementation with just boundary: "margin-top"
     * Example of an implementation with just type: "background-color"
     *
     *
     * @param boundary
     * @param type
     * @return key for style
     */
    protected abstract String getKey(Boundary boundary, TypeAttribute type);

    @Override
    public int size() {
        return declarations.size();
    }

    @Override
    public boolean isEmpty() {
        return declarations.isEmpty();
    }

    @Override
    public boolean containsKey(Object o) {
        return declarations.containsKey(o);
    }

    @Override
    public boolean containsValue(Object o) {
        return declarations.containsValue(o);
    }

    @Override
    public Declaration get(Object o) {
        return declarations.get(o);
    }

    @Override
    public Declaration put(String k, Declaration v) {
        return declarations.put(k, v);
    }

    @Override
    public Declaration remove(Object o) {
        return declarations.remove(o);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Declaration> map) {
        declarations.putAll(map);
    }

    @Override
    public void clear() {
        declarations.clear();
    }

    @Override
    public Set<String> keySet() {
        return declarations.keySet();
    }

    @Override
    public Collection<Declaration> values() {
        return declarations.values();
    }

    @Override
    public Set<Entry<String, Declaration>> entrySet() {
        return declarations.entrySet();
    }

    @Override
    public String getKey() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setKey(String key) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Value getValue() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setValue(Value value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
