/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.css.declaration;

/**
 * Used to style a specific type for a property
 *
 * Applicable for:
 * {@link com.organick.css.declaration.box.model.BorderDeclaration},
 * {@link com.organick.css.declaration.box.model.OutlineDeclaration},
 * {@link com.organick.css.declaration.styling.BackgroundDeclaration}
 *
 * @author matt
 */
public enum TypeAttribute {

    WIDTH("width"),
    STYLE("style"),
    COLOR("color"),
    IMAGE("image"),
    REPEAT("repeat"),
    ATTACHMENT("attachment"),
    POSITION("position");

    private String name;

    TypeAttribute(String name) {
        this.name = name;
    }

    public String type() {
        return name;
    }
}
