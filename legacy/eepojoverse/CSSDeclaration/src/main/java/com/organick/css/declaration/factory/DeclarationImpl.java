/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.factory;

import com.organick.css.declaration.value.Value;
import com.organick.css.declaration.Declaration;

/**
 *
 * @author matt
 */
class DeclarationImpl extends Declaration {

    private String key;
    private Value value;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public Value getValue() {
        return value;
    }

    @Override
    public void setValue(Value value) {
        this.value = value;
    }

}
