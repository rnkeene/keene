/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value;

/**
 *
 * @author matt
 */
public interface ColorValue extends Value {

    public void setColorName(ColorName color);
    public void setHexValue(String hexValue);

    public enum ColorName{
        YELLOW("yellow"),
        RED("red"),
        BLUE("blue"),
        BLACK("black"),
        BROWN("brown"),
        CYAN("cyan"),
        GREEN("green"),
        GREY("grey");

        private String name;

        ColorName(String name){
            this.name = name;
        }

        public String color(){
            return name;
        }
    }
}
