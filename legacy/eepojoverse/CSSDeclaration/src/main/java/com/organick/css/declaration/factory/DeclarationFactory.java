/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.factory;

import com.organick.css.declaration.Declaration;
import com.organick.css.declaration.value.Value;

/**
 *
 * @author matt
 */
public class DeclarationFactory {
    public static Declaration createDeclaration(){
        return new DeclarationImpl();
    }

    public static Declaration createDeclaration(String key, Value value){
        Declaration declaration = new DeclarationImpl();
        declaration.setKey(key);
        declaration.setValue(value);
        return declaration;
    }
}
