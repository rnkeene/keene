/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.css.declaration.styling;

import com.organick.css.declaration.DeclarationSet;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.Value;

/**
 *
 * @author matt
 */
public abstract class BackgroundDeclaration extends DeclarationSet {

    public abstract void setColor(ColorValue color);

    public abstract void setImage(Value image);

    public abstract void setRepeat(Value repeat);

    public abstract void setAttachment(Value attachment);

    public abstract void setPosition(Value position);
}
