/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.css.declaration.box.factory;

import com.organick.css.declaration.box.model.BorderDeclaration;
import com.organick.css.declaration.Boundary;
import com.organick.css.declaration.TypeAttribute;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.StyleValue;
import java.util.List;
import java.util.Map;

/**
 *
 * @author matt
 */
public class BorderDeclarationImpl extends BorderDeclaration {

    @Override
    public void setBorderWidth(Boundary boundary, DimensionValue width) {
        setAttribute(width, boundary, TypeAttribute.WIDTH);
    }

    @Override
    public void setBorderWidths(List<Boundary> boundaries, DimensionValue widths) {
        for (Boundary boundary : boundaries) {
            setAttribute(widths, boundary, TypeAttribute.WIDTH);
        }
    }

    @Override
    public void setBorderWidths(Map<Boundary, DimensionValue> widths) {
        for (Boundary boundary : widths.keySet()) {
            setAttribute(widths.get(boundary), boundary, TypeAttribute.WIDTH);
        }
    }

    @Override
    public void setBorderStyle(Boundary boundary, StyleValue style) {
        setAttribute(style, boundary, TypeAttribute.STYLE);
    }

    @Override
    public void setBorderStyles(List<Boundary> boundaries, StyleValue styles) {
        for (Boundary boundary : boundaries) {
            setAttribute(styles, boundary, TypeAttribute.STYLE);
        }
    }

    @Override
    public void setBorderStyles(Map<Boundary, StyleValue> styles) {
        for (Boundary boundary : styles.keySet()) {
            setAttribute(styles.get(boundary), boundary, TypeAttribute.STYLE);
        }
    }

    @Override
    public void setBorderColor(Boundary boundary, ColorValue color) {
        setAttribute(color, boundary, TypeAttribute.COLOR);
    }

    @Override
    public void setBorderColors(List<Boundary> boundaries, ColorValue colors) {
        for (Boundary boundary : boundaries) {
            setAttribute(colors, boundary, TypeAttribute.COLOR);
        }
    }

    @Override
    public void setBorderColors(Map<Boundary, ColorValue> colors) {
        for (Boundary boundary : colors.keySet()) {
            setAttribute(colors.get(boundary), boundary, TypeAttribute.COLOR);
        }
    }

    @Override
    public void setTopAttributes(DimensionValue width, StyleValue style, ColorValue color) {
        setAttribute(width, Boundary.TOP, TypeAttribute.WIDTH);
        setAttribute(style, Boundary.TOP, TypeAttribute.STYLE);
        setAttribute(color, Boundary.TOP, TypeAttribute.COLOR);
    }

    @Override
    public void setBottomAttributes(DimensionValue width, StyleValue style, ColorValue color) {
        setAttribute(width, Boundary.BOTTOM, TypeAttribute.WIDTH);
        setAttribute(style, Boundary.BOTTOM, TypeAttribute.STYLE);
        setAttribute(color, Boundary.BOTTOM, TypeAttribute.COLOR);
    }

    @Override
    public void setRightAttributes(DimensionValue width, StyleValue style, ColorValue color) {
        setAttribute(width, Boundary.RIGHT, TypeAttribute.WIDTH);
        setAttribute(style, Boundary.RIGHT, TypeAttribute.STYLE);
        setAttribute(color, Boundary.RIGHT, TypeAttribute.COLOR);
    }

    @Override
    public void setLeftAttributes(DimensionValue width, StyleValue style, ColorValue color) {
        setAttribute(width, Boundary.LEFT, TypeAttribute.WIDTH);
        setAttribute(style, Boundary.LEFT, TypeAttribute.STYLE);
        setAttribute(color, Boundary.LEFT, TypeAttribute.COLOR);
    }

    @Override
    public void setAllBoundaries(DimensionValue width, StyleValue style, ColorValue color) {
        System.out.println("in BorderDeclarationImpl.setAllBoundaries()");
        for (Boundary boundary : Boundary.values()) {
            setAttribute(width, boundary, TypeAttribute.WIDTH);
        }
        for (Boundary boundary : Boundary.values()) {
            setAttribute(style, boundary, TypeAttribute.STYLE);
        }
        for (Boundary boundary : Boundary.values()) {
            setAttribute(color, boundary, TypeAttribute.COLOR);
        }
    }

    @Override
    protected String getKey(Boundary boundary, TypeAttribute type) {
        StringBuilder key = new StringBuilder();
        key.append("border-");
        key.append(boundary.boundary());
        key.append("-");
        key.append(type.type());
        return key.toString();
    }
}
