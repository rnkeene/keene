/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.value;

/**
 * Defines a value for a specific style
 * i.e. 20 for width
 *
 * TODO: get em vs. px switching easily, and other stuff like that
 *
 * @author matt
 */
public interface Value {
    public String getValue();
}
