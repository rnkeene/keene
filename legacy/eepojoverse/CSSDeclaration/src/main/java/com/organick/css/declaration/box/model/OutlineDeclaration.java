/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.declaration.box.model;

import com.organick.css.declaration.DeclarationSet;
import com.organick.css.declaration.value.ColorValue;
import com.organick.css.declaration.value.DimensionValue;
import com.organick.css.declaration.value.StyleValue;

/**
 *
 * @author matt
 */
public abstract class OutlineDeclaration extends DeclarationSet {
    public abstract void setStyle(StyleValue style);
    public abstract void setColor(ColorValue color);
    public abstract void setWidth(DimensionValue width);
    public abstract void setAllAttributes(StyleValue style, ColorValue color, DimensionValue width);
}
