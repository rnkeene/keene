/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.rule.factory;

import com.organick.css.declaration.Declaration;
import com.organick.css.rule.Rule;
import com.organick.css.rule.Selector;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author matt
 */
class RuleImpl extends ArrayList<Declaration> implements Rule {

    private List<Selector> selectors;

    @Override
    public List<Selector> getSelectors() {
        init();
        return selectors;
    }

    @Override
    public Rule addSelector(Selector selector) {
        init();
        selectors.add(selector);
        return this;
    }

    private void init(){
        if (selectors == null){
            selectors = new ArrayList<Selector>();
        }
    }

}
