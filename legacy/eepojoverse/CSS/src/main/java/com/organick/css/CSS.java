/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css;

import com.organick.css.rule.Rule;
import java.util.Map;

/**
 * Implement this interface to build your CSS files by adding {@link com.organick.css.rule.Rule}'s
 *
 * @author rnkeene
 */
public interface CSS extends Map<String, Rule>{

    public String getStylesheet();
}
