/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css;

import org.w3c.dom.Element;

/**
 * This is the Style object that should wrap any elements you want to apply CSS to
 *
 * @author rnkeene
 */
public interface Style extends Element {
    public void addCSS(CSS style, String className);
    public void addClassName(String className);
    public void setElement(Element element);
}
