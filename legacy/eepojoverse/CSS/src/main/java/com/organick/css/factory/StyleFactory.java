/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.factory;

import com.organick.css.Style;
import org.w3c.dom.Element;

/**
 *
 * @author matt
 */
public class StyleFactory {
    
    public static Style getInstance(){
        Style style = new StyleImpl();
        return style;
    }

    public static Style getInstance(Element element){
        Style style = new StyleImpl();
        style.setElement(element);
        return style;
    }
}
