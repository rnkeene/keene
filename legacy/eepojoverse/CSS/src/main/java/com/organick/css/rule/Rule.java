/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.rule;

import com.organick.css.declaration.Declaration;
import java.util.List;

/**
 * The CSSRule interface represents a rule within a CSS file.
 * The selector(s) are normally the HTML element(s) you want to style.
 * The Rule is itself a List of Declarations (key, value pair) defining the styles of the Rule
 *
 * @author matt
 */
public interface Rule extends List<Declaration>{
    /**
     * Get all the current selectors for this rule
     *
     * @return
     */
    public List<Selector> getSelectors();

    /**
     * Add a selector to this rule
     *
     * @param selector
     */
    public Rule addSelector(Selector selector);

}
