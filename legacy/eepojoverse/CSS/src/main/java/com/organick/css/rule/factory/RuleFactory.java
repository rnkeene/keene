/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.rule.factory;

import com.organick.css.rule.Rule;
import com.organick.css.rule.Selector;
import com.organick.css.rule.Selector.SelectorType;

/**
 *
 * @author matt
 */
public class RuleFactory {

    public static Rule createRule(){
        return new RuleImpl();
    }

    public static Selector createSelector(){
        return new SelectorImpl();
    }

    public static Selector createSelector(String name, SelectorType selectorType){
        Selector selector = new SelectorImpl();
        selector.setName(name);
        selector.setSelectorType(selectorType);
        return selector;
    }

}
