/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.factory;

import com.organick.css.CSS;
import com.organick.css.declaration.Declaration;
import com.organick.css.rule.Rule;
import com.organick.css.rule.Selector;
import java.util.LinkedHashMap;

/**
 *
 * @author matt
 */
public class CSSImpl extends LinkedHashMap<String, Rule> implements CSS{
    
    @Override
    public String getStylesheet() {
        StringBuilder css = new StringBuilder();

        for (String ruleName : keySet()) {
            Rule rule = get(ruleName);
            for (Selector selector : rule.getSelectors()){
                css.append(selector.getSelectorType().type());
                css.append(selector.getName());
                if (selector.getPsuedoClass() != null){
                    css.append(":");
                    css.append(selector.getPsuedoClass().getClassName());
                }
                css.append(" ");
            }
            css.append("{\n");
            for (Declaration declaration : rule) {
                css.append(declaration);
            }
            css.append("}\n");
        }
        return css.toString();
    }
}
