/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.rule.factory;

import com.organick.css.rule.Selector;

/**
 *
 * @author matt
 */
class SelectorImpl implements Selector {

    private String name;
    private SelectorType selectorType;
    private PsuedoClass psuedoClass;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public SelectorType getSelectorType() {
        return selectorType;
    }

    @Override
    public void setSelectorType(SelectorType selectorType) {
        this.selectorType = selectorType;
    }

    @Override
    public PsuedoClass getPsuedoClass() {
        return psuedoClass;
    }

    @Override
    public void setPsuedoClass(PsuedoClass psuedoClass) {
        this.psuedoClass = psuedoClass;
    }
}
