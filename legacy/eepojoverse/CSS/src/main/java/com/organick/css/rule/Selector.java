package com.organick.css.rule;

/**
 * The selector is normally the HTML element you want to style
 *
 * @author matt
 */
public interface Selector {

    /**
     * Name of the selector (i.e. "error")
     *
     * @return String
     */
    public String getName();

    /**
     * Name of the selector (i.e. "error")
     *
     * @param name
     */
    public void setName(String name);

    /**
     * Name selector type - i.e. ID('.'), CLASS('#'), or CONTEXTUAL('')
     *
     * @return selector type
     */
    public SelectorType getSelectorType();

    /**
     * Name selector type - i.e. ID('.'), CLASS('#'), or CONTEXTUAL('')
     *
     * @param selector type
     */
    public void setSelectorType(SelectorType selectorType);

    /**
     * Get the psuedo-class that will be applied to the element. i.e. hover
     *
     * @return psuedoClass
     */
    public PsuedoClass getPsuedoClass();

    /**
     * Set the psuedo-class that will be applied to the element. i.e. hover
     *
     * @param psuedoClass
     */
    public void setPsuedoClass(PsuedoClass psuedoClass);

    /**
     * The type of element you are applying styles to.
     */
    public enum SelectorType {

        /**
         * apply this style to an element based on it's id attribute
         */
        ID("#"),
        /**
         * apply this style to an element based on it's class attribute
         */
        CLASS("."),
        /**
         * apply this style to all elements of this type. i.e. div
         */
        CONTEXTUAL("");
        private String name;

        SelectorType(String name) {
            this.name = name;
        }

        public String type() {
            return name;
        }
    }

    /**
     * CSS pseudo-classes are used to add special effects to some selectors.
     */
    public enum PsuedoClass {

        LINK("link"),
        VISITED("visited"),
        ACTIVE("active"),
        HOVER("hover"),
        FOCUS("focus"),
        FIRST_LETTER("first-letter"),
        FIRST_LINE("first-line"),
        FIRST_CHILD("first-child"),
        BEFORE("before"),
        AFTER("after"),
        LANG("lang");
        
        private String name;

        PsuedoClass(String name) {
            this.name = name;
        }

        public String getClassName(){
            return name;
        }
    }
}
