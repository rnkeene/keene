/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.organick.css.factory;

import com.organick.css.Style;
import com.organick.css.CSS;
import com.organick.css.declaration.Declaration;
import com.organick.css.rule.Rule;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;
import org.w3c.dom.UserDataHandler;


/**
 *
 * @author rnkeene
 */
public class StyleImpl implements Style {

    private Element element;

    StyleImpl(){}

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    @Override
    public void addCSS(CSS style, String className) {
        System.out.println("in addCSS()");
        Rule rule = style.get(className);
        
        for (Declaration declaration : rule){
            StringBuilder currentStyle = new StringBuilder();
            if (element.getAttribute("style") != null){
                currentStyle.append(element.getAttribute("style"));
            }
            currentStyle.append(declaration);
            element.setAttribute("style", currentStyle.toString());
        }
    }

    @Override
    public void addClassName(String className) {
        StringBuilder currentClass = new StringBuilder();
        if (element.getAttribute("class") != null){
            currentClass.append(element.getAttribute("class"));
            currentClass.append(" ");
        }
        currentClass.append(className);
        element.setAttribute("class", currentClass.toString());
    }

    public String getTagName() {
        return element.getTagName();
    }

    public String getAttribute(String string) {
        return element.getAttribute(string);
    }

    public void setAttribute(String string, String string1) throws DOMException {
        element.setAttribute(string, string1);
    }

    public void removeAttribute(String string) throws DOMException {
        element.removeAttribute(string);
    }

    public Attr getAttributeNode(String string) {
        return element.getAttributeNode(string);
    }

    public Attr setAttributeNode(Attr attr) throws DOMException {
        return element.setAttributeNode(attr);
    }

    public Attr removeAttributeNode(Attr attr) throws DOMException {
        return element.removeAttributeNode(attr);
    }

    public NodeList getElementsByTagName(String string) {
        return element.getElementsByTagName(string);
    }

    public String getAttributeNS(String string, String string1) throws DOMException {
        return element.getAttributeNS(string, string1);
    }

    public void setAttributeNS(String string, String string1, String string2) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeAttributeNS(String string, String string1) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Attr getAttributeNodeNS(String string, String string1) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Attr setAttributeNodeNS(Attr attr) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public NodeList getElementsByTagNameNS(String string, String string1) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasAttribute(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasAttributeNS(String string, String string1) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public TypeInfo getSchemaTypeInfo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setIdAttribute(String string, boolean bln) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setIdAttributeNS(String string, String string1, boolean bln) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setIdAttributeNode(Attr attr, boolean bln) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getNodeName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getNodeValue() throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setNodeValue(String string) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public short getNodeType() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node getParentNode() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public NodeList getChildNodes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node getFirstChild() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node getLastChild() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node getPreviousSibling() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node getNextSibling() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public NamedNodeMap getAttributes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Document getOwnerDocument() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node insertBefore(Node node, Node node1) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node replaceChild(Node node, Node node1) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node removeChild(Node node) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node appendChild(Node node) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasChildNodes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Node cloneNode(boolean bln) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void normalize() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isSupported(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getNamespaceURI() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getPrefix() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setPrefix(String string) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getLocalName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasAttributes() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getBaseURI() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public short compareDocumentPosition(Node node) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getTextContent() throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setTextContent(String string) throws DOMException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isSameNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String lookupPrefix(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isDefaultNamespace(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String lookupNamespaceURI(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isEqualNode(Node node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Object getFeature(String string, String string1) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Object setUserData(String string, Object o, UserDataHandler udh) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Object getUserData(String string) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    

}
