package com.organick.persist.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//import org.apache.commons.codec.binary.Base64;
//import org.apache.commons.lang.StringEscapeUtils;
//import org.jasypt.util.text.BasicTextEncryptor;

public class SerializeUtil
{
    /*private static BasicTextEncryptor crypt;
    
    static
    {
        crypt = new BasicTextEncryptor();
        crypt.setPassword(System.getProperty("phriws.key"));
    }*/
    
    public static byte[] serializeObject(Object obj) throws Exception
    {
        try
        {
            byte[] serialized = serialize(obj);
            //String encrypted = crypt.encrypt(serialized);
            //String escaped = StringEscapeUtils.escapeHtml(encrypted);
            return serialized;
        }
        catch (Exception e)
        {
            throw e;
        }
    }
    
    public static Object deserializeObject(byte[] serialized) throws Exception
    {
        if (serialized != null)
        {
            //String tokenUnescaped = StringEscapeUtils.unescapeHtml(token);
            //String tokenDecrypted = crypt.decrypt(tokenUnescaped);
            return (Object) deserialize(serialized);
        }
        
        throw new Exception("Invalid Request");
    }
    
    /**
     * Serializes the object into a Base64 encoded string
     * 
     * @param login
     * @return serialized string
     */
    private static byte[] serialize(Object serializable) throws Exception
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try
        {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(serializable);
            //byte[] base64Encoding = Base64.encodeBase64(baos.toByteArray());
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            throw e;
        }
    }
    
    /**
     * De-serializes a derivative instance of this class that's in a <br>
     * Base64 encoded serialized format.
     */
    private static Object deserialize(byte[] serialized) throws IOException, ClassNotFoundException
    {
        //byte [] serializedBytes = Base64.decodeBase64(serializedToken.getBytes());
        ByteArrayInputStream bais = new ByteArrayInputStream(serialized);
        ObjectInputStream ois = new ObjectInputStream(bais);

        return ois.readObject();
    }
}
