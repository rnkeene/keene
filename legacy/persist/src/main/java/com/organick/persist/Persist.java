package com.organick.persist;

import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class Persist
{
    private static PersistDAO persistDAO;

    static
    {
        //System.out.println(System.getProperties());
        //System.setProperty("java.class.path", System.getProperty("java.class.path") + "/home/rnkeene/Desktop/svn/mavenProjects/persist/src/main/resources");
        //System.out.println(System.getProperty("java.class.path"));
        XmlBeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("beans.xml"));

        for (String beanName : beanFactory.getBeanDefinitionNames())
        {
            System.out.println(beanName);
        }

        persistDAO = (PersistDAO) beanFactory.getBean("persistDAO");
    }

    public static boolean save(Serializable serializable, String id) throws Exception
    {
        persistDAO.saveObject(serializable, id);
        return true;
    }

    public static boolean update(Serializable serializable, String id) throws Exception
    {
        return persistDAO.updateObject(serializable, id);
    }

    public static <T extends Serializable> T get(String id, String className) throws Exception
    {
        return (T)persistDAO.getObject(id, className);
    }

    public static <T extends Serializable> List<T> getByClass(String className) throws Exception
    {
        return persistDAO.getObjectsByClassName(className);
    }
}