package com.organick.persist.entity;

import java.io.Serializable;
import java.util.Arrays;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@IdClass(ObjectId.class)
@Table(name="serializedObjects")
public class ObjectPersist implements Serializable
{
    @Id
    @Column(name="serializedObjectsId")
    //@GeneratedValue(strategy=GenerationType.AUTO)
    private String id;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Id
    @Column
    private String className;

    public String getClassName()
    {
        return className;
    }

    public void setClassName(String className)
    {
        this.className = className;
    }

    @Lob
    private byte[] serializedObject;

    public byte[] getSerializedObject()
    {
        return serializedObject;
    }

    public void setSerializedObject(byte[] serializedObject)
    {
        this.serializedObject = serializedObject;
    }

    @Column
    private String xmlObject;

    public String getXmlObject() {
        return xmlObject;
    }

    public void setXmlObject(String xmlObject) {
        this.xmlObject = xmlObject;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ObjectPersist other = (ObjectPersist) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.className == null) ? (other.className != null) : !this.className.equals(other.className)) {
            return false;
        }
        if (!Arrays.equals(this.serializedObject, other.serializedObject)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 79 * hash + (this.className != null ? this.className.hashCode() : 0);
        hash = 79 * hash + Arrays.hashCode(this.serializedObject);
        return hash;
    }


    
}