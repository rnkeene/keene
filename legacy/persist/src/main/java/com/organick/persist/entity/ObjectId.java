/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.organick.persist.entity;

import java.io.Serializable;

/**
 *
 * @author matt
 */
public class ObjectId implements Serializable{

    private String id;
    private String className;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ObjectId() {}

    public ObjectId(String id, String className)
    {
        this.id = id;
        this.className = className;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ObjectId other = (ObjectId) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.className == null) ? (other.className != null) : !this.className.equals(other.className)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 53 * hash + (this.className != null ? this.className.hashCode() : 0);
        return hash;
    }
}
