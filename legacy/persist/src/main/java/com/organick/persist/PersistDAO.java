package com.organick.persist;

import com.organick.persist.entity.ObjectId;
import com.organick.persist.entity.ObjectPersist;
import com.organick.persist.util.SerializeUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

class PersistDAO extends HibernateDaoSupport
{

   
    public boolean saveObject(Serializable serializable, String id) throws Exception
    {
       ObjectPersist entity = new ObjectPersist();
       byte[] serialized = SerializeUtil.serializeObject(serializable);
       entity.setSerializedObject(serialized);
       entity.setClassName(serializable.getClass().getName());
       entity.setId(id);
       getHibernateTemplate().save(entity);
       return true;
    }

    public <T extends Serializable> T getObject(String id, String className) throws Exception
    {
        System.out.println("PersistDAO.getObject() getting " + className + "type object with ID " + id);
        ObjectPersist entity = getHibernateTemplate().get(ObjectPersist.class, new ObjectId(id, className));
        if (entity != null)
        {
            return (T) SerializeUtil.deserializeObject(entity.getSerializedObject());
        }

        return null;
    }

    public boolean updateObject(Serializable serializable, String id) throws Exception
    {
        ObjectPersist entity = getHibernateTemplate().get(ObjectPersist.class, new ObjectId(id, serializable.getClass().getName()));
        if (entity != null)
        {
            if (!entity.getClassName().equals(serializable.getClass().getName()))
            {
                throw new Exception("Error! Tried to update an object of a different type");
            }

            entity.setSerializedObject(SerializeUtil.serializeObject(serializable));
            getHibernateTemplate().merge(entity);

            return true;
        }

        return false;
    }

    public <T extends Serializable> List<T> getObjectsByClassName(String className) throws Exception
    {
        List<ObjectPersist> entities = getHibernateTemplate().find("from ObjectPersist obj where obj.className=?",className);
        List<T> objects= new ArrayList<T>();
        if (entities != null)
        {
            for (ObjectPersist entity : entities)
            {
                objects.add((T) SerializeUtil.deserializeObject(entity.getSerializedObject()));
            }
        }

        return objects;
    }

}