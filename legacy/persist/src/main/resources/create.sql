
CREATE TABLE serializedObjects (
    id varchar(250) NOT NULL,
    className character varying(250) NOT NULL,
    serializedObject blob NOT NULL,
    PRIMARY KEY (id, className)
);