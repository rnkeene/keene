package com.redhat.cms.browser.invoker.browser.remote.factory;

import com.redhat.cms.browser.invoker.browser.factory.RedHatBrowserFactory;
import com.redhat.cms.browser.invoker.browser.controller.RemoteBrowserServer;

public abstract class RedHatRemoteBrowserFactory implements RedHatBrowserFactory {

    private RemoteBrowserServer seleniumHub;

    protected RedHatRemoteBrowserFactory(RemoteBrowserServer seleniumHub) {
        this.seleniumHub = seleniumHub;
    }

    public RemoteBrowserServer getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(RemoteBrowserServer seleniumHub) {
        this.seleniumHub = seleniumHub;
    }
    
}
