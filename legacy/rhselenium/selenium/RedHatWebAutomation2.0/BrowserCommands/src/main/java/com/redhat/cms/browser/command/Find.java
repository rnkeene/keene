package com.redhat.cms.browser.command;

public interface Find<I,R> extends RedHatBrowserCommand<I> {

    public String getTarget();

    public void setTarget(String target);

    public R getFindResults();

    public void setFindResults(R results);
    
}
