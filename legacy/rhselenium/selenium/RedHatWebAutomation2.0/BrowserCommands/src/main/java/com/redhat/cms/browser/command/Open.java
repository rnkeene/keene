package com.redhat.cms.browser.command;

public interface Open<I> extends RedHatBrowserCommand<I> {

    public String getUrl();

    public void setUrl(String url);
}
