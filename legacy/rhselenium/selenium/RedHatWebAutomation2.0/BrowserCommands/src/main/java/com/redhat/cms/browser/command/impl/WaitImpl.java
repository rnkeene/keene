package com.redhat.cms.browser.command.impl;

import com.redhat.cms.browser.command.Wait;

public abstract class WaitImpl<I> implements Wait<I>{

    private long waitTime;

    protected WaitImpl() {
    }

    protected WaitImpl(long waitTime) {
        this.waitTime = waitTime;
    }

    @Override
    public long getWaitTime() {
        return waitTime;
    }

    @Override
    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }
    
    @Override
    public String toString() {
        return Long.toString(this.waitTime);
    }
}
