package com.redhat.cms.browser.builder;

import com.redhat.cms.browser.command.RedHatBrowserCommand;
import com.redhat.cms.browser.invoker.browser.RedHatBrowserInvoker;

public interface RedHatBrowserCommandBuilder
{

    public RedHatBrowserCommand buildCommand(RedHatBrowserInvoker invoker);
}
