package com.redhat.cms.browser.command;

public interface Type<I,R> extends Find<I,R>{
    
    public String getValue();

    public void setValue(String value);

}
