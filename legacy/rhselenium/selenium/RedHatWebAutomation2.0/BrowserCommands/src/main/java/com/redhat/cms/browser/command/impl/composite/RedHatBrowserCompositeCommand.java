package com.redhat.cms.browser.command.impl.composite;

import com.redhat.cms.browser.command.RedHatBrowserCommand;
import java.util.ArrayList;

public class RedHatBrowserCompositeCommand<I> extends ArrayList<RedHatBrowserCommand> implements RedHatBrowserCommand<I>
{

    @Override
    public void execute(I invoker)
    {
        for (RedHatBrowserCommand command : this)
        {
            command.execute(invoker);
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\t\t - Red Hat Composite Command:");
        for (RedHatBrowserCommand command : this)
        {
            toString.append("\n\t\t\t");
            toString.append(command);
        }
        return toString.toString();
    }
}
