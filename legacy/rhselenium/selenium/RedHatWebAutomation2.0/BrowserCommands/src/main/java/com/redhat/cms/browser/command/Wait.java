package com.redhat.cms.browser.command;

public interface Wait<I> extends RedHatBrowserCommand<I> {

    public long getWaitTime();

    public void setWaitTime(long waitTime);
}