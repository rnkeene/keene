package com.redhat.cms.browser.command;

public interface Close<I> extends RedHatBrowserCommand<I>
{
}
