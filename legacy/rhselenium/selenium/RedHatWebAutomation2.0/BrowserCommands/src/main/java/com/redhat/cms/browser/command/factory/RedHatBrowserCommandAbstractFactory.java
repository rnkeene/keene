package com.redhat.cms.browser.command.factory;

import com.redhat.cms.browser.command.Click;
import com.redhat.cms.browser.command.Close;
import com.redhat.cms.browser.command.Find;
import com.redhat.cms.browser.command.Open;
import com.redhat.cms.browser.command.Type;
import com.redhat.cms.browser.command.Wait;

public abstract class RedHatBrowserCommandAbstractFactory
{

    public abstract Click buildClick();

    public abstract Close buildClose();

    public abstract Find buildFind();

    public abstract Open buildOpen();

    public abstract Type buildType();

    public abstract Wait buildWait();
    
}
