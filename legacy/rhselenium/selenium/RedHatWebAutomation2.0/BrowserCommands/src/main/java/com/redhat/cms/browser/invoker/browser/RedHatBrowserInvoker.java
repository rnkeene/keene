package com.redhat.cms.browser.invoker.browser;

import com.redhat.cms.browser.command.RedHatBrowserCommand;
import com.redhat.cms.browser.command.impl.composite.RedHatBrowserCompositeCommand;

public abstract class RedHatBrowserInvoker<I>
{
    private RedHatBrowserCompositeCommand<I> commands;
    private I invoker;

    protected RedHatBrowserInvoker()
    {
        this.commands = new RedHatBrowserCompositeCommand<I>();
    }

    public void addCommand(RedHatBrowserCommand command)
    {
        this.commands.add(command);
    }

    public void execute()
    {
        if (this.invoker != null)
        {
            this.commands.execute(this.invoker);
        }
    }

    private void execute(I invoker)
    {
        for (RedHatBrowserCommand<I> command : this.commands)
        {
            command.execute(invoker);
        }
    }

    public I getInvoker()
    {
        return invoker;
    }

    public void setInvoker(I invoker)
    {
        this.invoker = invoker;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\t\t - Red Hat Browser Invoker:\n\t\t\tInvoker: ");
        toString.append(this.invoker);
        toString.append("\n\t\t\t Commands: ");
        toString.append(this.commands);
        return toString.toString();
    }
}
