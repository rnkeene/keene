package com.redhat.cms.browser.command;

public interface RedHatBrowserCommand<I>
{

    public void execute(I invoker);
}
