package com.redhat.cms.browser.command.impl;

import com.redhat.cms.browser.command.Find;

public abstract class FindImpl<I,R> implements Find<I,R>{

    private String target;
    private R results;

    public FindImpl()
    {
    }

    protected FindImpl(String target) {
        this.target = target;
    }

    @Override
    public String getTarget() {
        return target;
    }

    @Override
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public R getFindResults() {
        return this.results;
    }

    @Override
    public void setFindResults(R results) {
        this.results = results;
    }

    @Override
    public String toString() {
        if(results!=null){
            return this.results.toString();
        }
        return this.target;
    }
}
