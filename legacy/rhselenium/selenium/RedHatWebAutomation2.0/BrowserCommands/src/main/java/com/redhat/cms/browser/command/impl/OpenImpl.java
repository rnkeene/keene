package com.redhat.cms.browser.command.impl;

import com.redhat.cms.browser.command.Open;

public abstract class OpenImpl<I> implements Open<I>{

    private String url;

    public OpenImpl()
    {
    }

    protected OpenImpl(String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return this.url;
    }

}
