package com.redhat.cms.selenium.invoker.browser.driver;

import com.redhat.cms.browser.invoker.browser.RedHatBrowserInvoker;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverInvoker extends RedHatBrowserInvoker<RemoteWebDriver>
{

    private RemoteWebDriver driver;

    public DriverInvoker()
    {
        super();
        super.setInvoker(driver);
    }

    public RemoteWebDriver getDriver()
    {
        return driver;
    }

    public void setDriver(RemoteWebDriver driver)
    {
        this.driver = driver;
    }

}
