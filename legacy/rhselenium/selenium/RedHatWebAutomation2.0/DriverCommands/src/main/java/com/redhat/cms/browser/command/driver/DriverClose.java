package com.redhat.cms.browser.command.driver;

import com.redhat.cms.browser.command.Close;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverClose implements Close<RemoteWebDriver>
{

    public DriverClose()
    {
    }

    @Override
    public void execute(RemoteWebDriver browser)
    {
        browser.quit();
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.quit()");
        return toString.toString();
    }
}
