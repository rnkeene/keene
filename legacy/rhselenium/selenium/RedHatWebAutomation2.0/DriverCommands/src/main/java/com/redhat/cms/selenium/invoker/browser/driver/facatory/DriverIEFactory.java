package com.redhat.cms.selenium.invoker.browser.driver.facatory;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverIEFactory implements DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new InternetExplorerDriver();
    }

}
