package com.redhat.cms.selenium.invoker.browser.driver.facatory;

import org.openqa.selenium.remote.RemoteWebDriver;

public interface DriverFactory {

    public RemoteWebDriver getInstance();

}
