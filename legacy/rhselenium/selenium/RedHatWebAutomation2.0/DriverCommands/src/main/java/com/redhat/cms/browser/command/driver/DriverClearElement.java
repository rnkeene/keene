package com.redhat.cms.browser.command.driver;

import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverClearElement extends DriverFindElement
{

    public DriverClearElement(String xpath)
    {
        super(xpath);
    }

    @Override
    public void execute(RemoteWebDriver browser)
    {
        super.execute(browser);
        this.getFindResults().clear();
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append(super.toString());
        toString.append(".clear()");
        return toString.toString();
    }
}
