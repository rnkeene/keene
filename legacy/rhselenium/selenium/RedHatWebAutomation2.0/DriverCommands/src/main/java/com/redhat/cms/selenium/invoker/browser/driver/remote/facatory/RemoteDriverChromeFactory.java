package com.redhat.cms.selenium.invoker.browser.driver.remote.facatory;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteDriverChromeFactory extends RemoteDriverFactory{

    public RemoteDriverChromeFactory(SeleniumHub hub) {
        super(hub, DesiredCapabilities.chrome());
    }

}
