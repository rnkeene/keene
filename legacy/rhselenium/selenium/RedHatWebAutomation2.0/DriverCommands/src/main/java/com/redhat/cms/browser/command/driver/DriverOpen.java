package com.redhat.cms.browser.command.driver;

import com.redhat.cms.browser.command.impl.OpenImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverOpen extends OpenImpl<RemoteWebDriver>
{

    public DriverOpen()
    {
    }

    public DriverOpen(String url)
    {
        super(url);
    }

    @Override
    public void execute(RemoteWebDriver browser)
    {
        browser.get(this.getUrl());
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.get(");
        toString.append(")");
        return toString.toString();
    }
}
