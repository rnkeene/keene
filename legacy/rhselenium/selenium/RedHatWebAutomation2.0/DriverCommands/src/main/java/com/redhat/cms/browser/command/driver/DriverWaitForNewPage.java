package com.redhat.cms.browser.command.driver;

import com.redhat.cms.browser.command.impl.WaitImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverWaitForNewPage extends WaitImpl<RemoteWebDriver>
{

    private String currentPage;

    public DriverWaitForNewPage(long duration, String currentPage)
    {
        super(duration);
        this.currentPage = currentPage;
    }

    @Override
    public synchronized void execute(RemoteWebDriver browser)
    {
        while (browser.getCurrentUrl().equals(currentPage))
        {
            try
            {
                wait(this.getWaitTime());
            } catch (InterruptedException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.wait(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
