package com.redhat.cms.selenium.invoker.browser.driver.remote.facatory;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteDriverIEFactory extends RemoteDriverFactory{

    public RemoteDriverIEFactory(SeleniumHub hub) {
        super(hub, DesiredCapabilities.internetExplorer());
        this.getCapabilities().setPlatform(Platform.ANY);
    }

}
