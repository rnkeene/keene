package com.redhat.cms.selenium.invoker.browser.driver.facatory;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverChromeFactory implements DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new ChromeDriver();
    }

}
