package com.redhat.cms.browser.command.driver;

import com.redhat.cms.browser.command.impl.WaitImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverWait extends WaitImpl<RemoteWebDriver>
{

    public DriverWait()
    {
    }

    public DriverWait(long duration)
    {
        super(duration);
    }

    @Override
    public synchronized void execute(RemoteWebDriver browser)
    {
        try
        {
            this.wait(this.getWaitTime());
        } catch (InterruptedException ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.wait(");
        toString.append(")\n");
        return toString.toString();
    }
}
