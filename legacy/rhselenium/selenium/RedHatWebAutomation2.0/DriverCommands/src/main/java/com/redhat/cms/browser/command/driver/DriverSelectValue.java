package com.redhat.cms.browser.command.driver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class DriverSelectValue extends DriverFindElement
{

    private String value;
    private Select driverSelect;

    public DriverSelectValue(String target, String value)
    {
        super(target);
        this.value = value;
    }

    @Override
    public void execute(RemoteWebDriver browser)
    {
        super.execute(browser);
        this.driverSelect = new Select(this.getFindResults());
        this.driverSelect.selectByVisibleText(this.value);
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: new Select(");
        toString.append(super.toString());
        toString.append(").selectByVisibleText(");
        toString.append(this.value);
        toString.append(")");
        return toString.toString();
    }
}
