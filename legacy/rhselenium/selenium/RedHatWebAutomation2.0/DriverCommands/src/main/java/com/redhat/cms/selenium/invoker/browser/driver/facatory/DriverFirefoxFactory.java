package com.redhat.cms.selenium.invoker.browser.driver.facatory;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFirefoxFactory implements DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new FirefoxDriver();
    }

}
