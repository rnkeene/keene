package com.redhat.cms.browser.command.driver;

import com.redhat.cms.browser.command.Screenshot;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverScreenshot implements Screenshot<RemoteWebDriver>
{

    @Override
    public void execute(RemoteWebDriver browser)
    {
        WebDriver augmentedDriver = new Augmenter().augment(browser);
        File file = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
        try
        {
            FileUtils.copyFile(file, new File("/home/nkeene/NetBeansProjects/RedHatSelenium/FitnesseDecorator/target/pics/" + Calendar.getInstance().getTimeInMillis() + ".jpg"));
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.TakesScreenshot(");
        toString.append(super.toString());
        toString.append(")");
        return toString.toString();
    }
}
