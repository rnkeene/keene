package com.redhat.cms.browser.command.driver.factory;

import com.redhat.cms.browser.command.Click;
import com.redhat.cms.browser.command.Close;
import com.redhat.cms.browser.command.Find;
import com.redhat.cms.browser.command.Open;
import com.redhat.cms.browser.command.Type;
import com.redhat.cms.browser.command.Wait;
import com.redhat.cms.browser.command.driver.DriverClick;
import com.redhat.cms.browser.command.driver.DriverClose;
import com.redhat.cms.browser.command.driver.DriverFindElement;
import com.redhat.cms.browser.command.driver.DriverOpen;
import com.redhat.cms.browser.command.driver.DriverSendKeys;
import com.redhat.cms.browser.command.driver.DriverWait;
import com.redhat.cms.browser.command.factory.RedHatBrowserCommandAbstractFactory;

public class DriverBrowserCommandFactory extends RedHatBrowserCommandAbstractFactory
{

    @Override
    public Click buildClick()
    {
        return new DriverClick();
    }

    @Override
    public Close buildClose()
    {
        return new DriverClose();
    }

    @Override
    public Find buildFind()
    {
        return new DriverFindElement();
    }

    @Override
    public Open buildOpen()
    {
        return new DriverOpen();
    }

    @Override
    public Type buildType()
    {
        return new DriverSendKeys();
    }

    @Override
    public Wait buildWait()
    {
        return new DriverWait();
    }

}
