package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.browser.command.Type;
import com.thoughtworks.selenium.Selenium;

public class SeleniumType extends SeleniumFind implements Type<Selenium, String> {

    private String value;

    public SeleniumType()
    {
    }

    public SeleniumType(String target, String value) {
        super(target);
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void execute(Selenium selenium){
        super.execute(selenium);
        selenium.type(this.getFindResults(), this.value);
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.type(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
