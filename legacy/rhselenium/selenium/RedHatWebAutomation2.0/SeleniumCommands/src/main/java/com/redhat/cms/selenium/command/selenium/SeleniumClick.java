package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.browser.command.Click;
import com.thoughtworks.selenium.Selenium;

public class SeleniumClick extends SeleniumFind implements Click<Selenium,String>{

    public SeleniumClick()
    {
    }

    public SeleniumClick(String target) {
        super(target);
    }

    @Override
    public void execute(Selenium selenium){
        super.execute(selenium);
        selenium.click(this.getFindResults());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.click(");
        toString.append(")\n");
        return toString.toString();
    }
}
