package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.browser.command.impl.OpenImpl;
import com.thoughtworks.selenium.Selenium;

public class SeleniumOpen extends OpenImpl<Selenium>{

    public SeleniumOpen()
    {
    }

    public SeleniumOpen(String url) {
        super(url);
    }

    @Override
    public void execute(Selenium selenium) {
        selenium.open(this.getUrl());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.open(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
