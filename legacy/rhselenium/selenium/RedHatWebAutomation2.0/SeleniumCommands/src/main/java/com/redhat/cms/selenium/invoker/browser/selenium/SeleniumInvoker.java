package com.redhat.cms.selenium.invoker.browser.selenium;

import com.redhat.cms.browser.invoker.browser.RedHatBrowserInvoker;
import com.thoughtworks.selenium.Selenium;

public class SeleniumInvoker extends RedHatBrowserInvoker<Selenium>
{

    private Selenium driver;

    public SeleniumInvoker()
    {
        super();
        super.setInvoker(driver);
    }

}
