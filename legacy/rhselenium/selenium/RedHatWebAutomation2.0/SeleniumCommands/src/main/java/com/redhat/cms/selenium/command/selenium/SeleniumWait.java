package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.browser.command.impl.WaitImpl;
import com.thoughtworks.selenium.Selenium;

public class SeleniumWait extends WaitImpl<Selenium> {

    public SeleniumWait() {
    }

    public SeleniumWait(long duration) {
        super(duration);
    }

    @Override
    public void execute(Selenium selenium){
        selenium.waitForPageToLoad(Long.toString(this.getWaitTime()));
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.waitForPageToLoad(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }

}
