package com.redhat.cms.selenium.command.selenium.factory;

import com.redhat.cms.browser.command.Click;
import com.redhat.cms.browser.command.Close;
import com.redhat.cms.browser.command.Find;
import com.redhat.cms.browser.command.Open;
import com.redhat.cms.browser.command.Type;
import com.redhat.cms.browser.command.Wait;
import com.redhat.cms.browser.command.factory.RedHatBrowserCommandAbstractFactory;
import com.redhat.cms.selenium.command.selenium.SeleniumClick;
import com.redhat.cms.selenium.command.selenium.SeleniumClose;
import com.redhat.cms.selenium.command.selenium.SeleniumFind;
import com.redhat.cms.selenium.command.selenium.SeleniumOpen;
import com.redhat.cms.selenium.command.selenium.SeleniumType;
import com.redhat.cms.selenium.command.selenium.SeleniumWait;

public class SeleniumBrowserCommandFactory extends RedHatBrowserCommandAbstractFactory
{

    @Override
    public Click buildClick()
    {
        return new SeleniumClick();
    }

    @Override
    public Close buildClose()
    {
        return new SeleniumClose();
    }

    @Override
    public Find buildFind()
    {
        return new SeleniumFind();
    }

    @Override
    public Open buildOpen()
    {
        return new SeleniumOpen();
    }

    @Override
    public Type buildType()
    {
        return new SeleniumType();
    }

    @Override
    public Wait buildWait()
    {
        return new SeleniumWait();
    }
}
