package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.browser.command.impl.FindImpl;
import com.thoughtworks.selenium.Selenium;

public class SeleniumFind extends FindImpl<Selenium,String>{

    public SeleniumFind()
    {
    }

    public SeleniumFind(String target) {
        super(target);
    }

    @Override
    public void execute(Selenium invoker){
        this.setFindResults(this.getTarget());
    }

}
