package com.redhat.marketing.automation.command.browser.concrete.selenium;

public class DriverWaitForElement extends DriverWait
{

    private int maxTries = 50;
    private String target;

    public DriverWaitForElement(String target, long interval)
    {
        super(interval);
        this.target = target;
    }

    @Override
    public void execute()
    {
        DriverFind find = new DriverFind(this.target);
        find.setBrowser(this.getBrowser());
        int tries = 0;
        while (find.getFindResults() == null && tries < maxTries)
        {
            try
            {
                super.execute();
                find.execute();                
            } catch (Exception e)
            {
            }
            tries++;
        }

    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.waitForElement(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
