package com.redhat.marketing.automation.command.browser.concrete.impl;

import com.redhat.marketing.automation.command.browser.concrete.Wait;

public abstract class WaitImpl<B> extends BrowserCommandImpl<B> implements Wait<B>{

    private long waitTime;

    protected WaitImpl() {
    }

    protected WaitImpl(long waitTime) {
        this.waitTime = waitTime;
    }

    @Override
    public long getWaitTime() {
        return waitTime;
    }

    @Override
    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }
    
    @Override
    public String toString() {
        return Long.toString(this.waitTime);
    }
}
