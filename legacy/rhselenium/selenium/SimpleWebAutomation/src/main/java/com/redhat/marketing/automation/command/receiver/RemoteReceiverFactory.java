package com.redhat.marketing.automation.command.receiver;

public abstract class RemoteReceiverFactory<B> implements ReceiverFactory<B> {

    private RemoteReceiverServer receiverServer;

    protected RemoteReceiverFactory(RemoteReceiverServer receiverServer) {
        this.receiverServer = receiverServer;
    }

    public RemoteReceiverServer getReceiverServer() {
        return receiverServer;
    }

    public void setReceiverServer(RemoteReceiverServer receiverServer) {
        this.receiverServer = receiverServer;
    }
    
}
