package com.redhat.marketing.automation.command;

import com.redhat.marketing.automation.kernel.KernelFunction;

/**
 *
 * This begins the slow growth towards a Web QA Operating System, centered around a core set of Network Based Commands
 * @author nkeene
 */
public interface NetworkCommand extends KernelFunction
{
}
