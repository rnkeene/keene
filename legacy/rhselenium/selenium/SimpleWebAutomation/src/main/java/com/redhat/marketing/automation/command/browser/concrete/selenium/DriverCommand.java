package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.*;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class DriverCommand extends BrowserCommandImpl<RemoteWebDriver>
{

    @Override
    public void initialize()
    {
    }
}
