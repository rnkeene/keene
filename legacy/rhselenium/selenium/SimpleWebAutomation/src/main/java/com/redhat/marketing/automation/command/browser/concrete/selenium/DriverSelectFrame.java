package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.FindImpl;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverSelectFrame extends FindImpl<RemoteWebDriver, WebElement> implements SeleniumDriverCommand
{
    private int frameLoadAttemptCount;

    public DriverSelectFrame(String targetClass)
    {
        super(targetClass);
    }

    @Override
    public void execute()
    {
        WebElement iframe = this.waitForIFrame();
        this.setFindResults(iframe);
        this.getBrowser().switchTo().frame(iframe);
    }

    private WebElement waitForIFrame()
    {
        WebElement iframe;
        try
        {
            List<WebElement> iframeList = this.getBrowser().findElements(By.tagName("iframe"));
            while (iframeList != null || iframeList.size() > 0)
            {
                iframe = iframeList.remove(0);
                if (iframe.getAttribute("class") != null && iframe.getAttribute("class").equals(this.getTarget()))
                {
                    return iframe;
                }
            }
        } catch (Exception e)
        {
        }
        DriverWait wait = new DriverWait(1000);
        wait.setBrowser(this.getBrowser());
        wait.execute();
        this.frameLoadAttemptCount++;
        return waitForIFrame();
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.switchTo().frame('name')");
        toString.append(super.toString());
        toString.append("))");
        return toString.toString();
    }
}
