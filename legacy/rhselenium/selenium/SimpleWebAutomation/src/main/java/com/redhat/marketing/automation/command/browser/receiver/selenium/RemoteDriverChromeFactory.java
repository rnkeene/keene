package com.redhat.marketing.automation.command.browser.receiver.selenium;

import com.redhat.marketing.automation.command.receiver.RemoteReceiverServer;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteDriverChromeFactory extends RemoteDriverFactory{

    public RemoteDriverChromeFactory(RemoteReceiverServer hub) {
        super(hub, DesiredCapabilities.chrome());
    }

}
