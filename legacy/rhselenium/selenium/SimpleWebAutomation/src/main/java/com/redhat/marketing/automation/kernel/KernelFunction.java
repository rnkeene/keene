package com.redhat.marketing.automation.kernel;

public interface KernelFunction
{

    public void execute();
}
