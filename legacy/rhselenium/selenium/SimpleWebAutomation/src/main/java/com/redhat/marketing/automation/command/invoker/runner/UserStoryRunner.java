package com.redhat.marketing.automation.command.invoker.runner;

import com.redhat.marketing.automation.command.builder.CommandBuilder;
import com.redhat.marketing.automation.command.invoker.UserStory;

public interface UserStoryRunner<U extends UserStory, B extends CommandBuilder>
{

    public boolean isReady();

    public void runUserStory();

    public void runUserStory(U story);

    public void runUserStory(U story, B builder);
}
