package com.redhat.marketing.automation.command.browser.receiver.selenium;

import com.redhat.marketing.automation.command.receiver.ReceiverFactory;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class DriverFactory implements ReceiverFactory<RemoteWebDriver>{

}
