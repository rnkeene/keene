package com.redhat.marketing.automation.command;

/**
 *
 * In order to achieve a high degree of Control Inversion while successfully managing a System of Unknown Network Complexity; an Indeterminite System, with Indeterminite Functionality.
 *
 * This System will not have as many problems, as it treats almost every
 * 
 * This operating system should remain as IOC friendly as possible; which means its stability is highly volitile if not managed carefully.
 *
 * All commands are required to be proxied... for management by the operating system.... such as interrupting commands, blocking commands, etc.
 *
 * e.g. Everything not wanting to damage this operating system!
 * 
 * @author nkeene
 */
public interface IndeterminateCommand<I, M extends IndeterminateCommand> extends InjectedCommand<I, M>, NetworkCommand
{

    /**
     * Any command that is not otherwise created or already set by the System.
     *
     * Creating Commands:
     * - Consider that your command must run 'along with any others' in the system, and exact system state can never be guaranteed.
     * - Appropriate considerations should be made when implementing new Commands on the system/context it executes within.
     *
     * Accessing Commands:
     * - Be sure if this is not an object that is creating commands, that any udates to the Command may have otherwise unknown effects on the system.     *
     */
    public void getCommand();
}
