package com.redhat.marketing.automation.command.browser.invoker.selenium.runner;

import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.SeleniumCommandBuilder;
import com.redhat.marketing.automation.command.browser.invoker.runner.BrowserUserStoryRunner;
import com.redhat.marketing.automation.command.browser.invoker.selenium.SeleniumUserStory;

public abstract class SeleniumUserStoryRunner<U extends SeleniumUserStory> extends BrowserUserStoryRunner<U, SeleniumCommandBuilder>
{
}
