package com.redhat.marketing.automation.command.exception;

/**
 *
 * It is important that if a Command within the system is unable to execute for some reason that it restores the System State to the point it started.
 *
 * If an ExceptionalCommand is used to handle these scenarios then there is a more robust conditional
 *
 * @author nkeene
 */
public class ExceptionalCommand {

}
