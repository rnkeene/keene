package com.redhat.marketing.automation.kernel;

/**
 *
 * This loosely coupled framework of code provides the components to handle the specific nature of Simple Problems in Web Testing.
 *
 * The intent is to provide a System of Command and Control around the Execution within a Federation of Web Testing Solutions.
 *
 * At any time there is anywhere between <b>0</b> to <b>Indeterminte</b> States available within the System; the Command itself should be as decoupled as possible from this information.
 *
 * For this System to successfully increment itself (e.g. Process Commands), it must be able to identify the context in which a Processes is to successfully perform a Command exection function.
 *  - StableState ::
 *  - UnstableState :: 
 *
 * @author nkeene
 */
public interface KernelState
{
}
