package com.redhat.marketing.automation.command.receiver;

public interface ReceiverFactory<B> {

    public B getInstance();
}
