package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.WaitImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverWait extends WaitImpl<RemoteWebDriver> implements SeleniumDriverCommand
{

    public DriverWait(long duration)
    {
        super(duration);
    }

    @Override
    public void execute()
    {
        try
        {
            Thread.sleep(this.getWaitTime());
        } catch (InterruptedException e)
        {
            assert false : "InterruptedException Exception while waiting - " + e.toString() + " - Command - " + this.toString();
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.wait(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
