package com.redhat.marketing.automation.command.browser.concrete.impl;

import com.redhat.marketing.automation.command.browser.concrete.Open;

public abstract class OpenImpl<B> extends BrowserCommandImpl<B> implements Open<B>{

    private String url;
    
    protected OpenImpl(String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return this.url;
    }

}
