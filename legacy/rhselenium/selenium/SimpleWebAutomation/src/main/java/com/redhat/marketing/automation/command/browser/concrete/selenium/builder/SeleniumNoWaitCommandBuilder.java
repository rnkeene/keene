package com.redhat.marketing.automation.command.browser.concrete.selenium.builder;

import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverWaitForElement;
import java.util.Date;

public abstract class SeleniumNoWaitCommandBuilder extends SeleniumWaitingCommandBuilder
{

    public SeleniumNoWaitCommandBuilder()
    {
        super(0);
    }
    
    public SeleniumNoWaitCommandBuilder(long standardWait)
    {
        super(standardWait);
    }

    @Override
    public void addClick(String xpath)
    {
        this.addClick(xpath, this.getStandardWait());
    }

    @Override
    public void addClosePopUp()
    {
        this.addClosePopUp(this.getStandardWait());
    }

    @Override
    public void addFind(String xpath)
    {
        this.addFind(xpath, this.getStandardWait());
    }

    @Override
    public void addOpen(String url)
    {
        this.addOpen(url, this.getStandardWait());
    }

    @Override
    public void addNavigate(String url)
    {
        this.addNavigate(url, this.getStandardWait());
    }

    @Override
    public void addType(String xpath, String value)
    {
        this.addType(xpath, value, this.getStandardWait());
    }

    @Override
    public void addScreenshot(String pathAndName)
    {
        this.addScreenshot(pathAndName, this.getStandardWait());
    }

    @Override
    public void addWaitForElement(String target)
    {
        this.addCommand(new DriverWaitForElement(target, this.getStandardWait()));
    }
    
    @Override
    public void addSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure)
    {
        this.addSetCookie(cookieName, cookieValue, domain, path, expiresOn, isSecure, this.getStandardWait());
    }

    @Override
    public void addDeleteCookie(String name)
    {
        this.addDeleteCookie(name, this.getStandardWait());
    }

    @Override
    public void addDeleteAllCookies()
    {
        this.addDeleteAllCookies(this.getStandardWait());
    }

    @Override
    public void addSelectDropdown(String target, String value)
    {
        this.addSelectDropdown(target, value, this.getStandardWait());
    }

    @Override
    public void addSelectFrameByClassName(String target)
    {
        this.addSelectFrameByClassName(target, this.getStandardWait());
    }
}
