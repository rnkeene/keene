package com.redhat.marketing.automation.command.browser.concrete;

public interface WaitForElement<B> extends Wait<B>
{
    public String getTarget();

    public void setTarget(String target);
}
