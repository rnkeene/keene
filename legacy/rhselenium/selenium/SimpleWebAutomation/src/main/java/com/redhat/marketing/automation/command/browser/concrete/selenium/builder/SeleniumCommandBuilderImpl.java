package com.redhat.marketing.automation.command.browser.concrete.selenium.builder;

public class SeleniumCommandBuilderImpl extends SeleniumNoWaitCommandBuilder implements SeleniumCommandBuilder
{

    @Override
    public void build()
    {
        //nothing to build.
    }
}
