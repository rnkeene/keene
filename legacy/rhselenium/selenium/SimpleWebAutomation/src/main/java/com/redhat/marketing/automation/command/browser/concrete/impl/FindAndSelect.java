package com.redhat.marketing.automation.command.browser.concrete.impl;

public abstract class FindAndSelect<B,S,W> extends FindImpl<B,W>{

    private String selectLabel;
    private S selectElement;

    public FindAndSelect(String target, String value) {
        super(target);
        this.selectLabel = value;
    }

    public S getSelectElement()
    {
        return selectElement;
    }

    public void setSelectElement(S selectElement)
    {
        this.selectElement = selectElement;
    }

    public String getSelectLabel()
    {
        return selectLabel;
    }

    public void setSelectLabel(String selectLabel)
    {
        this.selectLabel = selectLabel;
    }

}
