package com.redhat.marketing.automation.command;

/**
 *
 * The <b>Whole Point</b> of Inversion of Control (IoC) is to help decouple objects from one another.
 *
 * <b>Lower order Objects should not reference Higher order ones.</b>
 *
 * That said; this Inverted Control Mechanism is for Effeciency and Tracking within this Particular Framework, typically because there is an indeterminite set of commands that result in an otherwise untracked set determinite states (without adding 'too much' responsibility to the previously indeterminite commands).  This is why the InvertedMediatorController will be required to collect the path backwards from a command in order to recover information on previous Stable States.
 *
 * @author nkeene
 * @param <P> the parent Decorator for a particular Injected Object.
 * @param <T> the target of the Decoration
 */
public interface InvertedMediatorController<P, T> {

    public P getMediator();

    /**
     *
     * Capture the Target of the Inversion
     *
     * @return
     */
    public T getInjection();


}
