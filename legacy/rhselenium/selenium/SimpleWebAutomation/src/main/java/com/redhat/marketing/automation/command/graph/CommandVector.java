package com.redhat.marketing.automation.command.graph;

import com.redhat.marketing.automation.command.InjectedCommand;

/**
 *
 * "A Command!".... (is held here for execution);
 *
 * This is the wrapper for the CommandVector; a Command is naturally an unstable action between Stable States.
 *
 * The Graph is a natural fit for the concept of a command as an "Edge" (or transition).
 *
 * The Edge holds the context-specific data that exists when a "Command" exists between any two Defined Stable States.
 *
 * A CommandVector 'actually' determines the association, rules, meta-data and 'thread-safety' considerations associated with
 *
 * The CommandVector represents the execution of a command that causes the transition between stable states; e.g. the Edge in a Graph of Vertexes of Stable States.
 * 
 * @author nkeene
 */
public interface CommandVector extends InjectedCommand
{
}
