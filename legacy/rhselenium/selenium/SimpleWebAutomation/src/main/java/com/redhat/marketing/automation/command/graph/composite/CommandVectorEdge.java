package com.redhat.marketing.automation.command.graph.composite;

import com.redhat.marketing.automation.command.IndeterminateCommand;
import com.redhat.marketing.automation.command.IndeterminateCommand;
import org.jgrapht.graph.DefaultEdge;

/**
 * 
 * The StableState roughly saves the History of the last command executed.
 * 
 * The StableState also assumes a "stable" state after one Command executes, but before the next can execute.
 *
 * @author nkeene
 */
public class CommandVectorEdge extends DefaultEdge
{

    private IndeterminateCommand lastCommand;

    public CommandVectorEdge(IndeterminateCommand lastCommand)
    {
        this.lastCommand = lastCommand;
    }

    public IndeterminateCommand getLastCommand()
    {
        return lastCommand;
    }

    public void setLastCommand(IndeterminateCommand lastCommand)
    {
        this.lastCommand = lastCommand;
    }
}
