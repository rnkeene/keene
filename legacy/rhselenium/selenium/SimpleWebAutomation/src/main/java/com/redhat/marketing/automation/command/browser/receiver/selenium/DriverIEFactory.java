package com.redhat.marketing.automation.command.browser.receiver.selenium;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverIEFactory extends DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new InternetExplorerDriver();
    }

}
