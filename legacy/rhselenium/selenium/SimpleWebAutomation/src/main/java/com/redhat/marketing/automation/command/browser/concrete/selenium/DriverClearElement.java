package com.redhat.marketing.automation.command.browser.concrete.selenium;

public class DriverClearElement extends DriverFind
{

    public DriverClearElement(String xpath)
    {
        super(xpath);
    }

    @Override
    public void execute()
    {
        super.execute();
        this.getFindResults().clear();
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append(super.toString());
        toString.append(".clear()");
        return toString.toString();
    }
}
