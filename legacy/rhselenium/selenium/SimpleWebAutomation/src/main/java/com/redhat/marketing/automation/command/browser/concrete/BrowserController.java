package com.redhat.marketing.automation.command.browser.concrete;

public interface BrowserController<B>
{
    public void startBrowser();

    public void stopBrowser();
}
