package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.Click;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverClick extends DriverFind implements Click<RemoteWebDriver,WebElement> {

    public DriverClick(String xpath) {
        super(xpath);
    }

    @Override
    public void execute() {
        super.execute();
        this.getFindResults().click();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(super.toString());
        toString.append(".click()");
        return toString.toString();
    }
}
