package com.redhat.marketing.automation.command.browser.concrete;

import com.redhat.marketing.automation.command.IndeterminateCommand;

/**
 * The BrowserCommand is used as a partial abstraction that combines the building out the the Command Pattern via a generic "Browser" object (generic - &lt;B&gt; ), and also the <Component> of the Composite Pattern to be built up.<br /><br />
 *
 * [[<br /><br />
 *
 * <Design Principle - Open Close Principle><br />
 * <Motivation><br />
 * The Open Close Principle states that the design and writing of the code should be done in a way that new functionality should be added with minimum changes in the existing code.<br />
 * The design should be done in a way to allow the adding of new functionality as new classes, keeping as much as possible existing code unchanged.<br /><br />
 *
 * <Conclusion><br />
 * Like every principle OCP is only a principle. Making a flexible design involves additional time and effort spent for it and it introduce new level of abstraction increasing the complexity of the code. So this principle should be applied in those area which are most likely to be changed.<br />
 * There are many design patterns that help us to extend code without changing it. For instance the Decorator pattern help us to follow Open Close principle. Also the Factory Method or the Observer pattern might be used to design an application easy to change with minimum changes in the existing code.<br /><br />
 *
 * ]]<br />
 * -<a href="http://www.oodesign.com">OO Design</a><br /><br />
 *
 * @param <B> The Browser; which knows nothing of the command... the command executes on the browser; per generics & open-close principle.
 */
public interface BrowserCommand<B> extends IndeterminateCommand
{

    public B getBrowser();

    public void setBrowser(B browser);
}
