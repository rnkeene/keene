package com.redhat.marketing.automation.command.browser.concrete.selenium.builder;

import com.redhat.marketing.automation.command.browser.concrete.impl.builder.BrowserCommandBuilderImpl;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverClick;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverClosePopUp;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverDeleteAllCookies;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverDeleteCookie;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverFindAndSelect;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverFind;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverNavigate;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverOpen;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverScreenshot;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverSelectFrame;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverSetCookie;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverType;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverWait;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverWaitForElement;
import java.util.Date;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class SeleniumWaitingCommandBuilder extends BrowserCommandBuilderImpl<RemoteWebDriver> implements SeleniumCommandBuilder
{

    private long standardWait;

    public SeleniumWaitingCommandBuilder()
    {
        this.standardWait = 0;
    }

    public SeleniumWaitingCommandBuilder(long standardWait)
    {
        this.standardWait = standardWait;
    }

    public long getStandardWait()
    {
        return standardWait;
    }

    public void setStandardWait(long standardWait)
    {
        this.standardWait = standardWait;
    }

    /**
     * 
     * This method creates the actual Selenium Command that will cause the Browser to wait.
     *
     * @param time
     */
    @Override
    public void addWait(long time)
    {
        if (time > 0)
        {
            this.addCommand(new DriverWait(time));
        }
    }

    @Override
    public void addWaitForElement(String target, long time)
    {
        this.addCommand(new DriverWaitForElement(target, time));
    }

    @Override
    public void addClosePopUp(long andWait)
    {
        this.addCommand(new DriverClosePopUp());
        this.addWait(andWait);
    }

    @Override
    public void addClick(String xpath, long andWait)
    {
        this.addCommand(new DriverClick(xpath));
        this.addWait(andWait);
    }

    @Override
    public void addFind(String xpath, long andWait)
    {
        this.addCommand(new DriverFind(xpath));
        this.addWait(andWait);
    }

    @Override
    public void addOpen(String url, long andWait)
    {
        this.addCommand(new DriverOpen(url));
        this.addWait(andWait);
    }

    @Override
    public void addNavigate(String url, long andWait)
    {
        this.addCommand(new DriverNavigate(url));
        this.addWait(andWait);
    }

    @Override
    public void addType(String xpath, String value, long andWait)
    {
        this.addCommand(new DriverType(xpath, value));
        this.addWait(andWait);
    }

    @Override
    public void addScreenshot(String pathAndName, long andWait)
    {
        this.addCommand(new DriverScreenshot(pathAndName));
        this.addWait(andWait);
    }

    @Override
    public void addSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure, long andWait)
    {
        this.addCommand(new DriverSetCookie(cookieName, cookieValue, domain, path, expiresOn, isSecure));
        this.addWait(andWait);
    }

    @Override
    public void addDeleteCookie(String name, long andWait)
    {
        this.addCommand(new DriverDeleteCookie(name));
        this.addWait(andWait);
    }

    @Override
    public void addDeleteAllCookies(long andWait)
    {
        this.addCommand(new DriverDeleteAllCookies());
        this.addWait(andWait);
    }

    @Override
    public void addSelectDropdown(String target, String value, long andWait)
    {
        this.addCommand(new DriverFindAndSelect(target, value));
        this.addWait(andWait);
    }

    @Override
    public void addSelectFrameByClassName(String target, long andWait)
    {
        this.addCommand(new DriverSelectFrame(target));
        this.addWait(andWait);
    }
}
