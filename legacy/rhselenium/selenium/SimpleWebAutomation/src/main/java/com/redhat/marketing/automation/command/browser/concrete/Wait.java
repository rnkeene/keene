package com.redhat.marketing.automation.command.browser.concrete;

public interface Wait<B> extends BrowserCommand<B>{

    public long getWaitTime();

    public void setWaitTime(long waitTime);
}