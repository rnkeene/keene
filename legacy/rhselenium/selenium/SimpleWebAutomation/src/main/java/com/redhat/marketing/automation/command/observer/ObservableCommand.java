package com.redhat.marketing.automation.command.observer;

import com.redhat.marketing.automation.command.UnstableState;

/**
 * Sometimes it's important that Objects are Aware when Certain Commands are executed; such that they can run any processes that have a relationship to a particular Command execution within the System (in general for reporting; or handlingly particular conditions that need management within the System.
 * 
 * @author nkeene
 */
public interface ObservableCommand<O, S> extends UnstableState<S>
{

    public void addCommandObserver(O observer);

    public void removeCommandObserver(O observer);

    public void notifyCommandObservers();
}
