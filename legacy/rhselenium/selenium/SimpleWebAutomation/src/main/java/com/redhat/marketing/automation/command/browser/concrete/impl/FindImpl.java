package com.redhat.marketing.automation.command.browser.concrete.impl;

import com.redhat.marketing.automation.command.browser.concrete.Find;

public abstract class FindImpl<B,R> extends BrowserCommandImpl<B> implements Find<B,R>{

    private String target;
    private R results;
    
    public FindImpl(String target) {
        this.target = target;
    }

    @Override
    public String getTarget() {
        return target;
    }

    @Override
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public R getFindResults() {
        return this.results;
    }

    @Override
    public void setFindResults(R results) {
        this.results = results;
    }

    @Override
    public String toString() {
        if(results!=null){
            return this.results.toString();
        }
        return this.target;
    }
}
