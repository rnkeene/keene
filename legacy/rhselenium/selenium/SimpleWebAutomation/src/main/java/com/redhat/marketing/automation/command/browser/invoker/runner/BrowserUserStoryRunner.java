package com.redhat.marketing.automation.command.browser.invoker.runner;

import com.redhat.marketing.automation.command.browser.concrete.BrowserCommandBuilder;
import com.redhat.marketing.automation.command.browser.invoker.BrowserUserStory;
import com.redhat.marketing.automation.command.invoker.AbstractUserStoryRunner;
import org.apache.log4j.Logger;

public abstract class BrowserUserStoryRunner<U extends BrowserUserStory, B extends BrowserCommandBuilder> extends AbstractUserStoryRunner<U, B>
{

//    private static Logger logger = Logger.getLogger(BrowserUserStoryRunner.class);

    @Override
    public void runUserStory()
    {
//        if(logger.isDebugEnabled()) logger.debug("Running the User Story -- starting up the browser, etc!");
        if (this.isReady())
        {
            this.getUserStory().startBrowser();
            super.runUserStory();
            this.getUserStory().stopBrowser();
//            if(logger.isDebugEnabled()) logger.debug("Done; Closing Browser Session!");
        }
    }
}
