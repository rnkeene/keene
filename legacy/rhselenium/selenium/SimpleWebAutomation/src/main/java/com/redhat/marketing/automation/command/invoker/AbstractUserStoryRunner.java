package com.redhat.marketing.automation.command.invoker;

import com.redhat.marketing.automation.command.invoker.runner.UserStoryRunner;
import com.redhat.marketing.automation.command.builder.CommandBuilder;

public class AbstractUserStoryRunner<U extends UserStory, B extends CommandBuilder> implements UserStoryRunner<U, B>
{

//    private static Logger logger = Logger.getLogger(AbstractUserStoryRunner.class);
    private String configurationId;
    private U userStory;
    private B commandBuilder;

    @Override
    public void runUserStory()
    {
//        if(logger.isDebugEnabled()) logger.debug("Running the User Story -- referencing the command builder, building and executing!");
        if (this.isReady())
        {
            this.getUserStory().setCommandBuilder(this.getCommandBuilder());
            this.getUserStory().initialize();
            this.getUserStory().build();
            this.getUserStory().execute();
//            if(logger.isDebugEnabled()) logger.debug("Done executing!");
        }
    }

    @Override
    public void runUserStory(U story)
    {
        this.setUserStory(story);
        this.runUserStory();
    }

    @Override
    public void runUserStory(U story, B builder)
    {
        this.setUserStory(story);
        this.setCommandBuilder(builder);
        this.runUserStory();
    }

    @Override
    public boolean isReady()
    {
        if (this.getUserStory() == null)
        {
            assert false : "The User Story is not Initialized.";
        }
        if (this.getCommandBuilder() == null)
        {
            assert false : "The Command Builder is not Initialized.";
        }
        return true;
    }

    public U getUserStory()
    {
        return userStory;
    }

    public void setUserStory(U userStory)
    {
        this.userStory = userStory;
    }

    public B getCommandBuilder()
    {
        return commandBuilder;
    }

    public void setCommandBuilder(B commandBuilder)
    {
        this.commandBuilder = commandBuilder;
    }

    public String getConfigurationId()
    {
        return configurationId;
    }

    public void setConfigurationId(String configurationId)
    {
        this.configurationId = configurationId;
    }
}
