package com.redhat.marketing.automation.command.invoker;

import com.redhat.marketing.automation.command.IndeterminateCommand;
import com.redhat.marketing.automation.command.builder.CommandBuilder;

public interface UserStory<B> extends IndeterminateCommand, CommandBuilder{
    public B getCommandBuilder();
    public void setCommandBuilder(B commandBuilder);
}
