package com.redhat.marketing.automation.command.browser.concrete.impl;

import com.redhat.marketing.automation.command.IndeterminateCommand;
import com.redhat.marketing.automation.command.graph.composite.CompositeCommandControllerGraph;
import com.redhat.marketing.automation.command.browser.concrete.BrowserCommand;

public class BrowserCommandImpl<B> extends CompositeCommandControllerGraph implements BrowserCommand<B>
{

    private B browser;

    @Override
    public void execute()
    {
        for (IndeterminateCommand command : getCommands())
        {
            ((BrowserCommand) command).setBrowser(this.getBrowser());
            command.execute();
        }
    }

    @Override
    public B getBrowser()
    {
        return this.browser;
    }

    @Override
    public void setBrowser(B browser)
    {
        this.browser = browser;
    }
}
