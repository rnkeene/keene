package com.redhat.marketing.automation.command.browser.receiver.selenium;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFirefoxFactory extends DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new FirefoxDriver();
    }

}
