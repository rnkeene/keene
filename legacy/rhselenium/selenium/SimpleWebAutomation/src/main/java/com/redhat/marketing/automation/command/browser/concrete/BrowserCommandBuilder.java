package com.redhat.marketing.automation.command.browser.concrete;

import com.redhat.marketing.automation.command.builder.CommandBuilder;
import com.redhat.marketing.automation.command.graph.CompositeCommandNodeController;
import java.util.Date;

/**
 * The BrowserCommand is used as a partial abstraction that combines the building out the the Command Pattern via a generic "Browser" object <B>, and also the <Component> of the Composite Pattern to be built up.<br /><br />
 *
 * [[<br /><br />
 *
 * <Design Principle - Open Close Principle><br />
 * <Motivation><br />
 * The Open Close Principle states that the design and writing of the code should be done in a way that new functionality should be added with minimum changes in the existing code.<br />
 * The design should be done in a way to allow the adding of new functionality as new classes, keeping as much as possible existing code unchanged.<br />
 *
 * <Conclusion><br />
 * Like every principle OCP is only a principle. Making a flexible design involves additional time and effort spent for it and it introduce new level of abstraction increasing the complexity of the code. So this principle should be applied in those area which are most likely to be changed.<br />
 * There are many design patterns that help us to extend code without changing it. For instance the Decorator pattern help us to follow Open Close principle. Also the Factory Method or the Observer pattern might be used to design an application easy to change with minimum changes in the existing code.<br /><br /><
 *
 * ]]<br /><br />
 * -<a href="http://www.oodesign.com/open-close-principle.html">OO Design - The Open/Close Principle</a><br /><br />
 *
 * e.g. a Robust Base Set of Capabilities means there's many facets of the Framework that can be used in a multitied of ways; depending on the imagination of the developer.<br /><br />
 *
 * @param <B> The Browser; which knows nothing of the command... the command executes on the browser; per generics & open-close principle.
 */
public interface BrowserCommandBuilder<B> extends BrowserCommand<B>, CommandBuilder, CompositeCommandNodeController
{

    public void addClick(String xpath);

    public void addClick(String xpath, long andWait);

    public void addClosePopUp();

    public void addClosePopUp(long andWait);

    public void addFind(String xpath);

    public void addFind(String xpath, long andWait);

    public void addOpen(String url);

    public void addOpen(String url, long andWait);

    public void addSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure);

    public void addSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure, long wait);

    public void addDeleteCookie(String cookieName);

    public void addDeleteCookie(String cookieName, long andWait);

    public void addNavigate(String url);

    public void addNavigate(String url, long andWait);

    public void addType(String xpath, String value);

    public void addType(String xpath, String value, long andWait);

    public void addWait(long time);

    public void addWaitForElement(String target);

    public void addWaitForElement(String target, long time);

    public void addScreenshot(String filePathAndName);

    public void addScreenshot(String filePathAndName, long andWait);

    public void addDeleteAllCookies();

    public void addDeleteAllCookies(long andWait);

    public void addSelectFrameByClassName(String target);

    public void addSelectFrameByClassName(String target, long andWait);

    public void addSelectDropdown(String target, String value);

    public void addSelectDropdown(String target, String value, long andWait);
}
