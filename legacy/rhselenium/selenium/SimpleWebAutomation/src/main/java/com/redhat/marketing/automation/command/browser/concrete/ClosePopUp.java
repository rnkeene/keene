package com.redhat.marketing.automation.command.browser.concrete;

public interface ClosePopUp<B> extends BrowserCommand<B>
{
}
