package com.redhat.marketing.automation.command.browser.concrete;

public interface Navigate<B> extends BrowserCommand<B> {

    public String getUrl();

    public void setUrl(String url);
}
