package com.redhat.marketing.automation.command;

import com.redhat.marketing.automation.command.NetworkCommand;

/**
 *
 * **UTILITY/CHECK INTERFACE**
 * -- not actually required; and will be removed from production builds --
 *
 * Use CommandInjectedDepenendcy to debug the system; then, no longer extend this Interface from the IndeterminateCommand Interface!
 * 
 * Get data about dependent objects and system related reports.
 *
 * This class will help this system manage the various requirements on the execution of a Command.
 *
 * The only requirement, a function required to operate in isolation and manage and its own transition between stable states.
 *
 * It processing should gracefully execute within this environment; and otherwise manage its own exceptional cases.
 */
/**
 *
 * @author nkeene
 * @param <I> the Object that controls, wraps, proxies, decorates or otherwise provides responsiblility for managing this Object.
 */
public interface InjectedCommand<I, C extends NetworkCommand> extends NetworkCommand
{

    /**
     * A non-native Command, an interrupte command, a decorated command, etc.
     * @return
     */
    public void setCommand(C command);
}
