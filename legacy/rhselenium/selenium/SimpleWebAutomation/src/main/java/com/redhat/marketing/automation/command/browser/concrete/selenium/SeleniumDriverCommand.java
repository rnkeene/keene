package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.BrowserCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

public interface SeleniumDriverCommand extends BrowserCommand<RemoteWebDriver>
{
}
