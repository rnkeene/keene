package com.redhat.marketing.automation.command.browser.concrete.selenium;

public class DriverWaitForNewPage extends DriverWait {

    private int maxTries;
    private String currentPage;

    public DriverWaitForNewPage(String currentPage,long interval) {
        super(interval);
        this.currentPage = currentPage;
    }

    @Override
    public void execute() {
        int tries = 0;
        while(this.getBrowser().getCurrentUrl().equals(currentPage) && tries < maxTries){
            super.execute();
            tries++;
        }
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.waitForNewPage(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
