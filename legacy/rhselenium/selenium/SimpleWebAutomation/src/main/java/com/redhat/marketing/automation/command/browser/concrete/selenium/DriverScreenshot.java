package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.Screenshot;
import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.decorator.SeleniumScreenshotDecorator;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverScreenshot extends DriverCommand implements Screenshot<RemoteWebDriver>
{

    private String tempScreenshotPath = "/home/nkeene/Desktop/GatedFormBrowserComparison/";

    private static Logger logger = Logger.getLogger(SeleniumScreenshotDecorator.class);
    private String pathAndName;

    public DriverScreenshot(String pathAndName)
    {
        this.pathAndName = pathAndName;
    }

    @Override
    public void execute()
    {
        try
        {
            if (this.getBrowser() != null && this.getBrowser().getSessionId() != null)
            {
                WebDriver augmentedDriver = new Augmenter().augment(this.getBrowser());
                File file = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
                String showMe = this.tempScreenshotPath + this.pathAndName + "/" + this.getBrowser().getCapabilities().getBrowserName() + "/snapshot.jpg";
                if(logger.isDebugEnabled()) logger.debug("Saving screenshot: " + showMe);
                File screenshot = new File(showMe);
                FileUtils.copyFile(file, screenshot);
            }
        } catch (IOException e)
        {
        }
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.TakesScreenshot(");
        toString.append(super.toString());
        toString.append(")");
        return toString.toString();
    }
}
