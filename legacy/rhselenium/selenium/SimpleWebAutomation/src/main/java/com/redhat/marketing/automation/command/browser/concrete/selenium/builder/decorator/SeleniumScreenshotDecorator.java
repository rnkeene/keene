package com.redhat.marketing.automation.command.browser.concrete.selenium.builder.decorator;

import com.redhat.marketing.automation.command.IndeterminateCommand;
import com.redhat.marketing.automation.command.browser.concrete.BrowserCommand;
import com.redhat.marketing.automation.command.browser.concrete.impl.builder.decorator.BrowserCommandBuilderDecorator;
import com.redhat.marketing.automation.command.browser.concrete.selenium.DriverScreenshot;
import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.SeleniumCommandBuilder;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * 
 * This Decorator Builder will add a Screenshot before and/or after each Command is Executed within the Browser.
 *
 * @author nkeene
 */
public class SeleniumScreenshotDecorator extends BrowserCommandBuilderDecorator<RemoteWebDriver, SeleniumCommandBuilder> implements SeleniumCommandBuilder
{

//    private static Logger logger = Logger.getLogger(SeleniumScreenshotDecorator.class);
    private boolean before;
    private boolean after;
    private String screenshotFilePath = "target/screenshot/";
    private int screenshots;
    private String category;

    public SeleniumScreenshotDecorator(String category, SeleniumCommandBuilder decorated, boolean before, boolean after)
    {
        super(decorated);
        this.before = before;
        this.after = after;
        this.screenshots = 0;
        this.category = category;
    }

    @Override
    public void build()
    {
        //Nothing to Build.
    }

    @Override
    public void execute()
    {
        for (IndeterminateCommand command : this.getCommands())
        {
            screenshots++;
            this.takeBefore();
            ((BrowserCommand) command).setBrowser(this.getBrowser());
//            if(logger.isDebugEnabled())logger.debug("Running Command: " + command.toString());
            command.execute();
            this.takeAfter();
        }
    }

    private void takeBefore()
    {
        if (this.before)
        {
            this.takeScreenshot("A");
        }
    }

    private void takeAfter()
    {
        if (this.after)
        {
            this.takeScreenshot("B");
        }
    }

    private void takeScreenshot(String timing)
    {
        DriverScreenshot screenshot = new DriverScreenshot(this.category + "/" + screenshots+"/"+timing+"/");
        screenshot.setBrowser(this.getBrowser());
//        if(logger.isDebugEnabled())logger.debug("Running Command: " + screenshot.toString());
        screenshot.execute();
    }

    public boolean isAfter()
    {
        return after;
    }

    public void setAfter(boolean after)
    {
        this.after = after;
    }

    public boolean isBefore()
    {
        return before;
    }

    public void setBefore(boolean before)
    {
        this.before = before;
    }

    public String getScreenshotFilePath()
    {
        return screenshotFilePath;
    }

    public void setScreenshotFilePath(String screenshotFilePath)
    {
        this.screenshotFilePath = screenshotFilePath;
    }
}
