package com.redhat.marketing.automation.command.browser.concrete;

public interface Type<B,R> extends Find<B,R>{
    
    public String getValue();

    public void setValue(String value);

}
