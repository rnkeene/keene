package com.redhat.marketing.automation.command.receiver;

import java.net.MalformedURLException;
import java.net.URL;

public class RemoteReceiverServer{

    private String scheme;
    private String domain;
    private int port;
    private String registerContext;
    private String fullURL;

    public RemoteReceiverServer() {
    }
    
    public RemoteReceiverServer(String protocol, String url, String registerContext, int port) {
        this.scheme = protocol;
        this.domain = url;
        this.port = port;
        this.registerContext = registerContext;
        this.fullURL = this.scheme + "://"+ this.domain + ":" + this.port + this.registerContext;
    }

    public String getProtocol() {
        return scheme;
    }

    public void setProtocol(String protocol) {
        this.scheme = protocol;
    }
    
    public String getDomain() {
        return domain;
    }

    public void setDomain(String url) {
        this.domain = url;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getRegisterContext() {
        return registerContext;
    }

    public void setRegisterContext(String registerContext) {
        this.registerContext = registerContext;
    }

    public URL getRegisterURL() {
        try {
            return new URL(fullURL);
        } catch (MalformedURLException e) {
            assert false : "Malformed URL Exception - " + e.toString() + " - Command - " + this.toString();
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("New Selenium Hub:\n\t\tURL - ");
        builder.append(this.domain);
        builder.append("\n\t\tPort - ");
        builder.append(this.port);
        builder.append("\n\t\tRegister Context - ");
        builder.append(this.registerContext);
        builder.append("\n\t\tFull Registration URL - ");
        builder.append(this.fullURL);
        builder.append("\n");
        return builder.toString();
    }

    public enum ReceiverStandardEnvironment{
        LOCAL("http","localhost",4444,"/wd/hub");

        private String protocol;
        private String domain;
        private int port;
        private String registrationContext;

        ReceiverStandardEnvironment(String protocol, String domain, int port, String registrationContext)
        {
            this.protocol = protocol;
            this.domain = domain;
            this.port = port;
            this.registrationContext = registrationContext;
        }

        public String getDomain()
        {
            return domain;
        }

        public int getPort()
        {
            return port;
        }

        public String getProtocol()
        {
            return protocol;
        }

        public String getRegistrationContext()
        {
            return registrationContext;
        }
        
    }
}
