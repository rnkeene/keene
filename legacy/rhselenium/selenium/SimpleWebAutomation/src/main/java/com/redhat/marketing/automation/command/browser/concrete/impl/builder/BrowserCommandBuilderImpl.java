package com.redhat.marketing.automation.command.browser.concrete.impl.builder;

import com.redhat.marketing.automation.command.browser.concrete.BrowserCommandBuilder;
import com.redhat.marketing.automation.command.browser.concrete.impl.BrowserCommandImpl;

/**
 * The CommandBuilder is an Object designed to dynamically create and add test Command Implementations.<br /><br />
 *
 * This is a file that is roughly counter to the Design Pattern "request" to leave the concrete instantiation to the Invoker; however, this is operates as a proxy specifically so the Invoker can more dynamically create Commands.
 *
 * The CommandBuilder Object also extends the CompositeCommand so it is possible to add to this particular implementation indefinitily.
 *
 * @author nkeene
 */
public abstract class BrowserCommandBuilderImpl<B> extends BrowserCommandImpl<B> implements BrowserCommandBuilder<B>
{
}
