package com.redhat.marketing.automation.command.builder;

import com.redhat.marketing.automation.command.IndeterminateCommand;

public interface CommandBuilder extends IndeterminateCommand
{

    public void build();
}
