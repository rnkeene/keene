package com.redhat.marketing.automation.command.browser.invoker.selenium;

import com.redhat.marketing.automation.command.browser.concrete.BrowserCommandBuilder;
import com.redhat.marketing.automation.command.browser.concrete.impl.BrowserCommandImpl;
import com.redhat.marketing.automation.command.browser.invoker.BrowserUserStory;
import com.redhat.marketing.automation.command.browser.receiver.selenium.RemoteDriverFactory;
import com.redhat.marketing.automation.command.receiver.RemoteReceiverServer;
import java.util.Map;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * The SeleniumUserStory should be used any time there is a need to create Selenium based User Stories.<br /><br />
 *
 * This Object provides a logical abstraction and encapsulation of various Selenium capabilities.  There are a lot of potential and common events that can cause problems with a particular execution of a Selenium Test.<br /><br />
 *
 * By building out this implementation we can largely avoid the problems typically associated with using a Selenium Based solution for Web Testing Automation.
 *
 * @author nkeene
 */
public abstract class SeleniumUserStory extends BrowserCommandImpl<RemoteWebDriver> implements BrowserUserStory<RemoteWebDriver>
{

    private BrowserCommandBuilder<RemoteWebDriver> commandBuilder;
    private Map configurationMap;
    private Object browserId;
    private Object receiverProtocol;
    private Object receiverDomain;
    private Object receiverContext;
    private Object receiverPort;
    private RemoteReceiverServer receiverServer;
    private String sessionId;

    @Override
    public void initialize(Map configurationMap)
    {
        super.initialize();
        this.configurationMap = configurationMap;
    }

    @Override
    public BrowserCommandBuilder<RemoteWebDriver> getCommandBuilder()
    {
        return commandBuilder;
    }

    @Override
    public void setCommandBuilder(BrowserCommandBuilder<RemoteWebDriver> commandBuilder)
    {
        this.commandBuilder = commandBuilder;
    }

    @Override
    public void startBrowser()
    {
        if (this.configurationMap != null)
        {
            this.browserId = configurationMap.get(SeleniumUserStory.ConfigurationKeys.BROWSER_ID.getKey());
            if (browserId != null)
            {
                this.receiverProtocol = configurationMap.get(SeleniumUserStory.ConfigurationKeys.RECEIVER_PROTOCOL.getKey());
                this.receiverDomain = configurationMap.get(SeleniumUserStory.ConfigurationKeys.RECEIVER_DOMAIN.getKey());
                this.receiverContext = configurationMap.get(SeleniumUserStory.ConfigurationKeys.RECEIVER_REGISTER_CONTEXT.getKey());
                this.receiverPort = configurationMap.get(SeleniumUserStory.ConfigurationKeys.RECEIVER_PORT.getKey());
                if (this.receiverProtocol != null && this.receiverDomain != null && this.receiverPort != null && this.receiverProtocol != null)
                {
                    int port = Integer.parseInt((String) this.receiverPort);
                    this.receiverServer = new RemoteReceiverServer((String) this.receiverProtocol, (String) this.receiverDomain, (String) this.receiverContext, port);
                    this.setBrowser(RemoteDriverFactory.getRemoteDriver((String) this.browserId, this.receiverServer));
                    this.sessionId = this.getBrowser().getSessionId().toString();
                }
            }
        }
    }

    @Override
    public void stopBrowser()
    {
        this.getBrowser().close();
        this.getBrowser().quit();
        this.cleanup();
    }

    public Map getConfigurationMap()
    {
        return configurationMap;
    }

    public String getBrowserId()
    {
        return (String) browserId;
    }

    public String getReceiverContext()
    {
        return (String) receiverContext;
    }

    public String getReceiverDomain()
    {
        return (String) receiverDomain;
    }

    public String getReceiverPort()
    {
        return (String) receiverPort;
    }

    public String getReceiverProtocol()
    {
        return (String) receiverProtocol;
    }

    public RemoteReceiverServer getReceiverServer()
    {
        return receiverServer;
    }

    public String getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(String sessionId)
    {
        this.sessionId = sessionId;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("SeleniumUserStory{\n");
        toString.append("ACTIVE SESSION ID: ");
        toString.append(this.sessionId);
        toString.append("\n");
        toString.append("Configuration Map{\n\t");
        if (this.configurationMap != null)
        {
            toString.append(this.configurationMap.toString());
        }
        toString.append("\n}\nBrowser Id:  ");
        toString.append(this.browserId);
        toString.append("\nReceiver Protocol:  ");
        toString.append(this.receiverProtocol);
        toString.append("\nReceiver Domain:  ");
        toString.append(this.receiverDomain);
        toString.append("\nReceiver Port:  ");
        toString.append(this.receiverPort);
        toString.append("\nReceiver Registration Context:  ");
        toString.append(this.receiverContext);
        toString.append("\nRemote Receiver Server{\n\t");
        toString.append(this.receiverServer);
        toString.append("\n}\n");
        toString.append("\nRemote Web Driver{\n\t");
        toString.append(this.getBrowser());
        toString.append("\n}\n");
        toString.append("\n}");
        return toString.toString();
    }

    public enum ConfigurationKeys
    {

        BROWSER_ID("BROWSER_ID"),
        RECEIVER_PROTOCOL("RECEIVER_PROTOCOL"),
        RECEIVER_DOMAIN("RECEIVER_DOMAIN"),
        RECEIVER_PORT("RECEIVER_PORT"),
        RECEIVER_REGISTER_CONTEXT("RECEIVER_REGISTER_CONTEXT"),
        CLEAR_COOKIES("CLEAR_COOKIES");
        private final String key;

        ConfigurationKeys(String key)
        {
            this.key = key;
        }

        public String getKey()
        {
            return key;
        }
    }
}
