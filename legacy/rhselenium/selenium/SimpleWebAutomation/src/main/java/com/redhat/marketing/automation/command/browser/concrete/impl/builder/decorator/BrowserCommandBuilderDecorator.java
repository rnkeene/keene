package com.redhat.marketing.automation.command.browser.concrete.impl.builder.decorator;

import com.redhat.marketing.automation.command.IndeterminateCommand;
import com.redhat.marketing.automation.command.browser.concrete.BrowserCommandBuilder;
import java.util.Date;
import java.util.List;

public class BrowserCommandBuilderDecorator<B, C extends BrowserCommandBuilder<B>> implements BrowserCommandBuilder<B>
{

    private C decorated;

    public BrowserCommandBuilderDecorator(C decorated)
    {
        this.decorated = decorated;
    }

    @Override
    public void addCommand(IndeterminateCommand command)
    {
        this.decorated.addCommand(command);
    }

    @Override
    public List<IndeterminateCommand> getCommands()
    {
        return this.decorated.getCommands();
    }

    @Override
    public void addClick(String xpath)
    {
        decorated.addClick(xpath);
    }

    @Override
    public void addClick(String xpath, long andWait)
    {
        decorated.addClick(xpath, andWait);
    }

    @Override
    public void addClosePopUp()
    {
        decorated.addClosePopUp();
    }

    @Override
    public void addClosePopUp(long andWait)
    {
        decorated.addClosePopUp(andWait);
    }

    @Override
    public void addFind(String xpath)
    {
        decorated.addFind(xpath);
    }

    @Override
    public void addFind(String xpath, long andWait)
    {
        decorated.addClick(xpath, andWait);
    }

    @Override
    public void addOpen(String url)
    {
        decorated.addOpen(url);
    }

    @Override
    public void addOpen(String url, long andWait)
    {
        decorated.addOpen(url, andWait);
    }

    @Override
    public void addSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure)
    {
        decorated.addSetCookie(cookieName, cookieValue, domain, path, expiresOn, isSecure);
    }

    @Override
    public void addSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure, long wait)
    {
        decorated.addSetCookie(cookieName, cookieValue, domain, path, expiresOn, isSecure, wait);
    }

    @Override
    public void addDeleteCookie(String cookieName)
    {
        decorated.addDeleteCookie(cookieName);
    }

    @Override
    public void addDeleteCookie(String cookieName, long andWait)
    {
        decorated.addDeleteCookie(cookieName, andWait);
    }

    @Override
    public void addNavigate(String url)
    {
        decorated.addNavigate(url);
    }

    @Override
    public void addNavigate(String url, long andWait)
    {
        decorated.addNavigate(url, andWait);
    }

    @Override
    public void addType(String xpath, String value)
    {
        decorated.addType(xpath, value);
    }

    @Override
    public void addType(String xpath, String value, long andWait)
    {
        decorated.addType(xpath, value, andWait);
    }

    @Override
    public void addWait(long time)
    {
        decorated.addWait(time);
    }

    @Override
    public void addWaitForElement(String target)
    {
        decorated.addWaitForElement(target);
    }

    @Override
    public void addWaitForElement(String target, long time)
    {
        decorated.addWaitForElement(target, time);
    }

    @Override
    public void addScreenshot(String filePathAndName)
    {
        decorated.addScreenshot(filePathAndName);
    }

    @Override
    public void addScreenshot(String filePathAndName, long andWait)
    {
        decorated.addScreenshot(filePathAndName, andWait);
    }

    @Override
    public void addDeleteAllCookies()
    {
        decorated.addDeleteAllCookies();
    }

    @Override
    public void addDeleteAllCookies(long andWait)
    {
        decorated.addDeleteAllCookies(andWait);
    }

    @Override
    public void addSelectFrameByClassName(String target)
    {
        decorated.addSelectFrameByClassName(target);
    }

    @Override
    public void addSelectFrameByClassName(String target, long andWait)
    {
        decorated.addSelectFrameByClassName(target, andWait);
    }

    @Override
    public void addSelectDropdown(String target, String value)
    {
        decorated.addSelectDropdown(target, value);
    }

    @Override
    public void addSelectDropdown(String target, String value, long andWait)
    {
        decorated.addSelectDropdown(target, value, andWait);
    }

    @Override
    public B getBrowser()
    {
        return decorated.getBrowser();
    }

    @Override
    public void setBrowser(B browser)
    {
        decorated.setBrowser(browser);
    }

    @Override
    public void initialize()
    {
        decorated.initialize();
    }

    @Override
    public void execute()
    {
        decorated.execute();
    }

    @Override
    public void cleanup()
    {
        decorated.cleanup();
    }

    @Override
    public void build()
    {
        decorated.build();
    }
}
