package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.BrowserCommandImpl;
import java.util.Date;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverSetCookie extends BrowserCommandImpl<RemoteWebDriver> implements SeleniumDriverCommand
{

    private String cookieName;
    private String cookieValue;
    private String domain;
    private String path;
    private Date expiresOn;
    private boolean isSecure;

    public DriverSetCookie(String cookieName, String cookieValue, String domain, String path, Date expiresOn, boolean isSecure)
    {
        this.cookieName = cookieName;
        this.cookieValue = cookieValue;
        this.domain = domain;
        this.path = path;
        this.expiresOn = expiresOn;
        this.isSecure = isSecure;
    }

    @Override
    public void execute()
    {
        Cookie cookie = new Cookie(cookieName, cookieValue, domain, path, expiresOn, isSecure);
        this.getBrowser().manage().addCookie(cookie);
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.manage().addCookie(");
        toString.append(cookieName);
        toString.append(", ");
        toString.append(cookieValue);
        toString.append(", ");
        toString.append(domain);
        toString.append(", ");
        toString.append(path);
        toString.append(", ");
        toString.append(expiresOn);
        toString.append(", ");
        toString.append(isSecure);
        toString.append(")");
        return toString.toString();
    }
}
