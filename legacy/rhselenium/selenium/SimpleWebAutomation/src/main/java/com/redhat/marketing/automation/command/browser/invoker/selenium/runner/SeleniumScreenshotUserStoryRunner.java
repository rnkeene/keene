package com.redhat.marketing.automation.command.browser.invoker.selenium.runner;

import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.SeleniumCommandBuilder;
import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.SeleniumCommandBuilderImpl;
import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.decorator.SeleniumScreenshotDecorator;
import com.redhat.marketing.automation.command.browser.invoker.selenium.SeleniumUserStory;

public class SeleniumScreenshotUserStoryRunner<U extends SeleniumUserStory> extends SeleniumUserStoryRunner<U>
{

    private String category;

    public SeleniumScreenshotUserStoryRunner(String category)
    {
        this.category=category;
    }

    @Override
    public void runUserStory()
    {
        if (this.getCommandBuilder() == null)
        {
            SeleniumCommandBuilder seleniumCommandBuilder = new SeleniumCommandBuilderImpl();
            this.setCommandBuilder(new SeleniumScreenshotDecorator(this.category, seleniumCommandBuilder, true, true));
        }
        super.runUserStory();
    }

    @Override
    public void runUserStory(U story)
    {
        this.setUserStory(story);
        this.runUserStory();
    }
}
