package com.redhat.marketing.automation.command;

import com.redhat.marketing.automation.kernel.KernelState;

/**
 *
 * A Command should always return the System to a valid System State, as a definition any action/thread/event/function that destabilizes the System should likely be considered a Command in an UnstableState.
 *
 * This Object can be used to manage the functionality that protects the System and a Set of Commands from potentially putting the System into an Error State.
 * 
 * @author nkeene
 */
public interface UnstableState extends KernelState
{

    public void handle();
}
