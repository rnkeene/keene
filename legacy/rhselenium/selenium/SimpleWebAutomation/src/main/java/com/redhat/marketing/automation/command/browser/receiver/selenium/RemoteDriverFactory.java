package com.redhat.marketing.automation.command.browser.receiver.selenium;

import com.redhat.marketing.automation.command.receiver.RemoteReceiverServer;
import com.redhat.marketing.automation.command.receiver.RemoteReceiverFactory;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class RemoteDriverFactory extends RemoteReceiverFactory<RemoteWebDriver>
{

    private DesiredCapabilities capabilities;

    RemoteDriverFactory(RemoteReceiverServer hub, DesiredCapabilities capabilities)
    {
        super(hub);
        this.capabilities = capabilities;
    }

    public DesiredCapabilities getCapabilities()
    {
        return capabilities;
    }

    public void setCapabilities(DesiredCapabilities capabilities)
    {
        this.capabilities = capabilities;
    }

    @Override
    public RemoteWebDriver getInstance()
    {
        return new RemoteWebDriver(this.getReceiverServer().getRegisterURL(), this.capabilities);
    }

    public static RemoteWebDriver getRemoteDriver(String browser, RemoteReceiverServer seleniumServer)
    {
        RemoteDriverFactory factory = null;
        if (RemoteDriverFactory.BrowserId.FIREFOX.getBrowserId().equals(browser))
        {
            factory = new RemoteDriverFirefoxFactory(seleniumServer);
        } else
        {
            if (RemoteDriverFactory.BrowserId.INTERNET_EXPLORER.getBrowserId().equals(browser))
            {
                factory = new RemoteDriverIEFactory(seleniumServer);
            } else
            {
                if (RemoteDriverFactory.BrowserId.CHROME.getBrowserId().equals(browser))
                {
                    factory = new RemoteDriverChromeFactory(seleniumServer);
                }
            }
        }
        return factory.getInstance();
    }

    public enum BrowserId
    {
        FIREFOX("*firefox"),
        INTERNET_EXPLORER("*iexplore"),
        CHROME("*chrome");
        private String browserId;

        BrowserId(String browserId)
        {
            this.browserId = browserId;
        }

        public String getBrowserId()
        {
            return browserId;
        }
    }
}
