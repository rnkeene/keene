package com.redhat.marketing.automation.command.graph.composite;

import com.redhat.marketing.automation.command.graph.CompositeCommandNodeController;
import com.redhat.marketing.automation.command.IndeterminateCommand;
import java.util.Collections;
import java.util.List;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 * [[<br />
 * < Structural Pattern - Composite ><br />
 * <Motivation><br />
 * There are times when a program needs to manipulate a tree data structure and it is necessary to treat both Branches as well as Leaf Nodes uniformly. Consider for example a program that manipulates a file system. A file system is a tree structure that contains Branches which are Folders as well as Leaf nodes which are Files. Note that a folder object usually contains one or more file or folder objects and thus is a complex object where a file is a simple object. Note also that since files and folders have many operations and attributes in common, such as moving and copying a file or a folder, listing file or folder attributes such as file name and size, it would be easier and more convenient to treat both file and folder objects uniformly by defining a File System Resource Interface.<br /><br />
 *
 * The classes participating in the pattern are classically:<br />
 * - <Component> - Component is the abstraction for leafs and composites. It defines the interface that must be implemented by the objects in the composition. For example a file system resource defines move, copy, rename, and getSize methods for files and folders.<br />
 * - <Leaf> - Leafs are objects that have no children. They implement services described by the Component interface. For example a file object implements move, copy, rename, as well as getSize methods which are related to the Component interface.<br />
 * - <Composite> - A Composite stores child components in addition to implementing methods defined by the component interface. Composites implement methods defined in the Component interface by delegating to child components. In addition composites provide additional methods for adding, removing, as well as getting components.<br />
 * - <Client> - The client manipulates objects in the hierarchy using the component interface.<br /><br />
 *
 * A client has a reference to a tree data structure and needs to perform operations on all nodes independent of the fact that a node might be a branch or a leaf.<br />
 * The client simply obtains reference to the required node using the component interface, and deals with the node using this interface; it doesn't matter if the node is a composite or a leaf.<br /><br />
 *
 * <Intent><br />
 * The intent of this pattern is to compose objects into tree structures to represent part-whole hierarchies.<br />
 * Composite lets clients treat individual objects and compositions of objects uniformly.<br />
 * ]]<br />
 * - <a href="http://www.oodesign.com/composite-pattern.html">OO Design - Composite Pattern</a>
 * <br />
 *
 * Also implemented a graph solution; per future flexibility around various analytics.
 */
public class CompositeCommandControllerGraph implements IndeterminateCommand, CompositeCommandNodeController
{

    /**
     * This linearGraph (a List) will provide for unlimited flexibility (if so desired) and provide support for numerous useful agorthims.
     */
    private DirectedGraph<CommandVectorEdge, IndeterminateCommand> linearGraph;
    /**
     * This variables holds the root of the graph for later traversals/etc.
     */
    private CommandVectorEdge selfInGraph;
    /**
     * This variable holds the currentStableState; which holds any history related to the last Command executed, along with a reference to the Command.
     */
    private CommandVectorEdge currentStableState;

    public CompositeCommandControllerGraph()
    {
        this.linearGraph = new SimpleDirectedGraph<CommandVectorEdge, IndeterminateCommand>(IndeterminateCommand.class);
    }

    /**
     * Initialize the reference to the root BrowserStableState(this) of the graph and also set that to be the current stable state.
     * e.g. this == root stable state == currentStableState
     */
    @Override
    public void initialize()
    {
        this.currentStableState = new CommandVectorEdge(this);
        this.selfInGraph = this.currentStableState;
        this.linearGraph.addVertex(this.currentStableState);//#1
    }

    @Override
    public void execute()
    {
        for (IndeterminateCommand command : getCommands())
        {
            command.execute();//#1-n::1
        }
    }

    /**
     * Adding a Command will automatically create a reference between the previous Command that was added and the one one being added.
     *
     * @param browserCommand the new command being added
     */
    @Override
    public void addCommand(IndeterminateCommand command)
    {
        if (this.currentStableState == null)
        {
            this.initialize();
        }
        CommandVectorEdge nextStableState = new CommandVectorEdge(command);
        this.linearGraph.addVertex(nextStableState);
        this.linearGraph.addEdge(this.currentStableState, nextStableState, command);
        this.currentStableState = nextStableState;
    }

    /**
     * This method returns the 'Ordered' list of BrowserCommands that are ready to be run.
     *
     * @param browserCommand
     */
    @Override
    public List<IndeterminateCommand> getCommands()
    {
        return Collections.unmodifiableList(DijkstraShortestPath.findPathBetween(this.linearGraph, this.selfInGraph, this.currentStableState));
    }

    @Override
    public void cleanup()
    {
        this.currentStableState = null;
        this.selfInGraph = null;
        this.linearGraph = null;
    }
}
