package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.BrowserCommandImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverDeleteCookie extends BrowserCommandImpl<RemoteWebDriver> implements SeleniumDriverCommand{

    private String cookieName;

    public DriverDeleteCookie(String cookieName) {
        this.cookieName = cookieName;
    }

    @Override
    public void execute(){
        this.getBrowser().manage().deleteCookieNamed(this.cookieName);
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: .manage().deleteCookieNamed(");
        toString.append(this.cookieName);
        toString.append(")");
        return toString.toString();
    }

}
