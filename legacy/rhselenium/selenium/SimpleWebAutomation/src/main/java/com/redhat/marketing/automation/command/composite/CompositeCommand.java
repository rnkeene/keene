package com.redhat.marketing.automation.command.composite;

import com.redhat.marketing.automation.command.InjectedCommand;
import com.redhat.marketing.automation.command.IndeterminateCommand;
import java.util.Collection;

/**
 * A CompositeCommand represents a <B>Data Structure</b> of Commands masked as a <b>single Command</b> and has one or more Commands 'hidden' within it.
 *
 * Generally this is used to create a tree or other self-describing and naturally recursive data structure (e.g. XML, etc).
 *
 * @author nkeene
 */
public interface CompositeCommand<C extends InjectedCommand> extends IndeterminateCommand<C>
{

    /**
     *
     * Consealed Commands
     *
     * @return
     */
    public Collection<C> getCommands();
}
