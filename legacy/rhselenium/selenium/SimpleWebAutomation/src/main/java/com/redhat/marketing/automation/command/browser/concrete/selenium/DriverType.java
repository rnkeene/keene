package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.Type;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverType extends DriverFind implements Type<RemoteWebDriver,WebElement>
{
    private String value;

    public DriverType(String xpath, String value)
    {
        super(xpath);
        this.value = value;
    }

    @Override
    public String getValue()
    {
        return this.getValue();
    }

    @Override
    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public void execute()
    {
        super.execute();
        this.getFindResults().sendKeys(this.value);
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append(super.toString());
        toString.append(".sendKeys(");
        toString.append(this.value);
        toString.append(")");
        return toString.toString();
    }
}
