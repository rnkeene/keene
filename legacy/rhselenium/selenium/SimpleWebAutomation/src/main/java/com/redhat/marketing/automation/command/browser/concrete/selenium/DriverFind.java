package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.FindImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFind extends FindImpl<RemoteWebDriver,WebElement> implements SeleniumDriverCommand{

    public DriverFind(String target)
    {
        super(target);
    }

    @Override
    public void execute()
    {
        this.setFindResults( this.getBrowser().findElement(By.xpath(this.getTarget())) );
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.findElement(By.xpath(");
        toString.append(super.toString());
        toString.append("))");
        return toString.toString();
    }

}
