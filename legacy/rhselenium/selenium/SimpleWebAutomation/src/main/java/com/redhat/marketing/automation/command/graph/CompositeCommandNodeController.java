package com.redhat.marketing.automation.command.graph;

import com.redhat.marketing.automation.command.IndeterminateCommand;
import java.util.List;

public interface CompositeCommandNodeController extends IndeterminateCommand
{

    public void addCommand(IndeterminateCommand command);

    public List<IndeterminateCommand> getCommands();
}
