package com.redhat.marketing.automation.command.browser.concrete.selenium.builder;

import com.redhat.marketing.automation.command.browser.concrete.BrowserCommandBuilder;
import org.openqa.selenium.remote.RemoteWebDriver;

public interface SeleniumCommandBuilder extends BrowserCommandBuilder<RemoteWebDriver>
{
}
