package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.FindAndSelect;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class DriverFindAndSelect extends FindAndSelect<RemoteWebDriver,Select,WebElement> implements SeleniumDriverCommand{

    
    private String value;

    public DriverFindAndSelect(String target, String value) {
        super(target, value);
        this.value = value;
    }

    @Override
    public void execute() {
        DriverFind find = new DriverFind(this.getTarget());
        find.setBrowser(this.getBrowser());
        find.execute();
        this.setSelectElement(new Select(find.getFindResults()));
        this.getSelectElement().selectByVisibleText(this.value);
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: new Select(");
        toString.append(super.toString());
        toString.append(").selectByVisibleText(");
        toString.append(this.value);
        toString.append(")");
        return toString.toString();
    }

}
