package com.redhat.marketing.automation.command.browser.receiver.selenium;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverChromeFactory extends DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new ChromeDriver();
    }

}
