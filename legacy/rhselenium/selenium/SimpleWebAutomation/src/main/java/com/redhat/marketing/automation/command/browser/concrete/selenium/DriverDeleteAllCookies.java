package com.redhat.marketing.automation.command.browser.concrete.selenium;

/**
 * Opens a browser to a particular URL.
 */
public class DriverDeleteAllCookies extends DriverCommand
{

    public DriverDeleteAllCookies()
    {
    }

    @Override
    public void execute()
    {
        this.getBrowser().manage().deleteAllCookies();
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.manage().deleteAllCookies()");
        return toString.toString();
    }
}
