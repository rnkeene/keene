package com.redhat.marketing.automation.command.browser.receiver.selenium;

import com.redhat.marketing.automation.command.receiver.RemoteReceiverServer;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteDriverFirefoxFactory extends RemoteDriverFactory{

    public RemoteDriverFirefoxFactory(RemoteReceiverServer hub) {
        super(hub, DesiredCapabilities.firefox());
    }

}
