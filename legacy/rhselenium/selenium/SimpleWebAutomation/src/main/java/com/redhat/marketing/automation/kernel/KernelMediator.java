package com.redhat.marketing.automation.kernel;
/**
 * 
 * Do not create dependencies between the Invoker and the System Function; such that the Invoker does not know the 'true' System Function, and the System Function never knows the Invoker.
 *
 * @author nkeene
 * @param <I>
 * @param <F>
 */
public interface KernelMediator<I, F> {

    public I getInvoker();
    public F getFunction();

}
