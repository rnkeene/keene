package com.redhat.marketing.automation.command.browser.concrete.selenium;

import org.openqa.selenium.Alert;

/**
 * Opens a browser to a particular URL.
 */
public class DriverClosePopUp extends DriverCommand {

    public DriverClosePopUp() {
    }

    @Override
    public void execute(){
        Alert alert = this.getBrowser().switchTo().alert();
        alert.dismiss();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: (browser.switchTo().alert()).dismiss()");
        return toString.toString();
    }


}
