package com.redhat.marketing.automation.command.browser.concrete.selenium;

import com.redhat.marketing.automation.command.browser.concrete.impl.OpenImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverOpen extends OpenImpl<RemoteWebDriver> implements SeleniumDriverCommand{

    public DriverOpen(String url) {
        super(url);
    }

    @Override
    public void execute(){
        this.getBrowser().get(this.getUrl());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.get(");
        toString.append(super.toString());
        toString.append(")");
        return toString.toString();
    }

}
