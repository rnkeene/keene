package com.redhat.marketing.automation.command.browser.receiver.selenium;

import com.redhat.marketing.automation.command.receiver.RemoteReceiverServer;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteDriverIEFactory extends RemoteDriverFactory{

    public RemoteDriverIEFactory(RemoteReceiverServer hub) {
        super(hub, DesiredCapabilities.internetExplorer());
        this.getCapabilities().setPlatform(Platform.ANY);
    }

}
