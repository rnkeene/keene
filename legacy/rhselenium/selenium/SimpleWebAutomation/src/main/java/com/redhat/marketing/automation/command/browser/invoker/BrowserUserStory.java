package com.redhat.marketing.automation.command.browser.invoker;

import com.redhat.marketing.automation.command.browser.concrete.BrowserCommandBuilder;
import com.redhat.marketing.automation.command.browser.concrete.BrowserController;
import com.redhat.marketing.automation.command.invoker.UserStory;
import java.util.Map;

public interface BrowserUserStory<B> extends UserStory<BrowserCommandBuilder<B>>, BrowserController<B>
{

    public void initialize(Map configurationMap);
}
