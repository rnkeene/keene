package com.redhat.marketing.automation.command.testng.dataprovider;

import com.redhat.marketing.automation.command.testng.FactoryMethod;
import com.redhat.marketing.automation.command.testng.data.factory.SimpleWebAutomationTestDataFactory;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * This Object will ultimately provide the necessary Object[][] or Iterator<Object[]> required to fulfill the TestNG @DataProvider annotation.
 *
 * Optionally, can create Lazy loading and finer grained control/flexibility by implementing a SimpleWebAutomationIterator that implements the <Iterator<Object[]>> method and overrides the next() method.
 *
 * TestNG requires the @DataSource Objects to provide static reference to their data stores.
 *
 * In essence the @DataSource for 'n' number of tests will all be created at once, before any tests run -- so careful with shared variables!
 */
public abstract class SimpleWebAutomationTestDataProvider extends ArrayList<SimpleWebAutomationTestDataFactory> implements FactoryMethod<Iterator<Object[]>> {

    /**
     *
     * This method combines all of the data held in each SimpleWebAutomationCompositeDataFactory.
     *
     * Anywhere that a TestNG @DataProvider is required an implementation of this class can be provided with the appropriate TestNG annotations.
     *
     * @return Iterator<Object[]> the list of input parameters for a SimpleWebAutomationTestFactory and supports the TestNG model as an @DataProvider for an @Factory or @Test.
     */
    @Override
    public Iterator<Object[]> getInstance() {
        List<Object[]> data = new ArrayList<Object[]>();
        for (SimpleWebAutomationTestDataFactory provider : this) {
            data.addAll(provider.build());
        }
        return data.iterator();
    }
    /**
     *
     * This is an example implementation of the data provider for an @Test using two different techniques depending on the desired functionality.
     *
    private final static MockSimpleWebAutomationTestCase testCaseData = new MockSimpleWebAutomationTestCase();
    private final static Iterator<Object[]> cache;

    static {
    testCaseData.add(new MockSimpleWebAutomationTestDataFactory());
    cache = testCaseData.getInstance();
    }
     */
    /**
     *
     * This method ensures all Tests run every TestCases; e.g. the number of executions = #Tests x #TestCases.
     *
    @DataProvider(name = "testData", parallel = true)
    public static Iterator<Object[]> getTestData() {
    return testCaseData.getInstance();
    }
     */
    /**
     *
     * This method ensures all Tests work to finish a single workload of TestCases; e.g. the number of executions = #TestCases
     *
    @DataProvider(name = "testDataCache", parallel = true)
    public static Iterator<Object[]> getTestDataCache() {
    return cache;
    }
     */
}
