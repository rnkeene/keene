package com.redhat.marketing.automation.command.testng.datasource.bridge;

public interface InputInitializer<I>{

    public void initialize(I input);

}
