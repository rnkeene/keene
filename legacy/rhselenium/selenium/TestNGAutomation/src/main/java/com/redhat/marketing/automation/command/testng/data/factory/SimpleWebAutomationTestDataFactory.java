package com.redhat.marketing.automation.command.testng.data.factory;

import com.redhat.marketing.automation.command.testng.datasource.bridge.SimpleWebAutomationData;
import com.redhat.marketing.automation.command.testng.Builder;
import com.redhat.marketing.automation.command.testng.FactoryMethod;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * The SimpleWebAutomationDataFactory is a List data structure that holds any number of implementations of SimpleWebAutomationData.
 *
 * A particular SimpleWebAutomationDataFactory implementation may populate itself with particular pre-programmed (hard-coded) SimpleWebAutomationData, or they can be configured to take configurations from external Controller Objects that build out custom Tests.
 *
 * **getInstance can retrieve hard-coded or cached versions of SimpleWebAutomationData.
 * **build() can process new information that the Factory may have gained access to and return new SimpleWebAutomationData.
 *
 * TestNG Support - The data can be any implementation of SimpleWebAutomationData and will be created by this factory and provided to a @DataProvider for creating the data required for some @Factory or @Test depending on implementation.
 */
public class SimpleWebAutomationTestDataFactory extends ArrayList<Object[]> implements Builder<List<Object[]>>, FactoryMethod<List<Object[]>> {

    /**
     *
     * This method provides an array of SimpleWebAutomationCompositeData in the form of an List<Object[]> in support of the TestNG framework.
     *
     * @return List<M> the master list of all SimpleWebAutomationCompositeData from a list of SimpleWebAutomationCompositeDataFactory.
     */
    @Override
    public List<Object[]> getInstance() {
        return this;
    }

    public boolean add(SimpleWebAutomationData e) {
        System.out.println(this.toString());
        return super.add( new Object[]{e} );
    }



    /**
     *
     * This method is designed to provide a way to add SimpleWebAutomationCompositeData Objects to this SimpleWebAutomationDataFactory.
     *
     * The RedhatCompositeData will be the container that holds all of that data required to run any given Command (or @Factory or @Test in the case of TestNG).
     *
     * In general this method will need to be Overriden and new SimpleWebAutomationCompositeData objects should be created as necessary (per unit of Data to be passed into the @Factory or @Test).
     *
     * For Example, you could use something similar to the following psudo-code: -- this.add( new Object[]{ new MockSimpleWebAutomationCompositeData() } ); --
     *
     * This Objects List<Object[]> will be added to a master List<Object[]> via addAll.  The master List also holds data returned from other SimpleWebAutomationDataFactories.
     *
     * @return a List of Object[], where each Object[] contains a single SimpleWebAutomationCompositeMap; which is the basic entry for a master list provided to a @Factory or @Test.
     */
    @Override
    public List<Object[]> build() {
        this.clear();
        return this;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\n == SimpleWebAutomationDataFactory Data == \n\t - Contents of Factory - ");
        for (Object[] data : this) {
            toString.append("\n\t\t .. Object[] .. \n\t\t\t");
            toString.append(data[0]);
        }
        return toString.toString();
    }
}
