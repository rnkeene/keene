package com.redhat.marketing.automation.command.testng.data;


/**
 *
 * Generic Base SuiteData Container
 * 
 */
public abstract class SimpleWebAutomationTestSuite {
    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" = SimpleWebAutomationSuiteData = ");
        //toString.append(super.toString());
        return toString.toString();
    }
}
