package com.redhat.marketing.automation.command.testng.dataprovider;

/**
 *
 * Objects extending this class functionality will ultimately serve as a @DataProvider for a @Test.
 *
 * Optionally, can create Lazy loading and finer grained control/flexibility by implementing a RedHatIterator that implements the <Iterator<Object[]>> method and overrides the next() method.
 *
 */
public abstract class SimpleWebAutomationTestCaseProvider extends SimpleWebAutomationTestDataProvider {
}
