package com.redhat.marketing.automation.command.testng;

public interface Builder<R> {

    public R build();

}
