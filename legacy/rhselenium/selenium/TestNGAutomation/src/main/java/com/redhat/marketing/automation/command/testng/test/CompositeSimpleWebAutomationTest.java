package com.redhat.marketing.automation.command.testng.test;

import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestCase;
import java.util.ArrayList;

/**
 *
 * A collection of blank template Test Objects.
 *
 * Each CompositeSimpleWebAutomationTest will use RedHatData from an @DataProvider to populate it's requisite data for execution.
 *
 * An @Factory will provide a collection of instances of the SimpleWebAutomationTest that will run in multi-threaded environment, for this reason it is important SimpleWebAutomationTest Objects are thread safe.
 *
 */
public abstract class CompositeSimpleWebAutomationTest extends ArrayList<SimpleWebAutomationTest> {
    /**
     *
     * Each @Test from a @Factory will use a thread from the thread-pool to run the @Tests concurrently; limited by either the @Factory's @Test List size or the thread-pool set by TestNG.
     *
     * For each item returned from the @Test dataProvider reference the same rules will apply as to the @Factory, although instead of using the thread-pool it will use the data-provider-thread-pool.
     *
     * Each thread will simultaniously access the @Test method within the Same Instance of @Test Object; so it is important to consider whether or not this class/method is capable of being executed in a multithreaded way.
     *
     * It is possible to toggle - parallel=[true|false] - within the @DataProvider() parameters.
     *
     * @param runningCase is one Object from the list of data returned from the dataProvider.
     * @Test(dataProvider = "testData", dataProviderClass = MockSimpleWebAutomationTestCase.class)
     */
    public void runTestMethod(SimpleWebAutomationTestCase SimpleWebAutomationTest) {
        for (SimpleWebAutomationTest test : this) {
            test.runTestMethod(SimpleWebAutomationTest);
        }
    }
}
