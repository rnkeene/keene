package com.redhat.marketing.automation.command.testng.datasource.bridge;

import com.redhat.marketing.automation.command.testng.Builder;


public class SimpleWebAutomationDataSource<D extends SimpleWebAutomationData> implements Builder<SimpleWebAutomationData>, InputInitializer<SimpleWebAutomationCompositeData>, SimpleWebAutomationData {

    private D redHatData;

    @Override
    public SimpleWebAutomationData build() {
        return this.redHatData;
    }

    @Override
    public void initialize(SimpleWebAutomationCompositeData input) {
        input.put(SimpleWebAutomationDataSource.class, this);
    }

    public D getSimpleWebAutomationData() {
        return redHatData;
    }

    public void setSimpleWebAutomationData(D redHatData) {
        this.redHatData = redHatData;
    }
}
