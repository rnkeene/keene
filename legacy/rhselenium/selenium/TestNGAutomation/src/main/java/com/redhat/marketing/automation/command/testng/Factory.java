package com.redhat.marketing.automation.command.testng;

public interface Factory<O,I> {

    public O getInstance(I input);
}
