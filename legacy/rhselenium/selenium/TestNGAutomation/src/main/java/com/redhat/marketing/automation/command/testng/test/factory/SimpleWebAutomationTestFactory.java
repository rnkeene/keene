package com.redhat.marketing.automation.command.testng.test.factory;

import com.redhat.marketing.automation.command.testng.Builder;
import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestSuite;
import com.redhat.marketing.automation.command.testng.test.SimpleWebAutomationTest;
import java.util.ArrayList;

/**
 *
 * The SimpleWebAutomationTestFactory is a List data structure that holds implementation <T> of a SimpleWebAutomationTest.
 *
 * The Objects that extend this SimpleWebAutomationTestFactory functionality needs to provide the mechanism for knowing how and when to produce particular tests.
 *
 * @param <T> the implementation of the SimpleWebAutomationTest that is being created by this Factory.
 */
public class SimpleWebAutomationTestFactory<T extends SimpleWebAutomationTest> extends ArrayList<T> implements Builder<Object[]>{
    /**
     *
     * The Selenium Test
     *
     * @param testData
     * @return Object[] the list of test suites.
     *
     * @Factory(dataProvider="Xyz" class=asldk.class);
     */
    public Object[] getTests(SimpleWebAutomationTestSuite testData) {
        return this.build();
    }

    /**
     * 
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return Object[] the list of test suites.
     */
    @Override
    public Object[] build() {
        this.clear();
        return this.toArray();
    }
}
