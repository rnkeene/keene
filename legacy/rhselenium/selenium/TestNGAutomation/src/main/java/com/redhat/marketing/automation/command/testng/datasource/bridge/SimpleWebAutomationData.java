package com.redhat.marketing.automation.command.testng.datasource.bridge;

/**
 *
 * This is the Raw Data that represents another flexible method to control the dynamic functionality within the Command framework.
 *
 * Every Object implementing SimpleWebAutomationData is responsible for adding itself to the RedhatCompositeData during the initialization (enforced by the InputInitializer interface.
 *
 * If an object fails to add itself to the SimpleWebAutomationCompositeData, then it will not be available when needed later.
 *
 * The process to add to the RedhatCompositeData is as simple as providing the proper Override of the initialize method:
 * 
 *  public void initialize(SimpleWebAutomationCompositeData data)
 *  {
 *      data.put(CustomData.class, this);
 *  }
 *
 */
public interface SimpleWebAutomationData extends InputInitializer<SimpleWebAutomationCompositeData> {
    /**
     *
     * The InputInitializer interface will provide the following method stub based on this interface extension:
     *
     *    1.      @param input SimpleWebAutomationCompositeData an implementation of the Composite Pattern using a Map as the data structure.
     *    2.      public void initialize(SimpleWebAutomationCompositeData input) {}
     *
     * By default the implementating Object for this interface should store itself and any related dependencies into the CompositeData map.
     *
     * Optionally the intialize method can be used to access Objects that are required by this class in order to finalize initialization.
     *
     * What, when and how any particular piece of data is populated is flexible and based on implementation:
     *   - it could be done during construction
     *   - it could be done by the initialize method.
     *      *via any particular data providing resource known to the implentation.
     *      *via hard-coded constants within this or a referenced POJO.
     *   - it could be done by an external controller
     *      *via default accessors and mutators
     *      *via custom API additions to the SimpleWebAutomationData implementation.
     */
}
