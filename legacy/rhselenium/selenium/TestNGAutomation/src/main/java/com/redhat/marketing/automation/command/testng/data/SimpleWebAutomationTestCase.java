package com.redhat.marketing.automation.command.testng.data;

import com.redhat.marketing.automation.command.testng.datasource.bridge.SimpleWebAutomationData;
import org.apache.log4j.Logger;

/**
 *
 * Generic Base TestData Container
 * 
 */
public abstract class SimpleWebAutomationTestCase implements SimpleWebAutomationData{

    static final Logger log = Logger.getLogger(SimpleWebAutomationTestCase.class);

    public SimpleWebAutomationTestCase() {
        super();
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" = GatedFormTestCase = ");
        return toString.toString();
    }
}
