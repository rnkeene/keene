package com.redhat.marketing.automation.command.testng;

public interface FactoryMethod<O> {

    public O getInstance();

}
