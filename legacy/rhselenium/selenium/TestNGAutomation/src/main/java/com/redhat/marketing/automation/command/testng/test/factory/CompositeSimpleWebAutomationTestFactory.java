package com.redhat.marketing.automation.command.testng.test.factory;

import com.redhat.marketing.automation.command.testng.Factory;
import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestSuite;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 *
 * The CompositeSimpleWebAutomationTestFactory is a List data structure that holds implementation SimpleWebAutomationTestFactory<T> where <T> is a SimpleWebAutomationTest.
 *
 * The Objects that extend this CompositeSimpleWebAutomationTestFactory functionality need to provide the SimpleWebAutomationTestFactory<T> instances, or a controller needs to.
 *
 */
public abstract class CompositeSimpleWebAutomationTestFactory extends ArrayList<SimpleWebAutomationTestFactory> implements Factory<Object[], SimpleWebAutomationTestSuite> {
    /**
     *
     * The Selenium Tests
     *
     * @param redHatData the TestCase data needed to populate the Object[].
     * @return Object[] the list of Tests.
     */
    public Object[] getTests(SimpleWebAutomationTestSuite redHatData) {
        return this.getInstance(redHatData);
    }

    /**
     *
     * Overrides build[]
     *
     * The build method is designed to create an T of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @param redHatData the TestCase data needed to populate the Object[].
     * @return Object[] the list of Tests.
     */
    @Override
    public Object[] getInstance(SimpleWebAutomationTestSuite redHatData) {
        List testFactories = new ArrayList();
        for (SimpleWebAutomationTestFactory testFactory : this) {
            testFactories.addAll(Arrays.asList(testFactory.getTests(redHatData)));
        }
        return testFactories.toArray();
    }
}
