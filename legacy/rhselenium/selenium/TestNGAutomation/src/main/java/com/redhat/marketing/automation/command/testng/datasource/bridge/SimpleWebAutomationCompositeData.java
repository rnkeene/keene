package com.redhat.marketing.automation.command.testng.datasource.bridge;

import java.util.HashMap;
import java.util.Set;

/**
 *
 * *** An implementation will need to be made for both your SuiteData and TestData for each unique at @Factory and @Test as appropriate.
 * *** The implementations are responsible for being the container for the SimpleWebAutomationData - e.g. the SuiteData and TestData (implementations).
 *
 * This class is designed to work in unison with SimpleWebAutomationData to allow a collection of data to be connected without explicitly references.
 * Dependend Objects will be pulled as required during the initialization phase, so the order items are added can matter.
 *
 * If a SimpleWebAutomationData implementation needs access to another piece of SimpleWebAutomationData then it accesses that particular Object from this container.
 * Only SimpleWebAutomationData is allowed into the CompositeData Map, so any additional required Object references will need to be contained within the SimpleWebAutomationData imlementation (or implement the SimpleWebAutomationData interface).
 *
 * Any number of SimpleWebAutomationData Objects may be created and added to the container during the initialization phase of any particular SimpleWebAutomationData implementation.
 *
 * **This is similar to an implementation of the Chain of Responsibility Pattern, where an Object is passed down a line of executions that are un-aware of one another but can all modify a target object on their 'turn'.
 * **e.g. each object gets it's chance to initialize the container object, but it is not aware of when it is created and added relative to other SimpleWebAutomationData Objects.
 *
 */
public abstract class SimpleWebAutomationCompositeData extends HashMap<Class, SimpleWebAutomationData> implements SimpleWebAutomationData {

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\n == SimpleWebAutomationCompositeData == \n\t - ROOT MAP - ");
        Set<Class> keys = this.keySet();
        for (Class key : keys) {
            if (key != SimpleWebAutomationCompositeData.class) {
                toString.append("\n\t\t .. Key: ");
                toString.append(key);
                toString.append("\n\t\t .. Value: ");
                SimpleWebAutomationData rhd = this.get(key);
                if(rhd!=this){
                    toString.append(rhd);
                }
            }
        }
        return toString.toString();
    }
}
