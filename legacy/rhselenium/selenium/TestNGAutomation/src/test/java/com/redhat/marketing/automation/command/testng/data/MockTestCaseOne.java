package com.redhat.marketing.automation.command.testng.data;

public class MockTestCaseOne extends MockTestCase
{

    public MockTestCaseOne()
    {
        this("Default TestOne Message");
    }

    public MockTestCaseOne(String testMessage)
    {
        super(testMessage + " - 11111");
        System.out.println("== MockTestCaseOne==");
    }
}
