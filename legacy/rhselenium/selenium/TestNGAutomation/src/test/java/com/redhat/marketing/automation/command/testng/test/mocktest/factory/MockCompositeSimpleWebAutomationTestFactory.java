package com.redhat.marketing.automation.command.testng.test.mocktest.factory;

import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestSuite;
import com.redhat.marketing.automation.command.testng.dataprovider.MockSimpleWebAutomationTestSuiteProvider;
import com.redhat.marketing.automation.command.testng.test.factory.CompositeSimpleWebAutomationTestFactory;
import org.testng.annotations.Factory;

/**
 *
 * The SimpleWebAutomationTestFactory is a List data structure that holds implementation <T> of a SimpleWebAutomationTest.
 *
 * The Objects that extend this SimpleWebAutomationTestFactory functionality needs to provide the mechanism for knowing how and when to produce particular tests.
 *
 * @param <T> the implementation of the SimpleWebAutomationTest that is being created by this Factory.
 */
public class MockCompositeSimpleWebAutomationTestFactory extends CompositeSimpleWebAutomationTestFactory {

    /**
     *
     * The Selenium Test
     *
     * @param testData
     * @return
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockSimpleWebAutomationTestSuiteProvider.class)
    @Override
    public Object[] getTests(SimpleWebAutomationTestSuite factorySuite) {
        return super.getTests(factorySuite);
    }

    @Override
    public Object[] getInstance(SimpleWebAutomationTestSuite redHatData) {
        return super.getInstance(redHatData);
    }

}
