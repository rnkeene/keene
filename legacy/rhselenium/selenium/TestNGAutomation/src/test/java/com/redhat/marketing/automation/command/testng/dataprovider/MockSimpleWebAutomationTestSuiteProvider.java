package com.redhat.marketing.automation.command.testng.dataprovider;

import com.redhat.marketing.automation.command.testng.data.factory.MockSimpleWebAutomationSuiteCaseFactory;
import java.util.Iterator;
import org.testng.annotations.DataProvider;

/**
 *
 * An Example @DataProvider Object designed to be the target of a particular set of @Factory Objects.
 *
 * Two methodologies for delivering content for the @Factory executions has been provided.
 *
 */
public class MockSimpleWebAutomationTestSuiteProvider extends SimpleWebAutomationTestSuiteProvider {

    /**
     *
     * Static data initialization per TestNG requirement.
     *
     */
    private final static MockSimpleWebAutomationTestSuiteProvider testSuiteData = new MockSimpleWebAutomationTestSuiteProvider();
    private final static Iterator<Object[]> cache;

    static {
        testSuiteData.add(new MockSimpleWebAutomationSuiteCaseFactory());
        cache = testSuiteData.getInstance();
    }

    /**
     * Empty Constructor for TestNG.
     */
    private MockSimpleWebAutomationTestSuiteProvider() {
        super();
    }

    /**
     *
     * This method ensures all Tests run every TestCases; e.g. the number of executions = #Tests x #TestCases.
     *
     * @return a copy of the cached Iterator<Object[]> for each test to run
     */
    @DataProvider(name = "suiteData", parallel = true)
    public static Iterator<Object[]> getSuiteData() {
        return testSuiteData.getInstance();
    }

    /**
     *
     * This method ensures all Tests work to finish a single workload of TestCases; e.g. the number of executions = #TestCases
     *
     * @return cached Iterator<Object[]> for all of the Tests to share
     */
    @DataProvider(name = "suiteDataCache", parallel = true)
    public static Iterator<Object[]> getSuiteDataCache() {
        return cache;
    }
}
