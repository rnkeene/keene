package com.redhat.marketing.automation.command.testng.data;

public abstract class MockSuiteCase extends SimpleWebAutomationTestSuite{

    private String suiteMessage;

    public MockSuiteCase() {
        this.suiteMessage = "Default Suite Message";
    }

    public MockSuiteCase(String suiteMessage) {
        this.suiteMessage = suiteMessage;
    }
    
    public String getSuiteMessage() {
        return suiteMessage;
    }

    public void setSuiteMessage(String suiteMessage) {
        this.suiteMessage = suiteMessage;
    }

    @Override
    public String toString(){
        return this.suiteMessage;
    }
}
