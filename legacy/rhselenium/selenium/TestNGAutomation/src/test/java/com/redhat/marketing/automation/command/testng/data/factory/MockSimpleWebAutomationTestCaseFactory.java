package com.redhat.marketing.automation.command.testng.data.factory;

import com.redhat.marketing.automation.command.testng.data.MockTestCaseOne;
import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestCase;
import com.redhat.marketing.automation.command.testng.dataprovider.MockSimpleWebAutomationTestSuiteProvider;
import java.util.List;
import org.testng.annotations.Factory;

public class MockSimpleWebAutomationTestCaseFactory extends SimpleWebAutomationTestDataFactory {

    public MockSimpleWebAutomationTestCaseFactory() {
        super();
    }

    /**
     * 
     * Data to add to the @DataProvider master list.
     * 
     * Each Test Case added to the list during the build will execute an @Test method/constructor.
     *
     * This provides a way to quickly multiply the number of configured instances of this @Test that will be run.
     *
     * Configured properly this can cause the total number of Test executions to be {(@Factory @DataProvider list size) x (the @Test @DataProvider list size)}.
     *
     * Assuming all @Test point to the same Test @DataProvider and all @Factories point to the same Factory @DataProvider - there's two generically available options:
     *
     * [TOTAL TESTS] = (Facotry @DataProvider size x Test @DataProvider size)
     *
     *  - OR -
     *
     * [TOTAL TESTS] = (Test @DataProvider size)
     *
     * Multithreading and Concurrency control is another configuration that can be toggled at various levels or on various Objects.
     *
     * The two primary areas of difficulty is determining the breakdown of threads that are running, and identifying code that isn't thread safe.
     * 
     * @return List<Object>
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockSimpleWebAutomationTestSuiteProvider.class)
    @Override
    public List<Object[]> build() {
        super.build();
        System.out.println(this.toString());
        SimpleWebAutomationTestCase testOne = new MockTestCaseOne();
        this.add(testOne);
//
//        SimpleWebAutomationTestCase testTwo = new MockTestCaseTwo();
//        this.add(testTwo);
//
//        SimpleWebAutomationTestCase testThree = new MockTestCaseThree();
//        this.add(testThree);

//        SimpleWebAutomationTestData testDataTwo = new TestTwo();
//        testDataTwo.initialize(testDataTwo);
//        this.add(testDataTwo);

//        SimpleWebAutomationTestData testDataThree = new TestThree();
//        testDataThree.initialize(testDataThree);
//        this.add(testDataThree);

//        SimpleWebAutomationTestData ctestData = new TestOne(" --- CUSTOM ONE --- 11111");
//        ctestData.initialize(ctestData);
//        this.add(ctestData);

//        SimpleWebAutomationTestData ctestDataTwo = new TestTwo(" --- CUSTOM TWO --- 22222");
//        ctestDataTwo.initialize(ctestDataTwo);
//        this.add(ctestDataTwo);

//        SimpleWebAutomationTestData ctestDataThree = new TestThree(" --- CUSTOM THREE --- 33333");
//        ctestDataThree.initialize(ctestDataThree);
//        this.add(ctestDataThree);

        return this;
    }
}
