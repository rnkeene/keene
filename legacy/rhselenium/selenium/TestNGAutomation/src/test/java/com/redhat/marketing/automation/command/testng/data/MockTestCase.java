package com.redhat.marketing.automation.command.testng.data;

import com.redhat.marketing.automation.command.testng.datasource.bridge.SimpleWebAutomationCompositeData;

public class MockTestCase extends SimpleWebAutomationTestCase{

    protected String testMessage;

    public MockTestCase() {
        this("Default Test Message");
    }

    public MockTestCase(String testMessage) {
        this.testMessage = testMessage;
        System.out.println(this.toString());
    }

    public String getTestMessage() {
        return testMessage;
    }

    public void setTestMessage(String testMessage) {
        this.testMessage = testMessage;
    }

    @Override
    public String toString(){
        return this.testMessage;
    }

    @Override
    public void initialize(SimpleWebAutomationCompositeData input)
    {
        System.out.println("//an old interface requires this method... may be useful later - so leaving it.");
    }
}
