package com.redhat.marketing.automation.command.testng.data;

public class MockTestCaseThree extends MockTestCase {

    public MockTestCaseThree() {
        super("Default TestThree Message - 33333");
    }

    public MockTestCaseThree(String testMessage) {
        super(testMessage + " - 33333");
    }
}
