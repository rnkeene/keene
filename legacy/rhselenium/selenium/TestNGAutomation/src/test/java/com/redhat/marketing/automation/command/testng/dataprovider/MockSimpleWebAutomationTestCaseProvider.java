package com.redhat.marketing.automation.command.testng.dataprovider;

import com.redhat.marketing.automation.command.testng.data.factory.MockSimpleWebAutomationTestCaseFactory;
import java.util.Iterator;
import org.testng.annotations.DataProvider;

/**
 *
 * An Example @DataProvider Object designed to be the target of a particular set of @Test Objects.
 *
 * Two methodologies for delivering content for the @Test executions has been provided.
 *
 */
public class MockSimpleWebAutomationTestCaseProvider extends SimpleWebAutomationTestCaseProvider {

    /**
     *
     * Static data initialization per TestNG requirement.
     * 
     */
    private final static MockSimpleWebAutomationTestCaseProvider testCaseData = new MockSimpleWebAutomationTestCaseProvider();
    private final static Iterator<Object[]> cache;

    static {
        testCaseData.add(new MockSimpleWebAutomationTestCaseFactory());
        cache = testCaseData.getInstance();
    }

    /**
     * Empty Constructor for TestNG.
     */
    private MockSimpleWebAutomationTestCaseProvider() {
        super();
    }

    /**
     *
     * This method ensures all Tests run every TestCases; e.g. the number of executions = #Tests x #TestCases.
     * 
     * @return a copy of the cached Iterator<Object[]> for each test to run
     */
    @DataProvider(name = "testData", parallel = true)
    public static Iterator<Object[]> getTestData() {
        return testCaseData.getInstance();
    }

    /**
     *
     * This method ensures all Tests work to finish a single workload of TestCases; e.g. the number of executions = #TestCases
     * 
     * @return cached Iterator<Object[]> for all of the Tests to share
     */
    @DataProvider(name = "testDataCache", parallel = true)
    public static Iterator<Object[]> getTestDataCache() {
        return cache;
    }
}
