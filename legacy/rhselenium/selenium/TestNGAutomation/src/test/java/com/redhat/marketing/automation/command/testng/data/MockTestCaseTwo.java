package com.redhat.marketing.automation.command.testng.data;

public class MockTestCaseTwo extends MockTestCase {

    public MockTestCaseTwo() {
        super("Default TestTwo Message - 22222");
    }

    public MockTestCaseTwo(String testMessage) {
        super(testMessage + " - 22222");
    }
}
