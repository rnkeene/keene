package com.redhat.marketing.automation.command.testng.test.mocktest.factory;

import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestSuite;
import com.redhat.marketing.automation.command.testng.dataprovider.MockSimpleWebAutomationTestSuiteProvider;
import com.redhat.marketing.automation.command.testng.test.factory.SimpleWebAutomationTestFactory;
import com.redhat.marketing.automation.command.testng.test.mocktest.MockSimpleWebAutomationTest;
import com.redhat.marketing.automation.command.testng.test.mocktest.MockSimpleWebAutomationTestOne;
import com.redhat.marketing.automation.command.testng.test.mocktest.MockSimpleWebAutomationTestThree;
import com.redhat.marketing.automation.command.testng.test.mocktest.MockSimpleWebAutomationTestTwo;
import org.testng.annotations.Factory;
/**
 *
 * The SimpleWebAutomationTestFactory is a List data structure that holds implementation <T> of a SimpleWebAutomationTest.
 *
 * The Objects that extend this SimpleWebAutomationTestFactory functionality needs to provide the mechanism for knowing how and when to produce particular tests.
 *
 * @param <T> the implementation of the SimpleWebAutomationTest that is being created by this Factory.
 */
public class MockRedSimpleWebAutomationFactory extends SimpleWebAutomationTestFactory<MockSimpleWebAutomationTest> {

    /**
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockSimpleWebAutomationTestSuiteProvider.class)
    @Override
    public Object[] getTests(SimpleWebAutomationTestSuite factorySuite) {
        return super.getTests(factorySuite);
    }

    /**
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Override
    public Object[] build() {
        this.add(new MockSimpleWebAutomationTestOne());
        this.add(new MockSimpleWebAutomationTestTwo());
        this.add(new MockSimpleWebAutomationTestThree());
        return this.toArray();
    }
}
