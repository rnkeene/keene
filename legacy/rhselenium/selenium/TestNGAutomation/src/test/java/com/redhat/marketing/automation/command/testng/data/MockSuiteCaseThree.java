package com.redhat.marketing.automation.command.testng.data;

public class MockSuiteCaseThree extends MockSuiteCase {

    public MockSuiteCaseThree() {
        super("Default SuiteThree Message - 33333");
    }

    public MockSuiteCaseThree(String suiteMessage) {
        super(suiteMessage + " - 33333");
    }
}
