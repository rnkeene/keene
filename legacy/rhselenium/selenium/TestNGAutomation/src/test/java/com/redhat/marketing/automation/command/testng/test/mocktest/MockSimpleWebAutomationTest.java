package com.redhat.marketing.automation.command.testng.test.mocktest;

import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestCase;
import com.redhat.marketing.automation.command.testng.test.SimpleWebAutomationTest;


public abstract class MockSimpleWebAutomationTest extends SimpleWebAutomationTest {
    
    @Override
    public void runTestMethod(SimpleWebAutomationTestCase simpleWebAutomationTest) {
        System.out.println(this.toString());
    }
    
    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" - MockSimpleWebAutomationTest - ");
        return toString.toString();
    }
}
