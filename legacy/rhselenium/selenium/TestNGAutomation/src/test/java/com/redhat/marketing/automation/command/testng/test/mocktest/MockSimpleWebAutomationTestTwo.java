package com.redhat.marketing.automation.command.testng.test.mocktest;

import com.redhat.marketing.automation.command.testng.data.SimpleWebAutomationTestCase;
import com.redhat.marketing.automation.command.testng.dataprovider.MockSimpleWebAutomationTestCaseProvider;
import org.testng.annotations.Test;

public class MockSimpleWebAutomationTestTwo extends MockSimpleWebAutomationTest {

    @Test(dataProvider = "testData", dataProviderClass = MockSimpleWebAutomationTestCaseProvider.class)
    @Override
    public void runTestMethod(SimpleWebAutomationTestCase runningCase) {
        super.runTestMethod(runningCase);
    }

}
