package com.redhat.marketing.automation.command.testng.data.factory;

import java.util.List;

public class MockSimpleWebAutomationSuiteCaseFactory extends SimpleWebAutomationTestDataFactory {

    public MockSimpleWebAutomationSuiteCaseFactory() {
        super();
    }

    /**
     *
     * Data to add to the @DataProvider master list.
     *
     * Each Suite Case added to the list during the build will execute the @Factory method/constructor.
     *
     * This provides a way to quickly multiply the number of tests created by the @Factory - but also have the option of altering configurations to support the load.
     *
     * @return List<Object>
     */
    @Override
    public List<Object[]> build() {
        super.build();

//        SimpleWebAutomationTestSuite suiteData = new MockSuiteCaseOne();
//        this.add(suiteData);

//        SimpleWebAutomationSuiteData suiteDataTwo = new SuiteTwo();
//        suiteDataTwo.initialize(suiteDataTwo);
//        this.add(suiteDataTwo);

//        SimpleWebAutomationSuiteData suiteDataThree = new SuiteThree();
//        suiteDataThree.initialize(suiteDataThree);
//        this.add(suiteDataThree);

//        SimpleWebAutomationSuiteData suiteDataOneTwo = new SuiteThree();
//        suiteDataOneTwo.initialize(suiteDataOneTwo);
//        this.add(suiteDataOneTwo);

        return this;
    }
}
