package com.redhat.marketing.automation.command.testng.data;

public class MockSuiteCaseTwo extends MockSuiteCase {

    public MockSuiteCaseTwo() {
        super("Default SuiteTwo Message - 22222");
    }

    public MockSuiteCaseTwo(String suiteMessage) {
        super(suiteMessage + " - 22222");
    }
}
