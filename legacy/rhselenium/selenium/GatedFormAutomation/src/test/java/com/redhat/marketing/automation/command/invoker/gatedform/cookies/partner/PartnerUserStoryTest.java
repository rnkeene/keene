package com.redhat.marketing.automation.command.invoker.gatedform.cookies.partner;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStoryTest;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.runner.GatedFormScreenshotUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.partner.PartnerAcceptance;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.partner.PartnerKnownVisitorAutoSubmit;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.partner.PartnerKnownVisitorNoCookie;
import org.junit.Test;

/**
 *
 * @author nkeene
 */
public class PartnerUserStoryTest
{
    @Test
    public void none(){
        assert true;
    }

    @Test
    public void cookieAcceptance()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("PartnerAcceptance", "partnerAcceptance"+GatedFormUserStoryTest.testNumber+"@test.com", new PartnerAcceptance(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmit()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("PartnerKnownVisitor", "partnerKnownVisitorAutoSubmit"+GatedFormUserStoryTest.testNumber+"@test.com", new PartnerKnownVisitorAutoSubmit(), new GatedFormCommandBuilder());
        assert true;
    }
    @Test
    public void knownVisitorAutoSubmitNoCookie()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("PartnerKnownVisitorNoCookie1", "partnerKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new PartnerAcceptance(), new GatedFormCommandBuilder());
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("PartnerKnownVisitorNoCookie2", "partnerKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new PartnerKnownVisitorNoCookie(), new GatedFormCommandBuilder());
        assert true;
    }
}
