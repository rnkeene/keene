package com.redhat.marketing.automation.command.invoker.gatedform.cookies.tactic;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStoryTest;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.runner.GatedFormScreenshotUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.TacticVsFutureAcceptance;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.TacticVsFutureKnownVisitorAutoSubmit;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.TacticVsFutureKnownVisitorNoCookie;
import org.junit.Test;

/**
 *
 * @author nkeene
 */
public class TacticVsFutureUserStoryTest
{

    @Test
    public void none()
    {
        assert true;
    }

    @Test
    public void acceptance()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("Acceptance", "tacticVsFutureCookieAcceptance"+GatedFormUserStoryTest.testNumber+"@new.com", new TacticVsFutureAcceptance(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmit()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("KnownVisitor", "tacticVsFutureKnownVisitorAutoSubmit"+GatedFormUserStoryTest.testNumber+"@new.com", new TacticVsFutureKnownVisitorAutoSubmit(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmitNoCookie()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("KnownVisitorNoCookie1", "tacticVsFutureKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@new.com", new TacticVsFutureAcceptance(), new GatedFormCommandBuilder());
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("KnownVisitorNoCookie2", "tacticVsFutureKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@new.com", new TacticVsFutureKnownVisitorNoCookie(), new GatedFormCommandBuilder());
        assert true;
    }
}
