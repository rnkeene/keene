package com.redhat.marketing.automation.command.invoker.gatedform;

import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.runner.GatedFormScreenshotUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.Acceptance;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.KnownVisitorAutoSubmit;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.KnownVisitorNoCookie;
import org.junit.Test;

/**
 * @author nkeene
 */
public class GatedFormUserStoryTest
{

    public static final int testNumber = 72;

    @Test
    public void none()
    {
        assert true;
    }

    @Test
    public void acceptance()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("Acceptance", "acceptance"+GatedFormUserStoryTest.testNumber+"@test.com", new Acceptance(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmit()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("KnownVisitor", "knownVisitor"+GatedFormUserStoryTest.testNumber+"@test.com", new KnownVisitorAutoSubmit(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmitNoCookie()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("KnownVisitorNoCookie1", "knowNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new Acceptance(), new GatedFormCommandBuilder());
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("KnownVisitorNoCookie2", "knowNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new KnownVisitorNoCookie(), new GatedFormCommandBuilder());
        assert true;
    }
}
