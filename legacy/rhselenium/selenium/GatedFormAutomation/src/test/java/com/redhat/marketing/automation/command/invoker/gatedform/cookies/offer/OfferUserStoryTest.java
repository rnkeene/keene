package com.redhat.marketing.automation.command.invoker.gatedform.cookies.offer;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStoryTest;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.runner.GatedFormScreenshotUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.offer.OfferAcceptance;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.offer.OfferKnownVisitorAutoSubmit;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.offer.OfferKnownVisitorNoCookie;
import org.junit.Test;

/**
 * 
 * (09:06:13 AM) nkeene: what's the parameter suppose to be?
 * (09:07:41 AM) gshereme: its not named -- look in router.js
 * (09:08:04 AM) gshereme: example: 'email/:offerId'
 * (09:08:09 AM) nkeene: tks
 * (09:08:14 AM) gshereme: so the url would be index.html#email/3
 * (09:08:18 AM) gshereme: where 3 = the offer
 *
 * @author nkeene
 */
public class OfferUserStoryTest
{

    @Test
    public void none()
    {
        assert true;
    }

    @Test
    public void cookieAcceptance()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("OfferAcceptance", "offerAcceptance"+GatedFormUserStoryTest.testNumber+"@test.com", new OfferAcceptance(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmit()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("OfferKnownVisitor", "offerKnownVisitorAutoSubmit"+GatedFormUserStoryTest.testNumber+"@test.com", new OfferKnownVisitorAutoSubmit(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmitNoCookie()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("OfferKnownVisitorNoCookie1", "offerKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new OfferAcceptance(), new GatedFormCommandBuilder());
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("OfferKnownVisitorNoCookie2", "offerKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new OfferKnownVisitorNoCookie(), new GatedFormCommandBuilder());
        assert true;
    }
}
