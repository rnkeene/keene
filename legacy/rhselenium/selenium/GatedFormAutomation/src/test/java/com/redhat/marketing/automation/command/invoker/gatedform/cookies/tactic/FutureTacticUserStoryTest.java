package com.redhat.marketing.automation.command.invoker.gatedform.cookies.tactic;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStoryTest;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.runner.GatedFormScreenshotUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.FutureTacticAcceptance;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.FutureTacticKnownVisitorAutoSubmit;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.FutureTacticKnownVisitorNoCookie;
import org.junit.Test;

/**
 *
 * @author nkeene
 */
public class FutureTacticUserStoryTest
{
    @Test
    public void none()
    {
        assert true;
    }

    @Test
    public void acceptance()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("FTacticAcceptance", "futureTacticCookieAcceptance"+GatedFormUserStoryTest.testNumber+"@new.com", new FutureTacticAcceptance(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmit()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("FTacticKnownVisitor", "futureTacticKnownVisitorAutoSubmit"+GatedFormUserStoryTest.testNumber+"@new.com", new FutureTacticKnownVisitorAutoSubmit(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmitNoCookie()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("FTacticKnownVisitorNoCookie1", "futureTacticKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@new.com", new FutureTacticAcceptance(), new GatedFormCommandBuilder());
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("FTacticKnownVisitorNoCookie2", "futureTacticKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@new.com", new FutureTacticKnownVisitorNoCookie(), new GatedFormCommandBuilder());
        assert true;
    }
}
