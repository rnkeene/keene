package com.redhat.marketing.automation.command.invoker.gatedform.cookies.tactic;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStoryTest;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.runner.GatedFormScreenshotUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.TacticAcceptance;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.TacticKnownVisitorAutoSubmit;
import com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic.TacticKnownVisitorNoCookie;
import org.junit.Test;

/**
 *
 * @author nkeene
 */
public class TacticUserStoryTest
{
    @Test
    public void none(){
        assert true;
    }

    @Test
    public void acceptance()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("TacticAcceptance", "tacticCookieAcceptance"+GatedFormUserStoryTest.testNumber+"@test.com", new TacticAcceptance(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmit()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("TacticKnownVisitor", "tacticKnownVisitorAutoSubmit"+GatedFormUserStoryTest.testNumber+"@test.com", new TacticKnownVisitorAutoSubmit(), new GatedFormCommandBuilder());
        assert true;
    }

    @Test
    public void knownVisitorAutoSubmitNoCookie()
    {
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("TacticKnownVisitorNoCookie1", "tacticKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new TacticAcceptance(), new GatedFormCommandBuilder());
        GatedFormScreenshotUserStoryRunner.runGatedFormScreenshotUserStory("TacticKnownVisitorNoCookie2", "tacticKnownVisitorNoCookie"+GatedFormUserStoryTest.testNumber+"@test.com", new TacticKnownVisitorNoCookie(), new GatedFormCommandBuilder());
        assert true;
    }
}
