package com.redhat.marketing.automation.command.invoker.gatedform.runner;

import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.SeleniumCommandBuilder;
import com.redhat.marketing.automation.command.browser.invoker.selenium.runner.SeleniumUserStoryRunner;
import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormConfiguration;
import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class GatedFormUserStoryRunner<U extends GatedFormUserStory> extends SeleniumUserStoryRunner<U>
{

    public static void runGatedFormUserStory(String configurationId, GatedFormUserStory story, SeleniumCommandBuilder builder)
    {
        GatedFormConfiguration configs = GatedFormConfiguration.getGatedFormConfigurations(configurationId);
        story.initialize(configs);
        GatedFormUserStoryRunner<GatedFormUserStory> arunner = new GatedFormUserStoryRunner<GatedFormUserStory>();
        arunner.setConfigurationId(configurationId);
        arunner.runUserStory(story, builder);
    }
}
