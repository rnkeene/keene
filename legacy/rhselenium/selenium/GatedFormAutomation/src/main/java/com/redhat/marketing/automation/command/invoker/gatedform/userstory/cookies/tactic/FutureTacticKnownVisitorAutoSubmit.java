package com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class FutureTacticKnownVisitorAutoSubmit extends GatedFormUserStory{

    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.addSetCookie(this.getGatedFormConfiguration().getTIDCookieDetails());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.getCommandBuilder().addNavigate(this.getGatedFormConfiguration().getLandingPage(), 1000);
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getGatedAssetReference(), 6000);
        this.addCommand(this.getCommandBuilder());
    }

}
