package com.redhat.marketing.automation.command.invoker.gatedform;

import com.redhat.marketing.automation.command.browser.invoker.selenium.SeleniumUserStory;
import com.redhat.marketing.automation.command.browser.receiver.selenium.RemoteDriverFactory;
import com.redhat.marketing.automation.command.receiver.RemoteReceiverServer;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class GatedFormConfiguration extends HashMap
{

    private String landingDomain = "http://cms-300.usersys.redhat.com/assets/gatedform/";
//    private String landingDomain = "http://localhost/gated-form/";

    /**
     * CONVERT the following variables TO SHARED_TEST_CASE OBJECT -- NEEDS DESIGN
     */
    private String landingPage = landingDomain + "gate.html";
    private String offer1LandingPage = "http://cms-300.usersys.redhat.com/assets/gatedform/gate.html?offer-1=1234";
    private String offer2LandingPage = "http://cms-300.usersys.redhat.com/assets/gatedform/gate.html?offer-2=2345";
    private String offer3LandingPage = "http://cms-300.usersys.redhat.com/assets/gatedform/gate.html?offer-3=3456";
    /**
     * CONVERT the following variables TO A USER_TEST_CASE OBJECT -- NEEDS DESIGN
     */
    private String userEmail;
    private String userFirstName = "Any First";
    private String userLastName = "Any Last";
    private String userPhoneNumber = "555-555-55555";
    private String userCompany = "Red Hat";
    private String userCompanyTitle = "Developer/Engineer";
    private String userCompanyDepartment = "IT/IS/Operations";
    /**
     * CONVERT the following variables TO A BROWSER_TEST_CASE OBJECT -- NEEDS DESIGN
     */
    private String gatedAssetReference = "//a[@id='offer-1']";
    private String gatedIFrameClassName = "fancybox-iframe";
    /**
     * Short Form Fields
     */
    private String shortFormEmail = "//input[@id='s_email']";
    private String shortFormSubmit = "//input[@value='Submit']";
    private String formLocationPrefix = "//fieldset[@id='user-profile']/ul/li";
    /**
     * Long Form Fields
     */
    private String formFirstName = formLocationPrefix + "[2]/input";
    private String formLastName = formLocationPrefix + "[3]/input";
    private String formPhoneNumber = formLocationPrefix + "[4]/input";
    private String formCompany = formLocationPrefix + "[5]/input";
    private String formCompanyTitle = formLocationPrefix + "[6]/select";
    private String formCompanyDepartment = formLocationPrefix + "[7]/select";
    private String formOptOut = "//ul[@id='opt-response']/li/input";
    private String formSubmit = "//fieldset[@id='submit']/input";
    /**
     * Cookie Details
     */
    private String cookieSCCIDValue = "sc_cid_value";
    private String cookieTIDValue = "tid_value";
    private String cookiePIDValue = "pid_value";
    private String cookieDomain = "cms-300.usersys.redhat.com";
    private String cookiePath = "/assets/gatedform";
    private Date cookieMaxAge;
    private boolean cookieSecure = false;
    /**
     * Offer ID info
     */
    private String offerIdValue = "offer_id_value";

    private GatedFormConfiguration(String userEmail)
    {
        super();
        this.userEmail = userEmail;
        Date expiresOn = Calendar.getInstance().getTime();
        expiresOn.setTime(expiresOn.getTime() + (1000 * 60 * 60 * 24 * 365));//add one year.
        this.cookieMaxAge = expiresOn;

        this.put(SeleniumUserStory.ConfigurationKeys.BROWSER_ID.getKey(), RemoteDriverFactory.BrowserId.FIREFOX.getBrowserId());
//        this.put(SeleniumUserStory.ConfigurationKeys.BROWSER_ID.getKey(), RemoteDriverFactory.BrowserId.INTERNET_EXPLORER.getBrowserId());
        this.put(SeleniumUserStory.ConfigurationKeys.RECEIVER_DOMAIN.getKey(), RemoteReceiverServer.ReceiverStandardEnvironment.LOCAL.getDomain());
        this.put(SeleniumUserStory.ConfigurationKeys.RECEIVER_PORT.getKey(), Integer.toString(RemoteReceiverServer.ReceiverStandardEnvironment.LOCAL.getPort()));
        this.put(SeleniumUserStory.ConfigurationKeys.RECEIVER_PROTOCOL.getKey(), RemoteReceiverServer.ReceiverStandardEnvironment.LOCAL.getProtocol());
        this.put(SeleniumUserStory.ConfigurationKeys.RECEIVER_REGISTER_CONTEXT.getKey(), RemoteReceiverServer.ReceiverStandardEnvironment.LOCAL.getRegistrationContext());
    }

    public static GatedFormConfiguration getGatedFormConfigurations(String testEmail)
    {
        return new GatedFormConfiguration(testEmail);
    }

    public CookieDetails getTIDCookieDetails()
    {
        CookieDetails details = new CookieDetails();
        details.setCookieName(CookieDetails.GatedFormCookieDetails.TID.getName());
        details.setCookieValue(this.cookieTIDValue);
        this.addDefaultCookieDetails(details);
        return details;
    }

    public CookieDetails getSCCIDCookieDetails()
    {
        CookieDetails details = new CookieDetails();
        details.setCookieName(CookieDetails.GatedFormCookieDetails.SC_CID.getName());
        details.setCookieValue(this.cookieSCCIDValue);
        this.addDefaultCookieDetails(details);
        return details;
    }

    public CookieDetails getPIDCookieDetails()
    {
        CookieDetails details = new CookieDetails();
        details.setCookieName(CookieDetails.GatedFormCookieDetails.PID.getName());
        details.setCookieValue(this.cookiePIDValue);
        this.addDefaultCookieDetails(details);
        return details;
    }

    private void addDefaultCookieDetails(CookieDetails details)
    {
        details.setDomain(this.cookieDomain);
        details.setPath(this.cookiePath);
        details.setMaxAge(this.cookieMaxAge);
        details.setIsSecure(this.cookieSecure);
    }

    public String getCookieDomain()
    {
        return cookieDomain;
    }

    public void setCookieDomain(String cookieDomain)
    {
        this.cookieDomain = cookieDomain;
    }

    public Date getCookieMaxAge()
    {
        return cookieMaxAge;
    }

    public void setCookieMaxAge(Date cookieMaxAge)
    {
        this.cookieMaxAge = cookieMaxAge;
    }

    public String getCookiePIDValue()
    {
        return cookiePIDValue;
    }

    public void setCookiePIDValue(String cookiePIDValue)
    {
        this.cookiePIDValue = cookiePIDValue;
    }

    public String getCookiePath()
    {
        return cookiePath;
    }

    public void setCookiePath(String cookiePath)
    {
        this.cookiePath = cookiePath;
    }

    public String getCookieSCCIDValue()
    {
        return cookieSCCIDValue;
    }

    public void setCookieSCCIDValue(String cookieSCCIDValue)
    {
        this.cookieSCCIDValue = cookieSCCIDValue;
    }

    public boolean isCookieSecure()
    {
        return cookieSecure;
    }

    public void setCookieSecure(boolean cookieSecure)
    {
        this.cookieSecure = cookieSecure;
    }

    public String getCookieTIDValue()
    {
        return cookieTIDValue;
    }

    public void setCookieTIDValue(String cookieTIDValue)
    {
        this.cookieTIDValue = cookieTIDValue;
    }

    public String getOfferIdValue()
    {
        return offerIdValue;
    }

    public void setOfferIdValue(String offerIdValue)
    {
        this.offerIdValue = offerIdValue;
    }

    public String getUserEmail()
    {
        return userEmail;
    }

    public String getFormCompany()
    {
        return formCompany;
    }

    public void setFormCompany(String formCompany)
    {
        this.formCompany = formCompany;
    }

    public String getFormCompanyDepartment()
    {
        return formCompanyDepartment;
    }

    public void setFormCompanyDepartment(String formCompanyDepartment)
    {
        this.formCompanyDepartment = formCompanyDepartment;
    }

    public String getFormCompanyTitle()
    {
        return formCompanyTitle;
    }

    public void setFormCompanyTitle(String formCompanyTitle)
    {
        this.formCompanyTitle = formCompanyTitle;
    }

    public String getFormFirstName()
    {
        return formFirstName;
    }

    public void setFormFirstName(String formFirstName)
    {
        this.formFirstName = formFirstName;
    }

    public String getFormLastName()
    {
        return formLastName;
    }

    public void setFormLastName(String formLastName)
    {
        this.formLastName = formLastName;
    }

    public String getFormLocationPrefix()
    {
        return formLocationPrefix;
    }

    public void setFormLocationPrefix(String formLocationPrefix)
    {
        this.formLocationPrefix = formLocationPrefix;
    }

    public String getFormOptOut()
    {
        return formOptOut;
    }

    public void setFormOptOut(String formOptOut)
    {
        this.formOptOut = formOptOut;
    }

    public String getFormPhoneNumber()
    {
        return formPhoneNumber;
    }

    public void setFormPhoneNumber(String formPhoneNumber)
    {
        this.formPhoneNumber = formPhoneNumber;
    }

    public String getFormSubmit()
    {
        return formSubmit;
    }

    public void setFormSubmit(String formSubmit)
    {
        this.formSubmit = formSubmit;
    }

    public String getGatedAssetReference()
    {
        return gatedAssetReference;
    }

    public void setGatedAssetReference(String gatedAssetReference)
    {
        this.gatedAssetReference = gatedAssetReference;
    }

    public String getGatedIFrameClassName()
    {
        return gatedIFrameClassName;
    }

    public void setGatedIFrameClassName(String gatedIFrameClassName)
    {
        this.gatedIFrameClassName = gatedIFrameClassName;
    }

    public String getLandingPage()
    {
        return landingPage;
    }

    public void setLandingPage(String landingPage)
    {
        this.landingPage = landingPage;
    }

    public String getOffer1LandingPage()
    {
        return offer1LandingPage;
    }

    public void setOffer1LandingPage(String offerLandingPage)
    {
        this.offer1LandingPage = offerLandingPage;
    }
    public String getOffer2LandingPage()
    {
        return offer2LandingPage;
    }

    public void setOffer2LandingPage(String offerLandingPage)
    {
        this.offer2LandingPage = offerLandingPage;
    }

    public String getOffer3LandingPage()
    {
        return offer3LandingPage;
    }

    public void setOffer3LandingPage(String offerLandingPage)
    {
        this.offer3LandingPage = offerLandingPage;
    }

    public String getShortFormEmail()
    {
        return shortFormEmail;
    }

    public void setShortFormEmail(String shortFormEmail)
    {
        this.shortFormEmail = shortFormEmail;
    }

    public String getShortFormSubmit()
    {
        return shortFormSubmit;
    }

    public void setShortFormSubmit(String shortFormSubmit)
    {
        this.shortFormSubmit = shortFormSubmit;
    }

    public String getUserCompany()
    {
        return userCompany;
    }

    public void setUserCompany(String userCompany)
    {
        this.userCompany = userCompany;
    }

    public String getUserCompanyDepartment()
    {
        return userCompanyDepartment;
    }

    public void setUserCompanyDepartment(String userCompanyDepartment)
    {
        this.userCompanyDepartment = userCompanyDepartment;
    }

    public String getUserCompanyTitle()
    {
        return userCompanyTitle;
    }

    public void setUserCompanyTitle(String userCompanyTitle)
    {
        this.userCompanyTitle = userCompanyTitle;
    }

    public String getUserFirstName()
    {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName)
    {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName()
    {
        return userLastName;
    }

    public void setUserLastName(String userLastName)
    {
        this.userLastName = userLastName;
    }

    public String getUserPhoneNumber()
    {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber)
    {
        this.userPhoneNumber = userPhoneNumber;
    }
}
