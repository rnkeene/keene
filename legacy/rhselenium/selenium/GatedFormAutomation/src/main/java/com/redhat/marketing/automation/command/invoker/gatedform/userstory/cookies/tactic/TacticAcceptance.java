package com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class TacticAcceptance extends GatedFormUserStory
{

    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.addSetCookie(this.getGatedFormConfiguration().getSCCIDCookieDetails());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.addCommand(this.getCommandBuilder());
    }
}
