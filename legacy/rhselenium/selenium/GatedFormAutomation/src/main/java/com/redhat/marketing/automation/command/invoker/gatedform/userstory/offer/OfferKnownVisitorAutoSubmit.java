package com.redhat.marketing.automation.command.invoker.gatedform.userstory.offer;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class OfferKnownVisitorAutoSubmit extends GatedFormUserStory{

    @Override
    public void build()
    {
        this.getCommandBuilder().addDeleteAllCookies();
        this.getCommandBuilder().addOpen(this.getGatedFormConfiguration().getOffer3LandingPage());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.getCommandBuilder().addNavigate(this.getGatedFormConfiguration().getOffer3LandingPage(), 1000);
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getGatedAssetReference(), 6000);
        this.addCommand(this.getCommandBuilder());
    }

}
