package com.redhat.marketing.automation.command.invoker.gatedform.runner;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.decorator.GatedFormScreenshotDecorator;

public class GatedFormScreenshotUserStoryRunner<U extends GatedFormUserStory> extends GatedFormUserStoryRunner<U>
{

    public static void runGatedFormScreenshotUserStory(String storyName, String configurationId, GatedFormUserStory story, GatedFormCommandBuilder builder)
    {
        GatedFormScreenshotDecorator decoratedCommandBuilder = new GatedFormScreenshotDecorator(storyName, builder, true, true);
        GatedFormUserStoryRunner.runGatedFormUserStory(configurationId, story, decoratedCommandBuilder);
    }
}
