package com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.partner;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class PartnerAcceptance extends GatedFormUserStory
{

    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.addSetCookie(this.getGatedFormConfiguration().getPIDCookieDetails());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.addCommand(this.getCommandBuilder());
    }
}
