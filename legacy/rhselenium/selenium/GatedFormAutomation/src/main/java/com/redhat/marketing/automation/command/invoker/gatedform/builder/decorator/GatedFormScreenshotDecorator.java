package com.redhat.marketing.automation.command.invoker.gatedform.builder.decorator;

import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.decorator.SeleniumScreenshotDecorator;
import com.redhat.marketing.automation.command.invoker.gatedform.builder.GatedFormCommandBuilder;

/**
 * @author nkeene
 */
public class GatedFormScreenshotDecorator extends SeleniumScreenshotDecorator
{

    public GatedFormScreenshotDecorator(String category, GatedFormCommandBuilder decorated, boolean before, boolean after)
    {
        super(category, decorated, before, after);
    }
}
