package com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.tactic;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class FutureTacticAcceptance extends GatedFormUserStory
{
    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.addSetCookie(this.getGatedFormConfiguration().getTIDCookieDetails());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.addCommand(this.getCommandBuilder());
    }
}
