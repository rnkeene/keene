package com.redhat.marketing.automation.command.invoker.gatedform.userstory;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

/**
 * 
 * The GatedFormAcceptanceUserStory class is designed to capture the most basic/default Flows that verify the Gated Form functionality for deployment!<br /><br />
 *
 * It will use a BrowserCommandBuilder Object to complete the Steps assigned within the build() method implementation here.<br /><br />
 *
 * This Object also begins to define some base level Abstraction for the Flows particular to the Gated Form.<br /><br />
 * 
 * @author nkeene
 */
public class Acceptance extends GatedFormUserStory
{

    @Override
    public void build()
    {
        super.build();
        this.addCommand(this.getCommandBuilder());
    }

}