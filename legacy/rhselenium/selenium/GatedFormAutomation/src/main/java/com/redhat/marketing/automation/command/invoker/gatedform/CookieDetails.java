package com.redhat.marketing.automation.command.invoker.gatedform;

import java.util.Date;

public class CookieDetails
{

    private String cookieName;
    private String cookieValue;
    private String domain;
    private String path;
    private Date maxAge;
    private boolean isSecure;

    public String getCookieName()
    {
        return cookieName;
    }

    public void setCookieName(String cookieName)
    {
        this.cookieName = cookieName;
    }

    public String getCookieValue()
    {
        return cookieValue;
    }

    public void setCookieValue(String cookieValue)
    {
        this.cookieValue = cookieValue;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public boolean isSecure()
    {
        return isSecure;
    }

    public void setIsSecure(boolean isSecure)
    {
        this.isSecure = isSecure;
    }

    public Date getMaxAge()
    {
        return maxAge;
    }

    public void setMaxAge(Date maxAge)
    {
        this.maxAge = maxAge;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public enum GatedFormCookieDetails
    {

        SC_CID("SC_CID"),
        TID("tid"),
        PID("pid"),
        OFFER_ID("offer-id"),
        DOMAIN("domain"),
        PATH("path"),
        MAX_AGE("max_age"),
        SECURE("secure");
        private String name;

        GatedFormCookieDetails(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }
}
