package com.redhat.marketing.automation.command.invoker.gatedform;

import com.redhat.marketing.automation.command.browser.invoker.selenium.SeleniumUserStory;

/**
 * The GatedFormAcceptanceUserStory class is designed to capture the most basic/default Flows that verify the Gated Form functionality for deployment!<br /><br />
 *
 * It will use a BrowserCommandBuilder Object to complete the Steps assigned within the build() method implementation here.<br /><br />
 *
 * This Object also begins to define some base level Abstraction for the Flows particular to the Gated Form.<br /><br />
 * 
 * @author nkeene
 */
public class GatedFormUserStory extends SeleniumUserStory
{

    public GatedFormConfiguration getGatedFormConfiguration()
    {
        return (GatedFormConfiguration) this.getConfigurationMap();
    }

    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
    }

    /**
     * Browse to a page that has a resource link behind the Gated Form.
     */
    protected void openCleanBrowserToGatedForm()
    {
        this.getCommandBuilder().addDeleteAllCookies();
        this.getCommandBuilder().addOpen(this.getGatedFormConfiguration().getLandingPage());
    }

    /**
     * Click the reference that triggers the Gated Form functionality.
     */
    protected void clickToGatedAsset()
    {
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getGatedAssetReference());
        this.getCommandBuilder().addSelectFrameByClassName(this.getGatedFormConfiguration().getGatedIFrameClassName(), 3000);
    }

    /**
     * Fill in an email (or other information) required by the "Short" version of the Gated Form.
     */
    protected void populateAndSubmitShortForm()
    {
        this.getCommandBuilder().addWaitForElement(this.getGatedFormConfiguration().getShortFormEmail());
        this.getCommandBuilder().addType(this.getGatedFormConfiguration().getShortFormEmail(), this.getGatedFormConfiguration().getUserEmail());
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getShortFormSubmit(), 3000);
    }

    protected void populateAndSubmitLongForm()
    {
        this.getCommandBuilder().addWaitForElement(this.getGatedFormConfiguration().getFormFirstName());
        this.getCommandBuilder().addType(this.getGatedFormConfiguration().getFormFirstName(), this.getGatedFormConfiguration().getUserFirstName());
        this.getCommandBuilder().addType(this.getGatedFormConfiguration().getFormLastName(), this.getGatedFormConfiguration().getUserLastName());
        this.getCommandBuilder().addType(this.getGatedFormConfiguration().getFormPhoneNumber(), this.getGatedFormConfiguration().getUserPhoneNumber());
        this.getCommandBuilder().addType(this.getGatedFormConfiguration().getFormCompany(), this.getGatedFormConfiguration().getUserCompany());
        this.getCommandBuilder().addSelectDropdown(this.getGatedFormConfiguration().getFormCompanyTitle(), this.getGatedFormConfiguration().getUserCompanyTitle());
        this.getCommandBuilder().addSelectDropdown(this.getGatedFormConfiguration().getFormCompanyDepartment(), this.getGatedFormConfiguration().getUserCompanyDepartment());
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getFormOptOut());
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getFormSubmit(), 3000);
    }

    public void addSetCookie(CookieDetails cookie){
        this.getCommandBuilder().addSetCookie(cookie.getCookieName(), cookie.getCookieValue(), cookie.getDomain(), cookie.getPath(), cookie.getMaxAge(), cookie.isSecure());
    }
}
