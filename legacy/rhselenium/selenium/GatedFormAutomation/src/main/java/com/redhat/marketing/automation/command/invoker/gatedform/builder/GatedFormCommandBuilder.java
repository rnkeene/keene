package com.redhat.marketing.automation.command.invoker.gatedform.builder;

import com.redhat.marketing.automation.command.browser.concrete.selenium.builder.SeleniumNoWaitCommandBuilder;

public class GatedFormCommandBuilder extends SeleniumNoWaitCommandBuilder{

    @Override
    public void build()
    {
        //Nothing to Build.
    }

}
