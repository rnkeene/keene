package com.redhat.marketing.automation.command.invoker.gatedform.userstory;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

/**
 * @author nkeene
 */
public class KnownVisitorAutoSubmit extends GatedFormUserStory
{

    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.getCommandBuilder().addNavigate(this.getGatedFormConfiguration().getLandingPage(), 1000);
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getGatedAssetReference(), 4000);
        this.addCommand(this.getCommandBuilder());
    }
}
