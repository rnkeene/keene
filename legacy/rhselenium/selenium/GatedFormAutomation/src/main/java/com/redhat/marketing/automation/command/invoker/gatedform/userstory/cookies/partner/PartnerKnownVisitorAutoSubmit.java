package com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.partner;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class PartnerKnownVisitorAutoSubmit extends GatedFormUserStory{

    @Override
    public void build()
    {
        this.openCleanBrowserToGatedForm();
        this.addSetCookie(this.getGatedFormConfiguration().getPIDCookieDetails());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.getCommandBuilder().addNavigate(this.getGatedFormConfiguration().getLandingPage(), 1000);
        this.getCommandBuilder().addClick(this.getGatedFormConfiguration().getGatedAssetReference(), 6000);
        this.addCommand(this.getCommandBuilder());
    }

}
