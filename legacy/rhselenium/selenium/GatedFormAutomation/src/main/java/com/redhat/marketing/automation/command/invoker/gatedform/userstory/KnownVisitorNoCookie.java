package com.redhat.marketing.automation.command.invoker.gatedform.userstory;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

/**
 * @author nkeene
 */
public class KnownVisitorNoCookie extends GatedFormUserStory
{

    @Override
    public void build()
    {
        this.getCommandBuilder().addOpen(this.getGatedFormConfiguration().getLandingPage(), 1000);
        this.getCommandBuilder().addDeleteAllCookies();
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.addCommand(this.getCommandBuilder());
    }
}
