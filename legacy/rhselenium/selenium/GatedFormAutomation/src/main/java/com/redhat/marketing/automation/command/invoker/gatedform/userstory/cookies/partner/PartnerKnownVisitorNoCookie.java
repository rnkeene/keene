package com.redhat.marketing.automation.command.invoker.gatedform.userstory.cookies.partner;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

/**
 * @author nkeene
 */
public class PartnerKnownVisitorNoCookie extends GatedFormUserStory
{

    @Override
    public void build()
    {
        this.getCommandBuilder().addOpen(this.getGatedFormConfiguration().getLandingPage(), 1000);
        this.addSetCookie(this.getGatedFormConfiguration().getPIDCookieDetails());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.addCommand(this.getCommandBuilder());
    }
}
