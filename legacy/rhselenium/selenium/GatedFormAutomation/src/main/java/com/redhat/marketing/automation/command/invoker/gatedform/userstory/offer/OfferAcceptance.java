package com.redhat.marketing.automation.command.invoker.gatedform.userstory.offer;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

public class OfferAcceptance extends GatedFormUserStory
{

    @Override
    public void build()
    {
        this.getCommandBuilder().addDeleteAllCookies();
        this.getCommandBuilder().addOpen(this.getGatedFormConfiguration().getOffer2LandingPage());
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.populateAndSubmitLongForm();
        this.addCommand(this.getCommandBuilder());
    }
}
