package com.redhat.marketing.automation.command.invoker.gatedform.userstory.offer;

import com.redhat.marketing.automation.command.invoker.gatedform.GatedFormUserStory;

/**
 * @author nkeene
 */
public class OfferKnownVisitorNoCookie extends GatedFormUserStory
{
    @Override
    public void build()
    {
        this.getCommandBuilder().addOpen(this.getGatedFormConfiguration().getOffer2LandingPage(), 1000);
        this.clickToGatedAsset();
        this.populateAndSubmitShortForm();
        this.addCommand(this.getCommandBuilder());
    }
}
