package com.redhat.cms.selenium.report;

public class PageFunction {

    /**

    private String url;
    private List<CommandMetric> commands;
    private Map<CommandMetric, Boolean> success;
    private boolean pass;

    public PageFunction() {
        this.commands = new ArrayList<Command>();
        this.success = new HashMap<Command, Boolean>();
    }

    public void addCommand(Command command) {
        this.commands.add(command);
    }

    public void addCommandResult(Command command, boolean success) {
        this.success.put(command, success);
    }

    public boolean getCommandResults(CommandMetric command) {
        return this.success.get(command);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void process() {
        for (Command command : this.commands) {
            if (!this.success.get(command)) {
                this.pass = false;
                return;
            }
        }
        this.pass = true;
    }

    public boolean isPass() {
        return pass;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\t");
        if (this.pass) {
            builder.append("SUCCESS - ");
        } else {
            builder.append("FAILURE - \n\t\t");
            for (Command command : this.commands) {
                if (!this.success.get(command)) {
                    builder.append(command);
                    builder.append("\n\t\tURL - ");
                }
            }
            builder.append(this.url);
            builder.append("\n");
        }
        builder.append(this.url);
        return builder.toString();
    }
     */
}
