package com.redhat.cms.selenium.report.builder;

public abstract class PageFunctionReport {
/**
    @Override
    public void buildReport(CompositeCommand<B, Command> commands) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void buildReport(CompositeCommand<B> browser, CommandMetric> commands) {
        PageFunction tempPage = null;
        for (Command metric : commands) {
            Command command = ((CommandMetric) metric).getCommand();
            if (DriverOpen.class.isInstance(command)) {
                PageFunction page = new PageFunction();
                DriverOpen open = (DriverOpen) command;
                page.setUrl(open.getUrl());
                this.add(page);
                tempPage = page;
            } else {
                tempPage.addCommand(command);
                tempPage.addCommandResult(command, ((DebugMetric) metric).isSuccessful());
            }
        }
        this.process();
    }

    private void process() {
        for (PageFunction function : this) {
            function.process();
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(" == Page Function Report == \n");
        for (PageFunction function : this) {
            builder.append(function);
            builder.append("\n");
        }
        return builder.toString();
    }
 */
}
