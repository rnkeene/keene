package com.redhat.cms.selenium.report;

import com.redhat.cms.selenium.command.decorator.metric.MetricDecorator;

public interface Report<M extends MetricDecorator> {

    public void buildReport(MetricDecorator metrics);
    
}
