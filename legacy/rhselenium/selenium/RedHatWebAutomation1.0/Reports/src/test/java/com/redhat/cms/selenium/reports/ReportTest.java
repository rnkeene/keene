package com.redhat.cms.selenium.reports;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ReportTest{

    static Logger log = Logger.getLogger(ReportTest.class);

    @BeforeClass
    public void start(){
        log.info("ReportTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("ReportTest.setUp()");
        assert true;
    }

    @Test
    public void testReport(){
        log.info("ReportTest.testReport()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("ReportTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("ReportTest.finish()");
        assert true;
    }
    
}
