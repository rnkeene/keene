package com.redhat.cms.selenium.webevent.constants.webpage;

import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import java.util.ArrayList;
import java.util.List;

public class URLList implements WebConstants {

    private String testProtocol;
    private String testDomain;
    private String testContexts;
    private List<String> urls;

    public URLList() {
    }

    public URLList(String testProtocol, String testDomain, String testContexts) {
        this.testProtocol = testProtocol;
        this.testDomain = testDomain;
        this.testContexts = testContexts;
    }

    public String getTestContexts() {
        return testContexts;
    }

    public void setTestContexts(String testContexts) {
        this.testContexts = testContexts;
    }

    public String getTestDomain() {
        return testDomain;
    }

    public void setTestDomain(String testDomain) {
        this.testDomain = testDomain;
    }

    public String getTestProtocol() {
        return testProtocol;
    }

    public void setTestProtocol(String testProtocol) {
        this.testProtocol = testProtocol;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        this.urls = this.build();
        if(this.urls!=null && !this.urls.isEmpty()){
            webConstants.put(URLList.class, this);
        }
    }
    
    public List<String> build() {
        List<String> strings = new ArrayList<String>();
        if (this.testContexts.contains(",")) {
            String[] stringArray = this.testContexts.split(",");
            for (int x = 0; x < stringArray.length; x++) {
                strings.add(this.testProtocol + this.testDomain + stringArray[x]);
            }
        } else {
            strings.add(this.testProtocol + this.testDomain + this.testContexts);
        }
        this.urls = strings;
        return strings;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tTest URL Constants: \n\t\tTest URL Protocol: ");
        toString.append(this.testProtocol);
        toString.append("\n\t\tTest URL Domain: ");
        toString.append(this.testDomain);
        toString.append("\n\t\tPre-split test Contexts: \n\t\t\t");
        toString.append(this.testContexts);
        return toString.toString();
    }
}
