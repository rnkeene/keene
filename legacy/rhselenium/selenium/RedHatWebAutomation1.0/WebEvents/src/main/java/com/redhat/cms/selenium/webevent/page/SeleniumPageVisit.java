package com.redhat.cms.selenium.webevent.page;

import com.redhat.cms.selenium.command.selenium.SeleniumOpen;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.thoughtworks.selenium.Selenium;

public abstract class SeleniumPageVisit extends WebPageVisit<Selenium> {

    /**
     *
     * Adds a Command to the list to open up the landing page.
     *
     */
    @Override
    public CompositeBrowserCommand<Selenium> build() {
        super.build();
        this.add(new SeleniumOpen(this.getLandingPage()));
        return this;
    }

    /**
     *
     * Closes the browser <B> in order to cleanup resources for the selenium server.
     *
     */
    @Override
    public void shutdownBrowser() {
        ((Selenium) this.getBrowser()).close();
        ((Selenium) this.getBrowser()).stop();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\t - Selenium Test Landing\n");
        toString.append(super.toString());
        return toString.toString();
    }
}
