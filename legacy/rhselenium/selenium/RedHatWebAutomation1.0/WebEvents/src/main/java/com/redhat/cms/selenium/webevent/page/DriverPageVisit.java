package com.redhat.cms.selenium.webevent.page;

import com.redhat.cms.selenium.command.driver.DriverOpen;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverPageVisit extends WebPageVisit<RemoteWebDriver> {

    static Logger log = Logger.getLogger(DriverPageVisit.class);

    @Override
    public CompositeBrowserCommand<RemoteWebDriver> build() {
        super.build();
        this.add(new DriverOpen(this.getLandingPage()));
        return this;
    }

    /**
     *
     * Closes the browser <B> in order to cleanup resources for the selenium server.
     *
     */
    @Override
    public void shutdownBrowser() {
        RemoteWebDriver browser = ((RemoteWebDriver) this.getBrowser());        
        if(browser!=null){
            browser.close();
        }
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\t - Driver Test Landing\n");
        toString.append(super.toString());
        return toString.toString();
    }
}
