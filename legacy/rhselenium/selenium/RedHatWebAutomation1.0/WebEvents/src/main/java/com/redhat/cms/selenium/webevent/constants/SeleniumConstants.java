package com.redhat.cms.selenium.webevent.constants;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.redhat.cms.selenium.browser.remote.selenium.factory.RemoteSeleniumFactory;
import com.thoughtworks.selenium.Selenium;
import org.apache.log4j.Logger;

public class SeleniumConstants extends BrowserConstants {

    static Logger log = Logger.getLogger(SeleniumConstants.class);

    @Override
    public Selenium getInstance(SeleniumHub seleniumServer) {
        return RemoteSeleniumFactory.getRemoteDriver(this.getBrowserName(), seleniumServer);
    }
}
