package com.redhat.cms.selenium.webevent.constants;

import com.redhat.cms.selenium.pattern.WrapperInitializer;
import java.util.HashMap;

public class WebConstantsMap extends HashMap<Class, WebConstants> implements WrapperInitializer<WebConstantsMap>,WebConstants {

    @Override
    public void initialize(WebConstantsMap input) {
        super.put(WebConstantsMap.class, input);
    }
}
