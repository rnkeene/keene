package com.redhat.cms.selenium.webevent.constants;

import com.redhat.cms.selenium.browser.remote.driver.factory.RemoteDriverFactory;
import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverConstants extends BrowserConstants{

    static Logger log = Logger.getLogger(DriverConstants.class);

    @Override
    public RemoteWebDriver getInstance(SeleniumHub seleniumServer) {
        return RemoteDriverFactory.getRemoteDriver(this.getBrowserName(), seleniumServer);
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t = Driver Constants - Remote Web Driver (2.0) =\n");
        toString.append(super.toString());
        return toString.toString();
    }

}