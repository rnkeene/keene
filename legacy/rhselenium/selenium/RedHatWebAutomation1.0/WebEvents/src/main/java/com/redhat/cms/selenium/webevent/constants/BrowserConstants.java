package com.redhat.cms.selenium.webevent.constants;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.redhat.cms.selenium.pattern.Factory;

public abstract class BrowserConstants implements WebConstants,Factory<Object, SeleniumHub> {

    private String browserName;

    public BrowserConstants() {
    }

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        if (this.browserName != null) {
            webConstants.put(BrowserConstants.class, this);
        }
    }
    
    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\t = Browser Constants =\n\t\tWeb Browser: \n\t\t\t");
        toString.append("\t\tBrowser Name: ");
        toString.append(this.browserName);
        return toString.toString();
    }
}
