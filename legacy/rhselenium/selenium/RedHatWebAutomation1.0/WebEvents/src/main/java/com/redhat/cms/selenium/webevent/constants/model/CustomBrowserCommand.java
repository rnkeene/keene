package com.redhat.cms.selenium.webevent.constants.model;

import com.redhat.cms.selenium.pattern.InputInitializer;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.redhat.cms.selenium.webevent.constants.BrowserConstants;
import com.redhat.cms.selenium.webevent.constants.HubConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import org.apache.log4j.Logger;

public abstract class CustomBrowserCommand<B> extends CompositeBrowserCommand<B> implements InputInitializer<WebConstantsMap>,WebConstants{

    static Logger log = Logger.getLogger(CustomBrowserCommand.class);

    /**
     * //Should be refactored into a Generic Browser object, over dealing with passing <B> around everywhere.
     * The <B> browser Object is finally initialized and created (opened!);
     *
     * @param webConstants
     */
    @Override
    public void initialize(WebConstantsMap webConstants) {
        BrowserConstants browserConstants = (BrowserConstants) webConstants.get(BrowserConstants.class);
        HubConstants hubConstants = (HubConstants) webConstants.get(HubConstants.class);
        this.setBrowser((B)browserConstants.getInstance(hubConstants.getSeleniumServer()));
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t = Web Constants =");
        return toString.toString();
    }
}
