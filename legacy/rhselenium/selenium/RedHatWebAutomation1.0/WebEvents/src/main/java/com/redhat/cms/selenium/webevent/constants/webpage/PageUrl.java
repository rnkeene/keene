package com.redhat.cms.selenium.webevent.constants.webpage;

import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;

public class PageUrl implements WebConstants {

    private String pageUrl;

    public PageUrl() {
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        if (this.pageUrl != null) {
            webConstants.put(PageUrl.class, this);
        }
    }
}
