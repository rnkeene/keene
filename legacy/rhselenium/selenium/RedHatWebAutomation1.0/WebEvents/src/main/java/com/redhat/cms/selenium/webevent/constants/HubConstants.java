package com.redhat.cms.selenium.webevent.constants;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;

public class HubConstants implements WebConstants {

    private String connectionProtocol;
    private String serverPort;
    private String serverDomain;
    private String registerContext;

    public HubConstants() {
    }

    public HubConstants(String connectionProtocol, String serverPort, String serverURL, String registerContext) {
        this.connectionProtocol = connectionProtocol;
        this.serverPort = serverPort;
        this.serverDomain = serverURL;
        this.registerContext = registerContext;
    }

    public String getConnectionProtocol() {
        return connectionProtocol;
    }

    public void setConnectionProtocol(String connectionProtocol) {
        this.connectionProtocol = connectionProtocol;
    }

    public String getRegisterContext() {
        return registerContext;
    }

    public void setRegisterContext(String registerContext) {
        this.registerContext = registerContext;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public String getServerURL() {
        return serverDomain;
    }

    public void setServerURL(String serverURL) {
        this.serverDomain = serverURL;
    }

    public SeleniumHub getSeleniumServer() {
        return new SeleniumHub(this.connectionProtocol, this.serverDomain, this.registerContext, this.serverPort);
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        if (this.serverPort != null && this.serverDomain != null && this.registerContext != null) {
            webConstants.put(HubConstants.class, this);
        }
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\t - Hub Constants:\n\t\tServer Port: ");
        toString.append(serverPort);
        toString.append("\n\t\tServer URL: ");
        toString.append(serverDomain);
        toString.append("\n\t\tRegistration Context: ");
        toString.append(registerContext);
        toString.append("\n\t\tServer Protocol: ");
        toString.append(connectionProtocol);
        return toString.toString();
    }
}
