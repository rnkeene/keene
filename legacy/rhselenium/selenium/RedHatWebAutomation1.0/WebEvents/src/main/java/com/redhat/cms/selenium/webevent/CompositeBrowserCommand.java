package com.redhat.cms.selenium.webevent;

import com.redhat.cms.selenium.browser.WebsiteBrowser;
import com.redhat.cms.selenium.command.browser.BrowserCommand;
import com.redhat.cms.selenium.command.browser.pattern.CompositeCommand;
import org.apache.log4j.Logger;

/**
 *
 * CompositeBrowserCommand extends the capabilities of the CompositeCommand  by providig a container for the browser Object.
 *
 * In order to manage the browser the following responsibilities have been inherited: provide browser { accessor, mutator, startup and shutdown } capabilities.
 *
 * This abstract class should be extended for each concrete Browser implementation.//until refactored.
 *
 * By default the BrowserCommand does not Override the -C build()- to add any additional command.//until refactored.
 *
 * @param <B> the browser driver that will be responsible for the communication with the actual web browser.
 */
public abstract class CompositeBrowserCommand<B> extends CompositeCommand<B, CompositeBrowserCommand<B>, BrowserCommand<B>> implements WebsiteBrowser<B> {

    static Logger log = Logger.getLogger(CompositeBrowserCommand.class);
    /**
     *
     * The <B> browser Object that should be useable by the commands loaded into this CompositeCommand.
     *
     */
    private B browser;

    protected CompositeBrowserCommand() {
    }

    /**
     *
     * The build method has been overridden to prevent the default CompositeCommand build method from clearing out the data.
     *
     * The startupBrowser() method has added a Command that needs to remain in the list.
     *
     * @return
     */
    @Override
    public CompositeBrowserCommand<B> build() {
        super.setTarget(null);
        return this;
    }

    /**
     *
     * The BrowserCommand.execute(B browser) method has three responsibilities:
     *
     *  1. Set the parameter <B> browser to the local variable this.browser.
     *
     *  2. Run the -super.execute(this.getBrowser());- method.
     *
     *  3. Run the -this.shutdownBrowser()- command to cleanup the resources related to the <B> browser Object if not done so by the Test.
     *
     * @param browser the browser that is going to be used to execute the Commands held by this Object.
     */
    @Override
    public void execute(B browser) {
        this.setBrowser(browser);
        CompositeBrowserCommand<B> build = this.build();
        try {
            if (build == this) {
                super.execute(this.browser);
            } else {
                build.execute(browser);
            }
            this.setTarget(build.getResults());
        } catch (Exception ex) {
            ex.printStackTrace();
            assert false : "The BrowserCommand failed to execute due to an exception - <br />Command: " + this.toString();
        }finally{
            this.shutdownBrowser();
        }
    }

    /**
     *
     * Default Accessor for the <B> browser Object.
     *
     * @return <B> the browser driver being used to execute Commands.
     */
    @Override
    public B getBrowser() {
        return this.browser;
    }

    /**
     *
     * Default Mutator for the <B> browser Object.
     *
     */
    @Override
    public void setBrowser(B browser) {
        this.browser = browser;
    }
}
