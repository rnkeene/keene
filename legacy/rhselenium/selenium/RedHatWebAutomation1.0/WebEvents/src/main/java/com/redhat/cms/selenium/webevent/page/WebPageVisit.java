package com.redhat.cms.selenium.webevent.page;

import com.redhat.cms.selenium.webevent.constants.model.CustomBrowserCommand;
import org.apache.log4j.Logger;

public abstract class WebPageVisit<B> extends CustomBrowserCommand<B>{

    static Logger log = Logger.getLogger(WebPageVisit.class);

    private String landingPage;
    
    public String getLandingPage() {
        return this.landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\t - Web Page Visit - \n\t\tLanding Page: ");
        toString.append(this.landingPage);
        toString.append("\n");
        toString.append(super.toString());
        return toString.toString();
    }

}
