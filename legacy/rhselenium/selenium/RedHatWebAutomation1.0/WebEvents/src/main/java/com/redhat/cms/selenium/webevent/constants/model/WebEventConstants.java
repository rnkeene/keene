package com.redhat.cms.selenium.webevent.constants.model;

import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;

public class WebEventConstants extends WebConstantsMap implements WebConstants {

    public WebEventConstants() {
    }

    @Override
    public void initialize(WebConstantsMap input) {
        super.initialize(input);
        input.put(WebConstants.class, this);
    }
}
