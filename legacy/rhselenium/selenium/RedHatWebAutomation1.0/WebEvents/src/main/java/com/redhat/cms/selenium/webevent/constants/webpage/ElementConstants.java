package com.redhat.cms.selenium.webevent.constants.webpage;

import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;

public class ElementConstants implements WebConstants {

    @Override
    public void initialize(WebConstantsMap webConstantsMap) {
        webConstantsMap.put(ElementConstants.class, this);
    }
 
}
