package com.redhat.cms.selenium.webevent.gluster;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GlusterTest{

    static Logger log = Logger.getLogger(GlusterTest.class);

    @BeforeClass
    public void start(){
        log.info("GlusterTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("GlusterTest.setUp()");
        assert true;
    }

    @Test
    public void testCommand(){
        log.info("GlusterTest.testCommand()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("GlusterTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("GlusterTest.finish()");
        assert true;
    }
    
}
