package com.redhat.cms.selenium.webevent.omniture;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class EloquaTest{

    static Logger log = Logger.getLogger(EloquaTest.class);

    @BeforeClass
    public void start(){
        log.info("EloquaTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("EloquaTest.setUp()");
        assert true;
    }

    @Test
    public void testCommand(){
        log.info("EloquaTest.testCommand()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("EloquaTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("EloquaTest.finish()");
        assert true;
    }
    
}
