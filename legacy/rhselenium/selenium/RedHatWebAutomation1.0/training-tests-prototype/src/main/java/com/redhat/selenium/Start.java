package com.redhat.selenium;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

public class Start {
	public static void main(String[] args) {

		XmlSuite suite = new XmlSuite();
		suite.setName("TmpSuite");
		
		if(args.length == 1){
			suite.setParallel("methods");
			suite.setDataProviderThreadCount(Integer.parseInt(args[0]));
		}

		XmlTest test = new XmlTest(suite);
		test.setName("TmpTest");
		List<XmlClass> classes = new ArrayList<XmlClass>();
		classes.add(new XmlClass("com.redhat.selenium.CoursesContentCheckTest"));
		test.setXmlClasses(classes) ;
		
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG tng = new TestNG();
		tng.setXmlSuites(suites);
		tng.run(); 
	}
}
