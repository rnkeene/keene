package com.redhat.selenium;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.time.StopWatch;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.redhat.cms.selenium.browser.remote.driver.factory.RemoteDriverFirefoxFactory;
import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.redhat.selenium.training.courses.dataprovider.RegionalCourseLocaleDataProvider;
import com.redhat.www.reports.training.TrainingCoursesReport;



@Test
public class CoursesContentCheckTest {

	
	private TrainingCoursesReport report ;
	private static Logger log = LoggerFactory.getLogger(CoursesContentCheckTest.class);
	private TenaciousBrowserFactoryWrapper<RemoteWebDriver> driverFactory;
	private   String DOC_LOCATION;
	private StopWatch sw;
	private int completedCount;

	
	@BeforeClass
	public void setup(){
		
		//Scratch code ***
		DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
		DOC_LOCATION=String.format("/home/dhenry/testCourseContent_%s.ods",df.format(new Date()));
		report = new TrainingCoursesReport(DOC_LOCATION);
		//SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","127.0.0.1","/wd/hub","4444");
		SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","qa-grid.devlab.phx1.redhat.com","/wd/hub","4445");
		//SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","ec2-50-16-153-37.compute-1.amazonaws.com","/wd/hub","4444");
		//SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","djhenry:afdfcf41-14c1-4d85-b101-772589ae1b65@ondemand.saucelabs.com","/wd/hub","80");
		driverFactory = new TenaciousBrowserFactoryWrapper<RemoteWebDriver>(new RemoteDriverFirefoxFactory(hub));
		sw=new StopWatch();
		sw.start();
		completedCount=0;
		//end scratch code ***
	}
	

	
	@Test(dataProvider="TrainingCoursesDataProvider",dataProviderClass=RegionalCourseLocaleDataProvider.class)
	public void testCourseContent( String region, String locale, String courseName){

		WebDriver driver = driverFactory.getInstance();
		// Get current time

	
		long start= System.currentTimeMillis(); 
		boolean testComplete = false;
		int attempt = 0;
		while(!testComplete && 5 > attempt){
			attempt++;
			try{
				TestLocaleCourseCommand cmd = new TestLocaleCourseCommand(courseName,locale);
				cmd.setReport(report);
				cmd.execute(driver);
				//save after every course
				report.saveToDisk(DOC_LOCATION);
				report.appendDatapoint(locale, courseName, "Test Status", "Done");
				testComplete = true;
			}
			catch(Exception e){
				//report.appendDatapoint("Runtime Errors", locale+" "+courseName+" Attempt#"+attempt, "Test Status", "Error"+e.getMessage());
				report.appendDatapoint(locale, courseName, "Test Status", "Error, retry attempt "+attempt);
				report.saveToDisk(DOC_LOCATION);
				//throw new RuntimeException(e);
				try{driver.quit();}
				catch(Exception ee){
					//ignore
				}
				driver = driverFactory.getInstance();
			}
		}
		
		// Get elapsed time in milliseconds
		long elapsedTimeMillis = System.currentTimeMillis()-start;
		// Get elapsed time in seconds
		Float elapsedTimeSec = elapsedTimeMillis/1000F;
		report.appendDatapoint(locale, courseName, "Test Execution Time (seconds)", elapsedTimeSec.toString());
		
		//*** more hack
			completedCount++;
			float minutesSoFar =sw.getTime()/60000F;
			float tpm = completedCount/minutesSoFar;
			log.info("Current tests per minute: "+tpm);
			log.info("Last test: "+elapsedTimeSec.toString());
		//***
		
		
		report.saveToDisk(DOC_LOCATION);
	
		driver.quit();
	}

}
