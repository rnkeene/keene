package com.redhat.cms.content.courses.test;

import org.apache.commons.lang.time.StopWatch;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import com.redhat.cms.selenium.browser.factory.BrowserFactory;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.test.RedHatTest;
import com.redhat.selenium.training.courses.dataprovider.RegionalCourseLocaleDataProvider;


public class CourseTest extends RedHatTest  {
	static final Logger log = LoggerFactory.getLogger(RedHatTest.class);
	private BrowserFactory<RemoteWebDriver> driverFactory;

	/**
	 * 
	 * @param driverFactory a driver factory for with which to create instance of browser
	 */
    public CourseTest(
			BrowserFactory<RemoteWebDriver> driverFactory) {
		this.driverFactory = driverFactory;
	}

	/**
    *
    * Provides a way to put off some of the initialization required for this test, to be run later (Just in Time).
    *
    * @param input an instance of the Test Case data.
    */
   @Override
   public void initialize(RedHatCompositeData redHatData) {
       super.initialize(redHatData);
       redHatData.put(CourseTest.class, this);
   }

   /**
    *
    * Each @Test from a @Factory will use a thread from the thread-pool to run the @Tests concurrently; limited by either the @Factory's @Test List size or the thread-pool set by TestNG.
    *
    * For each item returned from the @Test dataProvider reference the same rules will apply as to the @Factory, although instead of using the thread-pool it will use the data-provider-thread-pool.
    *
    * Each thread will simultaneously access the @Test method within the Same Instance of @Test Object; so it is important to consider whether or not this class/method is capable of being executed in a multithreaded way.
    *
    * It is possible to toggle - parallel=[true|false] - within the @DataProvider() parameters.
    *
    * @param runningCase is one Object from the list of data returned from the dataProvider.
    * 
    */
//	@Test(dataProvider="TrainingCoursesDataProvider",dataProviderClass=RegionalCourseLocaleDataProvider.class)
//	public void runTestMethod(CourseTestCase testCase) {
//		StopWatch stopWatch = new StopWatch();
//		stopWatch.start();
//		String locale = testCase.getLocale();
//		String courseName = testCase.getCourseName();
//		CourseTestResult result = new CourseTestResult(testCase);
//		WebDriver driver = driverFactory.getInstance();
//		boolean testComplete = false;
//		
//		int attempt = 0;
//		while(!testComplete && 5 > attempt){
//			attempt++;
//			try{
//				TestLocaleCourseCommand cmd = new TestLocaleCourseCommand(courseName,locale);				
//				cmd.execute(driver);
//				result.appendResult((CourseTestResult) cmd.getResult());
//				result.appendDatapoint("Test Status", "Done");
//				testComplete = true;
//			}
//			catch(Exception e){
//				//report.appendDatapoint("Runtime Errors", locale+" "+courseName+" Attempt#"+attempt, "Test Status", "Error"+e.getMessage());
//				result.appendDatapoint( "Test Status", "Error, retry attempt "+attempt);
//				//throw new RuntimeException(e);
//				try{driver.quit();}
//				catch(Exception ee){
//					//ignore
//				}
//				driver = driverFactory.getInstance();
//			}
//		}
//		
//		// Get elapsed time in milliseconds
//		long elapsedTimeMillis = stopWatch.getTime();
//		// Get elapsed time in seconds
//		Float elapsedTimeSec = elapsedTimeMillis/1000F;
//		result.appendDatapoint("Test Execution Time (seconds)", elapsedTimeSec.toString());
//
//		result.saveToDisk(DOC_LOCATION);
//	
//		driver.quit();
//	}
//   
   
   @Override
   public String toString() {
       StringBuilder toString = new StringBuilder();
       toString.append(" - TrainingCourseContentTest - ");
       return toString.toString();
   }

}