package com.redhat.cms.content.courses.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.www.page.courses.CourseDetailPage;
import com.redhat.www.page.courses.CoursesPage;
import com.redhat.www.page.courses.TabWidget;

public class TestLocaleCourseCommand  implements Command<WebDriver,TestLocaleCourseCommand>{
	private static Logger log = LoggerFactory.getLogger(TestLocaleCourseCommand.class);
	private String courseName;
	private String locale;
	private String baseUrl = "http://www.redhat.com/";
	private CourseTestResult result;
	
	public TestLocaleCourseCommand(CourseTestCase testCase){
		this.courseName = testCase.getCourseName();
		this.locale = testCase.getLocale();
		this.result = new CourseTestResult(testCase);
	}
		


	public void execute(WebDriver driver) throws Exception {
		//Step1: Set locale
		driver.get(String.format("%ssetCountryLanguage?localeValue=%s",baseUrl,locale));
		
		
		//Step2: Load /training/courses/ course list page
		driver.get(baseUrl+"/training/courses/");
		
		log.debug(String.format("*** Course: %s Locale: %s ***",courseName,locale));
		
		CoursesPage coursesPage = new CoursesPage(driver);
		coursesPage.navToPage("1");
		while(!coursesPage.checkForCourse(courseName) && coursesPage.hasNextPage()){
			//result.appendDatapoint("Listed?", String.format("Page %s: No",coursesPage.getCurrentPageNumber()));
			coursesPage.navToNextPage();
		}
		if(coursesPage.checkForCourse(courseName)){
			result.appendDatapoint("Listed?", String.format("Page %s: YES",coursesPage.getCurrentPageNumber()));
		}
		else{
			result.appendDatapoint("Listed?", String.format("Page %s: No",coursesPage.getCurrentPageNumber()));
		}
		

		doIndividualCourseTestProcedure(driver, locale,courseName);



	}
	
	public CourseTestResult getResult(){
		return result;
	}

	/**
	 * TODO:refactor
	 * @param coursePage
	 */
	private void doIndividualCourseTestProcedure(WebDriver driver, String locale, String courseName) {
		//Load individual course page
		driver.get(baseUrl+String.format("training/courses/%s/",courseName.toLowerCase()));

		CourseDetailPage coursePage = new CourseDetailPage(driver);
		
		//check that detail page is visible
		if(coursePage.isErrorNotFound()){
			result.appendDatapoint("Detail Page Visible?", "No (404 Error)");
		}
		else{
			result.appendDatapoint("Detail Page Visible?", "Yes");
		}
		
		//check for "Enroll Now" button
		List<WebElement> enrollNowButtons = driver.findElements(By.cssSelector("div.enr-today-btn>a"));
		if(enrollNowButtons.size()>0){
			String buttonUrl = enrollNowButtons.get(0).getAttribute("href");
			log.debug("Enroll Now URL: "+buttonUrl);
			result.appendDatapoint("Enroll Now Button", buttonUrl);
			
		}
		else {
			log.debug("X - Enroll Now Button MISSING!");
			result.appendDatapoint("Enroll Now Button", "MISSING!");
		}
		
		if(locale.contains("en")){
			result.appendDatapoint("Translated?", "N/A");
		}
		else if(coursePage.isTranslated()){
			result.appendDatapoint("Translated?", "Yes");
		}
		else{
			result.appendDatapoint("Translated?", "No");
		}
		
		try {
			//check the tab widgets		
			TabWidget tabWidget = coursePage.getCourseDetailTabWidget();
			log.debug("Price: "+coursePage.getCoursePrice());
			result.appendDatapoint("Price", coursePage.getCoursePrice());
			
			for(String tabHeaderText : tabWidget.getTabHeaderTextList()){
				
				String tabItemText = tabWidget.selectTabHeader(tabHeaderText).getText();
				//make sure each tab is present and does not contain the word "null"
				if(tabItemText.contains("null")){
					log.debug(String.format("X - found 'null' in tab '%s'",tabHeaderText));
					result.appendDatapoint("Content", String.format("Found 'null' in tab '%s'",tabHeaderText));
				}
				else if(tabItemText.isEmpty()){
					log.debug(String.format("X - found no content in tab '%s'",tabHeaderText));
					result.appendDatapoint("Content", String.format("Found no content in tab '%s'",tabHeaderText));
				}
				
			}
		}
		catch(org.openqa.selenium.TimeoutException e){
			log.debug("X - Course detail tab widget appears to be missing.");
			result.appendDatapoint("Content", "Course detail tab widget appears to be missing.");

		}
	}
}
