package com.redhat.cms.content.courses.test;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.data.test.RedHatTestData;

public class CourseTestCase extends RedHatTestData {

	private static final long serialVersionUID = 1L;

	private String region;
	private String locale;
	private String courseName;

	public CourseTestCase() {

	}

	public CourseTestCase(String region, String locale, String courseName) {
		this.region = region;
		this.locale = locale;
		this.courseName = courseName;
	}

	@Override
	public void initialize(RedHatCompositeData input) {
		super.initialize(input);
		input.put(CourseTestCase.class, this);
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	@Override
	public String toString() {
		return "CourseTestCase [region=" + region + ", locale=" + locale
				+ ", courseName=" + courseName + "]";
	}
}