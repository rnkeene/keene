import junit.framework.Assert;

import org.testng.annotations.Test;

import com.redhat.selenium.training.courses.dataprovider.RegionalCourseLocaleDataProvider;


public class TestRegionalCourseLocaleDataProvider {

	/**
	 * Assert the following:
	 * createData runs without error.
	 * 2d Object array returned is not empty
	 * 2d Object array has exactly 3 elements in every row
	 * no value in the 2d Object array is null
	 */
	@Test
	public void testCreateData(){
		Object[][] result = RegionalCourseLocaleDataProvider.createData();
		Assert.assertTrue(result.length >0);
		for(int i=0;i<result.length;i++){
			Assert.assertEquals(3, result[i].length);
			for(int j=0;j<result[i].length;j++){
				Assert.assertNotNull(result[i][j]);
			}
		}
	}
}
