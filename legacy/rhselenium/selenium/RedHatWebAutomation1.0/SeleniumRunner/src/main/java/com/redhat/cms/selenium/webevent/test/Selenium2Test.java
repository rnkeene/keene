package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Selenium2Test extends SeleniumTest<RemoteWebDriver> {

    static Logger log = Logger.getLogger(Selenium2Test.class);

    @Override
    public void initialize(RedHatCompositeData input) {
        super.initialize(input);
    }

//    @Override
//    public void shutdown() {
//        RemoteWebDriver driver = (RemoteWebDriver) this.webEvent.getBrowser();
//        if (driver != null) {
//            driver.close();
//            driver.quit();
//        }
//    }
}
