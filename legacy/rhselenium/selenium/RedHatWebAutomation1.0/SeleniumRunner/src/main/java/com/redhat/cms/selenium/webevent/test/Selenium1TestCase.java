package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.constants.SeleniumConstants;
import com.redhat.cms.selenium.webevent.data.BrowserProperties;
import org.apache.log4j.Logger;

public abstract class Selenium1TestCase extends SeleniumTestCase {

    static Logger log = Logger.getLogger(Selenium1TestCase.class);

    @Override
    public void initialize(RedHatCompositeData input) {
        super.initialize(input);
        BrowserProperties browser = new BrowserProperties(new SeleniumConstants());
        browser.initialize(input);
    }
}
