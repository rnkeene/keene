package com.redhat.cms.selenium.webevent.data;

import com.redhat.cms.selenium.webevent.constants.HubConstants;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import java.util.Properties;
import org.apache.log4j.Logger;

public class HubProperties extends FilePropertiesData {

    static Logger log = Logger.getLogger(HubProperties.class);

    private static final String SERVER_URL = "serverUrl";
    private static final String SERVER_REGISTER_CONTEXT = "serverRegisterContext";
    private static final String SERVER_PORT = "serverPort";
    private static final String SERVER_PROTOCOL = "serverProtocol";

    private HubConstants constants = new HubConstants();

    public HubProperties() {
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        Properties properties = ((FilePropertiesData) input.get(PropertiesData.class)).getProperties();
        this.constants.setConnectionProtocol(properties.getProperty(SERVER_PROTOCOL));
        this.constants.setServerPort(properties.getProperty(SERVER_PORT));
        this.constants.setServerURL(properties.getProperty(SERVER_URL));
        this.constants.setRegisterContext(properties.getProperty(SERVER_REGISTER_CONTEXT));
        this.constants.initialize( ((SeleniumTestConstantsMap)input).getWebConstantsMap());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tHub Properties: \n");
        toString.append(super.toString());
        return toString.toString();
    }

}
