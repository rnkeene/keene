package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.datasource.RedHatData;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import org.apache.log4j.Logger;

public abstract class SeleniumTestSuite extends RedHatTestSuite implements RedHatData {

    static Logger log = Logger.getLogger(SeleniumTestSuite.class);
}
