package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.constants.DriverConstants;
import com.redhat.cms.selenium.webevent.data.BrowserProperties;
import org.apache.log4j.Logger;

public abstract class Selenium2TestCase extends SeleniumTestCase{

    static Logger log = Logger.getLogger(Selenium2TestCase.class);

    @Override
    public void initialize(RedHatCompositeData input) {
        super.initialize(input);
        BrowserProperties browser = new BrowserProperties(new DriverConstants());
        browser.initialize(input);
    }

}
