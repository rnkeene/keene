package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.data.HubProperties;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.datasource.RedHatData;
import com.redhat.cms.testcore.data.RedHatTestCase;
import org.apache.log4j.Logger;

public abstract class SeleniumTestCase extends RedHatTestCase implements SeleniumTestConstants, RedHatData {

    static Logger log = Logger.getLogger(SeleniumTestCase.class);
    private SeleniumTestConstantsMap constants;

    @Override
    public void initialize(RedHatCompositeData input) {
        super.initialize(input);
        this.constants = (SeleniumTestConstantsMap) input;
        HubProperties serverProperties = new HubProperties();
        serverProperties.initialize(this.constants);
        this.constants.put(SeleniumTestConstantsMap.class, this);
    }

    @Override
    public WebConstantsMap build() {
        return (WebConstantsMap) this.constants.build();
    }
}
