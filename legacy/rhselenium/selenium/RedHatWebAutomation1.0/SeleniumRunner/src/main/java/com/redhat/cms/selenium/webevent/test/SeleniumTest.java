package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.constants.model.CustomBrowserCommand;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.data.RedHatTestCase;
import com.redhat.cms.testcore.test.RedHatTest;
import org.apache.log4j.Logger;

/**
 *
 * SeleniumTest connects the Selenium Web Event Framework with the TestNG Model framework.
 */
public abstract class SeleniumTest<B> extends RedHatTest {

    static Logger log = Logger.getLogger(SeleniumTest.class);
    protected CustomBrowserCommand webEvent;

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        super.initialize(redHatData);
        WebConstantsMap webConstants = ((SeleniumTestCase) redHatData).build();
        this.webEvent.initialize(webConstants);
    }

    /**
     * 
     * @Test(dataProvider = "next", dataProviderClass = SeleniumTestCaseFactory.class)
     *
     * @param testCase
     */
    @Override
    public void runTestMethod(RedHatTestCase redHatTest) {
        this.initialize(redHatTest);
        this.webEvent.execute(webEvent.getBrowser());
    }

    public CustomBrowserCommand getWebEvent() {
        return webEvent;
    }

    public void setWebEvent(CustomBrowserCommand webEvent) {
        this.webEvent = webEvent;
    }
}
