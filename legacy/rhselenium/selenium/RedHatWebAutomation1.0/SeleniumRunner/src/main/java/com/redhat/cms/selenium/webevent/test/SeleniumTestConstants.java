package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.pattern.Builder;
import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.datasource.RedHatData;

public interface SeleniumTestConstants extends RedHatData, Builder<WebConstants>{
}
