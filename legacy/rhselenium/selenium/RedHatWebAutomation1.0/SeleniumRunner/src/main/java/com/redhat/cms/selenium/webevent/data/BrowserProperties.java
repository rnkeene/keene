package com.redhat.cms.selenium.webevent.data;

import com.redhat.cms.selenium.webevent.constants.BrowserConstants;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import java.util.Properties;
import org.apache.log4j.Logger;

public class BrowserProperties<B extends BrowserConstants> extends FilePropertiesData {

    static Logger log = Logger.getLogger(BrowserProperties.class);
    public static final String BROWSERS = "browser";
    private B browserDriver;

    public BrowserProperties(B browserDriver) {
        this.browserDriver = browserDriver;
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        Properties properties = ((FilePropertiesData) input.get(PropertiesData.class)).getProperties();
        this.browserDriver.setBrowserName(properties.getProperty(BROWSERS));
        this.browserDriver.initialize(((SeleniumTestConstantsMap) input).getWebConstantsMap());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("== Browser Properties ==\n");
        toString.append(super.toString());
        toString.append("\n\tBrowser Driver: ");
        toString.append(this.browserDriver.toString());
        return toString.toString();
    }
}
