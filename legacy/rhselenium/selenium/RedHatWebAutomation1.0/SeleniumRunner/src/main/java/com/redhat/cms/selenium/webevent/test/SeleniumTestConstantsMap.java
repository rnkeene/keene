package com.redhat.cms.selenium.webevent.test;

import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import org.apache.log4j.Logger;

public class SeleniumTestConstantsMap extends RedHatCompositeData implements SeleniumTestConstants {

    static Logger log = Logger.getLogger(SeleniumTestConstantsMap.class);

    private WebConstantsMap webConstantsMap = new WebConstantsMap();

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(RedHatCompositeData.class, this);
    }

    @Override
    public WebConstants build() {
        webConstantsMap.initialize(webConstantsMap);
        return this.webConstantsMap;
    }

    public WebConstantsMap getWebConstantsMap() {
        return webConstantsMap;
    }
}
