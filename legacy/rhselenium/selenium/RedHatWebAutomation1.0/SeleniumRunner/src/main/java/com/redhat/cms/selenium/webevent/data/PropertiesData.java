package com.redhat.cms.selenium.webevent.data;

import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import java.util.Properties;

public class PropertiesData extends SeleniumSuiteData{

    private Properties properties;

    public PropertiesData() {
        this.properties = new Properties();
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(PropertiesData.class, this);
    }
}
