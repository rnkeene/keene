package com.redhat.cms.selenium.webevent.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.datasource.RedHatData;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class FilePropertiesData extends PropertiesData implements RedHatData{

    private String pathToProperties;

    public FilePropertiesData() {
    }

    public FilePropertiesData(String properties) {
        this.pathToProperties = properties;
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        super.initialize(input);
        this.initialize();
    }

    public void initialize(){
        this.clear();
        FileInputStream in = null;
        try {
            in = new FileInputStream(this.pathToProperties);
            this.getProperties().load(in);
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static FilePropertiesData getInstance(String filePath){
        FilePropertiesData file = new FilePropertiesData(filePath);
        file.initialize();
        return file;
    }

    public static List<String> splitString(String input, String deliminator) {
        String[] stringArray = input.split(deliminator);
        List<String> strings = Arrays.asList(stringArray);
        return strings;
    }

    @Override
    public String toString(){
        StringBuilder toString = new StringBuilder();
        toString.append("\tProperties File path: <br /><br />");
        toString.append(this.pathToProperties);
        Set<Object> keys = this.getProperties().keySet();
        for(Object key : keys){
            toString.append("<br /><br />\n\t\t - Key: ");
            toString.append(key);
            toString.append("<br />\n\t\t\t  - Value: ");
            Object value = this.getProperties().get(key);
            toString.append(value);
        }
        return toString.toString();
    }
}
