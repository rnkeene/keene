package com.redhat.cms.selenium.webevent.test.factory;

import com.redhat.cms.selenium.webevent.test.SeleniumTest;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import com.redhat.cms.testcore.test.factory.RedHatTestFactory;
import org.apache.log4j.Logger;

public abstract class SeleniumTestFactory<T extends SeleniumTest> extends RedHatTestFactory<T> {

    static final Logger log = Logger.getLogger(SeleniumTestFactory.class);

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        super.initialize(redHatData);
        redHatData.put(SeleniumTestFactory.class, this);
    }

    /**
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @Factory(dataProvider = "suiteData", dataProviderClass = MockRedHatSuiteCase.class)
     *
     *
     * @return
     *
     */
    @Override
    public Object[] getTests(RedHatTestSuite factorySuite) {
        return super.getTests(factorySuite);
    }

    /**
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Override
    public Object[] build() {
        return this.toArray();
    }
}
