package com.redhat.cms.selenium.webevent.data;

import com.redhat.cms.selenium.pattern.Factory;
import com.redhat.cms.selenium.webevent.constants.WebConstants;
import com.redhat.cms.selenium.webevent.constants.webpage.URLList;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstants;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import java.util.Properties;

public class URLProperties extends PropertiesData implements SeleniumTestConstants, Factory<URLProperties,Properties> {

    private static final String TEST_PROTOCOL = "urlProtocol";
    private static final String TEST_DOMAIN = "urlDomain";
    private static final String TEST_CONTEXTS = "urlContexts";

    private URLList urlConstants;

    public URLProperties() {
        this.urlConstants = new URLList();
    }

    public void initialize(Properties properties){
        this.urlConstants.setTestProtocol(properties.getProperty(TEST_PROTOCOL));
        this.urlConstants.setTestDomain(properties.getProperty(TEST_DOMAIN));
        this.urlConstants.setTestContexts(properties.getProperty(TEST_CONTEXTS));
    }

    public void initialize(SeleniumTestConstantsMap input) {
        Properties properties = ((FilePropertiesData) input.get(PropertiesData.class)).getProperties();
        this.initialize(properties);
        this.urlConstants.initialize(input.getWebConstantsMap());
    }

    @Override
    public WebConstants build() {
        urlConstants.build();
        return urlConstants;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tURL Properties: \n");
        toString.append(super.toString());
        return toString.toString();
    }

    @Override
    public URLProperties getInstance(Properties input) {
        URLProperties properties = new URLProperties();
        properties.initialize(input);
        return properties;
    }
}
