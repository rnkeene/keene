package com.redhat.cms.selenium.webevent.eloqua.data.factory;

import com.redhat.cms.selenium.webevent.data.factory.SeleniumTestCaseFactory;
import com.redhat.cms.selenium.webevent.eloqua.data.EloquaTestCase;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import java.util.List;
import org.apache.log4j.Logger;

public class EloquaTestCaseFactory extends SeleniumTestCaseFactory {

    static Logger log = Logger.getLogger(EloquaTestCaseFactory.class);
    private List<String> factoryList;

    public EloquaTestCaseFactory() {
    }

    @Override
    public List build() {
        super.build();
        for (String url : factoryList) {
            EloquaTestCase testCase = new EloquaTestCase("");
            testCase.initialize(new SeleniumTestConstantsMap());
            testCase.setPageUrl(url);           
            this.add(testCase);
        }
        return this;
    }

    public List<String> getFactoryList() {
        return factoryList;
    }

    public void setFactoryList(List<String> factoryList) {
        this.factoryList = factoryList;
    }
}
