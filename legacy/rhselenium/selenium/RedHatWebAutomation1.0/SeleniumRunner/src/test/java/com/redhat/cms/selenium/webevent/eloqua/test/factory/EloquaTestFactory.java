package com.redhat.cms.selenium.webevent.eloqua.test.factory;

import com.redhat.cms.selenium.webevent.eloqua.dataprovider.EloquaTestCaseProvider;
import com.redhat.cms.selenium.webevent.constants.webpage.URLList;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.webevent.data.URLProperties;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.test.factory.Selenium2TestFactory;
import com.redhat.cms.selenium.webevent.eloqua.dataprovider.EloquaTestSuiteProvider;
import com.redhat.cms.selenium.webevent.eloqua.test.EloquaScriptCheck;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

public class EloquaTestFactory extends Selenium2TestFactory {

    static Logger log = Logger.getLogger(EloquaTestFactory.class);
    FilePropertiesData testData;

    @Factory(dataProvider = "getTestSuiteData", dataProviderClass = EloquaTestSuiteProvider.class)
    public Object[] getTestData(SeleniumSuiteData testData) {
        this.testData = (FilePropertiesData) testData;
        return this.build();
    }

    @Override
    public Object[] build() {
        URLProperties properties = new URLProperties();
        properties.initialize((Properties)this.testData.getProperties());
        URLList constants = (URLList) properties.build();
        List<String> urlList = constants.getUrls();
        EloquaTestCaseProvider.add(urlList);
        this.add(new EloquaScriptCheck());
        return this.toArray();
    }
}
