package com.redhat.cms.selenium.webevent.cloudready.test;

import com.redhat.cms.selenium.webevent.cloudready.data.CloudTest;
import com.redhat.cms.selenium.webevent.cloudready.dataprovider.CloudreadyTestCaseProvider;
import com.redhat.cms.selenium.webevent.constants.webpage.URLList;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.URLProperties;
import com.redhat.cms.selenium.webevent.test.Selenium2Test;
import com.redhat.cms.testcore.data.RedHatTestCase;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class CloudReadyScriptCheck extends Selenium2Test {

    private static final String LANDING_PAGE;
    static final Logger log = Logger.getLogger(CloudReadyScriptCheck.class);

    static {
        FilePropertiesData data = FilePropertiesData.getInstance("src/test/java/com/redhat/cms/selenium/webevent/cloudready/test/testUrl.properties");
        URLProperties properties = new URLProperties();
        properties.initialize(data.getProperties());
        URLList constants = (URLList) properties.build();
        LANDING_PAGE = constants.getUrls().get(0);
    }


    /**
     *
     * @Test(threadPoolSize=n) will be controlled by the testng.xml configurations.
     * 
     * @param testCase
     */
    @Test(dataProvider = "next", dataProviderClass = CloudreadyTestCaseProvider.class)
    @Override
    public void runTestMethod(RedHatTestCase testCase) {
        log.info("RUNNING CLOUD READY SCRIPT CHECK!");
        log.info("\tLANDING PAGE!! " + LANDING_PAGE);
        CloudTest test = new CloudTest();
        test.setLandingPage(LANDING_PAGE);
        this.setWebEvent(test);
        super.runTestMethod(testCase);
    }
}
