package com.redhat.cms.selenium.webevent.cloudready.data;

import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;

public class CloudTestData extends SeleniumSuiteData {

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(CloudTestData.class, this);
    }
}
