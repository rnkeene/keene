package com.redhat.cms.selenium.webevent.eloqua.test;

import com.redhat.cms.selenium.webevent.eloqua.test.factory.EloquaTestFactory;
import com.redhat.cms.selenium.webevent.eloqua.data.EloquaTestCase;
import com.redhat.cms.selenium.webevent.eloqua.dataprovider.EloquaTestCaseProvider;
import com.redhat.cms.selenium.webevent.test.Selenium2Test;
import com.redhat.cms.selenium.webevent.eloqua.data.EloquaScriptTest;
import com.redhat.cms.testcore.data.RedHatTestCase;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class EloquaScriptCheck extends Selenium2Test {

    static Logger log = Logger.getLogger(EloquaTestFactory.class);

    public EloquaScriptCheck() {
    }

    /**
     *
     * @Test(threadPoolSize=n) will be controlled by the testng.xml configurations.
     * 
     * @param testCase
     */
    @Test(dataProvider = "next", dataProviderClass = EloquaTestCaseProvider.class)
    @Override
    public void runTestMethod(RedHatTestCase testCase) {
        EloquaScriptTest test = new EloquaScriptTest(((EloquaTestCase) testCase).getPageUrl());
        this.setWebEvent(test);
        super.runTestMethod(testCase);
    }
}
