package com.redhat.cms.selenium.webevent.cloudready.dataprovider;

import com.redhat.cms.selenium.webevent.cloudready.data.factory.CloudreadyTestSuiteFactory;
import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestSuiteProvider;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class CloudreadyTestSuiteProvider extends SeleniumTestSuiteProvider {

    static final Logger log = Logger.getLogger(CloudreadyTestSuiteProvider.class);
    private static Iterator<Object[]> cache;
    
    static {
        log.info("STATIC @DATAPROVIDER - SETUP");
        CloudreadyTestSuiteProvider data = new CloudreadyTestSuiteProvider();
        data.add(new CloudreadyTestSuiteFactory());
        log.info("STATIC @DATAPROVIDER - size = " + data.size());
        cache = (Iterator<Object[]>)data.getInstance();
        log.info("STATIC @DATAPROVIDER - CACHE SET");
    }

    private CloudreadyTestSuiteProvider() {
        log.info("@DATAPROVIDER - INITIALIZED");
    }

    //, parallel=true
    @DataProvider(name = "getTestData", parallel = true)
    public static Iterator<Object[]> getTestData() {
        log.info("STATIC @DATAPROVIDER - GETTING CACHE");
        log.info(cache);
        return cache;
    }
}
