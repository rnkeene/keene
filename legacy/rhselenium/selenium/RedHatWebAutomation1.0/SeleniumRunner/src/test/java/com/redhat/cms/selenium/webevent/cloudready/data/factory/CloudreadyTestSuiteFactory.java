package com.redhat.cms.selenium.webevent.cloudready.data.factory;

import com.redhat.cms.selenium.webevent.cloudready.data.CloudreadyTestSuite;
import com.redhat.cms.testcore.data.factory.RedHatTestSuiteFactory;
import java.util.List;
import org.apache.log4j.Logger;

public class CloudreadyTestSuiteFactory extends RedHatTestSuiteFactory {

    static Logger log = Logger.getLogger(CloudreadyTestSuiteFactory.class);

    public CloudreadyTestSuiteFactory() {
    }

    @Override
    public List<Object[]> build() {
        super.build();
        this.add(new CloudreadyTestSuite());
        return (List<Object[]>) this;
    }
}
