package com.redhat.cms.selenium.webevent.gluster.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;

public class GlusterTestSuite extends SeleniumSuiteData {

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        redHatData.put(GlusterTestSuite.class, this);
    }


}
