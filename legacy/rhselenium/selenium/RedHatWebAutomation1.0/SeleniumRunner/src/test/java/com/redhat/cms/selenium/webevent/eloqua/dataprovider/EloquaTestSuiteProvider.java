package com.redhat.cms.selenium.webevent.eloqua.dataprovider;

import com.redhat.cms.selenium.webevent.eloqua.data.factory.EloquaTestSuiteFactory;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestSuiteProvider;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class EloquaTestSuiteProvider extends SeleniumTestSuiteProvider {

    static final Logger log = Logger.getLogger(EloquaTestSuiteProvider.class);
    private static Iterator<Object[]> cache;
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/eloqua/dataprovider/testSuiteProvider.properties";
    private static final String TEST_FACTORY_STRING = "test.factory";

    static {
        EloquaTestSuiteProvider data = new EloquaTestSuiteProvider();
        FilePropertiesData fileDataProvider = FilePropertiesData.getInstance(FILE_PATH);
        List<String> factories = FilePropertiesData.splitString(fileDataProvider.getProperties().getProperty(TEST_FACTORY_STRING), ",");
        for (String factory : factories) {
            EloquaTestSuiteFactory eloquaTestData = new EloquaTestSuiteFactory(factory);
            data.add(eloquaTestData);
        }
        cache = (Iterator<Object[]>) data.getInstance();
    }

    private EloquaTestSuiteProvider() {
    }

    //, parallel=true
    @DataProvider(name = "getTestSuiteData", parallel = true)
    public static Iterator<Object[]> getTestData() {
        return cache;
    }
}
