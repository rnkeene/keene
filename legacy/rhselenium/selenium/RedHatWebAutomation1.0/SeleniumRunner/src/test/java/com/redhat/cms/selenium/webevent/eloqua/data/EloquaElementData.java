package com.redhat.cms.selenium.webevent.eloqua.data;

import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.constants.webpage.ElementConstants;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;

public class EloquaElementData extends ElementConstants{

    private static final String filePath = "src/test/java/com/redhat/cms/selenium/webevent/eloqua/data/eloquaElements.properties";

    private static final String ELQ_CFG_SCRIPT = "script.elq.cfg";
    private static final String ELQ_IMG_SCRIPT = "script.elq.img";

    private String elqCfgScriptElement;
    private String elqImgScriptElement;

    public EloquaElementData() {
        super();
        FilePropertiesData properties = FilePropertiesData.getInstance(filePath);
        this.elqCfgScriptElement = properties.getProperties().getProperty(ELQ_CFG_SCRIPT);
        this.elqImgScriptElement = properties.getProperties().getProperty(ELQ_IMG_SCRIPT);
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        super.initialize(webConstants);
        webConstants.put(EloquaElementData.class, this);
    }

    public String getElqCfgScriptElement() {
        return elqCfgScriptElement;
    }

    public void setElqCfgScriptElement(String elqCfgScriptElement) {
        this.elqCfgScriptElement = elqCfgScriptElement;
    }

    public String getElqImgScriptElement() {
        return elqImgScriptElement;
    }

    public void setElqImgScriptElement(String elqImgScriptElement) {
        this.elqImgScriptElement = elqImgScriptElement;
    }

}
