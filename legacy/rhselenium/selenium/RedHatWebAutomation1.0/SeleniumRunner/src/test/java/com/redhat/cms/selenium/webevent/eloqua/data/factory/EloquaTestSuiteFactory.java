package com.redhat.cms.selenium.webevent.eloqua.data.factory;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.factory.SeleniumTestSuiteFactory;
import java.util.List;
import org.apache.log4j.Logger;

public class EloquaTestSuiteFactory extends SeleniumTestSuiteFactory {

    static Logger log = Logger.getLogger(EloquaTestSuiteFactory.class);
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/eloqua/data/factory/testFactory";
    private static final String _PROPERTIES = ".properties";
    private String path;

    public EloquaTestSuiteFactory(String filePath) {
        this.path = FILE_PATH + filePath + _PROPERTIES;
    }

    @Override
    public List<Object[]> build() {
        super.build();
        FilePropertiesData fileDataProvider = FilePropertiesData.getInstance(this.path);
        fileDataProvider.initialize();
        this.add(fileDataProvider);
        return this;
    }
}
