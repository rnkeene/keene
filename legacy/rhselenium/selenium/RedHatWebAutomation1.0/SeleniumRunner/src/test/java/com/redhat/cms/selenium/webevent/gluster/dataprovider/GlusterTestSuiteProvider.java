package com.redhat.cms.selenium.webevent.gluster.dataprovider;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestSuiteProvider;
import com.redhat.cms.selenium.webevent.gluster.data.factory.GlusterTestSuiteFactory;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class GlusterTestSuiteProvider extends SeleniumTestSuiteProvider {

    static final Logger log = Logger.getLogger(GlusterTestSuiteProvider.class);
    private static Iterator<Object[]> cache;
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/gluster/dataprovider/testSuiteProvider.properties";
    private static final String TEST_FACTORY_STRING = "test.factory";

    static {
        GlusterTestSuiteProvider data = new GlusterTestSuiteProvider();
        FilePropertiesData fileDataProvider = FilePropertiesData.getInstance(FILE_PATH);
        List<String> factories = FilePropertiesData.splitString(fileDataProvider.getProperties().getProperty(TEST_FACTORY_STRING), ",");
        for (String factory : factories) {
            GlusterTestSuiteFactory glusterTestSuiteData = new GlusterTestSuiteFactory(factory);
            data.add(glusterTestSuiteData);
        }
        cache = (Iterator<Object[]>) data.getInstance();
    }

    private GlusterTestSuiteProvider() {
    }

    //, parallel=true
    @DataProvider(name = "getTestSuiteData", parallel = true)
    public static Iterator<Object[]> getTestData() {
        return cache;
    }
}
