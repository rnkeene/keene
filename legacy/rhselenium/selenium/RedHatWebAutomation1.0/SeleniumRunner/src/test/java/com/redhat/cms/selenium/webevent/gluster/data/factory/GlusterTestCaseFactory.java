package com.redhat.cms.selenium.webevent.gluster.data.factory;

import com.redhat.cms.selenium.webevent.data.factory.SeleniumTestCaseFactory;
import com.redhat.cms.selenium.webevent.gluster.data.GlusterTestCase;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import java.util.List;
import org.apache.log4j.Logger;

public class GlusterTestCaseFactory extends SeleniumTestCaseFactory {

    static final Logger log = Logger.getLogger(GlusterTestCaseFactory.class);
    private List<String> testCaseList;

    @Override
    public List build() {
        super.build();
        for (String testCaseData : testCaseList) {
            GlusterTestCase testCase = new GlusterTestCase(testCaseData);
            testCase.initialize(new SeleniumTestConstantsMap());
            this.add(testCase);
        }
        return this;
    }

    public List<String> getTestCaseList() {
        return testCaseList;
    }

    public void setTestCaseList(List<String> urlList) {
        this.testCaseList = urlList;
    }
}
