package com.redhat.cms.selenium.webevent.cloudready.data;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.PropertiesData;
import com.redhat.cms.selenium.webevent.eloqua.data.EloquaTestCase;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.test.Selenium2TestCase;
import org.apache.log4j.Logger;

public class CloudTestCase extends Selenium2TestCase {

    static Logger log = Logger.getLogger(EloquaTestCase.class);
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/gluster/data/factory/";    
    private static final String SELENIUM_SERVER = "seleniumServer.properties";    
    private FilePropertiesData properties;

    public CloudTestCase(String testCase) {
        this.properties = FilePropertiesData.getInstance(FILE_PATH + SELENIUM_SERVER);
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(PropertiesData.class, properties);
        super.initialize(input);
    }
}
