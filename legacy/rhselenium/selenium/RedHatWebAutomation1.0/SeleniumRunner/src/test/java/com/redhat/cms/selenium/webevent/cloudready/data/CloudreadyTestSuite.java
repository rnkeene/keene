package com.redhat.cms.selenium.webevent.cloudready.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;

public class CloudreadyTestSuite extends SeleniumSuiteData {

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        redHatData.put(CloudreadyTestSuite.class, this);
    }
}
