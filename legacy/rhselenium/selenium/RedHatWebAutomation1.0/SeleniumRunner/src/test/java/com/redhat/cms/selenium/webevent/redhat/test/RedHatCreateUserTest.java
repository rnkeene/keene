package com.redhat.cms.selenium.webevent.redhat.test;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.redhat.data.RedHatCreateUser;
import com.redhat.cms.selenium.webevent.test.Selenium1Test;
import org.testng.annotations.Test;

@Test(invocationCount = 1)
public class RedHatCreateUserTest extends Selenium1Test {

    private static final String testBatchA = "src/test/java/com/redhat/cms/selenium/test/redhat/testBatchA.properties";
    private static final String testBatchB = "src/test/java/com/redhat/cms/selenium/test/redhat/testBatchB.properties";
    private static final String testBatchC = "src/test/java/com/redhat/cms/selenium/test/redhat/testBatchC.properties";

    private String landingPage;

    public RedHatCreateUserTest(String landingPage) {
        this.landingPage = landingPage;
    }
    
    @Test
    public void runBatchA() {
        FilePropertiesData constants = new FilePropertiesData(testBatchA);
        RedHatCreateUser test = new RedHatCreateUser(this.landingPage);
    }
//    @Test
//    public void runBatchB() {
//        RedHatTestConstants constants = new RedHatTestConstants(testBatchB);
//        this.build(constants);
//    }
//    @Test
//    public void runBatchC() {
//        RedHatTestConstants constants = new RedHatTestConstants(testBatchC);
//        this.build(constants);
//    }
//
//    @Override
//    public RedHatCreateUser getInstance(String url) {
//        return new RedHatCreateUser(url);
//    }
}
