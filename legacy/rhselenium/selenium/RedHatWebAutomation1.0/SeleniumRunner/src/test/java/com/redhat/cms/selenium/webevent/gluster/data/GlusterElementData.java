package com.redhat.cms.selenium.webevent.gluster.data;

import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.constants.webpage.ElementConstants;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;

public class GlusterElementData extends ElementConstants {

    private static final String filePath = "src/test/java/com/redhat/cms/selenium/webevent/gluster/data/glusterElements.properties";

    private static final String INPUT_ID = "input.id";
    private static final String SELECT_ID = "select.id";
    private static final String TEXTAREA_ID = "textarea.id";
    private static final String END_ID = "end.id";
    private static final String EMAIL_CONSTANT = "input.constants.email";
    private static final String CONSTANTS = "input.constants";
    private static final String EMAIL = "input.id.email";
    private static final String BUDGET = "input.id.budget";
    private static final String USECASE = "select.id.usecase";
    private static final String COMPETITION = "input.id.competition";
    private static final String CHALLENGES = "select.id.challenges";
    private static final String DEPARTMENT = "input.textarea.id.department";
    private static final String ENVIRONMENT = "select.id.env";
    private static final String PRODUCT = "select.id.product";
    private static final String INTERCONNECTS = "select.id.inter";
    private static final String LEAD_GENERATION = "select.id.lead.generation";
    private static final String LEAD_SOURCE = "input.id.lead.source";
    private static final String LEAD_TYPE = "select.id.lead.type";
    private static final String NETWORK = "select.id.network";
    private static final String SERVERS = "input.id.servers";
    private static final String OPPORTUNITY = "select.id.opportunity";
    private static final String OS = "select.id.os";
    private static final String NEEDS = "select.id.needs";
    private static final String INTEREST = "select.id.interest";
    private static final String RESELLER_COMPANY = "input.id.reseller.";
    private static final String RESELLER_CONTACT = "input.id.reseller.";
    private static final String RESELLER_EMAIL = "input.id.reseller.";
    private static final String RESELLER_PHONE = "input.id.reseller.";
    private static final String RESELLER_BOCKERS_BUSINESS = "select.id.reseller.";
    private static final String RESELLER_BLOCKERS_TECHNICAL = "select.id.reseller.";
    private static final String RESELLER_CAPACITY = "input.id.reseller.";
    private static final String RESELLER_HARDWARE = "select.id.reseller.";
    private static final String RESELLER_TYPE = "select.id.reseller.";
    private static final String RESELLER_VIRTUAL_ENVIRONMENT = "select.id.reseller.";
    private static final String FORM_SUBMIT = "form.submit";
    private static final String VERIFY_SUCCESS = "verify.success";

    private String inputId = "input.id";
    private String selectId = "select.id";
    private String textareaId = "textarea.id";
    private String endId = "end.id";
    
    private String emailConstants = "input.constants.email";
    private String constants = "input.constants";
    
    private String email = "input.id.email";
    private String budget = "input.id.budget";
    private String usecase = "select.id.usecase";
    private String competition = "input.id.competition";
    private String challenges = "select.id.challenges";
    private String department = "input.textarea.id.department";
    private String environment = "select.id.env";
    private String product = "select.id.product";
    private String interconnects = "select.id.inter";
    private String leadGeneration = "select.id.lead.generation";
    private String leadSource = "input.id.lead.source";
    private String leadType = "select.id.lead.type";
    private String network = "select.id.network";
    private String servers = "input.id.servers";
    private String opportunity = "select.id.opportunity";
    private String os = "select.id.os";
    private String needs = "select.id.needs";
    private String interest = "select.id.interest";
    private String resellerCompany = "input.id.reseller.";
    private String resellerContact = "input.id.reseller.";
    private String resellerEmail = "input.id.reseller.";
    private String resellerPhone = "input.id.reseller.";
    private String resellerBusinessBlockers = "select.id.reseller.";
    private String resellerTechnicalBlockers = "select.id.reseller.";
    private String resellerCapacity = "input.id.reseller.";
    private String resellerHardware = "select.id.reseller.";
    private String resellerType = "select.id.reseller.";
    private String resellerVirtualEnvironment = "select.id.reseller.";
    private String formSubmit = "form.submit";
    private String verifySuccess = "verify.success";

    public GlusterElementData() {
        FilePropertiesData properties = FilePropertiesData.getInstance(filePath);
        this.inputId = properties.getProperties().getProperty(INPUT_ID);
        this.selectId = properties.getProperties().getProperty(SELECT_ID);
        this.textareaId = properties.getProperties().getProperty(TEXTAREA_ID);
        this.endId = properties.getProperties().getProperty(END_ID);
        this.emailConstants = properties.getProperties().getProperty(EMAIL_CONSTANT);
        this.constants = properties.getProperties().getProperty(CONSTANTS);
        this.email = properties.getProperties().getProperty(EMAIL);
        this.budget = properties.getProperties().getProperty(BUDGET);
        this.usecase = properties.getProperties().getProperty(USECASE);
        this.competition = properties.getProperties().getProperty(COMPETITION);
        this.challenges = properties.getProperties().getProperty(CHALLENGES);
        this.environment = properties.getProperties().getProperty(ENVIRONMENT);
        this.department = properties.getProperties().getProperty(DEPARTMENT);
        this.product = properties.getProperties().getProperty(PRODUCT);
        this.interconnects = properties.getProperties().getProperty(INTERCONNECTS);
        this.leadGeneration = properties.getProperties().getProperty(LEAD_GENERATION);
        this.leadSource = properties.getProperties().getProperty(LEAD_SOURCE);
        this.leadType = properties.getProperties().getProperty(LEAD_TYPE);
        this.network = properties.getProperties().getProperty(NETWORK);
        this.servers = properties.getProperties().getProperty(SERVERS);
        this.opportunity = properties.getProperties().getProperty(OPPORTUNITY);
        this.os = properties.getProperties().getProperty(OS);
        this.needs = properties.getProperties().getProperty(NEEDS);
        this.interest = properties.getProperties().getProperty(INTEREST);
        this.resellerCompany = properties.getProperties().getProperty(RESELLER_COMPANY);
        this.resellerContact = properties.getProperties().getProperty(RESELLER_CONTACT);
        this.resellerEmail = properties.getProperties().getProperty(RESELLER_EMAIL);
        this.resellerPhone = properties.getProperties().getProperty(RESELLER_PHONE);
        this.resellerBusinessBlockers = properties.getProperties().getProperty(RESELLER_BOCKERS_BUSINESS);
        this.resellerTechnicalBlockers = properties.getProperties().getProperty(RESELLER_BLOCKERS_TECHNICAL);
        this.resellerCapacity = properties.getProperties().getProperty(RESELLER_CAPACITY);
        this.resellerHardware = properties.getProperties().getProperty(RESELLER_HARDWARE);
        this.resellerType = properties.getProperties().getProperty(RESELLER_TYPE);
        this.resellerVirtualEnvironment = properties.getProperties().getProperty(RESELLER_VIRTUAL_ENVIRONMENT);
        this.resellerVirtualEnvironment = properties.getProperties().getProperty(FORM_SUBMIT);
        this.verifySuccess = properties.getProperties().getProperty(VERIFY_SUCCESS);

    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        webConstants.put(GlusterElementData.class, this);
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getChallenges() {
        return challenges;
    }

    public void setChallenges(String challenges) {
        this.challenges = challenges;
    }

    public String getCompetition() {
        return competition;
    }

    public void setCompetition(String competition) {
        this.competition = competition;
    }

    public String getConstants() {
        return constants;
    }

    public void setConstants(String constants) {
        this.constants = constants;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailConstants() {
        return emailConstants;
    }

    public void setEmailConstants(String emailConstants) {
        this.emailConstants = emailConstants;
    }

    public String getEndId() {
        return endId;
    }

    public void setEndId(String endId) {
        this.endId = endId;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getFormSubmit() {
        return formSubmit;
    }

    public void setFormSubmit(String formSubmit) {
        this.formSubmit = formSubmit;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInterconnects() {
        return interconnects;
    }

    public void setInterconnects(String interconnects) {
        this.interconnects = interconnects;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getLeadGeneration() {
        return leadGeneration;
    }

    public void setLeadGeneration(String leadGeneration) {
        this.leadGeneration = leadGeneration;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public String getLeadType() {
        return leadType;
    }

    public void setLeadType(String leadType) {
        this.leadType = leadType;
    }

    public String getNeeds() {
        return needs;
    }

    public void setNeeds(String needs) {
        this.needs = needs;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(String opportunity) {
        this.opportunity = opportunity;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getResellerBusinessBlockers() {
        return resellerBusinessBlockers;
    }

    public void setResellerBusinessBlockers(String resellerBusinessBlockers) {
        this.resellerBusinessBlockers = resellerBusinessBlockers;
    }

    public String getResellerCapacity() {
        return resellerCapacity;
    }

    public void setResellerCapacity(String resellerCapacity) {
        this.resellerCapacity = resellerCapacity;
    }

    public String getResellerCompany() {
        return resellerCompany;
    }

    public void setResellerCompany(String resellerCompany) {
        this.resellerCompany = resellerCompany;
    }

    public String getResellerContact() {
        return resellerContact;
    }

    public void setResellerContact(String resellerContact) {
        this.resellerContact = resellerContact;
    }

    public String getResellerEmail() {
        return resellerEmail;
    }

    public void setResellerEmail(String resellerEmail) {
        this.resellerEmail = resellerEmail;
    }

    public String getResellerHardware() {
        return resellerHardware;
    }

    public void setResellerHardware(String resellerHardware) {
        this.resellerHardware = resellerHardware;
    }

    public String getResellerPhone() {
        return resellerPhone;
    }

    public void setResellerPhone(String resellerPhone) {
        this.resellerPhone = resellerPhone;
    }

    public String getResellerTechnicalBlockers() {
        return resellerTechnicalBlockers;
    }

    public void setResellerTechnicalBlockers(String resellerTechnicalBlockers) {
        this.resellerTechnicalBlockers = resellerTechnicalBlockers;
    }

    public String getResellerType() {
        return resellerType;
    }

    public void setResellerType(String resellerType) {
        this.resellerType = resellerType;
    }

    public String getResellerVirtualEnvironment() {
        return resellerVirtualEnvironment;
    }

    public void setResellerVirtualEnvironment(String resellerVirtualEnvironment) {
        this.resellerVirtualEnvironment = resellerVirtualEnvironment;
    }

    public String getSelectId() {
        return selectId;
    }

    public void setSelectId(String selectId) {
        this.selectId = selectId;
    }

    public String getServers() {
        return servers;
    }

    public void setServers(String servers) {
        this.servers = servers;
    }

    public String getTextareaId() {
        return textareaId;
    }

    public void setTextareaId(String textareaId) {
        this.textareaId = textareaId;
    }

    public String getUsecase() {
        return usecase;
    }

    public void setUsecase(String usecase) {
        this.usecase = usecase;
    }

    public String getVerifySuccess() {
        return verifySuccess;
    }

    public void setVerifySuccess(String verifySuccess) {
        this.verifySuccess = verifySuccess;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" == GlusterTestConstants == \n");
        toString.append("\n\t");
        toString.append(super.toString());
        return toString.toString();
    }
}
