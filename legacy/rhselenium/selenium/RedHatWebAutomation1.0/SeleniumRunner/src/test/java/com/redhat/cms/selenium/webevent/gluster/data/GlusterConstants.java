package com.redhat.cms.selenium.webevent.gluster.data;

import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.constants.webpage.ElementConstants;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;

public class GlusterConstants extends ElementConstants {

    private static final String EMAIL = "email";
    private static final String CLICK_BUDGET = "click.budget";
    private static final String MULTISELECT_USECASE = "multiselect.usecase";
    private static final String COMPETITION = "competition";
    private static final String MULTISELECT_CHALLENGES = "multiselect.challenges";
    private static final String DEPARTMENT = "department";
    private static final String SELECT_ENVIRONMENT = "select.environment";
    private static final String SELECT_PRODUCT = "select.product";
    private static final String INTERCONNECT = "interconnect";
    private static final String SELECT_INTERCONNECT = "select.interconnect";
    private static final String SELECT_LEAD_GENERATE = "select.lead.generate";
    private static final String LEAD_SOURCE = "lead.source";
    private static final String SELECT_LEAD_TYPE = "select.lead.type";
    private static final String MULTISELECT_NETWORK = "multiselect.network";
    private static final String SERVERS = "servers";
    private static final String SELECT_OPPORTUNITY = "select.opportunity";
    private static final String SELECT_OS = "select.os";
    private static final String MULTISELECT_NEEDS = "multiselect.needs";
    private static final String SELECT_INTEREST = "select.interest";
    private static final String RESELLER = "reseller";
    private static final String RESELLER_NAME = "reseller.name";
    private static final String RESELLER_EMAIL = "reseller.email";
    private static final String RESELLER_PHONE = "reseller.phone";
    private static final String MULTISELECT_BUSINESS_BLOCKERS = "multiselect.business.blockers";
    private static final String MULTISELECT_TECHNICAL_BLOCKERS = "multiselect.technical.blockers";
    private static final String CAPACITY = "capacity";
    private static final String SELECT_HARDWARE = "select.hardware";
    private static final String SELECT_STORAGE = "select.storage";
    private static final String MULTISELECT_VIRTUAL = "multiselect.virtual";
    private static final String VERIFY_SUCCESS_PAGE = "verify.success.page";
    private String email;
    private String clickBudget;
    private String multiselectUseCase;
    private String competition;
    private String multiselectChallenges;
    private String department;
    private String selectEnvironment;
    private String selectProduct;
    private String interconnect;
    private String selectInterconnect;
    private String selectLeadGenerate;
    private String leadSource;
    private String selectLeadType;
    private String multiselectNetwork;
    private String servers;
    private String selectOpportunity;
    private String selectOs;
    private String multiselectNeeds;
    private String selectInterest;
    private String reseller;
    private String resellerName;
    private String resellerEmail;
    private String resellerPhone;
    private String multiselectBusinessBlockers;
    private String multiselectTechnicalBlockers;
    private String capacity;
    private String selectHardware;
    private String selectStorage;
    private String multiselectVirtual;
    private String verifySuccessPage;

    public GlusterConstants(String filePath) {
        FilePropertiesData properties = FilePropertiesData.getInstance(filePath);
        this.email = properties.getProperties().getProperty(EMAIL);
        this.clickBudget = properties.getProperties().getProperty(CLICK_BUDGET);
        this.multiselectUseCase = properties.getProperties().getProperty(MULTISELECT_USECASE);
        this.competition = properties.getProperties().getProperty(COMPETITION);
        this.multiselectChallenges = properties.getProperties().getProperty(MULTISELECT_CHALLENGES);
        this.department = properties.getProperties().getProperty(DEPARTMENT);
        this.selectEnvironment = properties.getProperties().getProperty(SELECT_ENVIRONMENT);
        this.selectProduct = properties.getProperties().getProperty(SELECT_PRODUCT);
        this.interconnect = properties.getProperties().getProperty(INTERCONNECT);
        this.selectInterconnect = properties.getProperties().getProperty(SELECT_INTERCONNECT);
        this.selectLeadGenerate = properties.getProperties().getProperty(SELECT_LEAD_GENERATE);
        this.leadSource = properties.getProperties().getProperty(LEAD_SOURCE);
        this.selectLeadType = properties.getProperties().getProperty(SELECT_LEAD_TYPE);
        this.multiselectNetwork = properties.getProperties().getProperty(MULTISELECT_NETWORK);
        this.servers = properties.getProperties().getProperty(SERVERS);
        this.selectOpportunity = properties.getProperties().getProperty(SELECT_OPPORTUNITY);
        this.selectOs = properties.getProperties().getProperty(SELECT_OS);
        this.multiselectNeeds = properties.getProperties().getProperty(MULTISELECT_NEEDS);
        this.selectInterest = properties.getProperties().getProperty(SELECT_INTEREST);
        this.reseller = properties.getProperties().getProperty(RESELLER);
        this.resellerName = properties.getProperties().getProperty(RESELLER_NAME);
        this.resellerEmail = properties.getProperties().getProperty(RESELLER_EMAIL);
        this.resellerPhone = properties.getProperties().getProperty(RESELLER_PHONE);
        this.multiselectBusinessBlockers = properties.getProperties().getProperty(MULTISELECT_BUSINESS_BLOCKERS);
        this.multiselectTechnicalBlockers = properties.getProperties().getProperty(MULTISELECT_TECHNICAL_BLOCKERS);
        this.capacity = properties.getProperties().getProperty(CAPACITY);
        this.selectHardware = properties.getProperties().getProperty(SELECT_HARDWARE);
        this.selectStorage = properties.getProperties().getProperty(SELECT_STORAGE);
        this.multiselectVirtual = properties.getProperties().getProperty(MULTISELECT_VIRTUAL);
        this.verifySuccessPage = properties.getProperties().getProperty(VERIFY_SUCCESS_PAGE);
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        webConstants.put(GlusterConstants.class, this);
        this.toString();
    }

    public String getMultiselectVirtual() {
        return multiselectVirtual;
    }

    public String getMultiselectTechnicalBlockers() {
        return multiselectTechnicalBlockers;
    }

    public String getMultiselectBusinessBlockers() {
        return multiselectBusinessBlockers;
    }

    public String getMultiselectNeeds() {
        return multiselectNeeds;
    }

    public String getMultiselectNetwork() {
        return multiselectNetwork;
    }

    public String getMultiselectUseCase() {
        return multiselectUseCase;
    }

    public String getMultiselectChallenges() {
        return multiselectChallenges;
    }

    public String getCapacity() {
        return capacity;
    }

    public String getCompetition() {
        return competition;
    }

    public String getDepartment() {
        return department;
    }

    public String getEmail() {
        return email;
    }

    public String getClickBudget() {
        return clickBudget;
    }

    public String getInterconnect() {
        return interconnect;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public String getReseller() {
        return reseller;
    }

    public String getResellerEmail() {
        return resellerEmail;
    }

    public String getResellerName() {
        return resellerName;
    }

    public String getResellerPhone() {
        return resellerPhone;
    }

    public String getSelectEnvironment() {
        return selectEnvironment;
    }

    public String getSelectHardware() {
        return selectHardware;
    }

    public String getSelectInterconnect() {
        return selectInterconnect;
    }

    public String getSelectInterest() {
        return selectInterest;
    }

    public String getSelectLeadGenerate() {
        return selectLeadGenerate;
    }

    public String getSelectLeadType() {
        return selectLeadType;
    }

    public String getSelectOpportunity() {
        return selectOpportunity;
    }

    public String getSelectOs() {
        return selectOs;
    }

    public String getSelectProduct() {
        return selectProduct;
    }

    public String getSelectStorage() {
        return selectStorage;
    }

    public String getServers() {
        return servers;
    }

    public String getVerifySuccessPage() {
        return verifySuccessPage;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" == GlusterTestConstants == \n");
        toString.append("\n\t");
        toString.append(EMAIL);
        toString.append("=");
        toString.append(this.email);
        toString.append("\n\t");
        toString.append(CLICK_BUDGET);
        toString.append("=");
        toString.append(this.clickBudget);
        toString.append("\n\t");
        toString.append(MULTISELECT_USECASE);
        toString.append("=");
        toString.append(this.multiselectUseCase);
        toString.append("\n\t");
        toString.append(COMPETITION);
        toString.append("=");
        toString.append(this.competition);
        toString.append("\n\t");
        toString.append(MULTISELECT_CHALLENGES);
        toString.append("=");
        toString.append(this.multiselectChallenges);
        toString.append("\n\t");
        toString.append(DEPARTMENT);
        toString.append("=");
        toString.append(this.department);
        toString.append("\n\t");
        toString.append(SELECT_ENVIRONMENT);
        toString.append("=");
        toString.append(this.selectEnvironment);
        toString.append("\n\t");
        toString.append(SELECT_PRODUCT);
        toString.append("=");
        toString.append(this.selectProduct);
        toString.append("\n\t");
        toString.append(INTERCONNECT);
        toString.append("=");
        toString.append(this.interconnect);
        toString.append("\n\t");
        toString.append(SELECT_INTERCONNECT);
        toString.append("=");
        toString.append(this.selectInterconnect);
        toString.append("\n\t");
        toString.append(SELECT_LEAD_GENERATE);
        toString.append("=");
        toString.append(this.selectLeadGenerate);
        toString.append("\n\t");
        toString.append(LEAD_SOURCE);
        toString.append("=");
        toString.append(this.leadSource);
        toString.append("\n\t");
        toString.append(SELECT_LEAD_TYPE);
        toString.append("=");
        toString.append(this.selectLeadType);
        toString.append("\n\t");
        toString.append(MULTISELECT_NETWORK);
        toString.append("=");
        toString.append(this.multiselectNetwork);
        toString.append("\n\t");
        toString.append(SERVERS);
        toString.append("=");
        toString.append(this.servers);
        toString.append("\n\t");
        toString.append(SELECT_OPPORTUNITY);
        toString.append("=");
        toString.append(this.selectOpportunity);
        toString.append("\n\t");
        toString.append(SELECT_OS);
        toString.append("=");
        toString.append(this.selectOs);
        toString.append("\n\t");
        toString.append(SELECT_INTEREST);
        toString.append("=");
        toString.append(this.selectInterest);
        toString.append("\n\t");
        toString.append(RESELLER);
        toString.append("=");
        toString.append(this.reseller);
        toString.append("\n\t");
        toString.append(RESELLER_NAME);
        toString.append("=");
        toString.append(this.resellerName);
        toString.append("\n\t");
        toString.append(RESELLER_EMAIL);
        toString.append("=");
        toString.append(this.resellerEmail);
        toString.append("\n\t");
        toString.append(RESELLER_PHONE);
        toString.append("=");
        toString.append(this.resellerPhone);
        toString.append("\n\t");
        toString.append(MULTISELECT_BUSINESS_BLOCKERS);
        toString.append("=");
        toString.append(this.multiselectBusinessBlockers);
        toString.append("\n\t");
        toString.append(MULTISELECT_TECHNICAL_BLOCKERS);
        toString.append("=");
        toString.append(this.multiselectTechnicalBlockers);
        toString.append("\n\t");
        toString.append(CAPACITY);
        toString.append("=");
        toString.append(this.capacity);
        toString.append("\n\t");
        toString.append(SELECT_HARDWARE);
        toString.append("=");
        toString.append(this.selectHardware);
        toString.append("\n\t");
        toString.append(SELECT_STORAGE);
        toString.append("=");
        toString.append(this.selectStorage);
        toString.append("\n\t");
        toString.append(MULTISELECT_VIRTUAL);
        toString.append("=");
        toString.append(this.multiselectVirtual);
        toString.append("\n\t");
        toString.append(VERIFY_SUCCESS_PAGE);
        toString.append("=");
        toString.append(this.verifySuccessPage);
        toString.append("\n\t");
        toString.append(super.toString());
        return toString.toString();
    }
}
