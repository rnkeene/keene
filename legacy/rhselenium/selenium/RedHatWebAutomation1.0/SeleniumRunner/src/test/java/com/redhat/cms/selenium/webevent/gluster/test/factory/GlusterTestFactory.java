package com.redhat.cms.selenium.webevent.gluster.test.factory;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.gluster.dataprovider.GlusterTestCaseProvider;
import com.redhat.cms.selenium.webevent.gluster.dataprovider.GlusterTestSuiteProvider;
import com.redhat.cms.selenium.webevent.gluster.test.GlusterFormCheck;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.webevent.test.factory.Selenium2TestFactory;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

public class GlusterTestFactory extends Selenium2TestFactory {

    static Logger log = Logger.getLogger(GlusterTestFactory.class);
    private static final String TEST_CONFIG = "test.cases";
    private static final String testFactoryA = "src/test/java/com/redhat/cms/selenium/webevent/gluster/test/factory/testFactory.properties";

    @Factory(dataProvider = "getTestSuiteData", dataProviderClass = GlusterTestSuiteProvider.class)
    public Object[] getTestData(SeleniumSuiteData testData) {
        return this.build();
    }

    @Override
    public Object[] build() {
        FilePropertiesData factoryData = FilePropertiesData.getInstance(testFactoryA);
        factoryData.initialize();
        String testCases = factoryData.getProperties().getProperty(TEST_CONFIG);
        List<String> testCaseList = FilePropertiesData.splitString(testCases, ",");
        GlusterTestCaseProvider.add(testCaseList);
        GlusterFormCheck test = new GlusterFormCheck();
        this.add(test);
        return this.toArray();
    }
}
