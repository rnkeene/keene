package com.redhat.cms.selenium.webevent.redhat.data.factory;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.testcore.data.factory.RedHatTestSuiteFactory;
import java.util.List;
import org.apache.log4j.Logger;

public class RedHatCreateUserTestSuiteFactory extends RedHatTestSuiteFactory {

    static Logger log = Logger.getLogger(RedHatCreateUserTestSuiteFactory.class);
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/eloqua/data/factory/testFactory";
    private static final String _PROPERTIES = ".properties";
    private String path;

    public RedHatCreateUserTestSuiteFactory(String filePath) {
        this.path = FILE_PATH + filePath + _PROPERTIES;
    }

    @Override
    public List<Object[]> build() {
        super.build();
        FilePropertiesData fileDataProvider = FilePropertiesData.getInstance(this.path);
        fileDataProvider.initialize();
        this.add(fileDataProvider);
        return (List<Object[]>)this;
    }
}
