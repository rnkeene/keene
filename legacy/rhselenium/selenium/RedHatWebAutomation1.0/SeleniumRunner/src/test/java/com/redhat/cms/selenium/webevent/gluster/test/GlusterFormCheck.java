package com.redhat.cms.selenium.webevent.gluster.test;

import com.redhat.cms.selenium.webevent.constants.webpage.URLList;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.URLProperties;
import com.redhat.cms.selenium.webevent.gluster.data.GlusterTest;
import com.redhat.cms.selenium.webevent.gluster.dataprovider.GlusterTestCaseProvider;
import com.redhat.cms.selenium.webevent.test.Selenium2Test;
import com.redhat.cms.testcore.data.RedHatTestCase;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class GlusterFormCheck extends Selenium2Test {

    private static final String LANDING_PAGE;
    static final Logger log = Logger.getLogger(GlusterFormCheck.class);

    static {
        FilePropertiesData data = FilePropertiesData.getInstance("src/test/java/com/redhat/cms/selenium/webevent/gluster/data/factory/testUrl.properties");
        URLProperties properties = new URLProperties();
        properties.initialize(data.getProperties());
        URLList constants = (URLList) properties.build();
        LANDING_PAGE = constants.getUrls().get(0);
    }

    /**
     *
     * @Test(threadPoolSize=n) will be controlled by the testng.xml configurations.
     * 
     * @param testCase
     */
    @Test(dataProvider = "next", dataProviderClass = GlusterTestCaseProvider.class)
    @Override
    public void runTestMethod(RedHatTestCase testCase) {
        GlusterTest test = new GlusterTest();
        test.setLandingPage(LANDING_PAGE);
        this.setWebEvent(test);
        super.runTestMethod(testCase);
    }
}
