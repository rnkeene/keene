package com.redhat.cms.selenium.webevent.redhat.data.factory;

import com.redhat.cms.selenium.webevent.eloqua.data.factory.*;
import com.redhat.cms.selenium.webevent.data.factory.SeleniumTestCaseFactory;
import com.redhat.cms.selenium.webevent.eloqua.data.EloquaTestCase;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import java.util.List;
import org.apache.log4j.Logger;

public class RedHatCreateUserTestCaseFactory extends SeleniumTestCaseFactory {

    static Logger log = Logger.getLogger(RedHatCreateUserTestCaseFactory.class);
    private List<String> urlList;

    public RedHatCreateUserTestCaseFactory() {
    }

    @Override
    public List build() {
        super.build();
        for (String url : urlList) {
            EloquaTestCase testCase = new EloquaTestCase("");
            testCase.setPageUrl(url);
            testCase.initialize(new SeleniumTestConstantsMap());
            this.add(testCase);
        }
        return (List) this;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    public void setUrlList(List<String> urlList) {
        this.urlList = urlList;
    }
}
