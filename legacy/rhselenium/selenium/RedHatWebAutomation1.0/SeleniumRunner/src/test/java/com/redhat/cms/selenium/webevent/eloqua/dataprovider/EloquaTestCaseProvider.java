package com.redhat.cms.selenium.webevent.eloqua.dataprovider;

import com.redhat.cms.selenium.webevent.eloqua.data.factory.EloquaTestCaseFactory;
import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestCaseProvider;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class EloquaTestCaseProvider extends SeleniumTestCaseProvider {

    static Logger log = Logger.getLogger(EloquaTestCaseProvider.class);
    private static Iterator<Object[]> cache;
    private static EloquaTestCaseProvider testCases = new EloquaTestCaseProvider();

    private EloquaTestCaseProvider() {
    }

    public static void add(List<String> urlList) {
        EloquaTestCaseFactory testCaseFactory = new EloquaTestCaseFactory();
        testCaseFactory.setFactoryList(urlList);
        testCases.add(testCaseFactory);
        cache = testCases.getInstance();
    }

    //, parallel=true
    @DataProvider(name = "next")
    public static Iterator<Object[]> next() {
        return cache;
    }
}
