package com.redhat.cms.selenium.webevent.eloqua.data;

import com.redhat.cms.selenium.command.driver.DriverFindElement;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.constants.model.CustomBrowserCommand;
import com.redhat.cms.selenium.webevent.page.DriverPageVisit;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

public class EloquaScriptTest extends DriverPageVisit {

    static Logger log = Logger.getLogger(EloquaScriptTest.class);
    private EloquaElementData data;

    public EloquaScriptTest(String landingPage) {
        this.setLandingPage(landingPage);
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        this.data = (EloquaElementData) webConstants.get(EloquaElementData.class);
        webConstants.put(CustomBrowserCommand.class, this);
        super.initialize(webConstants);
    }

    @Override
    public CompositeBrowserCommand<RemoteWebDriver> build() {
        super.build();
        if (data != null) {
            this.add(new DriverFindElement(data.getElqCfgScriptElement()));
            this.add(new DriverFindElement(data.getElqImgScriptElement()));
        } else {
            assert false : "Build Failure!";
        }
        return this;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        if (data != null) {
            toString.append("== EloquaScriptTest ==\n\tCfg Script: ");
            toString.append(data.getElqCfgScriptElement());
            toString.append("\n\tElq Script: ");
            toString.append(data.getElqImgScriptElement());
            toString.append("\n");
        }
        toString.append(" - Eloqua Script Test Case - \n");
        toString.append(super.toString());
        return toString.toString();
    }
}
