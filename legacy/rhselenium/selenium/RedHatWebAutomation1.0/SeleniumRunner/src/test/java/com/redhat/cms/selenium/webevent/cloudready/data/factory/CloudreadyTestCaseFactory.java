package com.redhat.cms.selenium.webevent.cloudready.data.factory;

import com.redhat.cms.selenium.webevent.cloudready.data.CloudTestCase;
import com.redhat.cms.selenium.webevent.data.factory.SeleniumTestCaseFactory;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import java.util.List;
import org.apache.log4j.Logger;

public class CloudreadyTestCaseFactory extends SeleniumTestCaseFactory {

    static Logger log = Logger.getLogger(CloudreadyTestCaseFactory.class);
    private List<String> testCaseList;

    public CloudreadyTestCaseFactory() {
    }

    public Object[] getTestData(SeleniumSuiteData testData) {
        return this.build().toArray();
    }

    @Override
    public List build() {
        super.build();
         for (String testCaseData : this.testCaseList) {
            CloudTestCase testCase = new CloudTestCase(testCaseData);
            testCase.initialize(new SeleniumTestConstantsMap());
            this.add(testCase);
        }
        return this;
    }

    public List<String> getTestCaseList() {
        return testCaseList;
    }

    public void setTestCaseList(List<String> testCaseList) {
        log.info("Test Case List is set: " + testCaseList);
        this.testCaseList = testCaseList;
    }
}
