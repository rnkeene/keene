package com.redhat.cms.selenium.webevent.gluster.dataprovider;

import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestCaseProvider;
import com.redhat.cms.selenium.webevent.gluster.data.factory.GlusterTestCaseFactory;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class GlusterTestCaseProvider extends SeleniumTestCaseProvider {

    static Logger log = Logger.getLogger(GlusterTestCaseProvider.class);
    private static Iterator<Object[]> cache;
    private static GlusterTestCaseProvider testCases = new GlusterTestCaseProvider();

    private GlusterTestCaseProvider() {
    }

    public static void add(List<String> urlList) {
        GlusterTestCaseFactory testCaseFactory = new GlusterTestCaseFactory();
        testCaseFactory.setTestCaseList(urlList);
        testCases.add(testCaseFactory);
        cache = testCases.getInstance();
    }

    //, parallel=true
    @DataProvider(name = "next")
    public static Iterator<Object[]> next() {
        return cache;
    }
}
