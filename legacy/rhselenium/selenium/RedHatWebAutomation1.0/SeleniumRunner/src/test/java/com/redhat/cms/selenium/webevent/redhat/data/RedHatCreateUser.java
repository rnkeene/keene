package com.redhat.cms.selenium.webevent.redhat.data;

import com.redhat.cms.selenium.command.selenium.SeleniumClick;
import com.redhat.cms.selenium.command.selenium.SeleniumType;
import com.redhat.cms.selenium.command.selenium.SeleniumWait;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.page.SeleniumPageVisit;
import com.thoughtworks.selenium.Selenium;

public class RedHatCreateUser extends SeleniumPageVisit {

    public RedHatCreateUser(String landingPage) {
        super.setLandingPage(landingPage);
    }

    @Override
    public void initialize(WebConstantsMap webConstants){
        super.initialize(webConstants);
    }

    @Override
    public CompositeBrowserCommand<Selenium> build() {
        super.build();
        this.add(new SeleniumClick("link=Resource library"));
        this.add(new SeleniumWait(30000));
        this.add(new SeleniumClick("id=loginhtml_a"));
        this.add(new SeleniumType("name=username", "nkeene"));
        return this;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("== RedHatTest ==\n");
        toString.append(super.toString());
        return toString.toString();
    }
}
