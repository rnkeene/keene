package com.redhat.cms.selenium.webevent.gluster.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.PropertiesData;
import com.redhat.cms.selenium.webevent.test.Selenium2TestCase;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import org.apache.log4j.Logger;

public class GlusterTestCase extends Selenium2TestCase {

    static Logger log = Logger.getLogger(GlusterTestCase.class);
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/gluster/data/factory/";
    private static final String TEST_BATCH = "testBatch";
    private static final String SELENIUM_SERVER = "seleniumServer.properties";
    private static final String _PROPERTIES = ".properties";
    private FilePropertiesData properties;
    private GlusterConstants constants;

    public GlusterTestCase(String testCase) {
        this.properties = FilePropertiesData.getInstance(FILE_PATH + SELENIUM_SERVER);
        constants = new GlusterConstants(FILE_PATH + TEST_BATCH + testCase + _PROPERTIES);
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(PropertiesData.class, properties);
        super.initialize(input);
        GlusterElementData elements = new GlusterElementData();
        elements.initialize(((SeleniumTestConstantsMap) input).getWebConstantsMap());
        constants.initialize(((SeleniumTestConstantsMap) input).getWebConstantsMap());
    }
}
