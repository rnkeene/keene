package com.redhat.cms.selenium.webevent.gluster.data;

import com.redhat.cms.selenium.command.decorator.assertion.AssertFindText;
import com.redhat.cms.selenium.command.driver.DriverClearElement;
import com.redhat.cms.selenium.command.driver.DriverClick;
import com.redhat.cms.selenium.command.driver.DriverFindElement;
import com.redhat.cms.selenium.command.driver.DriverSelectValue;
import com.redhat.cms.selenium.command.driver.DriverSendKeys;
import com.redhat.cms.selenium.command.driver.DriverWaitForNewPage;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.page.DriverPageVisit;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

public class GlusterTest extends DriverPageVisit {

    private static final long WAIT_TIME = 3000;
    static Logger log = Logger.getLogger(GlusterTest.class);
    private GlusterElementData elements;
    private GlusterConstants data;

    public GlusterTest() {
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        this.elements = (GlusterElementData) webConstants.get(GlusterElementData.class);
        this.data = (GlusterConstants) webConstants.get(GlusterConstants.class);
        super.initialize(webConstants);
    }

    @Override
    public CompositeBrowserCommand<RemoteWebDriver> build() {
        super.build();
        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443488_Email1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443488_Email1']", this.data.getEmail()));
        this.add(new DriverClick("//input[@id='LeadQual_DRHAT000000443722_Budget1_" + this.data.getClickBudget() + "']"));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Business_Case_Use_case1']", this.data.getMultiselectUseCase()));

        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Competition_what_else_1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Competition_what_else_1']", this.data.getCompetition()));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Current_Challenges_Pai1']", this.data.getMultiselectChallenges()));

        this.add(new DriverClearElement("//textarea[@id='LeadQual_DRHAT000000443722_Department___Division1']"));
        this.add(new DriverSendKeys("//textarea[@id='LeadQual_DRHAT000000443722_Department___Division1']", this.data.getDepartment()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Deployment_Environment1']", this.data.getSelectEnvironment()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Gluster_Product1']", this.data.getSelectProduct()));
        this.add(new DriverClearElement("//textarea[@id='LeadQual_DRHAT000000443722_Interconnects1']"));
        this.add(new DriverSendKeys("//textarea[@id='LeadQual_DRHAT000000443722_Interconnects1']", this.data.getInterconnect()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Interconnects1']", this.data.getSelectInterconnect()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Lead_Generation1']", this.data.getSelectLeadGenerate()));
        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Lead_Source_Detail1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Lead_Source_Detail1']", this.data.getLeadSource()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Lead_Type1']", this.data.getSelectLeadType()));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Network1']", this.data.getMultiselectNetwork()));

        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Number_of_Servers1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Number_of_Servers1']", this.data.getServers()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Opportunity_Type1']", this.data.getSelectOpportunity()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Primary_Operating_Syst1']", this.data.getSelectOs()));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Product_Feature_Needs1']", this.data.getMultiselectNeeds()));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Product_of_Interest1']", this.data.getSelectInterest()));
        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Reseller_Company1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Reseller_Company1']", this.data.getReseller()));
        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Reseller_Contact1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Reseller_Contact1']", this.data.getResellerName()));
        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Reseller_Email1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Reseller_Email1']", this.data.getResellerEmail()));
        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Reseller_Phone1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Reseller_Phone1']", this.data.getResellerPhone()));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Sales_Blockers_Busines1']", this.data.getMultiselectBusinessBlockers()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Sales_Blockers_Technic1']", this.data.getMultiselectTechnicalBlockers()));

        this.add(new DriverClearElement("//input[@id='LeadQual_DRHAT000000443722_Storage_Capacity_TB_PB1']"));
        this.add(new DriverSendKeys("//input[@id='LeadQual_DRHAT000000443722_Storage_Capacity_TB_PB1']", this.data.getCapacity()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Storage_Hardware_Curre1']", this.data.getSelectHardware()));
        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_Storage_Type1']", this.data.getSelectStorage()));

        this.add(new DriverSelectValue("//select[@id='LeadQual_DRHAT000000443722_What_Type_of_Virtual_E1']", this.data.getMultiselectVirtual()));

        this.add(new DriverClick("//input[@id='submit']"));
        this.add(new DriverWaitForNewPage(WAIT_TIME, this.getLandingPage()));
        DriverFindElement findElement = new DriverFindElement(elements.getVerifySuccess());
        AssertFindText assertFind = new AssertFindText(findElement, this.data.getVerifySuccessPage());
        this.add(assertFind);

        return this;
    }
}
