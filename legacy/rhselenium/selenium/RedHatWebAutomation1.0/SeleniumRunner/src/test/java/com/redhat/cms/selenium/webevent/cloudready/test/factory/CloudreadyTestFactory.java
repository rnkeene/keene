package com.redhat.cms.selenium.webevent.cloudready.test.factory;

import com.redhat.cms.selenium.webevent.cloudready.data.CloudreadyTestSuite;
import com.redhat.cms.selenium.webevent.cloudready.dataprovider.CloudreadyTestCaseProvider;
import com.redhat.cms.selenium.webevent.cloudready.dataprovider.CloudreadyTestSuiteProvider;
import com.redhat.cms.selenium.webevent.cloudready.test.CloudReadyScriptCheck;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.webevent.test.factory.Selenium2TestFactory;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

public class CloudreadyTestFactory extends Selenium2TestFactory {

    static Logger log = Logger.getLogger(CloudreadyTestFactory.class);
    CloudreadyTestSuite testData;
    private static final String TEST_CONFIG = "test.cases";
    private static final String testFactoryA = "src/test/java/com/redhat/cms/selenium/webevent/cloudready/test/factory/testFactory.properties";

    public CloudreadyTestFactory() {
        super();
    }

    @Factory(dataProvider = "getTestData", dataProviderClass = CloudreadyTestSuiteProvider.class)
    public Object[] getTestData(SeleniumSuiteData testData) {
        this.testData = (CloudreadyTestSuite) testData;
        return this.build();
    }

    @Override
    public Object[] build() {
        FilePropertiesData factoryData = FilePropertiesData.getInstance(testFactoryA);
        factoryData.initialize();
        String testCases = factoryData.getProperties().getProperty(TEST_CONFIG);
        log.info("CloudreadyTestFactory testCases: " + testCases);
        List<String> testCaseList = FilePropertiesData.splitString(testCases, ",");
        log.info("Adding : " + testCaseList);
        CloudreadyTestCaseProvider.add(testCaseList);
        log.info("@FACTORY -- BUILDING");
        this.add(new CloudReadyScriptCheck());
        log.info("@FACTORY -- DONE BUILDING");
        return this.toArray();
    }
}
