package com.redhat.cms.selenium.webevent.cloudready.dataprovider;

import com.redhat.cms.selenium.webevent.cloudready.data.factory.CloudreadyTestCaseFactory;
import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestCaseProvider;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class CloudreadyTestCaseProvider extends SeleniumTestCaseProvider {

    static Logger log = Logger.getLogger(CloudreadyTestCaseProvider.class);
    private static Iterator<Object[]> cache;
    private static CloudreadyTestCaseProvider testCases = new CloudreadyTestCaseProvider();

    private CloudreadyTestCaseProvider() {
        log.info("STATIC @DATAPROVIDER - TEST TEST TEST");
    }

    public static void add(List<String> urlList) {
        log.info("@DATAPROVIDER - NEW CLOUDREADY TEST CASE FACTORY");
        CloudreadyTestCaseFactory testCaseFactory = new CloudreadyTestCaseFactory();
        log.info("@DATAPROVIDER - add urlList : " + urlList);
        testCaseFactory.setTestCaseList(urlList);
        log.info("@DATAPROVIDER - urlList added; adding testCaseFactory to testCases");
        testCases.add(testCaseFactory);
        cache = testCases.getInstance();
    }

    //, parallel=true
    @DataProvider(name = "next")
    public static Iterator<Object[]> next() {
        log.info("STATIC @DATAPROVIDER - TEST TEST TEST cache");
        log.info(cache);
        return cache;
    }
}
