package com.redhat.cms.selenium.webevent.redhat.data;

import com.redhat.cms.selenium.webevent.eloqua.data.*;
import com.redhat.cms.selenium.webevent.test.SeleniumTestConstantsMap;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.PropertiesData;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.webevent.test.Selenium2TestCase;
import org.apache.log4j.Logger;

public class RedHatCreateUserTestCase extends Selenium2TestCase {

    static Logger log = Logger.getLogger(RedHatCreateUserTestCase.class);
    private static final String FILE_PATH = "src/test/java/com/redhat/cms/selenium/webevent/redhat/data/testDataFactory";
    private static final String _PROPERTIES = ".properties";
    private FilePropertiesData properties;
    private String pageUrl;

    public RedHatCreateUserTestCase(String testCase) {
        this.properties = FilePropertiesData.getInstance(FILE_PATH + testCase + _PROPERTIES);
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(PropertiesData.class, properties);
        super.initialize(input);
        EloquaElementData elements = new EloquaElementData();
        elements.initialize(((SeleniumTestConstantsMap) input).getWebConstantsMap());
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("<br />\nURL:\n<br /><br />\n");
        toString.append(this.pageUrl);
        toString.append("\n<br /><br />\nProperties: \n<br /><br />\n");
        toString.append(this.properties);
        toString.append("<<br /><br />\n\n");
        return toString.toString();
    }
}
