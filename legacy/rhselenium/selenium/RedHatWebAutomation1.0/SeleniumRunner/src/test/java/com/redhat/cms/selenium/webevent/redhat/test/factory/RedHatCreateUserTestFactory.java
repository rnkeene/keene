package com.redhat.cms.selenium.webevent.redhat.test.factory;

import com.redhat.cms.selenium.webevent.constants.webpage.URLList;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.webevent.data.URLProperties;
import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.redhat.dataprovider.RedHatCreateUserTestSuiteProvider;
import com.redhat.cms.selenium.webevent.redhat.test.RedHatCreateUserTest;
import com.redhat.cms.selenium.webevent.test.factory.Selenium1TestFactory;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

public class RedHatCreateUserTestFactory extends Selenium1TestFactory {

    static Logger log = Logger.getLogger(RedHatCreateUserTestFactory.class);
    FilePropertiesData testData;

    public RedHatCreateUserTestFactory() {
        super();
    }

    @Factory(dataProvider = "getTestData", dataProviderClass = RedHatCreateUserTestSuiteProvider.class)
    public Object[] getTestData(SeleniumSuiteData testData) {
        this.testData = (FilePropertiesData) testData;
        return this.build();
    }

    @Override
    public Object[] build() {
        URLProperties properties = new URLProperties();
        properties.initialize((Properties)this.testData.getProperties());
        URLList constants = (URLList) properties.build();
        List<String> urlList = constants.getUrls();
        this.add(new RedHatCreateUserTest(urlList.get(0)));
        return this.toArray();
    }
}
