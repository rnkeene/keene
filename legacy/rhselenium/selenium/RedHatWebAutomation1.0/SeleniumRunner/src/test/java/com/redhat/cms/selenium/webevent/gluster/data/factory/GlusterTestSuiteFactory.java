package com.redhat.cms.selenium.webevent.gluster.data.factory;

import com.redhat.cms.selenium.webevent.data.FilePropertiesData;
import com.redhat.cms.selenium.webevent.data.factory.SeleniumTestSuiteFactory;
import com.redhat.cms.selenium.webevent.gluster.data.GlusterTestSuite;
import com.redhat.cms.selenium.webevent.gluster.dataprovider.GlusterTestCaseProvider;
import java.util.List;
import org.apache.log4j.Logger;

public class GlusterTestSuiteFactory extends SeleniumTestSuiteFactory {

    static Logger log = Logger.getLogger(GlusterTestSuiteFactory.class);
    private static final String TEST_CONFIG = "test.cases";
    private static final String testFactoryA = "src/test/java/com/redhat/cms/selenium/webevent/gluster/test/factory/testFactory.properties";

    /**
     * Actually using this Test Suite Factory to prime the TestCases. -- didn't have to do it here;
     * this is just where the code happened to end up somehow, and it works fine this way.
     */
    static {
        FilePropertiesData factoryData = FilePropertiesData.getInstance(testFactoryA);
        factoryData.initialize();
        String testCases = factoryData.getProperties().getProperty(TEST_CONFIG);
        List<String> testCaseList = FilePropertiesData.splitString(testCases, ",");
        GlusterTestCaseProvider.add(testCaseList);
    }
    
    private String path;

    public GlusterTestSuiteFactory(String filePath) {
        this.path = filePath;
    }

    @Override
    public List<Object[]> build() {
        super.build();
        GlusterTestSuite testSuite = new GlusterTestSuite();
        testSuite.initialize(testSuite);
        this.add(testSuite);
        return this;
    }
}
