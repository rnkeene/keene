package com.redhat.cms.selenium.webevent.cmsadminlogin.data;

import com.redhat.cms.selenium.command.driver.DriverClearElement;
import com.redhat.cms.selenium.command.driver.DriverClick;
import com.redhat.cms.selenium.command.driver.DriverFindElement;
import com.redhat.cms.selenium.command.driver.DriverSelectValue;
import com.redhat.cms.selenium.command.driver.DriverSendKeys;
import com.redhat.cms.selenium.command.driver.DriverWait;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.redhat.cms.selenium.webevent.constants.WebConstantsMap;
import com.redhat.cms.selenium.webevent.constants.model.CustomBrowserCommand;
import com.redhat.cms.selenium.webevent.page.DriverPageVisit;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

public class CMSAdminTest extends DriverPageVisit {

    static Logger log = Logger.getLogger(CMSAdminTest.class);

    public CMSAdminTest() {
    }

    @Override
    public void initialize(WebConstantsMap webConstants) {
        webConstants.put(CMSAdminTest.class, this);
        super.initialize(webConstants);
    }

    @Override
    public CompositeBrowserCommand<RemoteWebDriver> build() {
        super.build();
        this.add(new DriverFindElement("//*[@id='Email']"));
        this.add(new DriverSendKeys("//*[@id='Email']", "gghali@redhat.com"));
//        this.add(new DriverWait(2500));
        log.info("Email Done");
        this.add(new DriverClearElement("//*[@id='FirstName']"));
        this.add(new DriverSendKeys("//*[@id='FirstName']", "Gabi"));
//        this.add(new DriverWait(2500));
        log.info("FirstName Done");
        this.add(new DriverClearElement("//*[@id='LastName']"));
        this.add(new DriverSendKeys("//*[@id='LastName']", "Ghali"));
        log.info("LastName Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverSelectValue("//*[@id='JobTitle']", "Other"));
        log.info("JobTitle Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverSelectValue("//*[@id='Department']", "IT/IS/Operations"));
        log.info("Department Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverClearElement("//*[@id='Company']"));
        this.add(new DriverSendKeys("//*[@id='Company']", "123 abc"));
        log.info("Company Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverClearElement("//*[@id='Company']"));
        this.add(new DriverSendKeys("//*[@id='Company']", "Red Hat"));
        log.info("Company Done Again");
//        this.add(new DriverWait(2500));
        this.add(new DriverClearElement("//*[@id='Address']"));
        this.add(new DriverSendKeys("//*[@id='Address']", "1801 Varsity Drive"));
        log.info("Address Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverClearElement("//*[@id='City']"));
        this.add(new DriverSendKeys("//*[@id='City']", "Raleigh"));
        log.info("City Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverSelectValue("//*[@id='Country']", "United States"));
        log.info("Country Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverSelectValue("//*[@id='StateProvince']", "North Carolina"));
        log.info("Province Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverClearElement("//*[@id='ZipCode']"));
        this.add(new DriverSendKeys("//*[@id='ZipCode']", "27609"));
        log.info("Zipcode Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverClearElement("//*[@id='Phone']"));
        this.add(new DriverSendKeys("//*[@id='Phone']", "555-555-5555"));
        log.info("Phone Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverSelectValue("//select[@id='AnnualRevenue']", "100-150MM"));
        log.info("AnnualRevenue Done");
//        this.add(new DriverWait(2500));
        this.add(new DriverSelectValue("//*[@id='PartnerOptIn']", "No, I'm not interested"));
        log.info("PartnerOptIn Done");
//        this.add(new DriverWait(2500));

        this.add(new DriverClick("//*[@id='submit']"));

        this.add(new DriverWait(2500));

//	driver.find_element_by_tag_name("a").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_css_selector("#next > span.arrow").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_css_selector("#next > span.arrow").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_css_selector("#next > span.arrow").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_css_selector("#next > span.arrow").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_css_selector("#next > span.arrow").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()
//        driver.find_element_by_id("a").click()
//        driver.find_element_by_css_selector("label").click()
//        driver.find_element_by_id("next").click()


        return this;
    }
}
