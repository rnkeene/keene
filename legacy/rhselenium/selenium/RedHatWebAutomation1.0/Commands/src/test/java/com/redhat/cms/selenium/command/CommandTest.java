package com.redhat.cms.selenium.command;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CommandTest{

    static Logger log = Logger.getLogger(CommandTest.class);

    @BeforeClass
    public void start(){
        log.info("CommandTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("CommandTest.setUp()");
        assert true;
    }

    @Test
    public void testCommand(){
        log.info("CommandTest.testCommand()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("CommandTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("CommandTest.finish()");
        assert true;
    }
    
}
