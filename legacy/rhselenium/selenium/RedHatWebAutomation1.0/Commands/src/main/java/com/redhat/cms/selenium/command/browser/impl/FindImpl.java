package com.redhat.cms.selenium.command.browser.impl;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import com.redhat.cms.selenium.command.browser.Find;

public abstract class FindImpl<B,R> implements Find<B,R>, BrowserCommand<B>{

    private String target;
    private R results;

    protected FindImpl(String target) {
        this.target = target;
    }

    @Override
    public String getTarget() {
        return target;
    }

    @Override
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public R getFindResults() {
        return this.results;
    }

    @Override
    public void setFindResults(R results) {
        this.results = results;
    }

    @Override
    public String toString() {
        if(results!=null){
            return this.results.toString();
        }
        return this.target;
    }
}
