package com.redhat.cms.selenium.command.selenium;

import com.thoughtworks.selenium.Selenium;

public class SeleniumClick extends SeleniumFind{

    public SeleniumClick(String target) {
        super(target);
    }

    @Override
    public void execute(Selenium selenium) throws Exception{
        super.execute(selenium);
        selenium.click(this.getFindResults());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.click(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }
}
