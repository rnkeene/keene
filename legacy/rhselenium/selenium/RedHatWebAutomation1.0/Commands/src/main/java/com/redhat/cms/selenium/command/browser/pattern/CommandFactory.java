package com.redhat.cms.selenium.command.browser.pattern;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.cms.selenium.pattern.Factory;

public interface CommandFactory<I extends Command, O extends Command> extends Factory<O,I>{
}
