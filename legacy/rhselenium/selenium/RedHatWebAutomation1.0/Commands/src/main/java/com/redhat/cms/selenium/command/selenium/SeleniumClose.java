package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.selenium.command.browser.Close;
import com.thoughtworks.selenium.Selenium;

public class SeleniumClose implements Close<Selenium>{

    public SeleniumClose() {
        super();
    }

    @Override
    public void execute(Selenium selenium) throws Exception {
        selenium.close();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.close();");
        return toString.toString();
    }

    @Override
    public SeleniumClose getInstance() {
        return this;
    }
}
