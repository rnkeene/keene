package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.Close;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverClose implements Close<RemoteWebDriver> {

    public DriverClose() {
    }

    @Override
    public void execute(RemoteWebDriver browser) throws Exception{
        browser.quit();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.quit()");
        return toString.toString();
    }

    @Override
    public DriverClose getInstance() {
        return this;
    }
}
