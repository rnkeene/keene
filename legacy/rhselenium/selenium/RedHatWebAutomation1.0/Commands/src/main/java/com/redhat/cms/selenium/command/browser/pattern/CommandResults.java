package com.redhat.cms.selenium.command.browser.pattern;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.cms.selenium.pattern.Results;

public interface CommandResults<C extends Command> extends Results<C> {
}
