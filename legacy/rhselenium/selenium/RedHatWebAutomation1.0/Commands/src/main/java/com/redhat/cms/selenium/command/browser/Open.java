package com.redhat.cms.selenium.command.browser;

public interface Open<B> extends BrowserCommand<B> {

    public String getUrl();

    public void setUrl(String url);
}
