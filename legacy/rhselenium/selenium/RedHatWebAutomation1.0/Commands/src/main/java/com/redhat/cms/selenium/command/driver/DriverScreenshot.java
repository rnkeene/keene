package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import java.io.File;
import java.util.Calendar;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverScreenshot implements BrowserCommand<RemoteWebDriver>
{
    @Override
    public void execute(RemoteWebDriver browser) throws Exception
    {
        WebDriver augmentedDriver = new Augmenter().augment(browser);
        File file = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
        // Now you can do whatever you need to do with it, for example copy somewhere
        FileUtils.copyFile(file, new File("/home/nkeene/NetBeansProjects/RedHatSelenium/FitnesseDecorator/target/pics/" + Calendar.getInstance().getTimeInMillis() + ".jpg"));
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.TakesScreenshot(");
        toString.append(super.toString());
        toString.append(")");
        return toString.toString();
    }

    @Override
    public DriverScreenshot getInstance()
    {
        return this;
    }
}
