package com.redhat.cms.selenium.command.browser;

public interface Type<B,R> extends Find<B,R>{
    
    public String getValue();

    public void setValue(String value);

}
