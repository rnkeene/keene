package com.redhat.cms.selenium.command.browser;

public interface Find<B, R> extends BrowserCommand<B>{

    public String getTarget();

    public void setTarget(String target);

    public R getFindResults();

    public void setFindResults(R results);
    
}
