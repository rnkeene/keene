package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.selenium.command.browser.Wait;
import com.redhat.cms.selenium.command.browser.impl.WaitImpl;
import com.thoughtworks.selenium.Selenium;

public class SeleniumWait extends WaitImpl<Selenium> {

    public SeleniumWait() {
    }

    public SeleniumWait(long duration) {
        super(duration);
    }

    @Override
    public void execute(Selenium selenium) throws Exception{
        selenium.waitForPageToLoad(Long.toString(this.getWaitTime()));
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tSeleniumDriver: selenium.waitForPageToLoad(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }

    @Override
    public SeleniumWait getInstance() {
        return this;
    }
}
