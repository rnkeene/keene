package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.impl.WaitImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverWait extends WaitImpl<RemoteWebDriver> {

    public DriverWait(long duration) {
        super(duration);
    }

    @Override
    public synchronized void execute(RemoteWebDriver browser) throws Exception {
        this.wait(this.getWaitTime());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.wait(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }

    @Override
    public DriverWait getInstance() {
        return this;
    }
}
