package com.redhat.cms.selenium.command.decorator;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.cms.selenium.pattern.impl.DecoratorList;

public class CommandDecoratorList<C extends Command> extends DecoratorList<C> {

    public CommandDecoratorList() {
    }

    public CommandDecoratorList(C first) {
        super(first);
    }
    
}
