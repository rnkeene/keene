package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.impl.OpenImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Opens a browser to a particular URL.
 */
public class DriverOpen extends OpenImpl<RemoteWebDriver> {

    public DriverOpen(String url) {
        super(url);
    }

    @Override
    public void execute(RemoteWebDriver browser) throws Exception{
        browser.get(this.getUrl());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.get(");
        toString.append(super.toString());
        toString.append(")");
        return toString.toString();
    }

    @Override
    public DriverOpen getInstance() {
        return this;
    }
}
