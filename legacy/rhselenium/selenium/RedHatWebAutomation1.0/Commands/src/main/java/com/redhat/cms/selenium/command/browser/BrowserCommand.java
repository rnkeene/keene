package com.redhat.cms.selenium.command.browser;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.cms.selenium.pattern.FactoryMethod;

public interface BrowserCommand<B> extends Command<B, BrowserCommand<B>>, FactoryMethod<BrowserCommand<B>> {
}
