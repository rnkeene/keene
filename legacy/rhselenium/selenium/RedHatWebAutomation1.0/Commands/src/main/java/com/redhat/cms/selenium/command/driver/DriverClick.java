package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.Click;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverClick extends DriverFindElement implements Click<RemoteWebDriver,WebElement> {

    public DriverClick(String xpath) {
        super(xpath);
    }

    @Override
    public void execute(RemoteWebDriver browser) throws Exception {
        super.execute(browser);
        this.getFindResults().click();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(super.toString());
        toString.append(".click()");
        return toString.toString();
    }
}
