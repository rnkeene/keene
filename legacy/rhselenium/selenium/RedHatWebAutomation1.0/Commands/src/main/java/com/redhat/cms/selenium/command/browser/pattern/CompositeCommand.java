package com.redhat.cms.selenium.command.browser.pattern;

import com.redhat.cms.selenium.command.decorator.CommandDecoratorList;
import com.redhat.cms.selenium.pattern.Command;
import java.util.List;

/**
 *
 * This is the Default Container for any Object that implements the Command interface, and should be used whenever possible to manage multiple Commands.
 *
 * Since Command objects are customarily executed consecutively 'in order' a List is used to manage them.
 *
 * The CompositeCommand is designed to be a fundamental building block for the Selenium framework and creates a relationship between Commands (e.g. a Mediator), and creates a manager for Commands.
 *
 * Almost all objects that have a relationship to a Selenium test will either utilize or be utilized by a CompositeCommand.
 *
 * At its core the CompositeCommand is providing or facilitating the following:
 *
 * (i) Data Structure is a List.
 *
 * (1) Command Pattern - since all interaction with Selenium involves the idea of a command, an Object<B> will serve as a placeholder for the Selenium Object that will be executing a command.
 *
 * (2) Composite Pattern (and Mediator) - Selenium commands can be organized into any number of ordered executions, so the CompositePattern is designed to conseal and manage 1-n Commands behind a single instance of Command.
 *
 * (3) Builder Pattern - overriding CompositeCommand implementations should use the -build();- method to finalize the Commands held in its list before execution.  This also doubles as a way to re-initialize a Command Object.
 *
 * (4) Results - in order to leave the Commands open to flexibility (and reduce additional code) the results will be managed by this root Object unless overridden.
 *
 * More features could potentially be added, but this core set of responsibilities is enough flexability without much code overhead or Object management, but enforces a standard/expectation for the Objects extending this class.
 *
 * @param <B> classically the Selenium Browser/Driver that will be used, in theory this could be set to any Object type.
 * @param <C> an Object that extends CompositeCommand<B,C,H> and will be the Results Object..
 * @param <H> the type of Commands that will be held by <C>.  
 */
public class CompositeCommand<B, C extends CompositeCommand<B, C, H>, H extends Command<B, H>> extends CommandDecoratorList<H> implements Command<B, C>, CommandBuilder<C>, CommandResults<C> {
    /**
     *
     * Protected default constructor, prevents concrete instances of this particular Object by other Objects.
     *
     */
    protected CompositeCommand() {
        super();
    }

    /**
     *
     * This is the root node of any build, all objects that override the build() method need to call super.build() before execution.
     *
     * This method is designed to be overridden, as the default behavior of the CompositeCommand will not provide anything for execution.
     *
     * This method should be self-contained and add in the content that it is designed to manage.
     *
     * This method should focus on initializing and building content that is required for execution, and is typically the primary region of focus/development in any implementation.
     *
     * ** In Objects with a reference to a CompositeCommand the build method should be called before adding additional additional content; that ensures that .this class is properly pre-populated with its parent's content.
     * ** Run -command.build();- and then do any -command.add(...);- calls before the -execute(B browser);- method is run.
     *
     * If this is not desired functionality in a build, then override this method or do not call it before execution (or both).
     *
     * @return
     */
    @Override
    public C build() {
        this.clear();
        super.setTarget(null);
        return (C) this.getResults();
    }

    /**
     *
     * The CompositeCommand is responsible for running the -execute();- method on the Commands it holds in the order they were added.
     *
     * The base Command Object does not provide any feedback from method execution, this was done purposefully to prevent complicating the implentation and to prevent undue responsibilities for implenting classes.
     *
     * Each implementation will have to manage any details created during execution (e.g. an underlying knowledge or awareness) of the Commands, and build out the additional features.
     *
     * This Object (and any implementation) becomes the gatekeeper between Command executions, and so can manage a variety of operations from data collection to error reporting.
     *
     * ** Optionally management can be done during the build() method, in general it's safe to add features during the build and not manage or alter executions (to prevent creating bugs with wrapping Object.  //e.g. This method is primarly for 'processing'/'reading' Commands.
     *
     * This makes Objects that manage a CompositeCommand inherently less safe when involved with multi-threaded operations, Selenium is typically quite sensitive to the order of execution so items being added/removed must be done so carefully.
     *
     * @param browser
     * @throws Exception
     */
    @Override
    public void execute(B browser) throws Exception {
        CompositeCommand<B,C,H> results = new CompositeCommand<B,C,H>();
        for (H command : this) {
            command.execute(browser);
            results.add(command);
        }
        super.setTarget((List<H>)results);
    }

    /**
     *
     * A placeholder for future implementations, as it is anticipated there will be a majority of Commands returning results of some kind.
     *
     * In this base case it returns a list of the commands that were executed.
     *
     * This stub should add a bit of code now to prevent duplicate implementations in later models (prevent some code re-use and posible bugs in duplicate management).
     *
     * @return
     */
    @Override
    public C getResults() {
        return (C)this.getTarget();
    }

    /**
     *
     * Something along the lines of this static method is required for most Objects that extends Command.
     *
     * This is a 'true' Abstract Method implementation.
     *
     * @return a CompositeCommand instance.
     */
    public static CompositeCommand newInstance() {
        return new CompositeCommand();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\t\t - Composite Command:");
        for (H command : this) {
            toString.append("\n\t\t\t");
            toString.append(command);
        }
        if (this.getResults() != null && this.getResults() != this) {
            toString.append(this.getResults());
        }
        return toString.toString();
    }
}
