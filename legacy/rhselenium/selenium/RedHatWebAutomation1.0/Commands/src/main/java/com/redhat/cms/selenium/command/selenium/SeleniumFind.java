package com.redhat.cms.selenium.command.selenium;

import com.redhat.cms.selenium.command.browser.impl.FindImpl;
import com.thoughtworks.selenium.Selenium;

public class SeleniumFind extends FindImpl<Selenium, String>{

    public SeleniumFind(String target) {
        super(target);
    }

    @Override
    public void execute(Selenium selenium) throws Exception{
        this.setFindResults(this.getTarget());
    }

    @Override
    public SeleniumFind getInstance() {
        return this;
    }
}
