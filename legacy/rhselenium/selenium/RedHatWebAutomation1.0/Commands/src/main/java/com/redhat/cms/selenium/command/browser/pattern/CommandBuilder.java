package com.redhat.cms.selenium.command.browser.pattern;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.cms.selenium.pattern.Builder;

public interface CommandBuilder<C extends Command> extends Builder<C> {

}
