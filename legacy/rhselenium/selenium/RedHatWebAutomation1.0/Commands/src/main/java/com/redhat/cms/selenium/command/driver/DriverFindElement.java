package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import com.redhat.cms.selenium.command.browser.impl.FindImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 * Example for finding an element in a page, example:
 *
 * new FindElement(By.xpath("//script[@type='text/javascript'][@src='/j/elqNow/elqCfg.js']")))
 *
 * Finds:
 *
 * <script type='text/javascript' language='JavaScript' src='/j/elqNow/elqCfg.js'></script>
 *
 *
 * /h:body/h:div/h:p (Selects <p> tags that are children of <div> tags that are children of the <body> tag)
 *
 */
public class DriverFindElement extends FindImpl<RemoteWebDriver, WebElement> {

    public DriverFindElement(String xpath) {
        super(xpath);
    }

    @Override
    public void execute(RemoteWebDriver browser) throws Exception {
        try {
            this.setFindResults(browser.findElement(By.xpath(this.getTarget())));
        } catch (Exception e) {
            assert false : "Exception - Command - " + this.toString();
        }
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("RemoteWebDriver: browser.findElement(By.xpath(");
        toString.append(super.toString());
        toString.append("))");
        return toString.toString();
    }

    @Override
    public BrowserCommand<RemoteWebDriver> getInstance() {
        return this;
    }
}
