package com.redhat.cms.selenium.command.browser.impl;

import com.redhat.cms.selenium.command.browser.Open;

public abstract class OpenImpl<B> implements Open<B>{

    private String url;

    protected OpenImpl(String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return this.url;
    }

}
