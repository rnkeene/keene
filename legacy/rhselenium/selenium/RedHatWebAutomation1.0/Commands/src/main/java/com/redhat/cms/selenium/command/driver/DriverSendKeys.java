package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.Type;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverSendKeys extends DriverFindElement implements Type<RemoteWebDriver,WebElement> {

    private String value;

    public DriverSendKeys(String xpath, String value) {
        super(xpath);
        this.value = value;
    }

    @Override
    public String getValue() {
        return this.getValue();
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void execute(RemoteWebDriver browser) throws Exception {
        super.execute(browser);
        this.getFindResults().sendKeys(this.value);
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(super.toString());
        toString.append(".sendKeys(");
        toString.append(this.value);
        toString.append(")");
        return toString.toString();
    }

}
