package com.redhat.cms.selenium.command.driver;

import com.redhat.cms.selenium.command.browser.impl.WaitImpl;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverWaitForNewPage extends WaitImpl<RemoteWebDriver> {

    private String currentPage;

    public DriverWaitForNewPage(long duration, String currentPage) {
        super(duration);
        this.currentPage = currentPage;
    }

    @Override
    public synchronized void execute(RemoteWebDriver browser) throws Exception {
        while(browser.getCurrentUrl().equals(currentPage)){
            wait(this.getWaitTime());
        }
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tRemoteWebDriver: browser.wait(");
        toString.append(super.toString());
        toString.append(")\n");
        return toString.toString();
    }

    @Override
    public DriverWaitForNewPage getInstance() {
        return this;
    }
}
