package com.redhat.cms.selenium.command.browser;

public interface Wait<B> extends BrowserCommand<B> {

    public long getWaitTime();

    public void setWaitTime(long waitTime);
}