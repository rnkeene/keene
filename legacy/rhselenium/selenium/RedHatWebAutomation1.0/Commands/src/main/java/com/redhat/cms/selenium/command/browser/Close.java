package com.redhat.cms.selenium.command.browser;

public interface Close<B> extends BrowserCommand<B> {
}
