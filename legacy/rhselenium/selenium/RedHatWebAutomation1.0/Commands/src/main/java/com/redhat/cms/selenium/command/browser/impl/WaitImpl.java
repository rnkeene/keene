package com.redhat.cms.selenium.command.browser.impl;

import com.redhat.cms.selenium.command.browser.Wait;

public abstract class WaitImpl<B> implements Wait<B>{

    private long waitTime;

    protected WaitImpl() {
    }

    protected WaitImpl(long waitTime) {
        this.waitTime = waitTime;
    }

    @Override
    public long getWaitTime() {
        return waitTime;
    }

    @Override
    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }
    
    @Override
    public String toString() {
        return Long.toString(this.waitTime);
    }
}
