package com.redhat.cms.selenium.fitnesse.screenshot.test;

import com.redhat.cms.selenium.fitnesse.action.Open;
import com.redhat.cms.selenium.fitnesse.screenshot.data.Browser;
import com.redhat.cms.selenium.fitnesse.screenshot.data.OperatingSystem;
import com.redhat.cms.selenium.fitnesse.screenshot.data.SeleniumHub;
import com.redhat.cms.selenium.fitnesse.screenshot.test.data.ScreenshotVisit;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.apache.velocity.test.misc.Test;

public class ScreenshotTest
{

    static final Logger log = Logger.getLogger(ScreenshotTest.class);
    private static List<OperatingSystem> osList = new ArrayList<OperatingSystem>();
    private static List<Browser> browserList = new ArrayList<Browser>();
    private static List<Open> actionList = new ArrayList<Open>();
    private static List<SeleniumHub> hubList = new ArrayList<SeleniumHub>();
    private boolean runTest;

    public ScreenshotTest()
    {
    }

    public static void addOperatingSystem(OperatingSystem os)
    {
        log.info("Adding Operating System: " + os);
        osList.add(os);
    }

    public static void addBrowser(Browser browser)
    {
        log.info("Adding Browser: " + browser);
        browserList.add(browser);
    }

    public static void addSeleniumHub(SeleniumHub hub)
    {
        log.info("Adding Selenium Hub: " + hub);
        hubList.add(hub);
    }

    public static void addAction(Open action)
    {
        log.info("Adding Action: " + action);
        actionList.add(action);
    }

    public boolean isRunTest()
    {
        return runTest;
    }

    public void setRunTest(boolean runTest)
    {
        this.runTest = runTest;
    }

//    @Test(dataProvider = "next", dataProviderClass = GlusterTestCaseProvider.class)
//    @Override
    public boolean runTest()
    {
        if (this.runTest)
        {
            log.info("----- !! NEW RUN !! ----- ");
            log.info("----- Operating Systems ----- ");
            for (OperatingSystem os : osList)
            {
                log.info("OS: " + os);
            }
            log.info("----- Browser List ----- ");
            for (Browser browser : browserList)
            {
                log.info("Browser: " + browser);
            }
            log.info("----- Action List ----- ");
            for (Open action : actionList)
            {
                log.info("Action: " + action);
                ScreenshotVisit visit = new ScreenshotVisit();
                visit.setLandingPage(action.getUrl());
                log.info("NEW COMMAND!!");
                log.info(visit);
            }
            log.info("----- !! DONE !! ----- ");
            return true;
        } else
        {
            log.info("----- !! TEST WILL NOT BE RUN !! ----- ");
            return false;
        }
    }
}
