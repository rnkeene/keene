package com.redhat.cms.selenium.fitnesse.screenshot.data;

import com.redhat.cms.selenium.fitnesse.screenshot.test.ScreenshotTest;

public class OperatingSystem extends Environment
{

    @Override
    public boolean add()
    {
        ScreenshotTest.addOperatingSystem(this);
        return true;
    }

    public enum Name
    {

        WINDOWS("windows"), UNIX("unix"), LINUX("linux"), MAC("mac");
        private String name;

        private Name(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }
}
