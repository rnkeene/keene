package com.redhat.cms.selenium.fitnesse.screenshot.data;

import com.redhat.cms.selenium.fitnesse.screenshot.test.ScreenshotTest;

public class Browser extends Environment
{

    @Override
    public boolean add()
    {
        ScreenshotTest.addBrowser(this);
        return true;
    }

    public enum Name
    {

        FIREFOX("firefox*"), CHROME("chrome*"), INTERNET_EXPLORER("iexplore*"), SAFARI("safari*");
        private String name;

        private Name(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }
}
