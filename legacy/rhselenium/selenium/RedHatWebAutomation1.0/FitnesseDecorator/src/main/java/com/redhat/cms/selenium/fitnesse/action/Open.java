package com.redhat.cms.selenium.fitnesse.action;

import com.redhat.cms.selenium.fitnesse.screenshot.data.ScreenshotData;
import com.redhat.cms.selenium.fitnesse.screenshot.test.ScreenshotTest;
import org.apache.log4j.Logger;

public class Open extends Action implements ScreenshotData
{

    static final Logger log = Logger.getLogger(Open.class);
    String protocol, subdomain, domain, topLevelDomain, context, resource, queryString, fragment, method;
    boolean takeScreenshot;
    int port;

    public Open()
    {
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getFragment()
    {
        return fragment;
    }

    public void setFragment(String fragment)
    {
        this.fragment = fragment;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    public String getQueryString()
    {
        return queryString;
    }

    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public String getResource()
    {
        return resource;
    }

    public void setResource(String resource)
    {
        this.resource = resource;
    }

    public String getSubdomain()
    {
        return subdomain;
    }

    public void setSubdomain(String subdomain)
    {
        this.subdomain = subdomain;
    }

    public String getTopLevelDomain()
    {
        return topLevelDomain;
    }

    public void setTopLevelDomain(String topLevelDomain)
    {
        this.topLevelDomain = topLevelDomain;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public String getUrl()
    {
        StringBuilder toString = new StringBuilder();
        toString.append(this.protocol);
        toString.append("://");
        toString.append(this.subdomain);
        toString.append(".");
        toString.append(domain);
        toString.append(".");
        toString.append(this.topLevelDomain);
        toString.append(this.context);
        toString.append(this.resource);
        if (this.queryString != null && this.queryString.length() > 0)
        {
            toString.append("?");
            toString.append(this.queryString);
        }
        if (this.fragment != null && this.fragment.length() > 0)
        {
            toString.append("#");
            toString.append(this.fragment);
        }
        log.info("getURL from 'Open': " + toString.toString());
        return toString.toString();
    }

    public boolean isScreenshot()
    {
        return takeScreenshot;
    }

    public void setTakeScreenshot(boolean takeScreenshot)
    {
        this.takeScreenshot = takeScreenshot;
    }

    @Override
    public boolean add()
    {
        ScreenshotTest.addAction(this);
        return true;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\nURL: ");
        toString.append("\n\tProtocol: ");
        toString.append(this.protocol);
        toString.append("\n\tSubdomain: ");
        toString.append(this.subdomain);
        toString.append("\n\tDomain: ");
        toString.append(this.domain);
        toString.append("\n\tTop Level Domain: ");
        toString.append(this.topLevelDomain);
        toString.append("\n\tPort: ");
        toString.append(this.port);
        toString.append("\n\tContext: ");
        toString.append(this.context);
        toString.append("\n\tResource: ");
        toString.append(this.resource);
        toString.append("\n\tQuery String: ");
        toString.append(this.queryString);
        toString.append("\n\tFragment: ");
        toString.append(this.fragment);
        toString.append("\n\tMethod: ");
        toString.append(this.method);
        return toString.toString();
    }
}
