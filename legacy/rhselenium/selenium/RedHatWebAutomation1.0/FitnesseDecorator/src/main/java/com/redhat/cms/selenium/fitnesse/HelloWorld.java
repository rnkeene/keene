package com.redhat.cms.selenium.fitnesse;

import org.apache.log4j.Logger;

public class HelloWorld
{

    static final Logger log = Logger.getLogger(HelloWorld.class);
    private String column, header, setter, getter;

    public HelloWorld()
    {
        log.info("NEW HELLO WORLD() FITNESS TEST");
    }

    public String doActionOne()
    {
        log.info("Column: " + this.column);
        log.info("Header: " + this.header);
        log.info("Setter: " + this.setter);
        log.info("Getter: " + this.getter);
        return "Action Ouput " + this.column.substring(this.column.length()-1, this.column.length());
    }
    public String doActionTwo()
    {
        log.info("Column: " + this.column);
        log.info("Header: " + this.header);
        log.info("Setter: " + this.setter);
        log.info("Getter: " + this.getter);
        int index = (this.column.indexOf("Input")+"Input".length());
        return "ActionOuput" + this.column.substring(index, this.column.length());
    }

    public String getColumn()
    {
        return column;
    }

    public void setColumn(String column)
    {
        log.info("Column: " + this.column);
        this.column = column;
    }

    public String getGetter()
    {
        return getter;
    }

    public void setGetter(String getter)
    {
        log.info("Getter: " + this.getter);
        this.getter = getter;
    }

    public String getHeader()
    {
        return header;
    }

    public void setHeader(String header)
    {
        log.info("Header: " + this.header);
        this.header = header;
    }

    public String getSetter()
    {
        return setter;
    }

    public void setSetter(String setter)
    {
        log.info("Setter: " + this.setter);
        this.setter = setter;
    }
}
