package com.redhat.cms.selenium.fitnesse.screenshot.test.data;

import com.redhat.cms.selenium.command.driver.DriverScreenshot;
import com.redhat.cms.selenium.webevent.CompositeBrowserCommand;
import com.redhat.cms.selenium.webevent.page.DriverPageVisit;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ScreenshotVisit extends DriverPageVisit{

    @Override
    public CompositeBrowserCommand<RemoteWebDriver> build()
    {
        super.build();
        this.add(new DriverScreenshot());
        return this;
    }

}
