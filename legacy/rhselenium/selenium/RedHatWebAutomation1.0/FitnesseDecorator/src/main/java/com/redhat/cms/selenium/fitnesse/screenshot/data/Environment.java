package com.redhat.cms.selenium.fitnesse.screenshot.data;

import org.apache.log4j.Logger;

public abstract class Environment implements ScreenshotData
{

    static final Logger log = Logger.getLogger(Environment.class);
    String type, name, versionName, version, release;

    public Environment()
    {
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getRelease()
    {
        return release;
    }

    public void setRelease(String release)
    {
        this.release = release;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getVersionName()
    {
        return versionName;
    }

    public void setVersionName(String versionName)
    {
        this.versionName = versionName;
    }

    @Override
    public String toString()
    {
        StringBuilder toString = new StringBuilder();
        toString.append("\nType: ");
        toString.append(this.type);
        toString.append("\nName: ");
        toString.append(this.name);
        toString.append("\nVersion Name: ");
        toString.append(this.versionName);
        toString.append("\nVersion: ");
        toString.append(this.version);
        toString.append("\nRelease: ");
        toString.append(this.release);
        return toString.toString();
    }

    public enum Name
    {

        BROWSER("browser"), OPERATING_SYSTEM("operatingSystem");
        private String name;

        private Name(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }
}
