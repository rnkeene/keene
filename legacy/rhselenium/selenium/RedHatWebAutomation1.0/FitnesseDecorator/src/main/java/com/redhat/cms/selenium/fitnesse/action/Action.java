package com.redhat.cms.selenium.fitnesse.action;

import com.redhat.cms.selenium.fitnesse.screenshot.test.ScreenshotTest;
import com.redhat.cms.selenium.fitnesse.screenshot.data.ScreenshotData;
import org.apache.log4j.Logger;

public abstract class Action implements ScreenshotData
{

    static final Logger log = Logger.getLogger(Action.class);
    private String action;

    public Action()
    {
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public enum Name
    {

        OPEN("open"), SCREENSHOT("screenshot");
        private String name;

        private Name(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }
    }
}
