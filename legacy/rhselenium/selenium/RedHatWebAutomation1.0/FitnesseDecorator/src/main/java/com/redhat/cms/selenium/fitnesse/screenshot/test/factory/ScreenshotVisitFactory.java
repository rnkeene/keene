package com.redhat.cms.selenium.fitnesse.screenshot.test.factory;

import com.redhat.cms.selenium.fitnesse.screenshot.test.ScreenshotTest;
import com.redhat.cms.selenium.fitnesse.screenshot.test.dataprovider.ScreenshotSuiteProvider;
import com.redhat.cms.selenium.webevent.test.SeleniumSuiteData;
import com.redhat.cms.selenium.webevent.test.factory.Selenium2TestFactory;
import org.testng.annotations.Factory;

public class ScreenshotVisitFactory extends Selenium2TestFactory
{

    @Factory(dataProvider = "getTestSuiteData", dataProviderClass = ScreenshotSuiteProvider.class)
    public Object[] getTestData(SeleniumSuiteData testData)
    {
        return this.build();
    }

    @Override
    public Object[] build()
    {
//        this.add(new ScreenshotTest());
        return this.toArray();
    }
}
