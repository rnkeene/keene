package com.redhat.cms.selenium.fitnesse.screenshot.test.dataprovider;

import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestSuiteProvider;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class ScreenshotSuiteProvider extends SeleniumTestSuiteProvider
{

    static final Logger log = Logger.getLogger(ScreenshotSuiteProvider.class);
    private static Iterator<Object[]> cache;

    @DataProvider(name = "getTestSuiteData", parallel = true)
    public static Iterator<Object[]> getTestData() {
        return cache;
    }
}
