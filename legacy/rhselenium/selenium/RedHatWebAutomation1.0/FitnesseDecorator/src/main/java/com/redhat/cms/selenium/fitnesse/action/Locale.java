package com.redhat.cms.selenium.fitnesse.action;

import org.apache.log4j.Logger;

public class Locale
{

     static final Logger log = Logger.getLogger(Locale.class);
    private String country;
    private String language;

    public Locale()
    {
        log.debug("Constuctor");
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        log.debug("Set Country: " + country);
        this.country = country;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        log.debug("Set Language: " + language);
        this.language = language;
    }

}
