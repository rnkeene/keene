package com.redhat.cms.selenium.fitnesse.screenshot.data;

import com.redhat.cms.selenium.fitnesse.screenshot.test.ScreenshotTest;
import com.redhat.cms.selenium.webevent.constants.HubConstants;
import org.apache.log4j.Logger;

public class SeleniumHub implements ScreenshotData{

    static Logger log = Logger.getLogger(SeleniumHub.class);

    private HubConstants constants = new HubConstants();

    public SeleniumHub() {
    }

    public String getConnectionProtocol() {
        return this.constants.getConnectionProtocol();
    }

    public void setConnectionProtocol(String connectionProtocol) {
        this.constants.setConnectionProtocol(connectionProtocol + "://");
    }

    public String getRegisterContext() {
        return this.constants.getRegisterContext();
    }

    public void setRegisterContext(String registerContext) {
        this.constants.setRegisterContext(registerContext);
    }

    public String getServerPort() {
        return this.constants.getServerPort();
    }

    public void setServerPort(String serverPort) {
        this.constants.setServerPort(serverPort);
    }

    public String getServerURL() {
        return  this.constants.getServerURL();
    }

    public void setServerURL(String serverURL) {
        this.constants.setServerURL(serverURL);
    }

    public com.redhat.cms.selenium.browser.remote.hub.SeleniumHub getSeleniumServer() {
        return this.constants.getSeleniumServer();
    }


    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tHub Properties: \n");
        toString.append(super.toString());
        return toString.toString();
    }

    @Override
    public boolean add()
    {
        ScreenshotTest.addSeleniumHub(this);
        return true;
    }

}
