package com.redhat.cms.selenium.fitnesse.screenshot.test.dataprovider;

import com.redhat.cms.selenium.fitnesse.screenshot.test.factory.ScreenshotVisitFactory;
import com.redhat.cms.selenium.webevent.dataprovider.SeleniumTestCaseProvider;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

public class ScreenshotCaseProvider extends SeleniumTestCaseProvider
{

    static Logger log = Logger.getLogger(ScreenshotCaseProvider.class);
    private static Iterator<Object[]> cache;
    private static ScreenshotCaseProvider testCases = new ScreenshotCaseProvider();

    public static void add(List<String> urlList) {
        ScreenshotVisitFactory testCaseFactory = new ScreenshotVisitFactory();
        
//        testCaseFactory.setFactoryList(urlList);
//        testCases.add(testCaseFactory);
//        cache = testCases.getInstance();
    }

    //, parallel=true
    @DataProvider(name = "next")
    public static Iterator<Object[]> next() {
        return cache;
    }
}
