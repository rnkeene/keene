package com.redhat.cms.selenium.browser.driver.factory;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverChromeFactory extends DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new ChromeDriver();
    }

}
