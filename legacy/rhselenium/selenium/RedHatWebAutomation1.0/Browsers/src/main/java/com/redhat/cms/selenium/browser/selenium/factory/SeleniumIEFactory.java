package com.redhat.cms.selenium.browser.selenium.factory;

import org.openqa.selenium.ie.InternetExplorerDriver;

public class SeleniumIEFactory extends SeleniumFactory{

    public SeleniumIEFactory() {
        super(new InternetExplorerDriver());
    }

}
