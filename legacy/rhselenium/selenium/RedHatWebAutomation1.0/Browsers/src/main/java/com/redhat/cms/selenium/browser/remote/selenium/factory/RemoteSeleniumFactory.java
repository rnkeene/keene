package com.redhat.cms.selenium.browser.remote.selenium.factory;

import com.redhat.cms.selenium.browser.remote.driver.factory.RemoteDriverFactory;
import com.redhat.cms.selenium.browser.remote.factory.RemoteBrowserFactory;
import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.WebDriverBackedSelenium;

public abstract class RemoteSeleniumFactory extends RemoteBrowserFactory<Selenium> {

    private RemoteDriverFactory remoteDriverFactory;

    RemoteSeleniumFactory(SeleniumHub seleniumHub, RemoteDriverFactory remoteDriverFactory) {
        super(seleniumHub);
        this.remoteDriverFactory = remoteDriverFactory;
    }

    @Override
    public Selenium getInstance() {
        return new WebDriverBackedSelenium(this.remoteDriverFactory.getInstance(), "http://");
    }

    public static Selenium getRemoteDriver(String browser, SeleniumHub seleniumHub){
        RemoteSeleniumFactory factory = null;
        if("*firefox".equals(browser)){
            factory = new RemoteSeleniumFirefoxFactory(seleniumHub);
        }else if("*iexplore".equals(browser)){
            factory = new RemoteSeleniumIEFactory(seleniumHub);
        }else if("*chrome".equals(browser)){
            factory = new RemoteSeleniumChromeFactory(seleniumHub);
        }
        return null;
    }

}
