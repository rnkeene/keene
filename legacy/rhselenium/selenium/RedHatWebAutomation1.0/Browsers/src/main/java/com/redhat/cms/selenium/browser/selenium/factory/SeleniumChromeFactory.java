package com.redhat.cms.selenium.browser.selenium.factory;

import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumChromeFactory extends SeleniumFactory{

    public SeleniumChromeFactory() {
        super(new ChromeDriver());
    }

}
