package com.redhat.cms.selenium.browser.remote.driver.factory;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RemoteDriverFirefoxFactory extends RemoteDriverFactory{

    public RemoteDriverFirefoxFactory(SeleniumHub hub) {
        super(hub, DesiredCapabilities.firefox());
    }

}
