package com.redhat.cms.selenium.browser.selenium.factory;

import com.redhat.cms.selenium.browser.factory.BrowserFactory;
import com.thoughtworks.selenium.Selenium;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;

public abstract class SeleniumFactory implements BrowserFactory<Selenium>{

    private WebDriver driver;

    protected SeleniumFactory() {
    }

    protected SeleniumFactory(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public Selenium getInstance() {
        return new WebDriverBackedSelenium(this.driver, "http://");
    }

}
