package com.redhat.cms.selenium.browser.remote.selenium.factory;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.redhat.cms.selenium.browser.remote.driver.factory.RemoteDriverChromeFactory;

public class RemoteSeleniumChromeFactory extends RemoteSeleniumFactory{

    public RemoteSeleniumChromeFactory(SeleniumHub hub) {
        super(hub, new RemoteDriverChromeFactory(hub));
    }

}
