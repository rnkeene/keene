package com.redhat.cms.selenium.browser.remote.selenium.factory;

import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.redhat.cms.selenium.browser.remote.driver.factory.RemoteDriverFirefoxFactory;

public class RemoteSeleniumIEFactory extends RemoteSeleniumFactory{

    public RemoteSeleniumIEFactory(SeleniumHub hub) {
        super(hub,new RemoteDriverFirefoxFactory(hub));
    }

}
