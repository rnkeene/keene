package com.redhat.cms.selenium.browser.driver.factory;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFirefoxFactory extends DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new FirefoxDriver();
    }

}
