package com.redhat.cms.selenium.browser;

/**
 *
 * This Object should be able to be decoupled from the Command object with a little bit of refactoring.
 *
 * For now it can be part of the implementations; but there is an easier way to manage the Browser than passing <B> around everywhere.
 *
 * **We'll need a 'UniversalBrowser' implementation, that will require a refactor of the way that the Command Project works and interacts with the Browser Project.
 *
 * @param <B> the browser driver type.
 */
public interface WebsiteBrowser<B> {

    public B getBrowser();

    public void setBrowser(B browser);

    /**
     *
     * //To be moved to the new Browser implementation after refactor; this object would hold a reference inistead of implementing WebsiteBrowser.
     *
     * Use this method to shutdown the <B> browser Object and make sure it has removed and destroyed all un-necessary references/Objectsd.
     *
     */
    public void shutdownBrowser();
}
