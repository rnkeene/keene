package com.redhat.cms.selenium.browser.selenium.factory;

import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumFirefoxFactory extends SeleniumFactory{

    public SeleniumFirefoxFactory() {
        super(new FirefoxDriver());
    }

}
