package com.redhat.cms.selenium.browser.driver.factory;

import com.redhat.cms.selenium.browser.factory.BrowserFactory;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class DriverFactory implements BrowserFactory<RemoteWebDriver>{

}
