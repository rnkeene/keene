package com.redhat.cms.selenium.browser.remote.driver.factory;

import com.redhat.cms.selenium.browser.remote.factory.RemoteBrowserFactory;
import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class RemoteDriverFactory extends RemoteBrowserFactory<RemoteWebDriver>{

    private DesiredCapabilities capabilities;

    RemoteDriverFactory(SeleniumHub hub, DesiredCapabilities capabilities) {
        super(hub);
        this.capabilities = capabilities;
    }

    public DesiredCapabilities getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(DesiredCapabilities capabilities) {
        this.capabilities = capabilities;
    }
    
    @Override
    public RemoteWebDriver getInstance() {
        return new RemoteWebDriver(this.getSeleniumHub().getRegisterURL(), this.capabilities);
    }

    public static RemoteWebDriver getRemoteDriver(String browser, SeleniumHub seleniumHub){
        RemoteDriverFactory factory = null;
        if("*firefox".equals(browser)){
            factory = new RemoteDriverFirefoxFactory(seleniumHub);
        }else if("*iexplore".equals(browser)){
            factory = new RemoteDriverIEFactory(seleniumHub);
        }else if("*chrome".equals(browser)){
            factory = new RemoteDriverChromeFactory(seleniumHub);
        }
        return factory.getInstance();
    }

}
