package com.redhat.cms.selenium.browser.remote.factory;

import com.redhat.cms.selenium.browser.factory.BrowserFactory;
import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;

public abstract class RemoteBrowserFactory<B> implements BrowserFactory<B> {

    private SeleniumHub seleniumHub;

    protected RemoteBrowserFactory(SeleniumHub seleniumHub) {
        this.seleniumHub = seleniumHub;
    }

    public SeleniumHub getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(SeleniumHub seleniumHub) {
        this.seleniumHub = seleniumHub;
    }
    
}
