package com.redhat.cms.selenium.browser.remote.hub;

import java.net.MalformedURLException;
import java.net.URL;

public class SeleniumHub<B>{

    private String protocol;
    private String domain;
    private String port;
    private String registerContext;

    public SeleniumHub() {
    }
    
    public SeleniumHub(String protocol, String url, String registerContext, String port) {
        this.protocol = protocol;
        this.domain = url;
        this.port = port;
        this.registerContext = registerContext;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    
    public String getDomain() {
        return domain;
    }

    public void setDomain(String url) {
        this.domain = url;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getRegisterContext() {
        return registerContext;
    }

    public void setRegisterContext(String registerContext) {
        this.registerContext = registerContext;
    }

    public URL getRegisterURL() {
        try {
            return new URL(this.protocol + this.domain + ":" + this.port + this.registerContext);
        } catch (MalformedURLException ex) {
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("New Selenium Hub:\n\t\tURL - ");
        builder.append(this.domain);
        builder.append("\n\t\tPort - ");
        builder.append(this.port);
        builder.append("\n\t\tRegister Context - ");
        builder.append(this.registerContext);
        builder.append("\n\t\tFull Registration URL - ");
        builder.append(this.getRegisterURL());
        builder.append("\n");
        return builder.toString();
    }

}
