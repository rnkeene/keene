package com.redhat.cms.selenium.browser.factory;

public interface BrowserFactory<B> {

    public B getInstance();
}
