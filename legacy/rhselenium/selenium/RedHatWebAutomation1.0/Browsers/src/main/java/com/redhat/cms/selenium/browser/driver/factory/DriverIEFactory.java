package com.redhat.cms.selenium.browser.driver.factory;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverIEFactory extends DriverFactory{

    @Override
    public RemoteWebDriver getInstance() {
        return new InternetExplorerDriver();
    }

}
