package com.redhat.cms.selenium.browser.factory;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BrowserTest {

    static Logger log = Logger.getLogger(BrowserTest.class);

    @BeforeClass
    public void start(){
        log.info("BrowserTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("BrowserTest.setUp()");
        assert true;
    }

    @Test
    public void testBrowser(){
        log.info("BrowserTest.testBrowser()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("BrowserTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("BrowserTest.finish()");
        assert true;
    }

}
