package com.redhat.cms.testcore.test.mocktest.factory;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import com.redhat.cms.testcore.dataprovider.MockRedHatTestSuiteProvider;
import com.redhat.cms.testcore.test.factory.CompositeRedHatTestFactory;
import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

/**
 *
 * The RedHatTestFactory is a List data structure that holds implementation <T> of a RedHatTest.
 *
 * The Objects that extend this RedHatTestFactory functionality needs to provide the mechanism for knowing how and when to produce particular tests.
 *
 * @param <T> the implementation of the RedHatTest that is being created by this Factory.
 */
public class MockCompositeRedHatTestFactory extends CompositeRedHatTestFactory {

    static final Logger log = Logger.getLogger(MockCompositeRedHatTestFactory.class);

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        super.initialize(redHatData);        
        redHatData.put(MockRedHatTestFactory.class, this);
    }

    /**
     *
     * The Selenium Test
     *
     * @param testData
     * @return
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockRedHatTestSuiteProvider.class)
    @Override
    public Object[] getTests(RedHatTestSuite factorySuite) {
        return super.getTests(factorySuite);
    }

    @Override
    public Object[] getInstance(RedHatTestSuite redHatData) {
        this.add(new MockRedHatTestFactory());
        return super.getInstance(redHatData);
    }

}
