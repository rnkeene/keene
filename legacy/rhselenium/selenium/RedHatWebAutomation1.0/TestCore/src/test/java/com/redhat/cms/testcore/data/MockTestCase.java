package com.redhat.cms.testcore.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;

public class MockTestCase extends RedHatTestCase{

    protected String testMessage;

    public MockTestCase() {
        this("Default Test Message");
    }

    public MockTestCase(String testMessage) {
        this.testMessage = testMessage;
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        super.initialize(input);
        input.put(MockTestCase.class, this);
    }

    public String getTestMessage() {
        return testMessage;
    }

    public void setTestMessage(String testMessage) {
        this.testMessage = testMessage;
    }

    @Override
    public String toString(){
        return this.testMessage;
    }
}
