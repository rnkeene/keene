package com.redhat.cms.testcore.dataprovider;

import com.redhat.cms.testcore.data.factory.MockRedHatTestCaseFactory;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

/**
 *
 * An Example @DataProvider Object designed to be the target of a particular set of @Test Objects.
 *
 * Two methodologies for delivering content for the @Test executions has been provided.
 *
 */
public class MockRedHatTestCaseProvider extends RedHatTestCaseProvider {

    static final Logger log = Logger.getLogger(MockRedHatTestCaseProvider.class);
    /**
     *
     * Static data initialization per TestNG requirement.
     * 
     */
    private final static MockRedHatTestCaseProvider testCaseData = new MockRedHatTestCaseProvider();
    private final static Iterator<Object[]> cache;

    static {
        testCaseData.add(new MockRedHatTestCaseFactory());
        cache = testCaseData.getInstance();
    }

    /**
     * Empty Constructor for TestNG.
     */
    private MockRedHatTestCaseProvider() {
        super();
    }

    /**
     *
     * This method ensures all Tests run every TestCases; e.g. the number of executions = #Tests x #TestCases.
     * 
     * @return a copy of the cached Iterator<Object[]> for each test to run
     */
    @DataProvider(name = "testData", parallel = true)
    public static Iterator<Object[]> getTestData() {
        return testCaseData.getInstance();
    }

    /**
     *
     * This method ensures all Tests work to finish a single workload of TestCases; e.g. the number of executions = #TestCases
     * 
     * @return cached Iterator<Object[]> for all of the Tests to share
     */
    @DataProvider(name = "testDataCache", parallel = true)
    public static Iterator<Object[]> getTestDataCache() {
        return cache;
    }
}
