package com.redhat.cms.testcore.dataprovider;

import com.redhat.cms.testcore.data.factory.MockRedHatSuiteCaseFactory;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

/**
 *
 * An Example @DataProvider Object designed to be the target of a particular set of @Factory Objects.
 *
 * Two methodologies for delivering content for the @Factory executions has been provided.
 *
 */
public class MockRedHatTestSuiteProvider extends RedHatTestSuiteProvider {

    static final Logger log = Logger.getLogger(MockRedHatTestSuiteProvider.class);
    /**
     *
     * Static data initialization per TestNG requirement.
     *
     */
    private final static MockRedHatTestSuiteProvider testSuiteData = new MockRedHatTestSuiteProvider();
    private final static Iterator<Object[]> cache;

    static {
        testSuiteData.add(new MockRedHatSuiteCaseFactory());
        cache = testSuiteData.getInstance();
    }

    /**
     * Empty Constructor for TestNG.
     */
    private MockRedHatTestSuiteProvider() {
        super();
    }

    /**
     *
     * This method ensures all Tests run every TestCases; e.g. the number of executions = #Tests x #TestCases.
     *
     * @return a copy of the cached Iterator<Object[]> for each test to run
     */
    @DataProvider(name = "suiteData", parallel = true)
    public static Iterator<Object[]> getSuiteData() {
        return testSuiteData.getInstance();
    }

    /**
     *
     * This method ensures all Tests work to finish a single workload of TestCases; e.g. the number of executions = #TestCases
     *
     * @return cached Iterator<Object[]> for all of the Tests to share
     */
    @DataProvider(name = "suiteDataCache", parallel = true)
    public static Iterator<Object[]> getSuiteDataCache() {
        return cache;
    }
}
