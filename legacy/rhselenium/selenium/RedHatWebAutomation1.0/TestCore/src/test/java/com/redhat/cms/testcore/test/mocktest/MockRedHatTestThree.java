package com.redhat.cms.testcore.test.mocktest;

import com.redhat.cms.testcore.data.RedHatTestCase;
import com.redhat.cms.testcore.data.MockTestCase;
import com.redhat.cms.testcore.dataprovider.MockRedHatTestCaseProvider;
import org.testng.annotations.Test;

public class MockRedHatTestThree extends MockRedHatTest {

    @Test(dataProvider = "testData", dataProviderClass = MockRedHatTestCaseProvider.class)
    @Override
    public void runTestMethod(RedHatTestCase runningCase) {
        super.runTestMethod(runningCase);
    }

    @Override
    public void alterTestCaseData(RedHatTestCase redHatData) {
        MockTestCase testCase = (MockTestCase) redHatData.get(MockTestCase.class);
        testCase.setTestMessage("I am RedHatTestThree!! " + testCase.getTestMessage());
    }
}
