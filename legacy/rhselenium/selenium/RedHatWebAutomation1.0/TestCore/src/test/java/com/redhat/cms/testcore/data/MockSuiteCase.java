package com.redhat.cms.testcore.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;

public abstract class MockSuiteCase extends RedHatTestSuite{

    private String suiteMessage;

    public MockSuiteCase() {
        this.suiteMessage = "Default Suite Message";
    }

    public MockSuiteCase(String suiteMessage) {
        this.suiteMessage = suiteMessage;
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(MockSuiteCase.class, this);
    }
    
    public String getSuiteMessage() {
        return suiteMessage;
    }

    public void setSuiteMessage(String suiteMessage) {
        this.suiteMessage = suiteMessage;
    }

    @Override
    public String toString(){
        return this.suiteMessage;
    }
}
