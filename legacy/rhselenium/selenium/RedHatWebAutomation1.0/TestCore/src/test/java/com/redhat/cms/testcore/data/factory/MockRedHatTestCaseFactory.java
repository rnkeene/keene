package com.redhat.cms.testcore.data.factory;

import com.redhat.cms.testcore.data.MockTestCaseOne;
import com.redhat.cms.testcore.data.MockTestCaseThree;
import com.redhat.cms.testcore.data.MockTestCaseTwo;
import com.redhat.cms.testcore.data.RedHatTestCase;
import com.redhat.cms.testcore.dataprovider.MockRedHatTestSuiteProvider;
import java.util.List;
import org.testng.annotations.Factory;

public class MockRedHatTestCaseFactory extends RedHatTestDataFactory {

    public MockRedHatTestCaseFactory() {
        super();
    }

    /**
     * 
     * Data to add to the @DataProvider master list.
     * 
     * Each Test Case added to the list during the build will execute an @Test method/constructor.
     *
     * This provides a way to quickly multiply the number of configured instances of this @Test that will be run.
     *
     * Configured properly this can cause the total number of Test executions to be {(@Factory @DataProvider list size) x (the @Test @DataProvider list size)}.
     *
     * Assuming all @Test point to the same Test @DataProvider and all @Factories point to the same Factory @DataProvider - there's two generically available options:
     *
     * [TOTAL TESTS] = (Facotry @DataProvider size x Test @DataProvider size)
     *
     *  - OR -
     *
     * [TOTAL TESTS] = (Test @DataProvider size)
     *
     * Multithreading and Concurrency control is another configuration that can be toggled at various levels or on various Objects.
     *
     * The two primary areas of difficulty is determining the breakdown of threads that are running, and identifying code that isn't thread safe.
     * 
     * @return List<Object>
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockRedHatTestSuiteProvider.class)
    @Override
    public List<Object[]> build() {
        super.build();

        RedHatTestCase testOne = new MockTestCaseOne();
        testOne.initialize(testOne);
        this.add(testOne);

        RedHatTestCase testTwo = new MockTestCaseTwo();
        testTwo.initialize(testTwo);
        this.add(testTwo);

        RedHatTestCase testThree = new MockTestCaseThree();
        testThree.initialize(testThree);
        this.add(testThree);

//        RedHatTestData testDataTwo = new TestTwo();
//        testDataTwo.initialize(testDataTwo);
//        this.add(testDataTwo);

//        RedHatTestData testDataThree = new TestThree();
//        testDataThree.initialize(testDataThree);
//        this.add(testDataThree);

//        RedHatTestData ctestData = new TestOne(" --- CUSTOM ONE --- 11111");
//        ctestData.initialize(ctestData);
//        this.add(ctestData);

//        RedHatTestData ctestDataTwo = new TestTwo(" --- CUSTOM TWO --- 22222");
//        ctestDataTwo.initialize(ctestDataTwo);
//        this.add(ctestDataTwo);

//        RedHatTestData ctestDataThree = new TestThree(" --- CUSTOM THREE --- 33333");
//        ctestDataThree.initialize(ctestDataThree);
//        this.add(ctestDataThree);

        return this;
    }
}
