package com.redhat.cms.testcore.data;

public class MockTestCaseOne extends MockTestCase {

    public MockTestCaseOne() {
        super("Default TestOne Message - 11111");
    }

    public MockTestCaseOne(String testMessage) {
        super(testMessage + " - 11111");
    }
}
