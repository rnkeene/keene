package com.redhat.cms.testcore.data;

public class MockSuiteCaseTwo extends MockSuiteCase {

    public MockSuiteCaseTwo() {
        super("Default SuiteTwo Message - 22222");
    }

    public MockSuiteCaseTwo(String suiteMessage) {
        super(suiteMessage + " - 22222");
    }
}
