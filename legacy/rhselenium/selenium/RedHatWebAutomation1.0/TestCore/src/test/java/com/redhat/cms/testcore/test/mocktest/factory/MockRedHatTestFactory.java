package com.redhat.cms.testcore.test.mocktest.factory;

import com.redhat.cms.testcore.test.mocktest.factory.MockCompositeRedHatTestFactory;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import com.redhat.cms.testcore.data.MockSuiteCase;
import com.redhat.cms.testcore.dataprovider.MockRedHatTestSuiteProvider;
import com.redhat.cms.testcore.test.factory.RedHatTestFactory;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTest;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTestOne;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTestThree;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTestTwo;
import org.apache.log4j.Logger;
import org.testng.annotations.Factory;

/**
 *
 * The RedHatTestFactory is a List data structure that holds implementation <T> of a RedHatTest.
 *
 * The Objects that extend this RedHatTestFactory functionality needs to provide the mechanism for knowing how and when to produce particular tests.
 *
 * @param <T> the implementation of the RedHatTest that is being created by this Factory.
 */
public class MockRedHatTestFactory extends RedHatTestFactory<MockRedHatTest> {

    static final Logger log = Logger.getLogger(MockCompositeRedHatTestFactory.class);

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        super.initialize(redHatData);
        redHatData.put(MockRedHatTestFactory.class, this);
    }

    /**
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockRedHatTestSuiteProvider.class)
    @Override
    public Object[] getTests(RedHatTestSuite factorySuite) {
        return super.getTests(factorySuite);
    }

    /**
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Override
    public Object[] build() {
        this.add(new MockRedHatTestOne());
        this.add(new MockRedHatTestTwo());
        this.add(new MockRedHatTestThree());
        return this.toArray();
    }
}
