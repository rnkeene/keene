package com.redhat.cms.testcore.data;

public class MockSuiteCaseOne extends MockSuiteCase{

    public MockSuiteCaseOne() {
        super("Default SuiteOne Message - 11111");
    }

    public MockSuiteCaseOne(String suiteMessage) {
        super(suiteMessage + " - 11111");
    }
}
