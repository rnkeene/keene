package com.redhat.cms.testcore.test.mocktest;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.testcore.data.RedHatTestCase;
import com.redhat.cms.testcore.data.MockTestCase;
import com.redhat.cms.testcore.test.RedHatTest;
import org.apache.log4j.Logger;

public abstract class MockRedHatTest extends RedHatTest {

    static final Logger log = Logger.getLogger(MockRedHatTest.class);

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        super.initialize(redHatData);
        redHatData.put(MockRedHatTest.class, this);
    }

    @Override
    public void runTestMethod(RedHatTestCase redHatTest) {
        this.alterTestCaseData(redHatTest);
        MockTestCase test = (MockTestCase) redHatTest.get(MockTestCase.class);
    }

    public abstract void alterTestCaseData(RedHatTestCase redHatTest);

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" - MockRedHatTest - ");
        return toString.toString();
    }
}
