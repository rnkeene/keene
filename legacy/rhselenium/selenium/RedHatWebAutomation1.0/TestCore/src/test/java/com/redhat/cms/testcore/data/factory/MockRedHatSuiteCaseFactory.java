package com.redhat.cms.testcore.data.factory;

import com.redhat.cms.testcore.data.MockSuiteCaseOne;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import java.util.List;

public class MockRedHatSuiteCaseFactory extends RedHatTestDataFactory {

    public MockRedHatSuiteCaseFactory() {
        super();
    }

    /**
     *
     * Data to add to the @DataProvider master list.
     *
     * Each Suite Case added to the list during the build will execute the @Factory method/constructor.
     *
     * This provides a way to quickly multiply the number of tests created by the @Factory - but also have the option of altering configurations to support the load.
     *
     * @return List<Object>
     */
    @Override
    public List<Object[]> build() {
        super.build();

        RedHatTestSuite suiteData = new MockSuiteCaseOne();
        suiteData.initialize(suiteData);
        this.add(suiteData);

//        RedHatSuiteData suiteDataTwo = new SuiteTwo();
//        suiteDataTwo.initialize(suiteDataTwo);
//        this.add(suiteDataTwo);

//        RedHatSuiteData suiteDataThree = new SuiteThree();
//        suiteDataThree.initialize(suiteDataThree);
//        this.add(suiteDataThree);

//        RedHatSuiteData suiteDataOneTwo = new SuiteThree();
//        suiteDataOneTwo.initialize(suiteDataOneTwo);
//        this.add(suiteDataOneTwo);

        return this;
    }
}
