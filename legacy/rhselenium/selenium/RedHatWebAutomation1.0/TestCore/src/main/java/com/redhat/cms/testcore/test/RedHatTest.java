package com.redhat.cms.testcore.test;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.datasource.RedHatData;
import com.redhat.cms.testcore.data.RedHatTestCase;
import org.apache.log4j.Logger;

/**
 *
 * A blank template Test Object to create a common root for services, containers and implementations to reference.
 *
 * A RedHatTest will use RedHatData from an @DataProvider to populate it's requisite data for execution.
 *
 * An @Factory will provide a collection of instances of the RedHatTest that will run in multi-threaded environment, for this reason it is important RedHatTest Objects are thread safe.
 *
 */
public abstract class RedHatTest implements RedHatData {

    static Logger log = Logger.getLogger(RedHatTest.class);

    private RedHatCompositeData redHatData;

    /**
     *
     * Provies a way to put off some of the initializiation required for this test, to be run later (Just in Time).
     *
     * @param input an instance of the Test Case data.
     */
    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(RedHatTest.class, this.redHatData);
    }

    /**
     *
     * Each @Test from a @Factory will use a thread from the thread-pool to run the @Tests concurrently; limited by either the @Factory's @Test List size or the thread-pool set by TestNG.
     *
     * For each item returned from the @Test dataProvider reference the same rules will apply as to the @Factory, although instead of using the thread-pool it will use the data-provider-thread-pool.
     *
     * Each thread will simultaniously access the @Test method within the Same Instance of @Test Object; so it is important to consider whether or not this class/method is capable of being executed in a multithreaded way.
     *
     * It is possible to toggle - parallel=[true|false] - within the @DataProvider() parameters.
     *
     * @param runningCase is one Object from the list of data returned from the dataProvider.
     * @Test(dataProvider = "testData", dataProviderClass = MockRedHatTestCase.class)
     */
    public abstract void runTestMethod(RedHatTestCase redHatTest);
}
