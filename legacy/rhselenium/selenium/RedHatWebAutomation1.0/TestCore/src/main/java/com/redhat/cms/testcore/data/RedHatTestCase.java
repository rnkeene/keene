package com.redhat.cms.testcore.data;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import org.apache.log4j.Logger;

/**
 *
 * Generic Base TestData Container
 * 
 */
public abstract class RedHatTestCase extends RedHatCompositeData {

    static final Logger log = Logger.getLogger(RedHatTestCase.class);

    public RedHatTestCase() {
        super();
    }

    /**
     *
     * See RedHatData documentation.
     *
     * Default self-initialization stores this Object and any related dependencies to the CompositeData map for later use.
     *
     * @param input RedHatCompositeData an implementation of the Composite Pattern using a Map as the data structure.
     */
    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(RedHatTestCase.class, this);
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(" = RedHatTestData = ");
        //toString.append(super.toString());
        return toString.toString();
    }
}
