package com.redhat.cms.testcore.dataprovider;

/**
 *
 * Objects extending this class functionality will ultimately serve as a @DataProvider for a @Factory.
 *
 * Optionally, can create Lazy loading and finer grained control/flexibility by implementing a RedHatIterator that implements the <Iterator<Object[]>> method and overrides the next() method.
 *
 */
public abstract class RedHatTestSuiteProvider extends RedHatTestDataProvider {
}
