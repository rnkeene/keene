package com.redhat.cms.testcore.test.factory;

import com.redhat.cms.selenium.pattern.Factory;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.datasource.RedHatData;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * The CompositeRedHatTestFactory is a List data structure that holds implementation RedHatTestFactory<T> where <T> is a RedHatTest.
 *
 * The Objects that extend this CompositeRedHatTestFactory functionality need to provide the RedHatTestFactory<T> instances, or a controller needs to.
 *
 */
public abstract class CompositeRedHatTestFactory extends ArrayList<RedHatTestFactory> implements Factory<Object[], RedHatTestSuite>, RedHatData {

    static final Logger log = Logger.getLogger(CompositeRedHatTestFactory.class);

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        redHatData.put(CompositeRedHatTestFactory.class, this);
    }

    /**
     *
     * The Selenium Tests
     *
     * @param redHatData the TestCase data needed to populate the Object[].
     * @return Object[] the list of Tests.
     */
    public Object[] getTests(RedHatTestSuite redHatData) {
        this.initialize(redHatData);
        return this.getInstance(redHatData);
    }

    /**
     *
     * Overrides build[]
     *
     * The build method is designed to create an T of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @param redHatData the TestCase data needed to populate the Object[].
     * @return Object[] the list of Tests.
     */
    @Override
    public Object[] getInstance(RedHatTestSuite redHatData) {
        List testFactories = new ArrayList();
        for (RedHatTestFactory testFactory : this) {
            testFactories.addAll(Arrays.asList(testFactory.getTests(redHatData)));
        }
        return testFactories.toArray();
    }
}
