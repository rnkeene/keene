package com.redhat.cms.testcore.test;

import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.datasource.RedHatData;
import com.redhat.cms.testcore.data.RedHatTestCase;
import java.util.ArrayList;

/**
 *
 * A collection of blank template Test Objects.
 *
 * Each CompositeRedHatTest will use RedHatData from an @DataProvider to populate it's requisite data for execution.
 *
 * An @Factory will provide a collection of instances of the RedHatTest that will run in multi-threaded environment, for this reason it is important RedHatTest Objects are thread safe.
 *
 */
public abstract class CompositeRedHatTest extends ArrayList<RedHatTest> implements RedHatData {

    private RedHatCompositeData redHatData;

    /**
     *
     * Provies a way to put off some of the initializiation required for this test, to be run later (Just in Time).
     *
     * @param input an instance of the Test Case data.
     */
    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(CompositeRedHatTest.class, this.redHatData);
    }

    /**
     *
     * Each @Test from a @Factory will use a thread from the thread-pool to run the @Tests concurrently; limited by either the @Factory's @Test List size or the thread-pool set by TestNG.
     *
     * For each item returned from the @Test dataProvider reference the same rules will apply as to the @Factory, although instead of using the thread-pool it will use the data-provider-thread-pool.
     *
     * Each thread will simultaniously access the @Test method within the Same Instance of @Test Object; so it is important to consider whether or not this class/method is capable of being executed in a multithreaded way.
     *
     * It is possible to toggle - parallel=[true|false] - within the @DataProvider() parameters.
     *
     * @param runningCase is one Object from the list of data returned from the dataProvider.
     * @Test(dataProvider = "testData", dataProviderClass = MockRedHatTestCase.class)
     */
    public void runTestMethod(RedHatTestCase redHatTest) {
        for (RedHatTest test : this) {
            test.runTestMethod(redHatTest);
        }
    }
}
