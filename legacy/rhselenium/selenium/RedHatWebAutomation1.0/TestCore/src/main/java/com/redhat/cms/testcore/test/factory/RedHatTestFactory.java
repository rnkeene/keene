package com.redhat.cms.testcore.test.factory;

import com.redhat.cms.selenium.pattern.Builder;
import com.redhat.cms.selenium.datasource.RedHatCompositeData;
import com.redhat.cms.selenium.datasource.RedHatData;
import com.redhat.cms.testcore.data.RedHatTestSuite;
import com.redhat.cms.testcore.test.RedHatTest;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * The RedHatTestFactory is a List data structure that holds implementation <T> of a RedHatTest.
 *
 * The Objects that extend this RedHatTestFactory functionality needs to provide the mechanism for knowing how and when to produce particular tests.
 *
 * @param <T> the implementation of the RedHatTest that is being created by this Factory.
 */
public class RedHatTestFactory<T extends RedHatTest> extends ArrayList<T> implements Builder<Object[]>, RedHatData {

    static final Logger log = Logger.getLogger(RedHatTestFactory.class);

    @Override
    public void initialize(RedHatCompositeData redHatData) {
        redHatData.put(RedHatTestFactory.class, this);
    }

    /**
     *
     * The Selenium Test
     *
     * @param testData
     * @return Object[] the list of test suites.
     *
     * @Factory(dataProvider="Xyz" class=asldk.class);
     */
    public Object[] getTests(RedHatTestSuite testData) {
        this.initialize(testData);
        return this.build();
    }

    /**
     * 
     * The build method is designed to create an Object[] of Tests that are alotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return Object[] the list of test suites.
     */
    @Override
    public Object[] build() {
        this.clear();
        return this.toArray();
    }
}
