package com.redhat.cms.selenium.datasource;

import com.redhat.cms.selenium.pattern.Builder;
import com.redhat.cms.selenium.pattern.InputInitializer;

public class RedHatDataSource<D extends RedHatData> implements Builder<RedHatData>, InputInitializer<RedHatCompositeData>, RedHatData {

    private D redHatData;

    @Override
    public RedHatData build() {
        return this.redHatData;
    }

    @Override
    public void initialize(RedHatCompositeData input) {
        input.put(RedHatDataSource.class, this);
    }

    public D getRedHatData() {
        return redHatData;
    }

    public void setRedHatData(D redHatData) {
        this.redHatData = redHatData;
    }
}
