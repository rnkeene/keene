package com.redhat.cms.selenium.datasource;

import java.util.HashMap;
import java.util.Set;
import org.apache.log4j.Logger;

/**
 *
 * *** An implementation will need to be made for both your SuiteData and TestData for each unique at @Factory and @Test as appropriate.
 * *** The implementations are responsible for being the container for the RedHatData - e.g. the SuiteData and TestData (implementations).
 *
 * This class is designed to work in unison with RedHatData to allow a collection of data to be connected without explicitly references.
 * Dependend Objects will be pulled as required during the initialization phase, so the order items are added can matter.
 *
 * If a RedHatData implementation needs access to another piece of RedHatData then it accesses that particular Object from this container.
 * Only RedHatData is allowed into the CompositeData Map, so any additional required Object references will need to be contained within the RedHatData imlementation (or implement the RedHatData interface).
 *
 * Any number of RedHatData Objects may be created and added to the container during the initialization phase of any particular RedHatData implementation.
 *
 * **This is similar to an implementation of the Chain of Responsibility Pattern, where an Object is passed down a line of executions that are un-aware of one another but can all modify a target object on their 'turn'.
 * **e.g. each object gets it's chance to initialize the container object, but it is not aware of when it is created and added relative to other RedHatData Objects.
 *
 */
public abstract class RedHatCompositeData extends HashMap<Class, RedHatData> implements RedHatData {

    static final Logger log = Logger.getLogger(RedHatCompositeData.class);
    /**
     * This represents the base node of every Map of RedHatData, and is responsible for adding holding the data collected by a @DataProvider and provide a single logical container/package to deliver to any particular test.
     *
     * In this way we can unify the input parameters of all tests to take a generic and universal Object; that can be customized as desired/required for any particular implementation of a Test.
     *
     * @param redHatData the Map that needs to hold the reference to .this Object; almost certainly adding a self-reference.
     */
    @Override
    public abstract void initialize(RedHatCompositeData redHatData);

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\n == RedHatCompositeData == \n\t - ROOT MAP - ");
        Set<Class> keys = this.keySet();
        for (Class key : keys) {
            if (key != RedHatCompositeData.class) {
                toString.append("\n\t\t .. Key: ");
                toString.append(key);
                toString.append("\n\t\t .. Value: ");
                RedHatData rhd = this.get(key);
                if(rhd!=this){
                    toString.append(rhd);
                }
            }
        }
        return toString.toString();
    }
}
