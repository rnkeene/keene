package com.redhat.cms.selenium.datasource;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DataSourceBridgeTest {

    static Logger log = Logger.getLogger(DataSourceBridgeTest.class);

    @BeforeClass
    public void start() {
        log.info("DataSourceBridgeTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp() {
        log.info("DataSourceBridgeTest.setUp()");
        assert true;
    }

    @Test()
    public void testPattern() {
        log.info("DataSourceBridgeTest.testPattern()");
        assert true;
    }

    @AfterMethod
    public void tearDown() {
        log.info("DataSourceBridgeTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish() {
        log.info("DataSourceBridgeTest.finish()");
        assert true;
    }
}
