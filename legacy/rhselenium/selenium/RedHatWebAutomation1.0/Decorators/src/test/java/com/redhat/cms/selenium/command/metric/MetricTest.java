package com.redhat.cms.selenium.command.metric;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MetricTest{

    static Logger log = Logger.getLogger(MetricTest.class);

    @BeforeClass
    public void start(){
        log.info("MetricTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("MetricTest.setUp()");
        assert true;
    }

    @Test
    public void testMetrics(){
        log.info("MetricTest.testMetrics()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("MetricTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("MetricTest.finish()");
        assert true;
    }
    
}
