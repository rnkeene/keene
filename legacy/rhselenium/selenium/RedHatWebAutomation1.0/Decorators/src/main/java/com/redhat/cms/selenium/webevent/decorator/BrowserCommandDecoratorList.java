package com.redhat.cms.selenium.webevent.decorator;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import com.redhat.cms.selenium.pattern.impl.DecoratorList;

public class BrowserCommandDecoratorList<C extends BrowserCommand> extends DecoratorList<C> {

    public BrowserCommandDecoratorList() {
    }

    public BrowserCommandDecoratorList(C first) {
        super(first);
    }
    
}