package com.redhat.cms.selenium.command.decorator.assertion;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import com.redhat.cms.selenium.webevent.decorator.BrowserCommandDecorator;

/**
 *
 * See CommandDecorator Documentation.
 *
 * MetricDecorator serves as the template for other Assertion Decorators to follow.
 * 
 * @param <B> represents the Browser that will be used for the current BrowserCommand <C> when it runs execute();
 * @param <C> represents the Command that will be wrapped by this AssertionDecorator.
 * @param <A> the AssertionDecorator being run.
 */
public class AssertionDecorator<B, C extends BrowserCommand<B>, A extends AssertionDecorator> extends BrowserCommandDecorator<B> {

    public AssertionDecorator(C command) {
        super(command);
    }

    /**
     *
     * This method Overrides the getInstance functionality so that this Object returns the appropriate Object type.
     *
     * @param command the Command implementation <C> that is being wrapped by this AssertionDecorator.
     * @return an instance of an AssertionDecorator <A> wrapping the Command parameter provided.
     */
    @Override
    public BrowserCommand<B> getInstance() {
        return (BrowserCommand<B>) this;
    }

    /**
     *
     * There may be any number of assertions run at any given time, so this implementation does not provide a solution for a completely flexible assertion framework.
     *
     * @throws AssertionError
     */
    public void runAssertion() throws AssertionError {
        assert (this.getCommand() != null);
    }

    /**
     *
     * This method is a psudo=implementation to provide an example for other implementations, or classes extending AssertionDecorator.
     * 
     * @return the results of running build() on the the Command Object passed into this AssertionDecorator.
     */
    @Override
    public C build() {
        //Here is an opportunity to make assertions.
        //this.runAssertion();
        super.build();
        //Here is an opportunity to make assertions.
        //this.runAssertion();
        return (C)this.getResults();
    }

    /**
     *
     * This method is a psudo=implementation to provide an example for other implementations, or classes extending AssertionDecorator.
     *
     */
    @Override
    public void execute(B browser) throws Exception {
        //Here is an opportunity to make assertions.
        //this.runAssertion();
        super.execute(browser);
        //here is an opportunity to make assertions.
        //this.runAssertion();
    }

    /**
     *
     * This method is a psudo=implementation to provide an example for other implementations, or classes extending AssertionDecorator.
     *
     * @return
     */
    @Override
    public BrowserCommand<B> getResults() {
        return super.getResults();
    }
}
