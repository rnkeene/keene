package com.redhat.cms.selenium.command.decorator.metric;

import com.redhat.cms.selenium.command.decorator.CommandDecorator;
import com.redhat.cms.selenium.pattern.Command;
/**
 *
 * See CommandDecorator Documentation.
 *
 * MetricDecorator serves as the template for other Metrics Decorators to follow.
 *
 * @param <B> represents the Browser that will be used for the current BrowserCommand <C> when it runs execute();
 * @param <C> represents the Command that will be wrapped by this MetricDecorator.
 * @param <M> the MetricDecorator being run.
 */
public abstract class MetricDecorator<B, C extends Command<B,C>, M extends MetricDecorator> extends CommandDecorator<B,C, M> {

    public MetricDecorator() {
        //careful -- empty default decorator, could cause null pointer exceptions.
    }

    public MetricDecorator(C command) {
        super(command);
    }
    /**
     *
     * This method Overrides the getInstance functionality so that this Object returns the appropriate Object type.
     *
     * @param command the Command implementation <C> that is being wrapped by this MetricDecorator.
     * @return an instance of a MetricDecorator <M> wrapping the Command parameter provided.
     */
    @Override
    public abstract M getInstance(C command);

    /**
     *
     * The build method for CommandMetrics is not typically Overridden, because the commands are already loaded into the target Command held by this Metric (packaged inside the decorator already.
     *
     * There may be instances where having additional Commands will provide important information for collecting the Metrics needed around the executing command.
     *
     * This could range in commands that have no impact on the wrapped command, to altering the conditions under which it runs; or even replace it all together.
     *
     * Each Metric wraps the target Command in such a way that subsequent weappers can only tell they are wrapping the command, when in actuality they may be wrapping a wrapper.
     *
     * By looking into the details of the command oe other Wrappers certain info could possibly be ready to be added to the metrics during the build() method execution; although unlikely.
     *
     * @return the Browser Command provided to the constructor with additional wrappers as desired for this particular BrowserCommand.
     */
    @Override
    public C build() {
        return (C) super.build();
    }

    /**
     *
     * The execute method represents the best time for the Metrics object to provide any pre-population, initializations, or data collection that needs to take place..
     *
     * Since the wrappers represent a composite pattern, they get the opportunity to Override any method that the target Command is otherwise responsible for.
     *
     * It is possible that two wrappers could otherwise corrupt the data for other Metrics wrappers if it is modifying shared resources such as the target Browser Command.
     *
     * This provides a way to add control over any number of aspects that could need to be managed before execution.
     *
     * In this case Metric data collection before and after execution as desired.
     *
     * It is typically extremely important to ensure that overring implementations call super.execute() or it could cause fatal errors in test execution, the exception is if attention is made to create a Wrapper is purposely control execution.
     *
     * @param browser the typical <B> browser instance/implementation that will be responsible for executing the WebCommands collected.
     * @throws Exception any possibly Exception that my be throw, the Wrapper is only responsible for handling errors that it may be the cause of.
     */
    @Override
    public void execute(B browser) throws Exception {
        //Setup any metric data pre execution.
        this.getCommand().execute(browser);
        //Setup any metrics data post execution.
    }
    
}
