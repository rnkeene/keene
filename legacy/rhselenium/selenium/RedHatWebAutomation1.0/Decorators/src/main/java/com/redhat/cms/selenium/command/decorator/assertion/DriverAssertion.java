package com.redhat.cms.selenium.command.decorator.assertion;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * This is the Driver Assertion that will work with the RemoteWebDriver, the reason being that insome cases additional commands are required to complete the Assertion.
 *
 * @param <C> the Command that needs Assertions added to it
 */
public class DriverAssertion<C extends BrowserCommand<RemoteWebDriver>> extends AssertionDecorator<RemoteWebDriver,C, DriverAssertion<C>>{

    public DriverAssertion(C command) {
        super(command);
    }
}
