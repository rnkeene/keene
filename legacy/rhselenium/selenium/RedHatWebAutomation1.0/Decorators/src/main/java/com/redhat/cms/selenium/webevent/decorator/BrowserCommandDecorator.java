package com.redhat.cms.selenium.webevent.decorator;

import com.redhat.cms.selenium.command.browser.BrowserCommand;
import com.redhat.cms.selenium.command.decorator.CommandDecorator;
import org.apache.log4j.Logger;

public class BrowserCommandDecorator<B> extends CommandDecorator<B, BrowserCommand<B>, BrowserCommandDecorator<B>> implements BrowserCommand<B> {

    static Logger log = Logger.getLogger(BrowserCommandDecorator.class);
//    private DecoratorImpl<BrowserCommand<B>> decorator = new DecoratorImpl<BrowserCommand<B>>();

    protected BrowserCommandDecorator() {
    }

    public BrowserCommandDecorator(BrowserCommand<B> command) {
        log.info("START - added Command to decorator");
        log.info("\tinitial command - " + command);
        this.setCommand(command);
        log.info("\tthis.get command - " + this.getCommand());
        log.info("DONE - added Command to decorator");
    }

    @Override
    public void execute(B browser) throws Exception {
        this.getCommand().execute(browser);
    }

    @Override
    public BrowserCommand<B> getInstance() {
        return (BrowserCommand<B>) this;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tCommand Wrapper:");
        toString.append(this.getCommand());
        return toString.toString();
    }
}
