package com.redhat.cms.selenium.command.decorator.metric;

import com.redhat.cms.selenium.pattern.Command;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * This class is a concrete implementation of the MetricDecorator.
 *
 * It provies minimal miscelanious information about the details of a Command execution.
 *
 * @param <B> represents the Browser that will be used for the current BrowserCommand <C> when it runs execute();
 * @param <C> represents the Command that will be wrapped by this MetricDecorator.
 */
public class TimerMetric<B, C extends Command<B, C>> extends MetricDecorator<B, C, TimerMetric<B, C>> {

    private long duration;

    public TimerMetric() {
    }

    public TimerMetric(C command) {
        super(command);
    }

    @Override
    public TimerMetric<B, C> getInstance(C command) {
        return new TimerMetric<B, C>(command);
    }
    
    public long getDuration() {
        return duration;
    }

    @Override
    public void execute(B browser) throws Exception{
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        super.execute(browser);
        stopwatch.stop();
        this.duration = stopwatch.getTime();
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("Timer Metrics - ");
        toString.append("\n\tDuration: ");
        toString.append(this.getDuration());
        toString.append("\n");
        toString.append(super.toString());
        return toString.toString();
    }


}
