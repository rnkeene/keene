package com.redhat.cms.selenium.command.decorator.assertion;

import com.redhat.cms.selenium.command.driver.DriverFindElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * See CommandDecorator Documentation
 *
 * The AssertFindTestDecorator serves as the template for other Assertion Decorators to follow.
 *
 */
public class AssertFindText extends DriverAssertion<DriverFindElement> {

    static Logger log = Logger.getLogger(AssertFindText.class);

    private String match;

    public AssertFindText(String xpath, String match) {
        this(new DriverFindElement(xpath),match);
    }

    public AssertFindText(DriverFindElement command, String match) {
        super(command);
        this.match = match;
    }

    @Override
    public void execute(RemoteWebDriver browser) throws Exception {
        super.execute(browser);
        this.runAssertion();
    }

    @Override
    public void runAssertion() {
        super.runAssertion();
        assert (this.match != null) : "No value to match against";
        assert (((DriverFindElement)this.getResults()).getFindResults() != null) : "Find Element Results does not exist";
        assert (((DriverFindElement)this.getResults()).getFindResults().getText() != null) : "Find Element Results does not contain text";
        assert (((DriverFindElement)this.getResults()).getFindResults().getText()).contains(this.match) : "Find Element Results do not match";        
    }
}