package com.redhat.cms.selenium.command.decorator;

import com.redhat.cms.selenium.command.browser.pattern.CommandBuilder;
import com.redhat.cms.selenium.command.browser.pattern.CommandFactory;
import com.redhat.cms.selenium.command.browser.pattern.CommandResults;
import com.redhat.cms.selenium.command.browser.pattern.CompositeCommand;
import com.redhat.cms.selenium.pattern.Command;
import com.redhat.cms.selenium.pattern.impl.DecoratorImpl;
import org.apache.log4j.Logger;

/**
 *
 * This class is available when an Object wants to quickly wrap a Command in order to implement the Decoratory Pattern characteristics.
 *
 * This means that it is possible to attach additional control over an Object dynamically, and without modifying the target Object.
 *
 * This also helps encapsulate responsibilities of an Object even if the flexibility is not required.
 *
 * It is very important that the CommandDecorator is transparent and appears to other Objects to be the Command it is holding.
 *
 * **If an Object already extends another class, and cannot use this one to wrap a Command, then apply an Adapter Pattern.
 * **The Adapter pattern provides the same functionality that is found here but is implimented inside of the Decorating Object (or that Object could hold an instance of CommandDecorator).
 *
 * The CommandDecorator is designed to add flexibility to Command Objects as an alternative to subclasses, and is good practice for OO design.
 *
 * This "stub" Object provides a default implementation that fulfills the requirments for properly implementing the Command interface and follow the Decorator Pattern.
 *
 * This Object holds an instance of the Command via the DecoratorImpl Object (which has getTarget() and setTarget() as accessors/mutators for the wrapped Command Object).
 *
 * It is up to each Decorator implementation to complete the implementation of it's functionality related to the Command API.
 *
 * @param <B> The browser that is used to execute command.
 * @param <C> the Command that will use <B> the browser to run execute.
 */
public class CommandDecorator<B, C extends Command<B, C>, D extends CommandDecorator> implements Command<B, C>, CommandFactory<C, D>, CommandBuilder<C>, CommandResults<C> {

    static Logger log = Logger.getLogger(CommandDecorator.class);
    /**
     * This Object represents the Wrapped Object <C> that is being held by <D>; it would also have been possible to extend this decorator but then it would have made this Decorator more rigid.
     */
    private DecoratorImpl<C> decorator = new DecoratorImpl<C>();

    public CommandDecorator() {
    }

    public CommandDecorator(C command) {
        this.decorator.setTarget(command);
    }

    /**
     * This method Overrides the getInstance functionality so that this Object returns an Instance of itself.
     *
     * @param command the Command implementation <C> that is being wrapped by this CommandDecorator.
     * @return an instance of a CommandDecorator wrapping the Command parameter provided.
     */
    @Override
    public D getInstance(C command) {
        return (D) new CommandDecorator(command);
    }

    /**
     *
     * Accessor to the wrapped Command <C>.
     *
     * @return <C> the Command wrapped by this Decorator.
     */
    public C getCommand() {
        return this.decorator.getTarget();
    }

    /**
     *
     * Mutator for the wrapped Command <C>.
     *
     */
    public void setCommand(C command) {
        this.decorator.setTarget(command);
    }

    /**
     *
     * Responsible to completing part of the Decorator Pattern requirements, a default implementation for the execute(B browser) method that hands off to the Decorator.
     *
     * @param browser the browser <B> that the Command <C> held by the Decorator will execute a command on the browser.
     * @throws Exception any exception thrown by the Command during execution.
     */
    @Override
    public void execute(B browser) throws Exception {
        this.getCommand().execute(browser);
    }

    /**
     *
     * Since the Command could be a CompositeCommand it is important to provide support.
     *
     * Responsible to completing part of the Decorator Pattern requirements, a default implementation for the build() method that hands off to the Decorator.
     *
     */
    @Override
    public C build() {
        if (this.isCompositeCommand()) {
            return (C) this.getCompositeCommand().build();
        }
        return (C) this.decorator.getTarget();
    }

    /**
     *
     * Since the Command could be a CompositeCommand it is important to provide support.
     *
     * Responsible to completing part of the Decorator Pattern requirements, a default implementation for the build() method that hands off to the Decorator.
     *
     */
    @Override
    public C getResults() {
        if (this.isCompositeCommand()) {
            return (C) this.getCompositeCommand().getResults();
        }
        return this.decorator.getTarget();
    }

    /*
     *
     * Custom method to check of the Command object being wrapped is an instance of the CompositeCommand, and not just a normal Command.
     *
     */
    private boolean isCompositeCommand() {
        if (this.decorator.getTarget().getClass() == CompositeCommand.class) {
            return true;
        }
        return false;
    }

    /*
     *
     * Custom method toget the wrapped Object as an instance of the CompositeCommand (if it is one), and not just the normal Command.
     *
     * This method is used to help provide functionality to other methods in this class that are overriding Command functionality.
     *
     */
    private CompositeCommand getCompositeCommand() {
        if (isCompositeCommand()) {
            return (CompositeCommand) this.decorator.getTarget();
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("\tCommand Wrapper:");
        toString.append(this.getCommand());
        return toString.toString();
    }
}
