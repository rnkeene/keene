package com.redhat.cms.selenium.command.decorator.metric;

import com.redhat.cms.selenium.pattern.Command;
import java.util.Calendar;

public class StartAndFinish<B, C extends Command<B, C>> extends MetricDecorator<B, C, StartAndFinish<B, C>>{

    private long startTime;
    private long finishTime;

    public StartAndFinish() {
    }

    public StartAndFinish(C command) {
        super(command);
    }

    @Override
    public StartAndFinish<B, C> getInstance(C command) {
        return new StartAndFinish<B, C>(command);
    }
    
    public long getStartTime() {
        return this.startTime;
    }

    public long getFinishTime() {
        return this.finishTime;
    }

    @Override
    public void execute(B browser) throws Exception{
        this.startTime = Calendar.getInstance().getTime().getTime();
        super.execute(browser);
        this.finishTime = Calendar.getInstance().getTime().getTime();
    }
    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("Start and Finish Metrics - ");
        toString.append("\n\tStart Time: ");
        toString.append(this.startTime);
        toString.append("\n\tFinish Time: ");
        toString.append(this.finishTime);
        toString.append("\n");
        toString.append(super.toString());
        return toString.toString();
    }
}
