package com.redhat.cms.selenium.command.decorator.metric;

import com.redhat.cms.selenium.pattern.Command;

/**
 *
 * This class is a concrete implementation of the MetricDecorator.
 *
 * It provies minimal miscelanious information about the details of a Command execution.
 *
 * @param <B> represents the Browser that will be used for the current BrowserCommand <C> when it runs execute();
 * @param <C> represents the Command that will be wrapped by this MetricDecorator.
 */
public class DebugMetric<B, C extends Command<B, C>> extends MetricDecorator<B, C, DebugMetric<B, C>> {

    private B browser;
    private boolean success;
    private Exception exception;
    private String name;
    private String message;

    public DebugMetric() {
    }

    public DebugMetric(C command) {
        super(command);
        this.name = command.getClass().getSimpleName();
        this.message = "Ready to Debug!";
    }

    @Override
    public DebugMetric<B, C> getInstance(C command) {
        return new DebugMetric<B, C>(command);
    }

    public B getBrowser() {
        return this.browser;
    }

    public Exception getException() {
        return this.exception;
    }

    public boolean isSuccessful() {
        return this.success;
    }

    public String getName() {
        return this.name;
    }

    public String getMessage() {
        return this.name;
    }

    @Override
    public void execute(B browser) {
        this.browser = browser;
        try {
            super.execute(browser);
            this.success = true;
            this.message = "Successful Execution!";
        } catch (Exception e) {
            this.exception = e;
            this.success = false;
            this.message = "An Error: " + this.getException().getClass().getCanonicalName();

        }
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append("Debug Metrics - ");
        toString.append("\n\tCommand Name:\n\t\t");
        toString.append(this.name);
        toString.append("\n\tResults Message:\n\t\t");
        toString.append(this.message);
        toString.append("\n");
        toString.append(super.toString());
        return toString.toString();
    }
}
