package com.redhat.cms.selenium.pattern.impl;

import com.redhat.cms.selenium.pattern.Decorator;
import java.util.ArrayList;
import java.util.List;

public class DecoratorList<T> extends ArrayList<T> implements Decorator<List<T>> {

    protected DecoratorList() {
    }

    protected DecoratorList(T first) {
        this.add(first);
    }

    @Override
    public List<T> getTarget() {
        return this;
    }

    @Override
    public void setTarget(List<T> target) {
        this.clear();
        if (target != null) {
            this.addAll(target);
        }
    }
}
