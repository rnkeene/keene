package com.redhat.cms.selenium.math;

import com.redhat.cms.selenium.pattern.Builder;
import java.util.List;

public class CrossProductBuilder<X extends List, Y extends List> implements Builder<Object[][]> {

    private X baseSet;
    private Y crossSet;

    public CrossProductBuilder() {
    }

    public CrossProductBuilder(X baseSet, Y crossSet) {
        this.baseSet = baseSet;
        this.crossSet = crossSet;
    }

    public X getBaseSet() {
        return baseSet;
    }

    public void setBaseSet(X baseSet) {
        this.baseSet = baseSet;
    }

    public Y getCrossSet() {
        return crossSet;
    }

    public void setCrossSet(Y crossSet) {
        this.crossSet = crossSet;
    }

    @Override
    public Object[][] build() {
        Object[][] matrix = new Object[][]{{}};
        if (this.baseSet != null && this.crossSet != null) {
            matrix = new Object[this.baseSet.size()][this.crossSet.size()];
            for (int x = 0; x < this.baseSet.size(); x++) {
                for(int y = 0; y < crossSet.size(); y++){
                    matrix[x][y] = new Object[]{ this.baseSet.get(x), this.crossSet.get(y) } ;
                }
            }
        }
        return matrix;
    }

    public Object[][] buildXOnly() {
        Object[][] matrix = new Object[this.baseSet.size()][1];
        for (int x = 0; x < this.baseSet.size(); x++) {
            matrix[x] = new Object[]{this.baseSet.get(x)};
        }
        return matrix;
    }
    
    public Object[][] buildYOnly() {
        Object[][] matrix = new Object[this.crossSet.size()][1];
        for (int y = 0; y < this.crossSet.size(); y++) {
            matrix[y] = new Object[]{this.crossSet.get(y)};
        }
        return matrix;
    }

}
