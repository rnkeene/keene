package com.redhat.cms.selenium.pattern;

public interface Initializer {

    public void initialize();

}
