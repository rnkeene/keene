package com.redhat.cms.selenium.pattern;

public interface Factory<O,I> {

    public O getInstance(I input);
}
