package com.redhat.cms.selenium.pattern;

public interface InputBuilder<R, I> {

    public R build(I input);

}
