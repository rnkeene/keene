package com.redhat.cms.selenium.pattern;

public interface FactoryMethod<O> {

    public O getInstance();

}
