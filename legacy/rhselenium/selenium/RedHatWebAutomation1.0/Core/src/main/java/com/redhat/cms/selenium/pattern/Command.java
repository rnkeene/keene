package com.redhat.cms.selenium.pattern;

public interface Command<O, C extends Command>{

    public void execute(O object) throws Exception;
    
}
