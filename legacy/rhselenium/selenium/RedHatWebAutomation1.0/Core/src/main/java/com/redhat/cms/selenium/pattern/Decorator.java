package com.redhat.cms.selenium.pattern;

public interface Decorator<T> {

    public T getTarget();

    public void setTarget(T target);
}
