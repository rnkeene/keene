package com.redhat.cms.selenium.pattern;

public interface InputInitializer<I>{

    public void initialize(I input);

}
