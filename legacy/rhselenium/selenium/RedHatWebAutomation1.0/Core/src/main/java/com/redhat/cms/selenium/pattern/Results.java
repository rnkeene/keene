package com.redhat.cms.selenium.pattern;

public interface Results<O> {

    public O getResults();

}
