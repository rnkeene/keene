package com.redhat.cms.selenium.pattern;

public interface Builder<R> {

    public R build();

}
