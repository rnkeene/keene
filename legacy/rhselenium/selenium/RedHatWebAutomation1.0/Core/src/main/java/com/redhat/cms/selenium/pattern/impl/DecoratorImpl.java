package com.redhat.cms.selenium.pattern.impl;

 public class DecoratorImpl<T> {

    private T target;

    public DecoratorImpl() {
    }

    public DecoratorImpl(T target) {
        this.target = target;
    }

    public T getTarget() {
        return target;
    }

    public void setTarget(T target) {
        this.target = target;
    }

}
