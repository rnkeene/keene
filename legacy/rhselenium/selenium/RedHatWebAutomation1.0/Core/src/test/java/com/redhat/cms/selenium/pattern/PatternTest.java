package com.redhat.cms.selenium.pattern;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PatternTest{    

    static Logger log = Logger.getLogger(PatternTest.class);

    @BeforeClass
    public void start(){
        log.info("PatternTest.start()");
        assert true;
    }

    @BeforeMethod
    public void setUp(){
        log.info("PatternTest.setUp()");
        assert true;
    }

    @Test()
    public void testPattern(){
        log.info("PatternTest.testPattern()");
        assert true;
    }

    @AfterMethod
    public void  tearDown(){
        log.info("PatternTest.tearDown()");
        assert true;
    }

    @AfterClass
    public void finish(){
        log.info("PatternTest.finish()");
        assert true;
    }
    
}
