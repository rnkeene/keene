package com.redhat.www.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class HomePage{

	private WebDriver driver;
	private TemplatedPage template;


	public HomePage(WebDriver driver){
		this.driver = driver;
		this.template = PageFactory.initElements(driver, TemplatedPage.class);
	}

	public SearchPage searchFor(String text) {
		return template.searchFor(text);
	}



}
