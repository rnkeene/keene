package com.redhat.www.page.courses;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
/**
 * 
 * @author dhenry
 *
 */
public class CourseDetailPage {
	private WebDriver driver;
	private Wait<WebDriver> wait ;
	public CourseDetailPage(WebDriver driver){
		this.driver = driver;
		//wait for up to 10 seconds for stuff that uses this wait
		wait = new WebDriverWait(driver,10);
	}
	
	public TabWidget getCourseDetailTabWidget(){
		//wait up to 5 seconds
		Wait<WebDriver> wait = new WebDriverWait(driver,5);
		WebElement tabbingRenderingPane = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.tabbingRenderingPane")));
		return new TabWidget(tabbingRenderingPane);
	}
	
	/**
	 * Click the enroll now button (opens in a new window/browser tab)
	 * TODO: handle new window/browser tab
	 * @return
	 */
	public EnrollNowPage clickEnrollButton(){
		driver.findElement(By.linkText("ENROLL TODAY")).click();
		return new EnrollNowPage(driver);
	}
	
	public String getCourseTitle(){
		return driver.findElement(By.cssSelector("div.maincontent_portlet>h1")).getText();
	}

	public String getCoursePrice() {

		WebElement tabbingRenderingPane = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.tabbingRenderingPane")));
		WebElement priceParagraph = tabbingRenderingPane.findElement(By.cssSelector("div.traning-detail p:nth-child(2)"));
		return priceParagraph.getText();
	}
	
	public boolean isErrorNotFound() {
		List<WebElement> elList = driver.findElements(By.id("contentColLeft"));
		if(elList.size()>0){
			return elList.get(0).findElement(By.tagName("h1")).getText().contains("404 Error");
		}
		else{
			return false;
		}
	}

	/**
	 * An attempt to check that a page is translated into a non-english language by looking for the words "COURSE DETAIL"
	 * in the tab headers
	 * @return
	 */
	public boolean isTranslated() {
		for (WebElement el : driver.findElements(By.cssSelector("#training_courses div.tabHeader"))){
			if(el.getText().contains("COURSE DETAIL")){
				return false;
			}
		}
		return true;

	}
}
