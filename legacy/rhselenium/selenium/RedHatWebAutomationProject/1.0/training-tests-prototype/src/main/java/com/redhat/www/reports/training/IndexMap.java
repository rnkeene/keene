package com.redhat.www.reports.training;

import java.util.ArrayList;

public class IndexMap<T> {
	private ArrayList<T> valueList;
	public IndexMap(){
		valueList = new ArrayList<T>();
	}
	/**
	 * Key index for key, add if not present
	 * @param key
	 * @return
	 */
	public Integer getIdx(T key){
		if(!valueList.contains(key)){
			valueList.add(key);
		}
		return valueList.indexOf(key);		
	}
	/**
	 * Determine if map has this key already
	 * @param key
	 * @return
	 */
	public boolean contains(T key){
		return valueList.contains(key);
	}
}
