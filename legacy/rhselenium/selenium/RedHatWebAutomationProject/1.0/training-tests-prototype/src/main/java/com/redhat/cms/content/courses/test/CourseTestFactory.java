package com.redhat.cms.content.courses.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.time.StopWatch;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.redhat.cms.selenium.browser.remote.driver.factory.RemoteDriverFirefoxFactory;
import com.redhat.cms.selenium.browser.remote.hub.SeleniumHub;
import com.redhat.cms.testcore.data.suite.RedHatSuiteData;
import com.redhat.cms.testcore.data.suite.SuiteCase;
import com.redhat.cms.testcore.dataprovider.MockRedHatSuiteCase;
import com.redhat.cms.testcore.test.RedHatTestFactory;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTestOne;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTestThree;
import com.redhat.cms.testcore.test.mocktest.MockRedHatTestTwo;
import com.redhat.selenium.TenaciousBrowserFactoryWrapper;
import com.redhat.selenium.TestLocaleCourseCommand;
import com.redhat.selenium.training.courses.dataprovider.RegionalCourseLocaleDataProvider;
import com.redhat.www.reports.training.TrainingCoursesReport;


public class CourseTestFactory extends RedHatTestFactory<CourseTest>{

	private static final long serialVersionUID = 1L;

	private TrainingCoursesReport report ; //TODO: get out of here
	private static Logger log = LoggerFactory.getLogger(CourseTestFactory.class);
	private TenaciousBrowserFactoryWrapper<RemoteWebDriver> driverFactory;
	private   String DOC_LOCATION;
	private StopWatch sw;
	private int completedCount;

	
    /**
     * The build method is designed to create an Object[] of Tests that are allotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Factory(dataProvider = "suiteData", dataProviderClass = MockRedHatSuiteCase.class)
    @Override
    public Object[] getTests(RedHatSuiteData factorySuite) {
		//Scratch code ***
		DateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
		DOC_LOCATION=String.format("/home/dhenry/testCourseContent_%s.ods",df.format(new Date()));
		report = new TrainingCoursesReport(DOC_LOCATION);
		//SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","127.0.0.1","/wd/hub","4444");
		//SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","qa-grid.devlab.phx1.redhat.com","/wd/hub","4445");
		SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","ec2-50-16-153-37.compute-1.amazonaws.com","/wd/hub","4444");
		//SeleniumHub<RemoteWebDriver> hub = new SeleniumHub<RemoteWebDriver>("http://","djhenry:afdfcf41-14c1-4d85-b101-772589ae1b65@ondemand.saucelabs.com","/wd/hub","80");
		driverFactory = new TenaciousBrowserFactoryWrapper<RemoteWebDriver>(new RemoteDriverFirefoxFactory(hub));
		sw=new StopWatch();
		sw.start();
		completedCount=0;
		//end scratch code ***
    	
    	
        log.info("Factory -> MockCompositeRedHatTestFactory");
        log.info(factorySuite.toString());
        log.info("Factory Suite Initialized: " + (SuiteCase) factorySuite.get(SuiteCase.class));
        return super.getTests(factorySuite);
    }
    
    /**
     * The build method is designed to create an Object[] of Tests that are allotted to run.
     *
     * This supports the expected format for the TestNG annotation for the @Factory, and Objects that want to implement @Factory need only Override this method and add the Annotation details.
     *
     * @return
     */
    @Override
    public Object[] build() {
    	this.add(new CourseTest(driverFactory));
        return this.toArray();
    }
    

}
