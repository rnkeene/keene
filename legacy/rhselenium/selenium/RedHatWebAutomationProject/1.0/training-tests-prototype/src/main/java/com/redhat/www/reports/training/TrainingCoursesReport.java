package com.redhat.www.reports.training;

import javax.annotation.concurrent.ThreadSafe;

import org.odftoolkit.simple.SpreadsheetDocument;
import org.odftoolkit.simple.table.Cell;
import org.odftoolkit.simple.table.Table;

/**
 * Keeps track of data to report about problems in the Red Hat training course pages.
 * 
 * TODO: This  was thrown together in a hurry, consider refactoring for better quality.
 * @author dhenry
 *
 */
@ThreadSafe
public class TrainingCoursesReport {
	private SpreadsheetDocument report;
	private IndexMap<String> dataTitleColumnIndexMap;
	private IndexMap<String> courseNameRowIndexMap;
	private String documentPath;
	
	public TrainingCoursesReport(String documentPath){
		this.documentPath = documentPath;
		dataTitleColumnIndexMap = new IndexMap<String>();
		courseNameRowIndexMap = new IndexMap<String>();
		dataTitleColumnIndexMap.getIdx("");
		courseNameRowIndexMap.getIdx("");
		try {
			report = SpreadsheetDocument.newSpreadsheetDocument();//loadDocument(documentPath);//
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void setDocumentPath(String documentPath){
		this.documentPath = documentPath;
	}
	/**
	 * Add a data point to this report.
	 * @param locale spreadsheet page to add this data to.  Each sheet is for a different language
	 * @param courseName each course is a row on the spreadsheet
	 * @param dataTitle each type of data reported on gets a new column
	 * @param value text value of data to report
	 */

	public synchronized void appendDatapoint(String locale,String courseName, String dataTitle, String value){
		
		int colIndex = dataTitleColumnIndexMap.getIdx(dataTitle);		
		int rowIndex = courseNameRowIndexMap.getIdx(courseName);
		Table sheet = report.getSheetByName(locale);
		//add sheet if not found
		if(sheet == null){
			sheet = report.appendSheet(locale);
		}
		//set row/col titles TODO:refactor, a bit hackish, doesn't need to set these every time.
		sheet.getCellByPosition(colIndex,0).setDisplayText(dataTitle);
		sheet.getCellByPosition(0,rowIndex).setDisplayText(courseName);
		
		Cell cell = sheet.getCellByPosition(colIndex, rowIndex);
		String newValue = cell.getDisplayText();
		if(!newValue.isEmpty()){
			newValue+="\n";
		}
		newValue += value;
		cell.setDisplayText(newValue);
	
	}

	public synchronized void saveToDisk(String documentPath){
		try {
			
			report.save(this.documentPath);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


}
