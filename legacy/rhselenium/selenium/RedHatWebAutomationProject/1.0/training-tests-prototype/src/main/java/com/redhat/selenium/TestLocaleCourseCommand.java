package com.redhat.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.cms.selenium.pattern.Command;
import com.redhat.www.page.courses.CourseDetailPage;
import com.redhat.www.page.courses.CoursesPage;
import com.redhat.www.page.courses.TabWidget;
import com.redhat.www.reports.training.TrainingCoursesReport;

public class TestLocaleCourseCommand  implements Command<WebDriver,TestLocaleCourseCommand>{
	private static Logger log = LoggerFactory.getLogger(TestLocaleCourseCommand.class);
	private String courseName;
	private String locale;
	private String baseUrl = "http://www.redhat.com/";
	private TrainingCoursesReport report;
	
	public TestLocaleCourseCommand(String course,String locale){
		this.courseName = course;
		this.locale = locale;
	}
		
	public void setReport(TrainingCoursesReport report){
		this.report = report;
	}
	

	public void execute(WebDriver driver) throws Exception {
		//Step1: Set locale
		driver.get(String.format("%ssetCountryLanguage?localeValue=%s",baseUrl,locale));
		
		
		//Step2: Load /training/courses/ course list page
		driver.get(baseUrl+"/training/courses/");
		
		log.debug(String.format("*** Course: %s Locale: %s ***",courseName,locale));
		
		CoursesPage coursesPage = new CoursesPage(driver);
		coursesPage.navToPage("1");
		while(!coursesPage.checkForCourse(courseName) && coursesPage.hasNextPage()){
			//report.appendDatapoint(locale, courseName, "Listed?", String.format("Page %s: No",coursesPage.getCurrentPageNumber()));
			coursesPage.navToNextPage();
		}
		if(coursesPage.checkForCourse(courseName)){
			report.appendDatapoint(locale, courseName, "Listed?", String.format("Page %s: YES",coursesPage.getCurrentPageNumber()));
		}
		else{
			report.appendDatapoint(locale, courseName, "Listed?", String.format("Page %s: No",coursesPage.getCurrentPageNumber()));
		}
		

		doIndividualCourseTestProcedure(driver, locale,courseName);



	}
	
	public Object getResult(){
		return report;
	}

	/**
	 * TODO:refactor
	 * @param coursePage
	 */
	private void doIndividualCourseTestProcedure(WebDriver driver, String locale, String courseName) {
		//Load individual course page
		driver.get(baseUrl+String.format("training/courses/%s/",courseName.toLowerCase()));

		CourseDetailPage coursePage = new CourseDetailPage(driver);
		
		//check that detail page is visible
		if(coursePage.isErrorNotFound()){
			report.appendDatapoint(locale, courseName, "Detail Page Visible?", "No (404 Error)");
		}
		else{
			report.appendDatapoint(locale, courseName, "Detail Page Visible?", "Yes");
		}
		
		//check for "Enroll Now" button
		List<WebElement> enrollNowButtons = driver.findElements(By.cssSelector("div.enr-today-btn>a"));
		if(enrollNowButtons.size()>0){
			String buttonUrl = enrollNowButtons.get(0).getAttribute("href");
			log.debug("Enroll Now URL: "+buttonUrl);
			report.appendDatapoint(locale, courseName, "Enroll Now Button", buttonUrl);
			
		}
		else {
			log.debug("X - Enroll Now Button MISSING!");
			report.appendDatapoint(locale, courseName, "Enroll Now Button", "MISSING!");
		}
		
		if(locale.contains("en")){
			report.appendDatapoint(locale, courseName, "Translated?", "N/A");
		}
		else if(coursePage.isTranslated()){
			report.appendDatapoint(locale, courseName, "Translated?", "Yes");
		}
		else{
			report.appendDatapoint(locale, courseName, "Translated?", "No");
		}
		
		try {
			//check the tab widgets		
			TabWidget tabWidget = coursePage.getCourseDetailTabWidget();
			log.debug("Price: "+coursePage.getCoursePrice());
			report.appendDatapoint(locale, courseName, "Price", coursePage.getCoursePrice());
			
			for(String tabHeaderText : tabWidget.getTabHeaderTextList()){
				
				String tabItemText = tabWidget.selectTabHeader(tabHeaderText).getText();
				//make sure each tab is present and does not contain the word "null"
				if(tabItemText.contains("null")){
					log.debug(String.format("X - found 'null' in tab '%s'",tabHeaderText));
					report.appendDatapoint(locale, courseName, "Content", String.format("Found 'null' in tab '%s'",tabHeaderText));
				}
				else if(tabItemText.isEmpty()){
					log.debug(String.format("X - found no content in tab '%s'",tabHeaderText));
					report.appendDatapoint(locale, courseName, "Content", String.format("Found no content in tab '%s'",tabHeaderText));
				}
				
			}
		}
		catch(org.openqa.selenium.TimeoutException e){
			log.debug("X - Course detail tab widget appears to be missing.");
			report.appendDatapoint(locale, courseName, "Content", "Course detail tab widget appears to be missing.");

		}
	}
}
