package com.redhat.selenium;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.cms.selenium.browser.factory.BrowserFactory;

public class TenaciousBrowserFactoryWrapper<B> implements BrowserFactory {
	private final BrowserFactory<B> factory;
	private int retryLimit = 5;
	private static Logger log = LoggerFactory
			.getLogger(TenaciousBrowserFactoryWrapper.class);

	public TenaciousBrowserFactoryWrapper(BrowserFactory<B> factory) {
		this.factory = factory;
	}

	public void setRetryLimit(int retries) {
		this.retryLimit = retries;
	}

	public B getInstance() {
		B browser = null;
		RuntimeException failureReason = null;
		for (int attempt = 1; browser == null && attempt <= retryLimit; attempt++) {
			try {
				browser = factory.getInstance();
			} catch (Exception e) {
				failureReason = new RuntimeException(String.format(
						"Failure on attempt %s of %s.", attempt, retryLimit), e);
				log.warn(failureReason.toString());
			}
		}
		if (browser != null) {
			return browser;
		} else if (failureReason != null) {
			throw failureReason;
		} else {
			throw new RuntimeException(
					"Browser factory appears to be returning null from getInstance()");
		}

	}
}
