package com.redhat.selenium.training.courses.dataprovider;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.testng.annotations.DataProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RegionalCourseLocaleDataProvider {
	@DataProvider(name="TrainingCoursesDataProvider",parallel=true)
	public static Object[][] createData() {
		List<Region> regionList =loadRegionList("region_locale_course.xml");

		//build list of test cases from list of regions
		List<String[]> testCaseList = new ArrayList<String[]>();
		for(Region region : regionList){
			for(String locale : region.getLocalesArray()){
				for(String course : region.getCoursesArray()){
					String[] testCase = new String[3];
					testCase[0] = region.getKey();
					testCase[1] = locale;
					testCase[2] = course;
					testCaseList.add(testCase);
				}
			}
		}
		
		//build 2d array of test cases out of list of test cases
		Object[][] testData = new Object[testCaseList.size()][testCaseList.get(0).length];
		
		for(int i=0;i<testCaseList.size();i++){
			for(int j=0;j<testCaseList.get(i).length;j++){
				testData[i][j] = testCaseList.get(i)[j]; 
			}
		}
		return testData;
		
	}
	
	private static List<Region> loadRegionList(String filePath){
		List<Region> regionList = new ArrayList<Region>();
		try {

			Resource resource = new ClassPathResource(filePath);
			File file = resource.getFile();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document document = db.parse(file);
			document.getDocumentElement().normalize();
			NodeList nodeList = document.getElementsByTagName("region");
			
			for (int i=0;i<nodeList.getLength();i++){
				
				Node node = nodeList.item(i);
				Element el = (Element) node;
				
				Region region = new Region();
				region.setCourses(el.getElementsByTagName("courses").item(0).getTextContent());
				region.setLocales(el.getElementsByTagName("locales").item(0).getTextContent());
				region.setKey(el.getAttribute("key"));
				regionList.add(region);
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return regionList;
	}

}
