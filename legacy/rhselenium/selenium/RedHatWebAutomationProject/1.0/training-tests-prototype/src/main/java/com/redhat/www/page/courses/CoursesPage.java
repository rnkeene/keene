package com.redhat.www.page.courses;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Page Object for the Red Hat Training Courses portal page.
 * 
 * @author dhenry
 *
 */
public class CoursesPage {
	private WebDriver driver;

	public CoursesPage(WebDriver driver){
		this.driver = driver;		
	}
	public CourseDetailPage navToCourse(String courseName){
		WebElement el = driver.findElement(By.partialLinkText(courseName));
		el.click();
		return new CourseDetailPage(driver);
	}
	public boolean checkForCourse(String courseName) {
		Wait<WebDriver> wait = new WebDriverWait(driver,10);
		WebElement resultsTable = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("facet_table")));
		//locate a td that contains the course name in the results table
		return resultsTable.findElements(By.xpath(String.format("//td[contains(.,'%s')]",courseName))).size() > 0;
	}
	public boolean hasNextPage() {
		WebElement el = driver.findElement(By.cssSelector("ol.pagination_events"));
		return el.findElements(By.cssSelector("a[rel=next]")).size()>0;
	}
	public CoursesPage navToNextPage(){
		int pageNumber = getCurrentPageNumber();
		WebElement el = driver.findElement(By.cssSelector("ol.pagination_events"));
		el.findElement(By.cssSelector("a[rel=next]")).click();
		int newPageNumber = getCurrentPageNumber();
		if(newPageNumber != pageNumber+1){
			throw new AssertionError(String.format("Previous page number: %s, new page number: %s",pageNumber,newPageNumber));
		}
		return this;
	}
	public CoursesPage navToPage(String pageNumberText) {
		WebElement nav = driver.findElement(By.cssSelector("ol.pagination_events"));
		List<WebElement> pageNavList = nav.findElements(By.linkText(pageNumberText));
		if(pageNavList.size() ==1){
			pageNavList.get(0).click();
		}
		assert(Integer.parseInt(pageNumberText) == getCurrentPageNumber());
		return this;
	}
	public int getCurrentPageNumber(){
		Wait<WebDriver> wait = new WebDriverWait(driver,10);
		WebElement pageNumberLi = wait.until(
				ExpectedConditions.presenceOfElementLocated(
						By.xpath("//ol[@class='pagination_events']/li[not(a)]")));		
		return Integer.parseInt(pageNumberLi.getText());
	}
}
