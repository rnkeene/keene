package com.redhat.www.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TemplatedPage {
	protected final WebDriver driver;
	
	@FindBy(id="searchBox")
	private WebElement searchBox;
    
	public TemplatedPage(WebDriver driver){
		this.driver = driver;
	}
	
	public SearchPage searchFor(String text) {
        // We continue using the element just as before
        searchBox.sendKeys(text);
        searchBox.submit();
        return PageFactory.initElements(driver, SearchPage.class);
    }
}
