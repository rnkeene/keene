package com.redhat.www.page.courses;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TabWidget {
	WebElement tabWidgetEl;
	
	public TabWidget(WebElement el){
		this.tabWidgetEl = el;
	}
	/**
	 * Return a list of tab titles for this tab widget 
	 * @return
	 */
	public List<String> getTabHeaderTextList(){
		WebElement tabWrapper = tabWidgetEl.findElement(By.cssSelector("div.titleWrapperStyle")); 
		List<String> tabTitleList = new ArrayList<String>();
		for(WebElement tabHeader : tabWrapper.findElements(By.cssSelector("div.tabHeader")) ){
			tabTitleList.add(tabHeader.getText());
		}
		return tabTitleList;
	}
	/**
	 * Find the tab header with the specified label and click on it
	 * @param tabTitle
	 * @return the tab that was clicked on.
	 */
	public WebElement selectTabHeader(String tabTitle){
		WebElement tabWrapper = tabWidgetEl.findElement(By.cssSelector("div.titleWrapperStyle")); 
		List<WebElement> tabs =  tabWrapper.findElements(By.cssSelector("div.tabHeader"));
		int tabNum = 0;
		for(WebElement tab :tabs){
			if(tab.getText().contains(tabTitle)){
				tab.click();

				return tabWidgetEl.findElements(By.cssSelector("div.tabBody")).get(tabNum);
			}
			tabNum++;
		}
		throw new RuntimeException(String.format("Tab '%s' not found!",tabTitle));
	}
	
	/**
	 * Get the visible text from the currently selected tab
	 * TODO: handle tab items
	 * TODO: optionally allow passing in tab header and tab item
	 * @return text of the currently selected tabBody
	 * @deprecated broken?  Plz fix!
	 */
	public String getActiveTabBodyText(){
		return tabWidgetEl.findElement(By.cssSelector("div.tabBody")).getText();
	}
	
}
