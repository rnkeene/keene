package com.redhat.selenium.training.courses.dataprovider;

public class Region {

	private String key;
	private String locales;
	private String courses;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getLocales() {
		return locales;
	}
	public void setLocales(String locale) {
		this.locales = locale;
	}
	public String getCourses() {
		return courses;
	}
	public void setCourses(String courses) {
		this.courses = courses;
	}
	public String[] getLocalesArray() {
		return locales.split(",");
	}
	
	public String[] getCoursesArray() {
		return courses.split(",");
	}
	

}
