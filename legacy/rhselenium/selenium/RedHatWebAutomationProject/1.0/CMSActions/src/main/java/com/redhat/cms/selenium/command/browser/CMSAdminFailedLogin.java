package com.redhat.cms.selenium.command.browser;

import com.redhat.cms.selenium.command.browser.pattern.CompositeCommand;

public class CMSAdminFailedLogin extends CompositeCommand
{

    @Override
    public CompositeCommand build()
    {
        super.build();
        this.add(new CMSAdminLogin());
        return this;
    }
}
