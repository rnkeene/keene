package com.redhat.cms.selenium.script;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskManagement {
	private Logger logger = LoggerFactory.getLogger(TaskManagement.class);

	private Config config;
	private WebElement element;
	
	public TaskManagement(Config config, WebElement element) {
		this.config = config;
		this.element = element;
	}
	
	public void approve(){
		logger.info("Clicking 'Approve'");
		WebDriverWait wait = new WebDriverWait(((WrapsDriver)element).getWrappedDriver(), 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Approve"))).click();
		//TODO: "NEVER" use Thread.sleep for waiting on something in a selenium test.
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//wait for the 'Loading...' modal to go away.
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("LoadingText")));
	}

}
