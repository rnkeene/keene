package com.redhat.cms.selenium.script;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Dashboard {
	private Logger logger = LoggerFactory.getLogger(Dashboard.class);

	private Config config;
	private WebDriver driver;

	public Dashboard(Config config, WebDriver driver) {
		this.config = config;
		this.driver = driver;
	}

	/**
	 * TODO: break this down, don't short circuit while nested 3 deep
	 * @param group
	 * @return
	 */
	public List<String> listTaskIdForGroup(String group) {
		logger.info(String.format("Listing taskIds for group '%s'", group));
		List<String> taskIdList = new ArrayList<String>();
		//Refactor, use xpath contains function to eliminate the need for the top level for loop.
		List<WebElement> divList = driver.findElements(By.xpath("//div[@id='groupTasksContainerDiv']/div/div"));
		for(WebElement div : divList){
			if( div.findElement(By.xpath("div[1]")).getText().contains(group) ){
				List<WebElement> taskIdTdList = div.findElements(By.xpath("div[3]/table/tbody/tr/td[1]"));
				for(WebElement taskIdTd: taskIdTdList){
					try{
						taskIdList.add( ""+Integer.parseInt(taskIdTd.getText()) );
					}
					catch (NumberFormatException e){
						//do nothing
					}
				}
				return taskIdList;
			}
		}
		throw new RuntimeException(String.format("Group '%s' Not Found!",group));
	}

	public void aquireTaskId(String taskId) {
		logger.info(String.format("Aquiring taskId '%s'", taskId));

		//find the TR for the row containing the specified taskId
		WebElement taskTr = driver.findElement(By.xpath(String.format("//div[3]/table/tbody/tr[contains(td[1],'%s')]",taskId)));
		
		logger.info("Task row looks like: "+taskTr.getText());
		
		//Click the acquire button
		taskTr.findElement(By.partialLinkText("Acquire")).click();
		
		
	}

	public TaskManagement manageTask(String taskId) {
		logger.info(String.format("Managing taskId '%s'", taskId));

		//find the TR for the row containing the specified taskId
		WebElement taskTr = driver.findElement(By.xpath(String.format("//div[@id='UserTaskGrid']/table/tbody/tr[contains(td[1],'%s')]",taskId)));
		taskTr.findElement(By.linkText("Manage")).click();
		WebDriverWait wait = new WebDriverWait(driver,5);
		
		return new TaskManagement(config,wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ControllerPopup"))));
	}
}
