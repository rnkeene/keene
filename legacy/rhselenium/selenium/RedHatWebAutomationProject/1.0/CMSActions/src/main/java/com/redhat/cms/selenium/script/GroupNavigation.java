package com.redhat.cms.selenium.script;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroupNavigation {
	private Logger logger = LoggerFactory.getLogger(GroupNavigation.class);

	private WebElement element;
	private Config config;
	public GroupNavigation(Config config, WebElement element) {
		this.config = config;
		this.element = element;
	}

	/**
	 * Look through all of the expand containers and click the expand link for the one
	 * that matches the desired node text.
	 * 
	 * This method has no effect if the node text is not found or the node is already open.
	 * @param nodeText
	 */
	public void expand(String nodeText){
		logger.debug(String.format("Expanding node '%s'. Searching for expandable node...", nodeText));
		List<WebElement> expandElementList = this.element.findElements(By.cssSelector("div.ExpandIcon"));
		for(WebElement expandElement : expandElementList){
			String thisNodeText = expandElement.getText().replaceAll(" ", "");
			if(thisNodeText.equalsIgnoreCase(nodeText)){
				logger.debug(String.format("Found node '%s'. Click and wait...", nodeText));

				expandElement.click();
				// porlet will completely refresh after each node click. Wait
				// for that to happen before finishing.
				WebDriverWait wait = new WebDriverWait(((WrapsDriver)element).getWrappedDriver(), 15);
				wait.until(ExpectedConditions
						.stalenessOf(element));
				
				//TODO: "NEVER" use Thread.sleep for waiting on something in a selenium test.
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info(String.format("Node '%s' expanded", nodeText));

				return;
			}
		}
		logger.warn(String.format("Node '%s' NOT FOUND, moving on.", nodeText));

	}
	
}
