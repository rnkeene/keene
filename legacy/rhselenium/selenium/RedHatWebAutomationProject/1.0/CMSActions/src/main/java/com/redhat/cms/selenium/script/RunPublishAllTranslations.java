package com.redhat.cms.selenium.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class RunPublishAllTranslations {
	Logger logger = LoggerFactory.getLogger(RunPublishAllTranslations.class);

	private WebDriver driver;
	private Config config;
	// private static final String BASE_URL =
	// "http://cms03.web.qa.ext.phx1.redhat.com:8443";
	//private static final String BASE_URL = "https://dev-qa-admin.usersys.redhat.com:8143/";
	//private static final String BASE_URL = "https://www-stage-admin.usersys.redhat.com:8243/";
	private static final String BASE_URL = "https://www-prod-admin.usersys.redhat.com";
	private static final String USERNAME = "charms";
	private static final String PASSWORD = "Parker99!";
	private static final String[] GROUPS = {
			"/redhat/translators/de/editors",
			"/redhat/translators/es/editors",
			"/redhat/translators/es_AR/editors",
			"/redhat/translators/fr/editors",
			"/redhat/translators/it/editors",
			"/redhat/translators/ja/editors",
			"/redhat/translators/ko/editors",
			"/redhat/translators/pt_BR/editors",
			"/redhat/translators/zh/editors",
			"/redhat/translators/de/approvers",
			"/redhat/translators/es/approvers",
			"/redhat/translators/es_AR/approvers",
			"/redhat/translators/fr/approvers",
			"/redhat/translators/it/approvers",
			"/redhat/translators/ja/approvers",
			"/redhat/translators/ko/approvers",
			"/redhat/translators/pt_BR/approvers",
			"/redhat/translators/zh/approvers" };
	private static final Map<String, String> GRP_MAP = new HashMap<String, String>() {
		{
			put("/redhat/translators/de/editors",
					"/redhat/translators/German/editors");
			put("/redhat/translators/es/editors",
					"/redhat/translators/Spanish/editors");
			put("/redhat/translators/es_AR/editors",
					"/redhat/translators/Spanish-Latam/editors");
			put("/redhat/translators/fr/editors",
					"/redhat/translators/French/editors");
			put("/redhat/translators/it/editors",
					"/redhat/translators/Italian/editors");
			put("/redhat/translators/ja/editors",
					"/redhat/translators/Japanese/editors");
			put("/redhat/translators/ko/editors",
					"/redhat/translators/Korean/editors");
			put("/redhat/translators/pt_BR/editors",
					"/redhat/translators/Brazilian-Portuguese/editors");
			put("/redhat/translators/zh/editors",
					"/redhat/translators/Simple-Chinese/editors");
			put("/redhat/translators/de/approvers",
					"/redhat/translators/German/approvers");
			put("/redhat/translators/es/approvers",
					"/redhat/translators/Spanish/approvers");
			put("/redhat/translators/es_AR/approvers",
					"/redhat/translators/Spanish-Latam/approvers");
			put("/redhat/translators/fr/approvers",
					"/redhat/translators/French/approvers");
			put("/redhat/translators/it/approvers",
					"/redhat/translators/Italian/approvers");
			put("/redhat/translators/ja/approvers",
					"/redhat/translators/Japanese/approvers");
			put("/redhat/translators/ko/approvers",
					"/redhat/translators/Korean/approvers");
			put("/redhat/translators/pt_BR/approvers",
					"/redhat/translators/Brazilian-Portuguese/approvers");
			put("/redhat/translators/zh/approvers",
					"/redhat/translators/Simple-Chinese/approvers");
		}
	};

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		config = new Config();
		config.setBaseUrl(BASE_URL);

	}

	@AfterClass
	public void afterClass() {
		// driver.quit();
	}

	public void testCMSAdminLogin() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		Assert.assertFalse(cmsAdmin.isLoggedIn());
		cmsAdmin.login(USERNAME, PASSWORD);
		Assert.assertTrue((cmsAdmin.isLoggedIn()));
		cmsAdmin.logout();
		Assert.assertFalse(cmsAdmin.isLoggedIn());

	}

	public void testAddUserToGroup() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		cmsAdmin.login(USERNAME, PASSWORD);
		OrganizationManagement om = cmsAdmin.navigateToOrganizationManagement();
		om.addUserToGroup(USERNAME, "/redhat/translators/Spanish/editors");
		cmsAdmin.logout();
		cmsAdmin.login(USERNAME, PASSWORD);
		om = cmsAdmin.navigateToOrganizationManagement();
		assert (om.isUserInGroup(USERNAME,
				"/redhat/translators/Spanish/editors"));
	}

	/**
	 * TODO: Prepare data so that user is present before trying this test. It
	 * will throw an exception if the user does not exist in the group
	 */
	public void testRemoveUserFromGroup() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		cmsAdmin.login(USERNAME, PASSWORD);
		OrganizationManagement om = cmsAdmin.navigateToOrganizationManagement();
		om.removeUserFromGroup(USERNAME, "/redhat/translators/Spanish/editors");
		cmsAdmin.logout();
		cmsAdmin.login(USERNAME, PASSWORD);
		om = cmsAdmin.navigateToOrganizationManagement();
		assert (!om.isUserInGroup(USERNAME,
				"/redhat/translators/Spanish/editors"));
	}

	public void testListTasksForGroup() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		cmsAdmin.login(USERNAME, PASSWORD);

		Dashboard db = cmsAdmin.navigateToDashboard();
		List<String> taskIdList = db
				.listTaskIdForGroup("/redhat/translators/ko/editors");
		for (String taskId : taskIdList) {
			db.aquireTaskId(taskId);
		}
	}

	public void testAquireTask() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		cmsAdmin.login(USERNAME, PASSWORD);

		Dashboard db = cmsAdmin.navigateToDashboard();
		db.aquireTaskId("166949");

	}

	public void testManageTask() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		cmsAdmin.login(USERNAME, PASSWORD);

		Dashboard db = cmsAdmin.navigateToDashboard();
		TaskManagement tm = db.manageTask("188113");
	}

	public void testApproveTask() {
		CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
		cmsAdmin.login(USERNAME, PASSWORD);

		Dashboard db = cmsAdmin.navigateToDashboard();
		TaskManagement tm = db.manageTask("188113");
		tm.approve();
	}

	@Test
	public void approveAll() {
		logger.info("Running 'approveAll'");
		logger.info(String.format("STARTING RUN on %s as user %s", BASE_URL,USERNAME));
		// for all translation and approval groups
		for (String group : GROUPS) {
			CMSAdmin cmsAdmin = new CMSAdmin(config, driver);
			// login
			cmsAdmin.login(USERNAME, PASSWORD);
			// Add user to group
			OrganizationManagement om = cmsAdmin
					.navigateToOrganizationManagement();

			// Remove the user from the group if they already exist in that
			// group
			if (om.isUserInGroup(USERNAME, GRP_MAP.get(group))) {
				om.removeUserFromGroup(USERNAME, GRP_MAP.get(group));
				cmsAdmin.logout();
				cmsAdmin.login(USERNAME, PASSWORD);
				om = cmsAdmin.navigateToOrganizationManagement();
			}
			// (Re)add the user to the group
			om.addUserToGroup(USERNAME, GRP_MAP.get(group));
			// Logout
			cmsAdmin.logout();
			// Login again
			cmsAdmin.login(USERNAME, PASSWORD);
			// Acquire and approve all tasks for group
			Dashboard db = cmsAdmin.navigateToDashboard();
			List<String> taskIdList = db.listTaskIdForGroup(group);
			for (String taskId : taskIdList) {
				try {
					db.aquireTaskId(taskId);
					TaskManagement tm = db.manageTask(taskId);
					tm.approve();
				} catch (Exception e) {
					logger.error(
							"Something went horribly wrong.  Launching new browser and moving on to the next taskId",
							e);
					
					//examples of things going wrong: CMS 500 error, js error, unexpected CMS behavior
					try{
						driver.quit();
					}
					catch(Exception e2){
						logger.error("Unable to close browser, moving on...",e2);
					}
					
					try {
						Thread.sleep(7000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					driver = new FirefoxDriver();
					try {
						Thread.sleep(7000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					cmsAdmin = new CMSAdmin(config, driver);
					// login

					cmsAdmin.login(USERNAME, PASSWORD);
					db = cmsAdmin.navigateToDashboard();
					
				}
			}

			cmsAdmin.navigateToOrganizationManagement();
			om.removeUserFromGroup(USERNAME, GRP_MAP.get(group));

			cmsAdmin.logout();
		}

	}
}
