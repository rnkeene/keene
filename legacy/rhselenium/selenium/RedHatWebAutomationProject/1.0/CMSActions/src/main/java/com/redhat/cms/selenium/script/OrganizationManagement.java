package com.redhat.cms.selenium.script;

import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrganizationManagement {
	private Logger logger = LoggerFactory.getLogger(OrganizationManagement.class);

	private WebDriver driver;
	private Config config;

	public OrganizationManagement(Config config, WebDriver driver) {
		this.config = config;
		this.driver = driver;
	}

	/**
	 * Determines if a user is currently displayed as existing in a group
	 * 
	 * @param username
	 * @param group
	 * @return
	 */
	public boolean isUserInGroup(String username, String group) {
		logger.debug(String.format("Checking for username '%s' in group '%s'...",username, group) );
		// TODO: Abstract copy/paste code from removeUserFromGroup()
		navigateToGroup(group);
		// find the group info table (wait 5 seconds for it to show up)
		WebDriverWait wait = new WebDriverWait(driver, 5);
		WebElement groupInfoTable = wait.until(ExpectedConditions
				.presenceOfElementLocated(By.cssSelector("table.UIGrid")));
		// find the row containing the username in the first column of that
		// table
		List<WebElement> rows = groupInfoTable.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			List<WebElement> cells = row.findElements(By.cssSelector("td"));
			if (cells.size() > 0
					&& cells.get(0).getText().equalsIgnoreCase(username)) {
				logger.debug(String.format("Username '%s' FOUND in group '%s'",username, group) );

				return true;
			}
		}
		
		logger.debug(String.format("Username '%s' NOT FOUND in group '%s'",username, group) );
		return false;
	}

	/**
	 * Adds a user to a group.
	 * @param user
	 * @param group
	 */
	public void addUserToGroup(String user, String group) {
		logger.debug(String.format("Adding user '%s' to group '%s'...",user, group) );

		navigateToGroup(group);
		driver.findElement(By.id("username")).sendKeys(user);
		driver.findElement(By.linkText("Save")).click();
		
		//TODO: "NEVER" use Thread.sleep for waiting on something in a selenium test.
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info(String.format("Added user '%s' to group '%s'.",user, group) );

	}

	/**
	 * Removes a user from a group.
	 * @param username
	 * @param group
	 */
	public void removeUserFromGroup(String username, String group) {
		logger.debug(String.format("Removing username '%s' from group '%s'...",username, group) );

		navigateToGroup(group);
		// find the group info table (wait 5 seconds for it to show up)
		WebDriverWait wait = new WebDriverWait(driver, 5);
		WebElement groupInfoTable = wait.until(ExpectedConditions
				.presenceOfElementLocated(By.cssSelector("table.UIGrid")));
		// find the row containing the username in the first column of that
		// table
		List<WebElement> rows = groupInfoTable.findElements(By.tagName("tr"));
		for (WebElement row : rows) {
			List<WebElement> cells = row.findElements(By.cssSelector("td"));
			if (cells.size() > 0
					&& cells.get(0).getText().equalsIgnoreCase(username)) {
				// find the img.DeleteUserIcon in that row
				row.findElement(By.cssSelector("img.DeleteUserIcon")).click();
				// accept the "are you sure?" alert
				Alert alert = driver.switchTo().alert();
				alert.accept();
				wait.until(ExpectedConditions.stalenessOf(row));
				logger.info(String.format("Removed username '%s' from group '%s'...",username, group) );

				return;
			}
		}
		throw new UserNotFoundException(username);
	}

	/**
	 * Navigate to a group in the GroupManagement view TODO:Sloppy, plz refactor
	 * 
	 * @param group
	 */
	private void navigateToGroup(String group) {
		String[] groupPath = group.split("/");
		driver.findElement(By.cssSelector("a.GroupButton")).click();
		WebDriverWait wait = new WebDriverWait(driver, 15);

		//wait for the breadcrumb info bar to be loaded (even if not visible because it is empty)
		wait.until(ExpectedConditions.presenceOfElementLocated(By
						.cssSelector("div.BreadcumbsInfoBar")));

		// "Level Up" until at the top of the tree
		while (!driver.findElement(By
						.cssSelector("div.BreadcumbsInfoBar")).getText().isEmpty()) {

			WebElement levelUpArrowIcon = wait.until(ExpectedConditions
					.presenceOfElementLocated(By
							.cssSelector("a.LevelUpArrowIcon")));
			
			levelUpArrowIcon.click();
			wait.until(ExpectedConditions.stalenessOf(levelUpArrowIcon));

			//TODO: "NEVER" use Thread.sleep for waiting on something in a selenium test.
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// now travel down the tree to the requested group
		for (String nodeName : groupPath) {
			if (!nodeName.isEmpty()) {
				WebElement groupNavigationContainer = wait
						.until(ExpectedConditions.presenceOfElementLocated(By
								.cssSelector("div.GroupNavigationContainer")));
				GroupNavigation gn = new GroupNavigation(config,
						groupNavigationContainer);
				gn.expand(nodeName);

			}
		}
	}

}
