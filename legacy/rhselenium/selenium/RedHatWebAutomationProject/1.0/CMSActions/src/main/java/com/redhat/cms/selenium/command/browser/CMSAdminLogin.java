package com.redhat.cms.selenium.command.browser;

import com.redhat.cms.selenium.command.browser.pattern.CompositeCommand;
import com.redhat.cms.selenium.command.driver.DriverClearElement;
import com.redhat.cms.selenium.command.driver.DriverClick;
import com.redhat.cms.selenium.command.driver.DriverOpen;
import com.redhat.cms.selenium.command.driver.DriverSendKeys;

public class CMSAdminLogin extends CompositeCommand
{

    @Override
    public CompositeCommand build()
    {
        super.build();
        this.add(new DriverOpen("https://dev-qa-admin.usersys.redhat.com:8143/"));
        this.add(new DriverClick("//td/input"));
        this.add(new DriverClearElement("//td/input"));
        this.add(new DriverSendKeys("//td/input", "charms"));
        this.add(new DriverClearElement("//tr[4]/td/input"));
        this.add(new DriverSendKeys("//tr[4]/td/input", "Parker99!"));
        this.add(new DriverClick("//tr[4]/td[2]/div/a"));
        return this;
    }
}
