package com.redhat.cms.selenium.script;

public class UserNotFoundException extends RuntimeException {

	public UserNotFoundException(String username) {
		super(String.format("The user '%s' was not found.", username));

	}

}
