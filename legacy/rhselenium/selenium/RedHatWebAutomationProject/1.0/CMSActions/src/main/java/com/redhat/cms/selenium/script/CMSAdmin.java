package com.redhat.cms.selenium.script;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CMSAdmin {
	private WebDriver driver;
	private Config config;
	public CMSAdmin(Config config, WebDriver driver) {
		this.config = config;
		this.driver = driver;
	}

	public WebDriver login(String username, String password) {
		driver.get(config.getBaseUrl()+"/rhecm/private/redhat/siteExplorer");
		WebElement usernameField = driver.findElement(By.id("username"));
		usernameField.click();
		usernameField.sendKeys(username);
		WebElement fakePasswordField = driver
				.findElement(By
						.xpath("/html/body/div/div[2]/div[4]/form/table/tbody/tr[3]/td/input"));
		fakePasswordField.click();
		WebElement passwordField = driver.findElement(By
				.xpath("//*[@id='password']"));
		passwordField.click();
		passwordField.sendKeys(password);
		driver.findElement(By.id("loginForm")).submit();
		return driver;
	}

	public boolean isLoggedIn() {
		List<WebElement> userInfoPortletList = driver.findElements(By.id("UIUserInfoPortlet"));
		return userInfoPortletList.size()>0;
		
	}

	public void logout() {
//		WebElement starToolBarPortlet = driver.findElement(By.id("StarToolBarPortlet"));
//		Actions builder = new Actions(driver);
//		Action logoutAction = builder.moveToElement(starToolBarPortlet)
//				.moveToElement(starToolBarPortlet.findElement(By.cssSelector("a.SignOutIcon")))
//				.click()
//				.build();
//		
//		logoutAction.perform();
		WebElement userInfoPortlet = driver.findElement(By.id("UIUserInfoPortlet"));
		((JavascriptExecutor)driver).executeScript("eXo.portal.logout();");
		WebDriverWait wait = new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.stalenessOf(userInfoPortlet));
	}
	
	public OrganizationManagement navigateToOrganizationManagement(){
		driver.get(config.getBaseUrl()+"/rhecm/private/redhat/organization/management");
		return new OrganizationManagement(config,driver);
	}
	
	public Dashboard navigateToDashboard(){
		driver.get(config.getBaseUrl()+"/rhecm/private/redhat/myDashboard");
		return new Dashboard(config,driver);
	}
}
