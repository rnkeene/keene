package com.google.gwt.sample.meartifacts.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class MEArtifacts implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	private FlexTable artifactFlexTable = new FlexTable();
	private VerticalPanel mainPanel = new VerticalPanel();
	private HorizontalPanel addPanel = new HorizontalPanel();
	private TextBox newANNo = new TextBox();
	private TextBox newANNa = new TextBox();
	private TextBox newAlleg = new TextBox();
	private TextBox newE1 = new TextBox();
	private TextBox newE2 = new TextBox();
	private TextBox newLoc = new TextBox();
	private TextBox newPoss = new TextBox();
	private TextBox newSo = new TextBox();
	private Button addArtifactButton = new Button("Add");
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		artifactFlexTable.setText(0, 0, "Artifact Number");
		artifactFlexTable.setText(0, 1, "Artifact Name");
		artifactFlexTable.setText(0, 2, "Allegiance");
		artifactFlexTable.setText(0, 3, "Effect 1");
		artifactFlexTable.setText(0, 4, "Effect 2");
		artifactFlexTable.setText(0, 5, "Location");
		artifactFlexTable.setText(0, 6, "Possession");
		artifactFlexTable.setText(0, 7, "Source");
		
		
		
		addPanel.add(newANNo);
		addPanel.add(newANNa);
		addPanel.add(newAlleg);
		addPanel.add(newE1);
		addPanel.add(newE2);
		addPanel.add(newLoc);
		addPanel.add(newPoss);
		addPanel.add(newSo);
		addPanel.add(addArtifactButton);
		mainPanel.add(addPanel);
		mainPanel.add(artifactFlexTable);
		
		RootPanel.get("artifactList").add(mainPanel);
		
		addArtifactButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addArtifact();
			}
		});
	}
	public void addArtifact() {
		int row = artifactFlexTable.getRowCount();
		artifactFlexTable.setText(row, 0, newANNo.getText());
		artifactFlexTable.setText(row, 1, newANNa.getText());
		artifactFlexTable.setText(row, 2, newAlleg.getText());
		artifactFlexTable.setText(row, 3, newE1.getText());
		artifactFlexTable.setText(row, 4, newE2.getText());
		artifactFlexTable.setText(row, 5, newLoc.getText());
		artifactFlexTable.setText(row, 6, newPoss.getText());
		artifactFlexTable.setText(row, 7, newSo.getText());
	}
}
